Sphinx Search
=============

Ceci est la doc technique pour développeur.

Divers
------

Pour obtenir uniquement les lignes actives de la config :

    egrep -v '^(\s*#|\s*$)' src/sphinx/sphinx.conf

Sont uniquement indexés les objets actifs (statut = "normal").


Index titres
------------

Liste des champs de l'index.

titrecomplet
:  full-text and string
:  Can be searched, will also be returned as a parameter.
sigle
:  full-text

lettre1
:  INT
:  rank of the title's first letter (0-26)
cletri
:  string ordinal
:  Used only to sort by title.
obsolete
:  BOOLEAN
revueid
:  INT
issn
:  MVA BIGINT
:  ISSN, ISSN-L, ISSN-E converted to a bigint by removing "-" and the checksum suffix.
langues
:  ??? **Should be a MVA INT**
hdateModif
:  timestamp
hdateVerfi
:  timestamp

editeurid
:  MVA INT
ressourceid
:  MVA INT
:  each "Ressource" that provides a "Service" on this title.
collectionid
:  MVA INT
detenu
:  MVA INT
:  partenaireId from the table "Partenaire_Titre".
suivi
:  MVA INT
:  partenaireId from the table "Suivi".
acces
:  MVA INT
:  list of types, where each value means:
	* 1 = inconnu/vide
	* 2 = Intégral
	* 3 = Résumé
	* 4 = Sommaire
	* 5 = Indexation
acceslibre
:  MVA INT
:  idem where acces='libre'
nbacces
: INT


Index ressources
----------------

nomcomplet
:  full-text and string
:  Can be searched, will also be returned as a parameter.
description
:  full-text
disciplines
:  full-text
lettre1
:  INT
:  rank of the title's first letter (0-26)
cletri
:  string ordinal
:  Used only to sort by title.
autoimport
: BOOLEAN
nbrevues
: INT
suivi
:  MVA INT
:  partenaireId from the table "Suivi".
hdateModif
:  timestamp
hdateVerfi
:  timestamp


Index editeurs
--------------

nomcomplet
:  full-text and string
:  Can be searched, will also be returned as a parameter.
description
:  full-text
geo
:  full-text
complement
:  full-text
lettre1
:  INT
:  rank of the title's first letter (0-26)
cletri
:  string ordinal
:  Used only to sort by title.
suivi
:  MVA INT
:  partenaireId from the table "Suivi".
hdateModif
:  timestamp
hdateVerfi
:  timestamp


