<?php

/**
 * This is the bootstrap file for unit tests.
 */

defined('YII_TEST') or define('YII_TEST', 1);

// disable Yii error handling logic
defined('YII_ENABLE_EXCEPTION_HANDLER') or define('YII_ENABLE_EXCEPTION_HANDLER', false);
defined('YII_ENABLE_ERROR_HANDLER') or define('YII_ENABLE_ERROR_HANDLER', false);

// Composer autoload
require_once dirname(__DIR__, 2) . '/vendor/autoload.php';

// Yii autoload with lowest priority
require_once dirname(__DIR__, 2) . '/vendor/yiisoft/yii/framework/YiiBase.php';
require_once dirname(__DIR__, 2) . '/vendor/codeception/yii-bridge/web/CodeceptionHttpRequest.php';
require_once dirname(__DIR__, 2) . '/src/www/protected/components/Yii.php';

$basePath = dirname(__DIR__, 2) . '/src/www/protected';
$config = CMap::mergeArray(
	CMap::mergeArray(require "$basePath/config/main.php", require "$basePath/config/local.php"),
	require "$basePath/config/test.php"
);
$config['basePath'] = $basePath;
unset($basePath);

$_SERVER['SERVER_NAME'] = 'localhost';

Yii::setApplication(null);
Yii::createWebApplication($config);
