<?php

class InterventionTest extends \Codeception\Test\Unit
{
	public function testListInterventionsSuivi()
	{
		$interventions = models\renderers\InterventionHelper::listInterventionsSuivi(1, 'accepté', 1356082800);
		$this->assertCount(10, $interventions);
		$this->assertEquals("Modification d'accès en ligne, revue « Recueil Dalloz » / « Sign@l »", $interventions[0]->description);
	}
}
