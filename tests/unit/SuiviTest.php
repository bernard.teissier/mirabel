<?php

class SuiviTest extends \Codeception\Test\Unit
{
	public function testListInterventionsNotTracked()
	{
		$since = 1569231125;
		$interventions = \Suivi::listInterventionsNotTracked('accepté', $since);
		// 5 + delete Editeur + delete Revue + update Service + ...
		$this->assertGreaterThan(4, $interventions);
		$this->assertEquals("Modification du titre « Actualité Juridique Droit Administratif »", $interventions[0]->description);

		foreach ($interventions as $intv) {
			$this->assertEquals('accepté', $intv->statut);
			$this->assertNotEquals(296, (int) $intv->ressourceId); // ressource suivie
			$this->assertNotEquals(444, (int) $intv->revueId); // revue suivie
			$this->assertNotEquals(28, (int) $intv->revueId); // revue suivie
			$this->assertGreaterThanOrEqual($since, $intv->hdateProp);
			$this->assertGreaterThanOrEqual($intv->hdateProp, $intv->hdateVal);
			$this->assertEquals(0, $intv->import);
		}
		$this->assertGreaterThanOrEqual($interventions[0]->hdateProp, $interventions[1]->hdateProp); // ASC (older first)
	}
}
