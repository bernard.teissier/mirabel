<?php

class PagerAlphaTest extends \Codeception\Test\Unit
{
	public function testEmpty()
	{
		$pager = new PagerAlpha();
		$pager->init();
		$this->assertEquals([1, 27], $pager->getPageRange());
		$this->assertEquals("0", $pager->getCurrentLetter());
	}

	/**
	 * @dataProvider providesLetters
	 */
	public function testMulti($counts, $initLetter, $selLetter, $expectLetters)
	{
		$pager = new PagerAlpha();
		$pager->setLettersFilter($counts);
		$pager->setCurrentLetter($initLetter);
		$this->assertEquals($selLetter, $pager->getCurrentLetter());
		$this->assertEquals(ord($selLetter) - 64, $pager->getCurrentPage());
		/*
		$this->assertEquals($expectLetters, $pager->getPageRange());
		 */
	}

	public function providesLetters()
	{
		return array(
			array([3,1,2,3,1,2,3], "B", "B", 7),
			array([0,1,2,3,1,2,3], "A", "A", 6),
			array([0,0,0,0,0,3], "0", "E", 1),
		);
	}

	public function testLinks()
	{
		$pager = new PagerAlpha;
		$pager->setLettersFilter(array(0,1,2));
		$pager->setCurrentLetter('B');
		/*
		ob_start();
		$pager->run();
		$contents = ob_get_contents();
		ob_end_clean();
		$this->assertSelectCount('li', 2, $contents);
		$this->assertRegExp('#href="[^"]*phpunit/urltest\?lettre=A"#', $contents);
		 */
	}
}
