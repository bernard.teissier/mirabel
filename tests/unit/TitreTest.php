<?php

class TitreTest extends \Codeception\Test\Unit
{
	/**
	 * @dataProvider provideFillBySudocNotices
	 */
	public function testFillBySudocNotices(array $expect, array $titreAttr, array $noticesAttr)
	{
		$notices = [];
		foreach ($noticesAttr as $noticeAttr) {
			$n = new \models\sudoc\Notice();
			foreach ($noticeAttr as $k => $v) {
				$n->$k = $v;
			}
			$notices[] = $n;
		}

		$titre = new Titre();
		$titre->setAttributes($titreAttr, false);
		$titre->fillBySudocNotices($notices);

		$this->assertEquals($expect, $titre->getAttributes(array_keys($expect)));
	}

	public function provideFillBySudocNotices()
	{
		return [
			[
				// simple case where 2 notices add distinct fields
				[
					// expect
					'dateDebut' => '1993',
					'dateFin' => '',
					'langues' => 'fre',
					'titre' => "Mon titre",
					'url' => "http://la.bas",
				],
				[
					// titreAttr
					'dateDebut' => '1900',
					'dateFin' => '2000',
					'titre' => "Mon titre avant",
					'url' => "http://la.bas",
				],
				[
					// noticesAttr
					[
						'dateDebut' => '1993',
						'dateFin' => '',
						'langues' => 'fre',
						'titre' => "Mon titre",
					],
					[
						'langues' => 'eng',
						'titre' => "Mon titre",
						'url' => "http://la.bas/nouveau",
					],
				]
			],
			[
				// The second notice only extends the end date.
				[
					'dateDebut' => '1993',
					'dateFin' => '2002',
					'langues' => 'fre',
					'titre' => "Mon titre",
				],
				[],
				[
					[
						'dateDebut' => '1993',
						'dateFin' => '2000',
						'langues' => 'fre',
						'titre' => "Mon titre",
						'url' => "http://la.bas",
					],
					[
						'dateFin' => '2002',
						'titre' => "Mon titre bis",
						'url' => "http://la.bas/bis",
					],
				]
			],
			[
				// complex date inputs
				[
					'dateDebut' => '1993',
					'dateFin' => '2002',
				],
				[],
				[
					[
						'dateDebut' => '1995',
						'dateFin' => '2000',
					],
					[
						'dateDebut' => '',
						'dateFin' => '',
					],
					[
						'dateDebut' => '1993',
						'dateFin' => '2002',
					],
				]
			],
			[
				// complex date inputs
				[
					'dateDebut' => '1993',
					'dateFin' => '',
				],
				[],
				[
					[
						'dateDebut' => '1995',
						'dateFin' => '2000',
					],
					[
						'dateDebut' => '1999',
						'dateFin' => '',
					],
					[
						'dateDebut' => '1993',
						'dateFin' => '2002',
					],
				]
			],
		];
	}
}
