<?php

use models\lang\Normalize;

class LangNormalize extends \Codeception\Test\Unit
{
	/**
	 * @dataProvider providesDates
	 */
	public function testfromStr($expect, $from)
	{
		$this->assertEquals($expect, Normalize::fromStr($from));
	}

	public function providesDates()
	{
		return [
			["fre", "fre"],
			["fre", "fra "],
			["fre", " fra , "],
			["eng, fre, spa", "eng,fra,  spa "],
			["eng, fre, spa", "en fr, esp,"],
		];
	}
}
