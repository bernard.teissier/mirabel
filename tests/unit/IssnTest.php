<?php

use models\sudoc\Api;

class IssnTest extends \Codeception\Test\Unit
{
	public function testFillBySudoc()
	{
		// create an Issn record
		$issn = new \Issn();
		$issn->setAttributes(
			[
				'titreId' => 1564,
				'issn' => "1661-1802",
				'dateDebut' => '2010',
				'statut' => \Issn::STATUT_ENCOURS,
				'sudocNoHolding' => true,
			],
			false
		);
		$this->assertEquals("2010", $issn->dateDebut);

		// update it with a stub Sudoc API
		$issn->fillBySudoc($this->createStubApi());

		// check the result
		$this->assertEquals(1564, $issn->titreId);
		$this->assertEquals("2010", $issn->dateDebut);
		$this->assertEquals("", $issn->dateFin);
		$this->assertEquals('1661-1802', $issn->issn);
		$this->assertEquals(\Issn::STATUT_VALIDE, $issn->statut);
		$this->assertEquals('090133803', $issn->sudocPpn);
		$this->assertFalse($issn->sudocNoHolding);
		$this->assertEquals("RESSI", $issn->titreReference);
		$this->assertEquals(\Issn::SUPPORT_ELECTRONIQUE, $issn->support);

		$this->assertTrue($issn->validateUnicity($issn->titre));
		$issn->issn = '1952-403X'; // not unique, used as issn-e
		$this->assertFalse($issn->validateUnicity($issn->titre));

	}

	private function createStubApi()
	{
		$content = [
				'ppn' => "090133803",
				'sudocNoHolding' => false,
				'titre' => "RESSI",
				'titreIssn' => "RESSI",
				'dateDebut' => "",
				'dateFin' => "",
				'issn' => "1661-1802",
				'issnl' => "1661-1802",
				'statut' => \Issn::STATUT_VALIDE,
				'support' => \Issn::SUPPORT_ELECTRONIQUE,
		];
		$notice = new \models\sudoc\Notice();
		foreach ($content as $k => $v) {
			$notice->$k = $v;
		}
		$ppn = new \models\sudoc\Ppn('090133803', false);

		return $this->make(
			Api::class,
			[
				'getNotice' => $notice,
				'getPpn' => [$ppn],
			]
		);
	}
}
