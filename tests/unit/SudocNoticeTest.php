<?php

use models\marcxml\Parser;
use models\sudoc\Notice;

class SudocNoticeTest extends \Codeception\Test\Unit
{
	private $samples = [];

	public function setUp()
	{
		parent::SetUp();
		$dataDir = dirname(__DIR__) . '/_data';
		$parser = new Parser();

		$this->samples["039228460"] = $parser->parse(file_get_contents("$dataDir/039228460.xml"));
		$this->samples["090133803"] = $parser->parse(file_get_contents("$dataDir/090133803.xml"));
		$this->samples["119905655"] = $parser->parse(file_get_contents("$dataDir/119905655.xml"));
	}

	public function testFromMarcXml()
	{
		$tests = [
			"039228460" => [
				'ppn' => "039228460",
				'bnfArk' => 'cb343491837',
				'dateDebut' => "",
				'dateFin' => "",
				'issn' => "0035-2950",
				'issnl' => "0035-2950",
				'pays' => 'FR',
				'statut' => \Issn::STATUT_VALIDE,
				'sudocNoHolding' => false,
				'support' => \Issn::SUPPORT_PAPIER,
				'titre' => "Revue française de science politique",
				'titreIssn' => "Revue française de science politique",
				'url' => 'http://www.afsp.msh-paris.fr/publi/rfsp/rfsp.html',
				'worldcat' => '185444438',
			],
			"090133803" => [
				'ppn' => "090133803",
				'bnfArk' => null,
				'dateDebut' => "",
				'dateFin' => "",
				'issn' => "1661-1802",
				'issnl' => "1661-1802",
				'pays' => 'CH',
				'sudocNoHolding' => false,
				'support' => \Issn::SUPPORT_ELECTRONIQUE,
				'titre' => "RESSI",
				'titreIssn' => "RESSI",
				'url' => 'http://www.ressi.ch',
				'worldcat' => 61762295,
			],
			"119905655" => [
				'ppn' => "119905655",
				'dateDebut' => "2005",
				'dateFin' => "",
				'bnfArk' => null,
				'issn' => null,
				'pays' => 'FR',
				'sudocNoHolding' => true,
				'support' => \Issn::SUPPORT_PAPIER,
				'titre' => "Collection La France de Jules Verne",
				'titreIssn' => null,
				'url' => null,
				'worldcat' => null,
			],
		];

		foreach ($tests as $ppn => $expected) {
			$notice = new Notice();
			$notice->fromMarcXml($this->samples[$ppn]);
			foreach ($expected as $eK => $eV) {
				$this->assertEquals($eV, $notice->$eK, "PPN $ppn: field '$eK' must have value '$eV'");
			}
		}
	}
}
