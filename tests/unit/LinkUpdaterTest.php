<?php

use commands\models\LinkUpdater;

class LinkUpdaterTest extends \Codeception\Test\Unit
{
	public function testValidation()
	{
		$source = Sourcelien::model()->findByAttributes(['nomcourt' => "romeo"]);
		$this->assertInstanceOf(Sourcelien::class, $source);

		$titreBackup = Titre::model()->findByPk(725);

		$titre = Titre::model()->findByPk(725);
		$titre->urlCouverture = "https://www-degruyter-com/doc/cover/s0722480X.jpg";
		$titre->save(false);
		$updater = new LinkUpdater($source);
		$csv = $updater->updateLink($titre, "0003-441X", "https://example.com/do-not-existtt");
		$this->assertEquals($csv, "718;725;Annales d'histoire économique et sociale;0003-441X;https://example.com/do-not-existtt;Lien ajouté;OK\n");

		$titreBackup->save(false);
	}

	public function testMultiple()
	{
		$titreBackup = Titre::model()->findByPk(1229);

		$sourceWen = Sourcelien::model()->findByAttributes(['nomcourt' => "wikipedia-en"]);
		$sourceWfr = Sourcelien::model()->findByAttributes(['nomcourt' => "wikipedia-fr"]);
		$titre = Titre::model()->findByPk(1229);

		$updater = new LinkUpdater();
		$updater->verbose = 2;
		$updater->addSource($sourceWen);
		$updater->addSource($sourceWfr);
		$csv = $updater->updateLinks(
			$titre,
			"XXX",
			[
				[$sourceWen->id, "https://en.wikipedia.org/tralala"],
				[$sourceWfr->id, "https://fr.wikipedia.org/tralala"],
			]
		);
		$this->assertEquals(
			$csv,
			"1174;1229;American journal of political science;XXX;https://en.wikipedia.org/tralala;Lien modifié;OK\n"
			. "1174;1229;American journal of political science;XXX;https://fr.wikipedia.org/tralala;Lien ajouté;OK\n"
		);

		$titreBackup->save(false);
	}
}
