<?php

use models\lang\Convert;

class LangConvert extends \Codeception\Test\Unit
{
	/**
	 * @dataProvider providesLangs
	 */
	public function testLists($expect, $from)
	{
		$this->assertEquals($expect, Convert::codesToFullNames($from));
	}

	public function providesLangs()
	{
		return [
			["français", "fre"],
			["français", "fra "],
			["français", " fra , "],
			["anglais, français, espagnol", "eng,fra,  spa "],
			["anglais, français, espagnol", "en fr, esp, nawak"],
		];
	}
}
