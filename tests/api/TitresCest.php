<?php 

class TitresCest
{
	public function abonnement(\ApiTester $I)
	{
		$I->sendGet('mes/titres?abonnement=1&partenaire=47f9a24f244a4aa664dc739157c89b6a');
		$I->seeResponseCodeIs(200);
		$I->seeResponseIsJson();
		$response = json_decode($I->grabResponse());
		$I->assertCount(16, $response);

		$I->sendGet('titres?abonnement=1');
		$I->seeResponseCodeIs(200);
		$I->seeResponseIsJson();
		$I->seeResponseContainsJson(['message' => "Arguments non-valides"]);
	}

	public function abonnementEtPossession(\ApiTester $I)
	{
		$I->sendGet('mes/titres?partenaire=47f9a24f244a4aa664dc739157c89b6a&abonnement=1&possession=1');
		$I->seeResponseCodeIs(200);
		$I->seeResponseIsJson();
		$response = json_decode($I->grabResponse());
		$I->assertCount(17, $response);
		$I->assertSame(false, $response[0]->issns[0]->sudocnoholding);
		$I->assertSame(474464968, $response[0]->issns[0]->worldcatocn);
	}

	public function all(\ApiTester $I)
	{
		$I->sendGet('titres');
		$I->seeResponseCodeIs(200);
		$I->seeResponseIsJson();
		$response = json_decode($I->grabResponse());
		$I->assertCount(35, $response);
	}

	public function combined(\ApiTester $I)
	{
		$I->sendGet('titres?issn=0247-3739,1662-8667,0246-9731&titre=%25marxiste%25');
		$I->seeResponseCodeIs(200);
		$I->seeResponseIsJson();
		$response = json_decode($I->grabResponse());
		$I->assertCount(1, $response);
	}

	public function issn(\ApiTester $I)
	{
		$I->sendGet('titres?issn=1955-2564');
		$I->seeResponseCodeIs(200);
		$I->seeResponseIsJson();
		$response = json_decode($I->grabResponse());
		$I->assertCount(1, $response);

		$I->sendGet('titres?issn=0001-7728,1760-7558');
		$I->seeResponseCodeIs(200);
		$I->seeResponseIsJson();
		$response = json_decode($I->grabResponse());
		$I->assertCount(1, $response);

		$I->sendGet('titres?issn=0370-1662');
		$I->seeResponseCodeIs(200);
		$I->seeResponseIsJson();
		$response = json_decode($I->grabResponse());
		$I->assertCount(0, $response);

		$I->sendGet('titres?issn=contrario');
		$I->seeResponseIsJson();
		$I->seeResponseContainsJson(['message' => "Arguments non-valides"]);
		$I->seeResponseContainsJson(['fields' => "issn"]);
	}

	public function revueid(\ApiTester $I)
	{
		$I->sendGet('titres?revueid=718');
		$I->seeResponseCodeIs(200);
		$I->seeResponseIsJson();
		$response = json_decode($I->grabResponse());
		$I->assertCount(6, $response);
		$I->assertNull($response[0]->obsoletepar);
		$I->assertNotNull($response[1]->obsoletepar);
		$I->assertCount(2, $response[0]->issns);
	}

	public function titre(\ApiTester $I)
	{
		$I->sendGet('titres?titre=A%20contrario%');
		$I->seeResponseCodeIs(200);
		$I->seeResponseIsJson();
		$response = json_decode($I->grabResponse());
		$I->assertCount(1, $response);

		$I->sendGet('titres?titre=%25contrario%25');
		$I->seeResponseCodeIs(200);
		$I->seeResponseIsJson();
		$response = json_decode($I->grabResponse());
		$I->assertCount(1, $response);

		$I->sendGet('titres?titre=A%20contrario');
		$I->seeResponseCodeIs(200);
		$I->seeResponseIsJson();
		$response = json_decode($I->grabResponse());
		$I->assertCount(0, $response);
	}
}
