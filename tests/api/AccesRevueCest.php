<?php

class AccesRevueCest
{
	public function invalid(\ApiTester $I)
	{
		$I->sendGet('acces/revue');
		$I->seeResponseIsJson();
		$I->seeResponseContainsJson(['code' => 500]);
	}

	public function issn(\ApiTester $I)
	{
		$I->sendGet('acces/revue?issn=1243-258X');
		$I->seeResponseCodeIs(200);
		$I->seeResponseIsJson();
		$response = json_decode($I->grabResponse());
		$I->assertCount(16, $response);

		$I->assertArrayHasKey('issn', (array) $response[0]);
		$I->assertEquals(["0395-2649", "1953-8146"], $response[0]->issn);
		//$I->assertFalse($response[0]->lacunaire);
		//$I->assertFalse($response[0]->selection);

		$I->assertEquals("JSTOR", $response[2]->ressource);
		$I->assertEquals("restreint", $response[2]->diffusion);
		$I->assertEquals("Cairn.info", $response[4]->ressource);
		$I->assertEquals("restreint", $response[4]->diffusion);
	}

	public function issnEtAbonnement(\ApiTester $I)
	{
		$I->sendGet('acces/revue?issn=1243-258X&partenaire=47f9a24f244a4aa664dc739157c89b6a');
		$I->seeResponseCodeIs(200);
		$I->seeResponseIsJson();
		$response = json_decode($I->grabResponse());
		$I->assertCount(16, $response);

		$I->assertEquals("JSTOR", $response[2]->ressource);
		$I->assertEquals("restreint", $response[2]->diffusion);
		$I->assertEquals("Cairn.info", $response[4]->ressource);
		$I->assertEquals("abonné", $response[4]->diffusion);
	}

	public function revueid(\ApiTester $I)
	{
		$I->sendGet('acces/revue?revueid=718');
		$I->seeResponseCodeIs(200);
		$I->seeResponseIsJson();
		$response = json_decode($I->grabResponse());
		$I->assertCount(16, $response);
		$I->assertArrayNotHasKey('issn', (array) $response[0]);
	}

	public function revueidNotFound(\ApiTester $I)
	{
		$I->sendGet('acces/revue?revueid=123456789');
		$I->seeResponseIsJson();
		$I->seeResponseContainsJson(['code' => 404]);
	}

	public function sudoc(\ApiTester $I)
	{
		$I->sendGet('acces/revue?sudoc=038729385');
		$I->seeResponseIsJson();
		$response = json_decode($I->grabResponse());
		$I->assertCount(16, $response);
	}

	public function worldcatNotFound(\ApiTester $I)
	{
		$I->sendGet('acces/revue?worldcat=987654321');
		$I->seeResponseIsJson();
		$I->seeResponseContainsJson(['code' => 404]);
	}
}
