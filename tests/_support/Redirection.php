<?php

namespace Helper;

/**
 * \Helper\Redirection is a CodeCeption Module that enables redirections in Yii1 functional testing.
 * 
 * By default, Codeception Yii1 does not follow redirects (HTTP 30X),
 * and there is no simple way to do it,
 * except adding `$this->followRedirects(true);` in `doRequest()`, `Connector/Yii1.php`
 */
class Redirection extends \Codeception\Module
{
	/**
	 * Toggle redirections until the test ends.
	 *
	 * @param bool $follow
	 */
	function followRedirects($follow)
	{
		$this->getModule('Yii1')->client->followRedirects($follow);
	}
}