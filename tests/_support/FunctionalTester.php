<?php


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class FunctionalTester extends \Codeception\Actor
{
    use _generated\FunctionalTesterActions;

	public function loginAs($username, $password = "mdp")
	{
		$this->amOnPage('/site/login');
		$this->fillField("Identifiant", $username);
		$this->fillField("Mot de passe", $password);
		$this->click("Se connecter");
		$this->dontSeeElement('.help-inline error');
	}

	public function logout()
	{
		$this->amOnPage('/site/logout');
	}
}
