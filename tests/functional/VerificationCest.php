<?php

class VerificationCest
{
	public function issn(FunctionalTester $I)
	{
		$I->amOnPage('/revue/');
		$I->loginAs('milady');
		$I->amOnPage('/verification');
		$I->see('Vérifications', 'h1');
		$I->click("ISSN-L partagés");
		$I->see("Au moins deux titres de ces revues ont le même ISSN-L.");
		$I->seeNumberOfElements('#issnl-partages li', 7);
	}

	public function generales(FunctionalTester $I)
	{
		$I->amOnPage('/revue/');
		$I->loginAs('milady');
		$I->amOnPage('/verification');
		$I->click("Collections temporaires");
		$I->seeNumberOfElements('#collections-temporaires li', 2);
	}

	public function wikidata(FunctionalTester $I)
	{
		$I->loginAs('milady');
		$I->amOnPage('/verification');
		$I->click("Wikidata");
		$I->seeNumberOfElements('#wikidata-grid tbody tr', 1);
		$I->see("Aucun résultat trouvé.", '#wikidata-grid tbody td');

		$I->amOnPage('/verification/wikidata?Wikidata[property]=P7363');
		$I->see("Cache Wikidata - P7363 issnl", 'h1');
		$I->seeNumberOfElements('#wikidata-grid tbody tr', 1);
		$I->dontSee("Aucun résultat trouvé.", '#wikidata-grid tbody td');
	}
}
