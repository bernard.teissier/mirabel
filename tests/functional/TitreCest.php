<?php

class TitreCest
{
	public function ajaxComplete(FunctionalTester $I)
	{
		$I->amOnPage('/titre/ajaxComplete?term=juridique');
		$I->seeInSource('Actualit\u00e9 Juridique Droit Administratif');
	}

	public function ajaxIssn(FunctionalTester $I)
	{
		$I->amOnPage('/titre/ajaxIssn?position=0&titreId=1');
		$I->seeInField('input[name="Issn[0][titreId]"]', '1');
	}

	public function proposeUpdate(FunctionalTester $I)
	{
		$I->loginAs('milady');
		$I->amOnPage('/revue/20');
		$I->dontSeeElement('#revue-titres tr:first-child a[title="Proposer une modification"]');
		$I->logout();

		$I->amOnPage('/revue/20');
		$I->click('#revue-titres tr:first-child a[title="Proposer une modification"]');
		$I->see("Modifier le titre Recueil Dalloz", 'h1');
		$I->seeInField('Titre[liens][0][url]', 'http://www.jurisguide.fr/fiches-documentaires/recueil-dalloz/');
		$I->seeInField('Titre[liens][2][url]', 'https://biblioweb.hypotheses.org/16577');
		$I->see("Les modifications que vous proposez", '.alert');
	}

	public function proposeUpdateRelation(FunctionalTester $I)
	{
		$I->loginAs('milady');
		$I->amOnPage('/titre/update/6');
		$I->see("Modifier le titre Esprit", 'h1');
		$I->see("Les modifications que vous proposez", '.alert');
		$I->seeCheckboxIsChecked('fieldset.editeurs input[type="checkbox"]');
		$I->uncheckOption('fieldset.editeurs input[type="checkbox"]');
		$I->fillField('#Intervention_commentaire', "Retire l'éditeur");
		$I->click("Proposer cette modification", '.form-actions');
		$I->seeCurrentUrlEquals('/revue/6/Esprit');
		/*
		 * Yii1 checkboxes cannot be tested with Codeception!
		 *
		 * Submiting this form only removes the dynamic fields (Autres liens).
		 *
		$I->amOnPage('/intervention/admin?q[titre_titre]=esprit');
		$I->click("Voir", '#intervention-grid tbody');
		$I->see("Modification du titre", 'h2');
		$I->see("Détache l'éditeur « Esprit »", '#intervention-detail');
		 */
		$I->logout();
	}

	/**
	 * @todo What side-effect makes the test update() fail when run after this one?
	 */
	public function create(FunctionalTester $I)
	{
		$I->loginAs('milady');
		$I->amOnPage('/titre/create');
		$I->see("Créer une nouvelle revue", 'h1');
		$I->see("Les modifications seront appliquées immédiatement.", '.alert');
		$I->fillField("#Titre_titre", "Ma revue temporaire");
		$I->checkOption("Suivre cette revue (trois mousquetaires)");
		$I->click("Enregistrer", '.form-actions');
		$I->see("Ma revue temporaire", 'h1');
		$I->see("suit cette revue dans Mir@bel", '.objets-suivis');

		$I->click("Supprimer cette revue");
		$I->see("Revues", 'h1');
	}

	public function createByIssn(FunctionalTester $I)
	{
		$I->amOnPage('/titre/createByIssn');
		$I->seeElement('#create-without-issn');
		$I->click('#create-without-issn');
		$I->see("Créer une nouvelle revue", 'h1');
	}

	public function view(FunctionalTester $I)
	{
		$I->amOnPage('/revue/20');
		$I->dontSeeLink('', '/titre/20');
		$I->loginAs('milady');
		$I->seeLink('', '/titre/20');
		$I->click('#revue-titres tr:first-child a[title="Détails de ce titre"]');
		$I->see("Détail du Titre Recueil Dalloz", 'h1');
		$I->seeLink('https://biblioweb.hypotheses.org/16577');
	}
}
