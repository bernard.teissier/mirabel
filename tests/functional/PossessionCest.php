<?php

class PossessionCest
{
	public function export(FunctionalTester $I)
	{
		$I->amOnPage('/partenaire/1');
		$I->loginAs("d'artagnan");
		$I->amOnPage('/possession/export?partenaireId=1');
		$I->seeInSource('possession;bouquet;titre;"titre ID";issn;issne;issnl;"revue ID";"identifiant local"');
		$I->seeInSource('1;;"A contrario. Revue interdisciplinaire de sciences sociales";444;1660-7880;1662-8667;1660-7880;444;419857');
		$I->seeInSource('1;;"Vingtième siècle, revue d\'histoire";98;0294-1759;1950-6678;0294-1759;98;419674');
	}
}
