<?php

class AdminCest
{
	public function disconnectEverybody(FunctionalTester $I)
	{
		// block connections
		$I->loginAs("d'artagnan");
		$I->amOnPage('/admin');
		$I->click("Déconnexion générale");
		$I->logout();

		// log-in fails
		$I->amOnPage('/site/login');
		$I->fillField("Identifiant", "frollo");
		$I->fillField("Mot de passe", "mdp");
		$I->click("Se connecter");
		$I->see("maintenance", '.alert-error');

		// enable connections
		$I->loginAs("d'artagnan");
		$I->amOnPage('/admin');
		$I->click("Autoriser les connexions");
		$I->logout();

		// log-in succeeds
		$I->amOnPage('/site/login');
		$I->fillField("Identifiant", "frollo");
		$I->fillField("Mot de passe", "mdp");
		$I->click("Se connecter");
		$I->seeCurrentUrlEquals('/');
	}
}
