<?php

class DocCest
{
	public function search(FunctionalTester $I)
	{
		$I->amOnPage('/');
		$I->loginAs('milady');
		$I->amOnPage('/doc');
		$I->see("plan du site", 'a');

		$I->click('rss', 'li a');
		$I->see("Flux RSS", 'h1');
	}
}
