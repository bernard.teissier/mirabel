<?php

class InterventionCest
{
	public function adminAndView(FunctionalTester $I)
	{
		$I->amOnPage('/revue/444');
		$I->dontSeeLink('historique des interventions', '/intervention/admin');
		$I->loginAs('milady');
		$I->seeElement('a[title="historique des interventions"]');

		$I->click('historique des interventions');
		$I->see("Interventions", 'h1');
		$I->see("total de 60", '.grid-view .summary');

		$I->click('a[href="/intervention/98594"]', '.button-column');
		$I->see("Intervention manuelle");
		$I->see("Titre de référence", '#intervention-detail');
	}

	public function revertThenAccept(FunctionalTester $I)
	{
		$I->loginAs('milady');
		$I->amOnPage('/intervention/139058');
		$I->see("Modification d'accès en ligne", 'h2');
		$I->see("Vol. 14, no 14", '.content-after');
		$I->click("Annuler cette intervention");
		$I->see("refusé", 'tr.alert');
		$I->click("Accepter");
		$I->see("Accepté", 'tr.alert');
	}
}
