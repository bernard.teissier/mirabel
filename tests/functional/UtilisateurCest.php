<?php

class UtilisateurCest
{
	public function update(FunctionalTester $I)
	{
		$I->loginAs("milady");
		$I->amOnPage('/utilisateur/view/2');
		$I->click("Modifier", '#sidebar a');
		$I->seeCheckboxIsChecked("Abonné à la liste [mirabel_contenu]");
		$I->uncheckOption('#Utilisateur_listeDiffusion');
		$I->submitForm('#utilisateur-form', ['Utilisateur[listeDiffusion]' => '0']);
		$I->see("Liste de diffusion", '.detail-view tr:last-child th');
		$I->see("Non", '.detail-view tr:last-child td');
	}

	public function view(FunctionalTester $I)
	{
		$I->loginAs("d'artagnan");
		$I->amOnPage('/utilisateur/view/1');
		$I->see("Charles de Batz de Castelmore d'Artagnan", 'h1');
		$I->see("Administrateur", '.detail-view');
	}

	public function create(FunctionalTester $I)
	{
		$I->loginAs("d'artagnan");
		$I->amOnPage('/utilisateur/view/1');
		$I->click("Créer un utilisateur");
		$I->see("Nouvel utilisateur", 'h1');
		$I->seeCheckboxIsChecked("Abonné à la liste [mirabel_contenu]");
		$I->amOnPage('/utilisateur/view/1');
		$I->logout();
	}
}
