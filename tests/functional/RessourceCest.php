<?php

class RessourceCest
{
	public function abonnements(FunctionalTester $I)
	{
		$I->loginAs('milady');

		$I->amOnPage('/ressource/abonnements/3');
		$I->see("Cairn", 'h1');
		$I->see("Bouquet Général", 'h2');
		$I->see("Aucun abonné");

		$I->amOnPage('/ressource/abonnements/5');
		$I->see("Armand Colin", 'h1');
		$I->see("Armand Colin", 'h2');
		$I->see("Aucun abonné");
	}

	public function ajaxComplete(FunctionalTester $I)
	{
		$I->amOnPage('/ressource/ajaxComplete?term=Arm');
		$I->seeInSource('Armand Colin');
	}

	public function createAndDelete(FunctionalTester $I)
	{
		$I->loginAs('milady');
		$I->amOnPage('/ressource/create');
		$I->see("Nouvelle ressource", 'h1');
		$I->fillField('Nom', "ressource temporaire");
		$I->fillField('Préfixe', "Ma ");
		$I->fillField('Adresse web', "http://maressource.localhost");

		$I->click('Créer');
		$I->see("Ma ressource temporaire", 'h1');

		$I->click('Supprimer');
		$I->see("Cette ressource n'est pas utilisée", '.alert-info');
		$I->click('Confirmer la suppression');
		$I->see("Ressources", 'h1');
		//$I->see("La ressource a été supprimée.", '.alert-success');
	}

	public function index(FunctionalTester $I)
	{
		$I->amOnPage('/ressource');
		$I->seeNumberOfElements('tr.ressource', 4);
		$I->seeNumberOfElements('tr.ressource.suivi-none', 4);
		$I->click("S", '.pagination');
		$I->seeNumberOfElements('tr.ressource', 1);
		$I->seeNumberOfElements('tr.ressource.suivi-other', 1);
	}

	public function viewAndExhaustive(FunctionalTester $I)
	{
		// Guest user sees the column iff not empty
		$I->amOnPage('/ressource/3/Cairn-info');
		$I->dontSee("Couverture", '#ressource-collections thead th');
		$I->dontSee("exhaustive", '#ressource-collections');
		$I->amOnPage('/ressource/143/JSTOR');
		$I->see("Couverture", '#ressource-collections thead th');
		$I->see("exhaustive", '#ressource-collections');

		// Authenticated user sees a different column
		$I->loginAs('milady');
		$I->amOnPage('/ressource/3/Cairn-info');
		$I->dontSee("Couverture", '#ressource-collections thead th');
		$I->dontSee("exhaustive", '#ressource-collections');
		$I->amOnPage('/ressource/143/JSTOR');
		$I->dontSee("Couverture", '#ressource-collections thead th');
		$I->see("exhaustive", '#ressource-collections');
		$I->logout();
	}

	public function update(FunctionalTester $I)
	{
		$I->amOnPage('/ressource/3/Cairn-info');
		$I->click('a[title="Proposer une modification"]');
		$I->see("Modifier la ressource", 'h1');
		$I->see("Les modifications que vous proposez devront être validées", '.alert');
		$I->see("Proposer cette modification", 'button');

		// Authenticated user
		$I->loginAs('milady');
		$I->see("Les modifications seront appliquées immédiatement.", '.alert');
		$I->dontSee("Proposer cette modification", 'button');
		$I->selectOption("Type d'accès", "Inconnu");
		$I->fillField("Diffuseur", "Raminagrobis");

		$I->click("Enregistrer", 'button');
		$I->seeCurrentUrlMatches('#/ressource/3/Cairn-info$#');
		$I->see("Raminagrobis", '.detail-view');
	}
}
