<?php

class CmsCest
{
	public function render(FunctionalTester $I)
	{
		$I->amOnPage('/site/page/aide');
		$I->see("aide à la recherche", 'h2');
		$I->see("API Mir@bel", '#page-aide a');
	}

	public function update(FunctionalTester $I)
	{
		$I->amOnPage('/site/contact');
		$I->loginAs("d'artagnan");
		$I->amOnPage('/cms');
		$I->see("Liste des blocs éditoriaux", 'h1');

		$I->click('a[title="Mettre à jour"]', '.button-column');
		$I->see("Modifier le bloc", 'h1');
		$I->see("accueil-actu", 'h1');
	}

	public function view(FunctionalTester $I)
	{
		$I->amOnPage('/site/contact');
		$I->loginAs("d'artagnan");
		$I->amOnPage('/cms');

		$I->click('a[title="Voir"]', '.button-column');
		$I->see("Bloc éditorial", 'h1');
		$I->see("accueil-actu", 'h1');
		$I->seeElement('td a[href="/site/page/actualite"]');
	}
}
