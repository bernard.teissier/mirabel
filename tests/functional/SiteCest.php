<?php

class SiteCest
{
	public function contact(FunctionalTester $I)
	{
		$I->amOnPage('/site/contact');
		$I->see("Nous contacter", 'h1');
		$I->fillField("Votre nom", "Bibi");
		$I->fillField("Votre courriel", "bibi@example.org");
		$I->fillField("Sujet", "nada");
		$I->fillField("Message", "Tralala\nlala.");
		$I->click("Envoyer");
	}

	public function search(FunctionalTester $I)
	{
		$I->amOnPage('/revue');
		$I->fillField('input[aria-label="recherche"]', "dalloz");
		$I->click('.form-search button[type="submit"]');
		$I->see("Revues", 'h2');
		$I->seeLink('Actualité Législative Dalloz');
		$I->see("Ressources", 'h2');
		$I->see("Éditeurs", 'h2');
	}

	public function searchUnique(FunctionalTester $I)
	{
		$I->amOnPage('/');
		$I->fillField('input[aria-label="recherche"]', "contrario");
		$I->click('.form-search button[type="submit"]');
		$I->seeCurrentUrlEquals('/revue/444/A-contrario-Revue-interdisciplinaire-de-sciences-sociales');
	}
}
