<?php

class RevueCest
{
	public function categoriesDirect(FunctionalTester $I)
	{
		$I->amOnPage('/revue/29');
		$I->see("Notre-Dame de Paris suit cette revue", '#revue-suivi');
		$I->see("Europe de l'Ouest", '#revue-categories a'); // will be removed
		$I->dontSee("Préhistoire", '#revue-categories a'); // will be added
		$I->dontSeeLink("Définir la thématique");
		$I->loginAs("frollo"); // not admin, owner

		$I->click("Définir la thématique");
		$I->see("Thématique pour la revue Annales historiques de la Révolution française", 'h1');
		$I->see("Europe de l'Ouest", '#categorierevue-grid');
		$I->see("Les modifications seront appliquées immédiatement", '.alert');
		$I->seeElement('button[type="submit"]');

		$I->followRedirects(true);
		$I->submitForm(
			"#revue-categories",
			[
				'revueId' => '29',
				'forceProposition' => '0',
				'remove' => ['120'], // Europe de l'Ouest
				'add' => ['84'], // Préhistoire
			]
		);
		$I->seeCurrentUrlMatches('#revue/29/#');
		$I->dontSee("Europe de l'Ouest", '#revue-categories a'); // removed
		$I->see("Préhistoire", '#revue-categories a'); // added
		//$I->see("Les affectations de thèmes ont bien été modifiées", '.alert.alert-success');
	}

	public function categoriesDirectPerm(FunctionalTester $I)
	{
		$I->amOnPage('/revue/29');
		$I->see("Julliard suit cette revue", '#revue-suivi');
		$I->loginAs("editeur-98"); // not admin, owner, partenaire-editeur

		$I->click("Définir la thématique");
		$I->see("Les modifications seront appliquées immédiatement", '.alert');
	}

	public function categoriesProposal(FunctionalTester $I)
	{
		$I->amOnPage('/revue/6');
		$I->loginAs("milady"); // not admin, not owner
		$I->click("Définir la thématique");
		$I->see("Les modifications que vous proposez devront être validées", '.alert');
	}

	public function categoriesProposalPossible(FunctionalTester $I)
	{
		$I->amOnPage('/revue/6');
		$I->see("Cultures et sociétés", '#revue-categories a');
		$I->dontSeeLink("Définir la thématique");
		$I->loginAs("d'artagnan"); // admin

		$I->click("Définir la thématique");
		$I->see("Thématique pour la revue Esprit", 'h1');
		$I->see("Cultures et sociétés", '#categorierevue-grid');
		$I->see("Passer par l'état en attente", '.alert');
		$I->seeElement('button[type="submit"]');

		$I->followRedirects(true);
		$I->submitForm(
			"#revue-categories",
			[
				'revueId' => '6',
				'forceProposition' => '1',
				'remove' => ['2'], // Cultures et sociétés
				'add' => ['84'], // Préhistoire
			]
		);
		$I->seeCurrentUrlMatches('#revue/6/Esprit#');
		$I->see("Cultures et sociétés", '#revue-categories a'); // to be removed
		$I->dontSee("Préhistoire", '#revue-categories a'); // to be added
		//$I->see("Les affectations de thèmes ont bien été proposées", '.alert.alert-success');
	}

	public function delete(FunctionalTester $I)
	{
		$I->amOnPage('/revue/1954');
		$I->see("Suivi", 'h2');
		$I->dontSee("Supprimer cette revue");

		$I->loginAs('milady');
		$I->amOnPage('/revue/1954');
		$I->see("Suivi", 'h2');
		// This will alter the DB and modify the behavior of other tests.
		$I->click("Supprimer cette revue", '#sidebar button');
		// The JS confirm does not appear in this PHP browser
		$I->seeElement('.alert-success');
		$I->seeElement('.alert-danger');
		$I->seeLink("Université de Mahajanga"); // orphan publisher
		$I->logout();
	}

	public function search(FunctionalTester $I)
	{
		$I->amOnPage('/revue/search');
		$I->fillField("Titre", "cont");
		$I->click("Rechercher");
		$I->see("Résultats de 1 à 2");

		$I->selectOption("Pays de l'éditeur", "France");
		$I->click("Rechercher");
		$I->see("Afrique Contemporaine", 'h1');
	}

	public function view(FunctionalTester $I)
	{
		$I->amOnPage('/revue/2');
		$I->seeCurrentUrlMatches('/Afrique-Contemporaine/');
		$I->see("français", '#revue-titre-actif .detail-view');
		$I->dontSeeLink("Regrouper les accès");
		$I->dontSeeLink("Afficher le détail");

		// simulate a cookie for the customization of display
		$_COOKIE['institute'] = 1;
		$I->amOnPage('/revue/2');
		$I->dontSeeLink("Regrouper les accès");
		$I->seeLink("Afficher le détail");

		$I->click("Afficher le détail");
		$I->dontSeeLink("Afficher le détail");
		$I->seeLink("Regrouper les accès");
	}

	public function viewMultipleLang(FunctionalTester $I)
	{
		$I->amOnPage('/revue/1265');
		$I->see("Biscuit chinois", 'h1');
		$I->see("français, anglais", '#revue-titre-actif .detail-view');
	}
}
