<?php

class ApiCest
{
	public function aide(FunctionalTester $I)
	{
		$I->amOnPage('/api');
		$I->see("Modèles de données", 'h2');
	}
}
