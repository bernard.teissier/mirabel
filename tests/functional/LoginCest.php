<?php

class LoginCest
{
	public function loginFails(FunctionalTester $I)
	{
		$I->amOnPage('/site/login');
		$I->fillField("Identifiant", "esmeralda");
		$I->fillField("Mot de passe", "mdp");
		$I->click("Se connecter");
		$I->seeCurrentUrlMatches('#/site/login$#');
		$I->see("Nom ou mot de passe incorrect", '.alert-error');
	}

	public function loginSucceeds(FunctionalTester $I)
	{
		$I->amOnPage('/');
		$I->click("Connexion", 'footer');
		$I->fillField("Identifiant", "frollo");
		$I->fillField("Mot de passe", "mdp");
		$I->click("Se connecter");
		$I->seeCurrentUrlEquals('/');
	}
}
