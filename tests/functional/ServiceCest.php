<?php

class ServiceCest
{
	public function contact(FunctionalTester $I)
	{
		$I->amOnPage('/revue/366/Slence');
		$I->click('a[title="Proposer une modification via un formulaire de contact"]', '.service-4751');
		$I->see("Signaler un problème sur les accès importés", 'h1');
		$I->fillField('Code de vérification', "44");
	}

	public function create(FunctionalTester $I)
	{
		$I->amOnPage('/revue/366/Slence');
		$I->click('a[title="Proposer un nouvel accès"]');
		$I->see("Nouvel accès en ligne", 'h1');
		$I->see("Les modifications que vous proposez devront être validées", '.alert');
		$I->see("Proposer cette création", 'button');
	}

	public function update(FunctionalTester $I)
	{
		$id = 11394; // Service.id

		$I->amOnPage('/revue/366/Slence');
		$I->loginAs('milady');
		$I->see("S!lence", 'h1');
		$I->see("lacunaire", "tr.service-$id");
		$I->dontSee("2000-12-21", "tr.service-$id");
		$I->click('a[title="Modifier"]', "tr.service-$id");
		$I->seeInField("Adresse web", 'http://www.revuesilence.net/anciensnum');
		$I->seeCheckboxIsChecked('Couverture lacunaire');
		$I->fillField('Date de fin', '2002-12-21');

		$I->click('Modifier');
		$I->see("S!lence", 'h1');
		$I->see("décembre 2002", "tr.service-$id");
	}
}
