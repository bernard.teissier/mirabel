<?php

class PartenaireCest
{
	public function abonnements(FunctionalTester $I)
	{
		$I->amOnPage('/partenaire/1');
		$I->loginAs("d'artagnan");
		$I->amOnPage('/partenaire/abonnements/1');
		$I->see("Vous avez 27 abonnements actifs sur des ressources", 'p');
	}

	public function create(FunctionalTester $I)
	{
		$I->amOnPage('/partenaire/1');
		$I->loginAs("d'artagnan");
		$I->amOnPage('/partenaire/create');
		$I->fillField("Nom court", 'Zorglub');
	}

	public function possession(FunctionalTester $I)
	{
		$I->amOnPage('/partenaire/1');
		$I->see("trois mousquetaires", 'h1');
		$I->loginAs('milady');
		$I->amOnPage('/partenaire/possession/1');
		$I->see("Possessions du partenaire", 'h1');
		$I->see("trois mousquetaires", 'h1');
		$I->see("Possessions déjà déclarées", 'h2');
		$I->seeNumberOfElements('#possessions-table tbody > tr', 9);
	}

	public function suivi(FunctionalTester $I)
	{
		$I->loginAs('milady');
		$I->amOnPage('/partenaire/suivi?id=1&target=Revue');
		$I->see("Suivi pour le partenaire", 'h1');
		$I->see("trois mousquetaires", 'h1');
		$I->see("2 suivis déjà déclarés", 'h2');
	}

	public function tableauDeBord(FunctionalTester $I)
	{
		$I->amOnPage('/partenaire/1');
		$I->see("trois mousquetaires", 'h1');
		$I->loginAs('milady');
		$I->amOnPage('/partenaire/tableauDeBord/1');
		$I->see("Liens morts dans mes revues", 'h4');
		$I->see("A contrario", '.revue-444');
		$I->see("02/09/2019", '.revue-444');
		$I->seeElement('.revue-444 a[href^="/revue/444"]');
		$I->dontSeeElement('.revue-444 .label');
	}

	public function update(FunctionalTester $I)
	{
		$I->amOnPage('/partenaire/1');
		$I->loginAs("d'artagnan");
		$I->amOnPage('/partenaire/update/1');
		$I->seeInField("Nom court", "3Mousquetaires");
	}

	public function viewAsGuest(FunctionalTester $I)
	{
		// simulate a cookie for the customization of display
		$_COOKIE['institute'] = 1;

		$I->amOnPage('/');
		$I->see("Vous consultez Mir@bel avec la personnalisation trois mousquetaires", '#institute-warning');
		$I->seeLink("mousquetaires", '/partenaire/1');

		$I->amOnPage('/partenaire/1');
		$I->see("trois mousquetaires", 'h1');
		$I->see("<strong>Alexandre Dumas</strong>", '.detail-view td');
		// auth only
		$I->dontSee("https://possession.example.org/id=IDLOCAL", '.detail-view td');
		$I->dontSee("Utilisateurs associés", 'h2');
		$I->dontSee("Tableau de bord", '#sidebar .nav');
	}

	public function viewAsAuthenticated(FunctionalTester $I)
	{
		$I->loginAs('milady');
		$I->seeElement('.nav > li[title="trois mousquetaires"] > a');
		$I->amOnPage('/partenaire/1');
		$I->see("trois mousquetaires", 'h1');
		// auth only
		$I->see("https://possession.example.org/id=IDLOCAL", '.detail-view td');
		$I->see("Utilisateurs associés", 'h2');
		$I->see("Tableau de bord", '#sidebar .nav');
	}
}
