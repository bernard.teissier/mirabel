<?php

class HomeCest
{
	public function homeAsGuest(FunctionalTester $I)
	{
		$I->amOnPage('/');
		$I->see("Rechercher une revue", 'h2');
		$I->see("Thématiques des revues", 'h2');
		$I->see("actualité", '#bloc-accueil-actu');
		$I->seeLink("Connexion", '/site/login', 'footer');

		$I->click("Connexion", 'footer');
		$I->see("Connexion", 'h1');
	}

	public function homeAsAuthenticated(FunctionalTester $I)
	{
		$I->loginAs("d'artagnan");
		$I->seeCurrentUrlEquals('/');
		$I->see("Rechercher une revue", 'h2');
		$I->see("Thématiques des revues", 'h2');
		$I->dontSeeLink("Connexion", '/site/login', 'footer');

		$I->see("Informations réservées aux partenaires", 'h2');
		$I->see("Modifications suivies", 'h3');
	}
}
