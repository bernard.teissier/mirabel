<?php

class CategorieCest
{
	public function index(FunctionalTester $I)
	{
		$I->amOnPage('/categorie/');
		$I->see("Thématique", 'h1');
		$I->seeElement('#categorie-tree.jstree');
	}

	public function view(FunctionalTester $I)
	{
		$I->amOnPage('/categorie/view/2');
		$I->seeCurrentUrlEquals("/theme/2/Cultures-et-societes");
		$I->see("Cultures et sociétés", 'h1');
		$I->see("Rechercher dans ces revues", 'h2');
	}
}
