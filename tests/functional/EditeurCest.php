<?php

class EditeurCest
{
	private $createdId;

	public function create(FunctionalTester $I)
	{
		$I->amOnPage('/editeur/index');
		$I->click('a[title="Proposer un nouvel éditeur"]');
		$I->see("Nouvel éditeur", 'h1');
		$I->see("Proposer de créer cet éditeur", 'button');
		$I->loginAs('frollo');
		$I->dontSee("Proposer de créer cet éditeur", 'button');
		$I->fillField("Nom", "éditions temporaires");
		$I->fillField("Préfixe", "Les ");
		$I->fillField("IdRef", "https://www.idref.fr/029883091");
		$I->fillField("ID Sherpa", "https://v2.sherpa.ac.uk/id/publisher/7569");

		$I->followRedirects(true);
		$I->click("Créer cet éditeur");
		$I->see("Les éditions temporaires", 'h1');
		$this->createdId = (int) $I->grabFromCurrentUrl('#editeur/(\d+)/editions-temporaires#');
	}

	public function carte(FunctionalTester $I)
	{
		$I->amOnPage('/editeur/carte');
		$I->see("Cette carte ne s'affiche pas si JavaScript est désactivé dans votre navigateur.", '.alert');
	}

	/**
	 * @depends create
	 */
	public function deleteMonitored(FunctionalTester $I)
	{
		$editeurId = 10;
		$editeurName = "Gallimard";

		$I->comment("editeur-partenaire is not authorized to delete monitored publishers");
		$I->loginAs('editeur-98');
		$I->amOnPage("/editeur/$editeurId");
		$I->see($editeurName, 'h1');
		$I->dontSeeLink("Supprimer");
		$I->logout();

		$I->comment("milady is not authorized to delete monitored publishers");
		$I->loginAs('milady');
		$I->amOnPage("/editeur/$editeurId/$editeurName");
		$I->see($editeurName, 'h1');
		$I->dontSeeLink("Supprimer");
		try {
			$I->amOnPage("/editeur/delete/$editeurId");
			$I->seeResponseCodeIs(403);
		} catch (\CHttpException $e) {
		}
		$I->logout();

		$I->comment("frollo can delete monitored publishers (perm suiviEditeurs)");
		$I->loginAs('frollo');
		$I->amOnPage("/editeur/{$this->createdId}");
		$I->see("Les éditions temporaires", 'h1');
		$I->followRedirects(true);
		$I->click("Supprimer", '#sidebar');
		$I->see("supprimé", '.alert-success');
		$I->logout();
	}

	public function deleteUnmonitored(FunctionalTester $I)
	{
		$editeurId = 3370;
		$editeurName = "Société des études robespierristes";

		$I->comment("editeur-partenaire is not authorized to delete monitored publishers");
		$I->loginAs('editeur-98');
		$I->amOnPage("/editeur/$editeurId");
		$I->see($editeurName, 'h1');
		$I->dontSeeLink("Supprimer");
		$I->logout();

		$I->comment("milady is authorized to delete unmonitored publishers");
		$I->loginAs('milady');
		$I->amOnPage("/editeur/$editeurId");
		$I->see($editeurName, 'h1');
		$I->followRedirects(true);
		$I->click("Supprimer", '#sidebar');
		$I->seeCurrentUrlEquals('/editeur/3370/Societe-des-etudes-robespierristes-SER');
		$I->see("impossible", '.alert-error'); // cannot delete when target is used
		$I->logout();
	}

	public function pays(FunctionalTester $I)
	{
		$I->amOnPage('/editeur/pays/FRA/France');
		$I->see("22 éditeurs", '.summary');

		$I->amOnPage('/editeur/pays/DZA/Algerie');
		$I->see("Aucun résultat trouvé.");
	}

	public function search(FunctionalTester $I)
	{
		$I->amOnPage('/editeur/search');
		$I->see("Éditeurs", 'h1');
		$I->dontSeeElement('.grid-view');
		$I->dontSee("Critères", '#search-summary');

		$I->fillField('q[idref]', "!");
		$I->click("Rechercher");
		$I->seeElement('.grid-view');
		$I->click("Suivant");
		$I->see("Résultats de 26 à", '.summary');

		$I->selectOption("Pays", "France");
		$I->click("Rechercher");
		$I->see("[sans idref] [pays : France]", '#search-summary');
		$I->see("Résultats de 1 à", '.summary');
	}

	public function update(FunctionalTester $I)
	{
		$I->amOnPage('/editeur');
		$I->click('Alternatives économiques');
		$I->loginAs('milady');
		$I->click('Modifier');
		$rand = random_int(1, 1000);
		$I->fillField('Description', "Alternatives Economiques s'amuse ($rand) de l'économie comme enjeu collectif et social.");
		$I->fillField('IdRef', "12345678X");

		$I->followRedirects(true);
		$I->click('Enregistrer');
		$I->see("Alternatives Economiques s'amuse ($rand)");
	}

	public function verify(FunctionalTester $I)
	{
		$I->loginAs('frollo');
		$I->amOnPage("/editeur/331");
		$I->see("Alternatives économiques", 'h1');
		$derVerif = $I->grabTextFrom('p.verif');
		$I->see($derVerif, 'p.verif');
		$I->click("Indiquer que l'éditeur a été vérifié");
		$I->see("Alternatives économiques", 'h1');
		$I->dontSee($derVerif, 'p.verif');
	}
}
