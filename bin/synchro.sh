#!/bin/bash

if [[ "$#" -ne 1 ]]; then
	echo "synchro.sh dbname_to_fill"
	exit 1
fi

DBTOFILL="$1"
TODAY="$(date +'%Y-%m-%d')"
DBDUMPSYNC="${HOME}/mirabel2_synchro_${TODAY}.sql.gz"
DBPROD="mirabelv2_prod"

# DB
if [ -t 1 ] ; then
	echo "Sync DB..."
fi
if [[ ! -e "${DBDUMPSYNC}" ]]; then
	{
		mysqldump --single-transaction --quick --skip-lock-tables --skip-add-locks --ignore-table="${DBPROD}.LogExt" "${DBPROD}"
		printf "\n\n-- ### Force some non-production config ###\n"
		echo "UPDATE Config SET value = 'debug' WHERE \`key\` = 'email.host';"
		echo "-- UPDATE Config SET value = 1 WHERE \`key\` = 'maintenance.cron.arret';"
	} | gzip > "${DBDUMPSYNC}"
fi
zcat "${DBDUMPSYNC}" | mysql --show-warnings "$DBTOFILL"

# data
if [ -t 1 ] ; then
	echo "Sync files..."
fi
rsync --quiet -a --delete "${HOME}"/mirabel2-prod/src/www/images/ src/www/images
rsync --quiet -a --delete "${HOME}"/mirabel2-prod/src/www/public/ src/www/public
mkdir -p ./src/www/protected/data/upload/
rsync --quiet -a --delete "${HOME}"/mirabel2-prod/src/www/protected/data/upload/ src/www/protected/data/upload
chgrp -R www-data ./src/www/protected/data/upload/ ./src/www/public/ ./src/www/images/
chmod -R g=u ./src/www/protected/data/upload/ ./src/www/public/ ./src/www/images/
cp non-officielle.png src/www/images/
perl -i.SAVE -pe "s/'dataVersion' => '.*',/'dataVersion' => '${TODAY}',/" ./src/www/protected/config/local.php


# source code
if [ -t 1 ] ; then
	echo "Sync source code..."
fi
sleep 1
git pull --ff-only -q
