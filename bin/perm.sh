#!/bin/sh

cd "${0%/*}"
cd ..

chmod --quiet -R a+rwX src/www/assets src/www/protected/data src/www/protected/runtime

if [ ! -f src/www/protected/config/local.php ] ; then
	cp -i src/www/protected/config/local.dist.php src/www/protected/config/local.php
	echo "Please configure src/www/protected/config/local.php"
fi
