#!/bin/zsh
zparseopts -D -E -- -ids=ids h=help -help=help

if [[ -n $help ]]; then
	echo "./prod.sh [-h|--help] [--ids]"
	exit
fi

echo "Analyse les commits de *HEAD* qui ne sont pas dans *production*.
Vérifier que la branche 'production' est à jour:
	git fetch origin production:production"

local noticket=$(git log --oneline --no-merges --cherry-pick --right-only production...HEAD | grep -v "M#")
local numnoticket=$(echo -n "$noticket" | grep -c '^')
if [[ $numnoticket -gt 0 ]] ; then
	printf "\n\033[39;1m## Commits sans référence à un ticket\033[0m\n"
fi
if [[ $numnoticket -gt 12 ]] ; then
	printf "%d commits\n" $numnoticket
	echo 'git log --color --oneline --no-merges --cherry-pick --right-only production...HEAD | grep -v "M#"'
elif [[ $numnoticket -gt 0 ]] ; then
	echo "$noticket"
fi

printf "\n\033[39;1m## Commits associés à un ticket\033[0m\n"
local withticket=$(git log --oneline --no-merges --cherry-pick --right-only --grep "M#" production...HEAD)
local numwithticket=$(echo -n "$withticket" | grep -c '^')
if [[ $numwithticket -gt 12 ]] ; then
	printf "%d commits\n" $numwithticket
	echo 'git log --oneline --no-merges --cherry-pick --right-only --grep "M#" production...HEAD'
else
	echo "$withticket"
fi

printf "\n\033[39;1m## Tickets ayant des commits pas en production\033[0m\n"
local -a list=($(git log --oneline --no-merges --cherry-pick --right-only production...HEAD | perl -ne 'print "$1\n" if /M#(\d+)/' | sort -n | uniq))
echo "${(pj:  :)list}"
echo "https://tickets.silecs.info/plugin.php?page=TicketList/list&ids="${(pj:,:)list}

printf "\n\033[39;1m## Commande pour fusionner\033[0m\ngit co production && git pull --ff-only && "
if [[ ${#list[@]} -lt 4 ]] ; then
	printf "git merge --log -m 'Fusion. Ferme #%s' master" "${(pj: #:)list}"
else
	printf "git merge --log -m 'Fusion. Ferme %d tickets' -m 'Tickets %s' master" "${#list[@]}" "${(pj: :)list}"
fi
printf " && git log --oneline -n1 && git push && git co -\n"

printf "\n\033[39;1m## Procédure\033[0m
1. Vérifier que les commits sans ticket explicite ne référencent pas implicitement des tickets.
2. Vérifier dans Mantis que les tickets concernés sont terminés (validés ?) ou sûrs.
3. Fermer dans Mantis les tickets vus à l'étape précédente.
4. Fusionner les branches avec la commande git ci-dessus (modifier le commentaire ou la branche).
5. Sauvegarder la base de données de production. (répertoire prod: make dbdump)
6. Déployer (avec 'make push' ou manuellement par ssh).
7. Décrire la montée de version dans https://tickets.silecs.info/view.php?id=1875

"

local sphinxchanges=$(git log --oneline --cherry-pick --right-only production...HEAD -- src/www/protected/command/views)
local numsphinxchanges=$(echo -n "$sphinxchanges" | grep -c '^')
if [[ $numsphinxchanges -gt 0 ]] ; then
	printf "- \033[39;1mReconfigurer Sphinx\033[0m en utilisant ~/sphinx-create-conf.sh (%d modifs) :\n" "$numsphinxchanges"
	echo "$sphinxchanges"
fi

local cronchanges=$(git log --oneline --cherry-pick --right-only --grep='\bcron\b' production...HEAD)
local numcronchanges=$(echo -n "$cronchanges" | grep -c '^')
if [[ $numcronchanges -gt 0 ]] ; then
	printf "- \033[39;1mModifier la crontab\033[0m (%d mentions) :\n" "$numcronchanges"
	echo "$cronchanges"
fi
