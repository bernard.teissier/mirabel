Comment contribuer à Mir@bel ?
==============================

Pour contribuer aux données,
utilisez les formulaires en ligne de [reseau-mirabel.info](https://reseau-mirabel.info).

Pour des questions sur le projet ou des renseignements informels,
vous pouvez [contacter les porteurs du projet](https://reseau-mirabel.info/site/contact).

Pour tout le reste (proposer une modification, demander une fonctionnalité, se plaindre de la documentation, etc),
il vaut mieux d'abord [créer un ticket](https://gitlab.com/reseau-mirabel/mirabel/-/issues).
Les contributions au code seront ensuite bienvenues, sous licence libre Affero GPL.

Ce code source n'a qu'une instance en production, [reseau-mirabel.info](https://reseau-mirabel.info),
et il a vocation à rester ainsi pour partager aux maximum les données.
De ce fait, sa documentation de déploiement et de développement est incomplète.
Nous ferons de notre mieux pour vous aider si vous voulez installer un environnement de développement.


# Contributing to Mir@bel

In order to contribute to Mir@bel's data,
please use the forms in [reseau-mirabel.info](https://reseau-mirabel.info).

If you're interested in the Mira@bel project or if you need more information,
please [contact the project leads](https://reseau-mirabel.info/site/contact).

For anything else, like asking for a new feature, or submitting a Pull|Merge Request,
it's highly recommended to first [create an issue](https://gitlab.com/reseau-mirabel/mirabel/-/issues).
Your contributions (under the free license GNU Affero GPL) are welcome.
