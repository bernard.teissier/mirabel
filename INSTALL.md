Installation de Mir@bel
=======================

* Installer les dépendances avec `composer install`.

* Créer une base de données et un utilisateur MySQL.

* Initialiser la base avec un dump (par ex. celui de `tests/_data/mirabel2_tests.sql.gz`)
  ou le contenu de `src/sql/` (structure puis données).

* Installer SphinxSearch (v2.1+),
  par exemple `apt install sphinxsearch`

* Configurer le fichier `src/www/protected/config/local.php`
  à partir du fichier `local.dist.php` dans ce même répertoire.
	* prod ou debug
	* config MySQL
	* config Sphinx

* Générer le fichier de config de Sphinx avec
  `./yii sphinx conf`
	* La commande `./yii` suppose que `php` est accessible en ligne de commande.
	* S'il y a plusieurs instances sur un même démon searchd,
	  remplacer `conf` par `confLocal` pour chaque instance,
	  puis compléter par un `confCommon`.

* Démarrer sphinx avec cette config

* Exécuter `./yii migrate`.
  Si c'est une nouvelle installation ou que le code a évolué,
  une mise à jour de la base de données sera lancée interactivement.

En cas de problème
------------------

### Configuration de Mir@bel

Dans `/src/www/protected/config/local.php`, modifier les champs du tableau PHP :

- base de données (connectionString, username, password)
- baseUrl : l'URL de la racine du site,
- ldap
- sphinx
- autres paramètres facultatifs (commentés dans le fichier de config).

D'autres réglages (email, etc) se font par la page web de configuration.


### Fonctions PHP manquantes

Vérifier que les extensions nécessaires sont installées.
Avec Debian :
```
sudo apt install -y --no-install-recommends php-cli php-curl php-gd php-intl php-json php-ldap php-mbstring php-mysql php-sqlite3 php-xml php-zip
```

### Permissions de fichiers

Vérifier les permissions d'accès (par ex. `namei -l`) pour les répertoires
accessibles *récursivement* en lecture et en écriture par web et ligne de commande :

- src/www/assets
- src/www/images/ (7 sous-répertoires)
- src/www/protected/data
- src/www/protected/runtime

Concrètement, si l'utilisateur "mirabel" est dans le groupe "www-data" du serveur web :
```
chown -R www-data: src/www/assets src/www/images/ src/www/protected/data src/www/protected/runtime
chmod gu+rwX -R src/www/assets src/www/images/ src/www/protected/data src/www/protected/runtime
```

Annexes
-------

### Initialisation MySQL

Par exemple :
```
echo "
CREATE DATABASE mirabel_devel
GRANT ALL PRIVILEGES ON mirabel_devel.* TO mirabel IDENTIFIED BY 'thepassword';
" | mysql -u root -p
zcat tests/_data/mirabel2_tests.sql.gz | mysql -u mirabel -pthepassword mirabel_devel
```

### Configuration Nginx

Le VirtualHost doit au minimum orienter les requêtes vers `index.php` pour permettre les *pretty URLs*.

D'autres règles sont appliquées à ce niveau pour des raisons de sécurité ou de performance
(contrôle d'accès par IP, interdiction de /protected, cache navigateur, etc).

Exemple de configuration sous Debian avec un socket php-fpm 7.0.
```
server {
	listen 80;
	server_name mirabel.local;
	root /path/to/mirabel/src/www;

	charset utf-8;

	location ~ /(protected|themes) {
		deny all;
	}
	location ~ ^/(assets|css|images|js|public) {
		try_files $uri =404;
	}

	location / {
		try_files $uri /index.php?$args;
	}
	location ~ \.php(/|$) {
		include snippets/fastcgi-php.conf;
		fastcgi_pass unix:/var/run/php/php7.0-fpm.sock;
	}
}
```

### Configuration Apache

Exemple de configuration sous Debian avec un port php-fpm 7.0.
```
<VirtualHost *:80>
	ServerName mirabel.local
	DocumentRoot /path/to/mirabel/src/www

	<FilesMatch \.php$>
		SetHandler "proxy:fcgi://127.0.0.1:9009"
	</FilesMatch>
	
	<Directory "/path/to/mirabel/src/www">
		<IfModule mod_rewrite.c>
			RewriteEngine on
			# if a directory or a file exists, use it directly
			RewriteCond %{REQUEST_FILENAME} -f [OR]
			RewriteCond %{REQUEST_FILENAME} -d
			RewriteRule . - [L]
			# otherwise forward it to index.php
			RewriteRule ^ /index.php [L]
		</IfModule>
	</Directory>

	<Directory "/srv/mirabel2/www-devel/protected">
			Deny from All
	</Directory>
	<DirectoryMatch "^/srv/mirabel2/www-devel/(assets|css|images|js|public)">
			Options -Indexes
			php_admin_flag engine off
	</DirectoryMatch>
</VirtualHost>
```

### Démarrage de Sphinx Search

La version 2.2 est recommandée (fournie par Debian 10).
Les versions postérieures ne sont pas immédiatement compatibles, il faudrait modifier les fichiers de configuration.

Exemple de config systemd pour Sphinx Search
```
# /etc/systemd/system/sphinxsearch-mirabel.service
[Unit]
Description=Sphinx Search (Mirabel)
After=network.target

[Service]
WorkingDirectory=/path/to/mirabel/src/sphinx
ExecStartPre=/usr/bin/indexer --config sphinx.conf --all
ExecStart=/usr/bin/searchd --config sphinx.conf --nodetach
KillMode=process
Restart=always
User=sphinx
Group=sphinx

[Install]
WantedBy=multi-user.target
```

Si d'autres applications utilisent Sphinx sur le même serveur,
il est recommandé de créer un service paramétrable : `sphinxsearch@.service`.
```
# sphinxsearch@.service
# e.g. systemctl start sphinxsearch@mirabel.service

[Unit]
Description=SphinxSearch %I
After=network.target

[Service]
#User=sphinxsearch
#Group=sphinxsearch

# Run ExecStartPre with root-permissions
PermissionsStartOnly=true
ExecStartPre=/bin/mkdir -p /var/lib/sphinxsearch/%i
ExecStartPre=/bin/chown sphinxsearch.sphinxsearch /var/lib/sphinxsearch/%i

WorkingDirectory=/var/lib/sphinxsearch/%i
ExecStart=/usr/bin/searchd --config /etc/sphinxsearch/%i.conf --nodetach
ExecStop=/usr/bin/searchd --config /etc/sphinxsearch/%i.conf --stopwait
KillMode=process

[Install]
WantedBy=multi-user.target
```
