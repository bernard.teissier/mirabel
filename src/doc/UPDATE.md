Mise à jour de l'application
============================

Appliquer des mises à jour SQL
------------------------------

En ligne de commande :

    ./www/protected/yiic migrate

On peut connaître les migrations en attente avec `yiic migrate new`,
et l'historique des migrations appliquées avec `yiic migrate history`.



Créer des mises à jour SQL
--------------------------

En ligne de commande :

    ./www/protected/yiic migrate create nom_du_patch

Puis modifier le nouveau fichier "www/protected/migrations/m120402_102327_nom_du_patch".

