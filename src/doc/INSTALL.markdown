Installation
============

En bref
-------

1. hg clone http://hg.silecs.info/hg/interne/yiipii/
2. cd yiipii
3. ./bin/perm.sh
4. (cd /var/www/ ; sudo ln -s ~/src/yiipii/www yiipii)
5. Connecter le navigateur à http://localhost/yiipii


Installation dans Apache
------------------------

## Permissions de fichiers

Il faut vérifier que les répertoires `assets`, `protected/data` et
`protected/runtime` sont bien accessibles en écriture pour le serveur
web.

	# exemple Debian
	cd www
	chgrp -R www-data assets protected/data protected/runtime
	chmod g=u assets protected/data protected/runtime

Le script "bin/perm.sh" se charge d'ouvrir les droits d'écriture
sur ces répertoires.


## Sécurité

Si possible, le répertoire `yii` ne doit pas être sous le documentroot
(on ne doit pas pouvoir y accéder par un navigateur web).

Si Apache contient un `AllowOverride All`, alors les fichiers `.htaccess`
se chargeront de restreindre les accès. Sinon, ajouter les blocs figurant
dans le fichier d'exemple de configuration Apache ("exemples/vh.conf").


## Configuration Apache

Un simple lien symbolique vers le répertoire de l'application suffit.
On peut aussi utiliser l'exemple "exemples/app.conf".

Pour un **Virtual Host**, voir l'exemple dans "exemples/vh.conf".



Configuration
-------------

Copier le fichier `www/protected/config/local.dist.php` en
`www/protected/config/local.php` et le modifier en suivant les
commentaires, en particulier pour déclarer la base de données.
Le script "bin/perm.sh" effectue cette copie.

Pour les tests unitaires et fonctionnels, il faut modifer
`www/protected/config/test.php` pour utiliser une base de données
différente lors des tests.

A priori, les autres fichiers du répertoire "config" n'ont pas
à être modifiés.

Si le répertoire du code de Yii n'est pas au même niveau que
l'application (la structure attendue est "./yii/" et "./www/index.php"),
alors il faut modifier la constante `YII_PATH` dans le fichier
`yiipath.php`.

