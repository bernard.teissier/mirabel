# Documentation pour dévelopeurs

## Framework Yii

* Tutoriel générique
  <http://www.yiiframework.com/doc/guide/>

* API des classes de Yii
  <http://www.yiiframework.com/doc/api/>

* Validation des modèles (méthode `rules()` de CActiveRecord)
  <http://yii.googlecode.com/files/yii-1.1.0-validator-cheatsheet.pdf>


## Extensions Yii


### yii-bootstrap

**Yiipii** : granche "css-bootstrap" (`hg up css-bootstrap`)

Intégration à Yii du framework CSS Bootstrap (Twitter).

* Démo avec de nombreux exemples (résultat + code source)
  <http://www.cniska.net/yii-bootstrap/>

* Démo/doc officielle de Bootstrap
  <http://twitter.github.com/bootstrap/>

* Dans Gii, ne plus utiliser "CRUD" mais "Bootstrap".

* Pricipales modifs locales :
    * Ajout d'une propriété `$form->hints` pour BootActiveForm.
      C'est un tableau (attrName => hint) qui évite d'avoir à donner le "hint" ligne par ligne.
      On peut aussi utiliser un tableau à 2 niveaux : [ModelClass][attribute].
    * Ajout du paramètre booléen "selfvalues" dans le `$htmlOptions` de `dropDownListRow()` :
      `$form->dropDownListRow($model, $attr, array("Val1", "Val2"), array('selfvalues' => true);`


### yii-debug-toolbar

* <http://www.yiiframework.com/extension/yiidebugtb>

* Principales modifs locales :
    * Ajout d'un panneau "Synthèse".
      On peut y ajouter des éléments spécfiques à l'appli en modifiant
      "src/external/yii-debug-toolbar/yii-debug-toolbar/panels/YiiDebugToolbarPanelSummary.php",
      méthode `getApplicationInfo()`.

* Pour l'AJAX ou les réponses non HTML, la toolbar doit normalement se désactiver toute seule.
  **Attention :** Utiliser `Yii::app()->end()` plutôt que `die()` qui empêche l'enregistrement
                  des logs.


## Permissions et suivi

Utiliser `Yii::app()->user->checkAccess($operation, $params=array())`.

Les valeurs possibles pour "operation" et "params" sont :

| Opération | Paramètre | *Commentaire* |
|-----------|-----------|---------------|
| import    |  |  |
| admin     |  | Seulement pour les utilisateurs administrateurs |
| partenaire | ID du partenaire |  |
| suivi      | objet suivi | de classe Titre, Ressource, Editeur, Service |
