#! /usr/bin/perl -w

use v5.10;
use utf8;                        # utf-8 source code
use strict;
use warnings  qw(FATAL utf8);    # fatalize encoding glitches
use open      qw(:std :utf8);    # undeclared streams in UTF-8
use File::Slurp::Unicode;
use File::Basename;
use DBI;

use Getopt::Long qw(:config bundling);
#use Data::Dumper;


use constant { VERSION => 1 };

# initialize options
my %opts = (
	verbose => 0,
	debug => 0,
);

# Read command-line options
GetOptions(\%opts,
	"man",
	"help|h",
	"verbose|v+",
	"debug|D+",
	"update|u=s",
	"password|p=s",
);
$opts{verbose} += $opts{debug};

# Print help thanks to Pod::Usage
use Pod::Usage;
pod2usage(-verbose => 2) if $opts{man};
pod2usage(-verbose => 0) if $opts{help} or !@ARGV;

# now starts the real app

my %models_used = find_used_hints(glob('src/www/protected/views/*'));

my $db;
my $exists;
if ($opts{update}) {
	$db = DBI->connect(
		"DBI:mysql:" . $opts{update},
		'root', $opts{password},
		{ RaiseError => 1, AutoCommit => 1, }
	) or die ("Erreur MySQL : $DBI::errstr");
	$exists = $db->prepare("SELECT 1 FROM Hint WHERE model=? AND attribute=?");
}

foreach my $file (@ARGV) {
	open my $model, '<', $file
		or die $!;
	my $class = '';
	my %attributes = ();
	while (<$model>) {
		if (/class\s+(\w+)\s+extends/i) {
			$class = $1;
			next;
		}
		if (m/\s*\*\s+\@property\s+(?:string|integer|boolean|int|bool)\s+\$(\w+)/) {
			my $attr = $1;
			next if ($attr eq 'id');
			next if (/timestamp/ or ($attr =~ m/^hdate /));
			$attributes{$attr} = '';
		} elsif (my ($k, $v) = m/^\s*'(.+)'\s*=>\s*'(.+)'\s*,\s*$/) {
			if (exists($attributes{$k}) and !$attributes{$k}) {
				$attributes{$k} = $v;
			}
		}
	}
	if (not exists $models_used{$class}) {
		say "-- $class skipped (", basename($file), ")";
		next;
	}
	$models_used{$class} = 0;
	say "-- $class";
	foreach my $attr (keys %attributes) {
		if (!$attributes{$attr}) {
			warn "$class: empty attribute '$attr'.\n";
		}
		if ($attr eq "statut" && $class ne "Partenaire") {
			next;
		}
		if ($db) {
			$exists->execute($class, $attr);
			if (not $exists->fetchall_arrayref->[0]) {
				printf(
					"\t\"INSERT IGNORE INTO Hint VALUES (NULL, '%s', '%s', '%s', '', 0)\",\n",
					$class, $attr, $attributes{$attr}
				);
			}

		} else {
			printf(
				"INSERT IGNORE INTO Hint VALUES (NULL, '%s', '%s', '%s', '', 0) ;\n",
				$class, $attr, $attributes{$attr}
			);
		}
	}
	close $model;
}

foreach (keys %models_used) {
	say STDERR "Modèle $_ inutilisé" if $models_used{$_};
}

sub find_used_hints {
	my @dirs = @_;
	my %models = ();
	foreach my $dir (@dirs) {
		my @files = glob($dir . '/*.php');
		foreach my $file (@files) {
			next unless -f $file;
			my $content = read_file($file, encoding => 'utf8');
			if ($content =~ m/Hint::model\(\)->getMessages\(['"](\w+)['"]\)/s) {
				$models{$1} = 1;
			}
		}
	}
	return %models;
}

#################################################################


__END__

=encoding utf8

=head1 NAME

fill-hints.pl

=head1 SYNOPSIS

fill-hints.pl <model1.php> [<model2.php>...]

=head1 OPTIONS

=over 8

=item B<-h, --help>

Affiche une page d'aide abrégée.
Print a short help notice.

=item B<--man>

Affiche cette page de man.
Print this man page.

=item B<-v, --verbose>

Incrémente la verbosité.
Increase verbosity.

=back

=cut
