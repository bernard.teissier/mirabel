SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `Partenaire`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `Partenaire` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `nom` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `type` ENUM('normal','import','editeur') CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL DEFAULT 'normal' ,
  `description` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `prefixe` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT 'Article a placer avant le nom' ,
  `url` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `email` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `logoUrl` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT 'Nom du fichier de logo' ,
  `hash` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT 'Identifiant pour webservice' ,
  `ipAutorisees` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL DEFAULT '' COMMENT 'plages IP des membres du partenaire, même non authentifés.' ,
  `possessionUrl` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `hdateCreation` INT(10) UNSIGNED NOT NULL ,
  `hdateModif` INT(10) UNSIGNED NOT NULL ,
  `statut` ENUM('actif','inactif','provisoire','démo') CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 34
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `Cms`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `Cms` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL DEFAULT '' ,
  `singlePage` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '1 if the block defines a new page' ,
  `pageTitle` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL DEFAULT '' ,
  `type` ENUM('text','markdown','html') CHARACTER SET 'ascii' COLLATE 'ascii_bin' NOT NULL DEFAULT 'markdown' ,
  `content` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `partenaireId` INT(10) UNSIGNED NULL DEFAULT NULL ,
  `hdateCreat` INT(10) UNSIGNED NOT NULL DEFAULT '0' ,
  `hdateModif` INT(10) UNSIGNED NOT NULL DEFAULT '0' ,
  PRIMARY KEY (`id`) ,
  INDEX `partenaireId` (`partenaireId` ASC) ,
  CONSTRAINT `Cms_ibfk_1`
    FOREIGN KEY (`partenaireId` )
    REFERENCES `Partenaire` (`id` )
    ON DELETE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 44
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `Ressource`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `Ressource` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `nom` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `prefixe` VARCHAR(25) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `sigle` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `url` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `description` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `type` ENUM('Inconnu','Bouquet','Archive','Sommaires','Biblio','Catalogue','SiteWeb') CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL DEFAULT 'Inconnu' ,
  `acces` ENUM('Inconnu','Libre','Mixte','Restreint') CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL DEFAULT 'Inconnu' ,
  `verrou` ENUM('Inconnu','Tout','Libre','Restreint') CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL DEFAULT 'Inconnu' ,
  `disciplines` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT 'Disciplines couvertes' ,
  `noteContenu` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `diffuseur` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT 'Editeur/Hebergeur/Diffuseur des données' ,
  `partenaires` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `partenairesNb` TINYINT(3) UNSIGNED NOT NULL COMMENT 'Nombre de partenaires' ,
  `revuesNb` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `articlesNb` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `alerteRss` TINYINT(1) NULL DEFAULT NULL ,
  `alerteMail` TINYINT(1) NULL DEFAULT NULL ,
  `exportPossible` TINYINT(1) NULL DEFAULT NULL ,
  `indexation` TINYINT(1) NULL DEFAULT NULL ,
  `autoImport` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'Verrouillage si un import auto est possible' ,
  `partenaire` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'Affiche dans la liste des partenaires de Mirabel' ,
  `hdateModif` INT(10) UNSIGNED NOT NULL ,
  `hdateVerif` INT(10) UNSIGNED NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 532
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin
COMMENT = 'anciennement nommée fournisseur';

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `Collection`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `Collection` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ressourceId` INT(10) UNSIGNED NOT NULL ,
  `nom` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `description` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `url` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `ressourceId` (`ressourceId` ASC) ,
  INDEX `fk_Collection_Ressource1` (`ressourceId` ASC) ,
  CONSTRAINT `fk_Collection_Ressource1`
    FOREIGN KEY (`ressourceId` )
    REFERENCES `Ressource` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `Editeur`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `Editeur` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `nom` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `prefixe` VARCHAR(25) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `sigle` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `description` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `url` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `complement` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT 'Affiché en complément du nom, sert à évituer les doublons.' ,
  `specialite` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `geo` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT 'ville ou pays' ,
  `statut` ENUM('normal','suppr','attente') CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `hdateModif` INT(10) UNSIGNED NOT NULL ,
  `hdateVerif` INT(10) UNSIGNED NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 1192
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `Hint`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `Hint` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `model` VARCHAR(255) NOT NULL ,
  `attribute` VARCHAR(255) NOT NULL ,
  `name` VARCHAR(255) NOT NULL ,
  `description` TEXT NOT NULL ,
  `hdateModif` INT(10) UNSIGNED NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `attr_UNIQUE` (`model` ASC, `attribute` ASC) )
ENGINE = InnoDB
AUTO_INCREMENT = 436
DEFAULT CHARACTER SET = latin1
COMMENT = 'Documentation des champs de formulaires';

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `Revue`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `Revue` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `statut` ENUM('normal','suppr','attente') CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `hdateVerif` INT(10) UNSIGNED NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 1680
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `Titre`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `Titre` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `revueId` INT(10) UNSIGNED NOT NULL ,
  `titre` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `prefixe` VARCHAR(25) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `sigle` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `obsoletePar` INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'titre rendu obsolète par un autre titre de la même revue' ,
  `dateDebut` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT 'ISO, éventuellement incomplète' ,
  `dateFin` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT 'ISO, éventuellement incomplète' ,
  `sudoc` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `worldcat` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `issn` VARCHAR(13) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT 'de la forme 2-253-94276-6' ,
  `issnl` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `url` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `liensJson` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT 'JSON. Tableau assoc {src: url}' ,
  `periodicite` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `electronique` TINYINT(1) NULL DEFAULT NULL ,
  `langues` VARCHAR(40) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `statut` ENUM('normal','suppr','attente') CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `hdateModif` INT(10) UNSIGNED NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `revueId` (`revueId` ASC, `obsoletePar` ASC) ,
  INDEX `fk_Titre_Revue1` (`revueId` ASC) ,
  INDEX `fk_Titre_Titre1` (`obsoletePar` ASC) ,
  CONSTRAINT `fk_Titre_Revue1`
    FOREIGN KEY (`revueId` )
    REFERENCES `Revue` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Titre_Titre1`
    FOREIGN KEY (`obsoletePar` )
    REFERENCES `Titre` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1858
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `Identification`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `Identification` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `titreId` INT(11) UNSIGNED NOT NULL ,
  `ressourceId` INT(10) UNSIGNED NOT NULL ,
  `issne` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `bimpe` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `idInterne` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT 'identifiant utilisé en interne par la resource' ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_Titre_Ressource_Titre1` (`titreId` ASC) ,
  INDEX `fk_Titre_Ressource_Ressource1` (`ressourceId` ASC) ,
  CONSTRAINT `fk_Titre_Ressource_Ressource1`
    FOREIGN KEY (`ressourceId` )
    REFERENCES `Ressource` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Titre_Ressource_Titre1`
    FOREIGN KEY (`titreId` )
    REFERENCES `Titre` (`id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1004
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `Intervention`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `Intervention` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `description` VARCHAR(4096) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL DEFAULT '' ,
  `ressourceId` INT(10) UNSIGNED NULL DEFAULT NULL ,
  `revueId` INT(10) UNSIGNED NULL DEFAULT NULL ,
  `titreId` INT(10) UNSIGNED NULL DEFAULT NULL ,
  `editeurId` INT(10) UNSIGNED NULL DEFAULT NULL ,
  `statut` ENUM('attente','accepté','refusé') CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `contenuJson` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT 'JSON' ,
  `hdateProp` INT(10) UNSIGNED NOT NULL COMMENT 'NULL ssi import par cron' ,
  `hdateVal` INT(10) UNSIGNED NULL DEFAULT NULL ,
  `utilisateurIdProp` INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'NULL ssi suggestion anonyme' ,
  `suivi` TINYINT(1) NOT NULL DEFAULT '0' ,
  `import` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'importation ?' ,
  `ip` VARCHAR(40) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `email` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT 'si utilisateur non identifié par son ID' ,
  `commentaire` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `utilisateurIdVal` INT(10) UNSIGNED NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `ressourceId` (`ressourceId` ASC, `titreId` ASC) ,
  INDEX `utilisateurIdProp` (`utilisateurIdProp` ASC, `utilisateurIdVal` ASC) ,
  INDEX `fk_Intervention_Ressource1` (`ressourceId` ASC) ,
  INDEX `fk_Intervention_Titre1` (`titreId` ASC) ,
  INDEX `fk_Intervention_Editeur1` (`editeurId` ASC) ,
  INDEX `fk_Intervention_Revue1` (`revueId` ASC) ,
  CONSTRAINT `fk_Intervention_Revue1`
    FOREIGN KEY (`revueId` )
    REFERENCES `Revue` (`id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Intervention_Titre1`
    FOREIGN KEY (`titreId` )
    REFERENCES `Titre` (`id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `Intervention_ibfk_1`
    FOREIGN KEY (`ressourceId` )
    REFERENCES `Ressource` (`id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `Intervention_ibfk_2`
    FOREIGN KEY (`editeurId` )
    REFERENCES `Editeur` (`id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2234
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `LogExt`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `LogExt` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `level` VARCHAR(128) NOT NULL ,
  `category` VARCHAR(128) NOT NULL ,
  `logtime` INT(11) NOT NULL ,
  `message` TEXT NOT NULL ,
  `controller` VARCHAR(128) NOT NULL ,
  `action` VARCHAR(128) NOT NULL ,
  `modelId` INT(11) NULL DEFAULT NULL ,
  `ip` VARCHAR(40) CHARACTER SET 'latin1' COLLATE 'latin1_bin' NOT NULL ,
  `userId` INT(11) NULL DEFAULT NULL ,
  `session` VARCHAR(128) CHARACTER SET 'latin1' COLLATE 'latin1_bin' NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 617
DEFAULT CHARACTER SET = latin1;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `Parametre`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `Parametre` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `nom` VARCHAR(50) CHARACTER SET 'ascii' COLLATE 'ascii_bin' NOT NULL ,
  `valeur` TEXT NOT NULL COMMENT 'serialized in JSON' ,
  `type` ENUM('normal','html','serialized') NOT NULL ,
  `hdateModif` INT(10) UNSIGNED NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Les paramètres globaux de l\'application (version, etc.)';

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `Partenaire_Titre`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `Partenaire_Titre` (
  `partenaireId` INT(10) UNSIGNED NOT NULL ,
  `titreId` INT(10) UNSIGNED NOT NULL ,
  `identifiantLocal` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `bouquet` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  PRIMARY KEY (`partenaireId`, `titreId`) ,
  INDEX `fk_Partenaire_Titre_Partenaire1` (`partenaireId` ASC) ,
  INDEX `fk_Partenaire_Titre_Titre1` (`titreId` ASC) ,
  CONSTRAINT `Partenaire_Titre_ibfk_1`
    FOREIGN KEY (`titreId` )
    REFERENCES `Titre` (`id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `Partenaire_Titre_ibfk_2`
    FOREIGN KEY (`partenaireId` )
    REFERENCES `Partenaire` (`id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
COMMENT = 'Un partenaire détient des titres via cette table';

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `Service`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `Service` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ressourceId` INT(10) UNSIGNED NOT NULL ,
  `titreId` INT(10) UNSIGNED NOT NULL ,
  `type` ENUM('Intégral','Résumé','Sommaire','Indexation') CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `acces` ENUM('Libre','Restreint') CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `url` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `alerteRssUrl` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `alerteMailUrl` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `derNumUrl` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT 'url permanente du dernier numéro' ,
  `langues` VARCHAR(40) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT 'codes ISO-636-2 séparés par ,' ,
  `lacunaire` TINYINT(1) NOT NULL COMMENT 'oui si manque des numéros en ligne sur la période considérée' ,
  `selection` TINYINT(1) NOT NULL COMMENT 'oui si tous les articles d\\\'un numéro ne sont pas traités/repris' ,
  `numeroDebut` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `numeroFin` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `dateBarrDebut` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT 'au format JSON' ,
  `dateBarrFin` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT 'au format JSON' ,
  `dateBarrInfo` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT 'précalculé à partir de dateBarrDebut' ,
  `hdateModif` INT(10) UNSIGNED NOT NULL ,
  `statut` ENUM('normal','suppr','attente') CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `ressourceId` (`ressourceId` ASC, `titreId` ASC) ,
  INDEX `fk_Service_Ressource` (`ressourceId` ASC) ,
  INDEX `fk_Service_Titre1` (`titreId` ASC) ,
  CONSTRAINT `Service_ibfk_1`
    FOREIGN KEY (`ressourceId` )
    REFERENCES `Ressource` (`id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `Service_ibfk_2`
    FOREIGN KEY (`titreId` )
    REFERENCES `Titre` (`id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 6390
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
COMMENT = 'Accès en ligne';

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `Suivi`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `Suivi` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `partenaireId` INT(10) UNSIGNED NOT NULL ,
  `cible` ENUM('Revue','Ressource','Editeur') NOT NULL COMMENT 'Table de l\\\'objet suivi' ,
  `cibleId` INT(10) UNSIGNED NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_Droits_Partenaire1` (`partenaireId` ASC) ,
  CONSTRAINT `Suivi_ibfk_1`
    FOREIGN KEY (`partenaireId` )
    REFERENCES `Partenaire` (`id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 340
DEFAULT CHARACTER SET = latin1;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `Titre_Collection`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `Titre_Collection` (
  `titreId` INT(11) UNSIGNED NOT NULL ,
  `collectionId` INT(10) UNSIGNED NOT NULL ,
  PRIMARY KEY (`titreId`, `collectionId`) ,
  INDEX `fk_Titre_Collection_Titre1` (`titreId` ASC) ,
  INDEX `fk_Titre_Collection_Collection1` (`collectionId` ASC) ,
  CONSTRAINT `fk_Titre_Collection_Collection1`
    FOREIGN KEY (`collectionId` )
    REFERENCES `Collection` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Titre_Collection_Titre1`
    FOREIGN KEY (`titreId` )
    REFERENCES `Titre` (`id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `Titre_Editeur`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `Titre_Editeur` (
  `titreId` INT(11) UNSIGNED NOT NULL ,
  `editeurId` INT(10) UNSIGNED NOT NULL ,
  PRIMARY KEY (`titreId`, `editeurId`) ,
  INDEX `fk_Titre_Editeur_Titre1` (`titreId` ASC) ,
  INDEX `fk_Titre_Editeur_Editeur1` (`editeurId` ASC) ,
  CONSTRAINT `fk_Titre_Editeur_Editeur1`
    FOREIGN KEY (`editeurId` )
    REFERENCES `Editeur` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Titre_Editeur_Titre1`
    FOREIGN KEY (`titreId` )
    REFERENCES `Titre` (`id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `Utilisateur`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `Utilisateur` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `partenaireId` INT(10) UNSIGNED NOT NULL ,
  `login` VARCHAR(50) CHARACTER SET 'ascii' COLLATE 'ascii_bin' NOT NULL ,
  `email` VARCHAR(100) CHARACTER SET 'ascii' COLLATE 'ascii_bin' NOT NULL DEFAULT '' ,
  `nom` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL DEFAULT '' ,
  `prenom` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL DEFAULT '' ,
  `nomComplet` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL DEFAULT '' ,
  `actif` TINYINT(1) NOT NULL DEFAULT '1' ,
  `derConnexion` INT(10) UNSIGNED NULL DEFAULT NULL ,
  `hdateCreation` INT(10) UNSIGNED NOT NULL ,
  `hdateModif` INT(10) UNSIGNED NOT NULL ,
  `permAdmin` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'Permission globale d\\\'administrateur' ,
  `permImport` TINYINT(1) NOT NULL DEFAULT '0' ,
  `permPartenaire` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'Gestion des attributs et des pages du partenaire' ,
  `permIndexation` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'Thèmes' ,
  `permRedaction` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'Rédaction des blocs CMS' ,
  `suiviEditeurs` TINYINT(1) NOT NULL DEFAULT '0' ,
  `suiviNonSuivi` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'Peut modifier tous les objets non suivis, et suit leurs changements' ,
  `motdepasse` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL DEFAULT '' COMMENT 'Si Partenaire provisoire ou démo' ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `login` (`login` ASC) ,
  INDEX `fk_Utilisateur_Partenaire1` (`partenaireId` ASC) ,
  CONSTRAINT `Utilisateur_ibfk_1`
    FOREIGN KEY (`partenaireId` )
    REFERENCES `Partenaire` (`id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 41
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `tbl_migration`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `tbl_migration` (
  `version` VARCHAR(255) NOT NULL ,
  `apply_time` INT(11) NULL DEFAULT NULL ,
  PRIMARY KEY (`version`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

SHOW WARNINGS;

INSERT INTO `tbl_migration` VALUES ('m000000_000000_base',1349858614);
INSERT INTO `tbl_migration` VALUES ('m120829_131013_remove_id_column_in_relations',1349858616);
INSERT INTO `tbl_migration` VALUES ('m120914_151642_add_utilisateur_motdepasse',1349858616);
INSERT INTO `tbl_migration` VALUES ('m120918_170754_service_no_unknown_values',1349858616);
INSERT INTO `tbl_migration` VALUES ('m120919_183118_intervention_cascade',1349858616);
INSERT INTO `tbl_migration` VALUES ('m120921_112925_intervention_description',1349858616);
INSERT INTO `tbl_migration` VALUES ('m120922_190641_utilisateur_info',1349858616);
INSERT INTO `tbl_migration` VALUES ('m120924_113122_intervention_description_longue',1349858616);
INSERT INTO `tbl_migration` VALUES ('m120924_120912_identification_unicity_T_R',1349858616);
INSERT INTO `tbl_migration` VALUES ('m121004_112354_cms',1349858616);
INSERT INTO `tbl_migration` VALUES ('m121008_211603_cms_pageTitle',1349858616);
INSERT INTO `tbl_migration` VALUES ('m121010_073432_ressource_partenaire',1349858616);
INSERT INTO `tbl_migration` VALUES ('m121012_160548_cms_accueil',1350058485);
INSERT INTO `tbl_migration` VALUES ('m121016_075423_cms_accueil_connecte',1350374319);
INSERT INTO `tbl_migration` VALUES ('m121024_165343_fk_update',1350374319);
INSERT INTO `tbl_migration` VALUES ('m121106_161009_cms_add_page_partenaires',1352218424);
INSERT INTO `tbl_migration` VALUES ('m121123_103205_update_hint_searchdates',1353667227);
INSERT INTO `tbl_migration` VALUES ('m121129_081842_service_import',1354177389);


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
