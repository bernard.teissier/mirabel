TRUNCATE TABLE Cms ;

INSERT INTO `Cms` SET
`id` = '1',
`name` = 'accueil',
`type` = 2,
`content` = '# Voici un titre de niveau 1\n\n## Puis un titre de niveau 2\n\nDu texte en *Markdown*, avec `différentes` **mises en formes** et un [lien vers la doc](http://www.yiiframework.com/site/markdownHelp/).\n\nSecond paragraphe.\n\n<div class=\"alert alert-success\">Formatage HTML au milieu du format Wiki-Markdown</div>\n\nPuis un tableau :\n| col1 | col2 | aligné *à droite* |\n|------|------|--:|\n| *A*  |  B   | 0 |\n|A2|A3| 1 |\n\n## Quelques variables\n\n* Nombre de revues : %NB_REVUES%\n* Nombre de partenaires : %NB_PARTENAIRES%\n* Nombre d\'éditeurs : %NB_EDITEURS%\n* Trois dernières modifs : %DER_MAJ_3%\n',
`partenaireId` = NULL ;

-- exemple / partenaire
INSERT INTO `Cms` (`name`, `type`, `content`, `partenaireId`)
VALUES ('partenaire', 2, 'Un petit laïus sur ce partenaire qui s\'affichera sur sa page, sous le titre.', '1') ;

INSERT INTO `Cms` (`name`, `singlePage`, `type`, `content`, `partenaireId`)
VALUES ('contact', 0, 2, '', NULL) ;

-- pages-blocs
INSERT INTO `Cms` (`name`, `singlePage`, `type`, `content`, `partenaireId`)
VALUES
('présentation', 1, 2, '', NULL)
,('partenariat', 1, 2, '', NULL)
,('aide', 1, 2, '', NULL)
,('aide professionnelle', 1, 2, '', NULL)
,('actualité', 1, 2, '', NULL)
,('pilotage', 1, 2, '', NULL)
,('documentation', 1, 2, '', NULL)
;
