SET NAMES utf8;

CREATE TABLE IF NOT EXISTS `tbl_migration` (
	  `version` varchar(255) NOT NULL,
	  `apply_time` int(11) DEFAULT NULL,
	  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `tbl_migration` (`version`, `apply_time`) VALUES
('m000000_000000_base',	1347984353),
('m120829_131013_remove_id_column_in_relations',	1347984355),
('m120914_151642_add_utilisateur_motdepasse',	1347984355);

ALTER TABLE `Titre_Editeur` DROP `id` ;
ALTER TABLE `Titre_Editeur` ADD PRIMARY KEY (`titreId`, `editeurId`), DROP INDEX `index4` ;
ALTER TABLE `Titre_Collection` DROP `id` ;
ALTER TABLE `Titre_Collection` DROP INDEX `index4` ;

ALTER TABLE `Utilisateur`
ADD `motdepasse` varchar(255) COLLATE 'utf8_bin' NOT NULL DEFAULT ''
  COMMENT 'Si Partenaire provisoire ou démo' AFTER `suiviNonSuivi` ;
