<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:atom="http://www.w3.org/2005/Atom" version="1.0">
  <xsl:output method="html" indent="yes"/>
  <xsl:template match="/atom:feed">
    <html>
      <head>
        <title>
          <xsl:value-of select="atom:title"/>
        </title>
        <style type="text/css">
			dd { margin-top: 2ex; font-weight: bold; }
			a { text-decoration: none; }
			.item { float: left; width: 100ex; margin: 2ex; border: 1px solid #CCCCCC; padding: 3px 5px; border-radius: 1em; }
			#channel-description { margin-left: 1em; background-color: #F0FFF8; }
			.item .title { padding: 3px 5px; margin-top: 0.5ex; margin-bottom: 0; }
			.item .content { padding-top: 1ex; }
			#footer { clear: both; }
        </style>
      </head>
      <body>
        <h1>Mir@bel - Flux de nouveautés</h1>
        <h2>
          <xsl:element name="a">
            <xsl:attribute name="href">
              <xsl:value-of select="atom:link/@href"/>
            </xsl:attribute>
            <xsl:value-of select="atom:title"/>
          </xsl:element>
        </h2>
        <div id="channel">
          <div id="channel-description">
            <xsl:value-of select="atom:subtitle"/>
          </div>
          <div class="items">
            <xsl:for-each select="atom:entry">
              <div class="item">
                <h3 class="title">
                  <xsl:element name="a">
                    <xsl:attribute name="href">
                      <xsl:value-of select="atom:link/@href"/>
                    </xsl:attribute>
                    <xsl:value-of select="atom:title"/>
                  </xsl:element>
                </h3>
                <div class="content">
                  <xsl:value-of select="atom:summary" disable-output-escaping="yes"/>
                  <div class="pubdate">
                    <xsl:value-of select="atom:published"/>
                  </div>
                </div>
              </div>
            </xsl:for-each>
          </div>
        </div>
        <div id="footer">
          <xsl:value-of select="atom:author/atom:name"/>
        </div>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
