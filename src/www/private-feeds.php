<?php

require_once dirname(dirname(__DIR__)) . '/vendor/autoload.php';
spl_autoload_register(array('YiiBase','autoload'));

require __DIR__ . '/protected/components/FeedGenerator.php';

$type = 'atom';
if (isset($_GET['type'])) {
	$type = $_GET['type'];
}

$gen = new FeedGenerator($type);
$gen->privateFeed = true;
if (isset($_GET['valide'])) {
	$gen->showValidatedOnly((boolean) $_GET['valide']);
} else {
	$gen->showValidatedOnly(null);
}
if (isset($_GET['editeurs'])) {
	$gen->addCriteria('editeurId', "IS NOT NULL");
	$gen->feedTitle[] = 'Éditeurs';
}
foreach (array('revue', 'ressource', 'editeur') as $o) {
	if (!empty($_GET[$o])) {
		$gen->addCriteria($o . 'Id', (int) $_GET[$o]);
	}
}
if (!empty($_GET['partenaire'])) {
	$gen->addFilterMonitored((int) $_GET['partenaire']);
}
$gen->run();
