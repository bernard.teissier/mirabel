
/* global L */

function drawWorldMap (mapConfig, stamen){

var worldmap = L.map('world-map', { zoomControl: false }).setView([35, 5], 2.0);
new L.Control.Zoom({ position: 'bottomleft' }).addTo(worldmap);
var layer;
if (stamen) {
	layer = new L.StamenTileLayer(stamen);
} else {
	layer = L.tileLayer(
		'//{s}.tile.osm.org/{z}/{x}/{y}.png',
		{
			minZoom: 2,
			maxZoom: 11,
			dragging: false,
			attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
		}
	);
}
layer.addTo(worldmap);

if ('countries' in mapConfig && mapConfig.countries) {
	var computeRadius;
	if (mapConfig.radius.method === 'linear') {
		computeRadius = function(x) {
			return 2 + Math.round(mapConfig.radius.max * x / mapConfig.countriesMax);
		};
	} else if (mapConfig.radius.method === 'logarithmic') {
		computeRadius = function(x) {
			return 2 + Math.round(mapConfig.radius.max * Math.log(x) / Math.log(mapConfig.countriesMax));
		};
	} else if (mapConfig.radius.method === 'quadratic') {
		computeRadius = function(x) {
			return 2 + Math.round(mapConfig.radius.max * Math.sqrt(x) / Math.sqrt(mapConfig.countriesMax));
		};
	} else {
		computeRadius = function(x) {
			return mapConfig.radius.max;
		};
	}

	var circles = L.layerGroup();
	for (var i = 0; i < countries.length; i++) {
		var id = countries[i].id;
		if (id in mapConfig.countries) {
			var radius = computeRadius(mapConfig.countries[id][0]);
			console.log(id + " " + mapConfig.countries[id][0] + " / " + mapConfig. countriesMax + " -> " + radius);
			circles.addLayer(
				L.circleMarker(countries[i].coord, {
					color: '#44f',
					fillColor: '#44f',
					fillOpacity: 0.6,
					radius: radius
				}).bindPopup(mapConfig.popupMessage(mapConfig.countries[id]))
			);
		}
	}
	circles.addTo(worldmap);
}
if ('markers' in mapConfig && mapConfig.markers.length > 0) {
	var mirabelIcon = L.icon({
		iconUrl: '/images/marker-mirabel.png',
		iconRetinaUrl: '/images/marker-mirabel-2x.png',
		iconSize:     [25, 41],
		iconAnchor:   [12, 41],
		shadowUrl: '/images/marker-mirabel-shadow.png',
		shadowSize:   [41, 41],
		// shadowAnchor: [4, 62],  // the same for the shadow
		popupAnchor:  [1, -34] // point from which the popup should open relative to the iconAnchor
	});
	for (var i = 0; i < mapConfig.markers.length; i++) {
		var options = {};
		if (mapConfig.markers[i].length > 3 && mapConfig.markers[i][3] === 'normal') {
			options = {icon: mirabelIcon};
		}
		L.marker([mapConfig.markers[i][0], mapConfig.markers[i][1]], options)
			.bindPopup(mapConfig.markers[i][2])
			.addTo(worldmap);
	}
}
}
