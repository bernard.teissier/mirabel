
(function(){
	// Hmmm, IE is a pain until MSIE 11 overrules its ancestors
	function addEvent(element, type, handler) {
		if (element.attachEvent) {
			element.attachEvent("on" + type, handler);
		} else if (element.addEventListener) {
			element.addEventListener(type, handler, false);
		}
	}

	var form = document.getElementById('form-footer-institute');
	var sel = document.getElementById('sel-institute');
	if (form) {
		form.setAttribute('style', 'display: block;');
	}
	addEvent(sel, 'change', function(e){
		document.cookie = "institute=set-" + this.value + ';path=/';
		window.location.reload(true);
	});

	window.exportTableToCSV = function($table, filename) {
		var $rows = $table.find('tr'),

		colDelim = ';',
		rowDelim = '\r\n',

		// Grab text from table into CSV formatted string
		csv = $rows.map(function (i, row) {
				var cells = $(row).find('td, th');
				return cells.map(function (j, col) {
					var content = $(col).text();
					if (content) {
						return '"' + $(col).text().replace(/"/g, '""') + '"'; // escape double quotes
					} else {
						return "";
					}
				}).get().join(colDelim);
			}).get().join(rowDelim);

		// Data URI
		var csvData = 'data:text/csv;charset=UTF-8,' + encodeURIComponent(csv);

		$(this).attr({
			'download': filename,
			'href': csvData,
			'target':'_blank'
		});
	};

    $('table.exportable').before('<div class="pull-right"><a href="#" title="Exporter en CSV" class="table-export">CSV</a></div>');
    $('a.table-export').on('click', function (event) {
        exportTableToCSV.apply(this, [$(event.target).closest('div').next('table.exportable'), 'export.csv']);
    });

	$(".jstree").on("hover_node.jstree", function(e, data) {
		var li = data.instance.get_node(data.node.id, true);
		if (typeof data.node.data.description !== 'undefined' && data.node.data.description !== '') {
			$(li).children('a').attr('title', data.node.data.description.replace(/<br \/>/g, ''));
		} else {
			$(li).children('a').removeAttr('title');
		}
		return false;
	});

	$("#jstree-search button").on('click', function(e) {
		e.preventDefault();

		var searchStr = $("#jstree-search-q").val();
		if (searchStr) {
			$("#categorie-tree").jstree("close_all");
			// custom search function (using title and description of each node
			var searchFunction = new $.vakata.search(searchStr, true, { caseSensitive : false, fuzzy : false });
			$("#categorie-tree").jstree(true).settings.search.search_callback = function (str, node) {
				return searchFunction.search(node.text).isMatch || searchFunction.search(node.data.description).isMatch;
			};
			// search
			$("#categorie-tree").jstree(true).search(searchStr);
		} else {
			$("#categorie-tree").jstree(true).clear_search();
		}
	});

	$(".collapse-toggle")
		.on("click", function() {
			if ($(this).hasClass('toggled')) {
				$('i.icon-zoom-out', this).attr('class', 'icon-zoom-in');
			} else {
				$('i.icon-zoom-in', this).attr('class', 'icon-zoom-out');
			}
			$(this).toggleClass("toggled");
			var targetSelector = $(this).attr("data-collapse-target");
			if (targetSelector) {
				$(targetSelector + ".collapse-target").toggle();
			} else {
				$(this).siblings(".collapse-target").first().toggle();
			}
		})
		.append(' <i class="icon-zoom-in" title="Déplier les infos complémentaires"></i>');
	$(".collapse-toggle-show")
		.on("click", function() {
			var targetSelector = $(this).attr("data-collapse-target");
			if (targetSelector) {
				var target = $(targetSelector + ".collapse-target");
				if (target.is(':visible')) {
					return;
				}
				target.toggle();
				$('.collapse-toggle[data-collapse-target="' + targetSelector + '"]:not(.toggled)')
					.addClass("toggled")
					.find('i.icon-zoom-in').attr('class', 'icon-zoom-out');
			} else {
				$(this).siblings(".collapse-target").first().toggle();
			}
		});

	if (location.pathname.match(/update|create/)) {
		window.setTimeout(function() {
			warnOnUnfinishedForm(document.querySelector('form[method=POST]'));
		}, 	1000);
	}

	function warnOnUnfinishedForm(postForm) {
		if (!postForm) {
			return;
		}
		console.log("beforeunload: started");
		var initialContent = $(postForm).serialize();
		var isFormSubmitted = false;
		window.addEventListener("submit", function (e) {
			isFormSubmitted = true;
		});
		window.addEventListener("beforeunload", function (e) {
			if (isFormSubmitted) {
				return "";
			}
			var actualContent = $(postForm).serialize();
			if (actualContent === initialContent) {
				return "";
			}
			var confirmationMessage = "Un formulaire de cette page a été (partiellement) rempli et va être abandonné. Confirmez-vous que vous souhaitez quitter cette page ?";
			e.returnValue = confirmationMessage;     // Gecko, Trident, Chrome 34+
			return confirmationMessage;              // Gecko, WebKit, Chrome <34
		});
	}
})();

function htmlEscape(str) {
	return String(str)
		.replace(/&/g, '&amp;')
		.replace(/"/g, '&quot;')
		.replace(/</g, '&lt;')
		.replace(/>/g, '&gt;');
}

function padLeft(size, content) {
	var s = content.toString();
	var realWidth = s.length;
	if (realWidth < size) {
		return Array(size + 1 - realWidth).join(" ") + s;
	} else {
		return s;
	}
}
