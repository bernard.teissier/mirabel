<?php

class CategorieAliasController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'users' => ['@'],
			],
			['deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * Deletes a particular model.
	 *
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 *
	 * @param int $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		if (Yii::app()->request->isPostRequest) {
			$success = $model->delete();

			if (!isset($_GET['ajax'])) {
				if ($success) {
					Yii::app()->user->setFlash(
						'success',
						"Alias « " . CHtml::encode($model->alias) . " » supprimé."
					);
				} else {
					Yii::app()->user->setFlash(
						'error',
						"Alias « " . CHtml::encode($model->alias) . " » NON-supprimé : " . print_r($model->getErrors(), true)
					);
				}
				$this->redirect(['/vocabulaire/update', 'id' => $model->vocabulaireId]);
			}
		} else {
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 *
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param int $id the ID of the model to be loaded
	 * @return CategorieAlias
	 */
	public function loadModel($id)
	{
		$model = CategorieAlias::model()->findByPk((int) $id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $model;
	}
}
