<?php

class RessourceController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'actions' => ['ajaxComplete', 'create', 'index', 'update', 'view'],
				'users' => ['*'],
			],
			[
				'allow',
				'actions' => ['logo'],
				'roles' => ['ressource/logo'],
			],
			[
				'allow',
				'actions' => ['abonnements', 'delete', 'verify'],
				'users' => ['@'],
			],
			[
				'deny',  // otherwise, deny all users
				'users' => ['*'],
			],
		];
	}

	public function actionAbonnements($id)
	{
		$model = $this->loadModel($id);
		if ($model->hasCollections()) {
			$cibles = $model->getCollectionsDiffuseur();
		} else {
			$cibles = [$model];
		}

		$this->render(
			'abonnements',
			[
				'model' => $model,
				'cibles' => $cibles,
			]
		);
	}

	/**
	 * Displays a particular model.
	 *
	 * @param int $id the ID of the model to be displayed
	 * @param string $nom
	 */
	public function actionView($id, $nom = '')
	{
		$model = $this->loadModel($id);
		$encName = Norm::urlParam($model->nom);
		if (!$nom || $nom !== $encName) {
			$this->redirect(['view', 'id' => $id, 'nom' => $encName]);
		}
		$this->appendToHtmlHead(
			Controller::linkEmbeddedFeed('rss2', "Ressource " . $model->nom, ['ressource' => $model->id])
		);
		$abonnements = null;
		if (!empty(Yii::app()->user->institute)) {
			$abonnements = new AbonnementSearch();
			$abonnements->partenaireId = Yii::app()->user->institute;
		}
		$partenaireId = Yii::app()->user->getState('partenaireId');

		if (Yii::app()->session->contains('force-refresh')) {
			Yii::app()->session->remove('force-refresh');
			$forceRefresh = true;
		} else {
			$forceRefresh = false;
		}

		$this->render(
			'view',
			[
				'model' => $model,
				'searchNavigation' => $this->loadSearchNavigation(),
				'abonnements' => $abonnements,
				'partenaireCourant' => ($partenaireId ? Partenaire::model()->findByPk($partenaireId) : null),
				'forceRefresh' => $forceRefresh,
			]
		);
	}

	/**
	 * Creates a new model.
	 *
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new Ressource;
		$directAccess = Yii::app()->user->checkAccess('ressource/create-direct');

		if (isset($_POST['Ressource'])) {
			$i = $model->buildIntervention($directAccess);
			if (isset($_POST['Intervention']['email'])) {
				$i->attributes = $_POST['Intervention'];
			}
			$model->attributes = $_POST['Ressource'];
			if ($model->validate()) {
				$i->description = "Création de la ressource « {$model->nom} »";
				$i->action = 'ressource-C';
				$i->contenuJson->create($model);
				if ($this->validateIntervention($i, $directAccess)) {
					if ($directAccess) {
						$i->ressourceId = $i->contenuJson->lastInsertId['Ressource'];
					}
					$i->save(false);
					if ($directAccess) {
						$this->redirect(['view', 'id' => $i->ressourceId]);
					} else {
						$this->redirect(['index']);
					}
				}
			}
		}
		if (!isset($i)) {
			$i = new Intervention;
		}

		$this->render(
			'create',
			['model' => $model, 'direct' => $directAccess, 'intervention' => $i]
		);
	}

	/**
	 * Updates a particular model.
	 *
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param int $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		$directAccess = Yii::app()->user->checkAccess('ressource/update-direct', ['Ressource' => $model])
			&& empty($_POST['force_proposition']);
		$i = $model->buildIntervention($directAccess);

		if (isset($_POST['Ressource'])) {
			$old = clone $model;
			$model->attributes = $_POST['Ressource'];
			if (isset($_POST['Intervention']['email'])) {
				$i->attributes = $_POST['Intervention'];
			}
			$changedAttributes = array_keys(array_diff_assoc($model->getAttributes(), $old->getAttributes()));
			if ($model->validate($changedAttributes)) {
				$i->description = "Modification de la ressource « {$model->nom} »";
				$i->contenuJson->update($old, $model);
				unset($old);
				$this->validateIntervention($i, $directAccess, ['view', 'id' => $model->id]);
			}
		}

		$this->render(
			'update',
			['model' => $model, 'direct' => $directAccess, 'intervention' => $i]
		);
	}

	/**
	 * Deletes a particular model.
	 *
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param int $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$ressource = $this->loadModel($id);

		$hasIntervention = Yii::app()->db
			->createCommand("SELECT 1 FROM Intervention WHERE ressourceId = ? AND statut = 'attente'")
			->queryScalar([(int) $id]);
		if ($hasIntervention) {
			Yii::app()->user->setFlash('error', "Suppression impossible car au moins une intervention est en attente sur cette ressource.");
			$this->redirect(['view', 'id' => $ressource->id]);
		}

		if (Yii::app()->request->isPostRequest) {
			list($deletable, $deletableMsg) = $ressource->isDeletable();
			if (!$deletable && !Yii::app()->user->checkAccess('ressource/delete-force')) {
				Yii::app()->user->setFlash('error', $deletableMsg);
				$this->redirect(['view', 'id' => $ressource->id]);
			}
			if (!empty($_POST['confirm'])) {
				foreach ($ressource->services as $s) {
					$si = $s->buildIntervention(true);
					$si->action = 'service-D';
					$si->description = "Suppression d'un accès en ligne, revue « {$s->titre->titre} »"
						. " / « {$ressource->nom} »";
					$si->contenuJson->delete($s, "Suppression de l'accès en ligne");
					$si->accept();
				}
				$i = $ressource->buildIntervention(true);
				$i->description = "Suppression de la ressource « {$ressource->nom} »";
				$i->action = 'ressource-D';
				foreach ($ressource->collections as $c) {
					$i->contenuJson->delete($c, "Suppression de la collection « " . $c->nom . " »");
				}
				foreach ($ressource->identifications as $ident) {
					$i->contenuJson->delete($ident, "Suppression de l'identification entre « " . $ressource->nom . " » et « " . $ident->titre->titre . " »");
				}
				$i->contenuJson->delete($ressource, "Suppression de la ressource « " . $ressource->nom . " »");
				if ($i->validate()) {
					require_once Yii::app()->getBasePath() . '/components/FeedGenerator.php';
					FeedGenerator::saveFeedsToFile('Ressource', (int) $this->id);
				}
				if (!$i->accept(false)) {
					Yii::app()->user->setFlash('error', print_r($i->errors, true));
				} else {
					$i->ressourceId = null;
					if (!$i->save(false)) {
						throw new CHttpException(500, "Erreur : " . print_r($i->errors, true));
					}
					Yii::app()->user->setFlash('success', "La ressource a été supprimée.");
					$this->redirect($_POST['returnUrl'] ?? ['index']);
				}
			}
			$this->render(
				'delete',
				['model' => $ressource, 'deletable' => $deletable, 'msg' => $deletableMsg, ]
			);
		} else {
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}
	}

	/**
	 * Lists all models.
	 *
	 * @param string $lettre
	 * @param int $suivi Partenaire.id
	 * @param bool $noweb
	 */
	public function actionIndex($lettre = 'A', $suivi = null, $import = '', $noweb = false)
	{
		$dataProvider = Ressource::listAlpha($lettre, $suivi, (string) $import, (bool) $noweb);
		try {
			$data = $dataProvider->getData();
		} catch (\Exception $_) {
			throw new CHttpException(400, "Erreur dans la recherche de ressources. Navigation au-delà de la taille des résultats ?");
		}
		$searchCache = new SearchNavigation();
		$searchHash = $searchCache->save(
			'ressource/index',
			['lettre' => $lettre, 'suivi' => $suivi, 'import' => (string) $import, 'noweb' => (bool) $noweb],
			'SearchRessource',
			$data
		);
		if ($suivi || $import) {
			$lettre = '';
		} elseif (ctype_digit($lettre)) {
			$lettre = "#0-9";
		} else {
			$lettre = strtoupper($lettre);
		}
		$this->render(
			'index',
			[
				'dataProvider' => $dataProvider,
				'suivi' => (int) $suivi,
				'import' => (string) $import,
				'lettre' => $lettre,
				'searchHash' => $searchHash,
				'noweb' => $noweb,
			]
		);
	}

	public function actionLogo($id)
	{
		$model = $this->loadModel((int) $id);

		$imageTool = new ImageDownloader(
			Yii::getPathOfAlias('webroot') . '/images/ressources-logos/h55',
			Yii::getPathOfAlias('webroot') . '/images/ressources-logos'
		);
		$imageTool->filePattern = '%03d';
		$imageTool->boundingbox = '225x55';

		$upload = new Upload(new UploadDestination([
			'name' => 'Ressource - Logo',
			'path' => dirname(Yii::app()->basePath) . '/images/ressources-logos',
			'url' => Yii::app()->getBaseUrl(true) . '/images/ressources-logos/',
		]));
		$upload->overwrite = true;
		$newImage = false;
		$filename = $imageTool->getRawImageFileName($model->id);

		// remove the logo
		if (!empty($_POST['delete'])) {
			$imageTool->delete($model->id);
			Yii::app()->user->setFlash('success', "Le logo de la ressource a été mis à jour.");
			Yii::app()->session->add('force-refresh', true);
			$this->redirect(['ressource/view', 'id' => $model->id]);
		}

		// logo by URL
		if (!empty($_POST['Ressource'])) {
			$model->logoUrl = $_POST['Ressource']['logoUrl'];
			if ($model->save(false) && $model->logoUrl) {
				if ($imageTool->download($model->id, $model->logoUrl)) {
					$newImage = true;
				}
			}
		}

		// logo by file upload
		if (!empty($_POST['Upload'])) {
			$upload->attributes = $_POST['Upload'];
			$upload->file = CUploadedFile::getInstance($upload, 'file');
			if ($upload->file) {
				if ($upload->file->saveAs($filename)) {
					system("convert \"$filename\" \"$filename.png\"");
					unlink($filename);
					$newImage = true;
					// M#3422 remove the logoUrl when a upload superseeds it
					$model->logoUrl = '';
					$model->save(false);
				} else {
					Yii::app()->user->setFlash('error', "Erreur lors de la copie de ce fichier sur le serveur.");
				}
			} else {
				Yii::app()->user->setFlash('error', "Erreur lors du dépôt de ce fichier.");
			}
		}

		if ($newImage) {
			if ($imageTool->resizeByIdentifier($model->id)) {
				Yii::app()->user->setFlash('success', "Le logo de la ressource a été mis à jour.");
				Yii::app()->session->add('force-refresh', true);
				$this->redirect(['ressource/view', 'id' => $model->id]);
			} else {
				Yii::app()->user->setFlash('error', "Erreur lors du redimensionnement de l'image.");
			}
		}

		$this->render(
			'logo',
			['model' => $model, 'upload' => $upload]
		);
	}

	/**
	 * Marks as verified.
	 *
	 * @param int $id the ID of the model to be marked.
	 */
	public function actionVerify($id)
	{
		$model = $this->loadModel((int) $id);
		if (!Yii::app()->user->checkAccess('verify', $model)) {
			throw new CHttpException(403, "Vous n'avez pas les droits nécessaires pour cette opération.");
		}
		$model->hdateVerif = $_SERVER['REQUEST_TIME'];
		if ($model->save()) {
			Yii::app()->user->setFlash('success', "La date de vérification a été mise à jour.");
		}
		$this->redirect(['view', 'id' => $model->id]);
	}

	/**
	 * Propose completions for a term (AJAX).
	 */
	public function actionAjaxComplete($term, $noimport = 0)
	{
		$this->header('json');
		echo json_encode(Ressource::completeTerm((string) $term, [], (bool) $noimport));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 *
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param int $id the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model = Ressource::model()->findByPk((int) $id);
		if ($model === null) {
			throw new CHttpException(404, "La ressource demandée n'existe pas.");
		}
		return $model;
	}
}
