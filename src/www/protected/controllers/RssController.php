<?php

class RssController extends Controller
{
	public $defaultAction = 'breves';

	public function actionBreves()
	{
		$feed = $this->initFeed();
		$feed->title = Yii::app()->name . ": brèves";
		$feed->description = "Dernières informations sur le réseau Mi@bel. \n" . $feed->description;
		$blocks = Cms::model()->findAllBySql(
			"SELECT * FROM Cms WHERE name = 'brève' ORDER BY hdateCreat DESC LIMIT " . (int) Config::read('rss.limite.breves')
		);
		foreach ($blocks as $block) {
			/* @var $block Cms */
			$item = $feed->add('item');
			$item->id = $this->createAbsoluteUrl('/site') . "#actu" . $block->id;
			if (preg_match('/\|([^\|\n]+)\|\s*\n/', $block->content, $matches)
				|| preg_match('/^\s*#+\s*(\S.+)\s*\n/', $block->content, $matches)) {
				$item->title = htmlspecialchars(strip_tags($matches[1]));
			} else {
				$item->title = "Brève";
			}
			$item->published = $block->hdateCreat;
			$item->updated = $block->hdateModif;

			$description = $item->add('description');
			$description->type = 'html';
			$description->text = $block->toHtml(true);

			$link = $item->add('link');
			$link->href = $this->createAbsoluteUrl('/site/page', ['p' => 'actualite'])
				. "#actu" . $block->id;
			$link->rel = "alternate";
		}
		$processingInstructions = [
			[
				'xml-stylesheet',
				'type="text/xsl" href="' . Yii::app()->baseUrl . '/xslt/atom.xsl"',
			],
		];
		$xml = $feed->generate('atom', $processingInstructions);
		//header('Content-Type: ' . $feed->getContentType() . '; charset=utf-8'); // prevent Chrome from applying XSLT
		header('Content-Type: text/xml; charset=utf-8');
		echo $xml;
	}

	public function actionRevues()
	{
		$feed = $this->initFeed();
		$feed->title = Yii::app()->name . ": dernières revues ajoutées";
		$feed->description = "Dernières revues créées dans Mir@bel pour signaler les accès en ligne. \n" . $feed->description;
		$titres = Titre::model()->findAllBySql(
			"SELECT t.*, i.hdateVal AS confirm FROM Intervention i JOIN Titre t ON i.titreId = t.id "
			. " WHERE i.action = 'revue-C' "
			. " ORDER BY hdateVal DESC LIMIT " . (int) Config::read('rss.limite.revues')
		);
		foreach ($titres as $titre) {
			/* @var $titre Titre */
			$item = $feed->add('item');
			$item->id = $this->createAbsoluteUrl('/revue/view', ['id' => $titre->revueId]);
			$item->title = htmlspecialchars($titre->getFullTitle());
			$item->published = $titre->confirm;
			$item->updated = $titre->confirm;

			$description = $item->add('description');
			$description->type = 'html';
			$description->text = $this->renderPartial('revues', ['titre' => $titre], true);

			$link = $item->add('link');
			$link->href = $this->createAbsoluteUrl('/revue/view', ['id' => $titre->revueId]);
			$link->rel = "alternate";
		}
		$processingInstructions = [
			[
				'xml-stylesheet',
				'type="text/xsl" href="' . Yii::app()->baseUrl . '/xslt/atom.xsl"',
			],
		];
		$xml = $feed->generate('atom', $processingInstructions);
		//header('Content-Type: ' . $feed->getContentType() . '; charset=utf-8'); // prevent Chrome from applying XSLT
		header('Content-Type: text/xml; charset=utf-8');
		echo $xml;
	}

	private function initFeed()
	{
		$url = $this->createAbsoluteUrl('/rss/' . $this->getAction()->id);
		$feed = new ezcFeed();
		$feed->description = "Mir@bel est un réservoir d'informations qui, pour chaque revue recensée, "
			. "indique où trouver en ligne le texte intégral des articles, "
			. "les sommaires des numéros, les résumés des articles et les références bibliographiques.";
		$feed->published = time();
		$feed->updated = time();
		$feed->id = htmlspecialchars($url);
		$feed->language = 'fr';

		$author = $feed->add('author');
		/* @var $author ezcFeedPersonElement */
		$author->name = Yii::app()->name;
		//$author->email = ;

		$link = $feed->add('link');
		/* @var $author ezcFeedLinkElement */
		$link->href = $url;
		$link->rel = 'self';
		return $feed;
	}
}
