<?php

class InterventionController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'users' => ['@'],
			],
			[
				'deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * Displays a particular model.
	 *
	 * @param int $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = $this->loadModel($id);
		$this->render(
			'view',
			[
				'model' => $model,
				'canValidate' => Yii::app()->user->checkAccess('validate', $model),
			]
		);
	}

	/**
	 * Accepts and applies.
	 *
	 * @param int $id the ID of the model.
	 */
	public function actionAccept($id)
	{
		$model = $this->loadModel($id);
		if (!Yii::app()->user->checkAccess('validate', $model)) {
			throw new CHttpException(403, "Vous n'avez pas la permission de valider cette intervention.");
		}

		try {
			models\sherpa\Hook::enrichIntervention($model);
			$success = $model->accept();
		} catch (Exception $e) {
			if (Yii::app()->request->isAjaxRequest) {
				Yii::app()->end();
			}
			Yii::app()->user->setFlash(
				'error',
				"<p>L'intervention n'a pas pu être appliquée parce qu'une erreur s'est déclenchée,
					probablement à cause d'un conflit de données : </p>
				<code><pre>{$e->getMessage()}</pre></code>
				<p>Vous devez donc rejeter l'intervention et intervenir manuellement
					pour intégrer les modifications proposées que vous souhaitez prendre en compte.</p>"
			);
			$this->redirect(['view', 'id' => $model->id]);
			return;
		}

		require_once __DIR__ . '/TitreController.php';
		if ($model->titreId && $model->contenuJson->hasUrlCoverChange()) {
			TitreController::updateCouverture($model->titre);
		}

		if ($success) {
			if (Yii::app()->request->isAjaxRequest) {
				Yii::app()->end();
			} else {
				Yii::app()->user->setFlash('success', "Intervention acceptée et appliquée.");
				$this->redirect(['view', 'id' => $model->id]);
			}
		} else {
			Yii::app()->user->setFlash(
				'error',
				"<p>L'intervention n'a pas pu être acceptée :</p>"
				. "<ul><li>" . join("</li><li>", $model->render()->errors()) . "</ul>"
				. "<p>Vous devez donc rejeter l'intervention et intervenir manuellement pour intégrer les modifications proposées que vous souhaitez prendre en compte.</p>"
			);
			$this->redirect(['view', 'id' => $model->id]);
		}
	}

	/**
	 * Rejects.
	 *
	 * @param int $id the ID of the model.
	 */
	public function actionReject($id)
	{
		$model = $this->loadModel($id);
		if (!Yii::app()->user->checkAccess('validate', $model)) {
			throw new CHttpException(403, "Vous n'avez pas la permission de valider sur cette intervention.");
		}

		if ($model->reject()) {
			if (Yii::app()->request->isAjaxRequest) {
				Yii::app()->end();
			} else {
				Yii::app()->user->setFlash('success', "Intervention rejetée.");
				$this->redirect(['view', 'id' => $model->id]);
			}
		} else {
			Yii::app()->user->setFlash(
				'error',
				"L'intervention n'a pas pu être refusée. " . join(" ; ", $model->render()->errors())
			);
			$this->redirect(['view', 'id' => $model->id]);
		}
	}

	/**
	 * Reverts.
	 *
	 * @param int $id the ID of the model.
	 */
	public function actionRevert($id)
	{
		$model = $this->loadModel($id);
		if (!Yii::app()->user->checkAccess('validate', $model)) {
			throw new CHttpException(403, "Vous n'avez pas la permission de valider sur cette intervention.");
		}

		try {
			$success = $model->revert();
		} catch (Exception $e) {
			if (Yii::app()->request->isAjaxRequest) {
				Yii::app()->end();
			}
			Yii::app()->user->setFlash(
				'error',
				"L'intervention n'a pas pu être annulée, probablement parce que les données ont été modifiées : " . $e->getMessage()
			);
			$this->redirect(['view', 'id' => $model->id]);
			return;
		}
		if ($success) {
			if (Yii::app()->request->isAjaxRequest) {
				Yii::app()->end();
			} else {
				Yii::app()->user->setFlash('success', "Intervention annulée.");
				$this->redirect(['view', 'id' => $model->id]);
			}
		} else {
			Yii::app()->user->setFlash(
				'error',
				"L'intervention n'a pas pu être annulée. " . join(" ; ", $model->render()->errors())
			);
			$this->redirect(['view', 'id' => $model->id]);
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->layout = '//layouts/column1';
		$model = new InterventionSearch('search');
		$model->unsetAttributes();  // clear any default values
		if (Yii::app()->user->hasState('InterventionSuivi')) {
			$model->suivi = Yii::app()->user->getState('InterventionSuivi');
		}
		if (isset($_GET['q'])) {
			$model->setAttributes($_GET['q']);
			Yii::app()->user->setState('InterventionSuivi', $model->suivi);
			$model->validate();
		} else {
			$model->statut = 'attente';
		}

		$this->render(
			'admin',
			['model' => $model]
		);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 *
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param int $id the ID of the model to be loaded
	 * @return Intervention
	 */
	public function loadModel($id)
	{
		$model = Intervention::model()->findByPk((int) $id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $model;
	}
}
