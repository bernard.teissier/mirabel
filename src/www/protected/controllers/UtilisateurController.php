<?php

use models\forms\Login;
use models\forms\PasswordMailForm;

class UtilisateurController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	private $model;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'actions' => ['resetPassword'],
				'users' => ['*'],
			],
			[
				'allow',
				'actions' => ['messagePassword', 'suggestLogin', 'update', 'view'],
				'users' => ['@'],
			],
			[
				'allow',
				'roles' => ['utilisateur/admin'],
			],
			['deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	public function init()
	{
		parent::init();
		$isAdmin = Yii::app()->user->checkAccess('utilisateur/admin');
		if (isset($_GET['id']) && $_GET['id'] > 0) {
			$id = (int) $_GET['id'];
			$model = $this->loadModel($id);
		} else {
			$model = null;
		}
		$this->breadcrumbs = $this->createBreadrumbs($model, $isAdmin);
		$this->menu = $this->createMenu($model, $isAdmin);
	}

	/**
	 * Displays a particular model.
	 *
	 * @param int $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = $this->loadModel($id);

		if (!Yii::app()->user->checkAccess('utilisateur/view', $model)) {
			throw new CHttpException(403, "Vous ne pouvez consulter que les comptes du même institut.");
		}
		$this->render(
			'view',
			['model' => $model]
		);
	}

	/**
	 * Creates a new model.
	 *
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($partenaireId = 0)
	{
		$model = new Utilisateur('insert-web');
		if ($partenaireId) {
			$model->partenaireId = (int) $partenaireId;
			if (!Yii::app()->user->checkAccess('utilisateur/create', ['Partenaire' => $model->partenaire])) {
				throw new CHttpException(403, "Vous ne pouvez créer de compte pour ce partenaire.");
			}
		} else {
			if (!Yii::app()->user->checkAccess('utilisateur/create')) {
				throw new CHttpException(403, "Vous ne pouvez pas créer de compte.");
			}
		}
		if (empty($model->partenaireId) || $model->partenaire->type === 'normal') {
			$model->listeDiffusion = 1;
		}

		if (isset($_POST['Utilisateur'])) {
			$model->attributes = $_POST['Utilisateur'];
			if ($model->save()) {
				if (!$model->authLdap && empty($model->motdepasse)) {
					Yii::app()->user->setFlash(
						'success',
						"<p>Le nouvel utilisateur a été créé.</p>
<p>Son champ <em>mot de passe</em> étant vide, il ne peut pas se connecter pour le moment.
Mir@bel propose de lui attribuer un mot de passe aléatoire qui lui sera envoyé par courriel.
</p>"
					);
					$this->redirect(['messagePassword', 'id' => $model->id]);
				} else {
					Yii::app()->user->setFlash('success', "Le nouvel utilisateur a été créé.");
					$this->redirect(['view', 'id' => $model->id]);
				}
			}
		}

		$this->render(
			'create',
			['model' => $model]
		);
	}

	/**
	 * Display a form to edit the email that will be sent. Then set a new password and send the email.
	 *
	 * @param int $id
	 */
	public function actionMessagePassword($id)
	{
		$model = $this->loadModel($id);
		if (!Yii::app()->user->checkAccess('partenaire/utilisateurs', ['Partenaire' => $model->partenaire])) {
			throw new CHttpException(403, "Accès interdit.");
		}
		if ($model->authLdap) {
			throw new CHttpException(403, "Un utilisateur authentifié par LDAP ne peut changer de mot de passe.");
		}
		if (!$model->email) {
			Yii::app()->user->setFlash('error', "Cet utilisateur n'a pas d'adresse électronique à laquelle lui envoyer un message.");
			$this->redirect(['view', 'id' => $model->id]);
		}

		$formData = new PasswordMailForm();
		if ($model->partenaire->type === "editeur") {
			$formData->body = Config::read('password.mailOnCreate.EditeurPartenaire.body');
			$formData->subject = Config::read('password.mailOnCreate.EditeurPartenaire.subject');
		} else {
			$formData->body = Config::read('password.mailOnCreate.body');
			$formData->subject = Config::read('password.mailOnCreate.subject');
		}

		if (isset($_POST['form'])) { // 'form' from Controller::init()
			$formData->attributes = $_POST['form'];
			$uncryptedPassword = Utilisateur::generateRandomPassword();
			$model->motdepasse = $uncryptedPassword;
			if ($formData->validate() && $model->save(false)) {
				$message = Mailer::newMail()
					->setSubject($formData->subject)
					->setFrom(Config::read('email.from'))
					->setTo([$model->email])
					->setBody($formData->fillBody($model, $uncryptedPassword));
				$replyTo = Config::read('email.replyTo');
				if ($replyTo) {
					$message->setReplyTo($replyTo);
				}
				if (Mailer::sendMail($message)) {
					Yii::app()->user->setFlash('success', "Un courriel a été envoyé avec le nouveau mot de passe.");
				} else {
					Yii::log("L'envoi du mail d'inscription (mdp) a échoué.", 'error', 'email');
					Yii::app()->user->setFlash('error', "L'envoi par mail n'a pas fonctionné. L'erreur a été signalée aux administrateurs.");
				}
				$this->redirect(['view', 'id' => $model->id]);
			}
		}

		$this->render(
			'messagePassword',
			['model' => $model, 'formData' => $formData]
		);
	}

	/**
	 * Updates a particular model.
	 *
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param int $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		if (!Yii::app()->user->checkAccess('utilisateur/view', $model)) {
			throw new CHttpException(403, "Vous n'avez pas la permission de modifier ce compte.");
		}
		if (!Yii::app()->user->checkAccess('utilisateur/admin')) {
			if (Yii::app()->user->id == $model->id) {
				$model->scenario = 'updateown';
			}
		} else {
			$model->scenario = 'update-web';
		}

		if (isset($_POST['Utilisateur'])) {
			$model->setAttributes($_POST['Utilisateur']);
			if ($model->save()) {
				Yii::app()->user->setFlash('success', "Le compte de l'utilisateur a été modifié.");
				$this->redirect(['view', 'id' => $model->id]);
			}
		}

		$this->render(
			'update',
			['model' => $model]
		);
	}

	/**
	 * Disables a particular model.
	 *
	 * @param int $id the ID of the model to be disabled
	 */
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			// we only allow disabling via POST request
			$user = $this->loadModel($id);
			$user->actif = false;
			$user->save(false);

			// if AJAX request (triggered via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax'])) {
				Yii::app()->user->setFlash('success', "L'utilisateur " . CHtml::encode($user->nom) . " a été désactivé.<br />"
					. "Il peut être réactivé en modifiant son profil.");
				$this->redirect(
					$_POST['returnUrl'] ?? ['view', 'id' => $user->id]
				);
			}
		} else {
			throw new CHttpException(400, 'Requête invalide. Il est possible que JavaScript soit désactivé dans votre navigateur. Activez-le et recommencez.');
		}
	}

	/**
	 * Deletes totally a particular model.
	 *
	 * @param int $id the ID of the model to be disabled
	 */
	public function actionDeleteTotally($id)
	{
		if (Yii::app()->request->isPostRequest) {
			// we only allow disabling via POST request
			$user = $this->loadModel($id);
			$interv = Intervention::model()->findByAttributes(['utilisateurIdVal' => $id]);
			if (!$interv) {
				$interv = Intervention::model()->findByAttributes(['utilisateurIdProp' => $id]);
			}
			if ($interv) {
				Yii::app()->user->setFlash(
					'error',
					"L'utilisateur ne peut être supprimé, il est déjà intervenu au moins pour " . $interv->getSelfLink()
					. ".<br />" . (
						$user->actif ?
						"Vous pouvez par contre désactiver cet utilisateur."
						: "Par contre cet utilisateur est bien désactivé. Il n'a donc plus accès à Mir@bel et n'est plus abonné à la liste de diffusion des partenaires."
					)
				);
				$this->redirect($_POST['returnUrl'] ?? ['admin']);
			}
			try {
				$user->delete();
			} catch (Exception $e) {
				Yii::app()->user->setFlash(
					'error',
					"L'utilisateur n'a pu être supprimé, il est probablement utilisé dans les données : " . CHtml::encode($e->getMessage())
				);
				$this->redirect($_POST['returnUrl'] ?? ['admin']);
			}
			Yii::app()->user->setFlash('success', "L'utilisateur " . CHtml::encode($user->nom) . " a été supprimé définitivement.");
			$this->redirect($_POST['returnUrl'] ?? ['admin']);
		} else {
			throw new CHttpException(400, 'Requête invalide. Il est possible que JavaScript soit désactivé dans votre navigateur. Activez-le et recommencez.');
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model = new UtilisateurSearch();
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['UtilisateurSearch'])) {
			$model->setAttributes($_GET['UtilisateurSearch']);
		}

		$this->render(
			'admin',
			['model' => $model]
		);
	}

	/**
	 * Reset the password of an user identified by login or email.
	 */
	public function actionResetPassword()
	{
		$model = new Login();
		$user = null;
		$misconfigured = !Mailer::checkConfig();
		$notfound = false;

		if (isset($_POST['LoginForm'])) {
			$model->attributes = $_POST['LoginForm'];
			if ($model->validate(['username'])) {
				$user = Utilisateur::findByName($model->username);
			}
			if (!$user || $user->authLdap || !$user->email) {
				$notfound = true;
			}
		}
		if ($user && !$misconfigured && !$notfound) {
			$password = Utilisateur::generateRandomPassword();
			$user->motdepasse = $password;
			if ($user->save()) {
				//Yii::log("utilisateur/resetPassword: {$user->id} mdp = $password", "info");
				if ($this->mailPassword($user, $password)) {
					Yii::app()->user->setFlash('success', "Un nouveau mot de passe vous a été envoyé par mail.");
					$this->redirect(['site/login']);
				} else {
					Yii::log("utilisateur/resetPassword: sending mail failed.", "error");
					Yii::app()->user->setFlash('error', "L'envoi par mail n'a pas fonctionné. L'erreur a été signalée aux administrateurs.");
				}
			} else {
				Yii::log("utilisateur/resetPassword: could NOT save new password '$password'", "error");
				Yii::app()->user->setFlash('error', "Un nouveau mot de passe n'a pu être généré. L'erreur a été signalée aux administrateurs.");
			}
		}
		$this->render(
			'reset-password',
			[
				'model' => $model,
				'misconfigured' => $misconfigured,
				'notfound' => $notfound,
			]
		);
	}

	/**
	 * Service that returns a login (that does not exist yet).
	 *
	 * @param string $nom
	 * @param string $prenom
	 */
	public function actionSuggestLogin($nom, $prenom)
	{
		if (Yii::app()->request->isAjaxRequest) {
			$this->header('json');
			$cleanupName = function ($x) {
				$rm = [" " => "-", "'" => "-"];
				return strtolower(str_replace(array_keys($rm), array_values($rm), iconv("UTF-8", "ASCII//TRANSLIT", $x)));
			};
			$nom = $cleanupName($nom);
			$prenom = $cleanupName($prenom);
			foreach (range(1, strlen($prenom)) as $take) {
				$login = "m_" . $nom . "_" . substr($prenom, 0, $take);
				if (!Utilisateur::model()->findByAttributes(['login' => $login])) {
					echo CJSON::encode($login);
					return;
				}
			}
			echo '""';
		} else {
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 *
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param int $id the ID of the model to be loaded
	 * @return Utilisateur
	 */
	public function loadModel($id)
	{
		if (!$this->model) {
			$this->model = Utilisateur::model()->findByPk((int) $id);
		}
		if ($this->model === null) {
			throw new CHttpException(404, "Le compte utilisateur demandé n'existe pas.");
		}
		return $this->model;
	}

	/**
	 * Send an e-mail with the new password.
	 *
	 * @param Utilisateur $user
	 * @param string $clearPassword
	 * @return bool Success?
	 */
	protected function mailPassword(Utilisateur $user, $clearPassword)
	{
		$message = Mailer::newMail()
			->setSubject(Config::read('password.mailOnReset.subject'))
			->setFrom(Config::read('email.from'))
			->setTo([$user->email])
			->setBody(
				str_replace(
					['%LOGIN%', '%PASSWORD%', '%URL%'],
					[$user->login, $clearPassword, Yii::app()->getBaseUrl(true)],
					Config::read('password.mailOnReset.body')
				)
			);
		$replyTo = Config::read('email.replyTo');
		if ($replyTo) {
			$message->setReplyTo($replyTo);
		}
		return Mailer::sendMail($message);
	}

	/**
	 * @param ?Utilisateur $model
	 * @param bool $isAdmin
	 * @return array
	 */
	private function createBreadrumbs($model, $isAdmin)
	{
		$breadcrumbs = ($isAdmin ? ['Utilisateurs' => ['admin']] : null);
		if ($model) {
			$breadcrumbs['Partenaire ' . $model->partenaire->nom] = ['/partenaire/view', 'id' => $model->partenaireId];
			$breadcrumbs[$model->login] = ['view', 'id' => $model->id];
		}
		return $breadcrumbs;
	}

	/**
	 * @param ?Utilisateur $model
	 * @param bool $isAdmin
	 * @return array
	 */
	private function createMenu($model, $isAdmin)
	{
		$menu = [
			[
				'label' => 'Administration des utilisateurs',
				'url' => ['admin'],
				'visible' => $isAdmin,
			],
			[
				'label' => 'Créer un utilisateur',
				'url' => ['create'],
				'visible' => $isAdmin,
			],
			[
				'label' => 'Créer un utilisateur',
				'url' => ['create', 'partenaireId' => Yii::app()->user->partenaireId],
				'visible' => !$isAdmin && Yii::app()->user->checkAccess('partenaire/utilisateurs'),
			],
		];
		if ($model) {
			$isMe = ($model->id == Yii::app()->user->id);
			$isUpdateAllowed = Yii::app()->user->checkAccess('utilisateur/update', ['Utilisateur' => $model]);
			array_push(
				$menu,
				[
					'label' => ($isMe ? 'Mon profil' : 'Cet utilisateur'),
					'itemOptions' => ['class' => 'nav-header']
				],
				[
					'label' => 'Consulter',
					'url' => ['view', 'id' => $model->id]
				],
				[
					'label' => 'Modifier',
					'url' => ['update', 'id' => $model->id],
					'itemOptions' => ['title' => ($isMe ? "Modifier mon adresse électronique ou mon mot de passe" : "Modifier ce compte")],
					'visible' => $isUpdateAllowed,
				],
				[
					'label' => 'Envoyer un mot de passe',
					'url' => ['messagePassword', 'id' => $model->id],
					'visible' => $isUpdateAllowed && !$isMe
				],
				[
					'label' => 'Désactiver', 'url' => '#',
					'linkOptions' => [
						'submit' => ['delete', 'id' => $model->id],
						'confirm' => 'Êtes vous certain de vouloir désactiver cet utilisateur ?',
					],
					'visible' => $isUpdateAllowed,
				],
				[
					'label' => 'Supprimer', 'url' => '#',
					'linkOptions' => [
						'submit' => ['deleteTotally', 'id' => $model->id],
						'confirm' => 'Êtes vous certain de vouloir supprimer DÉFINITIVEMENT cet utilisateur ?',
					],
					'visible' => $isAdmin,
				]
			);
		}
		return $menu;
	}
}
