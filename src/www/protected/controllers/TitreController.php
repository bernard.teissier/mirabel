<?php

use models\sherpa\Hook;

class TitreController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
			[
				'COutputCache + publication',
				'duration' => YII_DEBUG ? 5 : 3600,
				'varyByParam' => ['id'],
				'varyBySession' => false,
			],
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'actions' => ['update', 'ajaxComplete', 'ajaxIssn', 'ajaxSudoc', 'create', 'createByIssn', 'export', 'publication'],
				'users' => ['*'],
			],
			[
				'allow',
				'actions' => ['couverture', 'delete', 'editeurs', 'identification', 'view'],
				'users' => ['@'],
			],
			[
				'allow',
				'actions' => ['changeJournal'],
				'roles' => ['titre/admin'],
			],
			[
				'deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * Displays a particular model.
	 *
	 * @param int $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = $this->loadModel($id);
		if (!Yii::app()->user->isGuest) {
			$this->layout = '//layouts/column2';
		}
		$lastUpdate = Intervention::model()->findBySql(
			"SELECT id, hdateProp, hdateVal FROM Intervention WHERE titreId = {$model->id} AND statut = 'accepté' "
			. " AND ressourceId IS NULL AND (`action` = 'revue-C' OR `action` IS NULL)"
			. " ORDER BY hdateVal DESC LIMIT 1"
		);
		$this->render(
			'view',
			['model' => $model, 'lastUpdate' => $lastUpdate, 'autresTitres' => $model->revue->getTitres()]
		);
	}

	/**
	 * @param int $id the ID of the model to be updated
	 */
	public function actionChangeJournal($id)
	{
		$model = $this->loadModel($id);

		if (isset($_POST['revueId'])) {
			$revueId = (int) $_POST['revueId'];
			if ($revueId === 0) {
				if (Titre::model()->countByAttributes(['revueId' => $model->revueId]) == 1) {
					Yii::app()->user->setFlash('error', "Impossible de déplacer un titre fils unique vers une revue vide.");
					return $this->redirect(['view', 'id' => $model->id]);
				}
				$revue = new Revue();
				$revue->statut = 'normal';
				$revue->save();
				$revueId = $revue->id;
			} else {
				$revue = Revue::model()->findByPk($revueId);
			}
			$i = $model->buildIntervention(true);
			$new = clone $model;
			$new->revueId = $revue->id;
			if ($revue) {
				$previousTitle = Titre::model()->findByAttributes(['revueId' => $model->revueId, 'obsoletePar' => $model->id]);
				if ($previousTitle) {
					$previousTitle->updateByPk($previousTitle->id, ['obsoletePar' => $model->obsoletePar]);
				}

				$otherDestTitles = $revue->titres;
				$oldestTitle = end($otherDestTitles);
				if ($oldestTitle) {
					$new->obsoletePar = $oldestTitle->id;
				} else {
					$new->obsoletePar = null;
				}
				$i->description = "Déplacement du titre « {$new->titre} »";
				$i->contenuJson->update($model, $new);
				$i->revueId = $revue->id;
				if (Titre::model()->countByAttributes(['revueId' => $model->revueId]) < 2) {
					$i->contenuJson->delete($model->revue);
				}
				$i->contenuJson->confirm = true;
				if ($i->accept()) {
					unset($model);
					$this->redirect(['/revue/view', 'id' => $new->revueId]);
					return;
				}
				Yii::app()->user->setFlash('error', "Erreur en enregistrant l'intervention : " . print_r($i->getErrors(), true));
			} else {
				Yii::app()->user->setFlash('error', "La revue d'ID {$new->revueId} n'a pas été trouvée.");
			}
		}

		$this->render(
			'changeJournal',
			[
				'model' => $model,
			]
		);
	}

	public function actionCreateByIssn($revueId = 0)
	{
		$model = new IssnForm();
		if ($revueId) {
			$model->revueId = (int) $revueId;
			$revue = Revue::model()->findByPk($revueId);
		} else {
			$revue = null;
		}

		if (isset($_POST['IssnForm'])) {
			$model->setAttributes($_POST['IssnForm']);
			if ($model->validate()) {
				try {
					Yii::app()->user->setState('createByIssn', $model->getSudocNotices());
					$url = ['create'];
					if ($model->revueId) {
						$url['revueId'] = $model->revueId;
					}
					return $this->redirect($url);
				} catch (Exception $e) {
					$model->addError('issn', "Erreur en interrogeant le Sudoc : « {$e->getMessage()} ». Recommencer plus tard, ou créer sans ISSN.");
				}
			}
		}

		$this->render('create-by-issn', ['model' => $model, 'revue' => $revue]);
	}

	/**
	 * Creates a new model.
	 *
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($revueId = 0)
	{
		$model = new Titre;
		$issns = $model->issns;
		//$model->suivre = !Yii::app()->user->isGuest;

		// if coming from createByIssn, load the ISSN data.
		if (Yii::app()->user->hasState('createByIssn')) {
			$sudocNotices = Yii::app()->user->getState('createByIssn');
			/* @var $sudocNotices \models\sudoc\Notice[] */
			Yii::app()->user->setState('createByIssn', null);

			$model->fillBySudocNotices($sudocNotices);
			foreach ($sudocNotices as $k => $sudocNotice) {
				$issn = new Issn();
				$issn->fillBySudocNotice($sudocNotice);
				$issns["new$k"] = $issn;
			}
			unset($k);
			unset($issn);

			Yii::app()->user->setFlash(
				'info',
				"Ce nouveau titre est pré-rempli avec les données de l'ISSN."
				. " Complétez, et vérifiez notamment si les dates du titre coïncident réellement avec celles de son ISSN."
			);
		}

		if (!empty($_POST['Titre']['revueId'])) {
			$revueId = (int) $_POST['Titre']['revueId'];
		}
		if ($revueId) {
			$revueId = (int) $revueId;
			$model->revueId = $revueId;
			if (empty($model->revue)) {
				throw new CHttpException(500, "Cette revue id={$model->revueId} n'existe pas.");
			}
		} else {
			$revueId = null;
		}
		$directAccess = Yii::app()->user->checkAccess('titre/create-direct', ['revueId' => (int) $revueId]);
		$forceProposition = false;
		if ($directAccess) {
			if (isset($_POST['force_proposition'])) {
				$forceProposition = (bool) $_POST['force_proposition'];
			} elseif (!Suivi::checkDirectAccessByIds($revueId, null)) {
				$forceProposition = true;
			}
		}
		if ($directAccess && $forceProposition) {
			$directAccess = false;
		}

		$editeurNew = $this->parseNewEditorLinks();
		if ($model->load($_POST)) {
			$issnChanges = Issn::loadMultiple($issns, $_POST);
			$i = $model->buildIntervention($directAccess);
			if (isset($_POST['Intervention']['email'])) {
				$i->attributes = $_POST['Intervention'];
			}
			if (!$model->getErrors() && $model->validate() && Issn::validateMultiple($issns, $model)) {
				// create a Revue before the Titre?
				if (!$revueId) {
					$i->description = "Création de la revue « {$model->titre} »";
					$i->action = 'revue-C';
					$revue = new Revue;
					$revue->statut = 'normal';
					$i->contenuJson->create($revue);
					$model->revueId = 0;
				} else {
					$i->description = "Création du titre « {$model->titre} »";
				}
				if (Hook::addSherpaLink($model, $issnChanges) && $directAccess) {
					Yii::app()->user->setFlash('info', "Un lien vers Sherpa-Romeo a été ajouté automatiquement au nouveau titre.");
				}
				// main object
				$i->contenuJson->create($model);
				// related issn records
				$i->addChanges($issnChanges);
				// relations
				$this->linkAndUnlinkEditors(0, $i);
				if ($this->validateIntervention($i, $directAccess)) {
					if ($directAccess) {
						$i->titreId = $i->contenuJson->lastInsertId['Titre'];
						$model->id = $i->titreId;
					}
					if (!$revueId && !empty($i->contenuJson[0]['id'])) {
						$model->revueId = $i->contenuJson[0]['id'];
						$i->revueId = $model->revueId;
						$this->updateUserRelations($model);
					}
					$i->save(false);
					if ($directAccess) {
						self::updateCouverture($model);
					}
					if ($model->revueId > 0) {
						return $this->redirect(['/revue/view', 'id' => $model->revueId]);
					}
					return $this->redirect(['/site/index']);
				}
			}
		}

		if (!isset($i)) {
			$i = new Intervention;
		}
		if (!empty($model->revueId)) {
			$i->revueId = $model->revueId;
			$i->suivi = null !== Suivi::isTracked($i->revue);
		}

		$this->render(
			'create',
			[
				'model' => $model,
				'editeurNew' => $editeurNew,
				'direct' => $directAccess,
				'intervention' => $i,
				'forceProposition' => $forceProposition,
				'issns' => $issns,
			]
		);
	}

	public function actionEditeurs(string $id)
	{
		$titre = $this->loadModel($id);
		$model = new \models\views\titre\Editeurs((int) Yii::app()->user->id, $titre);
		if (Yii::app()->getRequest()->isPostRequest) {
			$intervention = $model->createIntervention($_POST['TitreEditeur']);
			if ($intervention !== null) {
				if (Yii::app()->user->checkAccess('titre/update-direct', ['Titre' => $titre])) {
					$intervention->accept(false);
				}
				if ($intervention->save()) {
					$message = $intervention->statut === 'attente' ? " Elles seront appliquées après validation par les partenaires en charge de ce titre." : "";
					Yii::app()->user->setFlash('success', "Les modifications sont enregistrées.$message");
					$this->redirect(['/revue/view', 'id' => $titre->id]);
				} else {
					Yii::app()->user->setFlash('danger', "Erreur lors de l'enregistrement. Contactez l'équipe de Mir@bel.");
				}
			}
		}
		$this->render('editeurs', ['model' => $model]);
	}

	/**
	 * Updates a particular model.
	 *
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param int $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		if ($model->liensJson) {
			// normalize
			$model->liensJson = json_encode($model->getLiens());
		}
		$issns = $model->issns;
		//$model->suivre = !Yii::app()->user->isGuest;

		$directAccess = Yii::app()->user->checkAccess('titre/update-direct', ['Titre' => $model]);
		if ($directAccess) {
			$this->layout = '//layouts/column2';
		}
		$forceProposition = false;
		if ($directAccess) {
			if (isset($_POST['force_proposition'])) {
				$forceProposition = (bool) $_POST['force_proposition'];
			} elseif (!Suivi::checkDirectAccessByIds((int) $model->revueId, null)) {
				$forceProposition = true;
			}
		}
		if ($directAccess && $forceProposition) {
			$directAccess = false;
		}
		$i = $model->buildIntervention($directAccess);

		$editeurNew = $this->parseNewEditorLinks();
		$oldModel = clone $model;
		if ($model->load($_POST)) {
			$issnChanges = Issn::loadMultiple($issns, $_POST);
			$model->attributes = $_POST['Titre'];
			if (isset($_POST['Intervention']['email'])) {
				$i->attributes = $_POST['Intervention'];
			}
			$changedAttributes = array_keys(array_diff_assoc($model->getAttributes(), $oldModel->getAttributes()));
			if ($model->validate($changedAttributes) && Issn::validateMultiple($issns, $model)) {
				if (Hook::addSherpaLink($model, $issnChanges) && $directAccess) {
					Yii::app()->user->setFlash('info', "Un lien vers Sherpa-Romeo a été ajouté automatiquement au titre.");
				}
				$i->description = "Modification du titre « {$model->titre} »";
				$i->contenuJson->update($oldModel, $model, '', false);
				unset($oldModel);
				// related issn records
				$i->addChanges($issnChanges);
				// relations
				$this->linkAndUnlinkEditors((int) $model->id, $i);
				$urlRedir = ['/revue/view', 'id' => $model->revueId];
				if ($i->validate()) {
					if ($directAccess) {
						$i->contenuJson->confirm = true;
						if ($i->accept()) {
							Yii::app()->user->setFlash('success', "Modification enregistrée.");
							$i->emailForEditorChange();
						}
					} else {
						if ($i->save(false)) {
							Yii::app()->user->setFlash(
								'success',
								"Proposition enregistrée. Merci d'avoir contribué à Mir@bel."
							);
							$i->emailForEditorChange("", "Proposition");
						}
					}
					if ($directAccess) {
						FeedGenerator::removeFeedsFile('Revue', $model->revueId);
						if ($model->urlCouvertureDld) {
							self::updateCouverture($model);
						}
					}
					$this->redirect($urlRedir);
				} elseif ($model->urlCouvertureDld && $directAccess && count($i->errors) === 1 && $i->errors['commentaire']) {
					self::updateCouverture($model);
					$this->redirect($urlRedir);
				}
			}
		} else {
			// default values when no form is received
			$model->suivre = $model->monitoredBy();
			$model->posseder = $model->belongsTo();
		}

		$this->render(
			'update',
			[
				'model' => $model,
				'editeurNew' => $editeurNew,
				'direct' => $directAccess,
				'intervention' => $i,
				'forceProposition' => $forceProposition,
				'issns' => $issns,
			]
		);
	}

	/**
	 * Download the couverture.
	 *
	 * @param int $id the ID of the model to be displayed
	 */
	public function actionCouverture($id)
	{
		$model = $this->loadModel($id);
		self::updateCouverture($model);
		$this->redirect(['/revue/view', 'id' => $model->revueId]);
	}

	/**
	 * Deletes a particular model.
	 *
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 *
	 * @param int $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$titre = $this->loadModel($id);

		$hasIntervention = Yii::app()->db
			->createCommand("SELECT 1 FROM Intervention WHERE titreId = ? AND statut = 'attente'")
			->queryScalar([(int) $id]);
		if ($hasIntervention) {
			Yii::app()->user->setFlash('error', "Suppression impossible car au moins une intervention est en attente sur ce titre.");
			$returnUrl = $_POST['returnUrl']
					?? ['/revue/view', 'id' => $titre->revueId];
			$this->redirect($returnUrl);
		}

		// we only allow deletion via POST request
		if (Yii::app()->request->isPostRequest) {
			$i = $titre->buildIntervention(true);
			$i->contenuJson->delete($titre, "Suppression du titre " . $titre->titre);
			$numTitres = Titre::model()->countByAttributes(['revueId' => $titre->revueId]);
			if ($numTitres == 1) {
				$i->description = "Suppression de la revue « {$titre->titre} »";
				$i->contenuJson->delete($titre->revue);
				$returnUrl = ['/revue/index'];
				require_once Yii::app()->getBasePath() . '/components/FeedGenerator.php';
				FeedGenerator::saveFeedsToFile('Revue', (int) $titre->revueId);
			} else {
				$i->description = "Suppresson du titre « {$titre->titre} »";
				$returnUrl = $_POST['returnUrl']
					?? ['/revue/view', 'id' => $titre->revueId];
			}
			$success = $i->accept(false);
			$i->revueId = null;
			$i->titreId = null;

			if ($success) {
				$i->save(false);
				Yii::app()->user->setFlash('success', "Titre « {$titre->fullTitle} » supprimé.");
			} else {
				if ($numTitres == 1) {
					FeedGenerator::removeFeedsFile('Revue', $titre->revueId);
				}
				$errors = $titre->getDeleteErrors();
				$returnUrl = ['view', 'id' => $titre->id];
				$msg = "<ul><li>" . join("</li><li>", $errors) . "</li></ul>";
				Yii::app()->user->setFlash('info', "Suppression de « {$titre->fullTitle} » impossible :" . $msg);
			}
			$this->redirect($returnUrl);
		} else {
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}
	}

	/**
	 * Updates all the Identification entries.
	 *
	 * @param int $id the ID of the Titre record.
	 */
	public function actionIdentification($id)
	{
		$model = $this->loadModel($id);
		unset($id);
		$identifications = [];
		// add the Identification records from the Ressources linked by a Service
		$ressources = $model->getRessourcesByServices();
		$ids = [];
		foreach ($ressources as $ressource) {
			$attributes = ['titreId' => $model->id, 'ressourceId' => $ressource->id];
			$idents = Identification::model()->findAllByAttributes($attributes);
			if ($idents) {
				$identifications[$ressource->id] = $idents;
				foreach ($idents as $i) {
					$ids[] = $i->id;
				}
			} else {
				$empty = new Identification();
				$empty->titreId = $model->id;
				$empty->ressourceId = $ressource->id;
				$identifications[$ressource->id] = [$empty];
			}
		}
		// add the other Identification records
		$criteria = new CDbCriteria();
		$criteria->addColumnCondition(['titreId' => $model->id]);
		$criteria->addNotInCondition('id', $ids);
		$criteria->order = 'ressourceId';
		$idents = Identification::model()->findAll($criteria);
		if ($idents) {
			foreach ($idents as $ident) {
				if (isset($identifications[$ident->ressourceId])) {
					$identifications[$ident->ressourceId][] = $ident;
				} else {
					$identifications[$ident->ressourceId] = [$ident];
					$ressources[$ident->ressourceId] = Ressource::model()->findByPk($ident->ressourceId);
				}
			}
		}
		unset($idents);
		$creation = new Identification();
		$creation->titreId = $model->id;

		if (isset($_POST['Identification'])) {
			$valid = true;
			$toSave = [];
			foreach ($identifications as $rid => $idents) {
				foreach ($idents as $num => $ident) {
					$ident->attributes = $_POST['Identification'][$rid][$num];
					if ($ident->idInterne) {
						$valid = $ident->validate() && $valid;
						if ($valid) {
							$toSave[] = $ident;
						}
					} else {
						if (!$ident->isNewRecord && $ident->id && $ident->delete()) {
							unset($identifications[$rid][$num]);
							Yii::app()->user->setFlash(
								'info',
								"Suppression de l'identification pour « {$ident->ressource->nom} »"
							);
						}
					}
				}
			}
			$creation->attributes = $_POST['Identification'][0];
			if ($creation->idInterne) {
				$valid = $creation->validate() && $valid;
				if ($valid) {
					$toSave[] = $creation;
				}
			}
			if ($valid) {
				foreach ($toSave as $ident) {
					$ident->save(false);
				}
				$this->redirect(['view', 'id' => $model->id]);
			}
		}

		$this->render(
			'identification',
			[
				'model' => $model,
				'ressources' => $ressources,
				'identifications' => $identifications,
				'creation' => $creation,
			]
		);
	}

	/**
	 * Export all.
	 */
	public function actionExport($type = '')
	{
		if ($type !== 'ppn' && $type !== 'bnf') {
			$user = Yii::app()->user;
			if ($user->getIsGuest()) {
				$user->loginRequired();
			}
			if (!$user->checkAccess('titre/admin')) {
				throw new CHttpException(403, "Accès restreint");
			}
		}

		$basename = str_replace([' ', '@'], ['_', 'a'], Yii::app()->name);
		if ($type === 'identifiants') {
			$this->header('csv');
			header(sprintf('Content-disposition: attachment; filename="%s_%s_identifiants.csv"', $basename, date('Y-m-d')));
			ExportTitles::printIdentifiersCsv();
		} elseif ($type === 'maxi') {
			$this->header('csv');
			header(sprintf('Content-disposition: attachment; filename="%s_%s_maxi.csv"', $basename, date('Y-m-d')));
			ExportTitles::printMaxiCsv();
		} elseif ($type === 'ppn') {
			$this->header('csv');
			header(sprintf('Content-disposition: attachment; filename="%s_%s_ppn_url-mirabel.csv"', $basename, date('Y-m-d')));
			ExportTitles::printPpnCsv();
		} elseif ($type === 'bnf') {
			$this->header('csv');
			header(sprintf('Content-disposition: attachment; filename="%s_%s_BnF.csv"', $basename, date('Y-m-d')));
			ExportTitles::printBnfCsv();
		} else {
			$this->renderPartial('export');
		}
	}

	/**
	 * Propose completions for a term (AJAX).
	 */
	public function actionAjaxComplete()
	{
		if (empty($_GET['term'])) {
			throw new CHttpException(403, 'Missing "term" GET parameter.');
		}
		$term = $_GET['term'];
		$this->header('json');
		echo json_encode(Titre::completeTerm($term, [], [], false, 'text'));
	}

	public function actionAjaxIssn($position, $titreId)
	{
		$newIssn = new Issn();
		$newIssn->support = Issn::SUPPORT_INCONNU;
		$newIssn->titreId = (int) $titreId;
		ob_start();
		$form = $this->beginWidget(
			'bootstrap.widgets.BootActiveForm',
			[
				'id' => 'titre-form',
				'enableAjaxValidation' => false,
				'enableClientValidation' => false,
				'type' => BootActiveForm::TYPE_HORIZONTAL,
			]
		);
		ob_end_clean();
		$this->renderPartial(
			'_issn',
			[
				'form' => $form,
				'position' => $position,
				'issn' => $newIssn,
			]
		);
	}

	public function actionAjaxSudoc($issn = '', $issns = '', $ppn = '', $withHtml = false)
	{
		if (empty($issn) && empty($issns) && empty($ppn)) {
			throw new CHttpException(402, "An issn|issns|ppn parameter is mandatory.");
		}
		$this->header('json');
		$_SERVER['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest';

		$notices = [];
		$api = new \models\sudoc\Api();
		try {
			if ($ppn) {
				$notices[] = $api->getNotice($ppn);
			} elseif ($issn) {
				$notices[] = $api->getNoticeByIssn($issn);
			} else {
				foreach (preg_split('/[\s,]+/', $issns) as $issn) {
					$notices[] = $api->getNoticeByIssn($issn);
				}
				unset($issn);
			}
		} catch (Exception $e) {
			throw new CHttpException(500, $e->getMessage());
		}
		$notices = array_filter($notices);
		$response = [
			'notices' => $notices,
		];


		if ($withHtml) {
			if ($notices) {
				$titre = new Titre();
				$titre->fillBySudocNotices($notices);
				$issnRecords = [];
				foreach ($notices as $notice) {
					$issnR = new Issn();
					$issnR->fillBySudocNotice($notice);
					$issnRecords[] = $issnR;
				}
				$response['html'] = $this->renderPartial(
					'_sudoc-info',
					['notice' => $notice, 'titre' => $titre, 'issns' => $issnRecords],
					true
				);
			} else {
				http_response_code(404);
				$response['html'] =
					<<<EOHTML
						<div class="alert alert-warning">
						Cet ISSN est introuvable dans le Sudoc. Vérifiez que votre ISSN est correctement saisi.

						Si vous préférez créer le titre sans passer par l'import Sudoc, utilisez le bouton « Créer un titre sans ISSN »
						</div>
						EOHTML;
			}
		}

		echo json_encode($response, YII_DEBUG ? JSON_PRETTY_PRINT : 0);
	}

	public function actionPublication($id)
	{
		$titre = $this->loadModel($id);
		if (!$titre->getLiens()->containsSource(31)) { // 31 = Sherpa Romeo
			throw new \CHttpException(404, "Ce titre n'a pas de données sur sa politique de publication (source Sherpa-Romeo).");
		}
		$item = \models\sherpa\Item::loadFromCache((int) $titre->id);
		if (!$item) {
			$issns = array_filter(array_map(
				function (Issn $i): string {
					return $i->issn;
				},
				$titre->issns
			));
			$item = \models\sherpa\Item::loadFromCacheByIssn((int) $titre->id, $issns);
			if (!$item) {
				$apiKey = Yii::app()->params->itemAt('sherpa')['api-key'];
				$api = new \models\sherpa\Api($apiKey);
				$item = $api->fetchPublication((int) $titre->id, $issns);
			}
		}
		if (!$item) {
			throw new \CHttpException(404, "Ce titre n'a pas de données sur sa politique de publication (source Sherpa-Romeo).");
		}
		$this->render(
			'publication',
			[
				'titre' => $titre,
				'doajUrl' => Yii::app()->db
					->createCommand("SELECT url FROM LienTitre WHERE titreId = ? AND sourceId = ?")
					->queryScalar([$titre->id, 1]),
				'publication' => $item->getPublication(),
			]
		);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 *
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param int $id the ID of the model to be loaded
	 * @return Titre
	 */
	public function loadModel($id)
	{
		$model = Titre::model()->findByPk((int) $id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $model;
	}

	/**
	 * @param Titre $model
	 */
	public static function updateCouverture($model)
	{
		$tc = new TitreCouverture();

		if (!$model->urlCouverture) {
			$tc->delete($model->id);
		} else {
			try {
				if ($tc->download($model->id, $model->urlCouverture)) {
					Yii::app()->user->setFlash('info', "La couverture du titre a été téléchargée et redimensionnée.");
				}
				$tc->resizeByIdentifier($model->id);
				Yii::app()->session->add('force-refresh', true);
			} catch (Exception $e) {
				Yii::app()->user->setFlash('warning', "Erreur de téléchargement de la couverture du titre : " . $e->getMessage());
			}
		}
	}

	protected function parseNewEditorLinks()
	{
		if (empty($_POST['TitreEditeurNew'])) {
			$editeurNew = [];
		} else {
			foreach ($_POST['TitreEditeurNew'] as $relation) {
				$editeurNew[] = Editeur::model()->findByPk($relation['editeurId'] ?? 0);
			}
		}
		return array_filter($editeurNew);
	}

	protected function linkAndUnlinkEditors($titreId, Intervention $interv): void
	{
		// detach old relations and update existing relations
		if ($titreId && !empty($_POST['TitreEditeur'])) {
			foreach ($_POST['TitreEditeur'] as $eId => $e) {
				if (!isset($e['editeurId'])) {
					continue;
				}
				$relation = TitreEditeur::model()->findByPk(['titreId' => $titreId, 'editeurId' => (int) $eId]);
				if ($e['editeurId'] === '0') {
					$interv->contenuJson->delete(
						$relation,
						"Détache l'éditeur « {$relation->editeur->nom} »"
					);
				} else {
					$interv->contenuJson->updateByAttributes(
						$relation,
						[
							'ancien' => $e['ancien'] ?? null,
							'commercial' => $e['commercial'] ?? 0,
							'intellectuel' => $e['intellectuel'] ?? 0,
						]
					);
				}
			}
		}

		// attach new relations
		if (isset($_POST['TitreEditeurNew'][0])) {
			unset($_POST['TitreEditeurNew'][0]); // template for JS extensions
		}
		if (!empty($_POST['TitreEditeurNew'])) {
			$this->attachEditeurs($_POST['TitreEditeurNew'], $titreId, $interv);
		}
	}

	protected function attachEditeurs(array $edData, $titreId, Intervention $interv)
	{
		foreach ($edData as $data) {
			$relation = new TitreEditeur;
			$relation->titreId = (int) $titreId;
			$relation->editeurId = (int) $data['editeurId'];
			$relation->ancien = $data['ancien'] ?? null;
			$relation->commercial = $data['commercial'] ?? 0;
			$relation->intellectuel = $data['intellectuel'] ?? 0;
			if ($relation->editeurId > 0 && !empty($relation->editeur)) {
				$interv->contenuJson->create(
					$relation,
					"Attache l'éditeur « {$relation->editeur->nom} »"
				);
			}
		}
	}

	protected function updateUserRelations(Titre $titre)
	{
		$user = Yii::app()->user;
		if ($user->isGuest) {
			return false;
		}
		if ($titre->suivre && $titre->revueId && !$titre->monitoredBy($user->partenaireId)) {
			$titre->dbConnection
				->createCommand(
					"INSERT IGNORE INTO Suivi (partenaireId, cible, cibleId) VALUES "
					. "(:partenaireId, 'Revue', :id)"
				)->execute(
					[':partenaireId' => $user->partenaireId, ':id' => $titre->revueId]
				);
			Yii::app()->user->setState(
				'suivi',
				Partenaire::model()->findByPk($user->partenaireId)->getRightsSuivi()
			);
			Yii::app()->user->setFlash('info suivi', "Votre partenaire suit désormais cette revue.");
		}
		/*
		if ($titre->posseder && $titre->id && !$titre->belongsTo($user->partenaireId)) {
			$titre->dbConnection
				->createCommand(
					"INSERT IGNORE INTO Partenaire_Titre (partenaireId, titreId, identifiantLocal, bouquet) "
					. "VALUES (:partenaireId, :id, '', '')"
				)->execute(
					array(':partenaireId' => $user->partenaireId, ':id' => $titre->id)
				);
			Yii::app()->user->setFlash('info possession', "Votre partenaire possède désormais ce titre.");
		}
		 */
		return true;
	}
}
