<?php
Yii::import('application.models.import.*');

class ImportController extends Controller
{
	public $layout = '//layouts/column2';

	public function init()
	{
		parent::init();
		if (!Yii::app()->user->checkAccess('import')) {
			throw new CHttpException(403, 'Accès restreint.');
		}
	}

	/**
	 * Displays the import page / KBART
	 */
	public function actionKbart()
	{
		$import = null;
		$model = new ImportKbartForm();
		$model->ignoredFields = ImportKbart::$ignoredFields;
		if (isset($_POST['ImportKbartForm'])) {
			$model->attributes = $_POST['ImportKbartForm'];
			$model->csvfile = CUploadedFile::getInstance($model, 'csvfile');
			if ($model->validate()) {
				$import = new ImportKbart($model->getParameters());
			}
		}
		$this->runImport($import, 'KBART');
		$deleted = [];
		if ($import && $model->checkDeletion) {
			$deleted = $this->checkDeletion($import->getRessource(), $import->getId());
		}
		$this->render(
			'import',
			[
				'model' => $model,
				'importLog' => $import ? $import->log : null,
				'deleted' => $deleted,
				'deadTitles' => null,
				'ignoredTitles' => $import && $import->ignoreUnknownTitles ? $import->getIgnoredTitles() : null,
			]
		);
	}

	/**
	 * Displays the import page / spec imports (Cairn, etc)
	 */
	public function actionSpec()
	{
		$import = null;
		$source = 'spec';
		$model = new ImportSpecForm();
		if (isset($_POST[get_class($model)])) {
			$model->attributes = $_POST[get_class($model)];
			if ($model->validate()) {
				$source = $model->className;
				$className = 'Import' . $model->className;
				$import = new $className($model->getParameters());
			}
		}
		$this->runImport($import, $source);
		$deleted = [];
		if ($import && $model->checkDeletion) {
			$deleted = $this->checkDeletion($import->getRessource(), $import->getId());
		}
		$this->render(
			'import',
			[
				'model' => $model,
				'importLog' => $import ? $import->log : null,
				'deleted' => $deleted,
				'deadTitles' => null,
				'ignoredTitles' => null,
			]
		);
	}

	/**
	 * Runs the import requested.
	 *
	 * @param ?Import $import
	 * @param string $source
	 * @return bool Success?
	 */
	protected function runImport(?Import $import, string $source): bool
	{
		if ($import) {
			Yii::log("Import web de type '$source'", 'info', 'import');
			try {
				$import->init();
				$import->importAll();
			} catch (Exception $e) {
				Yii::app()->user->setFlash('error', "Erreur fatale lors de l'import : {$e->getMessage()}");
				Yii::log($e->getMessage(), 'error', 'import');
				if (YII_DEBUG) {
					echo "<h1>page d'erreur brute en mode DEBUG</h1>";
					var_dump($e);
					exit();
				}
				return false;
			}
		}
		return true;
	}

	/**
	 * Return an array of titles that had services deleted.
	 *
	 * @param ?Ressource $ressource
	 * @param int $importId
	 * @return array
	 */
	protected function checkDeletion(?Ressource $ressource, int $importId): array
	{
		if (!$ressource) {
			return [];
		}

		Yii::log("Import : recherche d'accès supprimés [{$ressource->nom}]", 'info', 'import');
		$services = Service::findDeletedFromImport($ressource->id, 0, $importId);
		$titles = [];
		if ($services) {
			foreach ($services as $s) {
				if (isset($titles[$s->titreId])) {
					$titles[$s->titreId]['services'][] = $s;
				} else {
					$titles[$s->titreId] = ['title' => $s->titre, 'services' => [$s]];
				}
			}
		}
		return $titles;
	}
}
