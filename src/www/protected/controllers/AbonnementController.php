<?php

class AbonnementController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'users' => ['@'],
			],
			['deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	public function actionDelete()
	{
		list($partenaireId, $collectionId, $ressourceId) = $this->parseArgs();

		if ($collectionId > 0) {
			Abonnement::model()->deleteAllByAttributes(['partenaireId' => $partenaireId, 'collectionId' => $collectionId]);
		} else {
			Abonnement::model()->deleteAllByAttributes(['partenaireId' => $partenaireId, 'ressourceId' => $ressourceId]);
		}
		$this->goBack($partenaireId, $collectionId, $ressourceId);
	}

	public function actionHide()
	{
		list($partenaireId, $collectionId, $ressourceId) = $this->parseArgs();
		$this->saveAbonnement($partenaireId, $collectionId, $ressourceId, Abonnement::MASQUE);
	}

	public function actionSubscribe()
	{
		list($partenaireId, $collectionId, $ressourceId) = $this->parseArgs();
		$this->saveAbonnement($partenaireId, $collectionId, $ressourceId, Abonnement::ABONNE);
	}

	public function actionToggleProxy($id)
	{
		$model = Abonnement::model()->findByPk((int) $id);
		if (!$model) {
			throw new CHttpException(404, "Pas d'abonnement correspondant à cet ID.");
		}
		if (Yii::app()->request->isPostRequest) {
			$model->proxy = !$model->proxy;
			$model->save(false);
			$this->goBack($model->partenaireId, $model->collectionId, $model->ressourceId);
		} else {
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}
	}

	public function actionUpdateProxyurl()
	{
		$id = (int) Yii::app()->request->getPost('id', 0);
		$model = Abonnement::model()->findByPk($id);
		if (!$model) {
			throw new CHttpException(404, "Pas d'abonnement correspondant à cet ID.");
		}
		if (Yii::app()->request->isPostRequest) {
			$model->proxyUrl = Yii::app()->request->getPost('proxyurl', null);
			if (!$model->save()) {
				Yii::app()->user->setFlash(
					'error',
					"La modification de l'abonnement est incorrecte : " . $model->getError('proxyUrl')
				);
			}
			$this->redirect(['/ressource/view', 'id' => $model->ressourceId]);
		} else {
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}
	}

	private function saveAbonnement($partenaireId, $collectionId, $ressourceId, $mask)
	{
		if ($collectionId > 0) {
			$model = Abonnement::model()->findByAttributes(['partenaireId' => $partenaireId, 'collectionId' => $collectionId]);
			if (!$model) {
				$model = new Abonnement();
				$model->collectionId = (int) $collectionId;
			}
		} else {
			$model = Abonnement::model()->findByAttributes(['partenaireId' => $partenaireId, 'ressourceId' => $ressourceId, 'collectionId' => null]);
			if (!$model) {
				$model = new Abonnement();
				$model->ressourceId = (int) $ressourceId;
			}
		}
		$model->partenaireId = $partenaireId;
		$model->mask = $mask;
		if ($model->isNewRecord && $model->partenaireId && $model->partenaire->proxyUrl) {
			$model->proxy = true;
		}

		if ($model->collectionId && $model->collection->type === Collection::TYPE_TEMPORAIRE) {
			Yii::app()->user->setFlash(
				'error',
				"L'abonnement à une collection temporaire est impossible."
			);
		} else {
			try {
				if ($model->save()) {
					Yii::app()->user->setFlash('success', "Abonnement enregistré.");
				} else {
					Yii::app()->user->setFlash(
						'error',
						"Erreur lors de l'enregistrement de l'abonnement dans la base de données : "
						. HtmlHelper::definitionList(
							array_map(
								function ($x) {
									return join('<br />', $x);
								},
								$model->getErrors()
							),
							[$model, 'getAttributeLabel']
						)
					);
				}
			} catch (Exception $e) {
				Yii::app()->user->setFlash(
					'error',
					"Erreur fatale lors de l'enregistrement de l'abonnement dans la base de données (doublon ?) : " . $e->getMessage()
				);
			}
		}
		$this->goBack($partenaireId, $collectionId, $ressourceId);
	}

	private function parseArgs()
	{
		$request = Yii::app()->getRequest();
		$partenaireId = (int) $request->getParam('partenaireId');
		if (!$partenaireId) {
			throw new CHttpException(404, 'Missing field partenaireId');
		}
		$collectionId = (int) $request->getParam('collectionId');
		$ressourceId = (int) $request->getParam('ressourceId');
		return [$partenaireId, $collectionId, $ressourceId];
	}

	private function goBack($partenaireId, $collectionId, $ressourceId)
	{
		$return = Yii::app()->getRequest()->getParam('return');
		if ($return) {
			return $this->redirect($return);
		}

		if (empty($ressourceId) && !empty($collectionId)) {
			$collection = Collection::model()->findByPk($collectionId);
			$ressourceId = $collection->ressourceId;
		}
		if (!empty($ressourceId)) {
			return $this->redirect(['/ressource/view', 'id' => (int) $ressourceId]);
		}

		return $this->redirect(['/partenaire/abonnements', 'id' => (int) $partenaireId]);
	}
}
