<?php

class DocController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	//public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'users' => ['@'],
			],
			['deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	public function actions()
	{
		return [
			'view' => [
				'class' => 'CViewAction',
			],
		];
	}

	/**
	 * List pages.
	 */
	public function actionIndex()
	{
		$pages = array_map(
			function ($v) {
				return preg_replace('/\.md$/', '', basename($v));
			},
			glob(YiiBase::getPathOfAlias('application.views.doc') . '/pages/*.md')
		);
		$pdfs = array_map(
			'basename',
			glob(YiiBase::getPathOfAlias('application.views.doc') . '/pages/*.pdf')
		);

		$this->render(
			'index',
			['pages' => $pages, 'pdfs' => $pdfs]
		);
	}

	public function actionPdf($file)
	{
		$base = realpath(YiiBase::getPathOfAlias('application.views.doc'));
		$pdf = $base . '/pages/' . $file;
		if (!file_exists($pdf)) {
			throw new CHttpException(404, 'PDF not found');
		}
		$pdf = realpath($pdf);
		if (!preg_match('/\.pdf$/', $pdf)) {
			throw new CHttpException(404, 'PDF not found or not allowed');
		}
		$this->header('pdf');
		readfile($pdf);
	}

	/**
	 * @param CAction $action
	 * @return bool
	 */
	protected function beforeAction($action)
	{
		if ($action->id === 'view') {
			$this->breadcrumbs = [
				'Doc technique' => ['/doc'],
				(empty($_GET['view']) ? '*' : $_GET['view']),
			];
		}
		return parent::beforeAction($action);
	}
}
