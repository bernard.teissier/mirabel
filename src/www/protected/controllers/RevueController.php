<?php

use models\validators\IssnValidator;
use models\wikidata\Compare;

class RevueController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	//public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'actions' => ['ajaxComplete', 'carte', 'checkLinks', 'export', 'index', 'search', 'view', 'viewBy'],
				'users' => ['*'],
			],
			[
				'allow',
				'actions' => ['categories', 'delete', 'exportToWikidata', 'retireCategorie', 'verify'],
				'users' => ['@'],
			],
			[
				'deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	public function actionCarte($stamen = '', $vivantes = null)
	{
		if (!preg_match('/^[\w-]+$/', $stamen)) {
			$stamen = '';
		}
		$countries = [];
		$max = 0;
		$query = Yii::app()->db->createCommand(
			"SELECT p.code, p.id, p.nom, count(distinct e.id) AS editeurs, count(distinct t.revueId) AS revues"
			. " FROM Editeur e JOIN Pays p ON e.paysId = p.id"
			. " LEFT JOIN Titre_Editeur te ON te.editeurId = e.id LEFT JOIN Titre t ON te.titreId = t.id"// AND t.obsoletePar IS NULL"
			. ($vivantes ? " WHERE t.dateFin = ''" : "")
			. " GROUP BY p.id, p.code"
		);
		foreach ($query->query() as $row) {
			if ($max < $row['editeurs']) {
				$max = $row['editeurs'];
			}
			$url = ['/revue/search', 'q' => ['paysId' => $row['id']]];
			if ($vivantes) {
				$url['q']['vivant'] = 1;
			}
			$countries[$row['code']] = [
				$row['revues'],
				CHtml::tag('div', [], CHtml::encode($row['nom']))
				. CHtml::tag(
					'strong',
					[],
					CHtml::link(
						$row['revues'] . " revue" . ($row['revues'] > 1 ? "s" : ""),
						$url
					)
				)
				. (
					$vivantes ? "" :
					"<br /> pour "
					. CHtml::link(
						$row['editeurs'] . " éditeur" . ($row['editeurs'] > 1 ? "s" : ""),
						['/editeur/pays', 'paysId' => $row['code'], 'paysNom' => $row['nom']]
					)
				),
			];
		}
		$this->render(
			'carte',
			[
				'stamen' => $stamen,
				'vivantes' => $vivantes,
				'nbRevues' => Yii::app()->db
					->createCommand("SELECT count(DISTINCT revueId) FROM Titre" . ($vivantes ? " WHERE dateFin = ''" : ""))
					->queryScalar(),
				'mapConfig' => [
					'countries' => $countries,
					'countriesMax' => $max,
					'radius' => [
						'max' => 12, // pixels
						'method' => 'logarithmic',
					],
					'popupMessage' => new CJavaScriptExpression('function(x) { return x[1]; }'),
				],
			]
		);
	}

	/**
	 * Displays a particular model.
	 *
	 * @param int $id Revue.id
	 * @param string  $nom Titre.nomComplet
	 * @param bool $public True if the view must ignore the subscriptions
	 */
	public function actionView($id, $nom = '', $public = false)
	{
		$revue = $this->loadModel($id);
		$titres = $revue->titres;
		$nomEnc = Norm::urlParam($titres[0]->getFullTitle());
		if ($nom !== $nomEnc) {
			$url = ['view', 'id' => (int) $id, 'nom' => $nomEnc];
			if (!empty($_GET['s'])) {
				$url['s'] = $_GET['s'];
			}
			if ($public) {
				$url['public'] = 1;
			}
			return $this->redirect($url);
		}
		if (Yii::app()->user->isGuest) {
			$this->layout = '//layouts/column1';
		}
		$this->appendToHtmlHead(
			Controller::linkEmbeddedFeed('rss2', "Revue " . $revue->getFullTitle(), ['revue' => $revue->id])
		);
		if (Yii::app()->session->contains('force-refresh')) {
			Yii::app()->session->remove('force-refresh');
			$forceRefresh = true;
		} else {
			$forceRefresh = false;
		}
		if (Yii::app()->user->isGuest) {
			$abonnement = null;
		} else {
			$abonnement = new AbonnementSearch();
			$abonnement->partenaireId = Yii::app()->user->partenaireId;
		}

		$this->render(
			'view',
			[
				'revue' => $revue,
				'titres' => $titres,
				'services' => $revue->getServices(),
				'partenaires' => $revue->getPartenairesSuivant(),
				'direct' => Yii::app()->user->checkAccess('revue/update-direct', ['Revue' => $revue]),
				'searchNavigation' => $this->loadSearchNavigation(),
				'forceRefresh' => $forceRefresh,
				'abonnement' => $abonnement,
				'publicView' => $public,
			]
		);
	}

	public function actionViewBy($by, $id)
	{
		$value = str_replace('_', ' ', $id);
		$sql = '';
		$params = [];
		switch ($by) {
			case 'titre-id':
				$sql = "WHERE id = :id";
				$params = [':id' => (int) $id];
				break;
			case 'titre':
				$sql = "WHERE titre = :t OR sigle = :s OR CONCAT(prefixe,titre) = :ft";
				$params = [':t' => $value, ':s' => $value, ':ft' => $value];
				break;
			case 'sudoc':
				$sql = "JOIN Issn i ON i.titreId = t.id WHERE i.sudocPpn = :id";
				$params = [':id' => $id];
				break;
			case 'worldcat':
				$sql = "JOIN Issn i ON i.titreId = t.id WHERE i.worldcatOcn = :id";
				$params = [':id' => $id];
				break;
			case 'issn':
				$v = new IssnValidator();
				$v->allowEmpty = false;
				$v->allowEmptyChecksum = true;
				$v->allowMissingCaret = true;
				$v->specialValues = [];
				$v->validateString($id);
				$issn = $v->lastIssn;
				$sql = "JOIN Issn i ON t.id = i.titreId WHERE i.issn = :issn";
				$params = [':issn' => $issn];
				break;
			default:
				throw new CHttpException(404, "Paramètres non-valides.");
		}
		$revuesIds = Yii::app()->db->createCommand(
			"SELECT DISTINCT t.revueId FROM Titre t " . $sql
		)->queryColumn($params);
		if (count($revuesIds) === 0) {
			throw new CHttpException(404, "Cette revue n'a pu être trouvée.");
		}
		if (count($revuesIds) > 1) {
			if ($by === 'titre') {
				Yii::app()->user->setFlash('info', "Plusieurs revues correspondent à ce critère.");
				$this->redirect(['revue/search', "SearchTitre[$by]" => $value]);
			} else {
				Yii::app()->user->setFlash('info', "Plusieurs revues correspondent à ce critère. Veuillez passer par une recherche.");
				$this->redirect(['revue/search']);
			}
		}
		return $this->actionView($revuesIds[0]);
	}

	/**
	 * Deletes a particular model.
	 *
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 *
	 * @param int $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$revue = $this->loadModel($id);

		$hasIntervention = Yii::app()->db
			->createCommand("SELECT 1 FROM Intervention WHERE revueId = ? AND statut = 'attente'")
			->queryScalar([(int) $id]);
		if ($hasIntervention) {
			Yii::app()->user->setFlash('error', "Suppression impossible car au moins une intervention est en attente sur cette revue.");
			$this->redirect(['view', 'id' => $revue->id]);
		}

		if (Yii::app()->request->isPostRequest) {
			$editeurs = Editeur::model()->findAllBySql(
				"SELECT e.* FROM Editeur e JOIN Titre_Editeur te ON te.editeurId = e.id JOIN Titre t ON t.id = te.titreID WHERE t.revueId = ?",
				[$revue->id]
			);
			$titres = $revue->getTitres(false);
			if (count($titres) > 1) {
				Yii::app()->user->setFlash('error', 'On ne peut supprimer une revue ayant plusieurs titres.');
				$this->redirect(['view', 'id' => $revue->id]);
			}
			require_once Yii::app()->getBasePath() . '/components/FeedGenerator.php';
			$returnUrl = ['/revue/index'];
			$i = new Intervention();
			$i->statut = 'attente';
			$i->action = 'revue-D';
			$i->utilisateurIdProp = Yii::app()->user->id;
			$i->import = 0;
			$i->ip = $_SERVER['REMOTE_ADDR'];
			$i->contenuJson = new InterventionDetail();
			if ($titres) {
				$fullTitle = $titres[0]->titre;
				$i->contenuJson->delete($titres[0], "Suppression de la revue " . $fullTitle);
				$i->description = "Suppression de la revue " . $fullTitle;
			} else {
				$fullTitle = 'SansTitre';
			}
			$i->contenuJson->delete($revue);
			if ($i->validate()) {
				FeedGenerator::saveFeedsToFile('Revue', (int) $revue->id);
			}
			$success = $i->accept(false);

			if ($success) {
				$i->save(false);
				Yii::app()->user->setFlash('success', "Revue « {$fullTitle} » supprimée.");
				$warning = "";
				foreach ($editeurs as $e) {
					if ($e->countRevues() === 0) {
						$warning .= "<br>L'éditeur « {$e->getSelfLink()} » n'a plus aucune revue. Vous devez peut-être le supprimer.";
					}
				}
				if ($warning) {
					Yii::app()->user->setFlash('danger', "<strong>Éditeur orphelin !</strong> $warning");
				}
			} else {
				FeedGenerator::removeFeedsFile('Revue', $revue->id);
				$errors = $titres[0]->getDeleteErrors();
				if (!$errors && !empty($i->errors['contenuJson'])) {
					$errors = $i->errors['contenuJson'];
				}
				$returnUrl = ['view', 'id' => $revue->id];
				$msg = "<ul><li>" . join("</li><li>", $errors) . "</li></ul>";
				Yii::app()->user->setFlash('info', "Suppression de « {$fullTitle} » impossible :" . $msg);
			}
			$this->redirect($returnUrl);
		} else {
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($lettre='A')
	{
		if (Yii::app()->user->isGuest) {
			$this->layout = '//layouts/column1';
		}
		if (ctype_digit($lettre)) {
			$display = "#0-9";
		} else {
			$display = strtoupper($lettre);
		}
		$dataProvider = Revue::listAlpha($lettre);
		try {
			$data = $dataProvider->getData();
		} catch (\Exception $e) {
			throw new CHttpException(400, "Erreur dans la recherche de revues. Navigation au-delà de la taille des résultats ?");
		}
		$searchCache = new SearchNavigation();
		$searchHash = $searchCache->save('revue/index', ['lettre' => $lettre], 'SearchTitre', $data);
		$this->render(
			'index',
			[
				'dataProvider' => $dataProvider,
				'lettre' => $display,
				'searchHash' => $searchHash,
				'totalRevues' => Revue::model()->count("statut = 'normal'"),
			]
		);
	}

	/**
	 * Search.
	 */
	public function actionSearch()
	{
		Yii::log("Contact, envoi réussi :\n" . print_r(Yii::app()->getComponent('logger'), true), CLogger::LEVEL_WARNING, 'contact');
		$model = new SearchTitre(Yii::app()->user->getInstitute());
		$results = null;
		$searchHash = '';

		if (isset($_GET['SearchTitre'])) {
			$filtered = array_filter($_GET['SearchTitre'], function ($x) {
				return $x !== '';
			});
			$model->attributes = $filtered;
			if ($model->validate()) {
				$this->redirect(['search', 'q' => $model->exportAttributes()]);
			}
		} elseif (isset($_GET['q'])) {
			$model->attributes = $_GET['q'];
			// Backward compatibility on monitoredNot
			if (!empty($_GET['q']['monitoredNot'])) {
				$model->monitored = SearchTitre::MONIT_NO;
			}
			if ($model->validate()) {
				$results = $model->search();
			}
		}

		if ($results) {
			if ($results->itemCount == 1) {
				$data = $results->data;
				$result = reset($data);
				$this->redirect(['view', 'id' => $result['attrs']['revueid']]);
			}
			if ($results->itemCount > 0) {
				$searchCache = new SearchNavigation();
				$searchHash = $searchCache->save(
					'revue/search',
					['SearchTitre' => array_filter($_GET['q'])],
					'SearchTitre',
					$results->getData(),
					$results->pagination
				);
			}
		}
		$this->render(
			'search',
			[
				'model' => $model,
				'results' => $results,
				'searchHash' => $searchHash,
			]
		);
	}

	/**
	 * Marks as verified.
	 *
	 * @param int $id the ID of the model to be marked.
	 */
	public function actionVerify($id)
	{
		$model = $this->loadModel((int) $id);
		if (!Yii::app()->user->checkAccess('verify', $model)) {
			throw new CHttpException(403, "Vous n'avez pas les droits nécessaires pour cette opération.");
		}
		$model->hdateVerif = $_SERVER['REQUEST_TIME'];
		if ($model->save()) {
			Yii::app()->user->setFlash('success', "La date de vérification a été mise à jour.");
		}
		$this->redirect(['view', 'id' => $model->id]);
	}

	/**
	 * Check the links of the sub-objects.
	 *
	 * @param int $id the ID of the model.
	 */
	public function actionCheckLinks($id)
	{
		$id = (int) $id;
		$model = $this->loadModel($id);
		$hasWikidata = Wikidata::isRevuePresent($id);
		if ($hasWikidata) {
			// M#4579 rafraîchissement des comparaisons WD/Mir@bel
			$WComp = new Compare(0, 0, $id);
			$WComp->compareProperties(false);
		}
		$this->render(
			'checkLinks',
			[
				'revue' => $model,
				'checks' => $model->checkLinks(),
				'hasWikidata' => $hasWikidata,
			]
		);
	}

	/**
	 * Export selected journals.
	 */
	public function actionExport($extended=false, $sfx=false)
	{
		$model = new SearchTitre(Yii::app()->user->getInstitute());
		if (!isset($_GET['q'])) {
			die("No selectors.");
		}
		$model->attributes = $_GET['q'];
		if (!$model->validate()) {
			die("Bad query.\n" . print_r($model->getErrors(), true));
		}
		$revuesIds = $model->searchTitreIds();
		if (!$revuesIds) {
			die("no result");
		}
		$partenaireId = null;
		if ($extended) {
			$partenaireId = (int) Yii::app()->user->partenaireId;
		}

		if ($sfx) {
			if (count($model->ressourceId) !== 1) {
				$this->header('text');
				echo "Le paramètre ressourceId est requis !";
			} else {
				$this->header('csv');
				Revue::exportToSfxCsv($revuesIds, $model->ressourceId[0]);
			}
		} else {
			$this->header('csv');
			header(sprintf('Content-disposition: attachment;filename=Mirabel_revues_%s.csv', date('Y-m-d')));
			$revueExporter = new models\exporters\Revue();
			if ($partenaireId > 0) {
				$revueExporter->partenaireId = $partenaireId;
			}
			$revueExporter->toCsv($revuesIds, ';');
		}
	}

	/**
	 * Export Mir@belID to Wikidata (QuickStatements and logs)
	 * @param string $target
	 */
	public function actionExportToWikidata($target)
	{
		$targets = ['quickstatements', 'log-multi-revues', 'log-multi-titres', 'log-multi-elements'];
		if (!in_array($target, $targets)) {
			die("paramètre incohérent : $target");
		}

		$WExport = new models\wikidata\Export(0);
		$out = fopen('php://output', 'w');

		switch ($target) {
			case 'quickstatements':
				$this->header('text');
				$WExport->genQuickstatements($out);
				break;
			case 'log-multi-revues':
				$this->header('csv');
				$WExport->logHeaders($out);
				$WExport->logWarning($out, 'nbrevues');
				break;
			case 'log-multi-titres':
				$this->header('csv');
				$WExport->logHeaders($out);
				$WExport->logWarning($out, 'nbtitres');
				break;
			case 'log-multi-elements':
				$this->header('csv');
				$WExport->logHeaders($out);
				$WExport->logMultielements($out);
				break;
		}
		fclose($out);
	}

	/**
	 * List categories of a model and display a form to edit these links.
	 *
	 * @param int $id the ID of the model.
	 */
	public function actionCategories($id)
	{
		$revue = $this->loadModel((int) $id);
		$form = new \models\forms\RevueCategoriesForm($revue);
		$directAccess = Yii::app()->user->checkAccess('revue/update-direct', ['Revue' => $revue]);

		if (Yii::app()->request->isPostRequest) {
			$form->setAttributes($_POST);
			$form->validate();

			$i = $form->createIntervention();
			$message = $form->applyChanges($i->contenuJson);
			if ($i->contenuJson->isEmpty()) {
				Yii::app()->user->setFlash(
					'error',
					$message . "<div>Aucune modification à effectuer.</div>"
				);
				$this->refresh();
			}
			if (!$directAccess || $form->isProposition()) {
				$i->save();
				Yii::app()->user->setFlash(
					($message ? 'warning' : 'success'),
					$message . "<div>Les affectations de thèmes ont bien été proposées "
					. " pour la revue <em>" . CHtml::encode($revue->activeTitle->titre) . "</em>.</div>"
				);
			} else {
				$i->accept();
				Yii::app()->user->setFlash(
					($message ? 'warning' : 'success'),
					$message . "<div>Les affectations de thèmes ont bien été modifiées "
					. " pour la revue <em>" . CHtml::encode($revue->activeTitle->titre) . "</em>.</div>"
				);
			}
			$this->redirect(['/revue/view', 'id' => $revue->id]);
		}
		$this->render(
			'categories',
			[
				'formData' => $form,
				'categorieLinks' => $revue->categorieLinks,
				'categoriesPubliques' => $revue->getThemes(),
				'directAccess' => $directAccess,
				'propositions' => (bool) Yii::app()->db
					->createCommand("SELECT 1 FROM Intervention WHERE revueId = :rid AND action = 'revue-I' AND hdateVal IS NULL LIMIT 1")
					->queryScalar([':rid' => $revue->id]),
			]
		);
	}

	/**
	 * Remove a Categorie from a model.
	 *
	 * @param int $id the ID of the model.
	 * @param int $categorieId Categorie_Revue ID
	 */
	public function actionRetireCategorie($id, $categorieId)
	{
		$revue = $this->loadModel((int) $id);
		$link = CategorieRevue::model()->findByPk(['revueId' => $id, 'categorieId' => $categorieId]);
		$categorie = $link->categorie;

		if ($link->delete()) {
			Yii::app()->user->setFlash(
				'success',
				"Le thème {$categorie->categorie} a été retiré de la revue {$revue->activeTitle->titre}."
			);
		} else {
			Yii::app()->user->setFlash(
				'Erreur',
				"Impossible de retirer le thème {$categorie->categorie} de la revue {$revue->activeTitle->titre} :"
				. print_r($link->getErrors(), true)
			);
		}
		$this->redirect(['categories', 'id' => $revue->id]);
	}

	/**
	 * Propose completions for a term (AJAX).
	 */
	public function actionAjaxComplete($term)
	{
		$this->header('json');
		echo json_encode(Titre::completeTerm($term, [], [], true, 'text'));
		Yii::app()->end();
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 *
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param int $id the ID of the model to be loaded
	 * @return Revue
	 */
	public function loadModel($id)
	{
		$model = Revue::model()->findByPk((int) $id);
		if ($model === null) {
			throw new CHttpException(404, "La revue demandée n'existe pas.");
		}
		return $model;
	}
}
