<?php

use models\renderers\InterventionHelper;

class PartenaireController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'actions' => ['carte', 'index', 'view'],
				'users' => ['*'],
			],
			[
				'allow',
				'actions' => ['abonnements', 'ajaxRefreshHash', 'create', 'possession', 'suivi', 'tableauDeBord', 'update'],
				'users' => ['@'], // more filters into actionUpdate() or each action
			],
			[
				'allow',
				'roles' => ['partenaire/admin'],
			],
			['deny',  // otherwise deny all users
				'users' => ['*'],
			],
		];
	}

	public function init()
	{
		if (Yii::app()->user->isGuest) {
			$this->layout = '//layouts/column1';
		}
		return parent::init();
	}

	public function actionAjaxRefreshHash($id)
	{
		$partenaire = $this->loadModel($id);
		$permModif = Yii::app()->user->checkAccess('partenaire/update', ['Partenaire' => $partenaire]);
		if (!$permModif) {
			throw new CHttpException(403, "Vous n'avez pas la permission de modifier ce partenaire.");
		}
		$partenaire->hash = Partenaire::getRandomHash();
		$this->header("text");
		if ($partenaire->save(false)) {
			echo CHtml::encode($partenaire->hash);
		} else {
			echo "[erreur]";
		}
	}

	/**
	 * Displays a particular model.
	 *
	 * @param int $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = $this->loadModel($id);
		if ($model->editeurId && !Yii::app()->user->checkAccess('suiviPartenairesEditeurs')) {
			$this->redirect($model->editeur->getSelfUrl());
		}
		if ($model->statut == 'inactif' && Yii::app()->user->isGuest) {
			throw new \DetailedHttpException(
				404,
				"Cet établissement partenaire de Mir@bel n'a pas été trouvé",
				"Consultez la " . CHtml::link("liste des partenaires", ['index']) . "."
			);
		}
		$this->render(
			'view',
			[
				'model' => $model,
				'logoImg' => $model->getLogoImg(),
				'abonnementInfo' => Yii::app()->user->isGuest ? null : new AbonnementInfo($model->id),
				'uploadedFiles' => $model->listConventionFiles(),
			]
		);
	}

	public function actionCarte($stamen = '')
	{
		if (!preg_match('/^[\w-]+$/', $stamen)) {
			$stamen = '';
		}

		//$countries = Tools::sqlToPairs("SELECT p.code, count(*) FROM Partenaire a JOIN Pays p ON a.paysId = p.id GROUP BY a.id, p.code");
		// circles per country are disabled

		$partenaires = Partenaire::model()->findAllBySql(
			"SELECT * FROM Partenaire WHERE statut = 'actif' AND latitude IS NOT NULL AND latitude <> 0"
		);
		$places = [];
		foreach ($partenaires as $p) {
			/* @var $p Partenaire */
			$geo = $p->getSelfLink() . "<br />";
			if ($p->type ==='editeur') {
				$geo .= 'Éditeur<br />';
			} elseif ($p->type ==='import') {
				$geo .= 'Import<br />';
			}
			$places[] = [
				(double) $p->latitude,
				(double) $p->longitude,
				$geo,
				$p->type,
			];
		}
		$this->render(
			'carte',
			[
				'stamen' => $stamen,
				'mapConfig' => [
					'countries' => [], // $countries,
					'countriesMax' => 0, // $countries ? max($countries) : 0,
					'markers' => $places,
					'radius' => [
						'max' => 200000, // meters
						'method' => 'linear',
					],
					'popupMessage' => new CJavaScriptExpression('function(x) { return x + " partenaires"; }'),
				],
			]
		);
	}

	/**
	 * Creates a new model.
	 *
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new Partenaire;

		$editeurPartenaire = Yii::app()->session->get('editeur-partenaire');
		if ($editeurPartenaire) {
			// creating a partenaire-editeur
			if (!Yii::app()->user->checkAccess('suiviPartenairesEditeurs')) {
				throw new CHttpException(403, "Vous n'êtes pas autorisé à créer un partenaire-éditeur.");
			}
			$model->scenario = 'partenaire-editeur';
			$model->attributes = $editeurPartenaire;
			$model->editeurId = $editeurPartenaire['editeurId'];
		} else {
			// creating a partenaire-veilleur
			if (!Yii::app()->user->checkAccess('partenaire/create')) {
				throw new CHttpException(403, "Vous n'êtes pas autorisé à créer un partenaire.");
			}
		}

		if (isset($_POST['Partenaire'])) {
			$model->attributes = $_POST['Partenaire'];
			if ($model->save()) {
				if ($editeurPartenaire) {
					Yii::app()->session->remove('editeur-partenaire');
					$this->partenaireEditeurSuivi($model);
					$partenaireLogo = dirname(Yii::app()->basePath) . sprintf('/images/logos/%03d.png', $model->id);
					$editeurLogo = dirname(Yii::app()->basePath) . sprintf('/images/editeurs/editeur-%06d.png', $model->editeurId);
					if (!file_exists($partenaireLogo) && file_exists($editeurLogo)) {
						copy($editeurLogo, $partenaireLogo);
						(new Upload(''))->resizeLogo($partenaireLogo);
					}
					if (!$model->utilisateurs) {
						return $this->partenaireEditeurCreateUser($model);
					}
				}
				Yii::app()->user->setFlash('success', "<p>Le partenaire a été créé.</p>
<ul>
	<li>Vérifiez les informations ci-dessous.</li>
	<li>L'adresse électronique de contact de ce partenaire est celle qui sera utilisée pour l'envoi des alertes de propositions de modifications.</li>
	<li>Ajoutez au moins un utilisateur principal, administrateur de ce partenaire (cf menu latéral).</li>
	<li>Tous les utilisateurs actifs seront automatiquement abonnés à la liste de diffusion mirabel_partenaires.</li>
	<li>Vous devez également ajouter le logo du partenaire (cf menu latéral).</li>
</ul>
");
				$this->redirect(['view', 'id' => $model->id]);
			}
		}

		$this->render(
			'create',
			['model' => $model]
		);
	}

	/**
	 * Updates a particular model.
	 *
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param int $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		if (!Yii::app()->user->checkAccess('partenaire/update', ['Partenaire' => $model])) {
			throw new CHttpException(403, 'Vous ne pouvez pas modifier ce partenaire.');
		}

		if (isset($_POST['Partenaire'])) {
			$oldAttributes = $model->attributes;
			$model->attributes = $_POST['Partenaire'];
			if ($model->save()) {
				$message = "Le partenaire a été modifié.";
				if (empty($oldAttributes['proxyUrl']) && $model->proxyUrl) {
					$num = Abonnement::model()->updateAll(['proxy' => 1], 'mask = 1 AND partenaireId = ' . (int) $model->id);
					if ($num) {
						$message .= " $num abonnements actuels sont configurés pour utiliser le proxy.";
					}
				}
				if ($oldAttributes['proxyUrl'] && empty($model->proxyUrl)) {
					$num = Abonnement::model()->updateAll(['proxy' => 0], 'mask = 1 AND partenaireId = ' . (int) $model->id);
					if ($num) {
						$message .= " $num abonnements actuels sont configurés pour ne plus utiliser de proxy.";
					}
				}
				Yii::app()->user->setFlash('success', $message);
				if (Yii::app()->user->partenaireId == $model->id) {
					// update the current user's session
					Yii::app()->user->setState('proxyUrl', $model->proxyUrl);
					Yii::app()->user->setState('partenaireType', $model->type);
				}
				$this->redirect(['view', 'id' => $model->id]);
			}
		}

		$this->render(
			'update',
			['model' => $model]
		);
	}

	/**
	 * Deletes a particular model.
	 *
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 *
	 * @param int $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			$partenaire = $this->loadModel($id);
			if ($partenaire->utilisateurs) {
				Yii::app()->user->setFlash('error', "Ce partenaire a des utilisateurs, la suppression est impossible.");
			} else {
				$partenaire->delete();
			}
			$this->redirect(['view', 'id' => $partenaire->id]);
		} else {
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$r = Ressource::model()->findAllByAttributes(['partenaire' => 1], ['order' => 'nom']);
		$this->render(
			'index',
			[
				'partenaires' => Partenaire::getListByType(),
				'ressources' => $r,
			]
		);
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model = new Partenaire('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Partenaire'])) {
			$model->attributes = $_GET['Partenaire'];
		}

		$this->render(
			'admin',
			['model' => $model]
		);
	}

	/**
	 * Updates the records in the table Suivi.
	 * @param int $id Partenaire.id
	 * @param string $target Among "Revue", "Ressource", "Editeur".
	 */
	public function actionSuivi($id, $target)
	{
		$model = $this->loadModel($id);
		if (!in_array($target, ['Revue', 'Ressource', 'Editeur'])) {
			throw new CHttpException(404, "L'argument 'target' n'est pas valide.");
		}
		if ($model->type === 'import') {
			throw new CHttpException(404, "Un partenaire 'Import' n'a pas de suivi.");
		}

		$suivi = new Suivi;
		$suivi->partenaireId = $model->id;
		$suivi->cible = $target;

		$isAdmin = Yii::app()->user->checkAccess('partenaire/admin', ['partenaire' => $model]);
		if (!$isAdmin && !Yii::app()->user->checkAccess('suivi', $suivi)) {
			throw new CHttpException(403, "Vous n'avez pas la permission de modifier ceci.");
		}

		if ($model->type !== 'normal' && !$isAdmin) {
			$objects = [];
			foreach ($_POST['Suivi']['cibleId'] as $id) {
				$object = $target::model()->findByPk($id);
				if ($object) {
					$objects[] = $object->nom . " ({$object->id})";
				}
			}
			if (count($objects) === 1 && $target === 'Revue' && $model->editeurId) {
				$object = $target::model()->findByPk((int) $_POST['Suivi']['cibleId'][0]);
				$sameEditeur = (boolean) Yii::app()->db
					->createCommand(
						"SELECT 1 FROM Titre t JOIN Titre_Editeur te ON te.titreId = t.id"
						. " WHERE te.editeurId = " . (int) $model->editeurId . " AND t.revueId = " . (int) $object->id
					)->queryScalar();
				if ($sameEditeur) {
					$suivi->cibleId = (int) $object->id;
					if ($suivi->save()) {
						if ($model->id == Yii::app()->user->partenaireId) {
							Yii::app()->user->setState('suivi', $model->getRightsSuivi());
						}
						Yii::app()->user->setFlash('success', "Vous suivez désormais : <em>$target</em> " . $object->getSelfLink());
						if (isset($_POST['returnUrl'])) {
							$this->redirect($_POST['returnUrl']);
						}
						$this->redirect(['suivi', 'id' => $model->id, 'target' => $target]);
					}
				}
			}
			$body = "Bonjour\nLe partenaire « {$model->fullName} » de type {$model->type} souhaite suivre les objets « {$target} » suivant :\n - "
				. join("\n - ", $objects)
				. "\n\nPage du partenaire : " . $this->createAbsoluteUrl('/partenaire/view', ['id' => $model->id]);
			$destinataires = array_filter(
				Yii::app()->db->createCommand("SELECT email FROM Utilisateur WHERE suiviPartenairesEditeurs = 1")->queryColumn()
			);
			if (!$destinataires) {
				// contact
				$destinataires = array_filter(array_map('trim', explode("\n", Config::read('contact.email'))));
			}
			$message = Mailer::newMail()
				->setSubject(Yii::app()->name . " : demande de suivi pour un partenaire {$model->type}")
				->setFrom(Config::read('email.from'))
				->setTo($destinataires)
				->setBody($body);
			Mailer::sendMail($message);
			Yii::app()->user->setFlash('info', "Suivi / $target : votre demande de suivi a été transmise aux administrateurs de Mir@bel.");
			if (isset($_POST['returnUrl'])) {
				$this->redirect($_POST['returnUrl']);
			} else {
				$this->redirect('/');
			}
		}

		if (isset($_POST['existing'])) {
			$deleted = 0;
			foreach ($_POST['existing'] as $id => $on) {
				if (!$on) {
					$deleted += Suivi::model()
						->deleteAllByAttributes(
							['partenaireId' => $model->id, 'cible' => $target, 'cibleId' => (int) $id]
						);
				}
			}
			if ($deleted) {
				Yii::app()->user->setFlash(
					'success',
					"Suivi / $target : $deleted suppression" . ($deleted > 1 ? 's' : '')
				);
			} else {
				Yii::app()->user->setFlash('info', "Suivi / $target : aucune suppression");
			}
			$this->redirect(['suivi', 'id' => $model->id, 'target' => $target]);
		} elseif (isset($_POST['Suivi'])) {
			$added = 0;
			if (isset($_POST['Suivi']['cibleId'])) {
				$countToAdd = count($_POST['Suivi']['cibleId']);
				foreach ($_POST['Suivi']['cibleId'] as $cibleId) {
					$add = clone ($suivi);
					$add->cibleId = (int) $cibleId;
					if ($add->save()) {
						$added++;
					}
				}
			} else {
				$countToAdd = 0;
			}
			if ($added && $model->id == Yii::app()->user->partenaireId) {
				Yii::app()->user->setState('suivi', $model->getRightsSuivi());
			}
			if ($added == $countToAdd) {
				Yii::app()->user->setFlash('success', "Suivi / $target : $added ajout" . ($added > 1 ? 's' : ''));
			} else {
				Yii::app()->user->setFlash('error', "Suivi / $target : certains suivis n'ont pu être ajoutés.");
			}
			if (isset($_POST['returnUrl'])) {
				$this->redirect($_POST['returnUrl']);
			}
			$this->redirect(['suivi', 'id' => $model->id, 'target' => $target]);
		}

		$existingAll = $model->getRightsSuivi();
		$existing = isset($existingAll[$target]) ?
			call_user_func([$target, 'model'])->sorted()->findAllByPk($existingAll[$target])
			: [];

		$this->render(
			'suivi',
			[
				'model' => $model,
				'target' => $target,
				'existing' => $existing,
				'suivi' => $suivi,
				'duplicates' => Suivi::findDuplicates($model->id, $target),
			]
		);
	}

	/**
	 * Updates the records a Partenaire owns.
	 */
	public function actionPossession($id)
	{
		$model = $this->loadModel($id);
		if (!$model->canOwnTitles()) {
			throw new CHttpException(404, "Un tel partenaire n'a pas de possessions dans Mir@bel.");
		}
		if (!Yii::app()->user->checkAccess('possession/update', ['Partenaire' => $model])) {
			throw new CHttpException(403, "Vous n'avez pas la permission de modifier ceci.");
		}

		$possession = new Possession;
		$possession->partenaireId = $model->id;

		if (isset($_POST['existing'])) {
			$ids = [];
			foreach ($_POST['existing'] as $tid => $on) {
				$tid = ltrim($tid, 't');
				if ($on && $tid > 0) {
					$ids[] = (int) $tid;
				}
			}
			if ($ids) {
				$deleted = Yii::app()->db
					->createCommand("DELETE FROM Partenaire_Titre WHERE partenaireId = {$model->id} AND titreId IN (" . join(',', $ids) . ")")
					->execute();
				Yii::app()->user->setFlash(
					'success',
					"Possessions : $deleted suppression" . ($deleted > 1 ? 's' : '') . " pour " . count($ids) . " désélections."
				);
			} else {
				Yii::app()->user->setFlash('info', "Possesssions : aucune suppression");
			}
			$this->redirect(['possession', 'id' => $model->id]);
		} elseif (isset($_POST['Possession'])) {
			$possession->attributes = $_POST['Possession'];
			if (empty($possession->titreId)) {
				$possession->titreId = $this->autoCompleteOffline('titreIdComplete', 'Titre');
			}
			if (empty($_POST['noAct'])) {
				$code = $possession->importAdd(false);
				Yii::app()->user->setFlash(
					($code >= Possession::IMPORT_NOTFOUND) ? 'error' : 'success',
					"Possession : " . Possession::getMsgFromCode($code)
				);
				$this->redirect(['possession', 'id' => $model->id]);
			}
		}

		$existing = null;
		$criteria = ['order' => 'titre.titre'];
		if (isset($_GET['tri'])) {
			if ($_GET['tri'] == 'bouquet') {
				$existing = Possession::groupByBouquet($id);
			} elseif ($_GET['tri'] == 'identifiant') {
				$criteria =  ['order' => 't.identifiantLocal'];
			} elseif ($_GET['tri'] == 'idnum') {
				$criteria =  ['order' => 'CAST(t.identifiantLocal AS UNSIGNED INTEGER)'];
			}
		}
		if (!$existing) {
			$existing = Possession::model()->with('titre')
				->findAllByAttributes(['partenaireId' => $id], $criteria);
		}

		$searchNavRevues = new SearchNavigation();
		$revues = [];
		$titres = [];
		foreach ($existing as $e) {
			$titres[] = ['id' => $e->titreId, 'attrs' => ['titre' => $e->titre->titre]];
			$revues[] = ['id' => $e->titre->revueId, 'attrs' => ['titrecomplet' => $e->titre->titre]];
		}
		$searchNavRevues->save(
			'partenaire/possession',
			['id' => $model->id],
			'revues',
			$revues
		);
		$searchNavTitres = new SearchNavigation();
		$searchNavTitres->save(
			'partenaire/possession',
			['id' => $model->id],
			'titres',
			$titres
		);

		$this->render(
			'possession',
			[
				'model' => $model,
				'existing' => $existing,
				'possession' => $possession,
				'bouquets' => Yii::app()->db
					->createCommand(
						"SELECT DISTINCT bouquet FROM Partenaire_Titre WHERE partenaireId = $id "
						. "ORDER BY bouquet"
					)->queryColumn(),
				'searchNavRevues' => $searchNavRevues,
				'searchNavTitres' => $searchNavTitres,
			]
		);
	}

	public function actionAbonnements($id)
	{
		$partenaire = $this->loadModel($id);
		if (!Yii::app()->user->checkAccess('abonnement/update', $partenaire)) {
			throw new CHttpException(403, "Vous n'avez pas la permission de modifier ceci.");
		}

		$abonnement = new AbonnementSearch();
		$abonnement->unsetAttributes();

		$abonnement->partenaireId = (int) $partenaire->id;
		if (isset($_GET['AbonnementSearch'])) {
			$abonnement->setAttributes($_GET['AbonnementSearch']);
		}

		//$this->layout = '//layouts/column1';
		$this->render(
			'abonnements',
			[
				'partenaire' => $partenaire,
				'abonnement' => $abonnement,
			]
		);
	}

	public function actionTableauDeBord($id)
	{
		$partenaire = $this->loadModel($id);

		$revueSearch = new RevueSearch();
		$revueSearch->setAttributes(Yii::app()->request->getParam("RevueSearch", []));
		$revueSearch->partenaireId = $partenaire->id;

		$ressourceSearch = new RessourceSearch('search');
		$ressourceSearch->setAttributes(Yii::app()->request->getParam("RessourceSearch", []));
		$ressourceSearch->partenaireId = $partenaire->id;

		$this->layout = '//layouts/column1';
		$this->render(
			'tableau-de-bord',
			[
				'partenaire' => $partenaire,
				'suiviRevues' => $revueSearch,
				'suiviRessources' => $ressourceSearch,
				'intvByCategory' => [
					'En attente' => InterventionHelper::listInterventionsSuivi($partenaire->id, 'attente'),
					'Validées' => InterventionHelper::listInterventionsSuivi($partenaire->id, 'accepté'),
				],
				'shares' => Yii::app()->db
					->createCommand("SELECT DISTINCT s1.cible FROM Suivi s1 JOIN Suivi s2 ON s1.cibleId = s2.cibleId AND s1.cible = s2.cible WHERE s1.partenaireId <> s2.partenaireId AND s1.partenaireId = :pid")
					->queryColumn([':pid' => $partenaire->id]),
			]
		);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 *
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param int $id the ID of the model to be loaded
	 * @return Partenaire
	 */
	public function loadModel($id)
	{
		$model = Partenaire::model()->findByPk((int) $id);
		if ($model === null) {
			throw new \DetailedHttpException(
				404,
				"Cet établissement partenaire de Mir@bel n'a pas été trouvé",
				"Consultez la " . CHtml::link("liste des partenaires", ['index']) . "."
			);
		}
		return $model;
	}

	protected function partenaireEditeurSuivi(Partenaire $model)
	{
		$defined = Suivi::model()->findByAttributes(['partenaireId' => $model->id, 'cible' => 'Editeur', 'cibleId' => $model->editeurId]);
		if (!$defined) {
			$suivi = new Suivi();
			$suivi->attributes = ['partenaireId' => $model->id, 'cible' => 'Editeur', 'cibleId' => $model->editeurId];
			$suivi->save(false);
		}
	}

	protected function partenaireEditeurCreateUser(Partenaire $model)
	{
		$u = new Utilisateur();
		$u->partenaireId = $model->id;
		$nom = ($model->sigle ?: $model->nom);
		$u->login = "editeur-" . $model->id;
		$u->email = $model->email;
		$u->nomComplet = "Éditeur " . $nom;
		$u->nom = "Éditeur " . $nom;
		$u->actif = true;
		$u->permPartenaire = true;
		$u->authLdap = false;
		if ($u->save()) {
			$msg = "Le partenaire a été modifié.<br />Un compte utilisateur a été créé, que vous pouvez modifier."
				. "<br />N'oubliez pas ensuite d'envoyer un courriel à l'utilisateur,"
				. " via le lien <strong>Envoyer un mot de passe</strong> du menu ci-contre."
				. '<br />Ajoutez ensuite les objets (revues/ressources) que <a href="'
				. $this->createUrl('partenaire/suivi', ['id' => $model->id, 'target' => 'Revue'])
				. '">ce partenaire va suivre</a>.';
			if ($u->partenaire->getLogoImg() === '') {
				$msg .= "<br />Il faut aussi aller sur le nouveau partenaire/éditeur pour déposer son logo.";
			}
			Yii::app()->user->setFlash('success', $msg);
			$this->redirect(['/utilisateur/view', 'id' => $u->id]);
		} else {
			Yii::app()->user->setFlash('error', "Erreur en créant le compte utilisateur associé à ce partenaire-éditeur.");
			$this->redirect(['view', 'id' => $model->id]);
		}
	}
}
