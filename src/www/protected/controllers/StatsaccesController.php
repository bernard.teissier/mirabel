<?php

/**
 * Access Statistics.
 */
class StatsaccesController extends Controller
{
	public const ROOT_DIR = '/../../statistiques/';

	public const ROOT_FILE_PREFIX = 'awstats.mirabel.sciencespo-lyon.fr';

	public const ROOT_FILE_SUFFIX = '.fr.html';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return ['accessControl'];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			['allow', 'actions' => ['liens'], 'users' => ['*']],
			['allow', 'actions' => ['index'], 'ips' => preg_split('/[\s,]+/', Config::read('server.ip'))],
			['allow', 'users' => ['@']],
			['deny', 'users' => ['*']],
		];
	}

	public function actionIndex()
	{
		$path = self::getStatsPath();
		$years = glob($path . "/*");
		$reports = [];
		foreach ($years as $yearPath) {
			if (!preg_match('#/(\d{4})$#', $yearPath, $m)) {
				continue;
			}
			$year = (int) $m[1];
			$reports[$year] = array_fill(0, 13, '-');
			$reports[$year][0] = $year;
			foreach (glob($yearPath . "/??") as $monthPath) {
				if (!preg_match('#/(\d{2})$#', $monthPath, $m)) {
					continue;
				}
				$dir = $m[1];
				if ($dir <= 0 || $dir > 12) {
					continue;
				}
				$reports[$year][(int) $dir] = CHtml::link("$year-$dir", ["/statsacces/$year-$dir"], ['target' => '_blank']);
			}
		}
		$this->render("index", ["reports" => $reports]);
	}

	public function missingAction($name)
	{
		if (preg_match('/^\d{4}-\d\d$/', $name)) {
			return $this->showMonthlyPage($name);
		}
		if (preg_match('/^(\d{4}-\d\d)-(.+)$/', $name, $m)) {
			Yii::app()->session->add('statsaccess-date', $m[1]);
			return $this->readStats($m[2]);
		}
		return $this->readStats($name);
	}

	public static function getStatsPath($date = '', $file = '')
	{
		return realpath(Yii::app()->basePath . self::ROOT_DIR)
			. ($date ? '/' . str_replace('-', '/', $date) : '')
			. ($file ? '/' . $file : '');
	}

	private function showMonthlyPage($date)
	{
		Yii::app()->session->add('statsaccess-date', $date);
		$this->redirect(['/statsacces/' . $date . '-' . self::ROOT_FILE_PREFIX . self::ROOT_FILE_SUFFIX]);
	}

	private function readStats($name)
	{
		$filename = self::getStatsPath(Yii::app()->session->get('statsaccess-date'), $name);
		if (file_exists($filename)) {
			if (Yii::app()->user->isGuest) {
				throw new CHttpException(404, "Les statistiques d'accès sont réservées aux utilisateurs authentifiés.");
			}
			readfile($filename);
		} else {
			throw new CHttpException(404, "Ce fichier des statistiques d'accès est introuvable.");
		}
	}
}
