<?php

class SudocController extends Controller
{
	/**
	 * @var string the default layout for the views.
	 */
	public $layout = '//layouts/column1';

	public function filters()
	{
		return ['accessControl'];
	}

	public function accessRules()
	{
		return [
			// allow authenticated
			['allow', 'users' => ['@']],
			// deny all users
			['deny', 'users' => ['*']],
		];
	}

	public function actionImport()
	{
		$path = Yii::getPathOfAlias('webroot') . '/public/sudoc-import';
		if (!is_dir($path)) {
			mkdir($path);
		}
		$reportFile = "";

		$model = new \models\forms\SudocImport();
		if (Yii::app()->request->isPostRequest) {
			$model->attributes = $_POST['models\forms\SudocImport'];
			$model->xlsx = CUploadedFile::getInstance($model, "xlsx");
			if ($model->validate()) {
				$sudocImport = new \models\sudoc\Import();
				$sudocImport->verbosity = 0;
				$reportFile = preg_replace('/\.xlsx/i', '_imported.xlsx', $model->xlsx->name);
				$sudocImport->file($model->xlsx->tempName, "$path/$reportFile");
			}
		}
		$this->render(
			'import',
			[
				'model' => $model,
				'reportFile' => $reportFile,
			]
		);
	}
}
