<?php

use models\forms\StatsForm;
use models\stats\Contenu;
use models\stats\Activite;

/**
 * Statistics.
 */
class StatsController extends Controller
{
	public $localhostConnection = false;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl',
			[
				'COutputCache - history, pdf',
				'duration' => Yii::app()->user->isGuest ? 86400 : (YII_DEBUG ? 0 : 3600),
				'varyByParam' => ['domain', 'name', 'partenaireId', 'filter', 'sort'],
				'varyBySession' => true,
			],
		];
	}

	public function init()
	{
		$this->localhostConnection = Tools::matchIpFilter(
			Yii::app()->request->getUserHostAddress(),
			Config::read('server.ip')
		);
		return parent::init();
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 *
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			['allow', 'actions' => ['liens'], 'users' => ['*']],
			['allow', 'actions' => ['index', 'editeurs', 'suivi'], 'ips' => preg_split('/[\s,]+/', Config::read('server.ip'))],
			['allow', 'actions' => ['activite', 'editeurs', 'history', 'index', 'liens', 'pdf'], 'users' => ['@']],
			['allow', 'actions' => ['suivi'], 'roles' => ['stats/suivi']],
			['deny', 'users' => ['*']],
		];
	}

	public function actionActivite($partenaireId)
	{
		$userData = new StatsForm();

		if (isset($_GET['filter'])) {
			$userData->attributes = $_GET['filter'];
			$userData->validate();
		} else {
			$userData->initDates();
		}

		$statsActivite = new Activite(Yii::app()->db);
		$statsActivite->partenaire = Partenaire::model()->findByPk($partenaireId);
		if (!$userData->errors) {
			$statsActivite->setInterval($userData->startTs, $userData->endTs);
		}

		if (!$statsActivite->partenaire) {
			throw new CHttpException(404, "Ce partenaire n'a pas été trouvé.");
		}
		if (!Yii::app()->user->checkAccess('stats/partenaire', ['Partenaire' => $statsActivite->partenaire])) {
			throw new CHttpException(403, "Permission refusée");
		}

		$this->render(
			'activite',
			[
				'statsActivite' => $statsActivite,
				'userData' => $userData,
			]
		);
	}

	public function actionEditeurs()
	{
		$this->render(
			'editeurs',
			[
				'liens' => Sourcelien::listFrequenciesEditeurs(),
			]
		);
	}

	public function actionSuivi()
	{
		$cmd = Yii::app()->db->createCommand(
			"SELECT
    p.id
  , p.nom
  , p.type
  , p.statut
  , u.login
  , u.derConnexion
  , SuiviRevue.verifRecente
  , SuiviRevue.verifAncienne
  , SuiviRevue.verifNon
  , SuiviRevue.revuesSuivies
  , SuiviRessource.ressourcesSuivies
FROM `Partenaire` p
  LEFT JOIN (
      SELECT partenaireId, MAX(`derConnexion`) AS maxDerConnexion FROM `Utilisateur` GROUP BY `partenaireId`
	) m ON m.partenaireId = p.id
  LEFT JOIN Utilisateur u ON u.partenaireId = p.id AND u.derConnexion = m.maxDerConnexion
  LEFT JOIN (
      SELECT
	    s.partenaireId
		, MAX(r.hdateVerif) AS verifRecente
		, MIN(IF(r.hdateVerif > 0, r.hdateVerif, NULL)) AS verifAncienne
		, COUNT(r.id) - COUNT(r.hdateVerif) AS verifNon
		, COUNT(DISTINCT s.cibleId) AS revuesSuivies
	  FROM Suivi s
	    LEFT JOIN Revue r ON r.id = s.cibleId
	  WHERE s.cible='Revue'
	  GROUP BY s.partenaireId
	) AS SuiviRevue ON SuiviRevue.partenaireId = p.id
  LEFT JOIN (
      SELECT partenaireId, COUNT(*) AS ressourcesSuivies FROM Suivi WHERE cible = 'Ressource' GROUP BY partenaireId
	) AS SuiviRessource ON SuiviRessource.partenaireId = p.id
GROUP BY p.id
"
		);
		$provider = new CSqlDataProvider(
			$cmd,
			[
				'sort' => [
					'attributes' => [
						'nom' => 'p.nom',
						'derConnexion' => 'u.derConnexion',
						'verifAncienne' => 'verifAncienne',
						'type' => "IF(p.type <> 'normal', p.type, p.statut)",
					],
					'defaultOrder' => [
						'nom' => CSort::SORT_ASC,
					],
				],
				'pagination' => false,
			]
		);
		$possessions = Tools::sqlToPairs(
			"SELECT partenaireId, COUNT(*) FROM Partenaire_Titre GROUP BY partenaireId"
		);
		$abo = Abonnement::ABONNE;
		$abonnements = Tools::sqlToPairs(
			"SELECT partenaireId, COUNT(*) FROM Abonnement WHERE mask = {$abo} GROUP BY partenaireId"
		);
		$mask = Abonnement::MASQUE;
		$masques = Tools::sqlToPairs(
			"SELECT partenaireId, COUNT(*) FROM Abonnement WHERE mask = {$mask} GROUP BY partenaireId"
		);
		$this->render(
			'suivi',
			[
				'provider' => $provider,
				'possessions' => $possessions,
				'abonnements' => $abonnements,
				'masques' => $masques,
			]
		);
	}

	public function actionIndex()
	{
		$stats = new Contenu(Yii::app()->db);
		$statsActivite = new Activite(Yii::app()->db);
		$userData = new StatsForm();

		if (isset($_GET['filter'])) {
			$userData->attributes = $_GET['filter'];
			$userData->validate();
		} else {
			$userData->initDates();
		}
		if (!$userData->errors) {
			$statsActivite->setInterval($userData->startTs, $userData->endTs);
		}

		$this->render(
			'index',
			[
				'stats' => $stats,
				'statsActivite' => $statsActivite,
				'userData' => $userData,
				'liens' => Sourcelien::listFrequenciesTitres(),
			]
		);
	}

	public function actionHistory()
	{
		$pdfs = array_map(
			'basename',
			glob(YiiBase::getPathOfAlias('application.views.stats') . '/history/*.pdf')
		);
		sort($pdfs);

		$this->render(
			'history',
			['pdfs' => $pdfs]
		);
	}

	public function actionPdf($file)
	{
		if (!preg_match('/^[A-Za-z0-9_.@-]+$/', $file)) {
			throw new CHttpException(401, 'Invalid file name');
		}
		$base = realpath(YiiBase::getPathOfAlias('application.views.stats'));
		$pdf = $base . '/history/' . $file;
		if (!file_exists($pdf)) {
			throw new CHttpException(404, 'PDF not found');
		}
		$pdf = realpath($pdf);
		if (!preg_match('/\.pdf$/', $pdf)) {
			throw new CHttpException(404, 'PDF not found or not allowed');
		}
		$this->header('pdf');
		header('Content-Disposition: inline;filename=' . $file);
		readfile($pdf);
	}

	public function actionLiens($domain = '', $name = '')
	{
		if ($domain) {
			$liensTitre = Sourcelien::listByDomain("Titre", $domain);
			$liensEditeur = Sourcelien::listByDomain("Editeur", $domain);
			$titrePage = "Dans Mir@bel, %d liens du domaine <em>" . CHtml::encode($domain) . "</em>";
		} elseif ($name) {
			$liensTitre = Sourcelien::listByName("Titre", $name);
			$liensEditeur = Sourcelien::listByName("Editeur", $name);
			$titrePage = "Dans Mir@bel, %d liens ayant le nom <em>" . CHtml::encode($name) . "</em>";
		} else {
			throw new CHttpException(400, "URL non valide. Un argument requis est absent.");
		}
		$this->render(
			'liens-et-titres',
			[
				'liensTitre' => $liensTitre,
				'liensEditeur' => $liensEditeur,
				'titrePage' => $titrePage,
			]
		);
	}
}
