<?php

use models\verifications\General;
use models\verifications\Issn;
use models\verifications\Ppn;
use models\wikidata\Compare;

class VerificationController extends Controller
{
	protected $navLinks;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return ['accessControl'];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			['allow', 'users' => ['@'], ],
			['deny',  'users' => ['*'], ],
		];
	}

	public function actionData()
	{
		$this->render(
			'data',
			['verif' => new General()]
		);
	}

	public function actionEditeurs()
	{
		$this->render(
			'editeurs',
			['verif' => new models\verifications\Editeurs()]
		);
	}

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionIssn()
	{
		$this->render(
			'issn',
			(new Issn())->getValues()
		);
	}

	public function actionPpn()
	{
		$this->render(
			'ppn',
			['ppn' => new Ppn()],
		);
	}

	public function actionTitresEditeurs()
	{
		$titresVivants = Titre::model()->findAllBySql(
			<<<EOSQL
				SELECT t.*
				FROM Titre t
				  LEFT JOIN Titre_Editeur te ON t.id = te.titreId
				WHERE te.editeurId IS NULL AND t.obsoletePar IS NULL AND t.dateFin = ''
				ORDER BY t.titre
				EOSQL
		);
		$titresMorts = Titre::model()->findAllBySql(
			<<<EOSQL
				SELECT t.*
				FROM Titre t
				  LEFT JOIN Titre_Editeur te ON t.id = te.titreId
				WHERE te.editeurId IS NULL AND (t.obsoletePar IS NOT NULL OR t.dateFin > 0)
				ORDER BY t.titre
				EOSQL
		);

		$this->render(
			'titres-editeurs',
			[
				'titresVivantsSansEditeurs' => $titresVivants,
				'titresMortsSansEditeurs' => $titresMorts,
			]
		);
	}

	public function actionLiens()
	{
		$this->render(
			'liens',
			['model' => new \models\verifications\Liens()]
		);
	}

	public function actionLiensCsv($exclut = "Operation timed out", $contient = '')
	{
		$csvFile = Yii::app()->getBasePath() . '/data/verification_liens.csv';
		if (file_exists($csvFile)) {
			$provider = new CsvDataProvider($csvFile, "\t");
			$provider->setFilter(
				function ($x) use ($exclut, $contient) {
					return isset($x[2])
						&& (empty($exclut) || strpos($x[2], $exclut) === false)
						&& (empty($contient) || strpos($x[2], $contient) !== false);
				}
			);
			$dateMaj = DateTime::createFromFormat("U", (string) filemtime($csvFile));
		} else {
			$provider = null;
			Yii::app()->user->setFlash(
				'warning',
				"Aucun fichier CSV trouvé à l'emplacement $csvFile sur le serveur. Vérifiez l'état du cron de vérification des liens."
			);
			$dateMaj = null;
		}
		$this->render(
			'liens-csv',
			['provider' => $provider, 'dateMaj' => $dateMaj, 'exclut' => $exclut, 'contient' => $contient]
		);
	}

	public function actionLiensRevues()
	{
		$this->verifLiens("revue");
	}

	public function actionLiensEditeurs()
	{
		$this->verifLiens("editeur");
	}

	public function actionLiensRessources()
	{
		$this->verifLiens("ressource");
	}

	public function actionThematique($minImports = 5)
	{
		$this->render(
			'thematique',
			[
				'minImports' => (int) $minImports,
				'revues' => Yii::app()->db->createCommand(
					<<<EOSQL
						SELECT t.revueId AS id, t.titre, COUNT(DISTINCT ca.categorieId) AS importees
						FROM Titre t
						  LEFT JOIN Categorie_Revue cr ON cr.revueId = t.revueId
						 JOIN CategorieAlias_Titre ct ON ct.titreId = t.id
						  JOIN CategorieAlias ca ON ca.id = ct.categorieAliasId
						WHERE cr.categorieId IS NULL
						GROUP BY t.revueId
						  HAVING importees >= ?
						ORDER BY importees DESC"
						EOSQL
				)->queryAll(true, [(int) $minImports]),
			]
		);
	}

	/**
	 * Manages Wikidata-cache records (index mainly)
	 */
	public function actionWikidata()
	{
		$model = new Wikidata('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Wikidata'])) {
			$model->attributes = $_GET['Wikidata'];
		}

		$WComp = new Compare(0, 0, null);
		$WComp->compareProperties(true);
		$this->render('wikidata', [
			'model' => $model,
		]);
	}

	/**
	 * Manages Wikidata bugs and inconsistencies
	 */
	public function actionWikidatabugs()
	{
		$model = new Wikidata('search');
		/**/
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Wikidata'])) {
			$model->attributes = $_GET['Wikidata'];
		}
		/**/
		$this->render('wikidatabugs', [
			'model' => $model,
		]);
	}

	private function verifLiens($name)
	{
		$nameU = ucfirst($name);
		$input = new VerifUrlForm();
		if (isset($_REQUEST['VerifUrlForm'])) {
			$input->setAttributes($_REQUEST['VerifUrlForm']);
		}
		if ($name === 'editeur') {
			$searchCmd = Yii::app()->db->createCommand()
				->select('v.*, e.nom')
				->from('VerifUrlEditeur v')
				->join("Editeur e", "e.id = v.editeurId")
				->where("v.success = 0");
			$suivi = null;
		} elseif ($name === 'revue') {
			$searchCmd = Yii::app()->db->createCommand()
				->select('v.*, t.titre')
				->from('VerifUrlRevue v')
				->join("Titre t", "t.revueId = v.revueId")
				->where("v.success = 0")
				->andWhere("t.obsoletePar IS NULL");
			$suivi = Yii::app()->db->pdoInstance
				->query("SELECT s.cibleId, IF(p.sigle='',p.nom,p.sigle) FROM Suivi s JOIN Partenaire p ON p.id = s.partenaireId WHERE s.cible = 'Revue'")
				->fetchAll(\PDO::FETCH_KEY_PAIR);
		} elseif ($name === 'ressource') {
			$searchCmd = Yii::app()->db->createCommand()
				->select('v.*, r.nom AS ressource')
				->from('VerifUrlRessource v')
				->join("Ressource r", "r.id = v.ressourceId")
				->where("v.success = 0");
			$suivi = Yii::app()->db->pdoInstance
				->query("SELECT s.cibleId, IF(p.sigle='',p.nom,p.sigle) FROM Suivi s JOIN Partenaire p ON p.id = s.partenaireId WHERE s.cible = 'Ressource'")
				->fetchAll(\PDO::FETCH_KEY_PAIR);
		} else {
			throw new Exception("Invalid parameter to verifLien()");
		}
		$searchCmd->order("v.id ASC");
		$provider = new CSqlDataProvider(
			$input->addSearchCriteria($searchCmd, $nameU),
			[
				'pagination' => false,
			]
		);
		$dateMajMax = '';
		$dateMajMin = '';
		if ($provider->getItemCount() > 0) {
			$dateMajMin = DateTime::createFromFormat(
				"!Y-m-d H:i:s",
				Yii::app()->db->createCommand("SELECT hdate FROM VerifUrl$nameU ORDER BY id ASC LIMIT 1")->queryScalar()
			);
			$dateMajMax = DateTime::createFromFormat(
				"!Y-m-d H:i:s",
				Yii::app()->db->createCommand("SELECT hdate FROM VerifUrl$nameU ORDER BY id DESC LIMIT 1")->queryScalar()
			);
		}
		$this->render(
			'liens-' . $name . 's',
			[
				'provider' => $provider,
				'dateMajMin' => $dateMajMin,
				'dateMajMax' => $dateMajMax,
				'input' => $input,
				'suivi' => $suivi,
			]
		);
	}
}
