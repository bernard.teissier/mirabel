<?php

class LogExtController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column1';

	public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'roles' => ['admin'],
			],
			['deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * Displays a particular model.
	 *
	 * @param int $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render(
			'view',
			['model' => $this->loadModel($id)]
		);
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model = new LogExt('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['LogExt'])) {
			$model->attributes = $_GET['LogExt'];
		}

		$this->render(
			'admin',
			['model' => $model]
		);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 *
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param int $id the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model = LogExt::model()->findByPk((int) $id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $model;
	}
}
