<?php

class CollectionController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			['allow', 'actions' => ['ajaxList'], 'users' => ['*']],
			[
				'allow',
				'users' => ['@'],
			],
			['deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	public function actionView($id)
	{
		$model = $this->loadModel($id);
		$this->redirect(['/ressource/view', 'id' => $model->ressourceId]);
	}

	public function actionIndex($ressourceId)
	{
		$ressource = Ressource::model()->findByPk($ressourceId);
		if (empty($ressource)) {
			throw new CHttpException(404, "La liste des ressources doit recevoir un paramètre ressourceId");
		}

		$criteria = new CDbCriteria(['order' => 'nom ASC']);
		$criteria->addColumnCondition(['ressourceId' => $ressource->id]);
		$collections = Collection::model()->findAll($criteria);

		$this->render(
			'index',
			[
				'collections' => $collections,
				'ressource' => $ressource,
			]
		);
	}

	/**
	 * Creates a new model.
	 *
	 * If creation is successful, the browser will be redirected to the ressource page.
	 */
	public function actionCreate($ressourceId)
	{
		$model = new Collection;
		$model->ressourceId = (int) $ressourceId;
		if (!$model->ressource->hasCollections() && $model->ressource->hasServices()) {
			return $this->redirect(['createTmp', 'ressourceId' => $model->ressourceId]);
		}
		if (!Yii::app()->user->checkAccess('collection/create', $model)) {
			throw new CHttpException(403, "Un non-admin ne peut pas créer une collection que dans une ressource suivie par autrui.");
		}

		if (isset($_POST['Collection'])) {
			$model->attributes = $_POST['Collection'];
			$i = $model->buildIntervention(true);
			$success = false;
			if ($model->validate()) {
				if (isset($i)) {
					$i->description = "Création de la collection « {$model->nom} »";
					$i->action = 'collection-C';
					$i->contenuJson->create($model);
					$success = $i->accept();
				} else {
					$success = $model->save(false);
				}
			}
			if ($success) {
				Yii::app()->user->setFlash('success', "Nouvelle collection « " . CHtml::encode($model->nom) . " » créée.");
				$this->redirect(['/ressource/view', 'id' => $model->ressourceId]);
			}
		}

		$this->render(
			'create',
			['model' => $model]
		);
	}

	public function actionCreateTmp($ressourceId)
	{
		$model = new Collection;
		$model->ressourceId = (int) $ressourceId;
		$model->type = Collection::TYPE_TEMPORAIRE; // par défaut
		$model->nom = "par défaut";

		if (Collection::model()->countByAttributes(['ressourceId' => $model->ressourceId]) > 0) {
			Yii::app()->user->setFlash('warning', "Cette ressource a déjà des collections, donc une collection temporaire ne peut y être ajoutée.");
			$this->redirect(['create', 'ressourceId' => $model->ressourceId]);
		}

		if (isset($_POST['Collection'])) {
			$model->attributes = $_POST['Collection'];
			if ($model->type === Collection::TYPE_TEMPORAIRE) {
				$model->visible = false;
			}
			$i = $model->buildIntervention(true);
			$i->description = "Création de la collection « {$model->nom} »";
			$i->action = 'collection-C';
			$i->contenuJson->create($model);
			if ($model->validate() && $i->accept()) {
				$model->id = $i->contenuJson->lastInsertId['Collection'];
				$numAcces = Yii::app()->db
					->createCommand("INSERT INTO Service_Collection SELECT id, {$model->id} FROM Service WHERE ressourceId = :rid")
					->execute([':rid' => $model->ressourceId]);
				$numAbos = Yii::app()->db
					->createCommand("UPDATE Abonnement SET collectionId = {$model->id}, ressourceId = NULL WHERE ressourceId = :rid")
					->execute([':rid' => $model->ressourceId]);
				Yii::app()->user->setFlash(
					'success',
					"La collection initiale « " . CHtml::encode($model->nom) . " »  de type {$model->type} est créée."
					. " $numAcces accès en ligne de la ressource y ont été liés."
					. " $numAbos abonnements y ont été migrés, avec courriels de signalement aux partenaires."
				);
				if ($model->type === Collection::TYPE_TEMPORAIRE && $numAbos > 0) {
					$partenaires = Partenaire::model()->findAllBySql(
						"SELECT p.* FROM Abonnement a JOIN Partenaire p ON a.partenaireId = p.id WHERE a.collectionId = {$model->id}"
					);
					foreach ($partenaires as $p) {
						$this->emailCollectionTemporaire($p->email, $model);
					}
				}
				$this->redirect(['create', 'ressourceId' => $model->ressourceId]);
			}
		}

		$this->render(
			'create-tmp',
			[
				'model' => $model,
				'nbAcces' => Service::model()->countByAttributes(['ressourceId' => $model->ressourceId]),
				'nbAbos' => Abonnement::model()->countByAttributes(['ressourceId' => $model->ressourceId]),
			]
		);
	}

	/**
	 * Updates a particular model.
	 *
	 * If update is successful, the browser will be redirected to the ressource page.
	 *
	 * @param int $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		if (isset($_POST['Collection'])) {
			$i = $model->buildIntervention(true);
			$old = clone $model;
			$model->attributes = $_POST['Collection'];
			if ($model->validate()) {
				$success = false;
				if (isset($i)) {
					$i->description = "Modification de la collection « {$model->nom} »";
					$i->contenuJson->update($old, $model);
					$success = $i->accept();
				} else {
					$success = $model->save(false);
				}
				if ($success) {
					Yii::app()->user->setFlash('success', "La collection a été modifiée.");
					$this->redirect(['/ressource/view', 'id' => $model->ressourceId]);
				}
			}
		}

		$this->render(
			'update',
			['model' => $model]
		);
	}

	/**
	 * Deletes a particular model.
	 *
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 *
	 * @param int $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		$services = ServiceCollection::model()->countByAttributes(['collectionId' => $model->id]);
		$servicesDeleted = Service::model()->with('titre', 'ressource')->findAllBySql(
			"SELECT s.* FROM Service s LEFT JOIN Service_Collection sc ON s.id = sc.serviceId AND sc.collectionId != :cid "
			. " WHERE sc.collectionId IS NULL AND s.ressourceId = :rid",
			[':rid' => $model->ressourceId, ':cid' => $model->id]
		);
		$numAbos = (int) Yii::app()->db
			->createCommand("SELECT count(*) FROM Abonnement WHERE collectionId = :cid")
			->queryScalar([':cid' => $model->id]);
		if (
				$numAbos == 0
				&& Yii::app()->request->isPostRequest
				&& (Yii::app()->request->getParam('confirm') > 0 || !$services)
			) {
			$success = true;
			$error = "";
			// delete orphaned services
			foreach ($servicesDeleted as $s) {
				$iS = $s->buildIntervention(true);
				$iS->action = 'service-D';
				$iS->description = "Suppression d'un accès en ligne, revue « {$s->titre->titre} »"
					. " / « {$s->ressource->nom} » pour la suppression de la collection « {$model->nom} »";
				$link = new ServiceCollection();
				$link->collectionId = $model->id;
				$link->serviceId = $s->id;
				$link->isNewRecord = false;
				$iS->contenuJson->delete($s, "Suppression de l'accès en ligne");
				$success = $iS->accept();
				if (!$success) {
					$error = "erreur de suppression d'accès en ligne: " . print_r($iS->getErrors(), true);
					break;
				}
			}
			// delete collection
			$i = $model->buildIntervention(true);
			if ($success) {
				if (isset($i)) {
					$i->description = "Suppression de la collection « {$model->nom} »";
					$i->action = 'collection-D';
					// delete the collection
					$i->contenuJson->delete(
						$model,
						"Suppression de la collection « " . $model->nom . " »"
						. ($servicesDeleted ? " après suppression des " . count($servicesDeleted) . " accès orphelins" : "")
					);
					$success = $i->accept();
					if (!$success) {
						$error = "intervention/collection : " . print_r($i->getErrors(), true);
					}
				} else {
					$success = $model->delete();
					if (!$success) {
						$error = "collection/delete : " . print_r($model->getErrors(), true);
					} else {
						Yii::app()->db
							->createCommand(
								"DELETE Service FROM Service LEFT JOIN Service_Collection ON Service.id = Service_Collection.serviceId"
								. " WHERE Service_Collection.collectionId IS NULL AND Service.ressourceId = :rid"
							)->execute([':rid' => $model->ressourceId]);
					}
				}
			}
			if (empty($_GET['ajax'])) {
				if ($success) {
					Yii::app()->user->setFlash(
						'success',
						"Collection « " . CHtml::encode($model->nom) . " » supprimée."
					);
					return $this->redirect(['/ressource/view', 'id' => $model->ressourceId]);
				}
				Yii::app()->user->setFlash(
					'error',
					"La suppression a échoué. Contactez un administrateur qui consultera les logs techniques."
				);
				Yii::log("Erreur lors de la suppression de collection : " . $error, CLogger::LEVEL_ERROR);
			} else {
				$this->header('json');
				echo json_encode(['success' => $success, 'error' => $error]);
			}
		}
		/**
		 * @todo Prévoir une liste des titres liés à cette collection via des accès ?
		 */
		$this->render(
			'delete',
			[
				'model' => $model,
				'services' => $services,
				'servicesDeleted' => count($servicesDeleted),
				'numAbos' => $numAbos,
			]
		);
	}

	/**
	 * Propose completions for a term (AJAX).
	 */
	public function actionAjaxComplete($term, $ressourceId = 0)
	{
		$this->header('json');
		echo json_encode(Collection::completeTerm($term, $ressourceId));
		Yii::app()->end();
	}

	/**
	 *JSON list of collections.
	 */
	public function actionAjaxList($ressourceId, $excludeImport = false)
	{
		$this->header('json');
		$query = Collection::model()->findAllBySql(
			"SELECT * FROM Collection WHERE ressourceId = :ressourceId AND visible = 1"
				. ($excludeImport ? " AND importee = 0" : "")
				. " ORDER BY nom ASC",
			[':ressourceId' => (int) $ressourceId]
		);
		$response = [];
		foreach ($query as $c) {
			/* @var $c Collection */
			$response[] = [
				'label' => "{$c->nom}   ({$c->type})" . ($c->importee ? " (importée)" : ""),
				'id' => $c->id,
				'hint' => $c->description,
				'imported' => (boolean) $c->importee,
				'visible' => (boolean) $c->visible,
			];
		}
		echo json_encode($response);
		Yii::app()->end();
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 *
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param int $id the ID of the model to be loaded
	 * @return Collection
	 */
	public function loadModel($id)
	{
		$model = Collection::model()->findByPk((int) $id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $model;
	}

	/**
	 * Send an e-mail with a warning about the tmp collection.
	 *
	 * @param string $email
	 * @param Collection $collection
	 * @return bool Success?
	 */
	protected function emailCollectionTemporaire($email, $collection)
	{
		$message = Mailer::newMail()
			->setSubject("[" . Yii::app()->name . "] Abonnement à une collection temporaire")
			->setFrom(Config::read('email.from'))
			->setTo($email)
			->setBcc(["mirabel_admin@listes.sciencespo-lyon.fr"])
			->setBody(
				"Bonjour,

vous étiez abonnés dans Mir@bel à une ressource qui a été migrée dans une
collection temporaire. Cette collection a vocation à disparaître
prochainement, vous devriez sans doute vous abonner à une autre
collection de cette ressource.

- Ressource {$collection->ressource->nom}
- Collection {$collection->nom}
- URL " . $this->createAbsoluteUrl('/ressource/view', ['id' => $collection->ressourceId]) . "

--
Mir@bel
"
			);
		$replyTo = Config::read('email.replyTo');
		if ($replyTo) {
			$message->setReplyTo($replyTo);
		}
		return Mailer::sendMail($message);
	}
}
