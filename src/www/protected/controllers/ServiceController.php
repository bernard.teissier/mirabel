<?php

class ServiceController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'actions' => ['contact', 'update', 'create', 'export'],
				'users' => ['*'],
			],
			[
				'allow',
				'users' => ['@'],
			],
			[
				'deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * Creates a new model.
	 *
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($revueId = null, $titreId = null)
	{
		$model = new Service;
		if (!empty($titreId)) {
			$model->titreId = $titreId;
			if (empty($revueId)) {
				$revueId = (int) $model->titre->revueId;
			}
		}

		$ressourceId = null;
		if (!empty($_POST['Service']['ressourceId'])) {
			$ressourceId = (int) $_POST['Service']['ressourceId'];
		}
		$directAccess = Yii::app()->user->checkAccess(
			'service/create-direct',
			['revueId' => (int) $revueId, 'ressourceId' => $ressourceId]
		);
		$forceProposition = false;
		if ($directAccess) {
			if (isset($_POST['force_proposition'])) {
				$forceProposition = (bool) $_POST['force_proposition'];
			} elseif (!Suivi::checkDirectAccessByIds($revueId, $ressourceId)) {
				$forceProposition = true;
			}
		}
		if ($directAccess && $forceProposition) {
			$directAccess = false;
		}

		if (isset($_POST['Service'])) {
			$collectionIds = $model->setAttributes($_POST['Service']);
			$i = $model->buildIntervention($directAccess);
			if (isset($_POST['Intervention']['email'])) {
				$i->attributes = $_POST['Intervention'];
			}
			if ($model->validate(null, false)) {
				$i->action = 'service-C';
				$i->description = "Création d'un accès en ligne revue « " . $model->titre->titre
					. " » / « {$model->ressource->nom} »";
				$i->contenuJson->create($model);
				$i->updateCollections($model, [], $collectionIds);
				$this->validateIntervention(
					$i,
					$directAccess,
					['/revue/view', 'id' => $model->titre->revueId]
				);
			}
		}
		$titres = null;
		if (!isset($i)) {
			$i = new Intervention;
		}
		if (!empty($revueId)) {
			$i->revueId = (int) $revueId;
			$i->suivi = Suivi::isTracked($i->revue) !== null;
			$titres = CHtml::listData(
				Titre::model()->findAllByAttributes(
					['revueId' => $revueId],
					['order' => '(obsoletePar IS NULL) DESC, dateDebut DESC, titre ASC']
				),
				'id',
				'fullTitleWithPerio'
			);
		}

		$this->render(
			'create',
			[
				'model' => $model,
				'direct' => $directAccess,
				'titres' => $titres,
				'intervention' => $i,
				'forceProposition' => $forceProposition,
			]
		);
	}

	/**
	 * Updates a particular model.
	 *
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param int $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		$directAccess = Yii::app()->user->checkAccess('service/update-direct', ['Service' => $model]);
		$forceProposition = false;
		if ($directAccess) {
			$this->layout = '//layouts/column2';

			if (isset($_POST['force_proposition'])) {
				$forceProposition = (bool) $_POST['force_proposition'];
			} elseif (!Suivi::checkDirectAccessByIds($model->titre->revueId, $model->ressourceId)) {
				$forceProposition = true;
			}
		}
		if ($directAccess && !empty($model->titre->revueId)) {
			$titres = CHtml::listData(
				Titre::model()->findAllByAttributes(
					['revueId' => $model->titre->revueId],
					['order' => 'dateDebut DESC, titre ASC']
				),
				'id',
				'fullTitleWithPerio'
			);
		} else {
			$titres = [];
		}
		$forbidCollectionsUpdate = Yii::app()->user->isGuest
			|| ($model->ressource->autoImport && !Yii::app()->user->checkAccess('collection/admin'));

		// switch to a proposition model
		$noProposition = $directAccess && !$forceProposition;

		if (isset($_POST['Service'])) {
			$old = clone $model;
			$collectionIds = $model->setAttributes($_POST['Service']);
			$i = $model->buildIntervention($noProposition);
			if (isset($_POST['Intervention']['email'])) {
				$i->setAttributes($_POST['Intervention']);
			}
			if (!$forbidCollectionsUpdate) {
				if (!$model->ressource->hasCollections()) {
					$collectionIds = [];
				}
				$i->updateCollections($model, $old->getCollectionIds(), $collectionIds);
			}
			$changedAttributes = array_keys(array_diff_assoc($model->getAttributes(), $old->getAttributes()));
			if ($model->validate($changedAttributes, false)) {
				if ($model->ressource->autoImport && !Yii::app()->user->checkAccess('service/update-autoimport')) {
					throw new CHttpException(403, "Seul un admin peut modifier un accès en ligne automatiquement importé.");
				}
				$i->description = "Modification d'un accès en ligne revue « " . $model->titre->titre
					. " » / « {$model->ressource->nom} »";
				$i->contenuJson->update($old, $model);
				unset($old);
				$this->validateIntervention(
					$i,
					$noProposition,
					['/revue/view', 'id' => $model->titre->revueId]
				);
			}
		}
		if (!isset($i)) {
			$i = $model->buildIntervention($directAccess);
		}

		$this->render(
			'update',
			[
				'model' => $model,
				'direct' => $directAccess,
				'intervention' => $i,
				'titres' => $titres,
				'forceProposition' => $forceProposition,
				'forbidCollectionsUpdate' => $forbidCollectionsUpdate,
			]
		);
	}

	/**
	 * Deletes a particular model.
	 *
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 *
	 * @param int $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if (!Yii::app()->request->isPostRequest) {
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}

		$service = $this->loadModel($id);
		$i = $service->buildIntervention(true);
		$i->action = 'service-D';
		$i->description = "Suppression d'un accès en ligne, revue « {$service->titre->titre} »"
			. " / « {$service->ressource->nom} »";
		$i->contenuJson->delete($service, "Suppression de l'accès en ligne");
		$i->accept();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if (!isset($_GET['ajax'])) {
			$collections = $service->collections;
			if ($collections) {
				$msgs = [];
				foreach ($collections as $c) {
					/* @var $c Collection */
					$services = $c->services;
					if ($services && count($services) === 1) {
						$msgs[] = "Suite à cette suppression d'accès, la collection {$c->nom} est sans accès.";
					}
				}
				if ($msgs) {
					Yii::app()->user->setFlash('warning', join("\n<br />", $msgs));
				}
			}
			if (isset($_POST['returnUrl'])) {
				$url = $_POST['returnUrl'];
			} else {
				$url = ['/revue/view', 'id' => $service->titre->revueId];
			}
			$this->redirect($url);
		}
	}

	/**
	 * Export services from selected journals.
	 */
	public function actionExport($format = 'csv')
	{
		// Access control
		/*
		if (Yii::app()->user->isGuest) {
			if (!$hash) {
				throw new CHttpException(403, 'Access denied to guests.');
			}
			$partenaire = Partenaire::model()->findByAttributes(array('hash' => $hash));
			if (!$partenaire) {
				throw new CHttpException(403, 'Access denied.');
			}
		}
		 */

		// criteria on Service (and Bacon exports)
		$filter = new ServiceFilter();
		$filter->setScenario('filter');
		if (isset($_GET['Service'])) {
			$filter->attributes = $_GET['Service'];
		}
		if (empty($filter->ressourceId)) {
			$filter->ressourceId = $this->autoCompleteOffline('ressourceIdComplete', 'Ressource');
		}
		if (!$filter->validate()) {
			Yii::log(print_r($filter->errors, true), 'error');
			throw new CHttpException(404, 'Invalid request. Please do not repeat this request again.');
		}

		// criteria on Titre
		$model = new SearchTitre(Yii::app()->user->getInstitute());
		if (isset($_GET['q'])) {
			$model->attributes = $_GET['q'];
		} elseif ($filter->ressourceId) {
			$model->ressourceId = [$filter->ressourceId];
		}
		if (!$model->validate()) {
			throw new CHttpException(404, "Invalid request. Bad query: " . print_r($model->errors, true));
		}
		if (array_filter($model->attributes)) {
			$revuesIds = $model->searchTitreIds();
		} else {
			$revuesIds = null;
		}

		// export
		$export = new ExportServices($revuesIds, $filter);
		if ($format === 'tsv' || $format === 'kbart' || $filter->isBacon()) {
			$format = 'tsv';
			$extraFields = !$filter->isBacon();
			$tabular = $export->exportToKbartV2($extraFields);
			if ($filter->baconLibre) {
				$filename = "MIRABEL_GLOBAL_LIBRESACCES_%s.txt";
			} elseif ($filter->baconDoaj) {
				$filename = "MIRABEL_GLOBAL_DOAJ_PARTIEL_%s.txt";
			} elseif ($filter->baconGlobal) {
				$filename = "MIRABEL_GLOBAL_TEXTEINTEGRAL_%s.txt";
			} else {
				$filename = "Mirabel_acces-en-ligne_%s.txt";
			}
			header(
				sprintf("Content-Disposition: attachment; filename=\"$filename\"", date('Y-m-d'))
			);
		} else {
			$tabular = $export->exportToCsv();
			header(
				sprintf('Content-Disposition: attachment; filename="Mirabel_acces-en-ligne_%s.csv"', date('Y-m-d'))
			);
		}
		$this->header($format);
		echo $tabular;
	}

	/**
	 * Displays the contact page (on an imported service).
	 */
	public function actionContact($id)
	{
		$service = $this->loadModel($id);
		$model = new ContactServiceForm;
		$model->subjectr = Yii::app()->name . ' — accès importé - '
			. $service->titre->titre . ' / ' . $service->ressource->nom;
		$bodyStart = "Je souhaite signaler une erreur dans cet accès en ligne, sur la page "
				. $this->createAbsoluteUrl('/revue/view', ['id' => $service->titre->revueId])
				. ".\nURL actuelle : " . $service->url;
		if (isset($_POST['ContactServiceForm'])) {
			$model->attributes = $_POST['ContactServiceForm'];
			$partenaire = Suivi::isTracked($service->ressource);
			$dest = (
				$partenaire ?
				$partenaire->email
				: Config::read('contact.email')
			);
			if (empty($_POST['nopost']) && $model->validate()) {
				$message = Mailer::newMail()
					->setSubject($model->subjectr)
					->setFrom([$model->emailr => $model->name])
					->setTo($dest)
					->setBody($bodyStart . "\n\n" . $model->body);
				if (Mailer::sendMail($message)) {
					Yii::app()->user->setFlash(
						'info',
						"Le message a été envoyé aux personnes qui suivent cette ressource dans Mir@bel. "
						. "Nous vous remercions de votre intérêt pour Mir@bel et vous répondrons "
						. "dans les meilleurs délais."
					);
					$this->redirect(['/revue/view', 'id' => $service->titre->revueId]);
				} else {
					Yii::log("Envoi raté d'email", 'error', 'service/contact');
					Yii::app()->user->setFlash(
						'error',
						'Le message n\'a pu être envoyé. Le problème a été signalé aux administrateurs.'
					);
					$this->refresh();
				}
			}
		}
		$this->render(
			'contact',
			[
				'model' => $model,
				'titre' => $service->titre,
				'bodyStart' => $bodyStart,
			]
		);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 *
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param int $id the ID of the model to be loaded
	 * @return Service
	 */
	public function loadModel($id)
	{
		$model = Service::model()->findByPk((int) $id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $model;
	}
}
