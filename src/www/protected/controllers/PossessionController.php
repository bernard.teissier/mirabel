<?php

class PossessionController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	//public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'users' => ['@'],
			],
			[
				'deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * Exports to CSV.
	 *
	 * @param int $partenaireId
	 */
	public function actionExport($partenaireId)
	{
		$partenaire = Partenaire::model()->findByPk($partenaireId);
		if (!$partenaire) {
			throw new CHttpException(404, "Ce partenaire n'existe pas.");
		}
		if (!Yii::app()->user->checkAccess('possession/update', ['Partenaire' => $partenaire])) {
			throw new CHttpException(403, "Vous n'avez pas la permission de gérer ces possessions.");
		}
		$sql = "SELECT 1, pt.bouquet, t.titre, t.id, i1.issn, i2.issn AS issne, IF(i1.issnl <> '', i1.issnl, i2.issnl) AS issnl, t.revueId, pt.identifiantLocal "
			. "FROM Partenaire_Titre pt JOIN Titre t ON pt.titreId = t.id"
			. " LEFT JOIN Issn i1 ON (i1.titreId = t.id AND i1.support = 'papier') "
			. " LEFT JOIN Issn i2 ON (i2.titreId = t.id AND i2.support = 'electronique') "
			. "WHERE pt.partenaireId = " . (int) $partenaireId
			. " ORDER BY pt.bouquet ASC, t.titre ASC";
		$cmd = Yii::app()->db->createCommand($sql);
		$this->header("csv");
		header('Content-Disposition: inline; filename=Mirabel_possessions_' . date('Y-m-d') . '.csv');
		$out = fopen('php://output', 'w');
		fputcsv($out, PossessionImport::$columns, ';');
		foreach ($cmd->queryAll(false) as $row) {
			fputcsv($out, $row, ';');
		}
	}

	public function actionExportMore($partenaireId)
	{
		$partenaire = Partenaire::model()->findByPk($partenaireId);
		if (!$partenaire) {
			throw new CHttpException(404, "Ce partenaire n'existe pas.");
		}
		if (!Yii::app()->user->checkAccess('possession/update', ['Partenaire' => $partenaire])) {
			throw new CHttpException(403, "Vous n'avez pas la permission de gérer ces possessions.");
		}

		$this->header('csv');
		header(sprintf('Content-disposition: attachment;filename=Mirabel_possessions-ext_%s.csv', date('Y-m-d')));
		$exporter = new models\exporters\Titre();
		$exporter->partenaireId = (int) $partenaire->id;
		$exporter->possessionsToCsv(";");
		exit(0);
	}

	/**
	 * Imports from CSV.
	 *
	 * @param int $partenaireId
	 */
	public function actionImport($partenaireId)
	{
		$partenaire = Partenaire::model()->findByPk($partenaireId);
		if (!$partenaire) {
			throw new CHttpException(404, "Ce partenaire n'existe pas.");
		}
		if (!Yii::app()->user->checkAccess('possession/update', ['Partenaire' => $partenaire])) {
			throw new CHttpException(403, "Vous n'avez pas la permission de gérer ces possessions.");
		}

		$model = new PossessionImport;
		$model->partenaireId = $partenaireId;

		if (isset($_POST['PossessionImport'])) {
			$model->attributes = $_POST['PossessionImport'];
			$model->importfile = CUploadedFile::getInstance($model, 'importfile');
			if ($model->validate()) {
				try {
					$model->import($model->importfile->tempName);
				} catch (Exception $e) {
					$model->addError('importfile', $e->getMessage());
				}
			}
		}

		$this->render(
			'import',
			['model' => $model, 'partenaire' => $partenaire]
		);
	}
}
