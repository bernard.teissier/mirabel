<?php

use models\forms\Login;

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return [
			'captcha' => [
				'class' => 'CaptchaSumAction',
				'backColor' => 0xFFFFFF,
			],
			// page action renders page from the DB table Cms.
			// They can be accessed via: index.php?r=site/page&p=FileName
			'page' => [
				'class' => 'CmsPageAction',
			],
		];
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		$this->backgroundL = true;
		$this->backgroundR = true;
		$this->pageTitle = "Mir@bel, le site qui facilite l'accès aux revues";

		$intvByCategory = null;
		if (!Yii::app()->user->isGuest) {
			$user = Yii::app()->user;
			$intvByCategory = Suivi::listInterventionsTrackedByUser($user);
		}
		$this->render('index', ['intvByCategory' => $intvByCategory]);
	}

	public function actionPresentation()
	{
		$this->render('presentation');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		$error = Yii::app()->errorHandler->error;
		if ($error) {
			if (Yii::app()->request->isAjaxRequest) {
				echo $error['message'];
			} else {
				if (defined('YII_TEST') && YII_TEST) {
					fprintf(STDERR, "Fatal error %s : %s\n  in '%s' l. %d\n", $error['code'], $error['message'], $error['file'], $error['line']);
				} else {
					if (!empty(Yii::app()->errorHandler->getException()->details)) {
						$error['details'] = Yii::app()->errorHandler->getException()->details;
					}
					$this->render('/system/error', $error);
				}
			}
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact($sent = 0)
	{
		$model = new ContactForm;
		if (isset($_POST['ContactForm'])) {
			$model->attributes = $_POST['ContactForm'];
			if (empty($_POST['nopost']) && $model->validate()) {
				$message = Mailer::newMail()
					->setSubject('[contact] ' . $model->sujet)
					//->setFrom(Config::read('email.from'))
					->setFrom([$model->courriel => $model->name])
					->setTo(Config::read('contact.email'))
					->setBody(
						$model->body
						. "\n--\n{$model->name} <{$model->courriel}>"
					);
				if (Mailer::sendMail($message)) {
					Yii::log(
						"Contact, envoi réussi :\nMESSAGE: " . print_r($message, true) . "\nMODEL: " . print_r($model->attributes, true) . "\nUser agent: {$_SERVER['HTTP_USER_AGENT']}",
						'warning',
						'contact'
					);
					Yii::app()->user->setFlash(
						'info',
						'Nous vous remercions de votre intérêt pour Mir@bel et vous répondrons dans les meilleurs délais.'
					);
					$this->redirect((['contact', 'sent' => 1]));
				} else {
					Yii::log("Contact, envoi raté :\nMESSAGE: " . print_r($message, true), 'error', 'contact');
					Yii::app()->user->setFlash(
						'error',
						'Le message n\'a pu être envoyé. Le problème a été signalé aux administrateurs.'
					);
					$this->refresh();
				}
			} elseif ($model->hasErrors('email')) {
				Yii::log("Contact, spam probable :\nMODEL: " . print_r($model->attributes, true), 'warning', 'contact');
				// fake success message
				Yii::app()->user->setFlash(
					'info',
					"Thank you for contacting us. S P A M ?"
				);
				$this->redirect((['contact', 'sent' => 1]));
			}
		}
		$this->render('contact', ['model' => $model, 'sent' => $sent]);
	}

	/**
	 * Global Search.
	 */
	public function actionSearch($global = null, $type = '')
	{
		$model = new SearchGlobal();
		$search = null;
		$totalCount = 0;
		if ($global !== null) {
			$model->q = trim($global);
			/*
			if (Yii::app()->user->getInstitute()) {
				$model->accesLibre = false;
			}
			 */
			if ($model->validate()) {
				if (empty($type)) {
					$search = [
						'titres' => $model->search('titres'),
						'ressources' => $model->search('ressources'),
						'editeurs' => $model->search('editeurs'),
						'hashes' => [
							'titres' => '',
							'ressources' => '',
							'editeurs' => '',
						],
					];
				} else {
					$search = [
						$type => $model->search($_GET['type']),
					];
				}

				$indexesToModelNames = [
					'titres' => 'Revue',
					'ressources' => 'Ressource',
					'editeurs' => 'Editeur',
				];
				foreach ($search as $index => $s) {
					if ($s && $index !== 'hashes') {
						try {
							$totalCount += $s->getItemCount();
						} catch (\Exception $e) {
							if (strpos($e->getMessage(), "offset out of bounds") !== false) {
								throw new CHttpException(404, "Erreur dans la recherche : navigation au-delà de la taille des résultats.");
							}
							Yii::log($e->getMessage() . "\n" . $e->getTraceAsString(), CLogger::LEVEL_ERROR);
							throw new CHttpException(500, "Erreur dans la recherche. Paramètres non valides ?");
						}
						if ($s->itemCount > 1) {
							$searchCache = new SearchNavigation();
							$search['hashes'][$index] = $searchCache->save(
								'site/search',
								['global' => $model->q],
								$indexesToModelNames[$index],
								$s->getData(),
								$s->pagination
							);
						}
					}
				}
				if ($totalCount === 1) {
					if (isset($search['titres']) && $search['titres']->itemCount == 1) {
						$data = $search['titres']->getData();
						$first = reset($data);
						$this->redirect(['revue/view', 'id' => $first['attrs']['revueid']]);
					} elseif (isset($search['ressources']) && $search['ressources']->itemCount == 1) {
						$data = array_keys($search['ressources']->getData());
						$this->redirect(['ressource/view', 'id' => $data[0]]);
					} else {
						$data = array_keys($search['editeurs']->getData());
						$this->redirect(['editeur/view', 'id' => $data[0]]);
					}
				}
			}
		}

		$this->render(
			'search',
			['model' => $model, 'search' => $search, 'totalCount' => $totalCount]
		);
	}

	/**
	 * Web service for koha.
	 */
	public function actionService()
	{
		if (!getenv('REDIRECT_WEBSERVICE')) {
			throw new CHttpException(403, "Accès restreint par IP.");
		}
		$this->header('xml');
		$model = new WebService();
		$model->attributes = $_GET;
		$output = $model->run();
		echo $output;
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		if (!Yii::app()->user->isGuest) {
			$this->redirect(Yii::app()->user->getReturnUrl());
		}

		$model = new Login;

		// if it is ajax validation request
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if (isset($_POST['LoginForm'])) {
			$model->attributes = $_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if ($model->validate() && $model->login()) {
				if ($model->getUtilisateur()->getLoginMessages()) {
					// Yii1 looses flash messages upon login, so one more redirection
					Yii::app()->user->setReturnUrl(Yii::app()->user->getReturnUrl());
					$this->redirect(['logged']);
				}
				if (Yii::app()->user->checkAccess('indexation')) {
					$this->redirect(['categorie/candidates']);
				} else {
					$this->redirect(Yii::app()->user->getReturnUrl());
				}
			}
		} elseif (
			!Yii::app()->user->getState('__returnUrl') && Yii::app()->request->urlReferrer
			&& strpos(Yii::app()->request->urlReferrer, Yii::app()->getBaseUrl(true)) === 0
		) {
			Yii::app()->user->setReturnUrl(Yii::app()->request->urlReferrer);
		}
		// display the login form
		$this->render('login', ['model' => $model]);
	}

	public function actionLogged()
	{
		$messages = Utilisateur::model()->findByPk(Yii::app()->user->id)->getLoginMessages();
		if ($messages) {
			Yii::app()->user->setFlash('info', $messages);
		}
		$this->redirect(Yii::app()->user->getReturnUrl());
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}
