<?php

class EditeurController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	//public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'actions' => ['ajaxComplete', 'carte', 'create', 'export', 'idref', 'index', 'pays', 'search', 'update', 'view'],
				'users' => ['*'],
			],
			[
				'allow',
				'actions' => ['admin', 'checkLinks', 'delete', 'logo', 'verify'],
				'users' => ['@'],
			],
			[
				'allow',
				'actions' => ['createPartenaire'],
				'roles' => ['suiviPartenairesEditeurs'],
			],
			[
				'deny',  // otherwise deny all users
				'users' => ['*'],
			],
		];
	}

	public function beforeRender($view)
	{
		$directCreation = Yii::app()->user->checkAccess('editeur/create-direct');
		if (empty($_GET['id']) && !$directCreation) {
			if ($this->getAction()->id !== 'search') {
				$this->layout = '//layouts/column1';
			}
			return parent::beforeRender($view);
		}
		$this->menu = [
			['label' => 'Liste', 'url' => ['index']],
			['label' => 'Carte', 'url' => ['carte']],
			[
				'label' => 'Nouvel éditeur',
				'url' => ['create'],
				'visible' => $directCreation,
			],
		];
		if (!empty($_GET['id'])) {
			$id = (int) $_GET['id'];
			$model = $this->loadModel($id);
			$partenaire = $model->partenaire;
			array_push(
				$this->menu,
				['label' => 'Cet éditeur', 'itemOptions' => ['class' => 'nav-header']],
				['label' => 'Vérifier les liens', 'url' => ['checkLinks', 'id' => $model->id]],
				[
					'label' => 'Modifier',
					'url' => ['update', 'id' => $model->id],
					'visible' => true, // direct or to-be-validated update
				],
				[
					'label' => 'Logo',
					'url' => ['logo', 'id' => $model->id],
					'visible' => $model->partenaire !== null && Yii::app()->user->checkAccess('editeur/logo', ['Editeur' => $model]),
				],
				[
					'label' => HtmlHelper::postButton(
						"Supprimer",
						['/editeur/delete', 'id' => $model->id],
						[],
						['class' => "btn btn-link", 'style' => "padding: 0", 'onclick' => 'return confirm("Êtes-vous certain de vouloir supprimer cet éditeur ?")']
					),
					'encodeLabel' => false,
					'visible' => Yii::app()->user->checkAccess('editeur/delete', ['Editeur' => $model]),
				]
			);
			if (empty($partenaire)) {
				$this->menu[] = [
					'encodeLabel' => false,
					'label' => HtmlHelper::postButton(
						"Créer un partenariat",
						['createPartenaire', 'id' => $model->id],
						[],
						['class' => 'btn btn-link', 'style' => "padding: 0; white-space: normal;"]
					),
					'visible' => Yii::app()->user->checkAccess('suiviPartenairesEditeurs'),
				];
			} elseif ($partenaire->statut === 'actif') {
				$this->menu[] = ['label' => 'Partenariat', 'itemOptions' => ['class' => 'nav-header']];
				$this->menu[] = [
					'encodeLabel' => false,
					'label' => CHtml::link("partenaire", ['partenaire/view', 'id' => $partenaire->id]),
				];
				if ($partenaire->utilisateurs) {
					$this->menu[] = [
						'encodeLabel' => false,
						'label' => CHtml::link("utilisateur", ['utilisateur/view', 'id' => $partenaire->utilisateurs[0]->id]),
					];
				}
			} else {
				$this->menu[] = ['label' => 'Partenariat (inactif)', 'itemOptions' => ['class' => 'nav-header']];
				$this->menu[] = [
					'encodeLabel' => false,
					'label' => CHtml::link("Réactiver", ['partenaire/update', 'id' => $partenaire->id]),
				];
			}
			if (Yii::app()->user->checkAccess('verify', $model)) {
				$this->menu[] = ['label' => 'Vérification', 'itemOptions' => ['class' => 'nav-header']];
				$this->menu[] = [
					'encodeLabel' => false,
					'label' => HtmlHelper::postButton(
						"Indiquer que l'éditeur a été vérifié",
						['verify', 'id' => $model->id],
						[],
						['class' => 'btn btn-primary btn-small', 'style' => "white-space: normal;"]
					),
				];
			}
		}
		return parent::beforeRender($view);
	}

	public function actionCreatePartenaire($id)
	{
		$model = $this->loadModel($id);
		if ($model->partenaire) {
			throw new CHttpException(500, "Cet éditeur a déjà le statut d'éditeur-partenaire.");
		}
		if ($model->statut !== 'normal') {
			throw new CHttpException(500, "Cet éditeur n'a pas le statut <i>normal</i>.");
		}
		$partenaire = new Partenaire();
		$partenaire->editeurId = $model->id;
		$partenaire->type = 'editeur';
		$partenaire->statut = 'actif';
		foreach (['nom', 'prefixe', 'sigle', 'description', 'url', 'paysId', 'geo'] as $attr) {
			$partenaire->{$attr} = $model->{$attr};
		}
		if (strpos($partenaire->nom, '=') > 0) {
			$partenaire->nom = trim(strtok($partenaire->nom, '='));
		}
		Yii::app()->user->setFlash(
			'success',
			"Le partenaire associé à cet éditeur est prêt à être enregistré dans la base. Vérifier et compléter (champ Courriel, etc)."
			. "<br />Un compte utilisateur sera ensuite créé automatiquement."
		);
		Yii::app()->session->add('editeur-partenaire', $partenaire->attributes);
		$this->redirect(['/partenaire/create']);
	}

	public function actionIdref($idref)
	{
		$model = Editeur::model()->findByAttributes(['idref' => $idref]);
		if ($model === null) {
			throw new CHttpException(404, "Aucun éditeur dans Mir@bel n'a cet IdRef.");
		}
		$this->redirect($model->getSelfUrl());
	}

	public function actionView($id, $nom = '')
	{
		$model = $this->loadModel($id);
		$encName = $model->getSelfUrl()['nom'];
		if (!$nom || $nom !== $encName) {
			$this->redirect($model->getSelfUrl());
		}

		$this->appendToHtmlHead(
			Controller::linkEmbeddedFeed('rss2', "Éditeur " . $model->nom, ['editeur' => $model->id])
		);
		if (Yii::app()->session->contains('force-refresh')) {
			Yii::app()->session->remove('force-refresh');
			$forceRefresh = true;
		} else {
			$forceRefresh = false;
		}
		$this->render(
			'view',
			[
				'model' => $model,
				'searchNavigation' => $this->loadSearchNavigation(),
				'forceRefresh' => $forceRefresh,
			]
		);
	}

	public function actionPays($paysId, $paysNom = '')
	{
		$pays = Pays::model()->find("code = :c OR id = :i OR nom = :n", [':c' => $paysId, ':i' => $paysId, ':n' => $paysId]);
		if (!$pays) {
			throw new CHttpException(404, "Pays inconnu");
		}
		$nomEnc = Norm::urlParam($pays->nom);
		if ($paysNom !== $nomEnc) {
			$url = ['pays', 'paysId' => $pays->code, 'paysNom' => $nomEnc];
			return $this->redirect($url);
		}

		$model = new \models\searches\EditeurSearch;
		$model->pays = $pays->id;
		$this->render(
			'pays',
			[
				'pays' => $pays,
				'model' => $model,
			]
		);
	}

	public function actionCarte($stamen = '')
	{
		if (!preg_match('/^[\w-]+$/', $stamen)) {
			$stamen = '';
		}
		$countries = [];
		$max = 0;
		$query = Yii::app()->db->createCommand(
			"SELECT p.code, p.id, p.nom, count(distinct e.id) AS editeurs, count(distinct t.revueId) AS revues"
			. " FROM Editeur e JOIN Pays p ON e.paysId = p.id"
			. " LEFT JOIN Titre_Editeur te ON te.editeurId = e.id LEFT JOIN Titre t ON te.titreId = t.id"
			. " GROUP BY p.id, p.code"
		);
		foreach ($query->query() as $row) {
			if ($max < $row['editeurs']) {
				$max = $row['editeurs'];
			}
			$countries[$row['code']] = [
				$row['editeurs'],
				CHtml::tag('div', [], CHtml::encode($row['nom']))
				. CHtml::tag(
					'strong',
					[],
					CHtml::link(
						$row['editeurs'] . " éditeur" . ($row['editeurs'] > 1 ? "s" : ""),
						['/editeur/pays', 'paysId' => $row['code'], 'paysNom' => $row['nom']]
					)
				)
				. "<br /> ⇔ "
				. CHtml::link(
					$row['revues'] . " revue" . ($row['revues'] > 1 ? "s" : ""),
					['/revue/search', 'q' => ['paysId' => $row['id']]]
				),
			];
		}
		$this->render(
			'carte',
			[
				'stamen' => $stamen,
				'mapConfig' => [
					'countries' => $countries,
					'countriesMax' => $max,
					'radius' => [
						'max' => 12, // pixels
						'method' => 'logarithmic',
					],
					'popupMessage' => new CJavaScriptExpression('function(x) { return x[1]; }'),
				],
			]
		);
	}

	/**
	 * Creates a new model.
	 *
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new Editeur;
		$directAccess = Yii::app()->user->checkAccess('editeur/create-direct');

		if (isset($_POST['Editeur'])) {
			$i = $model->buildIntervention($directAccess);
			if (isset($_POST['Intervention']['email'])) {
				$i->attributes = $_POST['Intervention'];
			}
			$model->attributes = $_POST['Editeur'];
			$model->statut = 'normal'; /// @TODO statut 'attente' parfois ?
			if ($model->validate()) {
				$i->description = "Création de l'éditeur « {$model->nom} »";
				$i->contenuJson->create($model);
				$i->action = 'editeur-C';
				if (Yii::app()->getRequest()->isAjaxRequest) {
					// on AJAX, always save, ignoring $directAccess
					if ($this->validateIntervention($i, true)) {
						$i->editeurId = $i->contenuJson->lastInsertId['Editeur'];
						$i->save(false);
						$i->emailForEditorChange();
						$this->header('json');
						echo json_encode(
							[
								'id' => $i->editeurId,
								'result' => 'success',
								'label' => CHtml::encode($model->getFullName()),
								'message' => "L'éditeur <em>" . CHtml::encode($model->getFullName())
									. "</em> a été créé.",
							]
						);
						Yii::app()->end();
					}
				} elseif ($this->validateIntervention($i, $directAccess)) {
					if ($directAccess) {
						$i->editeurId = (int) $i->contenuJson->lastInsertId['Editeur'];
						$i->save(false);
						$i->emailForEditorChange();
						$model->id = $i->editeurId;
						$this->redirect($model->getSelfUrl());
					} else {
						$i->emailForEditorChange($model->nom, "Proposition");
						$this->redirect(['/editeur/index']);
					}
				}
			} elseif (Yii::app()->getRequest()->isAjaxRequest) {
				$this->header('json');
				echo json_encode(
					[
						'result' => 'error',
						'html' => $this->renderPartial(
							'_form',
							['model' => $model, 'embedded' => true, 'intervention' => null],
							true
						),
					]
				);
				Yii::app()->end();
			}
		}
		if (!isset($i)) {
			$i = new Intervention;
		}

		$this->render(
			'create',
			['model' => $model, 'direct' => $directAccess, 'intervention' => $i]
		);
	}

	/**
	 * Updates a particular model.
	 *
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param int $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		$directAccess = Yii::app()->user->checkAccess('editeur/update-direct', ['Editeur' => $model]);
		if ($directAccess && !empty($_POST['force_proposition'])) {
			$directAccess = false;
		}
		$i = $model->buildIntervention($directAccess);

		if (isset($_POST['Editeur'])) {
			$old = clone $model;
			$model->attributes = $_POST['Editeur'];
			if (isset($_POST['Intervention']['email'])) {
				$i->attributes = $_POST['Intervention'];
			}
			$changedAttributes = array_keys(array_diff_assoc($model->getAttributes(), $old->getAttributes()));
			if ($model->validate($changedAttributes)) {
				$i->description = "Modification de l'éditeur « {$model->nom} »";
				$i->action = 'editeur-U';
				$i->contenuJson->update($old, $model);
				unset($old);
				$this->validateIntervention($i, $directAccess, $model->getSelfUrl());
			}
		}

		$this->render(
			'update',
			['model' => $model, 'direct' => $directAccess, 'intervention' => $i]
		);
	}

	/**
	 * Check the links of the sub-objects.
	 *
	 * @param int $id the ID of the model.
	 */
	public function actionCheckLinks($id)
	{
		$id = (int) $id;
		$model = $this->loadModel($id);
		$this->render(
			'checkLinks',
			['editeur' => $model, 'checks' => $model->checkLinks()]
		);
	}

	/**
	 * Updates a particular model.
	 *
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param int $id the ID of the model to be updated
	 */
	public function actionLogo($id)
	{
		$model = $this->loadModel($id);
		if (!Yii::app()->user->checkAccess('editeur/logo', ['Editeur' => $model])) {
			Yii::app()->user->setFlash('warning', "L'accès à cette page est réservé.");
			$this->redirect($model->getSelfUrl());
		}

		$imageTool = new ImageDownloader(Yii::getPathOfAlias('webroot') . '/images/editeurs');
		$imageTool->filePattern = 'editeur-%06d';
		$imageTool->boundingbox = '225x55';

		$upload = new Upload(new UploadDestination([
			'name' => 'Éditeurs - Logo',
			'path' => dirname(Yii::app()->basePath) . '/images/editeurs',
			'url' => Yii::app()->getBaseUrl(true) . '/images/editeurs/',
			//'forceDestName' =>
		]));
		$upload->overwrite = true;
		$newImage = false;
		$filename = $imageTool->getRawImageFileName($model->id);

		// remove the logo
		if (!empty($_POST['delete'])) {
			$imageTool->delete($model->id);
			Yii::app()->user->setFlash('success', "Le logo de l'éditeur a été supprimé.");
			Yii::app()->session->add('force-refresh', true);
			$this->redirect(['editeur/view', 'id' => $model->id]);
		}

		if (!empty($_POST['Editeur'])) {
			$interv = $model->buildIntervention(true);
			$interv->action = 'editeur-U';
			$interv->contenuJson->updateByAttributes($model, ['logoUrl' => $_POST['Editeur']['logoUrl']]);
			if ($interv->accept()) {
				$model->logoUrl = $_POST['Editeur']['logoUrl'];
				if ($model->logoUrl) {
					if ($imageTool->download($model->id, $model->logoUrl)) {
						$newImage = true;
					}
				} else {
					$imageTool->delete($model->id);
				}
			}
		}

		if (!empty($_POST['Upload'])) {
			$upload->attributes = $_POST['Upload'];
			$upload->file = CUploadedFile::getInstance($upload, 'file');
			if ($upload->file) {
				if ($upload->file->saveAs($filename)) {
					system("convert \"$filename\" \"$filename.png\"");
					unlink($filename);
					$newImage = true;
					// M#3422 remove the logoUrl when a upload superseeds it
					$model->logoUrl = '';
					$model->save(false);
				} else {
					Yii::app()->user->setFlash('error', "Erreur lors de la copie de ce fichier sur le serveur.");
				}
			} else {
				Yii::app()->user->setFlash('error', "Erreur lors du dépôt de ce fichier.");
			}
		}

		if ($newImage) {
			if (!empty($model->partenaire)) {
				$partenaireLogo = dirname(Yii::app()->basePath) . '/images/logos/' . sprintf('%03d.png', $model->partenaire->id);
				$filenameWithExt = glob($filename . ".*")[0];
				if ($filenameWithExt) {
					system("convert \"$filenameWithExt\" \"$partenaireLogo\"");
					$upload->resizeLogo($partenaireLogo);
				} else {
					Yii::app()->user->setFlash('warning', "Erreur lors de la copie du logo vers le partenaire-éditeur.");
				}
			}
			if ($imageTool->resizeByIdentifier($model->id)) {
				Yii::app()->user->setFlash('success', "Le logo a été mis à jour" . ($model->partenaire ? " pour l'éditeur et le partenaire associé." : "."));
				Yii::app()->session->add('force-refresh', true);
				$this->redirect($model->getSelfUrl());
			} else {
				Yii::app()->user->setFlash('error', "Erreur lors du redimensionnement de l'image.");
			}
		}

		$this->render(
			'logo',
			['model' => $model, 'upload' => $upload]
		);
	}

	/**
	 * Deletes a particular model.
	 *
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 *
	 * @param int $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$editeur = $this->loadModel($id);
		if (!Yii::app()->user->checkAccess('editeur/delete', ['Editeur' => $editeur])) {
			throw new CHttpException(403, "Vous n'avez pas accès à la suppression d'éditeurs.");
		}

		$hasIntervention = Yii::app()->db
			->createCommand("SELECT 1 FROM Intervention WHERE editeurId = ? AND statut = 'attente'")
			->queryScalar([(int) $id]);
		if ($hasIntervention) {
			Yii::app()->user->setFlash('error', "Suppression impossible car au moins une intervention est en attente sur cet éditeur.");
			$this->redirect($editeur->getSelfUrl());
		}

		$references = Yii::app()->db
			->createCommand("SELECT count(*) FROM Titre_Editeur WHERE editeurId = ?")
			->queryScalar([(int) $id]);
		if ($references) {
			Yii::app()->user->setFlash('error', "Suppression impossible car $references titres sont associés à cet éditeur.");
			$this->redirect($editeur->getSelfUrl());
		}

		// we only allow deletion via POST request
		if (!Yii::app()->request->isPostRequest) {
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}

		$name = $editeur->nom;
		$i = $editeur->buildIntervention(true);
		$i->description = "Suppression de l'éditeur « {$editeur->nom} »";
		$i->contenuJson->delete($editeur, "Suppression de l'éditeur");
		$i->action = 'editeur-D';
		if ($i->validate()) {
			require_once Yii::app()->getBasePath() . '/components/FeedGenerator.php';
			FeedGenerator::saveFeedsToFile('Editeur', (int) $this->id);
		}
		$success = $i->accept(false);
		$i->editeurId = null;
		if ($success) {
			$i->save(false);
			$i->emailForEditorChange($name);
		}

		// if AJAX request, we should not redirect the browser
		if (!isset($_GET['ajax'])) {
			$returnUrl = $_POST['returnUrl'] ?? ['index'];
			if ($success) {
				Yii::app()->user->setFlash('success', "Éditeur « {$editeur->nom} » supprimé.");
			} else {
				$returnUrl = $editeur->getSelfUrl();
				$errors = $editeur->getErrors();
				if ($i->getErrors()) {
					foreach ($i->getErrors() as $k => $v) {
						$errors[$k] = $v[0];
					}
				}
				$msg = "<ul><li>" . join("</li><li>", $errors) . "</li></ul>";
				Yii::app()->user->setFlash('error', "Suppression de « {$editeur->nom} » impossible :" . $msg);
			}
			$this->redirect($returnUrl);
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($lettre = 'A')
	{
		$dataProvider = Editeur::listAlpha($lettre);
		$searchCache = new SearchNavigation();
		$searchHash = $searchCache->save('editeur/index', ['lettre' => $lettre], 'SearchEditeur', $dataProvider->getData());
		$this->render(
			'index',
			[
				'dataProvider' => $dataProvider,
				'lettre' => $lettre,
				'suivi' => !empty($_GET['suivi']),
				'searchHash' => $searchHash,
				'totalEditeurs' => Editeur::model()->count("statut = 'normal'"),
			]
		);
	}

	public function actionExport(array $q)
	{
		$model = new \models\searches\EditeurSearch;
		$model->setAttributes($q);
		$provider = $model->search(false);
		$provider->criteria->limit = 10000;
		$provider->criteria->offset = 0;
	
		$this->header('csv');
		header('Content-Disposition: inline; filename=Mirabel_editeurs_' . date('Y-m-d') . '.csv');
		$out = fopen('php://output', 'w');
		fputcsv(
			$out,
			[
				"id M",
				"préfixe",
				"nom",
				"sigle",
				"id IdRef",
				"id Sherpa",
				"pays",
				"pays alpha2",
				"pays alpha3",
				"nb revues dans M",
				"nb titres vivants dans M",
				"date de vérification",
			],
			";"
		);
		foreach ($provider->getData() as $data) {
			/** @var models\sphinx\Editeurs $data */
			$record = $data->getRecord();
			if (!$record) {
				continue;
			}
			$pays = $record->pays;
			fputcsv(
				$out,
				[
					(int) $data->id,
					$record->prefixe,
					$record->nom,
					$record->sigle,
					$record->idref,
					$record->sherpa,
					$pays ? $pays->nom : '',
					$pays ? $pays->code2 : '',
					$pays ? $pays->code : '',
					(int) $data->nbrevues,
					(int) $data->nbtitresvivants,
					$record->hdateVerif ? date("Y-m-d", $record->hdateVerif) : "",
				],
				";"
			);
		}
	}

	public function actionSearch()
	{
		$model = new \models\searches\EditeurSearch;
		if (isset($_GET['q'])) {
			$model->setAttributes($_GET['q']);
		}
		$this->layout = $model->isEmpty() ? '//layouts/column1' : '//layouts/column2';

		$this->render(
			'search',
			['model' => $model]
		);
	}

	/**
	 * Marks as verified.
	 *
	 * @param int $id the ID of the model to be marked.
	 */
	public function actionVerify($id)
	{
		$model = $this->loadModel((int) $id);
		if (!Yii::app()->user->checkAccess('verify', $model)) {
			throw new CHttpException(403, "Vous n'avez pas les droits nécessaires pour cette opération.");
		}
		$model->hdateVerif = $_SERVER['REQUEST_TIME'];
		if ($model->save()) {
			Yii::app()->user->setFlash('success', "La date de vérification a été mise à jour.");
		}
		$this->redirect($model->getSelfUrl());
	}

	/**
	 * Propose completions for a term (AJAX).
	 */
	public function actionAjaxComplete(string $term)
	{
		$excludes = [];
		if (!empty($_GET['excludeIds'])) {
			$excludes = ['id' => $_GET['excludeIds']];
		}
		$this->header('json');
		echo json_encode(Editeur::completeTerm($term, [], $excludes));
		Yii::app()->end();
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 *
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param int $id the ID of the model to be loaded
	 * @return Editeur
	 */
	public function loadModel($id)
	{
		$model = Editeur::model()->findByPk((int) $id);
		if ($model === null) {
			throw new CHttpException(404, "L'éditeur demandé n'existe pas.");
		}
		return $model;
	}
}
