<?php

class VocabulaireController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	//public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'roles' => ['indexation'],
			],
			[
				'deny',  // deny all users by default
				'users' => ['*'],
			],
		];
	}

	public function beforeRender($view)
	{
		$this->menu = [
			['label' => "Thématique", 'itemOptions' => ['class' => 'nav-header']],
			['label' => 'Arbre thématique', 'url' => ['/categorie/index']],
			['label' => 'Administrer des thèmes', 'url' => ['/categorie/admin']],
			['label' => 'Nouveau thème', 'url' => ['/categorie/create']],
			['label' => 'Thèmes refusés', 'url' => ['/categorie/indexRefus']],
			['label' => "Import d'indexation", 'url' => ['/categorie/importIndexation'],
				'linkOptions' => ['title' => "Charger un CSV pour décrire des titres dans un vocabulaire, et affecter ainsi des thématiques aux revues."], ],
			['label' => "Vocabulaires", 'itemOptions' => ['class' => 'nav-header']],
			['label' => 'Liste', 'url' => ['/vocabulaire/index'],
				'linkOptions' => ['title' => "Liste des vocabulaires."], ],
			['label' => 'Exporter les vocabulaires', 'url' => ['/vocabulaire/export'],
				'linkOptions' => ['title' => "Télécharger un CSV listant les thèmes et leurs éventuels alias dans chaque vocabulaire."], ],
			['label' => 'Importer des vocabulaires', 'url' => ['/vocabulaire/prepareImport'],
				'linkOptions' => ['title' => "Charger un CSV pour ajouter du contenu aux vocabulaires."], ],
		];
		return parent::beforeRender($view);
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider = new CActiveDataProvider('Vocabulaire');
		$this->render(
			'index',
			['dataProvider' => $dataProvider]
		);
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		if (isset($_POST['Vocabulaire'])) {
			$model->attributes = $_POST['Vocabulaire'];
			if ($model->save()) {
				Yii::app()->user->setFlash('success', "Vocabulaire modifié.");
				$this->redirect(['index']);
			}
		}
		$cmd = Yii::app()->db->createCommand(
			"SELECT c.categorie, a.alias, a.id FROM CategorieAlias a JOIN Categorie c ON c.id = a.categorieId "
			. " WHERE a.vocabulaireId = {$model->id} ORDER BY c.categorie"
		);

		$this->render(
			'update',
			[
				'model' => $model,
				'categoriesEtAlias' => $cmd->queryAll(false),
			]
		);
	}

	public function actionExport()
	{
		$this->header('csv');
		echo Vocabulaire::exportToCsv(";");
	}

	public function actionPrepareImport()
	{
		if (Yii::app()->request->isPostRequest && isset($_FILES['csvfile']['name'])) {
			if (!preg_match('/\.csv$/', $_FILES['csvfile']['name'])) {
				Yii::app()->user->setFlash('error', "Erreur, le fichier n'a pas l'extension .csv.");
			} else {
				$tmpfile = tempnam(Yii::app()->runtimePath, 'up__');
				if (move_uploaded_file($_FILES['csvfile']['tmp_name'], $tmpfile)) {
					Yii::app()->user->setFlash('success', "Le fichier CSV a été chargé.");
					Yii::app()->user->setState('csv-file', $tmpfile);
					Yii::app()->user->setState('csv-separator', $_POST['separator']);
					$this->redirect(['import']);
				} else {
					Yii::app()->user->setFlash('error', "Erreur, l'envoi du fichier a échoué.");
				}
			}
		}
		$this->render('prepareImport');
	}

	public function actionImport()
	{
		$import = new CategorieAliasImport();
		$csv = Yii::app()->user->getState('csv-file');
		$separator = Yii::app()->user->getState('csv-separator');
		if (!$csv) {
			Yii::app()->user->setFlash('error', "Erreur, pas d'import en cours (session expirée ?)");
			$this->redirect(['loadAlias']);
		}
		$import->loadCsvFile($csv, $separator);
		if (Yii::app()->request->isPostRequest) {
			// Import into Vocabulaire and CategorieAlias
			$import->attributes = $_POST['CategorieAliasImport'];
			if ($import->validate()) {
				if ($import->import()) {
					Yii::app()->user->setFlash('success', $import->getImportReport());
					$this->redirect('index');
				} else {
					Yii::app()->user->setFlash('error', $import->getImportReport());
				}
			}
		}
		$this->render(
			'import',
			['importAlias' => $import]
		);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 *
	 * If the data model is not found, a 404 HTTP exception will be raised.
	 *
	 * @param int $id the ID of the model to be loaded
	 * @return Vocabulaire
	 */
	private function loadModel($id)
	{
		$model = Vocabulaire::model()->findByPk((int) $id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $model;
	}
}
