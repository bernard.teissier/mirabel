<?php

class HintController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column1';

	//public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'roles' => ['redaction/aide'],
			],
			['deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * Creates a new model.
	 *
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new Hint;

		if (isset($_POST['Hint'])) {
			$model->attributes = $_POST['Hint'];
			if ($model->save()) {
				$this->redirect(['admin']);
			}
		}

		$this->render(
			'create',
			['model' => $model]
		);
	}

	/**
	 * Updates a particular model.
	 *
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param int $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		if (isset($_POST['Hint'])) {
			$model->attributes = $_POST['Hint'];
			if ($model->save()) {
				$this->redirect(['admin', 'Hint[model]' => $model->model]);
			}
		}

		$this->render(
			'update',
			['model' => $model]
		);
	}

	/**
	 * Deletes a particular model.
	 *
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 *
	 * @param int $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax'])) {
				$this->redirect($_POST['returnUrl'] ?? ['admin']);
			}
		} else {
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model = new Hint('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Hint'])) {
			$model->attributes = $_GET['Hint'];
		}

		$this->render(
			'admin',
			['model' => $model]
		);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 *
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param int $id the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model = Hint::model()->findByPk((int) $id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $model;
	}
}
