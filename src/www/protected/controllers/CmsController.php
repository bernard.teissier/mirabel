<?php

class CmsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'users' => ['@'],
			],
			['deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * Displays a particular model.
	 *
	 * @param int $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render(
			'view',
			['model' => $this->loadModel($id)]
		);
	}

	/**
	 * Creates a new model.
	 *
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($partenaire=0)
	{
		if ($partenaire) {
			$pId = (int) $partenaire;
			$existing = Cms::model()->findByAttributes(['name' => 'partenaire', 'partenaireId' => $pId]);
			if ($existing) {
				$this->redirect(['update', 'id' => $existing->id]);
			}
			if (!Yii::app()->user->checkAccess('partenaire/editorial', ['partenaireId' => $pId])) {
				throw new CHttpException(403, "Vous ne pouvez modifier les données des autres partenaires.");
			}
			$model = new Cms;
			$model->partenaireId = $pId;
			$model->name = 'partenaire';
		} else {
			$model = new Cms;
			$model->name = 'brève';
		}

		$this->checkModelAccess($model);

		if (isset($_POST['Cms'])) {
			$model->attributes = $_POST['Cms'];
			if ($model->partenaireId && !Yii::app()->user->checkAccess('partenaire/editorial', ['partenaireId' => $pId])) {
				throw new CHttpException(403, "Vous ne pouvez modifier les données des autres partenaires.");
			}
			if ($model->save()) {
				$this->redirect(['view', 'id' => $model->id]);
			}
		}

		$this->render(
			'create',
			['model' => $model]
		);
	}

	/**
	 * Creates a new page.
	 */
	public function actionCreatePage()
	{
		$model = new Cms;
		$model->singlePage = true;
		$model->partenaireId = null;

		$this->checkModelAccess($model);

		if (isset($_POST['Cms'])) {
			$model->attributes = $_POST['Cms'];
			$model->singlePage = true;
			$model->partenaireId = null;
			if ($model->save()) {
				$this->redirect(['view', 'id' => $model->id]);
			}
		}

		$this->render(
			'create-page',
			['model' => $model]
		);
	}

	/**
	 * Updates a particular model.
	 *
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param int $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		if (isset($_POST['Cms'])) {
			$model->attributes = $_POST['Cms'];
			$this->checkModelAccess($model);
			if ($model->save()) {
				$this->redirect(['view', 'id' => $model->id]);
			}
		}

		$this->render(
			'update',
			['model' => $model]
		);
	}

	/**
	 * Deletes a particular model.
	 *
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 *
	 * @param int $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			$model = $this->loadModel($id);
			$this->checkModelAccess($model);
			$model->delete();
			$this->redirect($_POST['returnUrl'] ?? ['admin']);
		} else {
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin($filter='')
	{
		$model = new Cms('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Cms'])) {
			$model->attributes = $_GET['Cms'];
		}

		if (!Yii::app()->user->checkAccess('redaction/editorial')) {
			if (Yii::app()->user->checkAccess('partenaire/editorial', ['partenaireId' => Yii::app()->user->partenaireId])) {
				$model->partenaireId = Yii::app()->user->partenaireId;
			} else {
				throw new CHttpException(403, "Vous n'avez pas de droit de rédaction.");
			}
		}

		switch ($filter) {
			case 'blocs':
				$model->singlePage = false;
				$model->name = 'pas de brèves';
				break;
			case 'breves':
				$model->singlePage = false;
				$model->name = 'brève';
				break;
			case 'pages':
				$model->singlePage = true;
				break;
			default:
				$filter = '';
		}

		$this->render(
			'admin',
			['model' => $model, 'filter' => $filter]
		);
	}

	/**
	 * Preview a model.
	 */
	public function actionPreview()
	{
		$model = new Cms();
		if (isset($_POST['Cms'])) {
			$model->attributes = $_POST['Cms'];
			echo $model->toHtml();
		}
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 *
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param int $id the ID of the model to be loaded
	 * @return Cms
	 */
	public function loadModel($id)
	{
		$model = Cms::model()->findByPk((int) $id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $model;
	}

	protected function checkModelAccess(Cms $block)
	{
		if (!Yii::app()->user->checkAccess('redaction/editorial')) {
			if ($block->partenaireId) {
				if (!Yii::app()->user->checkAccess('partenaire/editorial', ['partenaireId' => $block->partenaireId])) {
					throw new CHttpException(403, "Vous ne pouvez modifier les données des autres partenaires.");
				}
			} else {
				throw new CHttpException(403, "Vous n'avez pas de droit de rédaction.");
			}
		}
	}
}
