<?php

class UploadController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'users' => ['@'],
			],
			[
				'deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * Called before any action.
	 */
	public function init()
	{
		$app = Yii::app();
		$user = $app->user;
		if (!$user->isGuest && !$user->checkAccess('redaction/editorial')) {
			$paddedPartenaireId = sprintf('%03d', $user->getState('partenaireId'));
		} else {
			$paddedPartenaireId = null;
		}
		Upload::$destinations = [
			'public' => new UploadDestination([
				'name' => 'Dépôt de fichiers publics',
				'path' => dirname($app->getBasePath()) . '/public',
				'url' => $app->getBaseUrl(true) . '/public/',
				//'forceDestName' => $paddedPartenaireId,
			]),
			'private' => new UploadDestination([
				'name' => 'Dépôt de fichiers privés',
				'path' => $app->getBasePath() . '/data/upload',
				'url' => $app->createAbsoluteUrl('/upload/view') . '/',
				'types' => 'odp, odt, pdf, docx, xls, zip, png, jpg, jpeg, csv, mkv, mp4, avi, webm, md, txt',
			]),
			'logos-p' => new UploadDestination([
				'name' => 'Logos des partenaires',
				'message' => "<li>Le logo doit avoir l'extension <em>jpg</em> ou <em>png</em>.</li>",
				'path' => dirname($app->getBasePath()) . '/images/logos/',
				'url' => $app->getBaseUrl(true) . '/images/logos/',
				'forceDestName' => $paddedPartenaireId,
				'types' => 'png, jpg, jpeg, md, txt',
			]),
			'videos' => new UploadDestination([
				'name' => 'Vidéos',
				'message' => "<li>La vidéo doit avoir l'extension <em>mkv</em> <em>avi</em> ou <em>mp4</em>.</li>"
					. "<li>L'image doit avoir l'extension <em>jpg</em> ou <em>png</em>.</li>",
				'path' => dirname($app->getBasePath()) . '/images/videos/',
				'url' => $app->getBaseUrl(true) . '/images/videos/',
				'types' => 'png, jpg, jpeg, mkv, mp4, avi, webm, md, txt',
			]),
			'editeurs' => new UploadDestination([
				'name' => 'Éditeurs - lettres',
				'path' => $app->getBasePath() . '/data/upload/editeurs',
				'url' => $app->createAbsoluteUrl('/upload/view') . '/',
				'subpath' => 'editeurs/',
				'types' => 'odp, odt, pdf, docx, xls, md, txt',
			]),
			'conventions' => new UploadDestination([
				'name' => 'Conventions - fichiers privés',
				'path' => $app->getBasePath() . '/data/upload/conventions',
				'url' => $app->createAbsoluteUrl('/upload/view') . '/',
				'subpath' => 'conventions/',
				'types' => 'odp, odt, pdf, docx, xls, md, txt',
			]),
		];
		return parent::init();
	}

	/**
	 * Default upload page.
	 *
	 * @param string $dest One the destinations declared in init().
	 */
	public function actionIndex($dest = 'public', $partenaireId = 0)
	{
		$user = Yii::app()->user;
		$model = new Upload($dest, $user->checkAccess('upload/rename'));

		if ($dest === 'logos-p') {
			if (!$user->checkAccess('partenaire/logo', ['partenaireId' => (int) $partenaireId]) && !$user->checkAccess('redaction/editorial')) {
				throw new CHttpException(403, "Vous n'avez pas de droit de modifier les données de votre établissement.");
			}
			$model->destName = sprintf('%03d', $partenaireId);
		} elseif ($dest === 'editeurs') {
			if (!$user->checkAccess('suiviPartenairesEditeurs') && !$user->checkAccess('redaction/editorial')) {
				throw new CHttpException(403, "Vous n'avez pas de droit d'accéder aux lettres aux éditeurs.");
			}
		} else {
			if (!$user->checkAccess('redaction/editorial')) {
				if ($user->checkAccess('partenaire/logo')) {
					$this->redirect(['index', 'dest' => 'logos-p']);
				} else {
					throw new CHttpException(403, "Vous n'avez pas de droit de rédaction pour les fonctions éditoriales.");
				}
			}
		}
		if ($this->receiveFile($model)) {
			$this->redirect(['index', 'dest' => $dest]);
		}

		$path = rtrim($model->destination->path, '/');
		if (file_exists($path . '/lisezmoi.md')) {
			$lisezmoi = (new CMarkdownParser())->safeTransform(file_get_contents($path . '/lisezmoi.md'));
		} else {
			$lisezmoi = '';
		}
		if ($dest === 'conventions') {
			$fileList = $this->listFiles($model->destination->path, 'name', 'DESC');
		} else {
			$fileList = $this->listFiles($model->destination->path);
		}
		$this->render(
			'index',
			[
				'model' => $model,
				'uploaded' => $fileList,
				'lisezmoi' => $lisezmoi,
			]
		);
	}

	/**
	 * Serves an uploaded file which is in a protected dir.
	 *
	 * @param string $file
	 */
	public function actionView($file)
	{
		$filePath = dirname(Yii::app()->getBasePath())
			. (strpos($file, 'protected') === false ? '/protected/data/upload/' . $file : $file);
		if (!Upload::isDownloadable($filePath)) {
			throw new CHttpException(403, "L'accès à ce fichier n'est pas autorisé.");
		}
		$mime = CFileHelper::getMimeTypeByExtension($filePath);
		if (!$mime) {
			$mime = CFileHelper::getMimeType($filePath);
		}
		if ($mime) {
			$char = '';
			if (strncmp($mime, 'text/', 5)) {
				$char = '; charset="UTF-8"';
			}
			header('Content-Type: ' . $mime . $char);
		} else {
			header('Content-Type: application/force-download');
		}
		header('Content-disposition: attachment;filename=' . rawurlencode(basename($file)));
		// clean up the application first to avoid timeout
		ob_start();
		Yii::app()->end(0, false);
		ob_end_clean();
		readfile($filePath);
		exit(0);
	}

	/**
	 * Deletes an uploaded file which is in a protected dir.
	 */
	public function actionDelete()
	{
		if (!Yii::app()->user->checkAccess('redaction/editorial')) {
			throw new CHttpException(403, "Vous n'avez pas de droit de rédaction pour les fonctions éditoriales.");
		}
		if (!Yii::app()->request->isPostRequest || empty($_POST['file'])) {
			throw new CHttpException(400, "POST only.");
		}
		$file = $_POST['file'];
		$filePath = rtrim(dirname(Yii::app()->getBasePath()), '/') . '/' . ltrim($file, '/');
		if (!Upload::isDownloadable($filePath)) {
			throw new CHttpException(403, "L'accès à ce fichier n'est pas autorisé.");
		}
		if (is_dir($filePath)) {
			throw new CHttpException(403, "La suppression des répertoires est interdite.");
		}
		$del = unlink($filePath);
		if (Yii::app()->request->isAjaxRequest) {
			if ($del) {
				echo '<em>supprimé</em>';
			} else {
				echo '<em>Erreur lors de la suppression</em>';
			}
		} else {
			$this->redirect('index');
		}
	}

	/**
	 * Helper function that returns an Upload object, or false if there was an error.
	 *
	 * @param Upload $model
	 * @return bool Upload succeeded
	 */
	protected function receiveFile($model)
	{
		if (!isset($_POST['Upload'])) {
			return false;
		}
		$model->setAttributes($_POST['Upload']);
		$model->file = CUploadedFile::getInstance($model, 'file');
		if (!$model->file) {
			$model->addError('file', "Aucun fichier envoyé.");
			return false;
		}
		if ($model->validate()) {
			if ($model->moveFile()) {
				Yii::app()->user->setFlash(
					'success',
					"Fichier déposé sur le serveur : " . $model->getViewUrl()
				);
				return true;
			}
			Yii::app()->user->setFlash(
				'error',
				"Le fichier n'a pas pu être écrit (doublon) dans le répertoire " . $model->destination->path
			);
		}
		return false;
	}

	/**
	 * Lists the files a directory.
	 *
	 * @param string $path
	 * @param string $order
	 * @param string $direction
	 * @return array Each item has the keys (name, size, date, path).
	 */
	protected function listFiles(string $path, string $order = 'name', string $direction = 'ASC'): array
	{
		chdir($path);
		$relPath = rtrim(str_replace(dirname(Yii::app()->getBasePath()), '', $path), '/');
		$list = [];
		foreach (glob('*') as $filename) {
			if (is_dir($filename) || is_link($filename)) {
				continue;
			}
			$list[] = [
				'name' => $filename,
				'size' => filesize($filename),
				'date' => date('Y-m-d', filemtime($filename)),
				'path' => $relPath . '/' . $filename,
			];
		}
		if ($order !== 'name' || $direction !== 'ASC') {
			usort(
				$list,
				function ($x, $y) use ($order, $direction) {
					if ($direction === 'ASC') {
						return is_int($x[$order]) ? $x[$order] <=> $y[$order] : strcmp($x[$order], $y[$order]);
					}
					return is_int($x[$order]) ? $y[$order] <=> $x[$order] : strcmp($y[$order], $x[$order]);
				}
			);
		}
		return $list;
	}
}
