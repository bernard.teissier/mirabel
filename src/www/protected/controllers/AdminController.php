<?php

class AdminController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl',
			'postOnly + deconnexion, finMaintenance',
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'roles' => ['admin'],
			],
			[
				'deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionDeconnexion()
	{
		if (PHP_SAPI !== 'cli') {
			$dir = Yii::app()->session->getSavePath();
			$files = array_diff(glob("$dir/sess_*"), [self::getSessionFile()]);
			foreach ($files as $file) {
				unlink($file);
			}
			Yii::app()->user->setFlash('info', "Les autres sessions ont été supprimées.");
		}
		\Config::writeRaw('maintenance.authentification.inactive', '1');
		$this->redirect(['index']);
	}

	public function actionFinMaintenance()
	{
		Yii::app()->user->setFlash('info', "Connexion ré-ouverte à tous.");
		\Config::writeRaw('maintenance.authentification.inactive', '0');
		$this->redirect(['index']);
	}

	private static function getSessionFile(): string
	{
		return sprintf(
			'%s/sess_%s',
			Yii::app()->session->getSavePath(),
			$_COOKIE[session_name()]
		);
	}
}
