<?php

/**
 * Controller is the customized base controller class.
 *
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout = '//layouts/column1';

	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu = [];

	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs = [];

	/**
	 * @var string
	 */
	public $pageDescription = "Description des accès en ligne aux contenus des revues";

	public $sidebarInsert = '';

	/**
	 * @var ?Partenaire
	 */
	public $institute;

	/**
	 * @var ?Partenaire
	 */
	public $partenaire;

	/**
	 * @var array
	 */
	protected $menuExt = [];

	/**
	 * @var bool If true, display leaves on the background LEFT.
	 */
	protected $backgroundL = true;

	/**
	 * @var bool If true, display leaves on the background RIGHT.
	 */
	protected $backgroundR = false;

	/**
	 * @var array List of: type => Mime http header
	 */
	protected $_contentTypes = [
		'json' => 'Content-Type: application/json; charset="UTF-8"',
		'text' => 'Content-Type: text/plain; charset="UTF-8"',
		'xml' => 'Content-Type: text/xml; charset="UTF-8"',
		'csv' => 'Content-Type: text/csv; charset="UTF-8"; header=present',
		'tsv' => 'Content-Type: text/tab-separated-values; charset="UTF-8"; header=present',
		'pdf' => 'Content-Type: application/pdf',
	];

	private $htmlHead;

	public function init()
	{
		if ($this->getModule() !== null) {
			return parent::init();
		}

		// use special base name in some CActiveForm instances, e.g. q[id] instead of InterventionSearch[id]
		CHtml::setModelNameConverter(function ($m) {
			$special = [
				'InterventionSearch' => 'q',
				'models\forms\Login' => 'LoginForm',
				'models\forms\StatsForm' => 'filter',
				'models\forms\PasswordMailForm' => 'form',
				'models\searches\EditeurSearch' => 'q',
			];
			$name = is_object($m) ? get_class($m) : (string) $m;
			return $special[$name] ?? $name;
		});

		$user = Yii::app()->user;
		if (!isset($user)) {
			return;
		}
		if (empty($user->isGuest) && isset($user->partenaireId)) {
			$this->partenaire = Partenaire::model()->findByPk($user->partenaireId);
		}
		if (isset($_COOKIE['institute']) && preg_match('/^set-(\d+)$/', $_COOKIE['institute'], $m)) {
			$user->setInstituteById((int) $m[1]);
		}
		if ($user->getInstitute()) {
			$this->institute = Partenaire::model()->findByPk($user->getInstitute());
		} else {
			$this->institute = null;
		}

		if (($user->isGuest && \Config::read('maintenance.message.invite'))
			|| (!$user->isGuest && \Config::read('maintenance.message.authentifie'))
			) {
			$user->setFlash('warning', \Config::read('maintenance.message.texte'));
		}
	}

	/**
	 * Sends to the browser the HTTP header for this type of data.
	 *
	 * @param string $type Among: text, json, xml, nocache. Or the full header.
	 * @return bool Success?
	 */
	public function header($type)
	{
		if (headers_sent()) {
			Yii::log("Cannot send header '$type'", 'warning');
			return false;
		}
		$typelow = strtolower($type);
		if ($typelow === 'nocache') {
			header("Expires: Sun, 25 Jul 1997 12:00:00 GMT");
			header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
			header("Cache-Control: no-store, no-cache, must-revalidate");
			header("Cache-Control: post-check=0, pre-check=0", false);
		} elseif (isset($this->_contentTypes[$typelow])) {
			header($this->_contentTypes[$typelow]);
		} else {
			header($type);
		}
		return true;
	}

	/**
	 * Loads a JS file named like the view, in the sub-directory js/ of the view path.
	 *
	 * @param string $viewFile Path to the view or to a JS file.
	 * @param int $position (opt) Defaults to CClientScript::POS_READY.
	 *   See http://www.yiiframework.com/doc/api/1.1/CClientScript/#registerScript-detail
	 */
	public function loadViewJs($viewFile, $position=CClientScript::POS_READY)
	{
		$pathinfo = pathinfo($viewFile);
		if (empty($pathinfo['extension'])) {
			Yii::log("loadViewJs expects a file with an extension", CLogger::LEVEL_ERROR);
			return;
		}
		switch ($pathinfo['extension']) {
			case 'js':
				$jsFile = $viewFile;
				break;
			case 'php':
				$jsFile = $pathinfo['dirname'] . '/js/' . $pathinfo['filename'] . '.js';
				break;
			default:
				Yii::log("loadViewJs expects a js file name, or `__FILE__`", CLogger::LEVEL_ERROR);
				return;
		}

		if (!file_exists($jsFile) or !is_readable($jsFile)) {
			Yii::log("loadViewJs could not read {$jsFile}", CLogger::LEVEL_ERROR);
			return;
		}
		Yii::app()->getClientScript()->registerScript(
			$pathinfo['filename'] . '-js',
			file_get_contents($jsFile),
			$position
		);
	}

	public static function linkEmbeddedFeed($type, $title=null, $parameters=[])
	{
		if (!$title) {
			$title = Yii::app()->name;
		} else {
			$title = Yii::app()->name . " - " . $title;
		}
		$title .= " ($type)";
		$types = [
			'rss1' => 'application/rss+xml',
			'rss2' => 'application/rss+xml',
			'atom' => 'application/atom+xml',
		];
		if (!isset($types[$type])) {
			throw new CHttpException(500, "Unknown feed type (rss1, rss2 atom)");
		}
		if ($type != 'rss2') {
			$parameters['type'] = $type;
		}
		$url = Yii::app()->baseUrl . '/feeds.php'
			. ($parameters ? '?' . http_build_query($parameters) : '');
		return '<link rel="alternate" type="' . $types[$type]
			. '" href="' . CHtml::encode($url)
			. '" title="' . CHtml::encode($title)
			. '" />';
	}

	/**
	 * @return string HTML
	 */
	public function getSearchForm($class = "navbar-search")
	{
		return <<<EOHTML
			<form class="$class form-search" action="{$this->createUrl('/site/search')}">
				<div class="input-append">
					<input type="text" name="global" class="search-query" placeholder="Recherche" aria-label="recherche" />
					<button type="submit" class="btn"><i class="icon-search"></i></button>
				</div>
			</form>
			EOHTML;
	}

	/**
	 * @param string $text Must be already encoded with CHtml::encode() and such
	 * @param string $unless (opt) PCRE pattern, will not append text if the pattern matches
	 */
	public function appendToHtmlHead($text, $unless = '')
	{
		if (!$unless || !preg_match($unless, $this->htmlHead)) {
			$this->htmlHead .= $text;
		}
	}

	/**
	 * @return string
	 */
	public function getHtmlHead()
	{
		$m = [];
		if ($this->pageDescription) {
			$description = CHtml::encode($this->pageDescription);
		} elseif (preg_match('/<meta name="description" content="(.+?)" lang/', $this->htmlHead, $m)) {
			$description = CHtml::encode($m[1]);
		} else {
			$description = "Description des accès en ligne aux contenus des revues";
		}
		$this->appendToHtmlHead(
			<<<EOL
				<meta name="author" content="Mirabel" />
				<meta name="keywords" content="revue, accès en ligne, texte intégral, sommaire, périodique" lang="fr" />
				<meta name="description" content="{$description}" lang="fr" />
				<link rel="schema.dcterms" href="http://purl.org/dc/terms/" />
				<meta name="dcterms.title" content="Mirabel" />
				<meta name="dcterms.subject" content="revue, accès en ligne, texte intégral, sommaire, périodique" />
				<meta name="dcterms.language" content="fr" />
				<meta name="dcterms.creator" content="Mirabel" />
				<meta name="dcterms.publisher" content="Sciences Po Lyon" />
				EOL,
			'/<meta\s+name="description"/'
		);
		$title = CHtml::encode($this->pageTitle);
		$logoCarre = Yii::app()->getBaseUrl(true) . '/images/logo-mirabel-carre.png';
		$this->appendToHtmlHead(
			<<<EOL
				<meta name="twitter:card" content="summary" />
				<meta name="twitter:site" content="@mirabel_revues" />
				<meta name="twitter:title" content="{$title}" />
				<meta name="twitter:description" content="{$description}" />
				<meta name="twitter:image" content="{$logoCarre}" />
				EOL,
			'/<meta name="twitter:card"/'
		);

		return $this->htmlHead;
	}

	protected function validateIntervention(Intervention $i, bool $directAccess, array $url = null): bool
	{
		if (!$i->validate(null, false)) {
			return false;
		}
		if ($directAccess) {
			$i->contenuJson->confirm = true;
			if ($i->accept(($url !== null))) {
				Yii::app()->user->setFlash('success', "Modification enregistrée.");
				if ($url) {
					$i->emailForEditorChange();
					$this->redirect($url);
				}
				return true;
			}
		} else {
			if ($i->save()) {
				Yii::app()->user->setFlash(
					'success',
					"Proposition enregistrée. Merci d'avoir contribué à Mir@bel."
				);
				if ($url) {
					$i->emailForEditorChange("", "Proposition");
					$this->redirect($url);
				}
				return true;
			}
		}
		return false;
	}

	protected function beforeAction($action)
	{
		if (isset($_GET['ppp'])) {
			Yii::app()->user->setInstituteById((int) $_GET['ppp']);
			$params = $_GET;
			unset($params['ppp']);
			if ($this->route === 'site/index') {
				$url = Yii::app()->baseUrl . "/" . ($params ? "?" . http_build_query($params) : '');
			} else {
				$url = Yii::app()->createAbsoluteUrl("/" . $this->route, $params);
			}
			$this->redirect($url);
			exit();
		}
		return true;
	}

	/**
	 * If the completion was not done asynchronously, try to do it on the server side.
	 *
	 * @param string $textField
	 * @param string $className
	 * @param string $method 'REQUEST' (default), 'GET' or 'POST'
	 * @return int|null ID found, or null
	 */
	protected function autoCompleteOffline($textField, $className, $method = 'REQUEST'): ?int
	{
		$method = '_' . strtoupper($method);
		if (empty($GLOBALS[$method][$textField])) {
			return null;
		}
		$text = $GLOBALS[$method][$textField];
		$completed = $className::findExactTerm($text);
		if (count($completed) === 0) {
			$completed = $className::completeTerm($text);
		}
		if (count($completed) !== 1) {
			return null;
		}
		return (int) $completed[0]['id'];
	}

	protected function loadSearchNavigation(): ?SearchNavigation
	{
		if (isset($_GET['s'])) {
			$searchNavigation = new SearchNavigation();
			$searchNavigation->loadByHash($_GET['s']);
			return ($searchNavigation->isEmpty() ? null : $searchNavigation);
		}
		return null;
	}
}
