<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class HtmlTable
{
	/**
	 * @var array
	 */
	public $data = [];

	/**
	 * @var array Titles displayed at the top (by default, keys of the first row, if not numeric).
	 */
	public $header = [];

	/**
	 * @var array Titles to display at the beginning of each line.
	 */
	public $rowHeaders = [];

	public $classes = [
		"table table-striped table-bordered table-condensed",
	];

	public $htmlAttributes = [];

	private $footnote = '';

	public function __construct($data = [], $header = null, $rowHeaders = [])
	{
		$this->data = $data;
		if ($header === null) {
			$firstRow = reset($this->data);
			if ($firstRow && !isset($firstRow[0])) {
				$header = array_keys($firstRow);
			}
		}
		if ($rowHeaders) {
			if ($header) {
				array_unshift($header, '');
			}
			$this->rowHeaders = $rowHeaders;
		}
		$this->header = $header;
	}

	public function __toString()
	{
		return $this->toHtml(true);
	}

	public static function build($data, $header=null, $rowHeaders=[])
	{
		return new self($data, $header, $rowHeaders);
	}

	public static function buildFromAssoc(array $data, bool $htmlEncode = true): \HtmlTable
	{
		$rows = [];
		$rowHeaders = [];
		foreach ($data as $key => $value) {
			if (!is_scalar($value)) {
				$value = json_encode($value);
			}
			$rowHeaders[] = $key;
			if ($htmlEncode) {
				$rows[] = [CHtml::encode($value)];
			} else {
				$rows[] = [$value];
			}
		}
		return new self($rows, [], $rowHeaders);
	}

	/**
	 * Set a footnote that will be displayed with %d replaced by the numer of lines.
	 *
	 * @param string $html
	 * @return \HtmlTable
	 */
	public function setFootnote(string $html)
	{
		$this->footnote = $html;
		return $this;
	}

	public function addClass($class)
	{
		if (is_array($class)) {
			$this->classes = array_unique(array_merge($this->classes, $class));
		} else {
			$this->classes[] = (string) $class;
		}
		return $this;
	}

	/**
	 * @param array $attributes
	 * @return \HtmlTable
	 */
	public function setHtmlAttributes(array $attributes)
	{
		$this->htmlAttributes = $attributes;
		return $this;
	}

	public function toHtml(bool $escapeData = false): string
	{
		if (!$this->data) {
			return "<em>Aucune donnée</em>";
		}
		$first = reset($this->data);
		if (!$first) {
			return '';
		}
		$html = '';
		if (!empty($this->header)) {
			$html .= '<thead><tr>';
			foreach ($this->header as $colHead) {
				if (is_array($colHead)) {
					$v = array_shift($colHead);
					$html .= CHtml::tag('th', $colHead, CHtml::encode($v));
				} else {
					$html .= '<th>' . CHtml::encode($colHead) . '</th>';
				}
			}
			$html .= "</tr></thead>\n";
		}
		$html .= '<tbody>';
		$count = 0;
		foreach ($this->data as $row) {
			$html .= '<tr>';
			if ($this->rowHeaders) {
				$html .= "<td><strong>" . $this->rowHeaders[$count] . "</strong></td>";
			}
			if (is_scalar($row)) {
				$html .= '<td>' . ($escapeData ? CHtml::encode($row) : $row) . '</td>';
			} else {
				foreach ($row as $v) {
					$html .= '<td>' . ($escapeData ? CHtml::encode($v) : $v) . '</td>';
				}
			}
			$html .= '</tr>';
			$count++;
		}
		$html .= '</tbody>';

		$htmlOptions = array_merge($this->htmlAttributes, ['class' => join(' ', $this->classes)]);
		$table = CHtml::tag('table', $htmlOptions, $html);

		if ($this->footnote) {
			$table .= sprintf('<div>' . $this->footnote . '</div>', $count);
		}
		return $table;
	}
}
