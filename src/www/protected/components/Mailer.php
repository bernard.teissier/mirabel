<?php

/**
 * Helper class for easier use of SwiftMail.
 */
class Mailer
{
	/**
	 * @var Swift_Mailer
	 */
	protected static $mailer;

	/**
	 * Check if the configuration is complete.
	 *
	 * @return bool
	 */
	public static function checkConfig()
	{
		if (!Config::read('email.from')) {
			Yii::log('Mailer: "email.from" is not configured', "error", 'mail');
			return false;
		}
		return true;
	}

	/**
	 * Sample:
	 *
	 *		$message = Mailer::newMail()
	 *			->setSubject($model->subject)
	 *			->setFrom($model->email)
	 *			->setTo(array(Config::read('contact.email')))
	 *			->setBody($model->body);
	 *
	 * @return MailMessage
	 */
	public static function newMail()
	{
		$instance = new MailMessage();
		$returnPath = Config::read('email.returnPath');
		if ($returnPath) {
			$instance->setReturnPath($returnPath);
		}
		return $instance;
	}

	/**
	 * Sends a message, built with ::newMail(), with the configured mail transport.
	 *
	 * @param Swift_Message $message
	 * @return bool Success?
	 */
	public static function sendMail($message)
	{
		$mailer = self::getMailer();
		if ($mailer->send($message) > 0) {
			return true;
		}
		Yii::log("Contact: mail could not be sent. Please check the configuration.", 'error', 'email');
		return false;
	}

	/**
	 * Use only when ::sendMail() is not suitable, e.g. sending multiple mail at once.
	 *
	 * @return Swift_Mailer
	 */
	public static function getMailer()
	{
		if (isset(self::$mailer)) {
			return self::$mailer;
		}
		$mailHost = trim(Config::read('email.host'));
		if ($mailHost) {
			if ($mailHost === 'debug') {
				Yii::log("Mails will be saved in runtime/mail-spool/ and NOT sent", "warning");
				$path = YiiBase::getPathOfAlias('application.runtime.mail-spool');
				if (!is_dir($path)) {
					mkdir($path);
				}
				$spool = new Swift_FileSpool($path);
				$transport = new Swift_SpoolTransport($spool);
			} else {
				$port = 25;
				$transport = new Swift_SmtpTransport($mailHost, $port);
			}
		} else {
			$transport = new Swift_SendmailTransport();
		}
		self::$mailer = new Swift_Mailer($transport);
		return self::$mailer;
	}
}
