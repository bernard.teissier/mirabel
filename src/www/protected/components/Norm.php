<?php

/**
 * Static methods to normalize strings.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class Norm
{
	public static function urlParam(string $urlPart): string
	{
		return trim(
			preg_replace(
				['/(\w)@(\w)/', '/[^\w_-]/', '/--+/'],
				['{$1}a{$2}', '', '-'],
				str_replace(
					[ ' & ', ' -- ', ' : ', ' ', "'", ' !', ' ?', ' ;', ';', '/', '.'],
					['-et-',    '-',   '-', '-', "-",   '',   '',  '-', '-', '-', '-'],
					iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $urlPart)
				)
			),
			"_-"
		);
	}

	public static function unaccent(string $text): string
	{
		return preg_replace('/[\x{0300}-\x{036f}]/u', '', \Normalizer::normalize($text, Normalizer::FORM_D));
	}

	/**
	 * Normalize the UTF-8 (keeping diacritics) and the various '´
	 */
	public static function text(string $text): string
	{
		if ($text === '') {
			return '';
		}
		if (extension_loaded('intl')) {
			return str_replace(
				["’", "´"],
				["'", "'"],
				\Normalizer::normalize($text) // ext intl
			);
		}
		return str_replace(
			["’", "´"],
			["'", "'"],
			$text
		);
	}
}
