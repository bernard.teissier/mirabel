<?php

/**
 * PagerAlpha class file.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 * @package bootstrap.widgets
 */

/**
 * PagerAlpha pager widget.
 */
class PagerAlpha extends CLinkPager
{
	public const MAX_RESULTS_PER_PAGE = 1000;

	/**
	 * @var string the text shown before page buttons. Defaults to ''.
	 */
	public $header = '';

	/**
	 * @var string|bool the URL of the CSS file used by this pager.
	 * Defaults to false, meaning that no CSS will be included.
	 */
	public $cssFile = false;

	/**
	 * @var bool whether to display the first and last items.
	 */
	public $displayNextAndPrevious = false;

	/**
	 * @var string Name of the parameter to set the page in the links.
	 */
	public $urlParameter = 'lettre';

	/**
	 * @var string Name of the parameter to set the *sub* page in the links.
	 */
	public $urlSubParameter = 'page';

	/**
	 * @var int Size of the pagination inside a letter.
	 */
	public $limit = 50;

	/**
	 * @var string To be printed after the main pagination
	 */
	public $subPagination = '';

	/**
	 * @var array The key/values will be added to each link.
	 */
	public $urlDefaultParameters = [];

	/**
	 * @var string Name used in tooltips "X elements.
	 */
	public $elementName = 'élément';

	/**
	 * @var int
	 */
	protected $_currentPage = 0;

	/**
	 * @var int
	 */
	protected $_subPage = 0;

	/**
	 * @var array filters the letters to display: assoc(letter => count).
	 */
	protected $_filter;

	/**
	 * Initializes the pager by setting some default property values.
	 */
	public function __construct(int $itemCount = 0)
	{
		parent::__construct();
		if ($this->nextPageLabel === null) {
			$this->nextPageLabel = Yii::t('bootstrap', 'Next') . ' &rarr;';
		}

		if ($this->prevPageLabel === null) {
			$this->prevPageLabel = '&larr; ' . Yii::t('bootstrap', 'Previous');
		}

		if (!isset($this->htmlOptions['class'])) {
			$this->htmlOptions['class'] = 'compact'; // would default to yiiPager
		}

		$this->maxButtonCount = 27;

		if (!$this->limit) {
			$this->pageSize = self::MAX_RESULTS_PER_PAGE;
		} else {
			$this->pageSize = $this->limit;
			$this->_subPage = 1;
			if (isset($_GET['page']) && $_GET['page'] > 1) {
				$this->_subPage = (int) $_GET['page'];
			}
		}

		if (!empty($itemCount)) {
			$this->setLettersFilter($itemCount);
		}
	}

	public function getOffset(): int
	{
		return ($this->_subPage - 1) * $this->limit;
	}

	/**
	 * Sets the current letter (or "0" for non-alpha).
	 *
	 * @param string $letter
	 */
	public function setCurrentLetter(string $letter): void
	{
		$this->_currentPage = ord(strtoupper($letter)) - 64;
		if ($this->_currentPage < 0 or $this->_currentPage > 26) {
			$this->_currentPage = 0;
		}
		// if there's nothing for this letter, find the next non-empty letter
		if (!empty($this->_filter) and $this->_filter[$this->_currentPage] == 0) {
			$page = $this->_currentPage;
			while ($this->_filter[$page] == 0 and $page < 26) {
				$page++;
			}
			if ($this->_filter[$page]) {
				$this->_currentPage = $page;
			}
		}
	}

	/**
	 * Returns the current letter (or "0" for non-alpha).
	 */
	public function getCurrentLetter(): string
	{
		return $this->_currentPage ? chr($this->_currentPage + 64) : "0";
	}

	/**
	 * Returns a numerical index between 0 and 26.
	 *
	 * @param bool $recalculate (opt) Unused.
	 * @return int between 0 and 26.
	 */
	public function getCurrentPage($recalculate = false): int
	{
		if ($recalculate) {
			$this->setCurrentLetter($this->getCurrentLetter());
		}
		return $this->_currentPage;
	}

	/**
	 * Sets the letters counts so that empty letters can be filtered out.
	 *
	 * The key is not the letter, but its rank (1 for "a", 26 for "z", 0 for non-alpha).
	 *
	 * @param array $counts assoc array(position => count).
	 */
	public function setLettersFilter(array $counts): void
	{
		$this->_filter = $counts;
		for ($i = 0 ; $i <= 26 ; $i++) {
			if (empty($this->_filter[$i])) {
				$this->_filter[$i] = 0;
			}
		}
	}

	/**
	 * Return the begin and end pages that need to be displayed.
	 *
	 * Not optimized, just for testing.
	 *
	 * @return array [int $start, int $end]
	 */
	public function getPageRange()
	{
		if (empty($this->_filter)) {
			return [1, 27];
		}
		return [1, count(
			array_filter($this->createPageButtons(), function ($x) {
				return $x !== '';
			})
		)];
	}

	public function applyLimit()
	{
	}

	/**
	 * Creates the page buttons.
	 * @return array a list of page buttons (in HTML code).
	 */
	protected function createPageButtons()
	{
		$buttons = [];

		// prev page
		if ($this->displayNextAndPrevious) {
			$page = max($this->_currentPage - 1, 0);
			$buttons[] = $this->createPageButton(
				$this->prevPageLabel,
				$page,
				'previous',
				$this->_currentPage == 0,
				false
			);
		}

		// internal pages
		for ($i = 0; $i <= 26; $i++) {
			$buttons[] = $this->createPageButton(
				$i ? strtoupper(chr($i + 64)) : '#0-9',
				$i,
				'',
				false,
				$i == $this->_currentPage
			);
		}

		// next page
		if ($this->displayNextAndPrevious) {
			$page = min($this->_currentPage + 1, 26);
			$buttons[] = $this->createPageButton(
				$this->nextPageLabel,
				$page,
				'next',
				$this->_currentPage == 26,
				false
			);
		}

		// subpages
		if ($this->limit && $this->getItemCount() > $this->pageSize) {
			$subs = [
				$this->createPageButton(
					$this->prevPageLabel,
					[$this->_currentPage, $this->_subPage - 1],
					'previous',
					$this->_subPage < 2,
					false
				),
			];
			$maxPage = ceil($this->getItemCount() / $this->pageSize);
			for ($i = 1; $i <= $maxPage; $i++) {
				$subs[] = $this->createPageButton(
					"$i",
					[$this->_currentPage, $i],
					'',
					false,
					$i == $this->_subPage
				);
			}
			$subs[] = $this->createPageButton(
				$this->nextPageLabel,
				[$this->_currentPage, $this->_subPage + 1],
				'next',
				$this->_subPage >= $maxPage,
				false
			);
			$this->subPagination = CHtml::tag('ul', ['class' => 'compact'], implode("\n", $subs));
		}

		return $buttons;
	}

	/**
	 * Creates a page button.
	 *
	 * You may override this method to customize the page buttons.
	 *
	 * @param string $label the text label for the button
	 * @param int|array $page the page number (or array(page, subpage)
	 * @param string $class the CSS class for the page button.
	 * @param bool $hidden whether this page button is visible
	 * @param bool $selected whether this page button is selected
	 * @return string the generated button
	 */
	protected function createPageButton($label, $page, $class, $hidden, $selected)
	{
		if (is_array($page)) {
			list($page, $subPage) = $page;
		}
		$letter = $page ? chr($page + 64) : "9";
		$parameters = $this->urlDefaultParameters;
		$parameters[$this->urlParameter] = $letter;
		if (!empty($subPage)) {
			$parameters[$this->urlSubParameter] = $subPage;
		}
		$url = Yii::app()->getController()->createUrl('', $parameters);

		$htmlOptions = [];
		if (!empty($this->_filter)) {
			if (!$selected and empty($this->_filter[$page])) {
				return '';
			}
			$htmlOptions['title'] = sprintf(
				"%d {$this->elementName}%s",
				$this->_filter[$page],
				$this->_filter[$page] > 1 ? 's' : ''
			);
		}
		if ($class) {
			$htmlOptions['class'] = $class;
		} else {
			$htmlOptions['class'] = '';
		}
		if ($hidden) {
			$htmlOptions['class'] .= ' hidden';
			$url = '';
		} elseif ($selected) {
			$htmlOptions['class'] .= ' active';
		}

		$link = CHtml::link($label, $url);
		return CHtml::tag('li', $htmlOptions, $link);
	}
}
