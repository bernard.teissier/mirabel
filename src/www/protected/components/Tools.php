<?php

/**
 * Contains some static helper methods.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class Tools
{
	public static function readCsv(string $csv, string $separator): array
	{
		$result = [];
		foreach (explode("\n", $csv) as $line) {
			if (trim($line) !== "") {
				$result[] = str_getcsv($line, $separator);
			}
		}
		return $result;
	}

	/**
	 * Returns an assoc array (col1 => col2).
	 *
	 * @param string $sql SQL.
	 * @param array $content (opt) Initial content.
	 * @return array assoc array (col1 => col2).
	 */
	public static function sqlToPairs(string $sql, array $content = []): array
	{
		Yii::app()->getDb()->setActive(true); // activate the PDO connection if necessary
		$rows = Yii::app()->getDb()->getPdoInstance()->query($sql, PDO::FETCH_NUM);
		foreach ($rows as $row) {
			$content[$row[0]] = $row[1];
		}
		return $content;
	}

	/**
	 * Returns an assoc array (column => Object).
	 *
	 * @param string $sql SQL.
	 * @param string $className
	 * @param string $colName (opt) column used as key (defaults to 'id').
	 * @return array assoc array (column => Object).
	 */
	public static function sqlToPairsObject(string $sql, string $className, string $colName = 'id'): array
	{
		$rows = Yii::app()->db->createCommand($sql)->queryAll();
		$model = call_user_func([$className, 'model']);
		assert($model instanceof CActiveRecord);
		$records = [];
		foreach ($rows as $attributes) {
			$record = $model->populateRecord($attributes, true);
			if ($record !== null) {
				$records[$attributes[$colName]] = $record;
			}
		}
		return $records;
	}

	/**
	 * Return true if one of the models has an error.
	 *
	 * @param \CModel[] $array
	 */
	public static function hasErrors(array $array): bool
	{
		if ($array) {
			foreach ($array as $a) {
				if ($a->hasErrors()) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Returns an HTML table with 2 columns.
	 *
	 * @codeCoverageIgnore
	 *
	 * @param array $table
	 * @return string HTML table.
	 */
	public static function htmlAssocTable(array $table): string
	{
		$html = '<table class="table table-striped table-bordered table-condensed"><tbody>';
		foreach ($table as $key => $value) {
			if (!is_scalar($value)) {
				$value = json_encode($value);
			}
			$html .= '<tr><th>' . CHtml::encode($key) . '</th><td>'
				. CHtml::encode($value) . "</td></tr>\n";
		}
		$html .= "</tbody></table>\n";
		return $html;
	}

	/**
	 * Returns an HTML table with 2 columns.
	 *
	 * @codeCoverageIgnore
	 *
	 * @param array $table
	 * @param bool  $thFirst (opt, true)
	 * @param bool  $encode HTML encode (opt, true)
	 * @return string HTML table.
	 */
	public static function htmlTable(array $table, bool $thFirst = true, bool $encode = true): string
	{
		$html = '<table class="table table-striped table-bordered table-condensed"><tbody>';
		foreach ($table as $values) {
			$html .= '<tr>';
			$th = $thFirst;
			foreach ($values as $name => $value) {
				if (!is_scalar($value)) {
					if (is_object($value) && method_exists($value, 'toHtml')) {
						$content = $value->toHtml();
					} else {
						$content = CHtml::encode(json_encode($value));
					}
				} elseif (preg_match('/url$/i', $name)) {
					$content = CHtml::link($value, $value);
				} elseif ($encode) {
					$content = CHtml::encode($value);
				} else {
					$content = $value;
				}
				if ($th) {
					$html .= '<th>' . $content . '</th>';
					$th = false;
				} else {
					$html .= '<td>' . $content . '</td>';
				}
			}
			$html .= "</tr>\n";
		}
		$html .= "</tbody></table>\n";
		return $html;
	}

	/**
	 * Check if an IP matches a CIDR mask.
	 *
	 * @param int|string $ip IP to check.
	 * @param int|string $maskIp Radical of the mask (e.g. 192.168.0.0).
	 * @param int $maskBits Size of the mask (e.g. 24).
	 * @returns boolean matches?
	 */
	public static function matchIpMask($ip, $maskIp, int $maskBits): bool
	{
		$mask =~(pow(2, 32-$maskBits)-1);
		if (false === is_int($ip)) {
			$ip = ip2long($ip);
		}
		if (!is_int($maskIp)) {
			if (ctype_digit($maskIp)) {
				$maskIp = (int) $maskIp;
			} else {
				$maskIp = ip2long($maskIp);
			}
		}
		if (($ip & $mask) === ($maskIp & $mask)) {
			return true;
		}
		return false;
	}

	public static function matchIpFilter($ip, ?string $filterString): bool
	{
		$filters = preg_split('/[,\s]+/', $filterString, null, PREG_SPLIT_NO_EMPTY);
		// expand ranges
		$expandedFilters = [];
		foreach ($filters as $filter) {
			if (strpos($filter, '-') === false) {
				$expandedFilters[] = $filter;
			} else {
				$packs = explode('.', $filter);
				$expanded = [[]];
				foreach ($packs as $pack) {
					if (preg_match('/^(\d+)-(\d+)$/', $pack, $m)) {
						$size = count($expanded);
						for ($i=0; $i<$size; $i++) {
							if ($m[1] < $m[2]) {
								$e = $expanded[$i];
								$expanded[$i][] = $m[1];
								for ($r=$m[1]+1; $r<=$m[2]; $r++) {
									$f = $e;
									$f[] = $r;
									$expanded[] = $f;
								}
							}
						}
					} else {
						for ($i=0; $i<count($expanded); $i++) {
							$expanded[$i][] = $pack;
						}
					}
				}
				foreach ($expanded as $e) {
					$expandedFilters[] = join('.', $e);
				}
			}
		}
		foreach ($expandedFilters as $filter) {
			// normal or incomplete IPv4
			if (preg_match('/^\d+\.(\d{1,3}\.)?(\d{1,3}\.)?(\d{1,3}|\*)?$/', $filter)) {
				if (substr($filter, -1) === '.' || substr($filter, -1) === '*') {
					$filter = rtrim($filter, '*');
					if (strncmp($ip, $filter, strlen($filter)) === 0) {
						return true;
					}
				} else {
					if (strcmp($ip, $filter) === 0) {
						return true;
					}
				}
			} elseif (preg_match('#^([\d.]+)/(\d+)$#', $filter, $match)) {  // CIDR
				if (Tools::matchIpMask($ip, $match[1], $match[2])) {
					return true;
				}
			} elseif ($ip === $filter) {                                     // IPv6
				return true;
			}
		}
		return false;
	}

	/**
	 * Prints a pagination list, with potential sub-pagination.
	 *
	 * @param CDataProvider $dataProv
	 */
	public static function printSubPagination(\CDataProvider $dataProv): void
	{
		if (empty($dataProv->pagination)) {
			return;
		}
		echo '<div class="pagination">';
		if ($dataProv->pagination instanceof CLinkPager) {
			$dataProv->pagination->run();
		}
		echo '</div>';
		if (($dataProv->pagination instanceof PagerAlpha) && $dataProv->pagination->subPagination) {
			echo '<div class="pagination subpagination">'
				, $dataProv->pagination->subPagination
				, '</div>';
		}
	}

	public static function dateFrToIso(string $frDate): string
	{
		$monthToInt = [
			'janvier' => '01',
			'janv' => '01',
			'jan' => '01',
			'février' => '02',
			'févr' => '02',
			'fév' => '02',
			'mars' => '03',
			'mar' => '03',
			'avril' => '04',
			'avr' => '04',
			'mai' => '05',
			'juin' => '06',
			'juillet' => '07',
			'juil' => '07',
			'août' => '08',
			'septembre' => '09',
			'sept' => '09',
			'sep' => '09',
			'octobre' => '10',
			'oct' => '10',
			'novembre' => '11',
			'nov' => '11',
			'décembre' => '12',
			'déc' => '12',
		];
		$fr = trim($frDate, ' -"');
		if (preg_match('/^\d{4}(-\d\d){0,2}$/', $fr)) {
			return $fr;
		}
		$parts = explode(" ", $fr);
		if (count($parts) === 1) {
			return $fr;
		}
		if (count($parts) === 2) {
			array_unshift($parts, '');
		}
		if (count($parts) !== 3) {
			return $fr;
		}
		if (ctype_digit($parts[1])) {
			$iso = array_reverse($parts);
		}
		if (isset($monthToInt[strtolower($parts[1])])) {
			$iso = [
				$parts[2],
				$monthToInt[strtolower($parts[1])],
				$parts[0],
			];
		} else {
			Yii::app()->user->setFlash('warning', "Le mois '{$parts[1]}' n'est pas valide.");
			$iso = [$parts[2], '01', $parts[0]];
		}
		return trim(join('-', $iso), '-');
	}

	public static function normalizeText(?string $text): ?string
	{
		if (empty($text)) {
			return $text;
		}
		static $chars = [
			"\x00", "\x01", "\x02", "\x03", "\x04", "\x05", "\x06", "\x07", "\x08", "\x09",
			"\x0B", "\x0C",         "\x0E", "\x0F",
			"\x10", "\x11", "\x12", "\x13", "\x14", "\x15", "\x16", "\x17", "\x18", "\x19",
			"\x1A", "\x1B", "\x1C", "\x1D", "\x1E", "\x1F",
			"\xe2\x80\x8e", // left-to-right
		];
		if (extension_loaded('intl')) { // UTF-8 normalization
			return Normalizer::normalize(str_replace($chars, '', $text));
		}
		return str_replace($chars, '', $text);
	}

	/**
	 * Read a text like "2007 2008-06" and return timestamps like [TS(2007-01-01), TS(2008-06-30)].
	 *
	 * @param string|null $range
	 * @return array|null
	 */
	public static function parseDateInterval(?string $range): ?array
	{
		if (!$range) {
			return null;
		}
		$interval = trim($range);
		if ($interval === '') {
			return null;
		}
		$m = [];
		if (preg_match('/^(\d{4}[\d-]*)$/', $interval, $m)) {
			list($start, $end) = [$m[1], $m[1]];
		} elseif (preg_match('/^>\s*(\d{4}[\d-]*)$/', $interval, $m)) {
			list($start, $end) = [$m[1], ''];
		} elseif (preg_match('/^<\s*(\d{4}[\d-]*)$/', $interval, $m)) {
			list($start, $end) = ['', $m[1]];
		} elseif (preg_match('/^(\d{4})?\s*-\s*(\d{4})?$/', $interval)) {
			list($start, $end) = preg_split('/\s*-\s*/', $interval);
		} elseif ($interval[0] === '-') {
			$start = '';
			$end = trim($interval, trim('-'));
		} elseif (preg_match('/-$/', $interval)) {
			$start = trim($interval, trim('-'));
			$end = '';
		} elseif (strpos($interval, ' ') !== false) {
			list($start, $end) = preg_split('/\s+/', $interval, 2);
		} else {
			$start = $interval;
			$end = '';
		}
		return [
			DateVersatile::convertdateToTs($start, false),
			DateVersatile::convertdateToTs($end, true),
		];
	}

	public static function dateIntervalToSqlCondition(?string $range, string $columnName): string
	{
		$ts = self::parseDateInterval($range);
		if (!$ts) {
			return "";
		}
		if (empty($ts[0])) {
			return "$columnName < {$ts[1]}";
		} elseif (empty($ts[1])) {
			return "$columnName > {$ts[0]}";
		} else {
			return "$columnName BETWEEN {$ts[0]} AND {$ts[1]}";
		}
	}
}
