<?php

class ProfileLogRoute extends CProfileLogRoute
{
	/**
     * Displays the log messages.
     * @param array $logs list of log messages
     */
    public function processLogs($logs)
	{
		if (!self::isHtmlResponse()) {
			return;
		}
		return parent::processLogs($logs);
	}

	private static function isHtmlResponse()
	{
		foreach (headers_list() as $header) {
			if (strncasecmp($header, "Content-type:", 13) === 0) {
				return strpos($header, "text/html", 13) !== false;
			}
		}
		return true;
	}
}
