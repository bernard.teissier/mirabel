<?php

namespace components\sphinx;

class Schema extends \CMysqlSchema
{
	protected function createCommandBuilder()
    {
        return new CommandBuilder($this);
    }

	/**
	 * Collects the table column metadata.
	 *
	 * @param CMysqlTableSchema $table the table metadata
	 * @return bool whether the table exists in the database
	 */
	protected function findColumns($table)
	{
		$sql = 'DESCRIBE ' . $table->rawName;
		try {
			$columns = $this->getDbConnection()->createCommand($sql)->queryAll();
		} catch (\Exception $_) {
			return false;
		}
		foreach ($columns as $column) {
			$c = $this->createColumn($column);
			if ($c !== null) {
				$table->columns[$c->name] = $c;
				if ($c->isPrimaryKey) {
					$table->primaryKey = $c->name;
				}
			}
		}
		return true;
	}

	/**
	 * Creates a table column.
	 * @param array $column column metadata
	 * @return ?CDbColumnSchema normalized column metadata
	 */
	protected function createColumn($column)
	{
		$c = new \CMysqlColumnSchema;
		$c->name = $column['Field'];
		$c->rawName = $this->quoteColumnName($c->name);
		$c->isPrimaryKey = ($column['Field'] === 'id');
		switch ($column['Type']) {
			case 'bigint':
				$c->init('int', 0);
				break;
			case 'field':
				return null;
			case 'mva':
				$c->dbType = 'mva';
				$c->allowNull = false;
				$c->defaultValue = [];
				$c->type = 'int[]';
				break;
			case 'timestamp':
				$c->init('int', 0);
				break;
			case 'uint':
				$c->init('unsigned int', 0);
				break;
			default:
				$c->init('text', '');
				break;
		}
		return $c;
	}

	/**
	 * Collects the foreign key column details for the given table.
	 * @param CMysqlTableSchema $table the table metadata
	 */
	protected function findConstraints($table)
	{
	}

	public function quoteTableName($name)
	{
		if ($name === '')  {
			return '';
		}
		return parent::quoteColumnName($name);
	}

	public static function quote($value)
	{
		if (\is_int($value) || \is_float($value)) {
			return $value;
		}

		$from = ['\\', '(', ')', '|', '-', '!', '@', '~', '"', '&', '/', '^', '$', '=', '<', "'"];
		$to   = ['\\\\', '\(', '\)', '\|', '\-', '\!', '\@', '\~', '\"', '\&', '\/', '\^', '\$', '\=', '\<', ' '];
		return str_replace($from, $to, $value);
	}

	protected function resolveTableNames($table, $name)
	{
		parent::resolveTableNames($table, $name);
		$table->rawName = $table->name;
	}
}
