<?php

namespace components\sphinx;

class DataProvider extends \CActiveDataProvider
{
	/**
	 * @var ?\CActiveRecord If set, this ActiveRecord model will be used to fetch source records from \Yii::app()->db
	 */
	public $source;

	/**
	 * Sphinx Search has an implicit 0,20 limit that we must overwrite.
	 */
	private const PAGINATION_MAX = 10000;

	/**
	 * @inheritdoc
	 */
	public function fetchData(): array
	{
		$data = parent::fetchData();
		if ($this->source && $data) {
			for ($i = 0; $i < count($data); $i += 500) {
				$this->fillData($data, $i, $i+500);
			}
		}
		return $data;
	}

	private function fillData(array &$data, int $from, int $to): void
	{
		$ids = [];
		if ($to > count($data)) {
			$to = count($data);
		}
		for ($i = $from; $i < $to; $i++) {
			$ids[] = (int) $data[$i]->id;
		}
		$criteria = new \CDbCriteria();
		$criteria->addCondition('id IN (' . join(',', $ids) . ')');
		$criteria->index = 'id';
		$sources = $this->source->findAll($criteria);
		for ($i = $from; $i < $to; $i++) {
			if (isset($sources[$data[$i]->id])) {
				$data[$i]->setRecord($sources[$data[$i]->id]);
			}
		}
	}

	/**
	 * @inheritdoc
	 */
	public function setPagination($pagination): void
	{
		if ($pagination === false) {
			$pagination = ['pageSize' => self::PAGINATION_MAX];
		}
		parent::setPagination($pagination);
	}
}
