<?php

namespace components\sphinx;

class CommandBuilder extends \CDbCommandBuilder
{
	public function applyLimit($sql, $limit, $offset)
	{
		if ($limit >= 0) {
			if ($offset > 0) {
				$sql .= ' LIMIT ' . (int) $offset . ',' . (int) $limit;
			} else {
				$sql .= ' LIMIT ' . (int) $limit;
			}
			if ($limit > 1000) {
				$sql .= " OPTION max_matches=$limit";
			}
		}
		return $sql;
	}

   /**
     * Creates a SELECT command for a single table. __Ignores the alias.__
     *
     * @param string|\CDbTableSchema $table the table schema or the table name.
     * @param \CDbCriteria $criteria the query criteria
     * @param string $alias the alias name of the primary table. Defaults to 't'.
     * @return \CDbCommand query command.
     */
    public function createFindCommand($table, $criteria, $alias = 't')
    {
        $this->ensureTable($table);
        $select = is_array($criteria->select) ? implode(', ', $criteria->select) : $criteria->select;
        $sql = ($criteria->distinct ? 'SELECT DISTINCT' : 'SELECT') . " {$select} FROM {$table->rawName}";
        $sql = $this->applyJoin($sql, $criteria->join);
        $sql = $this->applyCondition($sql, $criteria->condition);
        $sql = $this->applyGroup($sql, $criteria->group);
        $sql = $this->applyHaving($sql, $criteria->having);
        $sql = $this->applyOrder($sql, $criteria->order);
        $sql = $this->applyLimit($sql, $criteria->limit, $criteria->offset);
        $command = $this->dbConnection->createCommand($sql);
        $this->bindValues($command, $criteria->params);
        return $command;
    }

	public function createColumnCriteria($table,$columns,$condition='',$params=array(),$prefix=null)
	{
		return parent::createColumnCriteria($table, $columns, $condition, $params, '');
	}

	public function createCountCommand($table,$criteria,$alias='t')
	{
		return parent::createCountCommand($table, $criteria, '');
	}

	public function createInCondition($table, $columnName, $values, $prefix = null)
    {
        return parent::createInCondition($table, $columnName, $values, '');
    }
}
