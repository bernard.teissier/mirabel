<?php

require_once __DIR__ . '/WebApplication.php';

class Yii extends YiiBase
{
	public static function createWebApplication($config = null): WebApplication
	{
		$app = new WebApplication($config);
		self::initSessions();
		return $app;
	}

	public static function app(): WebApplication
	{
		$app = parent::app();
		if (!($app instanceof \WebApplication)) {
			throw new \Exception("Wrong application: " . $app->getId());
		}
		return $app;
	}

	private static function initSessions(): void
	{
		$dir = self::getPathOfAlias('application.runtime.sessions');
		if (!$dir) {
			throw new \Exception("Wrong session config.");
		}
		if (!is_dir($dir)) {
			@mkdir($dir);
		}
	}
}
