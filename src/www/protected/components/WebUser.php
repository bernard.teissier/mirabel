<?php
/**
 * The WebUser class.
 */

/**
 * Yii::app()->user will return an instance of this for the current user.
 * Persistent properties are set by UserIdentity with its setState() method,
 * they can be fetched through:
 *     Yii::app()->user->getState('zzz')
 * or
 *     Yii::app()->user->zzz
 *
 * @property int $partenaireId
 * @property string $partenaireType
 * @property string $login
 * @property bool $actif
 * @property int $derConnexion timestamp
 * @property int $hdateCreation timestamp
 * @property int $hdateModif timestamp
 * @property bool $permAdmin
 * @property bool $permImport
 * @property bool $permPartenaire
 * @property bool $permIndexation
 * @property bool $suiviEditeurs
 * @property bool $suiviNonSuivi
 * @property array   $suivi
 */
class WebUser extends CWebUser
{
	private $instituteId = 0;

	/**
	 * Always called when a session begins.
	 */
	public function init()
	{
		parent::init();
		if (PHP_SAPI !== 'cli' && !isset($_COOKIE['institute'])) {
			if (isset($_SERVER['REMOTE_ADDR'])) {
				$this->setInstituteById(Partenaire::findInstituteByIp($_SERVER['REMOTE_ADDR']));
			} else {
				$this->setInstitute(null);
			}
		}
		if (!$this->hasState('partenaireType')) {
			$this->setState('partenaireType', '');
		}
	}

	/**
	 * Return the default partenaireId for the current user.
	 *
	 * If a guest, IP is used. If authenticated, user.partenaireId.
	 * Else null.
	 *
	 * @return int|null
	 */
	public function getDefaultInstituteId(): ?int
	{
		if (PHP_SAPI === 'cli' || empty($_SERVER['REMOTE_ADDR'])) {
			return null;
		}
		if ($this->getState('partenaireId')) {
			return (int) $this->getState('partenaireId');
		}
		$p = Partenaire::findInstituteByIp($_SERVER['REMOTE_ADDR']);
		return $p ?? null;
	}

	/**
	 * @return int
	 */
	public function getInstitute(): int
	{
		if ($this->instituteId > 0) {
			return (int) $this->instituteId;
		}
		if (isset($_COOKIE['institute'])) {
			return (int) $_COOKIE['institute'];
		}
		return 0;
	}

	public function getInstituteName(): ?string
	{
		return $this->getState('institute_name');
	}

	public function getInstituteShortname(): ?string
	{
		return $this->getState('institute_shortname');
	}

	public function setInstituteById($id): void
	{
		$this->setInstitute(Partenaire::model()->findByPk($id));
	}

	public function getPartenaireId(): ?int
	{
		$id = $this->getState('partenaireId');
		return $id ? (int) $id : null;
	}

	/**
	 * @param Partenaire|null $institute
	 */
	public function setInstitute($institute): void
	{
		if ($institute) {
			$this->instituteId = (int) $institute->id;
			setcookie('institute', (string) $institute->id, 0, '/');
			$this->setState('institute_name', $institute->nom);
			$this->setState('institute_shortname', $institute->getShortName());
		} else {
			$this->instituteId = 0;
			setcookie('institute', '', 0, '/');
			$this->setState('institute_name', null);
			$this->setState('institute_shortname', null);
		}
	}

	/**
	 * Returns true if the current user has "Suivi" on this object.
	 * @param mixed $object
	 * @return bool
	 */
	public function isMonitoring($object): bool
	{
		if ($this->isGuest) {
			return false;
		}
		if (is_object($object)) {
			$model = get_class($object);
			$id = $object->id;
		} else {
			$model = $object['model'];
			$id = $object['id'];
		}
		if (isset($this->suivi[$model]) && in_array((int) $id, $this->suivi[$model])) {
			return true;
		}
		return false;
	}

	/**
	 * Overrides a Yii method that is used for roles in controllers (accessRules).
	 *
	 * @param string $operationName Name of the operation required (import, suivi...).
	 * @param mixed $params (opt) Parameters for this operation, usually the object to access.
	 * @param bool $allowCaching
	 * @return bool Permission granted?
	 */
	public function checkAccess($operationName, $params = [], $allowCaching = true)
	{
		if (empty($this->id)) {
			// Not identified => no rights
			return false;
		}
		if ($this->getState("permAdmin")) {
			return true;
		}
		$operation = strtolower($operationName);
		switch ($operation) {
			case 'abonnement/update': // params: Partenaire|partenaireId
				// $params is the ID of the Partenaire to modify, or the full object
				if (empty($params)) {
					return false;
				}
				if (is_array($params)) {
					$params = $params[0];
				}
				if (is_object($params) && $params instanceof Partenaire) {
					$partenaireId = (int) $params->id;
					$model = $params;
				} else {
					$partenaireId = (int) $params;
					$model = Partenaire::model()->findByPk($partenaireId);
				}
				// partenaire-éditeur ?
				if ($model->editeurId && $this->getState('suiviPartenairesEditeurs')) {
					return true;
				}
				// partenaire de l'utilisateur courant ?
				return (int) $this->getState('partenaireId') === $partenaireId;

			case 'collection/create':
			case 'collection/update':
				return Suivi::checkDirectAccess($params); // Ressource

			case 'editeur/create-direct':
				return $this->checkAccess('suiviEditeurs');
			case 'editeur/roles':
				return $this->getState('partenaireId') > 0;
			case 'editeur/logo':
				// Ticket #4612
				if (empty($params['Editeur']) || !($params['Editeur'] instanceof Editeur)) {
					throw new \Exception("Coding error: expected Editeur instance, not " . print_r($params, true));
				}
				$p = Partenaire::model()->findByAttributes(['editeurId' => $params['Editeur']->id]);
				if (!$p) {
					return false; // permission editeur/logo applies only to a partenaire-editeur
				}
				if ($this->getState('suiviPartenairesEditeurs')) {
					return true;
				}
				return ((int) $p->id === (int) $this->getState('partenaireId'));
			case 'editeur/delete':
			case 'editeur/logo':
			case 'editeur/update-direct':
				if (empty($params['Editeur']) || !($params['Editeur'] instanceof Editeur)) {
					throw new \Exception("Coding error: expected Editeur instance, not " . print_r($params, true));
				}
				if (Partenaire::model()->findByAttributes(['editeurId' => $params['Editeur']->id])) {
					// éditeur-partenaire
					return (bool) $this->getState('suiviPartenairesEditeurs');
				}
				return $this->checkAccess('suiviEditeurs') || Suivi::checkDirectAccess($params['Editeur']);

			case 'import':
				return $this->getState('permImport');

			case 'menu-veilleur':
				return $this->getState('partenaireType') === 'normal';

			case 'partenaire/admin':
				if (empty($params['Partenaire']) || !($params['Partenaire'] instanceof Partenaire)) {
					return false;
				}
				if ($params['Partenaire']->editeurId) {
					return $this->checkAccess('suiviPartenairesEditeurs');
				}
				return false;
			case 'partenaire/customize': // params: Partenaire|partenaireId
				// granted if the current user is a member of this Partenaire
				if (empty($params['Partenaire']) && empty($params['partenaireId'])) {
					return false;
				}
				if (!empty($params['Partenaire']) && $params['Partenaire'] instanceof Partenaire) {
					$partenaireId = (int) $params['Partenaire']->id;
				} elseif (!empty($params['partenaireId'])) {
					$partenaireId = (int) $params['partenaireId'];
				} else {
					return false;
				}
				return (int) Yii::app()->user->partenaireId === $partenaireId;
			case 'partenaire/editorial':
				// granted if member of this Partenaire, with U.permPartenaire
				if (isset($params['partenaireId']) && $params['partenaireId'] != $this->getState('partenaireId')) {
					return false;
				}
				return $this->getState('permPartenaire');
			case 'partenaire/logo':
				// granted if member of this Partenaire, with U.permPartenaire
				if (isset($params['partenaireId']) && $params['partenaireId'] != $this->getState('partenaireId')) {
					return false;
				}
				return $this->getState('permPartenaire');
			case 'partenaire/suivi':
				if ($this->checkAccess('partenaire/admin', $params)) {
					return true;
				}
				if ($params['Partenaire']->editeurId) {
					return $this->checkAccess('suiviPartenairesEditeurs');
				}
				return $this->getState('partenaireId') == $params['Partenaire']->id;
			case 'Partenaire':
				// $params is the ID of the Partenaire to modify, or the full object
				if (empty($params)) {
					return $this->getState('permPartenaire') && $this->getState('partenaireType') === 'normal';
				}
				if (is_array($params)) {
					$params = $params[0];
				}
				if (is_object($params) && $params instanceof Partenaire) {
					$partenaireId = (int) $params->id;
				} else {
					$partenaireId = (int) $params;
				}
				// partenaire de l'utilisateur courant ?
				if (
					$this->getState('partenaireId') == $partenaireId
					&& $this->getState('permPartenaire')
					) {
					return true;
				}
				// partenaire-éditeur ?
				if (is_object($params)) {
					$model = $params;
				} else {
					$model = Partenaire::model()->findByPk($partenaireId);
				}
				return $model->editeurId && $this->getState('suiviPartenairesEditeurs');
			case 'partenaire/update':
			case 'partenaire/utilisateurs':
				$partenaire = null;
				if (isset($params['Partenaire']) && $params['Partenaire'] instanceof Partenaire) {
					$partenaire = $params['Partenaire'];
				}
				if (isset($params['partenaireId']) && isset($params['partenaireId']) > 0) {
					$partenaire = Partenaire::model()->findByPk((int) $params['partenaireId']);
				}
				// partenaire de l'utilisateur courant, avec permPartenaire ?
				if ($this->getState('permPartenaire')) {
					if ($partenaire === null) {
						return $this->getState('partenaireType') === 'normal';
					}
					if ((int) $this->getState('partenaireId') === (int) $partenaire->id) {
						return true;
					}
				}
				// partenaire-éditeur, avec suiviPartenairesEditeurs ?
				return $partenaire !== null && $partenaire->editeurId && $this->getState('suiviPartenairesEditeurs');
			case 'partenaire/upload':
				if (isset($params['partenaireId']) && $params['partenaireId'] != $this->getState('partenaireId')) {
					return false;
				}
				return $this->getState('permPartenaire');
			case 'partenaire/view':
				return (int) $this->getState('partenaireId') === (int) $params['partenaireId'];

			case 'possession/update':
				if (isset($params['Partenaire']) && $params['Partenaire'] instanceof Partenaire) {
					return $params['Partenaire']->type === 'normal'
						&& $this->getState('partenaireId') == $params['Partenaire']->id;
				}
				return $this->getState('partenaireType') === 'normal';

			case 'redaction/aide':
				return false;
			case 'redaction/editorial':
				return false;

			case 'ressource/create-direct':
				return $this->getState('partenaireType') !== 'editeur';
			case 'ressource/update-direct':
				if (empty($params['Ressource']) || !($params['Ressource'] instanceof Ressource)) {
					throw new \Exception("Coding error: expected Ressource instance, not " . print_r($params, true));
				}
				return Suivi::checkDirectAccess($params['Ressource']);

			case 'revue/update-direct':
				if (empty($params['Revue']) || !($params['Revue'] instanceof Revue)) {
					throw new \Exception("Coding error: expected Revue instance, not " . print_r($params, true));
				}
				return Suivi::checkDirectAccess($params['Revue']);

			case 'service/admin':
				return false;
			case 'service/create-direct':
				return Suivi::checkDirectAccessByIds($params['revueId'], $params['ressourceId']);
			case 'service/update-direct':
				if (empty($params['Service']) || !($params['Service'] instanceof Service)) {
					throw new \Exception("Coding error: expected Service instance, not " . print_r($params, true));
				}
				return Suivi::checkDirectAccess($params['Service']);

			case 'stats/partenaire':
				if (empty($params['Partenaire'])) {
					return false;
				}
				 return (int) $params['Partenaire']->id === (int) $this->getState('partenaireId');
			case 'stats/suivi':
				return false;

			case 'suivi': // params: Partenaire | Suivi
				if (get_class($params) === 'Partenaire') {
					return $this->getState('partenaireId') == $params->id;
				}
				if (get_class($params) === 'Suivi') {
					return $this->getState('partenaireId') == $params->partenaireId;
				}
				// idem 'Suivi'
				// $params is the target object to verify
				if ($this->getState('partenaireType') === 'normal' && !Suivi::isTracked($params)) {
					return true;
				}
				return $this->isMonitoring($params);

			case 'titre/create-direct':
				if (empty($params['revueId'])) {
					return true; // auth user can create a Revue
				}
				return Suivi::checkDirectAccessByIds((int) $params['revueId'], null);
			case 'titre/update-direct':
				if (empty($params['Titre']) || !($params['Titre'] instanceof Titre)) {
					throw new \Exception("Coding error: expected Titre instance, not " . print_r($params, true));
				}
				return Suivi::checkDirectAccess($params['Titre']);

			case 'utilisateur/create':
				$partenaire = null;
				if (isset($params['Partenaire']) && $params['Partenaire'] instanceof Partenaire) {
					$partenaire = $params['Partenaire'];
				}
				if (isset($params['partenaireId']) && isset($params['partenaireId']) > 0) {
					$partenaire = Partenaire::model()->findByPk((int) $params['partenaireId']);
				}
				if ($partenaire === null) {
					return false;
				}
				return $this->checkAccess('partenaire/utilisateurs', ['Partenaire' => $partenaire]);
			case 'utilisateur/liste-diffusion':
				$target = $params['Utilisateur'];
				return !empty($target->partenaireId)
					&& $target->partenaire->type === 'normal'
					&& (int) Yii::app()->user->id === (int) $target->id; // updating himself
			case 'utilisateur/update':
				if ((int) Yii::app()->user->id === (int) $params['Utilisateur']->id) {
					return true; // updating himself
				}
				return $this->checkAccess('utilisateur/create', ['Partenaire' => $params['Utilisateur']->partenaire]);
			case 'utilisateur/update-self':
				$target = $params['Utilisateur'];
				return !empty($target->partenaireId)
					&& (int) Yii::app()->user->id === (int) $target->id; // updating himself
			case 'utilisateur/view':
				if (empty($params)) {
					return false;
				}
				if (!($params instanceof Utilisateur)) {
					throw new Exception("coding error");
				}
				if ($params->id && $params->id == $this->id) {
					return true; // view self
				}
				if (!empty($params->partenaireId) && $params->partenaire->editeurId && $this->getState('suiviPartenairesEditeurs')) {
					return true;
				}
				return false;

			case 'validate':
				return Suivi::checkDirectAccess($params);
			case 'verify':
				// $params is the target object to verify
				if ($this->getState('partenaireType') === 'normal' && !Suivi::isTracked($params)) {
					return true;
				}
				return $this->isMonitoring($params);

			default:
				if ($this->hasState('perm' . ucfirst($operation))) {
					return $this->getState('perm' . ucfirst($operation));
				}
				if ($this->hasState($operationName)) {
					return $this->getState($operationName);
				}
				Yii::log("checkAccess() pour une opération inconnue '$operation'", CLogger::LEVEL_WARNING);

		}
		return false;
	}

	public function buildConditionSuivi()
	{
		if ($this->isGuest) {
			throw new Exception("Erreur fatale de permission");
		}
		if (empty($this->suivi) && !$this->suiviEditeurs && !$this->suiviNonSuivi) {
			return '1=0';
		}
		$condition = [];
		if (!empty($this->suivi['Revue'])) {
			$condition[] = "(t.revueId IN (" . join(',', $this->suivi['Revue']) . "))";
		}
		if (!empty($this->suivi['Ressource'])) {
			$condition[] = "(t.ressourceId IN (" . join(',', $this->suivi['Ressource']) . "))";
		}
		if (!empty($this->suivi['Editeur'])) {
			$condition[] = "(t.editeurId IN (" . join(',', $this->suivi['Editeur']) . "))";
		}
		if (!empty($this->suiviEditeurs)) {
			$condition[] = "(t.editeurId IS NOT NULL)";
		}
		if (!empty($this->suiviNonSuivi)) {
			$condition[] = "(s.id IS NULL)";
		}
		if (empty($condition)) {
			return null;
		}
		return join(' OR ', $condition);
	}
}
