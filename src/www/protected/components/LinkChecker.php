<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class LinkChecker
{
	public const RESULT_NOTFOUND = 0;

	public const RESULT_SUCCESS = 1;

	public const RESULT_FORBIDDEN = 2;

	public const RESULT_IGNORED = 3;

	public const RESULT_MALFORMED = 4;

	public const RESULT_EMPTY = 5;

	public const DESCRIPTION = [
		self::RESULT_NOTFOUND => "non trouvée",
		self::RESULT_SUCCESS => "OK",
		self::RESULT_FORBIDDEN => "domaine interdit",
		self::RESULT_IGNORED => "domaine ignoré",
		self::RESULT_MALFORMED => "format d'URL non valide",
		self::RESULT_EMPTY => "URL vide",
	];

	public static $ignorePattern = '#^https?://([\w.]+\.)?(linkedin\.com|jstor\.org|brepolsonline\.net)#';

	public static $forbidDomains;

	/**
	 * @var int Max number of concurrent HTTP requests
	 */
	public $maxThreads = 8;

	/**
	 * @var int Wait (ms) between request groups.
	 */
	public $wait = 0;

	/**
	 * @var string Extra description of the last error
	 */
	protected $lastError = "";

	protected $curl;

	protected $timeout = 10;

	protected static $stats = [
		'urlsNum' => 0,
		'answersNum' => [], // each value will be #URLs treated in parallel
		'answersTimes' => [],
		'successes' => 0,
	];

	/**
	 * Constructor.
	 */
	public function __construct()
	{
		if (extension_loaded('curl') === false) {
			throw new Exception("L'extension cURL n'est pas installée sur le serveur.");
		}
		if (self::$forbidDomains === null) {
			// forbidden domains from the proxy used
			self::$forbidDomains = array_unique(array_filter(array_map(
				function ($url) {
					$m = [];
					if (preg_match('#//([^/]+)#', $url, $m)) {
						return $m[1];
					}
				},
				Yii::app()->db->createCommand("SELECT proxyUrl FROM Partenaire WHERE proxyUrl <> ''")->queryColumn()
			)));
			// forbidden domains from the Config table
			$extra = Config::read('validate.forbid.domains');
			// mix both sources
			if ($extra) {
				foreach (explode("\n", $extra) as $dom) {
					$d = trim($dom);
					if ($d) {
						self::$forbidDomains[] = $d;
					}
				}
			}
		}
	}

	/**
	 * Sets the timeout.
	 *
	 * @param int $seconds
	 */
	public function setTimeout(int $seconds)
	{
		$this->timeout = $seconds;
		if (!$this->curl) {
			$this->curl = $this->buildCurl();
		}
		curl_setopt($this->curl, CURLOPT_TIMEOUT, (int) $seconds);
	}

	/**
	 * Returns true is the URL can be fetched (HTTP HEAD request).
	 *
	 * @param string $url
	 * @return array [int self::RESULT_*, string description]
	 */
	public function checkLink(string $url)
	{
		if (!$url) {
			return [self::RESULT_EMPTY, self::DESCRIPTION[self::RESULT_EMPTY]];
		}
		$cleanUrl = self::cleanupUrl(trim($url));
		$invalid = $this->validateUrl($cleanUrl);
		if ($invalid) {
			return [$invalid, $this->lastError ?: self::DESCRIPTION[$invalid]];
		}
		return $this->checkValidUrl($cleanUrl);
	}

	/**
	 * Checks each URL in a list and return a [RESULT_*, description] for each.
	 *
	 * @param array $urls array of strings.
	 * @return array assoc array mapping URLs to self::RESULT_*: (url => [int, description])
	 */
	public function checkLinks(array $urls)
	{
		$results = [];
		if (empty($urls)) {
			return [];
		}

		// treat invalid URLs first
		$remaining = [];
		foreach ($urls as $rawUrl) {
			$url = self::cleanupUrl($rawUrl);
			$invalid = $this->validateUrl($url);
			if ($invalid) {
				$results[$rawUrl] = [
					$invalid,
					$this->lastError ?: self::DESCRIPTION[$invalid],
				];
			} else {
				$remaining[$rawUrl] = $url;
			}
		}

		// cURL for the remaining URLs
		if (count($remaining) === 1) {
			foreach ($remaining as $rawUrl => $url) {
				$results[$rawUrl] = $this->checkValidUrl($url);
			}
		} elseif (count($remaining) <= $this->maxThreads) {
			$results = array_merge($results, $this->checkLinksParallel($remaining));
		} else {
			foreach (array_chunk($remaining, $this->maxThreads, true) as $chunk) {
				$results = array_merge($results, $this->checkLinksParallel($chunk));
				if ($this->wait) {
					usleep(1000 * $this->wait);
				}
			}
		}

		return $results;
	}

	/**
	 * Closes.
	 */
	public function close()
	{
		if ($this->curl) {
			curl_close($this->curl);
		}
	}

	/**
	 * Checks each URL in a list and return a boolean for each.
	 *
	 * @param array|string $urls URL or array of URL.
	 * @return array array: (url => boolean)
	 */
	public static function check($urls): array
	{
		$new = new self;
		if (is_array($urls)) {
			$result = $new->checkLinks($urls);
		} else {
			$result = $new->checkLink($urls);
		}
		$new->close();
		unset($new);
		return $result;
	}

	public static function getStats(): array
	{
		$stats = self::$stats;
		return [
			'urlsNum' => $stats['urlsNum'],
			'batches' => count($stats['answersTimes']),
			'answersNum' => array_sum($stats['answersNum']),
			'overallTime' => array_sum($stats['answersTimes']),
			'slowChecks' => count(array_filter(
				$stats['answersTimes'],
				function ($x) {
					return $x > 5;
				}
			)),
			'errorsNum' => $stats['urlsNum'] - $stats['successes'],
		];
	}

	protected function checkValidUrl(string $url)
	{
		$startTime = self::startTimer(1);
		if (!$this->curl) {
			$this->curl = $this->buildCurl();
		}
		curl_setopt($this->curl, CURLOPT_URL, $url);
		curl_exec($this->curl);
		self::stopTimer(1, $startTime);
		if (curl_getinfo($this->curl, CURLINFO_HTTP_CODE) == 200) {
			return [self::RESULT_SUCCESS, self::DESCRIPTION[self::RESULT_SUCCESS]];
		}
		$this->lastError = curl_error($this->curl);
		return [self::RESULT_NOTFOUND, $this->lastError ?: self::DESCRIPTION[self::RESULT_NOTFOUND]];
	}

	/**
	 * Offline validation.
	 *
	 * @param string $url
	 * @return ?int RESULT_* if not valid, null if valid
	 */
	protected function validateUrl(string $url)
	{
		$this->lastError = "";
		if (!$url) {
			return self::RESULT_EMPTY;
		}

		if (!preg_match('#^https?://#', $url)) {
			return self::RESULT_MALFORMED;
		}

		if (self::$ignorePattern && preg_match(self::$ignorePattern, $url)) {
			return self::RESULT_IGNORED;
		}

		$m = [];
		if (self::$forbidDomains && preg_match('#//([^/]+)#', $url, $m)) {
			$urlDomain = $m[1];
			if (substr_count($urlDomain, '.') >= 5) {
				$this->lastError = "Un domaine à 6 composantes ou plus est probablement un proxy.";
				return self::RESULT_FORBIDDEN;
			}
			foreach (self::$forbidDomains as $forbidden) {
				if (strlen($urlDomain) >= strlen($forbidden) && substr_compare($urlDomain, $forbidden, 0 - strlen($forbidden)) === 0) {
					$this->lastError =  "Ce domaine est interdit dans les URL.";
					return self::RESULT_FORBIDDEN;
				}
			}
		}

		return null;
	}

	/**
	 * Inits a curl instance.
	 *
	 * @param string $url (opt)
	 * @return resource curl.
	 */
	protected function buildCurl(string $url = '')
	{
		$c = curl_init();
		$options = [
			CURLOPT_NOBODY => false,           // HEAD request, but breaks with FOLLOWLOC+multi
			CURLOPT_FOLLOWLOCATION => true,    // follow redirection (301, etc)
			CURLOPT_MAXREDIRS => 4,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HEADER => true,            // include the HTTP header in the response
			CURLOPT_TIMEOUT => $this->timeout,
			CURLOPT_SSL_VERIFYHOST => false,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_COOKIEFILE => "",          // store cookies
			CURLOPT_USERAGENT => 'Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0',
			CURLOPT_HTTPHEADER => [
				"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
				"Accept-Encoding: gzip, deflate",
				"Accept-Language: en,fr;q=0.9",
				"Cache-Control: no-cache",
				"Pragma:no-cache",
			],
			CURLINFO_HEADER_OUT => false,      // debug
		];
		if ($url) {
			$options[CURLOPT_URL] = $url;
		}
		curl_setopt_array($c, $options);
		return $c;
	}

	/**
	 * Remove anchors and empty strings.
	 *
	 * @param string $url
	 * @return string
	 */
	protected static function cleanupUrl(string $url): string
	{
		if ($url[0] === '/') {
			if (PHP_SAPI === 'cli') {
				$url = \Yii::app()->params->itemAt('baseUrl') . $url;
			} else {
				$url = \Yii::app()->getBaseUrl(true) . $url;
			}
		}
		return preg_replace('/#.+$/', '', $url);
	}

	protected static function startTimer(int $numUrls)
	{
		self::$stats['urlsNum'] += $numUrls;
		return microtime(true);
	}

	protected static function stopTimer(int $numAnswers, float $startTime): void
	{
		self::$stats['answersNum'][] = $numAnswers;
		self::$stats['answersTimes'][] = microtime(true) - $startTime;
	}

	/**
	 * Used by checkLinks() because we don't want to send more than $this->maxThreads requests in parallel.
	 *
	 * @param array $urls array of strings.
	 * @throws Exception
	 * @return array assoc array: (url => [RESULT_*, description])
	 */
	private function checkLinksParallel(array $urls): array
	{
		if (empty($urls)) {
			return [];
		}
		$num = count($urls);
		if (count($urls) > $this->maxThreads) {
			throw new Exception(
				"checkLinksThreaded(): No more than {$this->maxThreads} URLs expected."
			);
		}
		$startTime = self::startTimer($num);
		$curls = [];
		foreach ($urls as $k => $url) {
			$curls[$k] = $this->buildCurl($url);
			$error = curl_error($curls[$k]);
			if ($error) {
				throw new Exception("{$url} : $error");
			}
		}

		$mh = curl_multi_init(); // must occur AFTER each curl_init(), or cURL will crash!
		foreach (array_keys($urls) as $k) {
			curl_multi_add_handle($mh, $curls[$k]);
		}

		$active = null;
		do {
			do {
				$r = curl_multi_exec($mh, $active);
			} while ($r == CURLM_CALL_MULTI_PERFORM);
			if ($active) {
				curl_multi_select($mh); // wait for action, up to 1 s.
			}
		} while ($active > 0);

		$result = [];
		foreach (array_keys($urls) as $k) {
			$curl = $curls[$k];
			$code = (int) curl_getinfo($curl, CURLINFO_HTTP_CODE);
			$error = curl_error($curl);
			if ($code === 200) {
				$result[$k] = [self::RESULT_SUCCESS, self::DESCRIPTION[self::RESULT_SUCCESS]];
				self::$stats['successes']++;
			} elseif ($error) {
				$result[$k] = [self::RESULT_NOTFOUND, $error];
			} elseif (!$code) {
				$result[$k] = [self::RESULT_NOTFOUND, "Pas de réponse"];
			} else {
				$result[$k] = [self::RESULT_NOTFOUND, "Le code HTTP $code de la réponse n'est pas '200 OK'."];
			}
			curl_multi_remove_handle($mh, $curl);
			curl_close($curl);
		}
		curl_multi_close($mh);
		self::stopTimer($num, $startTime);
		return $result;
	}
}
