<?php

/**
 * UserIdentity represents the data needed to identify a user.
 *
 * It contains the authentication method that checks if the provided
 * data can identify the user.
 */
class UserIdentity extends CUserIdentity
{
	const ERROR_MAINTENANCE_NOTADMIN = 3;

	private $_id;

	/**
	 * Make 'id' a read-only attribute on '_id'.
	 *
	 * @return string Current user id.
	 */
	public function getId()
	{
		return $this->_id;
	}

	/**
	 * Authenticates a user.
	 *
	 * @return bool whether authentication succeeds.
	 */
	public function authenticate()
	{
		$user = Utilisateur::model()->find('actif = 1 AND login = ?', [$this->username]);
		if ($user === null) {
			$this->errorCode = self::ERROR_USERNAME_INVALID;
			$this->errorMessage = 'Nom ou mot de passe incorrect.';
		} elseif (!$user->validatePassword($this->password)) {
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
			$this->errorMessage = 'Nom ou mot de passe incorrect.';
		} elseif (!$user->permAdmin && \Config::read('maintenance.authentification.inactive')) {
			$this->errorCode = self::ERROR_MAINTENANCE_NOTADMIN;
			$this->errorMessage = 'En maintenance : connexion restreinte aux administrateurs';
		} else {
			// valid user and login
			$this->errorCode = self::ERROR_NONE;
			$this->_id = (string) $user->id;
			// add all fields as persistent attributes of  Yii::app()->user
			foreach ($user->attributes as $field => $value) {
				if ($field !== 'id' && $field !== 'motdepasse') {
					$this->setState($field, $value);
				}
			}
			$this->setState('suivi', $user->partenaire->getRightsSuivi(true));
			$this->setState('partenaireType', $user->partenaire->type);
			$this->setState('proxyUrl', $user->partenaire->proxyUrl);
			Yii::app()->user->setInstituteById($user->partenaireId);
		}
		return ($this->errorCode === self::ERROR_NONE);
	}

	public function getMessage(): string
	{
		return $this->errorMessage;
	}
}
