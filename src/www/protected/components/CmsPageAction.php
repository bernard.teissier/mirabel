<?php

/**
 * CmsPageAction class file.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 * @copyright Copyright © Silecs
 * @license GPL v3 Afero
 */

/**
 * The CmsPageAction class reads its content from the DB through the Cms class.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 * @copyright Copyright © Silecs
 * @license GPL v3 Afero
 */
class CmsPageAction extends CAction
{
	/**
	 * @var string the name of the GET parameter that contains the requested view name.
	 */
	public $viewParam = 'p';

	/**
	 * @var string the name of the default view when {@link viewParam} GET parameter is not provided by user.
	 */
	public $defaultView = 'Actualité';

	/**
	 * @var mixed the name of the layout to be applied to the views.
	 * This will be assigned to CController::layout before the view is rendered.
	 * Defaults to null, meaning the controller's layout will be used.
	 * If false, no layout will be applied.
	 */
	public $layout;

	private $viewName;

	/**
	 * Returns the name of the view requested by the user.
	 *
	 * If the user doesn't specify any view, the {@link defaultView} will be returned.
	 *
	 * @return string the name of the view requested by the user.
	 */
	public function getRequestedView()
	{
		if ($this->viewName === null) {
			if (!empty($_GET[$this->viewParam])) {
				$this->viewName = $_GET[$this->viewParam];
			} else {
				$this->viewName = $this->defaultView;
			}
		}
		return $this->viewName;
	}

	/**
	 * Displays the view requested by the user.
	 *
	 * @throws CHttpException if the view is invalid
	 */
	public function run()
	{
		$block = Cms::model()->findByAttributes(
			['singlePage' => 1, 'name' => $this->getRequestedView()]
		);
		if (!$block) {
			$block = Cms::model()->findByAttributes(
				['singlePage' => 1, 'name' => str_replace('_', ' ', $this->getRequestedView())]
			);
		}
		if (!$block) {
			throw new CHttpException(404, "Page non trouvée");
		}
		$controller = $this->getController();
		if ($block->private && Yii::app()->user->isGuest) {
			Yii::app()->user->loginRequired();
		}
		if ($this->layout !== null) {
			$layoutBackup = $controller->layout;
			$controller->layout = $this->layout;
		}

		$controller->pageTitle = $block->pageTitle;
		if ($controller instanceof \Controller) {
			$controller->breadcrumbs = [$block->pageTitle];
		}
		$controller->renderText('<div id="page-' . CHtml::encode($this->viewName) . '">' . $block->toHtml() . '</div>');

		if (isset($layoutBackup)) {
			$controller->layout = $layoutBackup;
		}
	}
}
