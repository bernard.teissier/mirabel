<?php

/**
 * Contains static functions that produce HTML.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class HtmlHelper
{
	public static function titleLinkItem($sphinxAttrs, $hash = null)
	{
		$url = ['/revue/view', 'id' => $sphinxAttrs['revueid'], 'nom' => Norm::urlParam($sphinxAttrs['titrecomplet'])];
		if ($hash) {
			$url['s'] = $hash;
		}
		return '<div class="' . self::linkItemClass('titre', $sphinxAttrs['suivi']) . '">'
			. CHtml::link(
				CHtml::encode($sphinxAttrs['titrecomplet']),
				$url
			)
			. "</div>";
	}

	public static function ressourceLinkItem($sphinxAttrs, $hash = null)
	{
		$url = ['/ressource/view', 'id' => $sphinxAttrs['id'], 'nom' => Norm::urlParam($sphinxAttrs['nomcomplet'])];
		if ($hash) {
			$url['s'] = $hash;
		}
		return '<div class="' . self::linkItemClass('ressource', $sphinxAttrs['suivi']) . '">'
			. CHtml::link(
				CHtml::encode($sphinxAttrs['nomcomplet']),
				$url
			)
			. "</div>";
	}

	public static function editeurLinkItem($sphinxAttrs, $hash = null)
	{
		$url = ['/editeur/view', 'id' => $sphinxAttrs['id'], 'nom' => Norm::urlParam($sphinxAttrs['nomcomplet'])];
		if ($hash) {
			$url['s'] = $hash;
		}
		return '<div class="' . self::linkItemClass('editeur', $sphinxAttrs['suivi']) . '">'
			. CHtml::link(
				CHtml::encode($sphinxAttrs['nomcomplet']),
				$url
			)
			. "</div>";
	}

	public static function linkItemClass($default, $suivi)
	{
		if (empty($suivi)) {
			return "$default suivi-none";
		}

		if (Yii::app()->user->isGuest) {
			$suiviSelf = false;
		} else {
			if (!isset($suivi['model'])) {
				$suiviSelf = in_array(Yii::app()->user->partenaireId, $suivi);
			} else {
				$suiviSelf = Yii::app()->user->isMonitoring($suivi);
			}
		}
		return $default . ($suiviSelf  ? ' suivi-self' : ' suivi-other');
	}

	public static function table($a, $classes="")
	{
		$first = reset($a);
		if (!$first) {
			return '';
		}
		$html = '<table class="table table-striped table-bordered table-condensed ' . $classes . '">'
			. '<thead><tr>';
		foreach (array_keys($first) as $colHead) {
			$html .= '<th>' . CHtml::encode($colHead) . '</th>';
		}
		$html .= '</tr></thead><tbody>';
		foreach ($a as $row) {
			$html .= '<tr>';
			foreach ($row as $v) {
				$html .= '<td>' . $v . '</td>';
			}
			$html .= '</tr>';
		}
		$html .= '</tbody></table>';
		return $html;
	}

	public static function displayLinksHint($form, $title, $name, $rank, $position)
	{
		if ($position === $rank) {
			if (!empty($form->hints[$name])) {
				return CHtml::tag(
					'a',
					[
						'href' => '#',
						'data-title' => $title,
						'data-content' => $form->hints[$name],
						'data-html' => true,
						'data-trigger' => 'hover',
						'rel' => 'popover',
					],
					'?'
				);
			}
		}
		return '';
	}

	public static function postButton($label, $url, $hiddenParams = [], $htmlOptions = [])
	{
		$htmlOptions['type'] = 'submit';
		return CHtml::form($url, 'post')
			. join(
				"\n",
				array_map(
					function ($k, $v) {
						return CHtml::hiddenField($k, $v);
					},
					array_keys($hiddenParams),
					array_values($hiddenParams)
				)
			)
			. CHtml::htmlButton($label, $htmlOptions)
			. CHtml::endForm();
	}

	public static function definitionList(array $list, $labelCallback = null)
	{
		if (!$list) {
			return "";
		}
		$html = '<dl>';
		foreach ($list as $k => $v) {
			if ($labelCallback) {
				$label = call_user_func($labelCallback, $k);
			} else {
				$label = $k;
			}
			$html .= '<dt>' . CHtml::encode($label) . '</dt>'
				. '<dd>' . CHtml::encode($v) . '</dd>';
		}
		$html .= "</dl>\n";
		return $html;
	}

	/**
	 * Return a function(title, url) that creates the TR for each URL.
	 *
	 * @param array $checks Assoc array [ URL => true|false ]
	 * @return callable
	 */
	public static function builderCheckLinks(array $checks)
	{
		return function ($url, $title = null) use ($checks) {
			list($success, $msg) = $checks[$url];
			$class = '';
			if ($url) {
				if ($success === LinkChecker::RESULT_SUCCESS) {
					$msg = 'OK';
					$class = 'success';
				} elseif ($success === LinkChecker::RESULT_IGNORED) {
					$msg = 'non-testable<div>Cette URL ne peut être testée automatiquement (protection anti-robots)</div>';
					$class = 'alert-info';
				} else {
					$msg = 'Erreur<div>' . CHtml::encode($msg) . '</div>';
					$class = 'error';
				}
			}
			return "<tr class=\"$class\">"
				. (isset($title) ? "<td>" . $title . "</td>" : '')
				. "<td>" . CHtml::link(CHtml::encode($url), $url) . "</td>"
				. "<td>$msg</td>"
				. "</tr>\n";
		};
	}

	/**
	 * Like CHtml::errorSummary, with 3 differences:
	 * - Bootstrap CSS classes
	 * - Does not HTML encode the errors
	 * - Special conversion of ConfirmDuplicate error message.
	 *
	 * @param array|\CModel $model
	 * @param string|null $header
	 * @param string|null $footer
	 * @param array $htmlOptions
	 * @return string HTML
	 */
	public static function errorSummary($model, ?string $header = null, ?string $footer = null, array $htmlOptions = []): string
	{
		if (!isset($htmlOptions['class'])) {
			// Bootstrap error class as default
			$htmlOptions['class'] = 'alert alert-block alert-error';
		}

		$content = '';
		if (!is_array($model)) {
			$model = [$model];
		}
		if (isset($htmlOptions['firstError'])) {
			$firstError = $htmlOptions['firstError'];
			unset($htmlOptions['firstError']);
		} else {
			$firstError = false;
		}
		foreach ($model as $m) {
			foreach ($m->getErrors() as $field => $errors) {
				foreach ($errors as $error) {
					if (strncmp($error, ConfirmDuplicate::PATTERN, strlen(ConfirmDuplicate::PATTERN)) === 0) {
						$content .= "<li>" . $m->getAttributeLabel($field) . " : vous devez confirmer qu'il ne s'agit pas d'un doublon.</li>\n";
					} elseif ($error != '') {
						$content .= "<li>$error</li>\n";
					}
					if ($firstError) {
						break;
					}
				}
			}
		}
		if ($content !== '') {
			if ($header === null) {
				$header = '<p>' . Yii::t('yii', 'Please fix the following input errors:') . '</p>';
			}
			if (!isset($htmlOptions['class'])) {
				$htmlOptions['class'] = self::$errorSummaryCss;
			}
			return CHtml::tag('div', $htmlOptions, $header . "\n<ul>\n$content</ul>" . $footer);
		}
		return '';
	}
}
