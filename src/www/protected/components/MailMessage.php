<?php

/**
 * Wraps a Swift_Message.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class MailMessage extends Swift_Message
{
	/**
	 * Parse email addresses.
	 *
	 * Valid arguments (upstream):
	 * ("jean@machin.org")
	 * (["jean@machin.org"])
	 * (["jean@machin.org", "karim@machin.org"])
	 * (["jean@machin.org" => "Jean", "karim@machin.org" => "Karim"])
	 * ("jean@machin.org", "Jean") // 2 string arguments. Unspecified if first arg is an array
	 *
	 * New valid arguments:
	 * (["jean@machin.org\nkarim@machin.org"])
	 * (["jean@machin.org\nkarim@machin.org\n"])
	 * (["jean@machin.org Jean\nKarim karim@machin.org\nShinji <shinji@koto.org>"])
	 *
	 * @param array|string $emails
	 * @param null|string $name
	 * @return $this
	 */
	public function setTo($emails, $name = null): MailMessage
	{
		if (is_string($emails) && strpos($emails, "\n") !== false) {
			return parent::setTo(self::splitEmails($emails, $name));
		}
		return parent::setTo($emails, $name);
	}

	/**
	 * Split the email texte (one email per line) into a format suitable for Swift_Message::setTo().
	 *
	 * @param string $emails To split
	 * @param string|null $name (opt)
	 * @return array [add1, add2, add3 => name]
	 */
	private static function splitEmails(string $emails, ?string $name = null): array
	{
		$list = array_filter(
			array_map('trim', explode("\n", $emails)),
			function ($x) {
				return strpos($x, "@") !== false;
			}
		);
		$to = [];
		$matches = [];
		foreach ($list as $line) {
			if (preg_match('/^(.+)\s+<(.+@.+\..+)>/', $line, $matches)) {
				$to[$matches[2]] = trim($matches[1], ' "');
			} elseif (preg_match('/^<(.+@.+\..+)>\s+(.+)$/', $line, $matches)) {
				$to[$matches[1]] = trim($matches[2], ' "');
			} elseif (preg_match('/^(.+)\s+(\S+@\S+\.\S+)$/', $line, $matches)) {
				$to[$matches[2]] = trim($matches[1], ' "');
			} elseif (preg_match('/^(\S+@\S+\.\S+)\s+(.+)$/', $line, $matches)) {
				$to[$matches[1]] = trim($matches[2], ' "');
			} elseif ($name) {
				$to[$line] = $name;
			} else {
				$to[] = $line;
			}
		}
		return $to;
	}
}
