<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class MdViewRenderer extends CViewRenderer
{
	public $fileExtension = '.md';

	protected function generateViewFile($sourceFile, $viewFile)
	{
		$converter = Yii::app()->getComponent('markdown');
		assert($converter instanceof \League\CommonMark\CommonMarkConverter);

		file_put_contents($viewFile, '<div class="from-markdown">' . $converter->convertToHtml(file_get_contents($sourceFile)) . '</div>');
	}
}
