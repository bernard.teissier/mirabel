<?php

/**
 * Doc: http://zetacomponents.org/documentation/trunk/Feed/tutorial.html
 */
class FeedGenerator
{
	public $maxItems = 50;

	public $privateFeed = false;

	public $ownUrl = '';

	public $feedTitle = [];

	/** @var ezcFeed */
	private $feed;

	private $type = 'atom';

	private $config;

	private $baseUrl;

	/** @var PDO */
	private $pdo;

	/** @var string */
	private $sql;

	private $where = [""];

	/**
	 * Constructor. Initializes a few properties.
	 */
	public function __construct(string $type)
	{
		if (in_array($type, ['atom', 'rss1', 'rss2'])) {
			$this->type = $type;
		}

		$path = dirname(__DIR__) . '/config';
		$main = include($path . '/main.php');
		$local = include($path . '/local.php');
		$this->config = CMap::mergeArray($main, $local);
		if (defined('YII_TEST') && YII_TEST) {
			$this->config = CMap::mergeArray($this->config, require("$path/test.php"));
		}

		if (!class_exists('Yii') || null === Yii::app()) {
			Yii::createWebApplication($this->config);
		}
		Yii::import('application.models.*', false);
		require_once Yii::getPathOfAlias('application.models.import') . '/Normalize.php';

		$this->baseUrl = Yii::app()->getBaseUrl(true);
		$this->ownUrl = "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		$this->sql = "SELECT i.id, statut, description AS title, '' AS description, hdateProp, hdateVal, "
			. "revueId, ressourceId, editeurId, contenuJson, action "
			. "FROM Intervention i "
			. "WHERE description != '' "
			. "AND (revueId IS NOT NULL OR ressourceId IS NOT NULL OR editeurId IS NOT NULL)";
		$this->maxItems = Config::read('rss.limite.interventions');
		if (!$this->maxItems) {
			$this->maxItems = 50;
		}

		$this->initFeed();
	}

	/**
	 * Tri-state value: null => show all, true => only validated, false => only not validated
	 *
	 * @param bool|null $state
	 */
	public function showValidatedOnly(?bool $state = true): void
	{
		if ($state === null) {
			$this->where[0] = '';
		} else {
			$this->where[0] = ($state ? "hdateVal  > 0 AND statut = 'accepté'" : "statut = 'attente'");
		}
	}

	/**
	 * Adds a criteria (revue|ressource|editeur) to the search of items.
	 *
	 * @param string $name (revue|ressource|editeur)Id
	 * @param string|int $id Integer ID or string expr ("IS NULL")
	 */
	public function addCriteria(string $name, $id): void
	{
		if (ctype_digit($id) || is_int($id)) {
			$this->where[] = $name . " = " . $id;
			$name = preg_replace('/Id$/', '', ucfirst($name));
			$object = call_user_func([$name, 'model'])->findByPk($id);
			if ($object) {
				$this->feedTitle[] = $name . " " . $object->nom;
			}
		} else {
			$this->where[] = $name . " " . $id;
		}
	}

	/**
	 * Add a criteria on the "Partenaire" that has "Suivi" on the objects.
	 *
	 * @param int $pId
	 */
	public function addFilterMonitored(int $pId): void
	{
		$pId = (int) $pId;
		$this->sql = "SELECT i.id, statut, description AS title, '' AS description, hdateProp, hdateVal, "
			. "revueId, ressourceId, editeurId, contenuJson "
			. "FROM Intervention i "
			. "JOIN Suivi s ON (s.partenaireId = $pId"
			. " AND ((s.cibleId=i.revueId AND s.cible='Revue')"
			. " OR (s.cibleId=i.ressourceId AND s.cible='Ressource')"
			. " OR (s.cibleId=i.editeurId AND s.cible='Editeur'))) "
			. "WHERE description != '' "
			. "AND (revueId IS NOT NULL OR ressourceId IS NOT NULL OR editeurId IS NOT NULL)";
		$this->initDb();
		$name = htmlspecialchars($this->pdo->query("SELECT nom FROM Partenaire")->fetchColumn());
		$this->feedTitle[] = "[P] $name";
		$this->feed->description .= " Ce flux ne contient que les objets suivis par le partenaire $name.";
	}

	/**
	 * Outputs the result (HTTP header included).
	 */
	public function run(): void
	{
		$this->addItems();
		if ($this->feedTitle) {
			$this->feed->title->text .=  " — " . htmlspecialchars(join(" / ", $this->feedTitle));
		}
		$processingInstructions = [
			[
				'xml-stylesheet',
				'type="text/xsl" href="' . $this->baseUrl . '/xslt/' . $this->type . '.xsl"',
			],
		];
		$xml = $this->feed->generate($this->type, $processingInstructions);
		header('Content-Type: text/xml; charset=utf-8');
		echo $xml;
	}

	/**
	 * Generates a feed for an object to be deleted.
	 *
	 * @return string XML
	 */
	public function generateDeletionFeed(string $modelName): string
	{
		$this->addDeletionItem($modelName);
		$this->addItems();
		return $this->feed->generate($this->type);
	}

	/**
	 * Writes in files the XML feeds of an object.
	 *
	 * @param string $modelName Revue|Ressource|Editeur.
	 * @param int $id
	 */
	public static function saveFeedsToFile(string $modelName, int $id): void
	{
		$path = Yii::app()->getBasePath() . '/runtime/feeds/' . $modelName;
		if (!is_dir($path)) {
			mkdir($path, 0777, true);
		}
		foreach (['rss1', 'rss2', 'atom'] as $type) {
			$gen = new FeedGenerator($type);
			$gen->showValidatedOnly(true);
			$gen->addCriteria(strtolower($modelName) . 'Id', $id);
			$xml = $gen->generateDeletionFeed($modelName);
			$filename = sprintf('%s/%06d_%s.xml', $path, $id, $type);
			file_put_contents($filename, $xml);
		}
	}

	/**
	 * Deletes the files where the XML feeds were written.
	 *
	 * @param string $modelName Revue|Ressource|Editeur.
	 * @param int $id
	 * @return bool Success?
	 */
	public static function removeFeedsFile(string $modelName, int $id): bool
	{
		$path = Yii::app()->getBasePath() . '/runtime/feeds/' . $modelName;
		if (!is_dir($path)) {
			return false;
		}
		foreach (['rss1', 'rss2', 'atom'] as $type) {
			$file = sprintf('%s/%06d_%s.xml', $path, $id, $type);
			if (file_exists($file)) {
				unlink($file);
			}
		}
		return true;
	}

	/**
	 * Outputs the feed from the XML file that matches (modelName, id) IF produced today.
	 *
	 * @param string $modelName Revue|Ressource|Editeur.
	 * @param int $id
	 */
	public function findFeedInFiles(string $modelName, int $id): void
	{
		$modelName = ucfirst($modelName);
		$path = dirname(__DIR__) . '/runtime/feeds/' . $modelName
			. sprintf('/%06d_%s.xml', $id, $this->type);
		if (!file_exists($path) || filemtime($path) < mktime(0, 0, 0)) {
			return;
		}
		header('Content-Type: application/' . ($this->type == 'atom' ? 'atom' : 'rss') . '+xml; charset=utf-8');
		readfile($path);
		exit();
	}

	protected function parseContent(array $line, ?string $action = '')
	{
		$details = "";
		if ($action === 'revue-I') {
			if ($line['operation'] == 'create') {
				$categorie = Categorie::model()->findByPk($line['after']['categorieId']);
				if ($categorie && ($categorie->role === 'public' || $this->privateFeed)) {
					$details = "<li>Ajout du thème <em>" . htmlspecialchars($categorie->categorie) . "</em>"
						. ($categorie->role === 'public' ? "" : " (non publique)") . ".</li>";
				}
			} elseif ($line['operation'] == 'delete') {
				$categorie = Categorie::model()->findByPk($line['before']['categorieId']);
				if ($categorie && ($categorie->role === 'public' || $this->privateFeed)) {
					$details = "<li>Retrait du thème <em>" . htmlspecialchars($categorie->categorie) . "</em>"
						. ($categorie->role === 'public' ? "" : " (non publique)") . ".</li>";
				}
			}
		} elseif ($line['operation'] == 'update') {
			$details = self::buildChangesList($line['model'], $line['before'], $line['after']);
			if ($line['model'] === 'Issn' && $details === '') {
				$details = "<li>Données ISSN complétées</li>";
			}
		} elseif ($line['model'] === 'Issn' && $action !== 'revue-C') {
			if ($line['operation'] == 'delete') {
				$keys = array_fill_keys(array_keys($line['before']), "");
				$details = "<li>Suppression d'un ISSN<ul>" . self::buildChangesList($line['model'], $line['before'], $keys) . "</ul></li>";
			} elseif ($line['operation'] == 'create') {
				$keys = array_fill_keys(array_keys($line['after']), "");
				$details = "<li>Ajout d'un ISSN<ul>" . self::buildChangesList($line['model'], $keys, $line['after']) . "</ul></li>";
			} else {
				$details = self::buildChangesList($line['model'], $line['before'] ?? [], $line['after']);
			}
		} elseif ($line['model'] === 'ServiceCollection') {
			$details = "<li>Attribution aux collections</li>";
		} elseif (!empty($line['msg'])) {
			$details = "<li>" . htmlspecialchars($line['msg']) . "</li>";
		}
		return $details;
	}

	/* ******** private functions ********* */

	private function initFeed(): void
	{
		$this->feedTitle = [];
		$this->feed = new ezcFeed();
		$this->feed->title = $this->config['name'];
		$this->feed->description = "Suivi des mises à jour de Mir@bel, avec les {$this->maxItems} dernières modifications.";
		$this->feed->published = time();
		$this->feed->updated = time();
		if ($this->type == 'atom') {
			$this->feed->id = htmlspecialchars($this->ownUrl);
		} else {
			$this->feed->id = $this->ownUrl;
		}
		$this->feed->language = 'fr';

		$author = $this->feed->add('author');
		$author->name = htmlspecialchars($this->config['name']);
		//$author->email = $this->config['params']['adminEmail'];

		$link = $this->feed->add('link');
		if ($this->type == 'atom') {
			$link->href = $this->ownUrl;
			$link->rel = 'self';
		} else {
			$link->href = htmlspecialchars($this->ownUrl);
		}
	}

	/**
	 * Adds items to the feed using the $sql property.
	 * Each row must have the keys: id, title, description, updatetime, (revue|ressource|editeur)Id.
	 */
	private function addItems(): void
	{
		$this->initDb();
		$where = join(' AND ', array_filter($this->where));
		$sql = $this->sql . ($where ? " AND " . $where : '')
			. " ORDER BY hdateVal DESC, hdateProp DESC LIMIT " . $this->maxItems;
		//die($sql);
		$result = $this->pdo->query($sql);
		if (!$result) {
			return;
		}
		foreach ($result as $row) {
			$item = $this->feed->add('item');
			/* @var $item ezcFeedEntryElement */
			$item->id = $this->baseUrl . '/index.php/intervention/view?id=' . $row['id'];
			$description = $item->add('description');
			$description->type = 'html';
			switch ($row['statut']) {
				case 'attente':
					$item->title = "[?] ";
					$description->text = "Cette proposition est en attente de validation. ";
					break;
				case 'refusé':
					$item->title = "[!] ";
					$description->text = "Cette proposition a été refusée. ";
					break;
				default:
					$item->title = "";
					if ($this->privateFeed) {
						if ($row['hdateProp'] == $row['hdateVal']) {
							$description->text = "Modification directe. ";
						} else {
							$description->text = sprintf(
								"Modification proposée le %s et validée le %s. ",
								strftime('%d %B %Y à %R', $row['hdateProp']),
								strftime('%d %B %Y à %R', $row['hdateVal'])
							);
						}
					}
			}
			$item->title .= htmlspecialchars($row['title']);
			if ($row['description']) {
				$description->text .= htmlspecialchars('<p>' . $row['description'] . '</p>');
			}
			if ($row['contenuJson']) {
				$contenu = json_decode($row['contenuJson'], true);
				$details = [];
				foreach ($contenu['content'] as $line) {
					$details[] = $this->parseContent($line, $row['action']);
				}
				$details = array_filter($details);
				if ($details) {
					$description->text .= "<ul>" . join("</ul><ul>\n", array_unique($details)) . "</ul>";
				} elseif ($row['action'] === 'revue-I') {
					// empty "catégorisation" message => remove it
					$this->feed->pop("item");
					continue;
				} elseif ($row['action'] === 'service-U') {
					$this->feed->pop("item");
					continue;
				}
			}
			$item->published = $row['hdateVal'];
			$item->updated = $row['hdateVal'];
			// add the first link among 3 candidates
			foreach (['revue', 'ressource', 'editeur'] as $o) {
				if (!empty($row[$o . 'Id'])) {
					$link = $item->add('link');
					$link->href = $this->baseUrl . '/index.php/' . $o . '/view?id=' . $row[$o . 'Id'];
					$link->rel = "alternate";
					break;
				}
			}
		}
	}

	private function initDb(): void
	{
		if (!isset($this->pdo)) {
			$db = $this->config['components']['db'];
			$this->pdo = new PDO($db['connectionString'], $db['username'], $db['password']);
			$this->pdo->exec("SET CHARACTER SET utf8mb4");
		}
	}

	/**
	 * Returns the full name of an attribute, if it has to be printed.
	 *
	 * @param string $objectName
	 * @param string $attrName
	 * @return array|string
	 */
	private static function attrToText(string $objectName, string $attrName)
	{
		static $map = [
			'Titre' => [
				'titre' => 'Titre',
				'prefixe' => 'Préfixe',
				'sigle' => 'Sigle',
				'dateDebut' => 'Date de début',
				'dateFin' => 'Date de fin',
				'sudoc' => 'Sudoc', // compatibilité anciens ISSN
				'worldcat' => 'WorldCat', // compatibilité anciens ISSN
				'issn' => 'ISSN', // compatibilité anciens ISSN
				'issnl' => 'ISSN-L', // compatibilité anciens ISSN
				'url' => 'Site web',
				'urlCouverture' => 'Vignette de couverture',
				'liensJson' => 'Autres liens',
				'periodicite' => 'Périodicité',
				'electronique' => 'Électronique', // compatibilité anciens ISSN
				'langues' => 'Langues',
			],
			'Issn' => [
				'bnfArk' => 'BnF ARK',
				'issn' => 'ISSN',
				'support' => "Support de l'ISSN",
				'issnl' => 'ISSN-L',
				'pays' => 'Pays',
				'sudocPpn' => 'Sudoc PPN',
				'worldcatOcn' => 'worldcat OCN',
			],
			'Ressource' => [
				'nom' => 'Nom',
				'prefixe' => 'Préfixe',
				'sigle' => 'Sigle',
				'description' => 'Description',
				'type' => 'Type',
				'acces' => 'Type d\'accès',
				'url' => 'Adresse web',
				'diffuseur' => 'Diffuseur',
				'partenaires' => 'Partenaires',
				'partenairesNb' => 'Nombre de partenaires',
				'disciplines' => 'Disciplines',
				'revuesNb' => 'Nombre de revues',
				'articlesNb' => 'Nombre d\'articles',
				'alerteRss' => 'Flux RSS',
				'alerteMail' => 'Alerte courriel',
				'exportPossible' => 'Export possible',
				'indexation' => 'Indexation',
				'noteContenu' => 'Note de contenu',
				'autoImport' => 'Import auto',
			],
			'Editeur' => [
				'nom' => 'Nom',
				'prefixe' => 'Préfixe',
				'sigle' => 'Sigle',
				'description' => 'Description',
				'url' => 'Adresse web',
				'geo' => 'Repère géo',
				'idref' => 'IdRef',
				'sherpa' => 'ID Sherpa',
				'liensJson' => 'Autres liens',
				'logoUrl' => "URL du logo",
			],
			'Service' => [
				'type' => 'Type',
				'acces' => 'Type d\'accès',
				'url' => 'Adresse web',
				'alerteRssUrl' => 'Flux RSS',
				'alerteMailUrl' => 'Alerte par courriel',
				'derNumUrl' => 'Dernier numéro',
				'lacunaire' => 'Couverture lacunaire',
				'selection' => 'Sélection d\'articles',
				'volDebut' => 'Premier volume (numérique)',
				'noDebut' => 'Premier numéro (numérique)',
				'numeroDebut' => 'Etat de collection (premier numéro)',
				'volFin' => 'Dernier volume (numérique)',
				'noFin' => 'Dernier numéro (numérique)',
				'numeroFin' => 'Etat de collection (dernier numéro)',
				'dateBarrDebut' => 'Date de début',
				'dateBarrFin' => 'Date de fin',
				'dateBarrInfo' => 'Date-barrière',
				'embargoInfo' => "Embargo",
				'notes' => "Notes KBART",
			],
		];
		if (empty($map['TitreEditeur'])) {
			$map['TitreEditeur'] = TitreEditeur::model()->attributeLabels();
		}
		if (isset($map[$objectName][$attrName])) {
			return $map[$objectName][$attrName];
		}
		return '';
	}

	private static function formatValue($v, string $attr): string
	{
		$bool = [
			'electronique' => 1, // compatibilité anciens ISSN
			'exportPossible' => 1,
			'indexation' => 1,
			'noteContenu' => 1,
			'autoImport' => 1,
			'lacunaire' => 1,
			'selection' => 1,
		];
		if (is_array($v)) {
			$v = json_encode($v);
		}
		if ($attr === 'liensJson') {
			return TitreLiens::htmlFormat($v);
		}
		if ($attr === 'embargoInfo' && $v) {
			return import\Normalize::getReadableKbartEmbargo($v);
		}
		if (preg_match('/(?:^u|[a-z]U)rl$/', $attr)) {
			$url = htmlspecialchars($v);
			return sprintf('<a href="%s">%s</a>', $url, $url);
		}
		if (isset($bool[$attr])) {
			return ($v ? 'Oui' : 'Non');
		}
		return htmlspecialchars('"' . $v . '"');
	}

	private function addDeletionItem(string $modelName): void
	{
		$row = Yii::app()->db->createCommand("SHOW TABLE STATUS LIKE 'Intervention'")->queryRow(true);
		$url = $this->baseUrl . '/index.php/' . strtolower($modelName);
		unset($row);
		$item = $this->feed->add('item');
		$item->id = $url;
		$msg = 'Suppression de ' . ($modelName == 'Editeur' ? "l'éditeur" : "la " . strtolower($modelName))
			. " dans Mir@bel";
		$description = $item->add('description');
		$description->text = $msg;
		$item->title = $msg;
		$item->published = $_SERVER['REQUEST_TIME'];
		$item->updated = $_SERVER['REQUEST_TIME'];
		$link = $item->add('link');
		$link->href = $url;
		$link->rel = "alternate";
	}

	private static function buildChangesList($model, array $beforeList, array $afterList): string
	{
		$details = [];
		$seen = [];
		foreach ($beforeList as $k => $v) {
			if (isset($seen[$k])) {
				continue;
			}
			$seen[$k] = true;
			$name = self::attrToText($model, $k);
			if (!$name) {
				continue;
			}
			$before = self::formatValue($v, $k);
			$after = self::formatValue($afterList[$k] ?? "", $k);
			if (($before || $after) && ($before !== $after)) {
				$details[$name] = "<li><b>" . htmlspecialchars($name) . '</b> : '
					. $before . ' &#8594; ' . $after
					. ".</li>\n";
			}
		}
		foreach ($afterList as $k => $v) {
			if (isset($seen[$k]) || $v === "") {
				continue;
			}
			$seen[$k] = true;
			$name = self::attrToText($model, $k);
			if (!$name) {
				continue;
			}
			$before = self::formatValue("", $k);
			$after = self::formatValue($v, $k);
			$details[$name] = "<li><b>" . htmlspecialchars($name) . '</b> : '
				. $before . ' &#8594; ' . $after
				. ".</li>\n";
		}
		ksort($details);
		return join("", $details);
	}
}
