<?php

class DetailedHttpException extends CHttpException
{
	/**
	 * @var string Detailed message (HTML)
	 */
	public $details;

	/**
	 * Constructor.
	 * @param int $status HTTP status code, such as 404, 500, etc.
	 * @param string $message error message
	 * @param string $details Detailed message (HTML)
	 */
	public function __construct($status, $message=null, string $details)
	{
		$this->details = $details;
		parent::__construct($status, $message, 0);
	}
}
