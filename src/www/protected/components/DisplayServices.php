<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class DisplayServices extends CComponent
{
	/**
	 * @var bool If true, show each Service instead of merging
	 */
	public $displayAll = false;

	/**
	 * @var array Cache the result of filterServices()
	 */
	protected $filteredServices = [];

	private $services = [];

	private $abonneId;

	private $hasMerged = false;

	private $mergeForbidden = false;

	/**
	 * @var AbonnementSearch
	 */
	private $abonnement;

	/**
	 * @param array $services Array of Service instances to filter.
	 * @param int $partenaireId 0 if no institute
	 */
	public function __construct(array $services, ?int $partenaireId)
	{
		$this->services = $services;
		// give a higher priority according to type
		usort($services, function ($a, $b) {
			$order = ['Intégral' => 4, 'Résumé' => 3, 'Sommaire' => 2, 'Indexation' => 1];
			$ao = $order[$a->type];
			$bo = $order[$b->type];
			return ($ao < $bo ? -1 : ($ao === $bo ? 0 : 1));
		});

		if ($partenaireId) {
			$partenaire = Partenaire::model()->findByPk($partenaireId);
			if ($partenaire) {
				$this->abonneId = (int) $partenaireId;
				if ($this->abonneId) {
					$this->abonnement = new AbonnementSearch();
					$this->abonnement->partenaireId = $this->abonneId;
				}
				if (!$partenaire->fusionAcces) {
					$this->displayAll = true;
					$this->mergeForbidden = true;
				}
			}
		}
	}

	/**
	 * @param bool $on
	 */
	public function setDisplayAll(bool $on = true): void
	{
		if ($this->mergeForbidden) {
			$this->displayAll = true;
		} else {
			$this->displayAll = (boolean) $on;
		}
	}

	/**
	 * Return a list list of Service after merging them according to subscriptions.
	 *
	 * @return array
	 */
	public function filterServices(): array
	{
		if (!$this->filteredServices) {
			if ($this->displayAll) {
				$this->filteredServices = $this->getVisibleServices($this->services);
			} else {
				$filteredServices = [];
				foreach ($this->services as $s) {
					/* @var $s Service */
					$key = $s->ressourceId . $s->url . $s->type; // possible merge of those that have the same key
					if (!isset($filteredServices[$key])) {
						// URL unknown => add this new record
						$filteredServices[$key] = [$s];
					} else {
						// URL known => add record or update an existing record?
						$access = $this->getServiceAccess($s);
						$alreadyDisplayed = false;
						foreach ($filteredServices[$key] as $previous) {
							$pAccess = $this->getServiceAccess($previous);
							//echo "{$s->url} : $access ({$cat[$access]}) / $pAccess ({$cat[$pAccess]})<br/>\n";
							//var_dump($previous->attributes);
							if (
								(($access === $pAccess) || ($access !== 'Restreint' && $pAccess !== 'Restreint'))
								&& $dates = self::mergeDates($previous, $s)
								) {
								$this->hasMerged = true;
								$previous->fusion = true;
								$alreadyDisplayed = true;
								if ($access === 'Abonné' && $pAccess === 'Abonné') {
									$previous->acces = 'Abonné';
								}
								if ($dates[0] === $s->dateBarrDebut) {
									$previous->dateBarrDebut = $s->dateBarrDebut;
									$previous->noDebut = $s->noDebut;
									$previous->volDebut = $s->volDebut;
									$previous->numeroDebut = $s->numeroDebut;
								}
								if ($dates[1] === $s->dateBarrFin) {
									$previous->dateBarrFin = $s->dateBarrFin;
									$previous->noFin = $s->noFin;
									$previous->volFin = $s->volFin;
									$previous->numeroFin = $s->numeroFin;
									$previous->derNumUrl = $s->derNumUrl;
								}
								foreach (['alerteMailUrl', 'alerteRssUrl', 'derNumUrl'] as $k) {
									if (empty($previous->{$k}) && !empty($s->{$k})) {
										$previous->{$k} = $s->{$k};
									}
								}
							}
						}
						if (!$alreadyDisplayed) {
							$filteredServices[$key][] = $s;
						}
					}
				}
				// flatten the array of arrays
				foreach ($filteredServices as $services) {
					foreach ($services as $s) {
						$this->filteredServices[] = $s;
					}
				}
				$this->filteredServices = $this->getVisibleServices($this->filteredServices);
			}
		}
		return $this->filteredServices;
	}

	/**
	 * Has the filtering process merged services?
	 *
	 * @return bool
	 */
	public function hasMerged(): bool
	{
		$this->filterServices();
		return $this->hasMerged;
	}

	/**
	 * Return the subscription type (Abonné|Caché) or the real type (Restreint|Libre).
	 *
	 * @param Service $service
	 * @return string
	 */
	public function getServiceAccess(Service $service)
	{
		if ($this->abonneId) {
			$status = $this->abonnement->readStatus($service);
			if ($status === Abonnement::ABONNE) {
				$acces = "Abonné";
			} elseif ($status === Abonnement::MASQUE) {
				$acces = "Caché";
			}
		}
		if (!isset($acces)) {
			$acces = $service->acces;
		}
		return $acces;
	}

	/**
	 * Return a SPAN of the subscription type (Abonné|Caché) or the real type (Restreint|Libre).
	 *
	 * @param Service $service
	 * @return string HTML
	 */
	public function getServiceAccessWithTitle(Service $service)
	{
		$titles = [
			"Libre" => "Librement accessible",
			"Restreint" => "Accès par abonnement ou payant à la consultation",
			"Abonné" => "Accès par l'abonnement de votre établissement",
			"Caché" => "Dans une collection masquée par votre établissement",
		];
		$acces = $this->getServiceAccess($service);
		return '<span title="' . $titles[$acces] . '">' . $acces . '</span>';
	}

	/**
	 * Has Abonnement.proxy=1 and Partenaire.proxyUrl!=''
	 *
	 * @param Service $service
	 * @return bool
	 */
	public function hasProxy(Service $service): bool
	{
		return $this->abonnement->readProxy($service);
	}

	/**
	 * Services without any Abonnement nor hidden.
	 *
	 * @param Service[] $services
	 * @return array
	 */
	protected function getVisibleServices(array $services): array
	{
		$f = [];
		foreach ($services as $s) {
			if ($this->abonneId) {
				$status = $this->abonnement->readStatus($s);
				if ($status === Abonnement::PAS_ABONNE) {
					$f[$this->serviceToSortKey($s, false)] = $s;
				} elseif ($status === Abonnement::ABONNE) {
					$f[$this->serviceToSortKey($s, true)] = $s;
				} elseif ($this->displayAll) {
					// masked, but display forced
					$f[$this->serviceToSortKey($s, false)] = $s;
				}
			} else {
				$f[$this->serviceToSortKey($s, false)] = $s;
			}
		}
		return self::sortByKeyAndTitle($f);
	}

	protected function serviceToSortKey(Service $s, bool $abo): string
	{
		$types = [
			'Intégral' => 3,
			'Résumé' => 2,
			'Sommaire' => 1,
			'Indexation' => 0,
		];
		if ($abo) {
			$acces = 2;
		} elseif ($s->acces === 'Libre') {
			$acces = 1;
		} else {
			$acces = 0;
		}
		// no date has the max value, then recent years.
		if (empty($s->dateBarrFin)) {
			$endKey = 50000000;
		} elseif (preg_match('/^(\d{4})-?(\d\d)?-?(\d\d)?\b/', $s->dateBarrFin, $m)) {
			if (empty($m[2])) {
				$endKey = "{$m[1]}0000";
			} elseif (empty($m[3])) {
				$endKey = "{$m[1]}{$m[2]}00";
			} else {
				$endKey = "{$m[1]}{$m[2]}{$m[3]}";
			}
		} else {
			// not empty nor a (partial) date
			$endKey = 0;
		}
		return sprintf("%03d%d/%d", 4 * $types[$s->type] + $acces, $endKey, $s->id);
	}

	/**
	 * Sort using: serviceToSortKey() DESC, title ASC.
	 *
	 * @param array $f [serviceToSortKey() => $service]
	 * @return array
	 */
	protected static function sortByKeyAndTitle(array $f): array
	{
		uksort(
			$f,
			function ($a, $b) {
				return strcmp($b, $a);
			}
		);
		return $f;
	}

	/**
	 * @param Service $s1
	 * @param Service $s2
	 * @return array [stringDate1, stringDate2] or []
	 */
	private static function mergeDates(Service $s1, Service $s2): array
	{
		$start1 = DateVersatile::convertdateToTs($s1->dateBarrDebut, false);
		$start2 = DateVersatile::convertdateToTs($s2->dateBarrDebut, false);
		$startDiff = abs($start1 - $start2);
		$end1 = DateVersatile::convertdateToTs($s1->dateBarrFin, true);
		$end2 = DateVersatile::convertdateToTs($s2->dateBarrFin, true);
		$endDiff = abs($end1 - $end2);
		$year = 365*24*3600;

		if (
			   (ctype_digit($s1->dateBarrFin) && ctype_digit($s2->dateBarrDebut) && abs((int) $s1->dateBarrFin - (int) $s2->dateBarrDebut) <= 1)
			|| (ctype_digit($s2->dateBarrFin) && ctype_digit($s1->dateBarrDebut) && abs((int) $s2->dateBarrFin - (int) $s1->dateBarrDebut) <= 1)
			|| $startDiff < $year                 // quite the same end
			|| $endDiff < $year                   // quite the same start
			|| abs($start2 - $end1) < $year       // s2 is just after s1
			|| abs($start1 - $end2) < $year       // s1 is just after s2
			|| $start2 > $start1 && $start2 < $end1 // s2 starts inside s1
			|| $end2 > $start1 && $end2 < $end1     // s2 ends inside s1
			|| $start1 > $start2 && $start1 < $end2 // s1 starts inside s2
			|| $end1 > $start2 && $end1 < $end2     // s1 ends inside s2
			) {
			return [
				($start1 < $start2 ? $s1->dateBarrDebut : $s2->dateBarrDebut),
				($end1 > $end2 ? $s1->dateBarrFin : $s2->dateBarrFin),
			];
		}
		return [];
	}
}
