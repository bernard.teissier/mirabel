<?php

/**
 * @property \Controller $controller The currently active controller.
 * @property \extensions\format\ExtFormatter $format
 * @property \WebUser $user The user session information.
 */
class WebApplication extends CWebApplication
{
	private $closureComponents = [];

	public function getComponent($id, $createIfNull = true)
	{
		if (isset($this->closureComponents[$id])) {
			$component = $this->closureComponents[$id];
			if (is_object($component) && ($component instanceof \Closure)) {
				$this->closureComponents[$id] = call_user_func($component);
			}
			return $this->closureComponents[$id];
		}
		return parent::getComponent($id, $createIfNull);
	}

	public function setComponent($id, $component, $merge = true)
	{
		if ($component !== null && is_object($component) && ($component instanceof \Closure)) {
			// function
			$this->closureComponents[$id] = $component;
		} else {
			parent::setComponent($id, $component, $merge);
		}
	}
}
