<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class CsvDataProvider extends CDataProvider
{
	private $rows = [];

	private $header = [];

	private $file = "";

	private $separator = ";";

	private $filter;

	public function __construct($file, $separator = ';')
	{
		if (!file_exists($file)) {
			throw new Exception("Fichier '$file' non trouvé.");
		}
		$this->file = $file;
		$this->separator = $separator;
	}

	public function setHeader($header)
	{
		$this->header = $header;
	}

	public function setFilter(callable $function)
	{
		$this->filter = $function;
	}

	protected function readFile()
	{
		$handle = fopen($this->file, "r");
		if (!$handle) {
			throw new Exception("Fichier '{$this->file}' non lisible.");
		}
		while ($row = fgetcsv($handle, 0, $this->separator)) {
			if (!array_filter($row)) {
				continue;
			}
			if (empty($this->header)) {
				$this->setHeader($row);
			} else {
				if ($this->filter && !call_user_func($this->filter, $row)) {
					continue;
				}
				if (count($this->header) !== count($row)) {
					var_dump($this->header);
					var_dump($row);
					die("CSV incohérent : nombre variable de colonnes");
				}
				$this->rows[] = array_combine($this->header, $row);
			}
		}
	}

	protected function calculateTotalItemCount()
	{
		if (empty($this->rows)) {
			$this->readFile();
		}
		return count($this->rows);
	}

	protected function fetchData()
	{
		if (empty($this->rows)) {
			$this->readFile();
		}
		return $this->rows;
	}

	protected function fetchKeys()
	{
		if (empty($this->header)) {
			$this->readFile();
		}
		return $this->header;
	}
}
