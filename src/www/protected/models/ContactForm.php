<?php

/**
 * ContactForm class.
 *
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ContactForm extends CFormModel
{
	use Honeypot;

	public const FAKE_DATA = "not used";

	public $name;

	public $courriel;

	public $sujet;

	public $body;

	public $verifyCode;

	public $verifyCodeEnc;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return [
			['name, courriel, sujet, body, verifyCode, verifyCodeEnc', 'required'],
			['courriel', 'email'],
			// fakes
			['email', 'validateFakeEmail'],
			['subject', 'validateFakeSubject'],
		];
	}

	public function afterValidate()
	{
		if (md5("guess " . $this->verifyCode) !== $this->verifyCodeEnc) {
			$this->addError("verifyCode", "Le code anti-spam n'est pas correct");
		}
		return parent::afterValidate();
	}

	public function getCaptchaText()
	{
		$a = rand(2, 20);
		$b = rand(1, 9);
		$this->verifyCode = "";
		$this->verifyCodeEnc = md5("guess " . ($a + $b));
		return "$a + $b";
	}

	/**
	 * Declares customized attribute labels.
	 *
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return [
			'name' => 'Votre nom',
			'courriel' => 'Votre courriel',
			'subject' => 'Sujet',
			'sujet' => 'Sujet',
			'body' => 'Message',
			'verifyCode' => 'Code anti-spam',
		];
	}
}
