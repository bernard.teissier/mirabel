<?php

/**
 * @property \stdClass $destination
 */
class Upload extends CFormModel
{
	/** @var ?CUploadedFile */
	public $file;

	/** @var string */
	public $destType = 'public';

	/** @var string */
	public $destName;

	/** @var bool */
	public $overwrite = false;

	/** @var array */
	public static $destinations = [];

	/** @var string Relative path and name of the destination file */
	protected $destRelFile;

	/** @var string Absolute path and name of the destination file */
	protected $destAbsFile;

	protected $overrideFileName = false;

	/**
	 * Constructor
	 *
	 * @param string|UploadDestination $destType
	 * @throws Exception
	 */
	public function __construct($destType, $overrideFileName = false)
	{
		if ($destType) {
			if (is_string($destType)) {
				if (!isset(self::$destinations[$this->destType])) {
					throw new Exception("Erreur de code : le type d'upload est inconnu.");
				}
			} else {
				self::$destinations[''] = $destType;
				$destType = '';
			}
			$this->destType = $destType;
			$forceDestName = $this->getDestination()->forceDestName;
			if ($forceDestName) {
				$this->destName = $forceDestName;
			}
			$this->overrideFileName = $overrideFileName;
		}
		parent::__construct();
	}

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		$rules = [
			'file' => ['file', 'file', 'types' => 'pdf, png, jpg, jpeg, csv, mkv, mp4, avi, webm, md, txt'],
			['destType', 'in', 'range' => array_keys(self::$destinations)],
			['overwrite', 'boolean'],
		];
		if ($this->canOverrideFileName()) {
			$rules[] = ['destName', 'match', 'pattern' => '/^[\w()_.-]+$/'];
		}
		if (!empty($this->getDestination()->types)) {
			$rules['file']['types'] = $this->getDestination()->types;
		}
		return $rules;
	}

	/**
	 * Called automatically before validate().
	 */
	public function beforeValidate()
	{
		// file saved in {destinationPath}/{destName}.{srcExt}
		$this->destRelFile = $this->file->getName();
		if ($this->destName) {
			$this->destRelFile = $this->destName . substr($this->destRelFile, strrpos($this->destRelFile, '.'));
		}
		$d = $this->getDestination();
		$this->destAbsFile = $d->path . '/' . $this->destRelFile;

		if ($this->file && !$this->destName) {
			if (!preg_match('/^[\w().-]+$/', $this->file->name)) {
				$this->addError('file', "Le nom de fichier doit être composé de lettres non accentuées et de \"()_.-\"");
			}
		}
		if ($this->destType === 'logos-p') {
			if (!$this->removeDuplicates()) {
				$this->addError('overwrite', "Ce dépôt écraserait le fichier existant.");
			}
		}
		return parent::beforeValidate();
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return [
			'file' => 'Fichier',
			'overwrite' => "Remplacer le fichier déjà présent",
			'destName' => "Destination",
		];
	}

	/**
	 * @return bool
	 */
	public function canOverrideFileName()
	{
		return empty($this->getDestination()->forceDestName) || $this->overrideFileName;
	}

	/**
	 * Remove duplicates if the "overwrite" flag is set.
	 *
	 * @return bool Success?
	 */
	public function removeDuplicates()
	{
		$pattern = preg_replace('/\.\w+$/', '.*', $this->destAbsFile);
		$duplicates = glob($pattern);
		if (glob($pattern)) {
			if ($this->overwrite) {
				foreach ($duplicates as $dup) {
					unlink($dup);
				}
			} else {
				return false;
			}
		}
		return true;
	}

	/**
	 * Move the uploaded file from its temporary place to its final destination.
	 *
	 * @return bool
	 */
	public function moveFile()
	{
		if (file_exists($this->destAbsFile) && !$this->overwrite) {
			return false;
		}
		if (!$this->file->saveAs($this->destAbsFile)) {
			return false;
		}
		if ($this->destType === 'logos-p') {
			return $this->resizeLogo($this->destAbsFile);
		}
		return true;
	}

	/**
	 * Return various info on the destination of this upload.
	 *
	 * @return \stdClass
	 */
	public function getDestination()
	{
		return self::$destinations[$this->destType];
	}

	/**
	 * Return the URL for this file name and this destination.
	 *
	 * @param string $filename
	 * @param string $subpath
	 * @return string
	 */
	public function getViewUrl($filename='', $subpath='')
	{
		if (!$filename) {
			$filename = $this->destRelFile;
		}
		if (!$subpath) {
			$subpath = $this->getDestination()->subpath;
		}
		if ($subpath && substr($subpath, -1) !== '/') {
			$subpath .= '/';
		}
		return $this->getDestination()->url . $subpath . rawurlencode($filename);
	}

	/**
	 * Is the file in one of the allowed destinations?
	 * Compares the absolute path of the requested file.
	 *
	 * @param string $file path (will be converted to absolute full path)
	 * @return bool
	 */
	public static function isDownloadable(string $file): bool
	{
		if (!file_exists($file)) {
			return false;
		}

		// for a convention, check rights on the Partenaire
		$m = [];
		if (preg_match('#/conventions/(\d+)_#', $file, $m)) {
			if (!Yii::app()->user->checkAccess('partenaire/update', ['partenaireId' => (int) $m[1]])) {
				return false;
			}
		}

		// check is the path of $file is one of the destinations
		$filePath = realpath($file);
		foreach (self::$destinations as $d) {
			$destPath = realpath($d->path);
			if (strncmp($filePath, $destPath, strlen($destPath)) === 0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns a human-readable size.
	 *
	 * @param int $size
	 * @param ?string $format (opt) printf format.
	 * @return string
	 */
	public static function getReadableFileSize(int $size, string $format = null): string
	{
		$sizes = ['b', 'kB', 'MB'];

		if ($format === null) {
			$format = '%01.2f %s';
		}

		if ($size < 1024) {
			$format = '%01d %s';
			$sizeUnit = $sizes[0];
		} else {
			$sizeUnit = $sizes[0];
			foreach ($sizes as $i => $u) {
				if ($size < 1024 || $i == (count($sizes) - 1)) {
					break;
				}
				$size >>= 10;
				$sizeUnit = $u;
			}
		}
		return sprintf($format, $size, $sizeUnit);
	}

	/**
	 * @param string $file
	 * @param int $maxWidth
	 * @param int $maxHeight
	 * @return bool
	 */
	public function resizeLogo(string $file, int $maxWidth=225, int $maxHeight=55): bool
	{
		list($width, $height) = getimagesize($file);
		if (!$width || !$height) {
			$this->addError('file', "Dimensions d'image illisibles. Erreur de format ?");
			return false;
		}
		$newWidth = round($width * $maxHeight / $height);
		if ($newWidth > $maxWidth) {
			$newWidth = $maxWidth;
			$newHeight = round($height * $maxWidth/ $width);
		} else {
			$newHeight = $maxHeight;
		}
		if (strtolower(substr($file, -4)) === '.png') {
			$oldImage = imagecreatefrompng($file);
		} else {
			$oldImage = imagecreatefromjpeg($file);
		}
		if (!$oldImage) {
			$this->addError('file', "Erreur de format d'image (jpeg/png).");
			return false;
		}
		$newImage = imagecreatetruecolor($newWidth, $newHeight);
		imagealphablending($newImage, false);
		imagesavealpha($newImage, true);
		imagecopyresampled($newImage, $oldImage, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

		$destDir = dirname($file) . '/h55';
		$destFile = preg_replace('/\..+?$/', '', basename($file));
		if (file_exists("$destDir/$destFile.jpg")) {
			unlink("$destDir/$destFile.jpg");
		}
		if (!imagepng($newImage, "$destDir/$destFile.png")) {
			$this->addError('file', "Erreur d'enregistrement de l'image redimensionnée.");
			return false;
		}
		return true;
	}

	public function onUnsafeAttribute($name, $value)
	{
		// do nothing
	}
}
