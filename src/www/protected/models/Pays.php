<?php

/**
 * This is the model class for table "Pays".
 *
 * The followings are the available columns in table 'Pays':
 * @property int $id
 * @property string $nom
 * @property string $code 3-letters ISO code
 *
 * @method Pays sorted() Cf scopes
 */
class Pays extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @return Pays the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Pays';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['nom, code', 'required'],
			['nom', 'length', 'max' => 200],
			['code', 'length', 'max' => 6],
			// The following rule is used by search().
			['nom, code', 'safe', 'on' => 'search'],
		];
	}

	/**
	 * Each array key defines a function that can be chained before a find*() method.
	 */
	public function scopes()
	{
		return [
			'sorted' => [
				'order' => 'nom ASC',
			],
		];
	}
}
