<?php

/**
 * This is the model class for table "LogExt".
 *
 * The followings are the available columns in table 'LogExt':
 * @property int $id
 * @property string $level
 * @property string $category
 * @property int $logtime
 * @property string $message
 * @property string $controller
 * @property string $action
 * @property int $modelId
 * @property string $ip
 * @property int $userId
 * @property string $session
 */
class LogExt extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @return LogExt the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'LogExt';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			// The following rule is used by search().
			[
				'id, level, category, logtime, message, controller, action, ip, userId, session',
				'safe', 'on' => 'search',
			],
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'utilisateur' => [self::BELONGS_TO, 'Utilisateur', 'userId'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'level' => 'Level',
			'category' => 'Category',
			'logtime' => 'Logtime',
			'message' => 'Message',
			'controller' => 'Controller',
			'action' => 'Action',
			'modelId' => 'Model',
			'ip' => 'IP',
			'userId' => 'User',
			'session' => 'Session',
		];
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @param int|bool $pageSize Number of items for pagination (false to disable pagination).
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($pageSize=25)
	{
		$criteria = new CDbCriteria;
		$criteria->with = [
			['utilisateur' => ['select' => 'login']],
		];

		$criteria->compare('id', $this->id);
		$criteria->compare('level', $this->level);
		$criteria->compare('category', $this->category);
		$criteria->addCondition(DateVersatile::buildTsConditionFromDateValue('logtime', (string) $this->logtime));
		$criteria->compare('message', $this->message, true);
		$criteria->compare('controller', $this->controller);
		$criteria->compare('action', $this->action);
		$criteria->compare('ip', $this->ip, true);
		if ($this->userId) {
			$criteria->addCondition(
				'userId <=> ' . ((string) $this->userId === 'null' ? 'null' : (int) $this->userId)
			);
		}
		$criteria->compare('session', $this->session);

		$sort = new CSort();
		$sort->attributes = [
			'level' => ['asc' => 't.level ASC, t.id DESC', 'desc' => 't.level DESC, t.id DESC'],
			'category' => ['asc' => 't.category ASC, t.id DESC', 'desc' => 't.category DESC, t.id DESC'],
			'controller' => ['asc' => 't.controller ASC, t.id DESC', 'desc' => 't.controller DESC, t.id DESC'],
			'logtime' => ['asc' => 't.id ASC', 'desc' => 't.id DESC'],
		];
		$sort->defaultOrder = ['logtime' => CSort::SORT_DESC];

		return new CActiveDataProvider(
			$this,
			[
				'criteria' => $criteria,
				'pagination' => ['pageSize' => $pageSize],
				'sort' => $sort,
			]
		);
	}

	/**
	 * Returns the first line of the message.
	 *
	 * @return string first line of the message.
	 */
	public function getMessageTitle()
	{
		return substr(
			preg_replace(
				'/\' (in |\()\/.+$/',
				'',
				preg_replace('/^.+ with message /', '', strtok($this->message, "\n"))
			),
			0,
			74
		);
	}
}
