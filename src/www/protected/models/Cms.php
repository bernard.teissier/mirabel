<?php

use models\renderers\InterventionHelper;

/**
 * This is the model class for table "Cms".
 *
 * The followings are the available columns in table 'Cms':
 * @property int $id
 * @property string $name
 * @property bool $singlePage
 * @property string $pageTitle
 * @property string $type
 * @property string $content
 * @property ?int $partenaireId
 * @property int $private
 * @property int $hdateCreat
 * @property int $hdateModif
 *
 * @property Partenaire $partenaire
 */
class Cms extends CActiveRecord
{
	public static $enumType = ['text', 'markdown', 'html'];

	public $newdate;

	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @return Cms the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Cms';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['name', 'required'],
			['name, pageTitle', 'length', 'max' => 255],
			['singlePage, private', 'boolean'],
			['type', 'length', 'max' => 8],
			['content', 'safe'],
			['partenaireId', 'numerical', 'integerOnly' => true],
			['newdate', 'length', 'max' => 16],
			['type', 'in', 'range' => self::$enumType], // enum
			// The following rule is used by search().
			['name, pageTitle, partenaireId, hdateCreat', 'safe', 'on' => 'search'],
		];
	}

	public function afterValidate()
	{
		if ($this->name && $this->singlePage) {
			$exists = Cms::model()->find('name = :name AND singlePage = :s', [':name' => $this->name, ':s' => (int) $this->singlePage]);
			if ($exists && $exists->id != $this->id) {
				$this->addError('name', "Ce nom de page n'est pas unique.");
			}
		}
		return parent::afterValidate();
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'partenaire' => [self::BELONGS_TO, 'Partenaire', 'partenaireId'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Nom',
			'singlePage' => 'Bloc-page',
			'pageTitle' => 'Titre de la page',
			'type' => 'Type',
			'content' => 'Contenu',
			'partenaireId' => 'Partenaire',
			'private' => "Privée",
			'hdateCreat' => 'Date de publication',
			'newdate' => 'Changer cette date',
			'hdateModif' => 'Dernière modification',
		];
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @return CActiveDataProvider the data provider that finds models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria = new CDbCriteria;
		$criteria->with = ['partenaire'];

		if ($this->name === 'pas de brèves') {
			$criteria->condition = "name != 'brève'";
			$this->name = '';
		} else {
			$criteria->compare('name', $this->name, true);
		}
		$criteria->compare('partenaireId', $this->partenaireId);
		if (isset($this->singlePage)) {
			$criteria->compare('singlePage', (int) $this->singlePage);
		}

		$sort = new CSort();
		$sort->attributes = ['name', 'type', 'content', 'partenaireId', 'hdateCreat'];
		$sort->defaultOrder = 'name ASC, hdateCreat DESC';

		return new CActiveDataProvider(
			$this,
			[
				'criteria' => $criteria,
				'pagination' => false,
				'sort' => $sort,
			]
		);
	}

	/**
	 * Returns a valid HTML ID in ASCII, derived from he name.
	 *
	 * @return string
	 */
	public function getHtmlId()
	{
		return "bloc-" . preg_replace(
			'/[^\w-]+/',
			'',
			strtr(strtolower(iconv('UTF-8', 'ASCII//TRANSLIT', $this->name)), ' "', '-_')
		);
	}

	/**
	 * Returns block matching a CMS name.
	 *
	 * @param string $name Unique block identifier.
	 * @param int $partenaireId (opt)
	 * @return ?Cms Block matching the criteria.
	 */
	public static function getBlock($name, $partenaireId = null)
	{
		$c = ['name' => $name];
		if ($partenaireId) {
			$c['partenaireId'] = $partenaireId;
		}
		return self::model()->findByAttributes($c);
	}

	/**
	 * Prints the HTML block matching a CMS name.
	 *
	 * @param string $name Unique block identifier.
	 * @param int $partenaireId (opt)
	 */
	public static function printBlock($name, $partenaireId = null)
	{
		$block = self::getBlock($name, $partenaireId);
		if ($block) {
			echo '<div id="' . $block->getHtmlId() . '">' . $block->toHtml() . '</div>';
		}
	}

	/**
	 * Returns the HTML block built from the type and content attributes.
	 *
	 * @param bool $expandVariables (opt, true)
	 * @return string HTML output
	 */
	public function toHtml($expandVariables=true)
	{
		switch ($this->type) {
			case 'text':
				$output = CHtml::encode($this->content);
				break;
			case 'html':
				$output = $this->content;
				break;
			case 'markdown':
				/**
				 * @todo $md = Yii::app()->getComponent('markdown'); $md->convertToHtml(...); // pb de compatibilité
				 *
				 */
				$md = new CMarkdown();
				$output = str_replace(
					['<table>', ' align="right">'],
					['<table class="table table-striped table-hover">', ' style="text-align:right;">'],
					$md->transform($this->content)
				);
				break;
			default:
				$output = "";
		}
		if ($expandVariables) {
			return $this->filterOutput(self::expandVariables($output));
		}
		return $output;
	}

	/**
	 * Returns the HTML for the last $num news.
	 * @todo build the HTML from the selected news.
	 *
	 * @param int $num
	 * @return string HTML.
	 */
	public static function getNews($num=0)
	{
		$num = (int) $num;
		$blocks = self::model()->findAllBySql(
			"SELECT * FROM Cms WHERE name = 'brève' ORDER BY hdateCreat DESC"
			. ($num ? " LIMIT " . $num : '')
		);
		$html = '<ul class="breves">';
		foreach ($blocks as $block) {
			$html .= '<li id="actu' . $block->id . '">' . $block->toHtml(false) . "</li>\n";
		}
		return $html . "</ul>";
	}

	/**
	 * Returns the HTML for the titles of the last $num news.
	 *
	 * @param int $num
	 * @return string HTML.
	 */
	public static function getNewsTitles($num=0)
	{
		$num = (int) $num;
		$blocks = self::model()->findAllBySql(
			"SELECT * FROM Cms WHERE name = 'brève' ORDER BY hdateCreat DESC"
			. ($num ? " LIMIT " . $num : '')
		);
		$html = '<table class="breves-titres">';
		foreach ($blocks as $block) {
			list($date, $image, $title) = $block->extractTitle();
			$url = CHtml::normalizeUrl(['site/page', 'p' => 'actualite']) . '#actu' . $block->id;
			$html .= '<tr id="actu' . $block->id . '">'
				. '<td class="image">' . ($image ? CHtml::link($image, $url) : '') . '</td>'
				. '<td>'
				. ($date ? "<div class=\"date\">$date</div> " : '')
				. '<div class="actu-titre">' . CHtml::link($title, $url) . "</div>"
				. "</td>"
				. "</tr>\n";
		}
		return $html . "</table>";
	}

	/**
	 * @return int
	 */
	public static function countLinks()
	{
		$value = Yii::app()->cache->get("CMS_countLinks");
		if ($value === false) {
			$numInTitles = Yii::app()->db->createCommand(
				"SELECT count(distinct t.url) -1 + count(distinct i.sudocPpn) - 1 + count(distinct i.worldcatOcn) - 1"
				. " + SUM(length(t.liensJson) - length(REPLACE(t.liensJson, '\"url\"', 'XURL'))) AS numliens"
				. " FROM Titre t LEFT JOIN Issn i ON i.titreId = t.id"
			)->queryScalar();
			$numInEd = Yii::app()->db->createCommand("SELECT count(*) FROM Editeur WHERE url <> ''")->queryScalar()
				+ Yii::app()->db->createCommand("SELECT SUM(length(liensJson) - length(REPLACE(liensJson, '\"url\"', 'XURL'))) FROM Editeur")->queryScalar();
			$numInRess = Yii::app()->db->createCommand("SELECT count(*) FROM Ressource WHERE url <> ''")->queryScalar();
			$numInServ = Yii::app()->db->createCommand("SELECT count(DISTINCT url) FROM Service")->queryScalar();
			$value = $numInTitles + $numInEd + $numInRess + $numInServ;
			Yii::app()->cache->set("CMS_countLinks", $value, 7200); // 2h
		}
		return $value;
	}

	/**
	 * Called automatically before save().
	 *
	 * @return bool
	 */
	protected function beforeSave()
	{
		$this->hdateModif = $_SERVER['REQUEST_TIME'];
		if ($this->newdate) {
			try {
				$date = new DateTime($this->newdate);
				$this->hdateCreat = $date->getTimestamp();
			} catch (Exception $_) {
				unset($this->hdateCreat);
			}
		} elseif ($this->isNewRecord && empty($this->hdateCreat)) {
			$this->hdateCreat = $_SERVER['REQUEST_TIME'];
		}
		return parent::beforeSave();
	}

	/**
	 * Extract info from a block, notably a "Brève".
	 *
	 * @return array [$date, $image, $title]
	 */
	protected function extractTitle()
	{
		$date = '';
		$image = '';
		$title = "";
		switch ($this->type) {
			case 'text':
				do {
					$first = strtok($this->content, "\n");
				} while (trim($first) === '');
				$title = CHtml::encode($first);
				// no break
			case 'html':
				/**
				 * @todo parse the DOM instead of regexps ?
				 */
				$m = [];
				if (preg_match('/<h(\d)[^>]*>(.+?)<\1>/', $this->content, $m)) {
					$title = $m[2];
				} elseif (preg_match('/<(b|strong)[^>]*>(.+?)<\1>/', $this->content, $m)) {
					$title = $m[2];
				} else {
					$title = $this->content;
				}
				// no break
			case 'markdown':
				$md = new CMarkdown();
				$m = [];
				if (preg_match('/(?:^|\n)([^\n]+)\n(?:={3,}|-{3,})/', $this->content, $m)) {
					$title = $md->transform($m[1]);
				} elseif (preg_match('/(?:^|\n)#+\s+(.+?)/', $this->content, $m)) {
					$title = $md->transform($m[1]);
				} elseif (preg_match('/^\|\s*([^|]+?)\s*\|\s*([^|]+?)\s*\|/', $this->content, $m)) {
					$date = $m[1];
					$title = strip_tags($md->transform($m[2]), '<b><i><em><strong><br>');
					if (preg_match('/\n\s*\|\s*!\[(.+?)\]\(([^)]+?)\)\s*\|/', $this->content, $m)) {
						// image seule
						$image = CHtml::image(Yii::app()->baseUrl . $m[2], $m[1]);
					} elseif (preg_match('/\n\s*\|\s*\[\s*!\[(.+?)\]\(([^)]+?)\)\s*\]\s*\(([^)|]+)\)\s*\|/', $this->content, $m)) {
						// image dans un lien
						$image = CHtml::link(CHtml::image(Yii::app()->baseUrl . $m[2], $m[1]), $m[3]);
					}
				} else {
					$title = "Not found";
				}
		}
		return [$date, $image, $title];
	}

	/**
	 * Replaces the special variables %MYVAR% by their values.
	 * @todo list the vars, get their values, use APC, replace.
	 *
	 * @param string $text Text where to replace.
	 * @param bool $authentified
	 * @return string Text with variables replaced.
	 */
	protected static function expandVariables($text, $authentified=null)
	{
		if (!preg_match('/%[A-Z\d_]+%/', $text)) {
			return $text;
		}
		if ($authentified === null) {
			$authentified = !Yii::app()->user->isGuest;
		}
		$strings = [
			'%NB_COMPTES_TYPE_PARTENAIRES%' => "SELECT count(*) FROM Utilisateur u JOIN Partenaire p ON p.id = u.partenaireId WHERE p.type='normal' AND p.statut IN ('actif', 'provisoire') AND u.actif = 1",
			'%NB_COMPTES_TYPE_EDITEUR%' => "SELECT count(*) FROM Utilisateur u JOIN Partenaire p ON p.id = u.partenaireId WHERE p.type='editeur' AND p.statut IN ('actif', 'provisoire') AND u.actif = 1",
			'%NB_COMPTES_TYPE_TOUT%' => "SELECT count(*) FROM Utilisateur u JOIN Partenaire p ON p.id = u.partenaireId WHERE p.type IN ('normal', 'editeur') AND p.statut IN ('actif', 'provisoire') AND u.actif = 1",
			'%NB_PARTENAIRES%' => "SELECT count(*) FROM Partenaire WHERE type='normal' AND statut='actif'",
			'%NB_PARTENAIRES_TYPE_EDITEUR%' => "SELECT count(*) FROM Partenaire WHERE type='editeur' AND statut='actif'",
			'%NB_PARTENAIRES_TYPE_IMPORT%' => "SELECT count(*) FROM Partenaire WHERE type='import' AND statut='actif'",
			'%NB_PARTENAIRES_TYPE_TOUT%' => "SELECT count(*) FROM Partenaire WHERE statut='actif'",
			'%NB_PARTENAIRES_STATUT_PROVISOIRE%' => "SELECT count(*) FROM Partenaire WHERE statut = 'provisoire'",
			'%NB_PARTENAIRES_STATUT_INACTIF%' => "SELECT count(*) FROM Partenaire WHERE statut != 'actif'",
			'%NB_PARTENAIRES_TOUT%' => "SELECT count(*) FROM Partenaire",
			'%NB_REVUES%' => "SELECT count(DISTINCT revueId) FROM Titre",
			'%NB_TITRES%' => "SELECT count(*) FROM Titre",
			'%NB_REVUES_SUIVIES%' => "SELECT count(DISTINCT s.cibleId) FROM Suivi s JOIN Revue r ON s.cibleId = r.id WHERE s.cible = 'Revue'",
			'%NB_RESSOURCES%' => "SELECT count(*) FROM Ressource",
			'%NB_RESSOURCES_SUIVIES%' => "SELECT count(DISTINCT s.cibleId) FROM Suivi s JOIN Revue r ON s.cibleId = r.id WHERE s.cible = 'Ressource'",
			'%NB_EDITEURS%' => "SELECT count(*) FROM Editeur",
			'%NB_RESSOURCES_IMPORT%' => "SELECT count(DISTINCT ressourceId) FROM Service WHERE import > 0",
			'%NB_REVUES_IMPORT%' => "SELECT count(DISTINCT t.revueId) FROM Service s JOIN Titre t ON s.titreId = t.id WHERE s.import > 0",
			'%NB_REVUES_IMPORT_AUTOMATISE%' => "SELECT count(DISTINCT t.revueId)
FROM Service s JOIN Titre t ON s.titreId = t.id JOIN Ressource r ON s.ressourceId = r.id
    LEFT JOIN Service_Collection sc ON sc.serviceId = s.id LEFT JOIN Collection c ON sc.collectionId = c.id
WHERE s.import > 0 AND (r.autoImport > 0 OR c.importee > 0)",
			'%DATE_DER_MAJ%' => "SELECT MAX(hdateVal) FROM Intervention",
		];
		$db = Yii::app()->db;
		foreach ($strings as $k => $v) {
			$strings[$k] = $db->createCommand($v)->queryScalar();
		}
		$strings['%DATE_DER_MAJ%'] = strftime('%e %B %Y', $strings['%DATE_DER_MAJ%']);

		$partenaire = Yii::app()->user->getInstitute() > 0 ? Partenaire::model()->findByPk(Yii::app()->user->getInstitute()) : null;
		if ($partenaire) {
			$strings['%PARTENAIRE%'] = $partenaire->nom;
			$strings['%PARTENAIRE.ID%'] = $partenaire->id;
		} else {
			$strings['%PARTENAIRE%'] = '';
			$strings['%PARTENAIRE.ID%'] = '';
		}

		// no complete array to avoid evaluating the substitutes
		$m = [];
		if (preg_match('/%DER_ACTU_(\d+)%/', $text, $m)) {
			$strings["%DER_ACTU_{$m[1]}%"] = self::getNews($m[1]);
		}
		if (preg_match('/%DER_ACTU_TITRES_(\d+)%/', $text, $m)) {
			$strings["%DER_ACTU_TITRES_{$m[1]}%"] = self::getNewsTitles($m[1]);
		}
		if (preg_match('/%DER_MAJ_(\d+)%/', $text, $m)) {
			$strings["%DER_MAJ_{$m[1]}%"] = InterventionHelper::listLast($m[1]);
		}
		if (preg_match('/%DER_CREA_(\d+)%/', $text, $m)) {
			$strings["%DER_CREA_{$m[1]}%"] = InterventionHelper::listLastJournalCreations($m[1]);
		}
		if (strpos($text, '%NB_LIENS%') !== false) {
			$text = str_replace('%NB_LIENS%', (string) self::countLinks(), $text);
		}
		if (strpos($text, '%RATIO_LIENS%') !== false) {
			$text = str_replace('%RATIO_LIENS%', (string) (round(self::countLinks() / $strings['%NB_REVUES%'])), $text);
		}
		if (strpos($text, '%SI_CONNECTE_DEB%') !== false) {
			if ($authentified) {
				$text = str_replace(['%SI_CONNECTE_DEB%', '%SI_CONNECTE_FIN%'], '', $text);
			} else {
				$text = preg_replace('/%SI_CONNECTE_DEB%.+?%SI_CONNECTE_FIN%/s', '', $text);
			}
		}
		if (strpos($text, '%ABCD_REVUES%') !== false) {
			$letters = array_filter(Tools::sqlToPairs(
				"SELECT FIND_IN_SET(LEFT(titre, 1), "
				. "'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z') AS rank, COUNT(*) AS num "
				. "FROM Titre "
				. "WHERE statut = 'normal' AND obsoletePar IS NULL "
				. "GROUP BY rank"
			));
			$abcd = '<div class="abcd-revues"><ul>';
			foreach ($letters as $i => $count) {
				$l = ($i > 0 ? strtoupper(chr($i + 64)) : '#0-9');
				$abcd .= "<li>"
					. CHtml::link(strtoupper($l), ['revue/index', 'lettre' => $l], ['title' => $count . ' revues'])
					. "</li>\n";
			}
			$abcd .= "</ul></div>\n";
			$text = str_replace('%ABCD_REVUES%', $abcd, $text);
		}
		if (strpos($text, '%RECHERCHE%') !== false) {
			$form = '<div id="main-search-form"><h2>Rechercher une revue</h2>'
				. Yii::app()->controller->getSearchForm('')
				. CHtml::link('Recherche avancée', ['/revue/search'], ['class' => 'advanced-search'])
				. '</div>';
			$text = str_replace('%RECHERCHE%', $form, $text);
		}

		if (strpos($text, '%BIBLIOS_LOGICIELLES%') !== false) {
			$composer = new models\services\Composer(dirname(__DIR__, 4));
			$libs = $composer->getLibraries();
			$libsHtml = [];
			foreach ($libs as $lib) {
				$name = htmlspecialchars($lib->name);
				$attr = empty($lib->description) ? [] : ['title' => $lib->description];
				$libsHtml[$lib->name] = "<li>"
					. (empty($lib->homepage) ? CHtml::tag('em', $attr, $name) : CHtml::link($name, $lib->homepage, $attr))
					. (empty($lib->license) ? '' : " (" . join(", ", $lib->license) . ")")
					. "</li>";
			}
			asort($libsHtml);
			$text = str_replace('%BIBLIOS_LOGICIELLES%', "<ul>" . join("", $libsHtml) . "</ul>", $text);
		}

		return str_replace(array_keys($strings), array_values($strings), $text);
	}

	/**
	 * Filters each HTML output.
	 *
	 * @param string $html
	 * @return string
	 */
	protected function filterOutput($html)
	{
		if ($this->name === 'partenaires') {
			$isAdmin = Yii::app()->user->checkAccess('utilisateur/admin');
			$append = "<p>Récapitulatif des ABOCOUNT abonnés à la liste de diffusion mirabel_partenaires (cette liste ne comprend pas les partenaires éditeurs).</p>"
				. '<ul class="partenaires">'
				;
			$partenaires = Partenaire::model()->ordered()->findAllByAttributes(['type' => 'normal']);
			$count = 0;
			foreach ($partenaires as $partenaire) {
				/* @var $partenaire Partenaire */
				$append .= '<li><h3 class="statut-' . $partenaire->statut . '" title="' . $partenaire->statut . '">'
					. $partenaire->getSelfLink()
					. '</h3>'
					. '<div><table class="table table-condensed table-bordered"><tbody>' . "\n";
				$utilisateurs = $partenaire->utilisateurs;
				if ($utilisateurs) {
					/* @var $u Utilisateur */
					foreach ($utilisateurs as $u) {
						if ($u->actif) {
							$count++;
							$dateDiff = $u->derConnexion
								? trim((new DateTime())->setTimestamp($u->derConnexion)->diff(new DateTime)->format('%R%a jours'), '+')
								: '';
							$dateExact = date('d/m/Y', $u->derConnexion);
							$append .= "<tr>"
								. '<td width="35%">' . CHtml::encode($u->nomComplet) . "</td>"
								. '<td>' . $u->email . "</td>"
								. '<td style="width:20ex">' . ($isAdmin ? $u->getSelfLink(false) : CHtml::encode($u->login)) . "</td>"
								. (
									$isAdmin
									? '<td style="width:12ex">' . ($u->derConnexion ? "<span title=\"$dateExact\">$dateDiff</span>" : '-') . "</td>"
									: ''
								)
								. "</tr>\n";
						}
					}
					$append .= "</tbody></table></div></li>\n";
				}
			}
			$append .= "</ul>\n";
			$html .= str_replace('ABOCOUNT', (string) $count, $append);
		}
		return $html;
	}
}
