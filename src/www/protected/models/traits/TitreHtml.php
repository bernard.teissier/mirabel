<?php

/**
 * String (plain text or HTML) methods of Titre.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
trait TitreHtml
{
	/**
	 * Alias nom=titre.
	 */
	public function getNom()
	{
		return $this->titre;
	}

	/**
	 * Returns the full title, prefix included, but without its 'sigle'.
	 *
	 * @return string
	 */
	public function getPrefixedTitle()
	{
		return $this->prefixe . $this->titre;
	}

	/**
	 * Returns the full title, prefix included, 'sigle' appended.
	 *
	 * @return string
	 */
	public function getFullTitle()
	{
		return $this->prefixe . $this->titre . ($this->sigle ? ' — ' . $this->sigle : '');
	}

	/**
	 * Returns the full title, prefix included, 'sigle' appended, and periodicity (if defined).
	 *
	 * @return string
	 */
	public function getFullTitleWithPerio()
	{
		$p = $this->getPeriode();
		return $this->getFullTitle() . ($p ? " ($p)" : '');
	}

	/**
	 * Returns a short title ('sigle' if it exists).
	 *
	 * @return string
	 */
	public function getShortTitle()
	{
		if ($this->sigle) {
			return $this->sigle;
		}
		return $this->titre;
	}

	public function getUrlSudoc()
	{
		$issn = $this->getPreferedIssn();
		if ($issn && $issn->sudocPpn && !$issn->sudocNoHolding) {
			return sprintf(self::URL_SUDOC, $issn->sudocPpn);
		}
		return '';
	}

	public function getUrlWorldcat()
	{
		$issn = $this->getPreferedIssn();
		if ($issn && $issn->worldcatOcn) {
			return sprintf(self::URL_WORLDCAT, $issn->worldcatOcn);
		}
		return '';
	}

	/**
	 * Returns the HTML link to one-self.
	 *
	 * @codeCoverageIgnore
	 * @return string HTML link.
	 */
	public function getSelfLink()
	{
		$htmlOptions = [];
		if (Yii::app()->user->hasState('suivi')) {
			$suivi = Yii::app()->user->getState('suivi');
			if (!empty($suivi['Revue']) and in_array($this->revueId, $suivi['Revue'])) {
				$htmlOptions["class"] = 'suivi-self';
			}
		}
		return CHtml::link(
			$this->getFullTitle(),
			Yii::app()->createUrl('/revue/view', ['id' => $this->revueId, 'nom' => Norm::urlParam($this->getFullTitle())]),
			$htmlOptions
		);
	}

	/**
	 * Returns a date interval built from dateDebut, dateFin.
	 *
	 * @return string HTML text.
	 */
	public function getPeriode()
	{
		$periode = '';
		if ($this->dateDebut || $this->dateFin) {
			$formatter = Yii::app()->format;
			if ($this->dateDebut) {
				$periode = $formatter->formatStrDate($this->dateDebut);
			} else {
				$periode = "…";
			}
			$periode .= " – ";
			if ($this->dateFin) {
				$periode .= $formatter->formatStrDate($this->dateFin);
			} else {
				$periode .= "…";
			}
		}
		return $periode;
	}

	/**
	 * @param string $text (opt) Link texte
	 * @return string HTML
	 */
	public function getLinkWorldcat($text='')
	{
		if (!$text) {
			$text = CHtml::image(Yii::app()->baseUrl . '/images/icons/worldcat.png', 'WorldCat') . " Worldcat";
		}
		$url = $this->getUrlWorldcat();
		if (!$url) {
			return '';
		}
		return CHtml::link(
			$text,
			$url,
			[
				'class' => 'nb worldcat',
				'title' => 'WorldCat : Catalogue mondial - Online Computer Library Center - OCLC',
			]
		);
	}
}
