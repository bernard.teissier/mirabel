<?php

namespace models\traits;

trait VerbosityEcho
{
	/**
	 * conditional echo, according to verbosity level
	 *
	 * @param int $verbmin
	 * @param string $text
	 */
	protected function vecho(int $verbmin, string $text): void
	{
		if ($this->verbose >= $verbmin) {
			echo $text;
		}
	}
}
