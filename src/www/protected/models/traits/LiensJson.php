<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
trait LiensJson
{
	/**
	 * @return \Liens
	 */
	public function getLiens(): \Liens
	{
		$liens = new Liens();
		$liens->setContent($this->liensJson);
		return $liens;
	}

	/**
	 * @param array|Liens $data
	 */
	public function setLiens($data)
	{
		if ($data instanceof Liens) {
			$liens = $data;
		} else {
			$liens = new Liens();
			$liens->setContent($data);
		}
		$this->liensJson = json_encode($liens);
		return $this;
	}

	public function getLiensInternes(): ?Liens
	{
		static $liens = null;
		if ($liens === null) {
			$internal = [];
			if ($this->liensJson) {
				$internal = array_filter(
					$this->getLiens()->getContent(),
					function ($x) {
						return $x->isInternal();
					}
				);
			}
			$liens = (new Liens)->setContent($internal);
		}
		return $liens;
	}

	public function getLiensEditoriaux(): ?Liens
	{
		static $liens = null;
		if ($liens === null) {
			$editorial = [];
			if ($this->liensJson) {
				$editorial = array_filter(
					$this->getLiens()->getContent(),
					function ($x) {
						return $x->isEditorial();
					}
				);
			}
			if ($editorial) {
				$liens = (new Liens)->setContent($editorial);
				$liens->sort();
			}
		}
		return $liens;
	}

	public function getLiensNormaux(): ?Liens
	{
		static $liens = null;
		if ($liens === null) {
			$external = [];
			if ($this->liensJson) {
				$external = array_filter(
					$this->getLiens()->getContent(),
					function ($x) {
						return !($x->isInternal() || $x->isEditorial());
					}
				);
			}
			$liens = (new Liens)->setContent($external);
			$liens->sort();
		}
		return $liens;
	}

	public function getLiensExternes(): ?Liens
	{
		static $liens = null;
		if ($liens === null) {
			$external = [];
			if ($this->liensJson) {
				$external = array_filter(
					$this->getLiens()->getContent(),
					function ($x) {
						return !$x->isInternal();
					}
				);
			}
			$liens = (new Liens)->setContent($external);
		}
		return $liens;
	}

	/**
	 * @return string HTML
	 */
	public function getDetailedLinksOther(): string
	{
		if (empty($this->liensJson)) {
			return '';
		}
		$output = '<ol>';
		foreach ($this->getLiens() as $link) {
			$output .= '<li><strong>' . CHtml::encode($link->src) . '</strong> : '
				. '<span>' . CHtml::link(CHtml::encode($link->url), $link->url) . '</span></li>';
		}
		$output .= '</ol>';
		return $output;
	}
}
