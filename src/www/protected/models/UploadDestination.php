<?php

class UploadDestination extends CComponent
{
	public $name = "";

	public $message = "";

	public $path = "";

	public $url = "";

	public $forceDestName = null;

	public $subpath = "";

	public $types = ''; // extensions

	public function __construct(array $config)
	{
		foreach (['name', 'path', 'url'] as $k) {
			if (!key_exists($k, $config)) {
				throw new Exception("Erreur dans le code, la configuration est incomplète pour $k.");
			}
		}
		foreach ($config as $k => $v) {
			$this->{$k} = $v;
		}

		if (!is_dir($this->path)) {
			if (!@mkdir($this->path, 0773, true)) {
				throw new \Exception("Création du répertoire '{$this->path}' impossible. Problème de permissions de fichiers.");
			}
		} elseif (!is_writable($this->path)) {
			Yii::app()->user->setFlash(
				'error',
				"Un répertoire de destination n'est pas accessible en écriture : " . $this->path
			);
		}
	}
}
