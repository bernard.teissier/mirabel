<?php

require_once __DIR__ . '/traits/LiensJson.php';

/**
 * This is the model class for table "Editeur".
 *
 * The followings are the available columns in table 'Editeur':
 * @property int $id
 * @property string $nom
 * @property string $prefixe
 * @property string $sigle
 * @property string $description
 * @property string $presentation
 * @property ?string $idref
 * @property ?int $sherpa
 * @property string $url
 * @property string $logoUrl
 * @property ?int $paysId
 * @property string $geo
 * @property string $liensJson
 * @property string $statut
 * @property int $hdateModif timestamp
 * @property int $hdateVerif timestamp
 *
 * @property Liens $liens
 * @property Intervention[] $interventions
 * @property TitreEditeur[] $titreEditeurs
 * @property ?Pays $pays
 * @property ?Partenaire $partenaire
 */
class Editeur extends AMonitored implements IWithIndirectSuivi, TitledObject, WithCheckLinks
{
	use LiensJson;

	public static $enumStatut = [
		'normal' => 'Normal', 'suppr' => 'Supprimé', 'attente' => 'En attente',
	];

	public $confirm;

	public function behaviors()
	{
		return [
			'timestampToDate' => [
				'class' => 'application.models.behaviors.TimestampToDate',
				'fieldsDateTime' => ['hdateModif', 'hdateVerif'],
			],
			'confirmDuplicate' => [
				'class' => 'application.models.behaviors.ConfirmDuplicate',
				'field' => 'nom',
			],
		];
	}

	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @return Editeur the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Editeur';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['nom', 'required'],
			['nom, sigle, geo', 'length', 'max' => 255],
			['idref', 'match', 'pattern' => '/^[0-9]{8}[0-9X]$/'],
			['url, logoUrl', 'url'],
			['url, logoUrl', 'length', 'max' => 512],
			['url', 'ext.validators.UrlFetchableValidator', 'on' => 'insert update'],
			['description, presentation', 'length', 'max' => 65535],
			['prefixe', 'length', 'max' => 25],
			['statut', 'in', 'range' => array_keys(self::$enumStatut)], // enum
			['confirm', 'boolean'],
			['paysId, sherpa', 'numerical', 'integerOnly' => true],
			['liensJson', 'safe', 'on' => 'import'], // an Intervention uses this
			['liens', 'safe'], // will use setLiens()
			// local validator methods
			['liensJson', 'validateLiensJson'],
			// The following rule is used by search().
			[
				'nom, description, paysId, geo, statut, hdateModif, hdateVerif',
				'safe', 'on' => 'search',
			],
		];
	}

	public function validateLiensJson($attribute, $params)
	{
		if ($attribute === 'liensJson' && !empty($this->liensJson)) {
			$urlValidation = !$this->confirm;
			if (($this->scenario === 'insert' || $this->scenario === 'update') && !$this->getLiens()->validate($urlValidation)) {
				$this->addError('liensJson', join('<br />', $this->getLiens()->getErrors()));
			}
		}
	}

	/**
	 * Called automatically before delete().
	 *
	 * @return bool
	 */
	public function beforeDelete()
	{
		$countRevues = $this->countRevues();
		if ($countRevues) {
			$this->addError('id', $countRevues . " revues sont liées à cet éditeur. Suppression impossible.");
			return false;
		}
		return parent::beforeDelete();
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'interventions' => [self::HAS_MANY, 'Intervention', 'editeurId'],
			'titreEditeurs' => [self::HAS_MANY, 'TitreEditeur', 'editeurId'],
			'pays' => [self::BELONGS_TO, 'Pays', 'paysId'],
			'partenaire' => [self::HAS_ONE, 'Partenaire', 'editeurId'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'nom' => 'Nom',
			'prefixe' => 'Préfixe',
			'sigle' => 'Sigle',
			'description' => 'Description',
			'presentation' => "Présentation",
			'idref' => 'IdRef',
			'sherpa' => 'ID Sherpa',
			'url' => 'Adresse web',
			'logoUrl' => "URL du logo",
			'paysId' => "Pays",
			'geo' => 'Repère géo',
			'statut' => 'Statut',
			'partenaireId' => "Partenaire associé",
			'hdateVerif' => 'Dernière vérification',
			'hdateModif' => 'Dernière modification',
			'confirm' => "Confirmer malgré l'avertissement",
		];
	}

	/**
	 * Each array key defines a function that can be chained before a find*() method.
	 */
	public function scopes()
	{
		return [
			'sorted' => [
				'order' => 'nom ASC',
			],
		];
	}

	/**
	 * Delete related data in "Suivi".
	 *
	 * @return bool
	 */
	public function afterDelete()
	{
		if ($this->id > 0) {
			Yii::app()->db->createCommand(
				"DELETE FROM Suivi WHERE cible = '" . $this->tableName() . "' AND cibleId = " . (int) $this->id
			)->execute();
		}
		return parent::afterDelete();
	}

	/**
	 * Called automatically after save().
	 *
	 * @return bool
	 */
	public function afterSave()
	{
		$this->getLiens()->save("Editeur", (int) $this->id);
		return parent::afterSave();
	}

	/**
	 * Returns an array of parents that have a direct "Suivi": [ [table => ,  id => ], ... ]
	 *
	 * @return array
	 */
	public function listParentsForSuivi()
	{
		return [
			['table' => 'Editeur', 'id' => $this->id],
		];
	}

	/**
	 * Returns the prefix+name.
	 *
	 * @return string prefix+name.
	 */
	public function getLongName(): string
	{
		return $this->prefixe . $this->nom;
	}

	/**
	 * Returns the full name, including prefix and sigle.
	 *
	 * @return string Full name.
	 */
	public function getFullName(): string
	{
		return $this->prefixe . $this->nom . ($this->sigle ? ' — ' . $this->sigle : '');
	}

	/**
	 * Returns a list of assoc (id =>, value=> , label=>) matching the requested term.
	 *
	 * @param string $term
	 * @param array $filters  array(field => array(values,to,require))
	 * @param array $excludes array(field => array(values,to,exclude))
	 * @return array Array of assoc (value=> , label=>).
	 */
	public static function completeTerm(string $term, $filters = [], $excludes = []): array
	{
		Yii::import('ext.Sphinx.*');
		$sphinxClient = new Sphinx(Yii::app()->params->itemAt("sphinx"));
		$sphinxClient->indexes = 'editeurs';
		$termNeutered = str_replace(['-', '@', '+', '/'], ' ', $term);
		$sphinxClient->query = $sphinxClient->EscapeString($termNeutered);

		$results = [];
		if ($filters) {
			foreach ($filters as $key => $values) {
				$sphinxClient->SetFilter($key, $values);
			}
		} else {
			$exact = self::model()->findAllByAttributes(['nom' => $termNeutered]);
			if ($exact && count($exact) === 1) {
				$results[] = [
					'id' => $exact[0]->id,
					'value' => $exact[0]->getFullName(),
					'label' => $exact[0]->getFullName(),
				];
			}
		}
		if ($excludes) {
			foreach ($excludes as $key => $values) {
				$sphinxClient->SetFilter($key, $values, true);
			}
		}
		$sphinxClient->SetSortMode(SPH_SORT_EXTENDED, 'cletri ASC');
		$data = $sphinxClient->fetchData();
		if (!empty($data['matches'])) {
			foreach ($data['matches'] as $id => $row) {
				$results[] = [
					'id' => $id,
					'value' => $row['attrs']['nomcomplet'],
					'label' => $row['attrs']['nomcomplet'],
				];
			}
			if (count($data['matches']) < (int) $data['total_found']) {
				$results[] = ['id' => '', 'value' => $term, 'label' => '…'];
			}
		}
		return $results;
	}

	/**
	 * Find the instances ID that have this exact name.
	 *
	 * @param string $term
	 * @return array array of ID.
	 */
	public static function findExactTerm($term)
	{
		return Yii::app()->db->createCommand(
			"SELECT id FROM Editeur WHERE nom = :term"
		)->queryAll(true, [':term' => $term]);
	}

	/**
	 * Returns a Data Provider for this model.
	 *
	 * @param string $letter In "0", "a", ..., "z"
	 * @return \SphinxDataProvider
	 */
	public static function listAlpha(string $letter): \SphinxDataProvider
	{
		$pager = new PagerAlpha();
		$pager->elementName = 'éditeur';
		$pager->setLettersFilter(
			Tools::sqlToPairs(
				"SELECT FIND_IN_SET(LEFT(nom, 1), "
				. "'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z') AS rank, COUNT(*) AS num "
				. "FROM Editeur "
				. "WHERE statut = 'normal' "
				. "GROUP BY rank"
			)
		);
		$pager->setCurrentLetter($letter);

		Yii::import('ext.Sphinx.*');
		$sphinxClient = new Sphinx(Yii::app()->params->itemAt("sphinx"));
		$sphinxClient->indexes = 'editeurs';
		$sphinxClient->SetFilter('lettre1', [$pager->getCurrentPage()]);

		$sort = new SphinxSort([]);
		$sort->defaultOrder = 'cletri ASC';

		return new SphinxDataProvider(
			$sphinxClient,
			null, // no ActiveRecord models
			[
				'pagination' => $pager,
				'sort' => $sort,
			]
		);
	}

	/**
	 * @param int $paysId
	 * @return array { editeurId: {nbrevues: X, suivi: [...]} }
	 */
	public static function searchByCountry(int $paysId): array
	{
		Yii::import('ext.Sphinx.*');
		$sphinxClient = new Sphinx(Yii::app()->params->itemAt("sphinx"));
		$sphinxClient->indexes = 'editeurs';
		$sphinxClient->SetFilter('paysid', [$paysId]);
		$sphinxClient->SetLimits(0, 1000);
		$data = $sphinxClient->fetchData();
		$r = [];
		if (isset($data['matches'])) {
			foreach ($data['matches'] as $eid => $m) {
				$r[$eid] = $m['attrs'];
			}
		}
		return $r;
	}

	/**
	 * Returns a link toward editeur/view/id/...
	 *
	 * @return string HTML link.
	 */
	public function getSelfLink($short = false)
	{
		$options = ['itemprop' => "publisher"];
		if ($short && $this->sigle) {
			$name = $this->sigle;
			$options['title'] = $this->getFullName();
		} else {
			$name = $this->getFullName();
		}
		return CHtml::link(
			CHtml::encode($name),
			$this->getSelfAbsoluteUrl(),
			$options
		);
	}

	public function getSelfAbsoluteUrl(): string
	{
		return Yii::app()->createAbsoluteUrl(
			'/editeur/view',
			['id' => $this->id, 'nom' => Norm::urlParam($this->nom . ($this->sigle ? ' — ' . $this->sigle : ''))]
		);
	}

	public function getSelfUrl(): array
	{
		return [
			'/editeur/view',
			'id' => $this->id,
			'nom' => Norm::urlParam($this->nom . ($this->sigle ? ' — ' . $this->sigle : '')),
		];
	}

	/**
	 * Returns the number of 'Revue' linked to this through 'Titre'.
	 *
	 * @return int Number of 'Revue'.
	 **/
	public function countRevues()
	{
		$sql = "SELECT count(DISTINCT t.revueId) "
			. "FROM Titre_Editeur te JOIN Titre t ON (te.titreId=t.id) "
			. "WHERE t.statut = 'normal' AND te.editeurId = {$this->id}";
		return (int) Yii::app()->db->createCommand($sql)->queryScalar();
	}

	/**
	 * Builds an Intervention object that can be completed later.
	 */
	public function buildIntervention(bool $direct): Intervention
	{
		$i = parent::buildIntervention($direct);
		if (isset($this->id)) {
			$i->editeurId = $this->id;
			$i->description = "Modification de l'éditeur « {$this->nom} »";
			$i->action = 'editeur-U';
		}
		$i->suivi = null !== Suivi::isTracked($this);
		return $i;
	}

	/**
	 * Helper function that returns an IMG if possible (checks if the file exists).
	 *
	 * @codeCoverageIgnore
	 * @param bool $forceRefresh
	 * @return string URL
	 */
	public function getLogoUrl($forceRefresh = false): string
	{
		// only if this Editeur is also a Partenaire.
		if ($this->partenaire === null) {
			return '';
		}

		$relativePath = '/images/editeurs/';
		$path = Yii::getPathOfAlias('webroot') . $relativePath;
		$url = '';
		if (file_exists($path . sprintf('editeur-%06d.png', $this->id))) {
			$url = sprintf('%s%sediteur-%06d.png', Yii::app()->getBaseUrl(true), $relativePath, $this->id);
		}
		if ($url && $forceRefresh) {
			$url .= '?refresh=' . rand(1000, 9999);
		}
		if (!$url && $this->logoUrl) {
			$url = $this->logoUrl;
		}
		return $url;
	}

	/**
	 * Helper function that returns an IMG if possible (checks if the file exists).
	 *
	 * @codeCoverageIgnore
	 * @param bool $reduced
	 * @param bool $forceRefresh
	 * @return string HTML img or plain text.
	 */
	public function getLogoImg($reduced = true, $forceRefresh = false): string
	{
		$url = $this->getLogoUrl($forceRefresh);
		if ($url) {
			return CHtml::image($url, "logo " . $this->nom, ['class' => 'editeur-logo']);
		}
		return '';
	}

	/**
	 * Check the links of the sub-objects.
	 *
	 * @param LinkChecker|null $linkChecker
	 * @return array assoc('urls'=> , 'checked'=> )
	 */
	public function checkLinks(?LinkChecker $linkChecker = null): array
	{
		$urls = [];
		$checks = [
			'Editeur' => [],
			'Collection' => [],
		];

		$urls[] = $this->url;
		$urls[] = $this->logoUrl;
		if ($this->liensJson) {
			foreach ($this->getLiens() as $link) {
				$urls[] = $link->url;
			}
		}
		$checks['Editeur'][$this->id] = $this;

		if ($linkChecker === null) {
			$linkChecker = new LinkChecker();
		}
		return [
			'urls' => $linkChecker->checkLinks(array_filter($urls)),
			'checked' => $checks,
		];
	}

	/**
	 * Called automatically before validate().
	 *
	 * @return bool
	 */
	protected function beforeValidate()
	{
		if (strlen($this->prefixe)) {
			$this->prefixe = str_replace(["’", "´"], "'", $this->prefixe);
			if (preg_match('/[\'-]\s*$/', $this->prefixe)) {
				$this->prefixe = trim($this->prefixe);
			} elseif (!preg_match('/[\s ]$/', $this->prefixe)) {
				$this->prefixe = trim($this->prefixe) . " ";
			}
		}
		if ($this->nom) {
			$this->nom = str_replace(["’", "´"], "'", trim($this->nom));
		}
		if ($this->geo) {
			$this->geo = trim(str_replace(["\t", "’", "´"], [" ", "'", "'"], $this->geo));
		}
		$m = [];
		if ($this->idref) {
			if (preg_match('#^https?://www\.idref\.fr/(\d+X?)$#', trim($this->idref), $m)) {
				$this->idref = $m[1];
			}
		} else {
			$this->idref = null;
		}
		if ($this->sherpa) {
			if (preg_match('#^https?://v2\.sherpa\.ac\.uk/id/publisher/(\d+)$#', trim($this->sherpa), $m)) {
				$this->sherpa = $m[1];
			}
		} else {
			$this->sherpa = null;
		}
		return parent::beforeValidate();
	}

	/**
	 * Called automatically before Save().
	 *
	 * @return bool
	 */
	protected function beforeSave()
	{
		$this->hdateModif = $_SERVER['REQUEST_TIME'];
		$this->nom = Tools::normalizeText($this->nom);
		$this->description = Tools::normalizeText($this->description);
		if (empty($this->presentation)) {
			$this->presentation = '';
		}
		if (empty($this->idref)) {
			$this->idref = null;
		}
		if (empty($this->sherpa)) {
			$this->sherpa = null;
		}
		if (empty($this->logoUrl)) {
			$this->logoUrl = '';
		}
		if (empty($this->paysId)) {
			$this->paysId = null;
		}
		if (isset($this->partenaire)) {
			$pChanged = false;
			foreach (['nom', 'prefixe', 'sigle', 'description', 'url', 'paysId', 'geo'] as $attr) {
				if ($this->partenaire->{$attr} !== $this->{$attr}) {
					$this->partenaire->{$attr} = $this->{$attr};
					$pChanged = true;
				}
			}
			if ($pChanged) {
				$this->partenaire->save(false);
			}
		}
		return parent::beforeSave();
	}
}
