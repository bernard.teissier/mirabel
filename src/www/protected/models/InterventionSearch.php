<?php

/**
 * @method int buildTsConditionFromDateAttr(string $a, string $b) from the behaviours
 */
class InterventionSearch extends Intervention
{
	/**
	 * @var ?int
	 */
	public $delai;

	public $ressource_nom;

	public $titre_titre;

	public $editeur_nom;

	public $partenaireId;

	public $suiviPartenaireId;

	public $suivi = true;

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			[
				'ressourceId, revueId, titreId, editeurId, import, utilisateurIdProp, utilisateurIdVal, partenaireId, suiviPartenaireId, delai',
				'numerical', 'integerOnly' => true,
			],
			['suivi', 'boolean'],
			['description, commentaire, email', 'length', 'max' => 255],
			['statut', 'in', 'range' => array_keys(Intervention::STATUTS)],
			[
				'ressource_nom, titre_titre, editeur_nom, hdateProp, hdateVal, ip, action',
				'safe', 'on' => 'search',
			],
		];
	}

	public function attributeLabels()
	{
		return array_merge(parent::attributeLabels(), [
			'delai' => "Délai de validation",
			'ressource_nom' => 'ressource',
			'titre_titre' => 'Titre',
			'editeur_nom' => 'Éditeur',
			'partenaireId' => 'Partenaire',
			'suiviPartenaireId' => 'Suivi par',
		]);
	}

	public function afterValidate()
	{
		parent::afterValidate();
		if ($this->delai === '') {
			$this->delai = null;
		}
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @param int|bool $pageSize Number of items for pagination (false to disable pagination).
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($pageSize = 25)
	{
		$criteria = new CDbCriteria;
		$criteria->select = [
			't.*',
			<<<EOSQL
			IF(
				s1.partenaireId IS NULL AND s2.partenaireId IS NULL AND s3.partenaireId IS NULL,
				'',
				CONCAT(
					IFNULL(GROUP_CONCAT(DISTINCT s1.partenaireId SEPARATOR ','), ''),
					',',
					IFNULL(GROUP_CONCAT(DISTINCT s2.partenaireId SEPARATOR ','), ''),
					',',
					IFNULL(GROUP_CONCAT(DISTINCT s3.partenaireId SEPARATOR ','), '')
				)
			) AS suivi
			EOSQL
		];
		$criteria->with = [
			'ressource' => ['select' => 'nom, prefixe'],
			'titreRevue' => ['select' => 'titre, prefixe'],
			'editeur' => ['select' => 'nom, prefixe'],
		];
		$criteria->join =
			<<<EOSQL
			LEFT JOIN Suivi s1 ON s1.cible = 'Revue' AND t.revueId = s1.cibleId
			LEFT JOIN Suivi s2 ON s2.cible = 'Ressource' AND t.ressourceId = s2.cibleId
			LEFT JOIN Suivi s3 ON s3.cible = 'Editeur' AND t.editeurId = s3.cibleId
			EOSQL;
		$criteria->group = "t.id";

		$criteria->compare('t.description', $this->description, true);
		$criteria->compare('ressource.nom', $this->ressource_nom, true);
		$criteria->compare('titreRevue.titre', $this->titre_titre, true);
		$criteria->compare('editeur.nom', $this->editeur_nom, true);
		$criteria->compare('t.ressourceId', $this->ressourceId);
		$criteria->compare('t.revueId', $this->revueId);
		$criteria->compare('t.titreId', $this->titreId);
		$criteria->compare('t.editeurId', $this->editeurId);
		$criteria->compare('t.statut', $this->statut);
		if ($this->hdateProp) {
			$criteria->addCondition($this->buildTsConditionFromDateAttr('t.hdateProp', 'hdateProp'));
		}
		if ($this->hdateVal) {
			$criteria->addCondition($this->buildTsConditionFromDateAttr('t.hdateVal', 'hdateVal'));
		} elseif ($this->statut === 'attente') {
			// optimize the SQL query by using this indexed column
			$criteria->addCondition("t.hdateVal IS NULL");
		} elseif ($this->statut === 'accepté') {
			// optimize the SQL query by using this indexed column
			$criteria->addCondition("t.hdateVal IS NOT NULL");
		}
		if ($this->delai > 0) {
			$criteria->addCondition("t.statut IN ('accepté', 'refusé')");
			$criteria->addCondition("t.import = 0");
			$criteria->addCondition("t.hdateVal IS NOT NULL AND (t.hdateVal - t.hdateProp) > {$this->delai}");
		}
		if (isset($this->import) && $this->import !== '') {
			$criteria->compare('t.import', (int) $this->import);
		}
		if ($this->utilisateurIdProp === '0') {
			$criteria->addCondition("utilisateurIdProp IS NULL");
			$criteria->compare('t.import', 0); // intervention manuelle
		} elseif ($this->utilisateurIdProp > 0) {
			$criteria->compare('utilisateurIdProp', $this->utilisateurIdProp);
		}
		$criteria->compare('utilisateurIdVal', $this->utilisateurIdVal);
		if ($this->partenaireId) {
			$usersId = Yii::app()->db
				->createCommand("SELECT id FROM Utilisateur WHERE partenaireId = :pid")
				->queryColumn([':pid' => $this->partenaireId]);
			$criteria->addInCondition('utilisateurIdProp', $usersId);
		}
		if ($this->suiviPartenaireId) {
			$criteria->addCondition(
				sprintf(
					's1.partenaireId = %d OR s2.partenaireId = %d OR s3.partenaireId = %d',
					$this->suiviPartenaireId, $this->suiviPartenaireId, $this->suiviPartenaireId
				)
			);
		}

		$criteria->compare('ip', $this->ip);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('commentaire', $this->commentaire, true);
		$criteria->compare('action', $this->action);

		if ($this->suivi) {
			/* More logical, but slower than listing the target IDs.
			 *
			$pid = (int) Yii::app()->user->getState('partenaireId');
			$criteria->addCondition("s1.partenaireId = $pid OR s2.partenaireId = $pid OR s3.partenaireId = $pid");
			 */
			$condSuivi = Yii::app()->user->buildConditionSuivi();
			if ($condSuivi) {
				$criteria->addCondition($condSuivi);
			}
		}

		$sort = new CSort();
		$sort->attributes = ['statut', 'hdateProp', 'hdateVal'];
		if ($this->revueId || $this->ressourceId || $this->editeurId) {
			$sort->defaultOrder = "(t.statut = 'attente') DESC, hdateVal DESC, hdateProp DESC";
		} else {
			$sort->defaultOrder = 'hdateProp DESC';
		}

		$countCriteria = clone $criteria;
		$countCriteria->select = "t.id";
		if (empty($this->suivi) && empty($this->suiviPartenaireId)) {
			$countCriteria->join = '';
		}
		if (empty($this->ressource_nom) && empty($this->titre_titre) && empty($this->editeur_nom)) {
			$countCriteria->with = null;
		}

		return new CActiveDataProvider(
			$this,
			[
				'criteria' => $criteria,
				'pagination' => ['pageSize' => $pageSize],
				'sort' => $sort,
				'countCriteria' => $countCriteria,
			]
		);
	}

	/**
	 * @return array of strings
	 */
	public function getSummary()
	{
		$criteria = [];

		$texts = ['description', 'ressource_nom', 'titre_titre', 'editeur_nom', 'statut', 'action', 'hdateProp', 'hdateVal'];
		foreach ($texts as $k) {
			if ($this->{$k}) {
				$criteria[$k] = "[" . $this->getAttributeLabel($k) . " : " . $this->{$k} . "]";
			}
		}

		$ids = [
			'ressourceId' => 'Ressource.nom',
			'revueId' => 'Titre.titre',
			'editeurId' => 'Editeur.nom',
			'utilisateurIdProp' => 'Utilisateur.login',
			'utilisateurIdVal' => 'Utilisateur.login',
			'partenaireId' => 'Partenaire.nom',
			'suiviPartenaireId' => 'Partenaire.nom',
		];
		foreach ($ids as $k => $rel) {
			if ($k === 'utilisateurIdProp' && $this->$k === '0') {
				$criteria['utilisateurIdProp'] = "[" . $this->getAttributeLabel($k) . ' : anonyme]';
			}
			if ($this->{$k}) {
				list($class, $field) = explode('.', $rel);
				if ($class === 'Titre') {
					$record = Titre::model()->findBySql("SELECT * FROM Titre WHERE revueId = :id ORDER BY obsoletePar ASC LIMIT 1", [':id' => $this->{$k}]);
				} else {
					$record = $class::model()->findByPk($this->{$k});
				}
				$criteria[$k] = "[" . $this->getAttributeLabel($k)
					. " : " . ($record->{$field} ?? "{$this->{$k}}/non-trouvé") . "]";
			}
		}

		if ($this->suivi) {
			$criteria['suivi'] = "[Mon suivi]";
		}
		if ($this->delai) {
			$criteria['delai'] = "[Délai de validation > {$this->delai}s]";
		}
		if ($this->import) {
			$criteria['import'] = "[Import : " . ImportType::getSourceName($this->import) . "]";
		}

		return $criteria;
	}
}
