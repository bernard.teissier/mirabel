<?php

namespace models\forms;

class SudocImport extends \CFormModel
{
	public $simulation = false;

	public $xlsx;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return [
			['simulation', 'boolean'],
			['xlsx', 'required'],
			['xlsx', 'file', 'types' => 'xlsx'],
		];
	}

	public function afterValidate()
	{
		$this->simulation = (bool) $this->simulation;
	}

	/**
	 * Declares customized attribute labels.
	 */
	public function attributeLabels()
	{
		return [
			'xlsx' => 'Fichier XLSX à importer',
		];
	}
}
