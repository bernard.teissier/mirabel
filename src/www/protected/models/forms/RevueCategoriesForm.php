<?php

namespace models\forms;

use Categorie;
use CategorieRevue;
use CHtml;
use Intervention;
use Yii;

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class RevueCategoriesForm extends \CModel
{
	public $add = [];

	public $forceProposition = true;

	public $remove = [];

	public $revueId = 0;

	/**
	 * @var \Revue
	 */
	public $revue;

	public function __construct(\Revue $revue)
	{
		$this->revue = $revue;
	}

	public function attributeNames()
	{
		return ['add', 'forceProposition', 'remove', 'revueId'];
	}

	public function rules()
	{
		return [
			['revueId', 'numerical', 'integerOnly' => true, 'allowEmpty' => false],
			[['add', 'remove'], 'ext.validators.ArrayOfIntValidator'],
			['forceProposition', 'boolean'],
		];
	}

	public function afterValidate()
	{
		$this->revueId = $this->revueId > 0 ? (int) $this->revueId : null;
		if ($this->revueId !== (int) $this->revue->id) {
			throw new \CHttpException(500, "Incohérence des données de revue.");
		}
		return parent::afterValidate();
	}

	public function createIntervention(): Intervention
	{
		$i = new Intervention();
		$i->setAttributes(
			[
				'description' => "Thématique de la revue « " . $this->revue->getFullTitle() . " »",
				'ressourceId' => null,
				'revueId' => (int) $this->revue->id,
				'titreId' => null,
				'editeurId' => null,
				'statut' => 'attente',
				'import' => 0,
				'action' => 'revue-I',
				'ip' => $_SERVER['REMOTE_ADDR'] ?? '',
				'contenuJson' => new \InterventionDetail(),
			],
			false
		);
		if (PHP_SAPI !== 'cli') {
			$i->utilisateurIdProp = (isset(Yii::app()->user) && empty(Yii::app()->user->isGuest) ? Yii::app()->user->id : null);
		}
		$i->contenuJson->confirm = true;
		return $i;
	}

	/**
	 * @param \InterventionDetail $d
	 * @return string HTML summary of the changes
	 */
	public function applyChanges(\InterventionDetail $d): string
	{
		$message = "";
		if (isset($this->remove)) {
			$ids = array_filter(array_unique($this->remove));
			foreach ($ids as $toRemove) {
				$link = CategorieRevue::model()->findByAttributes(['revueId' => $this->revue->id, 'categorieId' => $toRemove]);
				if ($link) {
					$categorie = Categorie::model()->findByPk($toRemove);
					$d->delete($link);
					$message .= '<div class="alert alert-success">Thème <b>' . CHtml::encode($categorie->categorie) . "</b> retiré.</div>";
				} else {
					$message .= "<div>L'association avec le thème d'ID " . (int) $toRemove
						. " n'existe pas et ne peut donc être supprimée.</div>";
				}
			}
		}
		if (isset($this->add)) {
			$ids = array_filter(array_unique($this->add));
			foreach ($ids as $toAdd) {
				$exists = CategorieRevue::model()->findByAttributes(['revueId' => $this->revue->id, 'categorieId' => $toAdd]);
				if ($exists) {
					$categorie = Categorie::model()->findByPk($toAdd);
					$message .= '<div class="alert alert-error">'
						. "L'association avec le thème <b>" . CHtml::encode($categorie->categorie) . "</b> d'ID " . (int) $toAdd
						. " existe déjà et ne peut donc être ajoutée.</div>";
				} else {
					$categorie = Categorie::model()->findByPk($toAdd);
					if (!$categorie) {
						$message .= '<div class="alert alert-error">Erreur, thème introuvable (ID = ' . (int) $toAdd . ").</div>";
					} elseif ($categorie->profondeur == 1) {
						$message .= '<div class="alert alert-error"><b>'
							. CHtml::encode($categorie->categorie)
							. "</b> : erreur, les thèmes de premier niveau ne sont pas autorisés, seulement ceux de niveau 2 ou 3.</div>";
					} elseif ($categorie->role !== "public" && !Yii::app()->user->checkAccess('indexation')) {
						$message .= '<div class="alert alert-error"><b>'
							. CHtml::encode($categorie->categorie)
							. "</b> : erreur, les thèmes publics sont autorisés, pas les candidats.</div>";
					} else {
						$message .= '<div class="alert alert-success">Thème <b>' . CHtml::encode($categorie->categorie) . "</b> ajouté"
							. ($categorie->role !== "public" ? " <b>(attention, thème non-public)</b>" : "") . "</div>";
						$link = new CategorieRevue();
						$link->revueId = (int) $this->revue->id;
						$link->categorieId = (int) $toAdd;
						$link->modifPar = Yii::app()->user->isGuest ? null : Yii::app()->user->id;
						$d->create($link);
					}
				}
			}
		}
		return $message;
	}

	public function isProposition(): bool
	{
		if (PHP_SAPI === 'cli' && !defined('YII_TEST')) {
			return false;
		}
		if (Yii::app()->user->isGuest) {
			return true;
		}
		if ($this->hasExternalTracker()) {
			// someone else is monitoring this Revue
			return $this->forceProposition;
		}
		return false;
	}

	/**
	 * Return true if some other Partenaire is tracking this Revue, and the current user is not.
	 *
	 * @return bool
	 */
	public function hasExternalTracker(): bool
	{
		if (Yii::app()->user->isGuest) {
			$partenaireId = 0;
		} else {
			$partenaireId = (int) Yii::app()->user->partenaireId;
		}
		$suiveurs = array_map(
			'intval',
			Yii::app()->db
				->createCommand("SELECT partenaireId FROM Suivi WHERE cible = 'Revue' AND cibleId = ?")
				->queryColumn([$this->revue->id])
		);
		return (
			!empty($suiveurs) // this Revue has trackers
			&& !in_array($partenaireId, $suiveurs, true) // the current user is not among those trackers
		);
	}
}
