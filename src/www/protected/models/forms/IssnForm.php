<?php

use models\sudoc\Api;
use models\validators\IssnValidator;

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class IssnForm extends CModel
{
	public $issn = '';

	public $revueId;

	/**
	 * @var string[]
	 */
	private $issns = [];

	/**
	 * @var \models\sudoc\Notice[]
	 */
	private $sudocNotices = [];

	public function rules()
	{
		return [
			['issn', 'filter', 'filter' => 'trim'],
			['issn', 'length', 'max' => '60'],
			['revueId', 'numerical', 'integerOnly' => true, 'allowEmpty' => true],
		];
	}

	public function afterValidate()
	{
		if ($this->issn) {
			$this->issns = preg_split('/[\s,]+/', $this->issn);
			$validator = new IssnValidator();
			foreach ($this->issns as $issn) {
				if (!$validator->validateString($issn)) {
					$this->addError('issn', "'$issn' n'est pas au format ISSN.");
				}
			}
			$this->issn = join(" ", $this->issns);
		}
		if ($this->issns) {
			foreach ($this->issns as $issn) {
				$titre = Titre::model()->findBySql(
					"SELECT t.* FROM Titre t JOIN Issn i ON t.id = i.titreId WHERE i.issn = :issn",
					[':issn' => $issn]
				);
				if ($titre) {
					$this->addError('issn', "L'ISSN '$issn' est déjà attribué au titre « {$titre->titre} » dans Mir@bel.");
				}
			}
		}
		if (!$this->hasErrors()) {
			$this->sudocNotices = [];
			$api = new Api();
			//$lastIssnl = 'start';
			try {
				foreach ($this->issns as $issn) {
					$notice = $api->getNoticeByIssn($issn);
					/*
					if ($lastIssnl !== 'start' && $lastIssnl !== $notice->issnl) {
						$this->addError('issn', "Les ISSN-L de ces ISSN ne sont pas identiques : {$lastIssnl} et {$notice->issnl}.");
					}
					$lastIssnl = $notice->issnl;
					 */
					$this->sudocNotices[] = $notice;
				}
			} catch (Exception $e) {
				$this->addError('issn', $e->getMessage());
			}
		}
		return parent::afterValidate();
	}

	public function attributeNames()
	{
		return ['issn'];
	}

	public function attributeLabels()
	{
		return ['issn' => 'ISSN'];
	}

	/**
	 * @return array
	 */
	public function getSudocNotices()
	{
		return array_filter($this->sudocNotices);
	}
}
