<?php

namespace models\forms;

use UserIdentity;
use Utilisateur;
use Yii;

/**
 * This is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class Login extends \CFormModel
{
	/**
	 * @var string
	 */
	public $username;

	/**
	 * @var string
	 */
	public $password;

	/**
	 * @var bool
	 */
	public $rememberMe;

	/**
	 * @var UserIdentity
	 */
	private $_identity;

	/**
	 * Declares the validation rules.
	 *
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return [
			// username and password are required
			['username, password', 'required'],
			// rememberMe needs to be a boolean
			['rememberMe', 'boolean'],
			// password needs to be authenticated
			['password', 'authenticate'],
		];
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return [
			'username' => 'Identifiant',
			'password' => 'Mot de passe',
			'rememberMe' => 'Remember me next time',
		];
	}

	/**
	 * Authenticates the password.
	 *
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute)
	{
		if (!$this->hasErrors()) {
			$this->_identity = new UserIdentity($this->username, $this->password);
			if (!$this->_identity->authenticate()) {
				$this->addError('password', $this->_identity->getMessage());
			}
		}
	}

	/**
	 * Logs in the user using the given username and password in the model.
	 *
	 * @return bool whether login is successful
	 */
	public function login()
	{
		if ($this->_identity === null) {
			$this->_identity = new UserIdentity($this->username, $this->password);
			$this->_identity->authenticate();
		}
		if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
			Yii::app()->user->login($this->_identity, 0);
			Utilisateur::model()->updateByPk(
				Yii::app()->user->id,
				['derConnexion' => $_SERVER['REQUEST_TIME']]
			);
			return true;
		}
		return false;
	}

	public function getUtilisateur(): ?Utilisateur
	{
		if ($this->_identity->isAuthenticated) {
			return Utilisateur::model()->findByPk($this->_identity->id);
		}
		return null;
	}
}
