<?php

namespace models\forms;

class StatsForm extends \CFormModel
{
	public $startDate;

	public $endDate;

	public $startTs;

	public $endTs;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return [
			['startDate', 'date', 'format' => 'yyyy-MM-dd', 'timestampAttribute' => 'startTs'],
			['endDate', 'date', 'format' => 'yyyy-MM-dd', 'timestampAttribute' => 'endTs'],
		];
	}

	public function afterValidate()
	{
		if (preg_match('/^(\d{4})-(\d\d)-(\d\d)$/', $this->endDate, $m)) {
			// ends at 23:59 and not 00:00
			$this->endTs = mktime(23, 59, 59, $m[2], $m[3], $m[1]);
		}
		return parent::afterValidate();
	}

	/**
	 * Declares customized attribute labels.
	 */
	public function attributeLabels()
	{
		return [
			'startDate' => 'Date de début',
			'endDate' => 'Date de fin',
		];
	}

	public function initDates()
	{
		$this->endTs = $_SERVER['REQUEST_TIME'];
		$this->startTs = $this->endTs - 365 * 24 * 3600; // ~ 1 year ago
		$this->endDate = date('Y-m-d', $this->endTs);
		$this->startDate = date('Y-m-d', $this->startTs);
	}
}
