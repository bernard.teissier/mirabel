<?php

Yii::import('application.extensions.phpass.PasswordHash');

/**
 * This is the model class for table "Utilisateur".
 *
 * The followings are the available columns in table 'Utilisateur':
 * @property int $id
 * @property int $partenaireId
 * @property string $login
 * @property string $email
 * @property string $nom
 * @property string $prenom
 * @property string $nomComplet
 * @property bool $actif
 * @property int $derConnexion timestamp
 * @property int $hdateCreation timestamp
 * @property int $hdateModif timestamp
 * @property bool $permAdmin
 * @property bool $permImport
 * @property bool $permPartenaire
 * @property bool $permIndexation
 * @property bool $permRedaction
 * @property bool $suiviEditeurs
 * @property bool $suiviNonSuivi
 * @property bool $suiviPartenairesEditeurs
 * @property string $motdepasse
 * @property bool $authLdap
 * @property bool $listeDiffusion
 *
 * @property Log[] $logs
 * @property Partenaire $partenaire
 */
class Utilisateur extends CActiveRecord implements TitledObject
{
	public $passwordDuplicate;

	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @return Utilisateur the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Utilisateur';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		if ($this->scenario == 'updateown') {
			return [
				['email', 'length', 'max' => 100],
				['email', 'email'],
				['listeDiffusion', 'boolean'],
				['motdepasse', 'length', 'max' => 50],
				['motdepasse', 'checkStrength'],
				['passwordDuplicate', 'length', 'max' => 50],
				['passwordDuplicate', 'compare', 'compareAttribute' => 'motdepasse'],
			];
		}
		return [
			['partenaireId, login', 'required'],
			['login', 'match', 'pattern' => '/^[\w\d-]+$/'], // alphanum + "-"
			['partenaireId', 'numerical', 'integerOnly' => true],
			['login', 'length', 'max' => 50],
			['login', 'unique', 'on' => ['insert', 'insert-web']],
			['email, nom, prenom', 'length', 'max' => 100],
			['nomComplet', 'length', 'max' => 255],
			['motdepasse', 'length', 'max' => 50],
			['motdepasse', 'checkStrength', 'on' => 'insert-web, update-web'],
			['passwordDuplicate', 'length', 'max' => 50, 'on' => 'insert-web, update-web'],
			['passwordDuplicate', 'compare', 'compareAttribute' => 'motdepasse', 'on' => 'insert-web, update-web'],
			[
				'actif, permAdmin, permImport, permPartenaire, permIndexation, permRedaction,'
				. ' suiviEditeurs, suiviNonSuivi, suiviPartenairesEditeurs, authLdap, listeDiffusion',
				'boolean',
			],
		];
	}

	public function checkStrength(string $attr, array $params)
	{
		$this->{$attr} = trim($this->{$attr});
		if (empty($this->{$attr})) {
			return;
		}
		$msgError = "Le mot de passe doit avoir au moins 6 caractères mélangeant lettres, chiffres et autres.";
		if (strlen($this->{$attr}) < 6) {
			$this->addError($attr, $msgError);
			return;
		}
		if (preg_match('/^[a-z0-9]*$/i', $this->{$attr})) {
			$this->addError($attr, $msgError);
			return;
		}
		if (!preg_match('/\d/', $this->{$attr})) {
			$this->addError($attr, $msgError);
			return;
		}
		if (!preg_match('/[a-z]/i', $this->{$attr})) {
			$this->addError($attr, $msgError);
			return;
		}
	}

	/**
	 * Called automatically before validate().
	 *
	 * @return bool
	 */
	public function beforeValidate()
	{
		if (isset($this->motdepasse) && $this->motdepasse === ''
			&& ($this->scenario === 'update' || $this->scenario === 'updateown')) {
			unset($this->motdepasse);
		}
		if (
				$this->authLdap
				&& !empty($this->motdepasse)
				&& ($this->scenario === 'update' || $this->scenario === 'create')
			) {
			$this->addError('motdepasse', "Un utilisateur LDAP ne peut pas avoir de mot de passe.");
		}
		return parent::beforeValidate();
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'logs' => [self::HAS_MANY, 'Log', 'utilisateurId'],
			'partenaire' => [self::BELONGS_TO, 'Partenaire', 'partenaireId'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'partenaireId' => 'Partenaire',
			'login' => 'Login',
			'email' => 'E-mail',
			'nom' => 'Nom',
			'prenom' => 'Prénom',
			'nomComplet' => 'Nom complet',
			'actif' => 'Actif',
			'derConnexion' => 'Dernière connexion',
			'hdateCreation' => 'Date de création',
			'hdateModif' => 'Modifié',
			'permAdmin' => 'Administrateur',
			'permImport' => 'Permission d\'import',
			'permPartenaire' => 'Permission sur son partenaire',
			'permIndexation' => 'Permission d\'indexation',
			'permRedaction' => 'Permission éditoriale globale',
			'suiviEditeurs' => 'Suivi des éditeurs',
			'suiviNonSuivi' => 'Suivi des objets non suivis',
			'suiviPartenairesEditeurs' => "Suivi des partenaires-éditeurs",
			'motdepasse' => 'Mot de passe',
			'passwordDuplicate' => 'Mot de passe (bis)',
			'authLdap' => 'Authentification par LDAP',
			'listeDiffusion' => "Liste de diffusion",
		];
	}

	/**
	 * Validates the password (with LDAP). In debug mode, password=nigol.
	 *
	 * @param string $password
	 * @return bool Success?
	 */
	public function validatePassword(string $password): bool
	{
		if (!$this->actif) {
			Yii::log("Login : essai de connexion de l'utilisateur inactif : {$this->login}", "info");
			return false;
		}
		if ($this->partenaire->statut == 'inactif') {
			Yii::log("Login : essai de connexion pour un partenaire inactif : {$this->login}", "info");
			return false;
		}
		if (Yii::app()->params['reversePasswords'] && strrev($password) === $this->login) {
			return true;
		}
		if (!$this->authLdap) {
			return $this->validateLocalPassword($password);
		}
		if (!empty(Yii::app()->params->itemAt('ldap'))) {
			$ldapconn = @ldap_connect(Yii::app()->params->itemAt('ldap')['server']);
			if (!$ldapconn) {
				Yii::log("Could not connect to LDAP server.", 'error');
				return false;
			}
			$queries = Yii::app()->params->itemAt('ldap')['queries'];
			foreach ($queries as $query) {
				$query = sprintf($query, $this->login);
				if (@ldap_bind($ldapconn, $query, $password)) {
					$query = preg_replace('/uid=\w+\s*,/', '', $query);
					$info = ldap_get_entries($ldapconn, ldap_list($ldapconn, $query, "uid={$this->login}"));
					@ldap_unbind($ldapconn);
					$this->updateFromLdap($info);
					return true;
				}
			}
		} else {
			Yii::log("Pas de serveur LDAP défini, authentification impossible.", 'error');
		}
		return false;
	}

	/**
	 * Returns true if this user identifies with a password from the SQL.
	 *
	 * @param string $password
	 * @return bool
	 */
	public function validateLocalPassword(string $password): bool
	{
		if ($this->motdepasse === '') {
			return false;
		}
		$hasher = new PasswordHash(8, false);
		return $hasher->CheckPassword($password, $this->motdepasse);
	}

	/**
	 * Find a record by login or email.
	 *
	 * @param string $login
	 * @return Utilisateur|null
	 */
	public static function findByName(string $login): ?Utilisateur
	{
		return self::model()->find(
			"actif = 1 AND login = :l OR email = :e",
			[':l' => $login, ':e' => $login]
		);
	}

	/**
	 * Generate a new random password of 8 characters.
	 *
	 * @return string
	 */
	public static function generateRandomPassword(): string
	{
		$from = [
			["azertyuiopqsdfghjkmwxcvbnAZERTYUOPQSDFGHJKMWXCVBN", 5],
			["0123456789", 2],
			[",;:!?./%&()=+-", 1],
		];
		$password = '';
		foreach ($from as $source) {
			$password .= substr(str_shuffle($source[0]), 0, $source[1]);
		}
		return str_shuffle($password);
	}

	public function getSelfLink(bool $fullname = true): string
	{
		if ($this->id) {
			return CHtml::link(
				CHtml::encode($fullname ? $this->nomComplet : $this->login),
				['utilisateur/view', 'id' => $this->id],
				['title' => $this->partenaire->nom]
			);
		}
		return CHtml::encode($fullname ? $this->nomComplet : $this->login);
	}

	/**
	 * Return HTML suitable for flash messages in the session.
	 */
	public function getLoginMessages(): string
	{
		$messages = [];
		$collections = Collection::model()
			->with(['ressource'])
			->findAllBySql(
				"SELECT c.* FROM Abonnement a JOIN Collection c ON c.id = a.collectionId"
				. " WHERE c.type = :type AND a.partenaireId = " . (int) $this->partenaireId,
				[':type' => Collection::TYPE_TEMPORAIRE]
			);
		if ($collections) {
			$message = "<p>Vous êtes abonné à des collections temporaires qui ont vocation à disparaître prochainement. "
				. "Il faut probablement supprimer ces reliquats et vous abonner à d'autres collections de ces ressource. "
				. "<ul>";
			foreach ($collections as $c) {
				/* @var $c Collection */
				$message .= "<li>Ressource " . $c->ressource->getSelfLink() . " : " . $c->nom . "</li>\n";
			}
			$message .= "</ul>\n";
			$messages[] = $message;
		}
		return join("<br /><br />", $messages);
	}

	/**
	 * Called automatically before save().
	 *
	 * @return bool
	 */
	protected function beforeSave()
	{
		if ($this->getIsNewRecord()) {
			$this->hdateCreation = $_SERVER['REQUEST_TIME'];
		}
		if (empty($this->nomComplet)) {
			$this->nomComplet = $this->prenom . " " . $this->nom;
		}
		if (empty($this->suiviPartenairesEditeurs)) {
			$this->suiviPartenairesEditeurs = 0;
		}
		$this->hdateModif = $_SERVER['REQUEST_TIME'];
		// Replace the raw password with the hashed one
		if ($this->motdepasse) {
			$hasher = new PasswordHash(8, false);
			$this->motdepasse = $hasher->HashPassword($this->motdepasse);
		} elseif ($this->id) {
			// do not clear the password of an existing user
			unset($this->motdepasse);
		}
		$this->nom = Tools::normalizeText($this->nom);
		$this->prenom = Tools::normalizeText($this->prenom);
		$this->nomComplet = Tools::normalizeText($this->nomComplet);
		return parent::beforeSave();
	}

	/**
	 * Updates and saves the attributes whose values have changed in the LDAP.
	 *
	 * @param array $info Result of ldap_info().
	 */
	protected function updateFromLdap($info)
	{
		$attrNames = [
			'mail' => 'email',
			'sn' => 'nom',
			'givenname' => 'prenom',
			'cn' => 'nomComplet',
		];
		$values = [];
		foreach ($attrNames as $k => $v) {
			if (!empty($info[0][$k][0]) && $info[0][$k][0] != $this->{$v}) {
				$values[$v] = $info[0][$k][0];
			}
		}
		if ($values) {
			$this->saveAttributes($values);
		}
	}
}
