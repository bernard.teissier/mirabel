<?php

namespace models\sphinx;

/**
 * @property int $id Editeur.id
 * @property int $cletri
 * @property int $lettre1
 * @property int $nbrevues
 * @property int $nbtitresmorts
 * @property int $nbtitresvivants
 * @property string $nomcomplet
 * @property int $paysid
 * @property int[] $suivi array of Partenaire.id
 * @property int hdatemodif timestamp
 * @property int hdateverif timestamp
 */
class Editeurs extends \CActiveRecord
{
	/**
	 * @var \Editeur
	 */
	private $record;

	public function getDbConnection()
	{
		return \Yii::app()->getComponent('sphinx');
	}

	public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{editeurs}}';
    }

	public function getRecord(): ?\Editeur
	{
		if ($this->record === null && $this->id > 0) {
			$this->record = \Editeur::model()->findByPk($this->id);
		}
		return $this->record;
	}

	public function setRecord(\Editeur $editeur): void
	{
		$this->record = $editeur;
	}

	public function getSuiviIds(): array
	{
		if (empty($this->suivi)) {
			return [];
		}
		return explode(',', $this->suivi);
	}
}
