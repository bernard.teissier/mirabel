<?php

namespace models\sherpa;

/**
 * Store a raw item from the response of the Sherpa API.
 *
 * This should match each entry of the JSON returned by the API,
 * e.g. curl "https://v2.sherpa.ac.uk/cgi/retrieve?api-key=$KEY&format=Json&item-type=publication&filter=%5B%5B%22issn%22%2C%22equals%22%2C%22$ISSN%22%5D%5D"
 * (where $KEY and $ISSN must be set).
 * Mirabel will then use this to fill in a sherpa\Publication instance.
 *
 * Cf https://v2.sherpa.ac.uk/api/metadata-schema.html with the blocks
 * "common to all types" and "publication".
 */
class Item
{
	/**
	 * @var int the internal id of the item
	 */
	public $id;

	/**
	 * @var array
	 */
	public $issns;

	/**
	 * @var string yes|no
	 */
	public $listed_in_doaj;

	/**
	 * @var array
	 */
	public $listed_in_doaj_phrases;

	/**
	 * @var string
	 */
	public $notes;

	/**
	 * @var array
	 */
	public $publisher_policy;

	/**
	 * @var array
	 */
	public $publishers;

	/**
	 * @var object {id: 4, publicly_visible: "yes", uri: "http...", publicly_visible_phrases: [...]}
	 */
	public $system_metadata;

	/**
	 * @var array
	 */
	public $title;

	/**
	 * @var string
	 */
	public $type;

	/**
	 * @var array
	 */
	public $type_phrases;

	/**
	 * @var string
	 */
	public $url;

	public function __construct(\stdClass $raw)
	{
		foreach ((array) $raw as $k => $v) {
			if (!property_exists(self::class, $k)) {
				\Yii::log("SherpaPublication, attribute '$k' is unknown.", \CLogger::LEVEL_WARNING);
			}
			$this->{$k} = $v;
		}
	}

	public static function loadFromCache(int $titreId): ?Item
	{
		$logPath = \Yii::getPathOfAlias('application.runtime.sherpa');
		$filePath = sprintf("%s/%d.json", $logPath, $titreId);
		if (!is_file($filePath)) {
			return null;
		}
		/**
		 * @todo add a cache
		 */
		return self::loadFromFile($filePath);
	}

	/**
	 * @param int $titreId
	 * @param string[] $issns
	 * @return Item|null
	 */
	public static function loadFromCacheByIssn(int $titreId, array $issns): ?Item
	{
		$logPath = \Yii::getPathOfAlias('application.runtime.sherpa');
		foreach ($issns as $issn) {
			$filePath = sprintf("%s/%s.json", $logPath, $issn);
			if (is_file($filePath)) {
				$newFilePath = sprintf("%s/%d.json", $logPath, $titreId);
				rename($filePath, $newFilePath);
				return self::loadFromFile($newFilePath);
			}
		}
		return null;
	}

	public static function loadFromFile(string $path): ?Item
	{
		$contents = file_get_contents($path);
		if (!$contents) {
			return null;
		}
		$data = json_decode($contents);
		if (!$data || empty($data->items)) {
			return null;
		}
		return new self($data->items[0]);
	}

	public function getRomeoUrl(): string
	{
		if ($this->system_metadata->publicly_visible !== 'yes' || empty($this->system_metadata->uri)) {
			return "";
		}
		return $this->system_metadata->uri;
	}

	public function getPoliciesSummary(): array
	{
		$policies = [
			"submitted" => 0,
			"accepted" => 0,
			"published" => 0,
		];
		if (empty($this->publisher_policy)) {
			return [];
		}
		foreach ($this->publisher_policy as $pp) {
			if (empty($pp->permitted_oa)) {
				continue;
			}
			foreach ($pp->permitted_oa as $oa) {
				if (empty($oa->article_version)) {
					continue;
				}
				foreach ($oa->article_version as $p) {
					$policies[$p]++;
				}
			}
		}
		return $policies;
	}

	public function getPublication(): Publication
	{
		$p = new Publication();
		$p->fill($this);
		return $p;
	}
}
