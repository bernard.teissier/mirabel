<?php

namespace models\sherpa;

use Titre;
use Yii;

/**
 * Integration of Sherpa into Mirabel's process.
 */
class Hook
{
	/**
	 * Add a Sherpa-Romeo link to a Titre, if it does not exist yet.
	 *
	 * @param Titre $titre
	 * @param array $issnsChanges
	 * @return bool Link added?
	 */
	public static function addSherpaLink(Titre $titre, array $issnsChanges): bool
	{
		if (!self::isSherpaApiEnabled()) {
			return false;
		}

		// List the ISSN numbers to be inserted or updated.
		$issns = [];
		if (isset($issnsChanges['insert'])) {
			foreach ($issnsChanges['insert'] as $x) {
				if (!empty($x['after']->issn)) {
					$issns[] = $x['after']->issn;
				}
			}
		}
		if (isset($issnsChanges['update'])) {
			foreach ($issnsChanges['update'] as $x) {
				if ($x['after']->issn && $x['after']->issn !== $x['before']->issn) {
					$issns[] = $x['after']->issn;
				}
			}
		}

		return self::updateTitle($titre, $issns);
	}

	public static function enrichIntervention(\Intervention $interv): bool
	{
		if (!self::isSherpaApiEnabled()) {
			return false;
		}

		$isTitleCreation = false;
		$titleAttributes = [];
		$issns = [];
		foreach ($interv->contenuJson->toArray() as $op) {
			if ($op['operation'] !== 'create') {
				continue;
			}
			if ($op['model'] === 'Titre') {
				$isTitleCreation = true;
				$titleAttributes = $op['after'];
			} elseif ($op['model'] === 'Issn' && !empty($op['after']['issn'])) {
				$issns[] = $op['after']['issn'];
			}
		}
		if (!$isTitleCreation || empty($issns)) {
			return false;
		}

		$titre = new Titre();
		$titre->setAttributes($titleAttributes, false);
		if (!self::updateTitle($titre, $issns)) {
			return false;
		}
		$interv->contenuJson->modifyDetail('Titre', ['liensJson' => [null, json_encode($titre->getLiens())]]);

		return true;
	}

	private static function isSherpaApiEnabled(): bool
	{
		if (defined('YII_TEST') && YII_TEST) {
			// automatic test, so do not use the network
			return false;
		}
		if (!Yii::app()->params->contains('sherpa') || empty(Yii::app()->params->itemAt('sherpa')['api-key'])) {
			// We cannot send requests to the Sherpa API.
			return false;
		}
		return true;
	}

	private static function updateTitle(Titre $titre, array $issns): bool
	{
		if (!$issns) {
			// Without ISSN numbers, we cannot send requests to the Sherpa API.
			return false;
		}

		$source = \Sourcelien::model()->findByAttributes(['nomcourt' => "romeo"]);
		if ($source === null) {
			return false;
		}

		// Stop if a link to Sherpa-Romeo is already there.
		if ($titre->getLiens()->containsSource((int) $source->id)) {
			return false;
		}

		// query Sherpa
		$apiKey = Yii::app()->params->itemAt('sherpa')['api-key'];
		$api = new Api($apiKey);
		try {
			$publication = $api->fetchPublication((int) $titre->id, $issns);
		} catch (\Exception $e) {
			$textIssns = join(",", $issns);
			Yii::log("Erreur avec l'API sherpa : ID= {$titre->id}, ISSNS=$textIssns, ERR={$e->getMessage()}", \CLogger::LEVEL_WARNING);
		}
		if (empty($publication)) {
			return false;
		}
		if (!$publication->getRomeoUrl()) {
			return false;
		}

		// add the Sherpa link to the Titre record
		$link = new \Lien();
		$link->sourceId = (int) $source->id;
		$link->src = $source->nom;
		$link->url = $publication->getRomeoUrl();
		$links = $titre->getLiens();
		$links->add($link);
		$titre->setLiens($links);
		return true;
	}
}
