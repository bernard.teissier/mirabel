<?php

namespace models\sherpa;

/**
 * This class models the API of Sherpa for Object Retrieval.
 * See https://v2.sherpa.ac.uk/api/
 */
class Api
{
	public const ENDPOINT = 'https://v2.sherpa.ac.uk/cgi/retrieve';

	protected $curl;

	protected $apiKey = '';

	protected $logPath = '';

	public function __construct(string $apiKey)
	{
		if (!$apiKey) {
			throw new \Exception("Sherpa API needs a key. Please configure Mir@bel.");
		}
		$this->apiKey = $apiKey;

		$this->initCurl();
		$this->initLog();
	}

	public function __destruct()
	{
		curl_close($this->curl);
	}

	/**
	 * Fetch a Publication by the first matching ISSN.
	 *
	 * @param int $titreId Mirabel ID
	 * @param string[] $issns
	 * @return ?Item
	 */
	public function fetchPublication(int $titreId, array $issns): ?Item
	{
		foreach ($issns as $issn) {
			$json = $this->fetchByIssn($issn);
			$publications = json_decode($json); // into \stdClass
			if (!empty($publications->items)) {
				if ($titreId > 0) {
					$filename = "{$this->logPath}/$titreId.json";
				} else {
					$filename = "{$this->logPath}/$issn.json";
				}
				file_put_contents($filename, $json);
				@chmod($filename, 0664); // do nothing if we do not own the file
				return new Item($publications->items[0]);
			}
		}
		return null;
	}

	/**
	 * @param string $issn
	 * @return string JSON-encoded
	 */
	public function fetchByIssn(string $issn): string
	{
		$encIssn = rawurlencode($issn);
		$url = self::ENDPOINT . "?"
			. http_build_query([
				'api-key' => $this->apiKey,
				'item-type' => "publication",
				'format' => 'Json',
			])
			// http_build_query() encodes keys of numeric arrays, which will break anywhere outside of PHP servers.
			// So we have to hack the URL in order to add the filter.
			// 'filter' => [ ['issn', 'equals', $issn] ],
			. "&filter=%5B%5B%22issn%22%2C%22equals%22%2C%22$encIssn%22%5D%5D";
		return $this->fetch($url);
	}

	private function fetch(string $url): string
	{
		curl_setopt($this->curl, CURLOPT_URL, $url);
		$result = curl_exec($this->curl);
		if ($result === false) {
			throw new \Exception("Erreur de téléchargement : " . curl_error($this->curl));
		}
		assert(is_string($result));
		$code = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
		if ($code < 200 || $code >= 300) {
			throw new \Exception("Erreur HTTP, code $code.");
		}
		return $result;
	}

	private function initCurl(): void
	{
		$this->curl = curl_init();
		curl_setopt_array(
			$this->curl,
			[
				CURLOPT_FOLLOWLOCATION => true,    // follow redirection (301, etc)
				CURLOPT_MAXREDIRS => 1,
				CURLOPT_TIMEOUT => 3,              // (seconds)
				CURLOPT_COOKIEFILE => "",          // store cookies?
				CURLOPT_RETURNTRANSFER => true,
				// same connection for multiple queries?
				CURLOPT_FRESH_CONNECT => false,
				CURLOPT_FORBID_REUSE => false,
			]
		);
	}

	private function initLog(): void
	{
		$this->logPath = \Yii::getPathOfAlias('application.runtime.sherpa');
		if (!is_dir($this->logPath)) {
			if (!@mkdir($this->logPath, 0775)) {
				\Yii::log("Could not create directory '{$this->logPath}'", \CLogger::LEVEL_ERROR);
			}
		}
	}
}
