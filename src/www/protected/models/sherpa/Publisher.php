<?php

namespace models\sherpa;

/**
 * A simplified publisher initialized with the data from Sherpa.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class Publisher
{
	/**
	 * @var string[]
	 */
	public $name;

	/**
	 * @var string
	 */
	public $relationship;

	/**
	 * @var string
	 */
	public $url;

	/**
	 * @var string
	 */
	public $urlSherpa;

	public function __construct(\stdClass $data)
	{
		if (empty($data->publisher->name)) {
			return;
		}
		foreach ($data->publisher->name as $name) {
			if (isset($name->name)) {
				$this->name[] = $name->name;
			}
		}
		if (!empty($data->relationship_type)) {
			$this->relationship = $data->relationship_type;
		}
		if (!empty($data->publisher->url)) {
			$this->url = $data->publisher->url;
		}
		if (!empty($data->publisher->uri)) {
			$this->urlSherpa = $data->publisher->uri;
		}
	}

	public function getPrintableRelationship(): string
	{
		$converter = \Config::read('sherpa.fr.relations');
		return empty($converter[$this->relationship]) ? $this->relationship : $converter[$this->relationship];
	}
}
