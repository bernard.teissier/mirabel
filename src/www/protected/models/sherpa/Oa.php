<?php

namespace models\sherpa;

/**
 * This maps the field "permitted_oa".
 *
 * See publisher_policy.permitted_oa in https://v2.sherpa.ac.uk/api/metadata-schema.html
 */
class Oa
{
	/**
	 * @var bool
	 */
	public $additionalOaFee = false;

	/**
	 * @var string[]
	 */
	public $articleVersions = [];

	/**
	 * @var string[]
	 */
	public $conditions = [];

	/**
	 * @var string
	 */
	public $copyrightOwner = "";

	/**
	 * @var object { amount: 1, units: "years" }
	 */
	public $embargo;

	/**
	 * @var string[]
	 */
	public $licenses = [];

	/**
	 * @var	array
	 */
	public $locations = [];

	/**
	 * @var bool
	 */
	public $prerequisites = false;

	/**
	 * @var bool
	 */
	public $publicNotes = false;

	public function __construct(\stdClass $oa)
	{
		if (isset($oa->article_version)) {
			$this->articleVersions = $oa->article_version;
		}
		if (isset($oa->additional_oa_fee)) {
			$this->additionalOaFee = ($oa->additional_oa_fee === 'yes');
		}
		if (isset($oa->conditions)) {
			$this->conditions = $oa->conditions;
		}
		if (isset($oa->copyright_owner)) {
			$this->copyrightOwner = $oa->copyright_owner;
		}
		if (!empty($oa->embargo->amount)) {
			$this->embargo = $oa->embargo;
		}
		if (!empty($oa->prerequisites)) {
			$this->prerequisites = true;
		}
		if (!empty($oa->public_notes)) {
			$this->publicNotes = true;
		}

		// license
		$converter = \Config::read('sherpa.fr.license');
		if (isset($oa->license)) {
			foreach ($oa->license as $license) {
				if (isset($converter[$license->license])) {
					$this->licenses[$license->license] = $converter[$license->license];
				} else {
					foreach ($license->license_phrases as $phrase) {
						if ($phrase->language === 'en') {
							$this->licenses[$license->license] = $phrase->phrase;
							break;
						}
					}
				}
			}
		}

		if (!empty($oa->location->location)) {
			foreach ($oa->location->location as $l) {
				if (isset($oa->location->{$l})) {
					$this->locations[$l] = $oa->location->{$l};
				} else {
					$this->locations[$l] = $l;
				}
			}
		}
	}

	/**
	 * @return string[]
	 */
	public function getPrintableConditions(): array
	{
		$converter = \Config::read('sherpa.fr.conditions');
		$result = [];
		foreach ($this->conditions as $v) {
			$result[] = $converter[$v] ?? $v;
		}
		return $result;
	}

	public function getPrintableCopyrightOwner(): string
	{
		$converter = \Config::read('sherpa.fr.copyrightowner');
		$v = $this->copyrightOwner;
		return $converter[$v] ?? $v;
	}

	public function getPrintableEmbargo(): string
	{
		if (empty($this->embargo)) {
			return "Pas d'embargo";
		}
		if ($this->embargo->amount > 1) {
			$converter = [
				'days' => 'jours',
				'weeks' => 'semaines',
				'months' => 'mois',
				'years' => 'années',
			];
		} else {
			$converter = [
				'days' => 'jour',
				'weeks' => 'semaine',
				'months' => 'mois',
				'years' => 'année',
			];
		}
		if (isset($this->embargo->units)) {
			return sprintf("%d %s", $this->embargo->amount, $converter[$this->embargo->units]);
		}
	}

	/**
	 * @return string[]
	 */
	public function getPrintableLocations(string $titleUrl): array
	{
		$converter = \Config::read('sherpa.fr.locations');
		$result = [];
		foreach ($this->locations as $k => $v) {
			$name = $converter[$k] ?? $k;
			if ($k === "this_journal") {
				if ($titleUrl) {
					$name = \CHtml::link($name, $titleUrl);
				} else {
					continue;
				}
			}
			if (is_array($v)) {
				$result[] = "{$name} : " . join(" ; ", $v);
			} else {
				$result[] = $name;
			}
		}
		return $result;
	}

	public function getSummaryIcons(): string
	{
		$icons = array_filter([
			(in_array('this_journal', $this->locations)
				? '<span class="glyphicon glyphicon-eye-open" title="Publication en accès ouvert sur le site de l´éditeur"></span>'
				: ''),
			($this->additionalOaFee
				? '<span class="glyphicon glyphicon-euro" title="Avec frais additionnels de publication"></span>'
				: ''),
			(!empty($this->embargo->amount) && $this->embargo->amount > 0
				? '<span class="glyphicon glyphicon-hourglass" title="Embargo de ' . \CHtml::encode($this->getPrintableEmbargo()) . '"></span>'
				: ''),
			($this->licenses
				? '<span class="cc-icon" title="Licence '
					. \CHtml::encode(strip_tags(join(", ", $this->licenses))) . '"><img src="/images/icons/cc.svg" width=21 height=21 /></span>'
				: ''),
			($this->locations
				? '<span class="glyphicon glyphicon-education" title="Emplacements autorisés: '
					. \CHtml::encode(join(' / ', $this->getPrintableLocations(''))) . '"></span>'
				: ''),
			($this->conditions
				? '<span class="glyphicon glyphicon-list" title="Sous conditions : '
					. \CHtml::encode(join(' + ', $this->getPrintableConditions())) . '"></span>'
				: ''),
			($this->prerequisites
				? '<span class="glyphicon glyphicon-check" title="Avec prérequis"></span>'
				: ''),
			($this->publicNotes
				? '<span class="glyphicon glyphicon-edit" title="Remarques additionnelles"></span>'
				: ''),
		]);
		if ($icons) {
			return '<span class="oa-summary">'
				. join(" ", $icons)
				. '</span>';
		}
		return "";
	}
}
