<?php

namespace models\sherpa;

/**
 * Selection of interesting data from the whole Sherpa response.
 *
 * Built from a raw Item which matches the API response.
 */
class Publication
{
	/**
	 * No open access at all.
	 */
	public const ACCESS_CLOSED = 0;

	/**
	 * All the accesses are open.
	 */
	public const ACCESS_OPEN = 1;

	/**
	 * Some accesses are open, others are not.
	 */
	public const ACCESS_MIXED = 2;

	/**
	 * @var string[]
	 */
	public $preferredTitle = [];

	/**
	 * @var string[]
	 */
	public $title = [];

	/**
	 * @var string[]
	 */
	public $acronym = [];

	/**
	 * @var null|string
	 */
	public $dateModified;

	/**
	 * @var Policy[] Each value maps an item directly under "permitted_oa"
	 */
	public $policy = [];

	/**
	 * @var Publisher[]
	 */
	public $publishers = [];

	/**
	 * @var string {"system_metadata" : {"uri" : XXX}}
	 */
	public $romeoUrl = "";

	/**
	 * @var string The journal URL
	 */
	public $url = "";

	public function fill(Item $item): void
	{
		// date
		if (!empty($item->system_metadata->date_modified)) {
			$this->dateModified = $item->system_metadata->date_modified;
		} elseif (!empty($item->system_metadata->date_created)) {
			$this->dateModified = $item->system_metadata->date_created;
		}

		// romeoUrl
		if ($item->system_metadata->publicly_visible === 'yes' && !empty($item->system_metadata->uri)) {
			$this->romeoUrl = $item->system_metadata->uri;
		}

		// title & ...
		if (isset($item->title)) {
			foreach ($item->title as $title) {
				if (!empty($title->title)) {
					$this->title[] = $title->title;
				}
				if (!empty($title->acronym)) {
					$this->acronym[] = $title->acronym;
				}
				if (!empty($title->preferred) && !empty($title->{$title->preferred})) {
					$preferred = $title->preferred;
					$this->preferredTitle[] = $title->{$preferred};
				} elseif (isset($title->title)) {
					$this->preferredTitle[] = $title->title;
				}
			}
		}

		// url
		if (!empty($item->url)) {
			$this->url = $item->url;
		}

		// policy
		if (isset($item->publisher_policy)) {
			foreach ($item->publisher_policy as $p) {
				$this->policy[] = new Policy($p);
			}
		}

		// publishers
		if (isset($item->publishers) && isset($item->publishers[0]->publisher->name)) {
			foreach ($item->publishers as $p) {
				$this->publishers[] = new Publisher($p);
			}
		}
	}

	/**
	 *
	 * @param string $version "published", etc
	 * @return Oa[]
	 */
	public function getOaByVersion(string $version): array
	{
		$oas = [];
		foreach ($this->policy as $p) {
			if ($p->oa[$version]) {
				$oas = array_merge($oas, $p->oa[$version]);
			}
		}
		return $oas;
	}

	/**
	 * Cf JSON publisher_policy.*.open_access_prohibited
	 *
	 * @return int Constant among self::ACCESS_*
	 */
	public function getOpenAccess(): int
	{
		// openAccess
		$accessOpen = 0;
		$accessCount = 0;
		foreach ($this->policy as $policy) {
			$accessCount++;
			if ($policy->openAccess) {
				$accessOpen++;
			}
		}
		if ($accessOpen == $accessCount) {
			return self::ACCESS_OPEN;
		}
		if ($accessOpen === 0) {
			return self::ACCESS_CLOSED;
		}
		return self::ACCESS_MIXED;
	}

	/**
	 * Return a assoc array of the resource URLs across all policies.
	 *
	 * @return array title => URL
	 */
	public function getResourceUrls(): array
	{
		$resources = [];
		foreach ($this->policy as $p) {
			foreach ($p->resourceUrls as $d => $u) {
				$resources[$d] = $u;
			}
		}
		return $resources;
	}
}
