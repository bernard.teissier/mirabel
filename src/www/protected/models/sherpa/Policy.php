<?php

namespace models\sherpa;

/**
 * See publisher_policy in https://v2.sherpa.ac.uk/api/metadata-schema.html
 */
class Policy
{
	/**
	 * @var Oa[][] { "published": [Oa1, ...], ... }
	 */
	public $oa = [
		'published' => [],
		'accepted' => [],
		'submitted' => [],
	];

	/**
	 * @var bool
	 */
	public $openAccess;

	/**
	 * @var string
	 */
	public $policyUrl = "";

	/**
	 * @var array "Ressources sur les politiques éditoriales" : { "urls" : [...]}
	 */
	public $resourceUrls = [];

	public function __construct(\stdClass $p)
	{
		$this->openAccess = ($p->open_access_prohibited === "no");
		$this->policyUrl = $p->uri;

		// resourceUrls
		if (!empty($p->urls)) {
			foreach ($p->urls as $x) {
				if (isset($x->description)) {
					$this->resourceUrls[$x->description] = $x->url;
				} else {
					$this->resourceUrls[$x->url] = $x->url;
				}
			}
		}

		// oa
		if (isset($p->permitted_oa)) {
			foreach ($p->permitted_oa as $poa) {
				$oa = new Oa($poa);
				foreach ($poa->article_version as $version) {
					$this->oa[$version][] = $oa;
				}
			}
		}
	}
}
