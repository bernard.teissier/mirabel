<?php

/**
 * This is the model class for table "Hint".
 *
 * The followings are the available columns in table 'Hint':
 * @property int $id
 * @property string $model
 * @property string $attribute
 * @property string $name
 * @property string $description
 * @property int $hdateModif timestamp
 */
class Hint extends CActiveRecord
{
	public static $enumModel = [
		'Service' => 'Accès en ligne',
		'Cms' => 'Blocs éditoriaux',
		'Collection' => 'Collection',
		'Editeur' => 'Éditeur',
		'Hint' => "Bulles d'aide",
		'Identification' => 'Identification',
		'ImportForm' => "Import (tout format)",
		'ImportKbartForm' => "Import KBART",
		'IndexationImportForm' => "Import thématique",
		'Issn' => "Issn",
		'Partenaire' => 'Partenaire',
		'Possession' => 'Possession',
		'PossessionImport' => 'Possession (import CSV)',
		'SearchTitre' => 'Recherche de revues',
		'Ressource' => 'Ressource',
		'Suivi' => 'Suivi',
		'Categorie' => 'Thématique',
		'Vocabulaire' => 'Thématique (vocabulaire)',
		'Titre' => 'Titre',
		'Utilisateur' => 'Utilisateur',
	];

	/**
	 * Imports external methods into this class.
	 */
	public function behaviors()
	{
		return [
			'timestampToDate' => [
				'class' => 'application.models.behaviors.TimestampToDate',
				'fieldsDateTime' => ['hdateModif'],
			],
		];
	}

	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @return Hint the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Hint';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['name, attribute', 'required'],
			['attribute, name', 'length', 'max' => 255],
			['description', 'length', 'max' => 4096],
			['model', 'validateModel', 'range' => array_keys(self::$enumModel)], // enum
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			['model, attribute, name, description, hdateModif', 'safe', 'on' => 'search'],
		];
	}

	public function validateModel($attribute): void
	{
		if (!isset(self::$enumModel[$this->{$attribute}])) {
			$exists = Yii::app()->db
				->createCommand("SELECT 1 FROM Hint WHERE model = :m LIMIT 1")
				->queryScalar([':m' => $this->{$attribute}]);
			if (!$exists) {
				$this->addError($attribute, "Modèle non valide");
			}
		}
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'model' => 'Objet',
			'attribute' => 'Attribut',
			'name' => 'Nom',
			'selection' => 'Sélection',
			'description' => 'Description',
			'hdateModif' => 'Dernière modification',
		];
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @param int|bool $pageSize Number of items for pagination (false to disable pagination).
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($pageSize=25)
	{
		$criteria = new CDbCriteria;

		$criteria->compare('model', $this->model, true);
		$criteria->compare('attribute', $this->attribute, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('hdateModif', $this->hdateModif);

		$sort = new CSort();
		$sort->attributes = [
			'model' => ['asc' => 'model ASC, attribute ASC', 'desc' => 'model DESC, attribute ASC'],
			'attribute',
			'name',
			'hdateModif',
		];
		$sort->defaultOrder = ['model' => CSort::SORT_ASC];

		return new CActiveDataProvider(
			$this,
			[
				'criteria' => $criteria,
				'pagination' => ['pageSize' => $pageSize],
				'sort' => $sort,
			]
		);
	}

	public function getMessage(string $modelName, string $attributeName): ?string
	{
		return Yii::app()->db
			->createCommand("SELECT description FROM Hint WHERE model = :m AND attribute = :a LIMIT 1")
			->queryScalar([':m' => $modelName, ':a' => $attributeName]);
	}

	/**
	 * Return an assoc array [attribute => description]
	 *
	 * @param string $modelName (Hint.model)
	 * @return array List: attribute => description.
	 */
	public function getMessages(string $modelName): array
	{
		$messages = [];
		foreach ($this->findAllByAttributes(['model' => $modelName]) as $message) {
			$messages[$message->attribute] = $message->description;
		}
		return $messages;
	}

	/**
	 * Called automatically before validate().
	 *
	 * @return bool
	 */
	protected function beforeValidate()
	{
		$this->hdateModif = $_SERVER['REQUEST_TIME'];
		return parent::beforeValidate();
	}

	/**
	 * Called automatically before Save().
	 *
	 * @return bool
	 */
	protected function beforeSave()
	{
		$this->description = Tools::normalizeText($this->description);
		return parent::beforeSave();
	}
}
