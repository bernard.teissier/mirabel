<?php

/**
 * This behavior transparently encodes/decodes JSON fields of the ActiveRecord
 * it is attached to.
 *
 * A Behavior (AKA trait or mixin) adds some functions to the class
 * that have it attached (called "the owner").
 *
 * Enable it by adding to the AR class:
 *
 * public function behaviors()
 * {
 * return array(
 * 'json' => array(
 * 'class' => 'application.models.behaviors.JsonFields',
 * 'fields' => array('aJson', 'bJson'),
 * ),
 * );
 * }
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class JsonFields extends CActiveRecordBehavior
{
	/** @var array lists the JSON fields */
	public $fields = [];

	protected $save = [];

	/**
	 * Called automatically before the validation of the owner's attributes.
	 *
	 * @param CModelEvent $event
	 */
	public function beforeValidate($event)
	{
		foreach ($this->fields as $field) {
			if (!empty($this->owner->{$field})
				and is_scalar($this->owner->{$field}) and !is_string($this->owner->{$field})) {
				$this->owner->addError($field, 'Should be an array or an object, or already serialized');
			}
		}
	}

	/**
	 * Called automatically before saving the owner's attributes to the DB.
	 *
	 * @param CModelEvent $event
	 */
	public function beforeSave($event)
	{
		foreach ($this->fields as $field) {
			$this->save[$field] = $this->owner->{$field};
			$this->serialize($field);
		}
	}

	/**
	 * Called automatically after saving the owner's attributes to the DB.
	 *
	 * @param CModelEvent $event
	 */
	public function afterSave($event)
	{
		foreach ($this->fields as $field) {
			if (isset($this->owner->{$field})) {
				$this->owner->{$field} = $this->save[$field];
				unset($this->save[$field]);
			} else {
				$this->unserialize($field);
			}
		}
	}

	/**
	 * Called automatically after reading the owner's attributes from the DB.
	 *
	 * @param CModelEvent $event
	 */
	public function afterFind($event)
	{
		foreach ($this->fields as $field) {
			$this->unserialize($field);
		}
	}

	/**
	 * Called automatically after reading the owner's validate().
	 *
	 * @param CModelEvent $event
	 */
	public function afterValidate($event)
	{
		foreach ($this->fields as $field) {
			if (!$this->owner->hasErrors($field)) {
				$this->unserialize($field);
			}
		}
	}

	public function serialize($attribute)
	{
		if (isset($this->owner->{$attribute})) {
			$value = $this->owner->{$attribute};
			if (is_object($value) && method_exists($value, 'serialize')) {
				$this->owner->{$attribute} = $value->serialize();
			} elseif (!is_string($value)) {
				$this->owner->{$attribute} = json_encode($value);
			}
			// do not convert if the input is a string
		} else {
			$this->owner->{$attribute} = "";
		}
	}

	public function unserialize($attribute)
	{
		if (empty($this->owner->{$attribute})) {
			$this->owner->{$attribute} = null;
		} elseif (is_string($this->owner->{$attribute})) {
			// arrays & assoc arrays, no objects
			$value = json_decode($this->owner->{$attribute}, true);
			// unserialize object
			if (isset($value['class']) && isset($value['content'])) {
				$value = new $value['class']($value['content']);
			}
			$this->owner->{$attribute} = $value;
		}
	}
}
