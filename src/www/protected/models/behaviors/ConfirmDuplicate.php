<?php

/**
 * Add to behaviors():
 * 'confirmDuplicate' => array(
 * 'class' => 'application.models.behaviors.ConfirmDuplicate',
 * 'field' => 'nom',
 * ),
 *
 * Add to the properties:
 * public $confirm;
 * Add to the rules():
 * array('confirm', 'boolean'),
 */
class ConfirmDuplicate extends CModelBehavior
{
	public const PATTERN = "Avertissement : il s'agit peut-être d'un doublon";

	public $field;

	/**
	 * Called automatically before the validation of the owner's attributes.
	 *
	 * @param CModelEvent $event
	 */
	public function afterValidate($event)
	{
		if (empty($this->field)) {
			return;
		}
		if ($this->owner->scenario == 'reprise' || !Yii::app()->params->contains('sphinx')) {
			return;
		}
		$text = $this->owner->{$this->field};
		if (!$text || $this->owner->confirm) {
			return;
		}

		$owner =  $this->owner;
		$pk = (int) $this->owner->getPrimaryKey();
		if (!$this->owner->isNewRecord) {
			$oldField = $owner::model()
				->findByPk($pk)
				->{$this->field};
			if ($oldField === $this->owner->{$this->field}) {
				return; // existing record, unchanged field
			}
		}

		$dupl = array_filter(
			$owner::completeTerm($text),
			function ($x) use ($pk) {
				return empty($x['id']) || ((int) $x['id']) !== $pk;
			}
		);
		if ($dupl) {
			$list = '<ol>';
			foreach ($dupl as $d) {
				if ($d['id']) {
					$list .= '<li>' . htmlspecialchars($d['label']) . '</li>';
				} else {
					$list .= '<li>' . $d['label'] . '</li>';
				}
			}
			$list .= '</ol>';
			$this->owner->addError(
				$this->field,
				"Avertissement : il s'agit peut-être d'un doublon, veuillez vérifier avec les éléments comparés."
				. " S'il ne s'agit pas d'un doublon, il vous suffit de cocher la case de confirmation ci-bas. "
				. $list
			);
			$this->owner->confirm = false;
		}
	}
}
