<?php

/**
 * This behavior transparently converts integer timestamp fields to dates ;
 * for a 'tstamp' field, it will create a 'tstampFt' property.
 *
 * A Behavior (AKA trait or mixin) adds some functions to the class
 * that have it attached (called "the owner").
 *
 * Enable it by adding to the AR class:
 *
 * public function behaviors()
 * {
 * return array(
 * 'timestampToDate' => array(
 * 'class' => 'application.models.behaviors.TimestampToDate',
 * 'fieldsDateTime' => array('hdate1', 'hdate2'), // format as date-time
 * 'fieldsDate' => array('hdate3'),               // format as date
 * 'suffix' => 'Formatted',                       // default: 'Ft'
 * 'formatDate' => '%d/%m/%Y',
 * 'formatDateTime' => '%d/%m/%Y %H:%M',
 * ),
 * );
 * }
 *
 * This example will add 3 virtual properties into the owner class:
 *  - "hdate1Ft" and "hdate2Ft" as date-time texts,
 *  - "hdate3Ft" as date text.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class TimestampToDate extends CActiveRecordBehavior
{
	/** @var string Date format, cf http://php.net/manual/en/function.strftime.php */
	public $formatDate = '%d/%m/%Y';

	/** @var string Date format, cf http://php.net/manual/en/function.strftime.php */
	public $formatDateTime = '%d/%m/%Y à %H:%M';

	/** @var array lists the JSON fields */
	public $fieldsDate = [];

	/** @var array lists the JSON fields */
	public $fieldsDateTime = [];

	/** @var string Suffix that converts a timestamp field */
	public $suffix = 'Ft';

	protected $_fields = [];

	/**
	 * Magic getter that will return the value of the virtual attributes this behavior declares.
	 *
	 *  @param string $name Attribute/property name.
	 * @return string Date(-time) value.
	 */
	public function __get($name)
	{
		if (!isset($this->_fields[$name])) {
			return parent::__get($name);
		}
		$attr = substr($name, 0, -2);
		if (empty($this->owner->{$attr})) {
			return '';
		}
		return strftime($this->_fields[$name], $this->owner->{$attr});
	}

	/**
	 * Behavior "constructor", called when the behavior is "attached" to its owner.
	 *
	 * Initialize the list of the virtual attributes this behavior declares.
	 *
	 * @param object $owner
	 */
	public function attach($owner)
	{
		foreach ($this->fieldsDate as $field) {
			$this->_fields[$field . $this->suffix] = $this->formatDate;
		}
		foreach ($this->fieldsDateTime as $field) {
			$this->_fields[$field . $this->suffix] = $this->formatDateTime;
		}
		parent::attach($owner);
	}

	/**
	 * Declares new properties in the owner class.
	 *
	 * Inherited from CBehavior::canGetProperty().
	 *
	 * @param string $name Name of the property requested.
	 * @return bool True if the property is defined by this behavior.
	 */
	public function canGetProperty($name)
	{
		if (isset($this->_fields[$name])) {
			return true;
		}
		return false;
	}
}
