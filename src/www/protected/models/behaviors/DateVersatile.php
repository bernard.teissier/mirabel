<?php

/**
 * This behavior validates incomplete dates in ISO and european formats (YYYY-MM & MM/YYYY).
 * The field will be converted to ISO.
 *
 * Enable it by adding to the AR class:
 *
 * public function behaviors()
 * {
 * return array(
 * 'dateVersatile' => array(
 * 'class' => 'application.models.behaviors.DateVersatile',
 * 'fields' => array('dateStart', 'dateEnd'),
 * 'onlyFullDate' => false,      // rejects '2005-05'
 * 'nothingAfter' => false,      // rejects '2005-05 with a comment'
 * 'padding'      => false,      // converts '05/2005' into '2005-05-00'
 * ),
 * );
 * }
 *
 * To allow the fields to be assigned through $model->attributes, the rules MUST CONTAIN:
 *
 * array('dateStart, dateEnd', 'safe'),
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class DateVersatile extends CModelBehavior
{
	/** @var array lists the JSON fields */
	public $fields = [];

	public $onlyFullDate = false;

	public $nothingAfter = false;

	public $padding = false;

	public $modernDate = 1650;

	/**
	 * Called automatically before the validation of the owner's attributes.
	 *
	 * @param CModelEvent $event
	 */
	public function beforeValidate($event)
	{
		if (empty($this->fields)) {
			return;
		}
		foreach ($this->fields as $field) {
			if ($this->owner->{$field} === null || trim($this->owner->{$field}) === '') {
				continue;
			}
			$value = str_replace(
				['/', "\xFE\xFF", "\xEF\xBB\xBF"],
				['-', '', ''],
				trim($this->owner->{$field}) . ' '
			);
			// ISO
			if (preg_match('/^(\d{4})(-\d\d)?(-\d\d)?\s+(.*?)$/', $value, $match)) {
				list(, $year, $month, $day, $comment) = $match;
			} elseif (preg_match('/^(\d\d-)?(\d\d-)?(\d{4})\s+(.*?)$/', $value, $match)) {
				list(, $day, $month, $year, $comment) = $match;
				if (empty($month)) {
					$month = $day;
					$day = '';
				}
			} else {
				$this->owner->addError($field, "Format de date non valide (YYYY-mm-dd ou dd/mm/YYYY) dans '{$value}'.");
				continue;
			}
			$month = trim($month, '-');
			$day = trim($day, '-');
			if (!checkdate($month ?: 1, $day ?: 1, $year)) {
				$this->owner->addError($field, 'Date non valide mais de format correct.');
				continue;
			}
			if ($this->onlyFullDate and (!$day or !$month)) {
				$this->owner->addError($field, 'La date est incomplète (YYYY-mm-dd ou dd/mm/YYYY).');
				continue;
			}
			if ($this->modernDate && $year < $this->modernDate) {
				$this->owner->addError($field, "La date est trop ancienne, elle doit être postérieure à {$this->modernDate}.");
				continue;
			}
			if ($this->nothingAfter and $comment) {
				$this->owner->addError($field, 'La date est suivie de texte excédentaire.');
				continue;
			}
			if ($this->padding) {
				$this->owner->{$field} = trim(sprintf('%4d-%02d-%02d %s', $year, $month, $day, $comment));
			} else {
				if (empty($month)) {
					$this->owner->{$field} = trim(sprintf('%4d %s', $year, $comment));
				} elseif (empty($day)) {
					$this->owner->{$field} = trim(sprintf('%4d-%02d %s', $year, $month, $comment));
				} else {
					$this->owner->{$field} = trim(sprintf('%4d-%02d-%02d %s', $year, $month, $day, $comment));
				}
			}
		}
	}

	/**
	 * Returns a timestamp from a date attribute.
	 *
	 * @param string $attr
	 * @param bool $endDate If true, complete the date at the end of the period it covers.
	 * @return ?int Timestamp or null.
	 */
	public function convertAttrToTs(string $attr, bool $endDate): ?int
	{
		try {
			return self::convertdateToTs($this->owner->{$attr}, $endDate);
		} catch (Exception $_) {
			$this->owner->addError($attr, "Invalid date format.");
			return null;
		}
	}

	/**
	 * Converts a (possibly incomplete) date to a timestamp.
	 *
	 * @param string $datePart Date to convert.
	 * @param bool $endDate If true, complete the date at the end of the period it covers.
	 * @throws Exception if the date is invalid
	 * @return int Timestamp.
	 */
	public static function convertdateToTs($datePart, $endDate)
	{
		if (!$datePart) {
			return ($endDate ? $_SERVER['REQUEST_TIME'] : 0);
		}
		if (preg_match('#^(\d\d)/(\d\d)/(\d{4})\b#', $datePart, $m)) {
			$datePart = "{$m[3]}-{$m[2]}-{$m[1]}";
		}
		if (preg_match('#^(\d\d)/(\d{4})\b#', $datePart, $m)) {
			$datePart = "{$m[2]}-{$m[1]}";
		}
		if (!preg_match('/^(\d{4})(-\d\d)?(-\d\d)?/', $datePart, $m)) {
			throw new Exception('Invalid date format.');
		}
		if (empty($m[2])) {
			$m[2] = $endDate ? 12 : 1;
		} else {
			$m[2] = ltrim($m[2], '-');
		}
		if ($endDate) {
			// end date, hard work
			if (empty($m[3])) {
				if ($m[2] == 12) {
					return mktime(24, 0, 0, 12, 31, $m[1]);
				}
				return strtotime(
					"-1 day",
					strtotime(sprintf("%d-%02d-01 24:00:00", $m[1], $m[2]+1))
				);
			}
			$m[3] = ltrim($m[3], '-');
			return mktime(24, 0, 0, $m[2], $m[3], $m[1]);
		}
		// start date, easy
		if (empty($m[3])) {
			$m[3] = 1;
		} else {
			$m[3] = ltrim($m[3], '-');
		}
		return mktime(0, 0, 0, $m[2], $m[3], $m[1]);
	}

	/**
	 * Returns a SQL condition (to use with CDbCriteria) from a date attribute.
	 *
	 * Example:
	 * ("hdateModif", "= 2001") ---> "hdateModif BETWEEN TS<2001-01-01> AND TS<2001-12-32>"
	 *
	 * @param string $column
	 * @param string $attribute
	 * @return string|array SQL condition (or array() if no condition).
	 */
	public function buildTsConditionFromDateAttr($column, $attribute)
	{
		if (!isset($this->owner->{$attribute})) {
			return [];
		}
		return self::buildTsConditionFromDateValue($column, $this->owner->{$attribute});
	}

	/**
	 * Returns a SQL condition (to use with CDbCriteria) from a date input.
	 *
	 * Example:
	 * ("hdateModif", "= 2001") ---> "hdateModif BETWEEN TS<2001-01-01> AND TS<2001-12-32>"
	 *
	 * @param string $column
	 * @param string $date
	 * @return string|array SQL condition (or array() if no condition).
	 */
	public static function buildTsConditionFromDateValue(string $column, string $date)
	{
		if (empty($date)) {
			return [];
		}
		$operation = "=";
		if (preg_match('/^\s*(=|<|>|<=|>=|!=|<>)\s*(\d.+)$/', $date, $m)) {
			$operation = $m[1];
			$date = $m[2];
		}
		$date = trim($date, " \t <>=!");
		try {
			switch ($operation) {
				case '!=':
				case '<>':
					return "$column NOT BETWEEN " . self::convertdateToTs($date, false)
						. " AND " . self::convertdateToTs($date, true);
				case '<':
				case '<=':
					return "$column $operation " . self::convertdateToTs($date, $operation != '<');
				case '>':
				case '>=':
					return "$column $operation " . self::convertdateToTs($date, $operation == '>');
				default:
					return "$column BETWEEN " . self::convertdateToTs($date, false)
						. " AND " . self::convertdateToTs($date, true);
			}
		} catch (Exception $_) {
			Yii::log("Invalid date format '$date' for column '$column'.", 'warning');
			return '0';
		}
	}
}
