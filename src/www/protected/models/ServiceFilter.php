<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class ServiceFilter extends CModel
{
	public $acces;

	public $baconLibre;

	public $baconDoaj;

	public $baconGlobal;

	public $ressourceId;

	public $type;

	public $lacunaire;

	public $selection;

	public $hdateModif;

	public function attributeNames()
	{
		return ['acces', 'baconLibre', 'baconDoaj', 'baconGlobal', 'ressourceId', 'type', 'lacunaire', 'selection', 'hdateModif'];
	}

	public function rules()
	{
		return [
			['acces', 'in', 'range' => array_keys(Service::$enumAcces)],
			['type', 'safe'], // 'in', 'range' => array_keys(Service::$enumType)],
			['ressourceId', 'numerical', 'integerOnly' => true],
			['baconLibre, baconDoaj, baconGlobal, selection, lacunaire', 'boolean'],
			['hdateModif', 'date', 'format' => 'yyyy-MM-dd'],
		];
	}

	public function afterValidate()
	{
		// when searching, validate type as an array of types
		if (!empty($this->type)) {
			if (!is_array($this->type)) {
				$this->type = [$this->type];
			}
			foreach ($this->type as $type) {
				if (!isset(Service::$enumType[$type])) {
					$this->addError('type', "Une valeur sélectionnée est inconnue.");
				}
			}
		}
		return parent::afterValidate();
	}

	public function isBacon(): bool
	{
		return $this->baconLibre || $this->baconDoaj || $this->baconGlobal;
	}

	public function onUnsafeAttribute($name, $value)
	{
	}
}
