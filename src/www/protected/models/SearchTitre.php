<?php

/**
 * Searches Titre through Sphinx
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class SearchTitre extends CFormModel
{
	/**
	 * Where to search themes:
	 * - 'categorie' for the most precise search
	 * - 'revuecategorie' for always applying themes to the Revue level (manual theming, or if missing, themes imported on titles)
	 */
	public const SPHINX_THEME = 'revuecategorie';

	public const MONIT_IGNORE = "";

	public const MONIT_YES = "1";

	public const MONIT_NO = "2";

	public $pid;

	public $titre;

	public $langues;

	public $issn;

	public $hdateModif;

	public $hdateVerif;

	public $collectionId = 0;

	public $paysId;

	public $editeurId = [];

	public $ressourceId = [];

	public $categorie = [];

	public $cNonRec = [];

	public $categoriesEt = false;

	public $sanscategorie = false;

	public $suivi = [];

	public $detenu = [];

	public $vivant = null;

	public $acces = [];

	public $accesLibre = false;

	public $sansAcces = false;

	public $abonnement = false;

	public $lien = [];

	public $owned = false;

	public $monitoredByMe = false;

	public $monitored = self::MONIT_IGNORE;

	/**
	 * @var int Partenaire.id as context for owned and monitored filters
	 */
	private $partenaireId;

	private $attrExclude = [];

	private $filterAbonnement = [];

	private $ts = [];

	public function __construct($partenaireId = null)
	{
		if ($partenaireId > 0) {
			$this->partenaireId = (int) $partenaireId;
		}
		parent::__construct();
	}

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return [
			['titre, langues', 'length', 'max' => 255],
			["monitored", 'length', 'max' => 1],
			['issn', '\models\validators\IssnValidator', 'allowEmptyChecksum' => true, 'allowMissingCaret' => true],
			[
				['acces', 'categorie', 'cNonRec', 'editeurId', 'ressourceId', 'suivi', 'detenu'],
				'ext.validators.ArrayOfIntValidator',
			],
			[
				'lien',
				'ext.validators.ArrayOfIntValidator',
				'allowNegative' => true,
			],
			['owned, abonnement, monitoredByMe, accesLibre, sansAcces, categoriesEt, sanscategorie, vivant', 'boolean'],
			['collectionId, paysId, pid', 'numerical', 'integerOnly' => true],
			// converted to ISO dates by the behavior DateVersatile
			['hdateModif, hdateVerif', 'safe'],
		];
	}

	/**
	 * Called automatically before validate().
	 */
	public function beforeValidate()
	{
		if (Yii::app()->user->isGuest && !empty($_GET['hash'])) {
			//Yii::app()->user->institute = Yii::app()->db->createCommand("SELECT id FROM Partenaire WHERE hash = :h")
			//	->queryScalar(array(':h' => $_GET['hash']));
		}
		if ($this->pid > 0) {
			$this->partenaireId = (int) $this->pid;
		}

		if ($this->owned) {
			if ($this->partenaireId) {
				$this->detenu = [$this->partenaireId];
			} else {
				$this->addError('owned', "Vous n'avez pas d'établissement de rattachement.");
			}
		}
		if ($this->abonnement) {
			if ($this->partenaireId) {
				$this->filterAbonnement = [$this->partenaireId];
			} else {
				$this->addError('abonnement', "Vous n'avez pas d'établissement de rattachement.");
			}
		}
		if ($this->monitored === self::MONIT_NO) {
			$this->suivi = range(1, 255);
			$this->attrExclude['suivi'] = true;
		} elseif ($this->monitored === self::MONIT_YES) {
			$this->attrExclude['suivi'] = false;
			$this->suivi = range(1, 255);
		}
		if ($this->monitoredByMe) {
			if ($this->partenaireId) {
				$this->attrExclude['suivi'] = false;
				$this->suivi = [$this->partenaireId];
			} else {
				$this->addError('monitoredByMe', "Vous n'avez pas d'établissement de rattachement.");
			}
		}
		if ($this->sanscategorie) {
			$this->categorie = [];
		}
		return parent::beforeValidate();
	}

	/**
	 * Called automatically after validate().
	 */
	public function afterValidate()
	{
		$this->fillTimestamps();
		foreach (['Modif', 'Verif'] as $name) {
			if ($this->ts[$name] && $this->ts[$name][0] > $this->ts[$name][1]) {
				$this->addError("hdate{$name}", "Date de début après la date de fin");
			}
		}
		if (!$this->pid && $this->partenaireId && ($this->monitoredByMe || $this->owned || $this->abonnement)) {
			$this->pid = $this->partenaireId;
		}
		return parent::afterValidate();
	}

	public function exportAttributes()
	{
		$attr = [];
		// string
		foreach (['titre', 'langues', 'issn', 'hdateModif', 'hdateVerif'] as $name) {
			if ($this->{$name}) {
				$attr[$name] = $this->{$name};
			}
		}
		// num or bool
		foreach ([
			'abonnement', 'accesLibre', 'monitored', 'monitoredByMe', 'owned', 'sansAcces', 'categoriesEt', 'sanscategorie', 'vivant',
			'collectionId', 'paysId', ] as $name) {
			if ($this->{$name}) {
				$attr[$name] = (int) $this->{$name};
			}
		}
		// int[]
		foreach (['acces', 'categorie', 'cNonRec', 'editeurId', 'ressourceId', 'suivi', 'detenu', 'lien'] as $name) {
			if ($this->{$name}) {
				$attr[$name] = join(',', $this->{$name});
			}
		}
		if ($this->owned) {
			unset($attr['detenu']);
		}
		if ($this->monitored || $this->monitoredByMe) {
			unset($attr['suivi']);
		}
		// partenaireId only if used
		if ($this->monitoredByMe || $this->owned || $this->abonnement) {
			$attr['pid'] = $this->pid;
		}
		return $attr;
	}

	public function getSuiviText(): string
	{
		if (!$this->suivi) {
			return "";
		}
		$names = \Yii::app()->db
			->createCommand("SELECT nom FROM Partenaire WHERE id IN (" . join(',', array_map('intval', $this->suivi)) .  ")")
			->queryColumn();
		return join(", ", $names);
	}

	/**
	 * Returns a short description of the search criteria.
	 *
	 * @return string
	 */
	public function getTitleSuffix()
	{
		$suffixes = [];
		$sources = ['ressource' => 'ressource', 'éditeur' => 'editeur'];
		foreach ($sources as $print => $name) {
			$field = $name . "Id";
			if ($this->{$field}) {
				if (!is_array($this->{$field})) {
					$ids = (int) $this->{$field};
				} else {
					$ids = join(',', array_map('intval', $this->{$field}));
				}
				$sql = "SELECT IF(sigle!='',sigle,nom)"
					. " FROM " . ucfirst($name) . " WHERE id IN ($ids) ORDER BY nom";
				$names = Yii::app()->db->createCommand($sql)->queryColumn();
				if (!empty($names)) {
					if (count($this->{$field}) == 1) {
						$suffixes[] = $print . ' ' . CHtml::encode($names[0]);
					} else {
						$suffixes[] = $print . 's ' . CHtml::encode(join(', ', $names));
					}
				}
			}
		}
		if ($this->collectionId) {
			$suffixes[] = Collection::model()
				->findByPk($this->collectionId, ['with' => ['ressource']])
				->getFullName();
		}
		if ($suffixes) {
			return ' : ' . join(' / ', $suffixes);
		}
		return '';
	}

	/**
	 * Converts an ISSN 1234-5678 into a number 1234567.
	 *
	 * @param string $issn
	 * @return int
	 */
	public static function issnToInt(string $issn): int
	{
		$int = substr(str_replace('-', '', $issn), 0, 7);
		return (ctype_digit($int) ? (int) $int : 0);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return [
			'titre' => 'Titre',
			'issn' => 'ISSN',
			'hdateModif' => 'Date de modification',
			'hdateVerif' => 'Date de vérification',
			'editeurId' => 'Éditeur',
			'collectionId' => 'Collection',
			'paysId' => "Pays de l'éditeur",
			'suivi' => 'Suivi par',
			'detenu' => 'Détenu par',
			'ressourceId' => 'Ressource',
			'acces' => 'Accès en ligne',
			'accesLibre' => 'Les accès libres',
			'sansAcces' => 'Sans aucun accès en ligne',
			'owned' => 'Les revues disponibles dans mon établissement',
			'abonnement' => "Mon établissement est abonné en ligne",
			'monitoredByMe' => 'Seulement les revues que mon établissement suit',
			'monitored' => 'Revues suivies…',
			'categorie' => 'Thématique',
			'sanscategorie' => 'Sans thématique',
		];
	}

	/**
	 * @return Categorie[]
	 */
	public function getCategories(): array
	{
		if ($this->categorie) {
			return Tools::sqlToPairsObject(
				"SELECT * FROM Categorie WHERE id in (" . join(",", $this->categorie) . ")",
				"Categorie"
			);
		}
		return [];
	}

	/**
	 * Return an HTML string.
	 *
	 * @return string
	 */
	public function htmlSummary(): string
	{
		$html = '';
		$criteria = $this->arraySummary();
		if (empty($criteria)) {
			return "aucun filtre.";
		}
		foreach ($criteria as $k => $v) {
			if (is_integer($k)) {
				$html .= '[<em>' . CHtml::encode($v) . '</em>] ';
			} else {
				$html .= '[' . CHtml::encode($k) . ' : <em>' . CHtml::encode($v) . '</em>] ';
			}
		}
		return $html;
	}

	public function search($pageSize = 25): SphinxDataProvider
	{
		Yii::import('ext.Sphinx.*');
		$sphinxClient = new Sphinx(Yii::app()->params->itemAt('sphinx'));
		$sphinxClient->indexes = 'titres';
		$sphinxClient->SetGroupBy('revueid', SPH_GROUPBY_ATTR, 'cletri ASC');
		$sphinxClient->SetSortMode(SPH_SORT_ATTR_ASC, 'obsolete');

		if ($this->vivant) {
			$sphinxClient->SetFilter('vivant', [1]);
		}
		if ($this->titre) {
			$sphinxClient->query = $sphinxClient->EscapeString(preg_replace('/([A-Z])\.\s?(?=[A-Z]\b)/', '$1', $this->titre));
		}

		if (!empty($this->issn)) {
			$sphinxClient->SetFilter('issn', [self::issnToInt($this->issn)]);
		}

		if ($this->filterAbonnement && $this->accesLibre) {
			// abo OR libre
			$sphinxClient->SetSelect(sprintf(
				"*, IF(IN(abonnement, %s) OR IN (acceslibre, %s), 1, 0) AS abolibre",
				join(",", $this->filterAbonnement),
				join(",", empty($this->acces) ? [1, 2, 3, 4, 5] : $this->acces)
			));
			$sphinxClient->SetFilter('abolibre', [1]);
		} else {
			if ($this->filterAbonnement) {
				$sphinxClient->SetFilter('abonnement', $this->filterAbonnement);
			}
			if (!empty($this->acces)) {
				if (empty($this->accesLibre)) {
					$sphinxClient->SetFilter('acces', $this->acces);
				} else {
					$sphinxClient->SetFilter('acceslibre', $this->acces);
				}
			} elseif (!empty($this->accesLibre)) {
				// require whatever free access
				$sphinxClient->SetFilter('acceslibre', [1, 2, 3, 4, 5]);
			}
		}

		if ($this->sansAcces) {
			$sphinxClient->SetFilter('nbacces', [0]);
		}

		if (!empty($this->collectionId)) {
			$sphinxClient->SetFilter('collectionid', [(int) $this->collectionId]);
		}
		if (!empty($this->paysId)) {
			$sphinxClient->SetFilter('paysid', [(int) $this->paysId]);
		}

		if (!empty($this->categorie)) {
			$categories = $this->getCategories();
			if ($this->categoriesEt) {
				foreach ($this->categorie as $cid) {
					if (in_array($cid, $this->cNonRec) || $categories[$cid]->profondeur == 3) {
						$sphinxClient->SetFilter(self::SPHINX_THEME, [$cid]);
					} else {
						$subids = $categories[$cid]->getDescendantsIds();
						$subids[] = $cid;
						$sphinxClient->SetFilter(self::SPHINX_THEME, $subids);
					}
				}
			} else {
				$rec = [];
				$nonRecIds = [];
				foreach ($this->categorie as $cid) {
					$nonRecIds[] = $cid;
					if (!in_array($cid, $this->cNonRec)) {
						$rec[] = $categories[$cid];
					}
				}
				$subids = array_merge($nonRecIds, Categorie::listDescendantsIds($rec));
				$sphinxClient->SetFilter(self::SPHINX_THEME, $subids);
			}
		} elseif ($this->sanscategorie) {
			$sphinxClient->SetFilterRange('revuecategorie', 0, 4294967295, true);
		}

		foreach (['editeurId', 'ressourceId', 'suivi', 'detenu'] as $attr) {
			if (!empty($this->{$attr})) {
				$sphinxClient->SetFilter(strtolower($attr), $this->{$attr}, !empty($this->attrExclude[$attr]));
			}
		}

		if ($this->lien) {
			$lienProhibited = [];
			foreach ($this->lien as $l) {
				if ($l < 0) {
					$lienProhibited[] = -1 * (int) $l;
				} elseif ($l > 0) {
					// Add a AND filter of this sourceId
					$sphinxClient->SetFilter('lien', [(int) $l], false);
				}
			}
			if ($lienProhibited) {
				$sphinxClient->SetFilter('lien', $lienProhibited, true);
			}
		}

		$this->fillTimestamps();
		foreach (['Modif', 'Verif'] as $attr) {
			if ($this->ts[$attr]) {
				$sphinxClient->SetFilterRange(
					'hdate' . strtolower($attr),
					$this->ts[$attr][0],
					$this->ts[$attr][1]
				);
			}
		}

		if ($this->langues) {
			if ($this->langues === 'aucune') {
				$sphinxClient->SetFilter('revuesanslangue', [1]);
			} else {
				$lang = preg_replace('/\s*,\s*/', '|', trim($this->langues, ',  '));
				$sphinxClient->query .= " @langues ($lang)";
			}
		}

		return new SphinxDataProvider(
			$sphinxClient,
			null, // no ActiveRecord models
			[
				'pagination' => ['pageSize' => $pageSize],
			]
		);
	}

	/**
	 * Returns all the Revue.id that match a search.
	 *
	 * @return array
	 */
	public function searchTitreIds(): array
	{
		$datap = $this->search();
		$datap->pagination = false;
		if ($this->filterAbonnement && $this->accesLibre) {
			$datap->sphinxClient->SetSelect(sprintf(
				"revueid, IF(abonnement IN (%s) OR acceslibre IN (%s), 1, 0) AS abolibre",
				join(",", $this->filterAbonnement),
				join(",", empty($this->acces) ? [1, 2, 3, 4, 5] : $this->acces)
			));
		} else {
			$datap->sphinxClient->SetSelect('revueid');
		}
		$datap->sphinxClient->SetLimits(0, 10000, 10000);
		$ids = [];
		foreach ($datap->getData() as $data) {
			$ids[] = $data['attrs']['revueid'];
		}
		return $ids;
	}

	/**
	 * Return a list of the parameters used, ready to be formatted.
	 *
	 * @return array
	 */
	protected function arraySummary(): array
	{
		if ($this->partenaireId) {
			$instName = "“" . Partenaire::model()->findByPk($this->partenaireId)->getShortName() . "”";
		} else {
			$instName = "";
		}

		$sum = [];
		// simple text
		if ($this->titre) {
			$sum['Titre'] = $this->titre;
		}
		if ($this->langues) {
			$sum['Langues'] = ($this->langues === "aucune" ?
				"aucune"
				: \models\lang\Convert::codesToFullNames(trim($this->langues, ", ")));
		}
		if ($this->issn) {
			$sum['ISSN'] = $this->issn;
		}
		if ($this->hdateModif) {
			$sum["Modifié"] = $this->hdateModif;
		}
		if ($this->hdateVerif) {
			$sum["Vérifié"] = $this->hdateVerif;
		}
		// boolean
		if ($this->vivant) {
			$sum[] = "Revue vivante";
		}
		if ($this->abonnement && $this->accesLibre) {
			$sum[] = "Accès libre ou abonné";
		} else {
			if ($this->abonnement) {
				$sum[] = ucfirst($instName) . " est abonné en ligne";
			}
			if ($this->accesLibre) {
				$sum[] = "Accès libre";
			}
		}
		if ($this->sansAcces) {
			$sum[] = "Sans accès";
		}
		if ($this->owned) {
			$sum[] = "De $instName";
		}
		if ($this->monitoredByMe) {
			$sum[] = "Suivi par $instName";
		}
		if ($this->monitored === self::MONIT_YES) {
			$sum[] = "Suivi";
		} elseif ($this->monitored === self::MONIT_NO) {
			$sum[] = "Non suivi";
		}
		// single ID
		if ($this->collectionId) {
			$sum['Collection'] = Collection::model()->findByPk($this->collectionId)->nom;
		}
		if ($this->paysId) {
			$sum['Pays'] = Pays::model()->findByPk($this->paysId)->nom;
		}
		// liste of IDs
		$getName = function ($ar) {
			return $ar->nom;
		};
		if ($this->editeurId) {
			$sum['Éditeur'] = join(" ; ", array_map($getName, Editeur::model()->findAllByPk($this->editeurId)));
		}
		if ($this->ressourceId) {
			$sum['Ressource'] = join(" ; ", array_map($getName, Ressource::model()->findAllByPk($this->ressourceId)));
		}
		if ($this->suivi && $this->monitored === self::MONIT_IGNORE) {
			$sum['Suivi par'] = join(" ; ", array_map($getName, Partenaire::model()->findAllByPk($this->suivi)));
		}
		if ($this->detenu && !$this->owned) {
			$sum['Détenu par'] = join(" ; ", array_map($getName, Partenaire::model()->findAllByPk($this->detenu)));
		}
		if ($this->categorie) {
			$cNonRec = $this->cNonRec;
			$sum['Thématique'] = join(
				($this->categoriesEt ? " + " : " | "),
				array_map(
					function ($x) use ($cNonRec) {
						return $x->categorie . (in_array($x->id, $cNonRec) || $x->profondeur > 2 ? "" : " (récursif)");
					},
					$this->getCategories()
				)
			);
		}
		if ($this->sanscategorie) {
			$sum[] = "Sans thématique";
		}
		// special
		if ($this->acces) {
			$accesToText = function ($v) {
				$conv = ['', '', 'Texte intégral', 'Résumé', 'Sommaire', 'Indexation'];
				return $conv[$v];
			};
			$sum["Accès"] = join(" + ", array_map($accesToText, $this->acces));
		}
		if ($this->lien) {
			$lienRequired = [];
			$lienProhibited = [];
			foreach ($this->lien as $l) {
				if ($l < 0) {
					$lienProhibited[] = -(int) $l;
				} elseif ($l > 0) {
					$lienRequired[] = (int) $l;
				}
			}
			if ($lienRequired) {
				$sum[] = "Avec lien " . join(
					", ",
					Yii::app()->db
						->createCommand("SELECT nom FROM Sourcelien WHERE id IN (" . join(",", $lienRequired) . ")")
						->queryColumn()
				);
			}
			if ($lienProhibited) {
				$sum[] = "Sans lien " . join(
					", ",
					Yii::app()->db
						->createCommand("SELECT nom FROM Sourcelien WHERE id IN (" . join(",", $lienProhibited) . ")")
						->queryColumn()
				);
			}
		}
		return $sum;
	}

	protected function fillTimestamps(): void
	{
		if (empty($this->ts)) {
			$this->ts['Modif'] = Tools::parseDateInterval($this->hdateModif);
			$this->ts['Verif'] = Tools::parseDateInterval($this->hdateVerif);
		}
	}
}
