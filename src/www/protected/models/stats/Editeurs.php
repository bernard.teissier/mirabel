<?php

namespace models\stats;

use CHtml;
use Editeur;
use HtmlTable;
use Tools;
use Yii;

/**
 * Indicateurs sur les éditeurs
 */
class Editeurs extends \CComponent
{
	public static function countByCountry(int $max = 20): HtmlTable
	{
		$table = new HtmlTable();
		$table->header = [
			"Pays",
			["Nombre d'éditeurs", "data-tsorter" => "numeric", 'class' => 'ascend'],
			["Revues", "data-tsorter" => "numeric"],
		];
		$pays = Yii::app()->db->createCommand(
			"SELECT p.id, p.nom, count(DISTINCT e.id) AS numEd, count(DISTINCT revueId) AS numRev"
			. " FROM Pays p JOIN Editeur e ON e.paysId = p.id "
			. "    LEFT JOIN Titre_Editeur te ON te.editeurId = e.id LEFT JOIN Titre t ON t.id = te.titreId"
			. " GROUP BY p.id ORDER BY numEd DESC, p.nom ASC"
			. ($max ? " LIMIT " . (int) $max : "")
		)->queryAll();
		$table->data = [];
		if ($pays) {
			foreach ($pays as $p) {
				$table->data[] = [
					CHtml::link(CHtml::encode($p['nom']), ['/editeur/search', 'q' => ['pays' => $p['id']]]),
					$p['numEd'],
					$p['numRev'],
				];
			}
		}
		return $table;
	}

	public static function countByRevues(array $boundaries = [0, 1, 5, 10, 20, 50, 100]): HtmlTable
	{
		$case = "CASE ";
		foreach (array_map('intval', $boundaries) as $b) {
			$case .= " WHEN numRevues <= $b THEN '$b'";
		}
		$case .= " ELSE 'more' END";
		$revues = Tools::sqlToPairs(
			<<<EOSQL
			SELECT $case AS categorie, count(*) AS num
			FROM (
				SELECT e.id, count(DISTINCT t.revueID) AS numRevues
				FROM Editeur e
					LEFT JOIN Titre_Editeur te ON te.editeurId = e.id
					LEFT JOIN Titre t ON t.id = te.titreId
				GROUP BY e.id
				) ed
			GROUP BY categorie
			EOSQL
		);

		$table = new HtmlTable();
		$table->header = ["Nombre de revues", "Nombre d'éditeurs"];
		$table->data = [];
		$previous = null;
		$sum = 0;
		foreach ($boundaries as $b) {
			if (!isset($previous) || $b === $previous) {
				$categorie = $b;
			} else {
				$categorie = "$previous - $b";
			}
			if (isset($revues[$b])) {
				$table->data[] = [$categorie, $revues[$b]];
				$sum += $revues[$b];
			} else {
				$table->data[] = [$categorie, 0];
			}
			$previous = $b + 1;
		}
		if (isset($revues["more"])) {
			$table->data[] = [$previous . " +", $revues["more"]];
			$sum += $revues["more"];
		}
		$table->data[] = ["Total", $sum];
		return $table;
	}

	public static function getEditeursMaxRevues(int $limit = 30): HtmlTable
	{
		$table = new HtmlTable();
		$table->header = ["Éditeur", "Nombre de revues"];
		$editeurs = Yii::app()->db->createCommand(
			"SELECT e.*, count(DISTINCT t.revueID) AS numRevues"
			. " FROM Editeur e JOIN Titre_Editeur te ON te.editeurId = e.id JOIN Titre t ON t.id = te.titreId"
			. " GROUP BY e.id ORDER BY numRevues DESC, e.nom ASC"
			. ($limit ? " LIMIT " . (int) $limit : "")
		)->queryAll();
		$table->data = [];
		if ($editeurs) {
			foreach ($editeurs as $row) {
				$editeur = Editeur::model()->populateRecord($row);
				$table->data[] = [
					$editeur->getSelfLink(),
					$row['numRevues'],
				];
			}
		}
		return $table;
	}

	public static function count(): int
	{
		return (int) Yii::app()->db->createCommand(
			"SELECT count(*) FROM Editeur WHERE statut = 'normal'"
			)->queryScalar();
	}

	public static function countHavingIdref(): int
	{
		return (int) Yii::app()->db->createCommand(
			"SELECT count(*) FROM Editeur WHERE idref IS NOT NULL"
			)->queryScalar();
	}
}
