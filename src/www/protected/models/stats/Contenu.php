<?php

namespace models\stats;

use Cms;
use HtmlTable;
use Ressource;

class Contenu extends \CComponent
{
	/**
	 * @var \CDbConnection
	 */
	private $db;

	public function __construct(\CDbConnection $db)
	{
		$this->db = $db;
	}

	public function getMainInfoRevues(): \HtmlTable
	{
		return new HtmlTable(
			[
				[$this->db->createCommand("SELECT count(DISTINCT revueId) FROM Titre WHERE statut = 'normal'")->queryScalar()],
				[$this->db->createCommand("SELECT count(DISTINCT revueId) FROM Titre WHERE statut = 'normal' AND dateFin = ''")->queryScalar()],
				[$this->db->createCommand("SELECT count(DISTINCT s.cibleId) FROM Suivi s JOIN Revue r ON s.cibleId = r.id WHERE s.cible = 'Revue'")->queryScalar()],
				[$this->db->createCommand("SELECT count(DISTINCT t.revueId) FROM Service s JOIN Titre t ON s.titreId = t.id WHERE s.import > 0")->queryScalar()],
				[$this->db->createCommand("SELECT count(DISTINCT t.revueId)
FROM Service s JOIN Titre t ON s.titreId = t.id JOIN Ressource r ON s.ressourceId = r.id
    LEFT JOIN Service_Collection sc ON sc.serviceId = s.id LEFT JOIN Collection c ON sc.collectionId = c.id
WHERE s.import > 0 AND (r.autoImport > 0 OR c.importee > 0)")->queryScalar()],
				[$this->db->createCommand("SELECT count(*) FROM Titre WHERE statut = 'normal'")->queryScalar()],
			],
			null,
			["Revues", "dont revues vivantes", "Revues suivies", "Revues mises à jour par import", "Revues mises à jour par import automatique", "Titres"]
		);
	}

	public function getMainInfoRessources(): \HtmlTable
	{
		return new HtmlTable(
			[
				[$this->db->createCommand("SELECT count(*) FROM Ressource")->queryScalar()],
				[$this->db->createCommand("SELECT count(DISTINCT s.cibleId) FROM Suivi s JOIN Revue r ON s.cibleId = r.id WHERE s.cible = 'Ressource'")->queryScalar()],
				[$this->db->createCommand("SELECT count(DISTINCT s.ressourceId) FROM Service s WHERE s.import > 0")->queryScalar()],
				[$this->db->createCommand("SELECT count(*) FROM Ressource WHERE autoImport = 1")->queryScalar()],
				[$this->db->createCommand("SELECT count(*) FROM Collection")->queryScalar()],
				[$this->db->createCommand("SELECT count(*) FROM Collection WHERE importee = 1")->queryScalar()],
			],
			null,
			["Ressources", "Ressources suivies", "Ressources ayant des accès importés", "Ressources importées", "Collections", "Collections importées"]
		);
	}

	public function getMainInfo(): \HtmlTable
	{
		return new HtmlTable(
			[
				[$this->db->createCommand("SELECT count(*) FROM Partenaire WHERE type='normal' AND statut='actif'")->queryScalar()],
				[$this->db->createCommand("SELECT count(*) FROM Partenaire WHERE type='editeur' AND statut='actif'")->queryScalar()],
				[$this->db->createCommand("SELECT count(*) FROM Editeur")->queryScalar()],
				[$this->db->createCommand("SELECT count(*) FROM Service WHERE statut = 'normal'")->queryScalar()],
				[Cms::countLinks()],
				[$this->db->createCommand("SELECT count(*) FROM Utilisateur u JOIN Partenaire p ON p.id = u.partenaireId WHERE p.type='normal' AND p.statut IN ('actif', 'provisoire') AND u.actif = 1")->queryScalar()],
				[$this->db->createCommand("SELECT count(*) FROM Utilisateur u JOIN Partenaire p ON p.id = u.partenaireId WHERE p.type='editeur' AND p.statut IN ('actif', 'provisoire') AND u.actif = 1")->queryScalar()],
			],
			null,
			["Partenaires veilleurs", "+ Partenaires éditeurs", "Éditeurs", "Accès en ligne", "Liens extérieurs", "Veilleurs actifs", "+ Veilleurs-éditeurs actifs"]
		);
	}

	public function getRessourcesParType(): array
	{
		$sql = "SELECT type, count(*) AS 'Ressources' FROM Ressource "
			. "GROUP BY type ORDER BY count(*) DESC";
		$rows = $this->db->createCommand($sql)->queryAll();
		$res = [];
		foreach ($rows as $row) {
			$res[] = [
				'Type' => Ressource::$enumType[$row['type']],
				'Ressources' => $row['Ressources'],
			];
		}
		return $res;
	}

	public function getRessourcesImportees(): array
	{
		$sql = "SELECT nom, type FROM Ressource "
			. "WHERE autoImport = 1 ORDER BY nom";
		return $this->db->createCommand($sql)->queryAll();
	}

	public function getRessourcesParAcces($limit = 20): array
	{
		$sql = "SELECT r.nom, count(DISTINCT t.revueId) AS 'Revues', count(DISTINCT t.id) AS 'Titres', count(*) AS 'Accès en ligne' "
			. "FROM Ressource r JOIN Service s ON r.id=s.ressourceId JOIN Titre t ON t.id=s.titreId "
			. "GROUP BY r.id ORDER BY count(DISTINCT t.revueId) DESC, count(DISTINCT t.id) DESC, count(*) DESC LIMIT " . (int) $limit;
		return $this->db->createCommand($sql)->queryAll();
	}

	public function getAccesParType(): array
	{
		$sql = "SELECT IFNULL(`type`, 'Tout type') AS `type`, IFNULL(acces, 'Total') AS acces, count(*) AS num "
			. "FROM Service s "
			. "WHERE statut = 'normal' "
			. "GROUP BY `type`, acces "
			. "WITH ROLLUP";
		$query = $this->db->createCommand($sql)->queryAll(false);
		$result = [];
		$libre = 0;
		$restreint = 0;
		foreach ($query as $row) {
			$type = $row[0];
			if (empty($result[$type])) {
				$result[$type] = ['type' => $type];
			}
			$result[$type][$row[1]] = $row[2];
			if ($row[1] === 'Total' && $type !== 'Tout type') {
				$result[$type]['Taux'] = round(100.0 * $result[$type]['Libre'] / $result[$type]['Total']);
			}
			if ($row[1] === 'Libre') {
				$libre += $row[2];
			} elseif ($row[1] === 'Restreint') {
				$restreint += $row[2];
			}
		}
		$result['Tout type']['Libre'] = $libre;
		$result['Tout type']['Restreint'] = $restreint;
		$result['Tout type']['Taux'] = round(100.0 * $libre / ($libre + $restreint));
		return $result;
	}

	public function getRevuesParAcces(): array
	{
		$sql = "SELECT SUM(integralLibre) AS 'Intégral libre', SUM(integralRestreint) AS 'Intégral restreint',
       SUM(sommaire) AS 'Sommaire', SUM(sommaireSansIntegral) AS 'Sommaire sans accès intégral',
       SUM(indexation) AS 'Indexation', SUM(sansAcces) AS 'Sans accès', COUNT(*) AS 'Total'
FROM (
SELECT revueId
, MAX(`type`='Intégral' AND acces='Libre') AS integralLibre
, MAX(`type`='Intégral' AND acces='Restreint') AS integralRestreint
, MAX(`type`='Sommaire') AS sommaire
, (MAX(`type`='Sommaire') AND NOT MAX(`type`='Intégral')) AS sommaireSansIntegral
, MAX(`type`='Indexation') AS indexation
, MIN(s.id IS NULL) AS sansAcces
FROM Titre t LEFT JOIN Service s ON (s.titreId = t.id)
WHERE s.statut = 'normal' OR s.statut IS NULL
GROUP BY t.revueId
) AS sub LIMIT 1";
		return $this->db->createCommand($sql)->queryRow();
	}

	public function countLangGroups(): int
	{
		return (int) $this->db->createCommand("SELECT count(DISTINCT langues) FROM Titre")->queryScalar();
	}

	public function countLangGroupsUnordered(): int
	{
		$langs = $this->db->createCommand("SELECT DISTINCT langues FROM Titre")->queryColumn();
		$counter = [];
		foreach ($langs as $l) {
			$sorted = preg_split('/,\s*/', $l, -1, PREG_SPLIT_NO_EMPTY);
			sort($sorted);
			$counter[join(' ', $sorted)] = 1;
		}
		return count($counter);
	}

	public function getLangNb(): \HtmlTable
	{
		$exact = $this->db->createCommand(
			"SELECT count(DISTINCT revueId) FROM Titre WHERE obsoletePar IS NULL AND langues NOT LIKE '%mul' AND langues = :lang"
		);
		$count = $this->db->createCommand(
			"SELECT count(DISTINCT revueId) FROM Titre WHERE obsoletePar IS NULL AND langues NOT LIKE '%mul' AND langues != '' AND (1 + LENGTH(langues) - LENGTH(REPLACE(langues, ',', ''))) = :count"
		);
		$many = $this->db->createCommand(
			"SELECT count(DISTINCT revueId) FROM Titre WHERE obsoletePar IS NULL AND (langues LIKE '%mul' OR (1 + LENGTH(langues) - LENGTH(REPLACE(langues, ',', ''))) > 3)"
		);
		$data = [
			['Sans langue précisée', $exact->queryScalar([':lang' => ''])],
			['Une langue', $count->queryScalar([':count' => 1])],
			['Deux langues', $count->queryScalar([':count' => 2])],
			['Trois langues', $count->queryScalar([':count' => 3])],
			['Plus de langues', $many->queryScalar()],
		];
		return new HtmlTable(
			$data,
			["Langue de la revue (dernier titre)", "Nombre de revues"]
		);
	}

	public function getLangInfo(): \HtmlTable
	{
		$exact = $this->db->createCommand(
			"SELECT count(DISTINCT revueId) FROM Titre WHERE obsoletePar IS NULL AND langues NOT LIKE '%mul' AND langues = :lang"
		);
		$exactDuo = $this->db->createCommand(
			"SELECT count(DISTINCT revueId) FROM Titre WHERE obsoletePar IS NULL AND langues NOT LIKE '%mul' AND (langues = :langa OR langues = :langb)"
		);
		$cmp = $this->db->createCommand(
			"SELECT count(DISTINCT revueId) FROM Titre WHERE obsoletePar IS NULL AND langues LIKE :lang"
		);
		$cmpDuo = $this->db->createCommand(
			"SELECT count(DISTINCT revueId) FROM Titre WHERE obsoletePar IS NULL AND langues LIKE :langa AND langues LIKE :langb"
		);
		$data = [
			['Français', $cmp->queryScalar([':lang' => '%fre%']), $exact->queryScalar([':lang' => 'fre'])],
			['Anglais', $cmp->queryScalar([':lang' => '%eng%']), $exact->queryScalar([':lang' => 'eng'])],
			['Espagnol', $cmp->queryScalar([':lang' => '%spa%']), $exact->queryScalar([':lang' => 'spa'])],
			['Allemand', $cmp->queryScalar([':lang' => '%ger%']), $exact->queryScalar([':lang' => 'ger'])],
			['Bilingue français-anglais', $cmpDuo->queryScalar([':langa' => '%fre%', ':langb' => '%eng%']), $exactDuo->queryScalar([':langa' => 'fre, eng', ':langb' => 'eng, fre'])],
		];
		return new HtmlTable(
			$data,
			["Langue", "Parmi les langues de la revue", "Langue unique de la revue"]
		);
	}

	public function getEditeursNb(): \HtmlTable
	{
		$query = $this->db->createCommand(
			"SELECT nbed, count(*) AS nbtitres FROM ("
			. "SELECT t.id, count(*) AS nbed FROM Titre t LEFT JOIN Titre_Editeur te ON te.titreId = t.id WHERE t.obsoletePar IS NULL GROUP BY t.id"
			. ") AS tmp GROUP BY nbed"
		);
		$data = [
			["Sans éditeur", 0],
			["Un éditeur", 0],
			["Deux éditeurs", 0],
			["Trois éditeurs", 0],
			["Plus de 3 éditeurs", 0],
		];
		$nums = $query->queryAll(false);
		foreach ($nums as $num) {
			if ($num[0] > 3) {
				$data[4][1] += $num[1];
			} else {
				$data[(int) $num[0]][1] += $num[1];
			}
		}
		if (!$data[0][1]) {
			array_shift($data);
		}
		return new HtmlTable(
			$data,
			["Nombre d'éditeurs (dernier titre)", "Nombre de revues"]
		);
	}
}
