<?php

namespace models\stats;

use CDbConnection;
use CDbExpression;
use HtmlTable;
use Intervention;
use Partenaire;
use Yii;

/**
 * Indicateurs d'activité
 */
class Activite extends \CComponent
{
	/**
	 * @var Partenaire|null
	 */
	public $partenaire;

	/**
	 * @var int Timestamp
	 */
	protected $startTs;

	/**
	 * @var int Timestamp
	 */
	protected $endTs;

	/**
	 * @var CDbConnection
	 */
	private $db;

	public function __construct(CDbConnection $db)
	{
		$this->db = $db;
		$this->startTs = mktime(0, 0, 0, null, null, (int) date('Y') - 1);
	}

	public function setInterval($startTs, $endTs)
	{
		$this->startTs = $startTs;
		$this->endTs = $endTs;
	}

	public function getIntervalMessage()
	{
		if (!$this->startTs || !$this->endTs) {
			return ' (période non valide)';
		}
		return " sur la période	<em>"
			. strftime('%d %B %Y', $this->startTs) . " — "
			. strftime('%d %B %Y', $this->endTs)
			. "</em>";
	}

	public function getSuivi()
	{
		$table = new HtmlTable();
		if (!$this->partenaire) {
			return $table;
		}
		$table->header = ["Suivis", "Total", "dont revues mortes", "dont (partiellement) importées", "Plus ancienne vérification"];
		$revuesSuivies = $this->partenaire->countRevuesSuivies();
		$oldestRessourceCheck = $this->db->createCommand(
			"SELECT min(hdateVerif) FROM Suivi s JOIN Ressource r ON (r.id = s.cibleId AND cible = 'Ressource')"
			. " WHERE s.partenaireId = {$this->partenaire->id}"
		)->queryScalar();
		$oldestRevueCheck = $this->db->createCommand(
			"SELECT min(hdateVerif) FROM Suivi s JOIN Revue r ON (r.id = s.cibleId AND cible = 'Revue')"
			. " WHERE s.partenaireId = {$this->partenaire->id}"
		)->queryScalar();
		$table->data = [
			[
				'Revues',
				$revuesSuivies,
				$revuesSuivies - $this->db->createCommand(
					"SELECT count(DISTINCT r.id) FROM Suivi s JOIN Revue r ON (r.id = s.cibleId AND cible = 'Revue')"
					. " JOIN Titre t ON t.revueId = r.id"
					. " WHERE s.partenaireId = {$this->partenaire->id} AND t.obsoletePar IS NULL AND dateFin = ''"
				)->queryScalar(),
				$this->db->createCommand(
					"SELECT count(DISTINCT r.id) FROM Suivi s JOIN Revue r ON (r.id = s.cibleId AND cible = 'Revue')"
					. " JOIN Titre t ON t.revueId = r.id"
					. " JOIN Service se ON se.titreId = t.id"
					. " WHERE s.partenaireId = {$this->partenaire->id} AND se.import > 0"
				)->queryScalar(),
				($oldestRevueCheck ? strftime("%e %B %Y", $oldestRevueCheck) : "aucune vérification"),
			],
			[
				'Ressources',
				$this->db->createCommand(
					"SELECT count(*) FROM Suivi s JOIN Ressource r ON (r.id = s.cibleId AND cible = 'Ressource')"
					. " WHERE s.partenaireId = {$this->partenaire->id}"
				)->queryScalar(),
				'-',
				$this->db->createCommand(
					"SELECT count(*) FROM Suivi s JOIN Ressource r ON (r.id = s.cibleId AND cible = 'Ressource')"
					. " WHERE s.partenaireId = {$this->partenaire->id} AND r.autoImport > 0"
				)->queryScalar(),
				($oldestRessourceCheck ? strftime("%e %B %Y", $oldestRessourceCheck) : "aucune vérification"),
			],
		];
		return $table;
	}

	/**
	 * @return HtmlTable
	 */
	public function getProp()
	{
		if (!$this->startTs || !$this->endTs) {
			return new HtmlTable;
		}
		$sql = "SELECT (count(*) - count(utilisateurIdProp)), count(utilisateurIdProp), count(*) "
			. "FROM Intervention "
			. "WHERE import = 0 AND ip <> ''"
			. " AND hdateProp BETWEEN :begin AND :end";
		$params = [':begin' => $this->startTs, ':end' => $this->endTs];
		$table = new HtmlTable();
		$table->header = ['', 'Interventions externes', 'Interventions internes', 'Ensemble des interventions'];
		$table->rowHeaders = ['Tout type', 'Dont propositions'];
		$table->data[] = $this->db->createCommand($sql)->queryRow(false, $params);
		$sql .= " AND (utilisateurIdVal IS NULL OR utilisateurIdProp IS NULL OR utilisateurIdVal != utilisateurIdProp)";
		$table->data[] = $this->db->createCommand($sql)->queryRow(false, $params);
		return $table;
	}

	/**
	 * @return HtmlTable
	 */
	public function getPropParStatut()
	{
		if (!$this->startTs || !$this->endTs) {
			return new HtmlTable;
		}
		$params = [':begin' => $this->startTs, ':end' => $this->endTs];
		$cmd = $this->db->createCommand()
			->select('statut, count(*) AS interventions')
			->from('Intervention i')
			->where("import = 0 AND ip <> '' AND hdateProp BETWEEN :begin AND :end")
			->group([new CDbExpression('statut WITH ROLLUP')]);
		if ($this->partenaire) {
			$cmd->join("Utilisateur u", "u.id = i.utilisateurIdProp")
				->andWhere("u.partenaireId = " . $this->partenaire->id);
		}
		return new HtmlTable(
			$cmd->queryAll(false, $params),
			["Statut", "Interventions"]
		);
	}

	/**
	 * @return HtmlTable
	 */
	public function getValParType()
	{
		if (!$this->startTs || !$this->endTs) {
			return new HtmlTable;
		}
		$params = [':begin' => $this->startTs, ':end' => $this->endTs];
		$cmd = $this->db->createCommand()
			->select('action, count(*) AS interventions')
			->from('Intervention i')
			->where("import = 0 AND ip <> '' AND hdateVal BETWEEN :begin AND :end")
			->group("action");
		if ($this->partenaire) {
			$cmd->join("Utilisateur u", "u.id = i.utilisateurIdProp")
				->andWhere("u.partenaireId = " . $this->partenaire->id);
		}
		$data = $cmd->queryAll(false, $params);
		foreach ($data as $num => $row) {
			if ($row[0]) {
				$data[$num][0] = Intervention::ACTIONS[$row[0]];
			} else {
				$data[$num][0] = "Modification";
			}
		}
		return new HtmlTable(
			$data,
			["Type d'action", "Interventions"]
		);
	}

	/**
	 * @return HtmlTable
	 */
	public function countInterventions()
	{
		if (!$this->startTs || !$this->endTs) {
			return new HtmlTable;
		}
		$params = [':begin' => $this->startTs, ':end' => $this->endTs];
		$cmd = $this->db->createCommand()
			->select('count(*)')
			->from('Intervention i')
			->where("hdateVal BETWEEN :begin AND :end");
		if ($this->partenaire) {
			$cmd->join("Utilisateur u", "u.id = i.utilisateurIdVal")
				->andWhere("u.partenaireId = " . $this->partenaire->id);
		}
		$cmdI = clone $cmd;
		$countI = $cmdI->andWhere("import > 0 OR ip = ''")->queryScalar($params);
		$countO = $cmd->andWhere("import = 0 AND ip <> ''")->queryScalar($params);
		return new HtmlTable(
			[
				[$countI, $countO, $countI + $countO],
			],
			["Interventions d'import", "Autres interventions validées", "Total"]
		);
	}

	/**
	 * @return HtmlTable
	 */
	public function countPropPerUser()
	{
		if (!$this->partenaire || !$this->startTs || !$this->endTs) {
			return new HtmlTable;
		}
		$params = [
			':beginp' => $this->startTs, ':endp' => $this->endTs,
			':beginv' => $this->startTs, ':endv' => $this->endTs,
		];
		$cmd = $this->db->createCommand(
			<<<EOSQL
				WITH propositions AS (
				    SELECT up.id, count(i.id) as countProp
				    FROM Utilisateur up JOIN Intervention i ON up.id = i.utilisateurIdProp
				    WHERE up.partenaireId = {$this->partenaire->id} AND import = 0 AND hdateProp BETWEEN :beginp AND :endp
				    GROUP BY up.id
				),
				validations AS (
				    SELECT uv.id, count(i.id) as countVal
				    FROM Utilisateur uv
				    JOIN Intervention i ON uv.id = i.utilisateurIdVal
				    WHERE uv.partenaireId = {$this->partenaire->id} AND import = 0 AND hdateVal BETWEEN :beginv AND :endv
				    GROUP BY uv.id
				)

				SELECT
				    CONCAT(u.nomcomplet, ' (', u.login, ')'), IF(actif, '', 'désactivé'),
				    propositions.countProp,
				    validations.countVal
				FROM Utilisateur u
				    LEFT JOIN propositions USING (id)
				    LEFT JOIN validations USING (id)
				WHERE u.partenaireId = {$this->partenaire->id}
				ORDER BY u.nom ASC
				EOSQL
		);
		return new HtmlTable(
			$cmd->queryAll(false, $params),
			["Utilisateur", "Compte activé ?", "Interventions proposées", "Interventions validées"]
		);
	}
}
