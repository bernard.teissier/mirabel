<?php

/**
 * ImportKbartForm class.
 *
 * Uses ImportKbart.
 *
 * @property string $csvfile
 * @property string $acces
 * @property bool $lacunaire
 * @property bool $selection
 * @property bool $simulation
 */
class ImportKbartForm extends CFormModel
{
	public $ressourceId;

	public $collectionId;

	public $csvfile;

	public $acces;

	public $lacunaire;

	public $selection;

	public $defaultType = '';

	public $ignoreUnknownTitles = true;

	public $simulation;

	public $directCreation; // only if admin

	public $checkDeletion;

	public $ignoreUrl;

	public $ignoredFields = [];

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return [
			['ressourceId', 'required'],
			['csvfile', 'file', 'types' => 'csv,tsv,txt'],
			['ressourceId', 'numerical', 'integerOnly' => true],
			['collectionId', 'safe'],
			['acces', 'length', 'max' => 30],
			['acces', 'in', 'range' => array_keys(Service::$enumAcces), 'allowEmpty' => true], // enum
			['lacunaire, selection, simulation, directCreation, checkDeletion, ignoreUnknownTitles, ignoreUrl', 'boolean'],
			['defaultType', 'in', 'range' => array_keys(Service::$enumType)],
		];
	}

	public function afterValidate()
	{
		if (!Yii::app()->user->checkAccess('import/admin')) {
			$this->directCreation = false;
		}
		if ($this->collectionId) {
			$collection = Collection::model()->findByPk($this->collectionId);
			if ((int) $this->ressourceId !== (int) $collection->ressourceId) {
				$this->addError('collectionId', "Cette collection n'appartient pas à la ressource choisie.");
			}
			if (!$collection) {
				$collection = Collection::model()->findByAttributes(
					['nom' => $this->collectionId, 'ressourceId' => $this->ressourceId]
				);
			}
			if (!$collection) {
				$this->addError('collectionId', "Cette collection est introuvable.");
			}
		} elseif ($this->getRessource()) {
			if ($this->getRessource()->hasCollections()) {
				$this->addError('collectionId', "Le choix d'une collection est obligatoire quand la ressource en contient.");
			}
		}
	}

	/**
	 * Declares customized attribute labels.
	 *
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return [
			'csvfile' => 'Fichier KBART à importer',
			'ressourceId' => 'Ressource',
			'acces' => 'Accès par défaut',
			'defaultType' => "Contenu par défaut",
			'collectionId' => 'Collection',
			'ignoreUnknownTitles' => "N'importer que les accès des revues déjà dans Mir@bel",
			'lacunaire' => 'Couverture lacunaire',
			'selection' => "Sélection d'articles",
			'simulation' => 'Simulation',
			'directCreation' => 'Création directe des titres (admin)',
			'checkDeletion' => "Vérifier si des titres ont été retirés",
			'ignoreUrl' => "Ignorer les URL des titres",
		];
	}

	/**
	 * Returns the parameters that will feed the Import's constructor.
	 *
	 * @return array
	 */
	public function getParameters()
	{
		return [
			'url' => $this->csvfile->tempName,
			'ressourceId' => (int) $this->ressourceId,
			'collectionId' => (int) $this->collectionId,
			'acces' => $this->acces,
			'defaultType' => $this->defaultType,
			'lacunaire' => $this->lacunaire,
			'selection' => $this->selection,
			'ignoreUnknownTitles' => (boolean) $this->ignoreUnknownTitles,
			'simulation' => (boolean) $this->simulation,
			'directCreation' => (boolean) $this->directCreation,
			'ignoreUrl' => (boolean) $this->ignoreUrl,
		];
	}

	/**
	 * @return Ressource
	 */
	public function getRessource()
	{
		if (empty($this->ressourceId)) {
			return null;
		}
		return Ressource::model()->findByPk($this->ressourceId);
	}

	public function onUnsafeAttribute($name, $value)
	{
		// do nothing
	}
}
