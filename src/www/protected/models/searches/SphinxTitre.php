<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class SphinxTitre extends CModel
{
	/**
	 * @var array
	 */
	public $metadata;

	/**
	 * @var int
	 */
	public $id;

	/**
	 * @var int
	 */
	public $revueid;

	/**
	 * @var string
	 */
	public $lettre1;

	/**
	 * @var int
	 */
	public $nbacces;

	/**
	 * @var bool
	 */
	public $obsolete;

	/**
	 * @var bool
	 */
	public $vivant;

	/**
	 * @var int
	 */
	public $hdatemodif;

	/**
	 * @var int
	 */
	public $hdateverif;

	/**
	 * @var int
	 */
	public $paysid;

	/**
	 * @var array
	 */
	public $editeurid;

	/**
	 * @var array
	 */
	public $ressourceid;

	/**
	 * @var array
	 */
	public $collectionid;

	/**
	 * @var array
	 */
	public $suivi;

	/**
	 * @var array
	 */
	public $detenu;

	/**
	 * @var int
	 */
	public $acces;

	/**
	 * @var int
	 */
	public $acceslibre;

	/**
	 * @var array
	 */
	public $issn;

	/**
	 * @var array
	 */
	public $categorie;

	/**
	 * @var array
	 */
	public $revuecategorie;

	/**
	 * @var bool
	 */
	public $abonnement;

	/**
	 * @var string
	 */
	public $titrecomplet;

	public function attributeNames()
	{
		return [
			'id',
			'revueid',
			'lettre1',
			'nbacces',
			'obsolete',
			'vivant',
			'hdatemodif',
			'hdateverif',
			'paysid',
			'editeurid',
			'ressourceid',
			'collectionid',
			'suivi',
			'detenu',
			'acces',
			'acceslibre',
			'issn',
			'categorie',
			'revuecategorie',
			'abonnement',
			'titrecomplet',
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'revueid' => 'Revue',
			'nbacces' => "Nombre d'accès",
			'obsolete' => "Obsolète",
			'vivant' => "Vivant",
			'hdatemodif' => "Dernière modif",
			'hdateverif' => "Dernière vérif",
			'paysid' => "Pays",
			'editeurid' => "Éditeurs",
			'ressourceid' => "Ressources",
			'collectionid' => "Collections",
			'suivi' => "Partenaires suivant",
			'detenu' => "Partenaires détenant",
			'acces' => "Liste des accès",
			'acceslibre' => "Accès libres",
			'issn' => "ISSN",
			'categorie' => "Thèmes",
			'revuecategorie' => "Thèmes de la revue",
			'abonnement' => "Abonnés",
			'titrecomplet' => "Titre",
		];
	}
}
