<?php

/**
 * AbonnementSearch adds search functions to Abonnement.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class AbonnementSearch extends Abonnement
{
	/**
	 * @var string Field for search and extra data.
	 */
	public $collectionNom;

	/**
	 * @var string Field for search and extra data.
	 */
	public $ressourceNom;

	/**
	 * @var string Field for extra data.
	 */
	public $collectionType;

	/**
	 * @var array Internal cache for a given partenaireId
	 */
	private $cache;

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['partenaireId', 'numerical', 'integerOnly' => true],
			['collectionNom, ressourceNom', 'length', 'max' => 50],
			//array('ressourceId, collectionId', 'numerical', 'integerOnly' => true),
			['mask', 'in', 'range' => [0, 1, 2]],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'ressourceNom' => 'Ressource',
			'collectionNom' => 'Collection',
			'collectionType' => 'Type de coll.',
			'mask' => 'Abonnement',
			'type' => 'Type de coll.',
		];
	}

	public function getFilters()
	{
		return array_filter([
			'collectionNom' => $this->collectionNom,
			'ressourceNom' => $this->ressourceNom,
			'collectionType' => $this->collectionType,
		]);
	}

	/**
	 * Return the type of Abonnement for a Ressource|Collection|Service record.
	 *
	 * @param CActiveRecord $record
	 * @throws Exception
	 * @return int const from Abonnement (PAS_ABONNE, etc)
	 */
	public function readStatus($record)
	{
		$type = self::detectRecordType($record);
		return $this->readStatusById($type[0], $type[1], 'mask');
	}

	/**
	 * Return true if there is at least one active Abonnement.proxy for a Ressource|Collection|Service record.
	 *
	 * @param CActiveRecord $record
	 * @return bool
	 */
	public function readProxy($record)
	{
		$type = self::detectRecordType($record);
		$states = $this->readAbonnemntsById($type[0], $type[1], 'proxy');
		if ($states) {
			return (bool) (int) max($states);
		}
		return false;
	}

	public function readAbonnement(CActiveRecord $record): ?Abonnement
	{
		if (!$this->cache) {
			$this->cache = $this->initCache();
		}
		list($table, $id) = self::detectRecordType($record);
		if (!is_array($id) && isset($this->cache[$table][$id])) {
			return $this->cache[$table][$id];
		}
		return null;
	}

	/**
	 * Return true if the Partenaire has masked this $id of Collection|Ressource.
	 *
	 * @param string $table
	 * @param int|array $id Collection|Ressource ID
	 * @param string $column From the Abonnement record: mask | proxy
	 * @return int By default (mask), a const from Abonnement (PAS_ABONNE, etc)
	 */
	public function readStatusById($table, $id, string $column = 'mask')
	{
		$states = $this->readAbonnemntsById($table, $id, $column);
		$status = self::PAS_ABONNE;
		foreach ($states as $s) {
			if ($s == self::ABONNE) {
				return self::ABONNE;
			}
			$status = $s;
		}
		return $status;
	}

	/**
	 * Return the number of Abonnement (all types) for this Partenaire.
	 *
	 * @return int
	 */
	public function countAll()
	{
		return $this->countByStatus();
	}

	/**
	 * Return the number of hiding Abonnement for this Partenaire.
	 *
	 * @return int
	 */
	public function countMasques()
	{
		return $this->countByStatus(2);
	}

	/**
	 * Return the number of Abonnement (not hidden) for this Partenaire.
	 *
	 * @return int
	 */
	public function countAbonnements()
	{
		return $this->countByStatus(1);
	}

	/**
	 * Retrieves collections with abonnements for a given Partenaire.
	 *
	 * @param int|bool $pageSize Number of items for pagination (false to disable pagination).
	 * @return CSqlDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($pageSize = 25)
	{
		$query = Yii::app()->db->createCommand()
			->from('Ressource r')
			->leftJoin("Collection c", "c.ressourceId = r.id")
			->leftJoin(
				"Abonnement a",
				"a.partenaireId = {$this->partenaireId} AND ((a.collectionId IS NOT NULL AND a.collectionId = c.id) OR (a.collectionId IS NULL AND a.ressourceId = r.id))"
			)
			->select("a.id, r.id AS ressourceId, c.id AS collectionId, a.partenaireId, a.mask, a.proxy, a.proxyUrl, r.nom AS ressourceNom, c.nom AS collectionNom, c.type as collectionType");

		if ($this->ressourceNom) {
			$query->andWhere("r.nom LIKE " . Yii::app()->db->quoteValue("%" . $this->ressourceNom . "%"));
		}
		if ($this->collectionNom) {
			$query->andWhere("c.nom LIKE " . Yii::app()->db->quoteValue("%" . $this->collectionNom . "%"));
		}
		if ($this->collectionType) {
			$query->andWhere("c.type = " . Yii::app()->db->quoteValue($this->collectionType));
		}
		if ($this->mask > 0) {
			$query->andWhere('a.mask = ' . (int) $this->mask);
		} elseif ($this->mask === "0") {
			$query->andWhere('a.mask IS NULL');
		}

		$sort = new CSort();
		$sort->attributes = [
			'mask' => '(a.mask IS NOT NULL AND a.mask=1) DESC, a.mask DESC, r.nom ASC, c.nom ASC',
			'ressourceNom' => 'r.nom',
			'collectionNom' => 'c.nom',
		];
		$sort->defaultOrder = '(a.id IS NOT NULL AND a.mask=1) DESC, a.mask DESC, r.nom ASC, c.nom ASC';

		$count = clone $query;
		$count->select("count(*)");

		return new CSqlDataProvider(
			$query,
			[
				'totalItemCount' => $count->queryScalar(),
				'pagination' => ($pageSize === false ? false : ['pageSize' => $pageSize]),
				'sort' => $sort,
			]
		);
	}

	/**
	 * Return the [table, ID|IDs] of the record.
	 *
	 * @param CActiveRecord $record
	 * @throws Exception
	 * @return array
	 */
	private static function detectRecordType($record)
	{
		if ($record instanceof Ressource) {
			return ['Ressource', $record->id];
		}
		if ($record instanceof Collection) {
			return ['Collection', $record->id];
		}
		if ($record instanceof Service) {
			$collectionsIds = $record->getCollectionIds();
			if ($collectionsIds) {
				return ['Collection', $collectionsIds];
			}
			return ['Ressource', $record->ressourceId];
		}
		throw new Exception("Abonnement : type inconnu.");
	}

	/**
	 * Return an array of values from Abonnement (PAS_ABONNE, etc).
	 *
	 * @param string $table
	 * @param int|array $id Collection|Ressource ID
	 * @param string $column From the Abonnement record: mask | proxy
	 * @return array Array of values from Abonnement (PAS_ABONNE, etc)
	 */
	private function readAbonnemntsById($table, $id, string $column): array
	{
		if (!$this->cache) {
			$this->cache = $this->initCache();
		}
		$ids = is_array($id) ? $id : [(int) $id];
		$values = [];
		foreach ($ids as $i) {
			if (isset($this->cache[$table][(int) $i])) {
				$values[] = $this->cache[$table][(int) $i]->{$column};
			}
		}
		return $values;
	}

	/**
	 * Return the number of Abonnement with this status for this Partenaire.
	 *
	 * @param int $status (opt)
	 * @return int
	 */
	private function countByStatus($status = null)
	{
		$sql = "SELECT count(*) FROM Abonnement WHERE partenaireId = :pid";
		$params = [':pid' => $this->partenaireId];
		if (isset($status)) {
			$sql .= " AND mask = :status";
			$params[':status'] = (int) $status;
		}
		return (int) Yii::app()->db->createCommand($sql)->queryScalar($params);
	}

	/**
	 * @throws Exception
	 * @return array
	 */
	private function initCache()
	{
		$cache = ['Ressource' => [], 'Collection' => []];
		if ($this->partenaireId) {
			$abos = Abonnement::model()->findAllByAttributes(['partenaireId' => $this->partenaireId]);
			foreach ($abos as $a) {
				if ($a->collectionId) {
					$cache['Collection'][(int) $a->collectionId] = $a;
					if ($a->mask == Abonnement::ABONNE) {
						$aRess = clone ($a);
						$aRess->mask = self::ABONNE_VIA_COLLECTION;
						$cache['Ressource'][(int) $a->ressourceId] = $aRess;
					}
				} elseif ($a->ressourceId) {
					$cache['Ressource'][(int) $a->ressourceId] = $a;
				} else {
					throw new Exception("Abonnement : type inconnu.");
				}
			}
		}
		return $cache;
	}
}
