<?php

/**
 * SphinxRevue comes from a GROUP BY of records in the Sphinx "titres" index.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class SphinxRevue extends SphinxTitre
{
	/**
	 * @var bool
	 */
	public $revuevivante;

	public function attributeNames()
	{
		$attributes = parent::attributeNames();
		$attributes[] = 'revuevivante';
		return $attributes;
	}

	public function attributeLabels()
	{
		return array_merge(
			parent::attributeLabels(),
			[
				'revuevivante' => "Vivant",
			]
		);
	}
}
