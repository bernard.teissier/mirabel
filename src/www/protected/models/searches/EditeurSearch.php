<?php

namespace models\searches;

use components\sphinx\DataProvider;
use models\sphinx\Editeurs;
use Pays;

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class EditeurSearch extends \CModel
{
	public $idref = '';
	public $nom = '';
	public $pays = '';
	public $sherpa = '';
	public $vivants = false;
	public $hdateModif = '';
	public $hdateVerif = '';

	/**
	 * @var ?Pays Cache for getPays()
	 */
	private $paysRecord;

	public function __construct()
	{
		// default scenario is "search"
		$this->setScenario('search');
	}

	public function attributeNames()
	{
		return ['idref', 'nom', 'pays', 'hdateModif', 'hdateVerif', 'sherpa'];
	}

	public function attributeLabels()
	{
		return [
			'hdateModif' => "Date de modification",
			'hdateVerif' => "Date de vérification",
			'idref' => "IdRef",
			'sherpa' => "ID Sherpa",
		];
	}

	public function rules()
	{
		return [
			['idref', 'match', 'pattern' => '/^([0-9]{8}[0-9X]|!|\*)$/'],
			['nom', 'length', 'max' => 255],
			['pays', 'match', 'pattern' => '/^([0-9]+|[A-Z]{2}|[A-Z]{3}|!|\*)$/'],
			['vivants', 'boolean'],
			['hdateModif, hdateVerif', 'validateTimeCondition'],
			['sherpa', 'match', 'pattern' => '/^(\d+|!|\*)$/'],
		];
	}

	public function beforeValidate()
	{
		$m = [];
		if ($this->idref && preg_match('#idref.fr/(\d{8}[\dX])\b#', $this->idref, $m)) {
			$this->idref = $m[1];
		}
		if ($this->sherpa && preg_match('#^https?://v2\.sherpa\.ac\.uk/id/publisher/(\d+)$#', trim($this->sherpa), $m)) {
			$this->sherpa = $m[1];
		}
		return parent::beforeValidate();
	}

	public function validateTimeCondition($attrName)
	{
		if ($this->{$attrName} !== '') {
			try {
				\Tools::parseDateInterval($this->{$attrName});
			} catch (\Exception $_) {
				$this->addError($attrName, "Format non valide");
			}
		}
	}

	public function getSummary(): string
	{
		$criteria = [];
		if ($this->nom) {
			$criteria[] = "[nommé « {$this->nom} »]";
		}
		if ($this->idref) {
			if ($this->idref === '*') {
				$criteria[] = "[avec IdRef]";
			} elseif ($this->idref === '!') {
				$criteria[] = "[sans IdRef]";
			} else {
				$criteria[] = "[IdRef {$this->idref}]";
			}
		}
		if ($this->pays) {
			$criteria[] = "[pays : {$this->getPays()->nom}]";
		}
		if ($this->vivants) {
			$criteria[] = "[avec revues vivantes]";
		}
		if ($this->hdateModif) {
			$criteria[] = "[modifié: {$this->hdateModif}]";
		}
		if ($this->hdateVerif) {
			$criteria[] = "[vérifié: {$this->hdateVerif}]";
		}
		if ($this->sherpa) {
			if ($this->sherpa === '*') {
				$criteria[] = "[avec Sherpa]";
			} elseif ($this->sherpa === '!') {
				$criteria[] = "[sans Sherpa]";
			} else {
				$criteria[] = "[Sherpa {$this->sherpa}]";
			}
		}
		return join(" ", $criteria);
	}

	public function isEmpty(): bool
	{
		return empty($this->idref) && empty($this->nom) && empty($this->pays) && !$this->vivants
			&& !$this->hdateModif && !$this->hdateVerif && !$this->sherpa;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @param int|bool $pageSize Number of items for pagination (false to disable pagination).
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($pageSize = 25)
	{
		$criteria = new \CDbCriteria;

		if ($this->validate()) {
			if ($this->nom) {
				$pattern = "@nomcomplet " . \components\sphinx\Schema::quote($this->nom);
				$criteria->addCondition("MATCH('" . $pattern  . "')");
			}

			if ($this->pays === '*') {
				$criteria->addCondition("paysid > 0");
			} elseif ($this->pays === '!') {
				$criteria->addCondition("paysid = 0");
			} elseif (ctype_digit ($this->pays)) {
				$criteria->addCondition("paysid = " . (int) $this->pays);
			} elseif ($this->pays) {
				$pays = $this->getPays();
				if ($pays !== null) {
					$criteria->addCondition("paysid = {$pays->id}");
				}
			}

			if ($this->idref === '*') {
				$criteria->addCondition("idref > 0");
			} elseif ($this->idref === '!') {
				$criteria->addCondition("idref = 0");
			} elseif ($this->idref) {
				$criteria->addCondition('idref = ' . (int) substr($this->idref, 0, 8));
			}

			if ($this->sherpa === '*') {
				$criteria->addCondition("sherpa > 0");
			} elseif ($this->sherpa === '!') {
				$criteria->addCondition("sherpa = 0");
			} elseif ($this->sherpa) {
				$criteria->addCondition("sherpa = " . (int) $this->sherpa);
			}

			if ($this->vivants) {
				$criteria->addCondition('nbtitresvivants > 0');
			}

			if ($this->hdateModif) {
				$cond = \Tools::dateIntervalToSqlCondition($this->hdateModif, 'hdateModif');
				if ($cond) {
					$criteria->addCondition($cond);
				}
			}

			if ($this->hdateVerif) {
				$cond = \Tools::dateIntervalToSqlCondition($this->hdateVerif, 'hdateVerif');
				if ($cond) {
					$criteria->addCondition($cond);
				}
			}
		} else {
			$criteria->addCondition('id = 0');
		}

		$sort = new \CSort();
		$sort->attributes = [
			'nom' => ['asc' => 'cletri ASC', 'desc' => 'cletri DESC'],
			'nbrevues' => ['asc' => 'nbrevues DESC', 'desc' => 'nbrevues ASC'],
			'nbtitresvivants' => ['asc' => 'nbtitresvivants DESC', 'desc' => 'nbtitresvivants ASC'],
		];
		$sort->defaultOrder = ['nom' => \CSort::SORT_ASC];

		return new DataProvider(
			Editeurs::model(),
			[
				'source' => \Editeur::model(),
				'criteria' => $criteria,
				'pagination' => $pageSize ? ['pageSize' => $pageSize] : false,
				'sort' => $sort,
			]
		);
	}

	private function getPays(): ?Pays
	{
		if ($this->paysRecord === null && $this->pays) {
			if (ctype_digit($this->pays)) {
				$this->paysRecord = Pays::model()->findByAttributes(['id' => $this->pays]);
			} elseif (strlen($this->pays) === 2) {
				$this->paysRecord = Pays::model()->findByAttributes(['code2' => $this->pays]);
			} elseif (strlen($this->pays) === 3) {
				$this->paysRecord = Pays::model()->findByAttributes(['code' => $this->pays]);
			}
			if ($this->paysRecord !== null) {
				$this->pays = (int) $this->paysRecord->id;
			}
		}
		return $this->paysRecord;
	}
}
