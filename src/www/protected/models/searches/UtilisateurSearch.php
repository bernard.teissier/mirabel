<?php

/**
 * Description of UtilisateurSearch
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class UtilisateurSearch extends Utilisateur
{
	public $actif = 1; //default value

	public $partenaireType = "normal";

	public $special;

	public static $specialValues = [
		'permAdmin' => "perm Administrateur",
		'permImport' => "perm Import",
		'permIndexation' => "perm Indexation",
		'permPartenaire' => "perm Partenaire",
		'permRedaction' => "perm Rédaction",
		'suiviEditeurs' => "suivi Editeurs",
		'suiviNonSuivi' => "suivi Non-suivi",
		'suiviPartenairesEditeurs' => "suivi Partenaires-Éditeurs",
	];

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['partenaireId', 'numerical', 'integerOnly' => true],
			['actif', 'boolean'],
			['special', 'in', 'range' => array_keys(self::$specialValues)],
			['login, derConnexion, hdateCreation, hdateModif, '
				. 'permAdmin, permImport, permPartenaire, permIndexation, permRedaction, '
				. 'suiviEditeurs, suiviNonSuivi, partenaireType',
				'safe',
			],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array_merge(
			parent::attributeLabels(),
			[
				'partenaireType' => "Type du partenaire",
				'special' => "Perm/Suivi",
			]
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @param int|bool $pageSize Number of items for pagination (false to disable pagination).
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($pageSize = 25)
	{
		if ($this->partenaireId) {
			$this->partenaireType = null;
		}

		$criteria = new CDbCriteria;
		$criteria->join = "JOIN Partenaire p ON p.id = t.partenaireId";
		$criteria->with = ['partenaire'];
		$criteria->select = "t.*, p.type AS partenaireType";

		$criteria->compare('partenaireId', $this->partenaireId);
		$criteria->compare('p.type', $this->partenaireType);
		$criteria->compare('login', $this->login, true);
		$criteria->compare('actif', $this->actif);
		$criteria->addCondition(DateVersatile::buildTsConditionFromDateValue('derConnexion', (string) $this->derConnexion));
		$criteria->addCondition(DateVersatile::buildTsConditionFromDateValue('hdateCreation', (string) $this->hdateCreation));
		$criteria->addCondition(DateVersatile::buildTsConditionFromDateValue('t.hdateModif', (string) $this->hdateModif));
		$criteria->compare('permAdmin', $this->permAdmin);
		$criteria->compare('permImport', $this->permImport);
		$criteria->compare('permPartenaire', $this->permPartenaire);
		$criteria->compare('permIndexation', $this->permIndexation);
		$criteria->compare('permRedaction', $this->permRedaction);
		$criteria->compare('suiviEditeurs', $this->suiviEditeurs);
		$criteria->compare('suiviNonSuivi', $this->suiviNonSuivi);
		if ($this->special) {
			$criteria->addColumnCondition([$this->special => 1]);
		}

		$sort = new CSort();
		$sort->attributes = ['partenaireId', 'login', 'derConnexion', 't.hdateCreation', 't.hdateModif'];
		$sort->defaultOrder = 'login ASC';

		return new CActiveDataProvider(
			$this,
			[
				'criteria' => $criteria,
				'pagination' => ($pageSize ? ['pageSize' => $pageSize] : false),
				'sort' => $sort,
			]
		);
	}

	public function getSpecialities()
	{
		$specialities = [];
		foreach (self::$specialValues as $attr => $name) {
			if ($this->{$attr}) {
				$s = explode(" ", $name);
				$specialities[] = '<span title="' . $name . '">' . $s[0][0] . $s[1][0] . '</span>';
			}
		}
		return join(" ", $specialities);
	}
}
