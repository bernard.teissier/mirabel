<?php

class VerifUrlForm extends CFormModel
{
	public $msgContient = "";

	public $msgExclut = "Operation timed out";

	public $urlContient = "";

	public $urlExclut = "";

	public $suiviPar;

	public $nonSuivi = false;

	public $accesIntegralLibre = false;

	public function rules()
	{
		return [
			['msgContient, msgExclut, urlContient, urlExclut', 'length', 'min' => 3, 'max' => 255],
			['suiviPar', 'numerical', 'integerOnly' => true],
			['accesIntegralLibre, nonSuivi', 'boolean'],
		];
	}

	public function getSearchData()
	{
		if (!$this->validate()) {
			return ['', []];
		}
		$conditions = [];
		$params = [];
		if ($this->msgExclut) {
			$conditions[] = "v.msg NOT LIKE :msgExclut";
			$params[':msgExclut'] = '%' . $this->msgExclut . '%';
		}
		if ($this->msgContient) {
			$conditions[] = "v.msg LIKE :msgContient";
			$params[':msgContient'] = '%' . $this->msgContient . '%';
		}
		if ($this->urlContient) {
			$conditions[] = "v.url LIKE :urlContient";
			$params[':urlContient'] = '%' . $this->urlContient . '%';
		}
		if ($this->urlExclut) {
			$conditions[] = "v.url NOT LIKE :urlExclut";
			$params[':urlExclut'] = '%' . $this->urlExclut . '%';
		}
		if ($conditions) {
			return [" AND " . join(" AND ", $conditions), $params];
		}
		return ['', []];
	}

	public function addSearchCriteria(CDbCommand $cmd, $target)
	{
		if (!$this->validate()) {
			$cmd->andWhere("1=0");
			return;
		}
		if ($this->msgExclut) {
			$cmd->andWhere("v.msg NOT LIKE :msgExclut", [':msgExclut' => '%' . $this->msgExclut . '%']);
		}
		if ($this->msgContient) {
			$cmd->andWhere("v.msg LIKE :msgContient", [':msgContient' => '%' . $this->msgContient . '%']);
		}
		if ($this->urlContient) {
			$cmd->andWhere("v.url LIKE :urlContient", [':urlContient' => '%' . $this->urlContient . '%']);
		}
		if ($this->urlExclut) {
			$cmd->andWhere("v.url NOT LIKE :urlExclut", [':urlExclut' => '%' . $this->urlExclut . '%']);
		}
		if ($this->nonSuivi && $this->suiviPar && $target === 'Revue') {
			$cmd->leftJoin("Suivi s", "s.cible = 'Revue' AND s.cibleId = v.revueId");
			$cmd->andWhere("s.partenaireId = :pid OR s.partenaireId IS NULL", [":pid" => $this->suiviPar]);
		} elseif ($this->suiviPar) {
			if ($target === 'Revue') {
				$cmd->join("Suivi s", "s.cible = 'Revue' AND s.cibleId = v.revueId");
			} elseif ($target === 'Editeur') {
				$cmd->join("Suivi s", "s.cible = 'Editeur' AND s.cibleId = v.id");
			} elseif ($target === 'Ressource') {
				$cmd->join("Suivi s", "s.cible = 'Ressource' AND s.cibleId = v.ressourceId");
			} else {
				throw new \Exception("Undefined target while filtering URLs.");
			}
			$cmd->andWhere("s.partenaireId = :pid", [":pid" => $this->suiviPar]);
		} elseif ($this->nonSuivi && $target === 'Revue') {
			$cmd->leftJoin("Suivi s", "s.cible = 'Revue' AND s.cibleId = v.revueId");
			$cmd->andWhere("s.partenaireId IS NULL");
		}
		if ($this->accesIntegralLibre) {
			if ($target === 'Revue') {
				$cmd->join("Service", "t.id = Service.titreId AND Service.url = v.url");
			} else {
				throw new \Exception("Undefined target while filtering URLs.");
			}
			$cmd->andWhere("Service.type = 'Intégral' AND Service.acces = 'Libre'");
		}
		return $cmd;
	}

	public function attributeLabels()
	{
		return [
			'msgContient' => "L'erreur contient",
			'msgExclut' => "L'erreur ne contient pas",
			'nonSuivi' => "Revues non suivies",
			'urlContient' => "L'URL contient",
			'urlExclut' => "L'URL ne contient pas",
		];
	}
}
