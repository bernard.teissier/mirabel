<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class RevueSearch extends CModel
{
	public $titrecomplet;

	public $partenaireId;

	public function attributeNames()
	{
		return ['partenaireId', 'titrecomplet'];
	}

	public function rules()
	{
		return [
			['partenaireId', 'numerical', 'integerOnly' => true],
			['titrecomplet', 'length', 'max' => 255],
		];
	}

	public function search()
	{
		Yii::import('ext.Sphinx.*');
		$sphinxClient = new Sphinx(Yii::app()->params->itemAt("sphinx"));
		$sphinxClient->indexes = 'titres';
		$sphinxClient->SetSortMode(SPH_SORT_EXTENDED, 'vivant DESC, obsolete ASC, id DESC');
		$sphinxClient->SetSelect("*, MAX(hdatemodif) AS datemodif, MAX(hdateverif) AS dateverif, MAX(vivant) AS revuevivante");

		$sphinxClient->SetFilter('suivi', [$this->partenaireId]);
		if ($this->titrecomplet) {
			$sphinxClient->query = $sphinxClient->EscapeString($this->titrecomplet);
		}

		if (!$this->validate()) {
			$sphinxClient->SetFilter('suivi', [-1]);
		}

		$sort = new SphinxSort([
			'titrecomplet' => 'cletri',
			'hdatemodif' => 'hdatemodif',
			'hdateverif' => 'hdateverif',
		]);
		$sort->defaultOrder = "cletri ASC";
		$order = $sort->getOrderBy();
		$sphinxClient->SetGroupBy('revueid', SPH_GROUPBY_ATTR, $order);

		return new SphinxDataProvider(
			$sphinxClient,
			'SphinxRevue', // model name
			[
				'pagination' => false,
				'sort' => $sort,
			]
		);
	}
}
