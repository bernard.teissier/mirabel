<?php

/**
 * Description of CollectionWithNums.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class CollectionWithNums extends Collection
{
	public $numRevues = 0;

	public $numTitres = 0;

	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @return Collection the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
