<?php

/**
 * This is the model class for table "Issn".
 *
 * The followings are the available columns in table 'Issn':
 * @property int $id SHA1(url)
 * @property int $sourceId Cf self::SOURCE_*
 * @property int $titreId
 * @property string $url
 * @property int $lastSeen timestamp
 *
 * @property Sourcelien $source
 * @property Titre $titre
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class ImportedLink extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @return Categorie the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ImportedLink';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'source' => [self::BELONGS_TO, 'Sourcelien', 'sourceId'],
			'titre' => [self::BELONGS_TO, 'Titre', 'titreId'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'sourceId' => "Source",
			'titreId' => 'Titre',
			'url' => 'URL',
			'lastSeen' => "Dernier import",
		];
	}

	public function save($runValidation = true, $attributes = null)
	{
		$this->id = sha1($this->url); // printable form, whereas the raw value is stored
		$this->lastSeen = $_SERVER['REQUEST_TIME'];
		if ($this->isNewRecord) {
			$this->getDbConnection()
				->createCommand(
					"INSERT INTO ImportedLink (id, sourceId, titreId, url, lastSeen)"
					. " VALUES (UNHEX(SHA1(:url1)), :sourceId, :titreId, :url2, :lastSeen1)"
					. " ON DUPLICATE KEY UPDATE lastSeen = :lastSeen2"
				)
				->execute([
					':url1' => $this->url,
					':sourceId' => $this->sourceId,
					':url2' => $this->url,
					':titreId' => $this->titreId,
					':lastSeen1' => $this->lastSeen,
					':lastSeen2' => $this->lastSeen,
				]);
		} else {
			$this->getDbConnection()
				->createCommand("UPDATE ImportedLink SET lastSeen = :lastSeen WHERE id = UNHEX(SHA1(:url)) AND sourceId = :sourceId")
				->execute([
					':url' => $this->url,
					':sourceId' => $this->sourceId,
					':lastSeen' => $this->lastSeen,
				]);
		}
		return true;
	}
}
