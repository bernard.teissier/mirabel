<?php

use models\wikidata\Compare;

/**
 * @todo Vue matrice de synthèse : en ligne les propriétes, en colonnes les différences-type, compte dans chaque cellule
 */

/**
 * This is the model class for table "Wikidata".
 *
 * @property int $id
 * @property int $revueId
 * @property int $titreId
 * @property string  $qId      eg. Q3428731
 * @property string  $property eg. "P2733" (identifiant Persée)
 * @property int $propertyType eg. 1 (voir WikidataComp)
 * @property string  $value    eg. "rfsp"
 * @property string  $url      eg. "https://www.persee.fr/collection/rfsp"
 * @property int $hdate timestamp
 * @property int $comparison see constants COMP_*
 * @property string  $compUrl Mirabel value to compare
 * @property string  $compDetails url components (json serialization)
 * @property int $compDate comparison timestamp
 *
 * @property Revue $revue
 * @property Titre $titre
 * @property Suivi[] $suivis
 */

class Wikidata extends CActiveRecord
{
	public const WD_BASE_URL = 'https://www.wikidata.org/wiki';

	public const REASONATOR_BASE_URL = 'https://tools.wmflabs.org/reasonator';

	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @return Wikidata the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Wikidata';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['property', 'safe', 'on' => 'search'],
			['propertyType', 'safe', 'on' => 'search'],
			['comparison', 'safe', 'on' => 'search'],
			['value', 'safe', 'on' => 'search'],
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'revue' => [self::BELONGS_TO, 'Revue', 'revueId'],
			'titre' => [self::BELONGS_TO, 'Titre', 'titreId'],
			'suivis' => [
				self::HAS_MANY,
				'Suivi', // in the SQL, aliased to the relation's name
				['cibleId' => 'revueId'], // foreign column => our column (witn a string instead, our PK would be implicit)
				'on' => "suivis.cible = 'Revue'", // additional condition added to the ON
				'select' => "suivis.partenaireId", // fetch less data than *
			],
		];
	}

	public function getRevueLink()
	{
		if (isset($this->revue)) {
			return $this->revue->selfLink;
		}
		return sprintf('revue-%d disparue', $this->revueId);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'revueId' => 'Revue',
			'titreId' => 'Titre',
			'qId' => 'QID',
			'property' => 'Propriété',
			'propertyType' => 'Type propriété',
			'value' => 'Valeur', //valeur brute
			'url' => 'URL',
			'hdate' => "Dernier import",
			'comparison' => "Comparaison",
			'compUrl' => "Url Mir@bel",
			'compDetails' => "Détails",
			'compDate' => "Date comparaison",
		];
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @param int|bool $pageSize Number of items for pagination (false to disable pagination).
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($pageSize=100)
	{
		$criteria = new CDbCriteria;
		$criteria->with = ['suivis'];

		$criteria->compare('id', $this->id);
		$criteria->compare('revueId', $this->revueId);
		$criteria->compare('titreId', $this->titreId);
		$criteria->compare('qId', $this->qId);
		if (empty($this->property)) {
			$criteria->addCondition('0=1');
		} else {
			$criteria->compare('property', $this->property);
		}
		$criteria->compare('propertyType', $this->propertyType);
		$criteria->compare('LOWER(value)', strtolower($this->value), true);
		$criteria->compare('url', $this->url, true);
		$criteria->compare('hdate', $this->hdate);
		$criteria->compare('comparison', $this->comparison);
		$criteria->compare('compUrl', $this->compUrl, true);
		$criteria->compare('compDate', $this->compDate, true);
		$criteria->compare('propertyType', '> 0');

		$sort = new CSort();
		$sort->attributes = [
			'id', 'revueId', 'titreId', 'qId', 'property', 'propertyType',  'value', 'url', 'hdate',
			'comparison', 'compUrl', 'compDate',
		];
		$sort->defaultOrder = sprintf(
			"propertyType ASC, property ASC, FIND_IN_SET(comparison, '%s'), revueId ASC",
			Compare::COMP_DEFAULT_ORDER
		);
		return new CActiveDataProvider(
			$this,
			[
				'criteria' => $criteria,
				'pagination' => ['pageSize' => $pageSize],
				'sort' => $sort,
			]
		);
	}

	/**
	 * Liste les incohérences détectées entre Wikidata et Mir@bel
	 * @param int $pageSize
	 * @return \CActiveDataProvider
	 */
	public function searchInconsistencies($pageSize = 100)
	{
		$criteria = new CDbCriteria;

		$criteria->compare('property', 'P1476'); // Titre, NB. choix arbitraire de la propriété support des erreurs graves
		$criteria->compare('comparison', $this->comparison);
		$criteria->compare('comparison', '> 1');
		$criteria->compare('LOWER(value)', strtolower($this->value), true);
		$sort = new CSort();
		$sort->attributes = [
			'id', 'revueId', 'titreId', 'qId', 'property', 'propertyType',  'value', 'url', 'hdate',
			'comparison', 'compUrl', 'compDate',
		];
		$sort->defaultOrder = 'comparison DESC, revueId ASC';

		return new CActiveDataProvider(
			$this,
			[
				'criteria' => $criteria,
				'pagination' => ['pageSize' => $pageSize],
				'sort' => $sort,
			]
		);
	}

	/**
	 * return property code and name
	 * @return string
	 */
	public function getExplicitProperty()
	{
		$properties = Compare::getWdProperties();
		if (isset($properties[$this->property])) {
			return sprintf('%s %s', $this->property, $properties[$this->property]);
		}
		return $this->property;
	}

	/**
	 * return a complete link from the url
	 * @return string
	 */
	public function getUrlLink(): string
	{
		if (isset($this->url)) {
			return CHtml::link($this->url, $this->url);
		}
		return '';
	}

	/**
	 *
	 * @param bool $link  true=>link, false=>url only
	 * @return string
	 */
	public function getQidLink($link=true): string
	{
		$url = sprintf('%s/%s', self::WD_BASE_URL, $this->qId);
		if ($link) {
			return CHtml::link($this->qId, $url);
		}
		return $url;
	}

	/**
	 *
	 * @param string $lang alpha2
	 * @param bool $link true=>link, false=>url only
	 * @return string
	 */
	public function getReasonatorLink($lang='fr', $link=true): string
	{
		$url = sprintf('%s/?q=%s&lang=%s', self::REASONATOR_BASE_URL, $this->qId, $lang);
		if ($link) {
			return CHtml::link($this->qId, $url);
		}
		return $url;
	}

	/**
	 * Est-ce que cette entrée de cache Wikidata est comparable avec la donnée Mir@bel ?
	 * @return bool
	 */
	public function isComparable(): bool
	{
		return (bool) $this->comparison;
	}

	/**
	 * Est-ce que cette entrée de cache devrait proposer l'édition du lien extérieur ? (absent ou différent)
	 * @return bool
	 */
	public function isEditable(): bool
	{
		return ((bool) $this->comparison
				&& $this->comparison < Compare::COMP_ISSN_INCONSISTENT
				&& Compare::COMP_LEVELS[$this->comparison] != 'success'
				&& $this->titreId > 0
				);
	}

	/**
	 *
	 * @param int $revueId
	 * @return bool  (false si la table Wikidata n'existe pas)
	 */
	public static function isRevuePresent(int $revueId): bool
	{
		try {
			return (bool) Yii::app()->db
				->createCommand("SELECT 1 FROM Wikidata WHERE revueId = ? LIMIT 1")
				->queryScalar([$revueId]);
		} catch (\Exception $_) {
			return false;
		}
	}

	/**
	 *
	 * @return string
	 */
	public function getComparisonText(): string
	{
		return Compare::getCompCode($this->comparison);
	}

	/**
	 * comparison level used as html class
	 * @return string ('success', 'warning', 'error', 'info')
	 */
	public function getComparisonLevel(): string
	{
		return Compare::getCompLevel($this->comparison);
	}

	/**
	 * get a count of records by comparison status for a given property
	 * @param string $property
	 * @return array
	 */
	public static function getPropertyComparisonStats(string $property): array
	{
		$sql = "SELECT comparison, COUNT(id) AS cnt "
				. "FROM Wikidata WHERE property=? "
				. "GROUP BY comparison ORDER BY comparison DESC";
		$rows = Yii::app()->db->createCommand($sql)->queryAll(true, [$property]);
		$res = [];
		foreach ($rows as $row) {
			$res[Compare::getCompCode($row['comparison'])] = $row['cnt'];
		}
		return $res;
	}

	/**
	 * provide a structured array of Wikidata info
	 * each row is a qid + value + url
	 * @param int $revueId
	 */
	public static function getRevueWdInfo(int $revueId): array
	{
		if (!self::isRevuePresent($revueId)) {
			return [];
		}
		$res = [];

		$propname = Compare::getIncomparableProperties();
		$sqlProp = "SELECT qId, property, value, url "
				. "FROM Wikidata "
				. "WHERE property LIKE 'P%' AND revueId=? ORDER BY qId";
		$rows = Yii::app()->db->createCommand($sqlProp)->queryAll(true, [$revueId]);
		foreach ($rows as $row) {
			if (!isset($propname[$row['property']])) {
				continue;
			}
			$res[] = [
				'rowhead' => sprintf('%s (%s)', $propname[$row['property']], $row['property']), // eg. 'Twitter'
				'qidLink' => CHtml::link($row['qId'], sprintf('%s/%s', self::WD_BASE_URL, $row['qId'])),
				'value' => $row['value'],
				'url' => $row['url'],
			];
		}
		return $res;
	}

	/**
	 * Returns a list of Partenaire linked to this Wikidata entry (through Suivi).
	 *
	 * @return Partenaire[]
	 */
	public function getPartenairesSuivant(): array
	{
		return Partenaire::model()->findAllBySql(
			"SELECT p.* "
			. "FROM Partenaire p JOIN Suivi s ON p.id = s.partenaireId "
			. "WHERE s.cible = 'Revue' AND s.cibleId = {$this->revueId} "
			. "ORDER BY p.nom ASC"
		);
	}

	public function getSuiviLevel(): string
	{
		$suivis = array_map(
			function ($x) {
				return (int) $x->partenaireId;
			},
			$this->suivis
		);
		if (empty($suivis)) {
			return '-';
		}
		if (in_array((int) Yii::app()->user->partenaireId, $suivis, true)) {
			return 'moi';
		}
		return 'autre';
	}
}
