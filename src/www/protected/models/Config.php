<?php

/**
 * This is the model class for table "Config".
 *
 * The followings are the available columns in table 'Config':
 * @property int $id
 * @property string $key
 * @property string $value
 * @property string $type
 * @property string $category
 * @property string $description
 * @property bool $required If true, the value must be modified after its creation
 * @property int $createdOn timestamp
 * @property int $updatedOn timestamp
 */
class Config extends CActiveRecord
{
	public const ENUM_TYPE = [
		'assoc' => 'clé/valeur',
		'boolean' => 'booléen',
		'csv' => 'tableau CSV',
		'integer' => 'integer',
		'float' => 'flottant',
		'string' => 'texte court',
		'text' => 'texte long',
		'html' => 'HTML',
		'json' => 'JSON',
	];

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Config';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['key', 'required'],
			['key, category', 'length', 'max' => 255],
			['value', 'safe'],
			['description', 'length', 'max' => 4096],
			['type', 'in', 'range' => array_keys(self::ENUM_TYPE)],
			['createdOn',
				'default', 'value' => $_SERVER['REQUEST_TIME'],
				'setOnEmpty' => false, 'on' => 'insert', ],
			['updatedOn',
				'default', 'value' => $_SERVER['REQUEST_TIME'],
				'setOnEmpty' => false, 'on' => 'update', ],
			// The following rule is used by search().
			['key, value, type, category, description, createdOn, updatedOn', 'safe', 'on' => 'search'],
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'key' => 'Clé',
			'value' => 'Valeur',
			'type' => 'Type',
			'category' => 'Thème',
			'description' => 'Description',
			'createdOn' => "Création",
			'updatedOn' => "Modification",
		];
	}

	/**
	 * Declares attribute hints.
	 */
	public function attributeHints()
	{
		return [
			'key' => "Nom unique du champ",
			'value' => "",
			'type' => "",
			'category' => "",
			'description' => "",
		];
	}

	public function beforeSave()
	{
		if (!$this->isNewRecord) {
			$this->updatedOn = time();
		}
		return parent::beforeSave();
	}

	/**
	 * Return the value of this parameter, read from the config files, then from the DB.
	 * Null if not present.
	 *
	 * @param string $name
	 * @return mixed
	 */
	public static function read(string $name)
	{
		/**
		 * @todo Optimize by querying an in-memory cache first
		 *   In this case, afterSave() should update the cached value.
		 */

		// if defined, return the param value from the config file
		$fromParams = Yii::app()->params->itemAt($name);
		if ($fromParams) {
			return $fromParams;
		}
		// else read and decode the value from the DB Config table
		$c = Config::model()->findByAttributes(['key' => $name]);
		if ($c) {
			return $c->decode();
		}
		return null;
	}

	/**
	 * Write into the DB the raw value
	 *
	 * @param string $name
	 * @param string $value
	 * @return bool
	 */
	public static function writeRaw(string $name, string $value): bool
	{
		$c = Config::model()->findByAttributes(['key' => $name]);
		if (!$c) {
			throw new \Exception("Erreur dans le code ou les données de config : la clé '$name' est inconnue.");
		}
		$c->value = $value;
		return $c->save();
	}

	/**
	 * Return the decoded value.
	 *
	 * @return mixed
	 */
	public function decode()
	{
		switch ($this->type) {
			case 'assoc':
				$kv = [];
				foreach (Tools::readCsv($this->value, ";") as $row) {
					list($k, $v) = $row;
					$kv[trim($k)] = trim($v);
				}
				return $kv;
			case 'boolean':
				return (boolean) $this->value;
			case 'csv':
				return Tools::readCsv($this->value, ";");
			case 'integer':
				return (int) $this->value;
			case 'float':
				return (double) $this->value;
			case 'string':
				return $this->value;
			case 'json':
				return json_decode($this->value);
			default:
				return $this->value;
		}
	}

	/**
	 * Count the required fields that are not filled.
	 *
	 * @return int
	 */
	public static function countRequired()
	{
		return (int) Yii::app()->db
			->createCommand("SELECT count(*) FROM Config WHERE required=1 AND updatedOn IS NULL")
			->queryScalar();
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @return CActiveDataProvider
	 */
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('`key`', $this->key, true);
		$criteria->compare('value', $this->value, true);
		$criteria->compare('type', $this->type);
		if ($this->category === 'autres') {
			$criteria->addNotInCondition('category', ['alerte', 'email', 'maintenance', 'sherpa']);
		} else {
			$criteria->compare('category', $this->category);
		}
		$criteria->compare('description', $this->description, true);
		$criteria->compare('createdOn', $this->createdOn);
		$criteria->compare('updatedOn', $this->updatedOn);

		$sort = new CSort();
		$sort->attributes = ['key', 'value', 'type', 'category', 'createdOn', 'updatedOn'];
		$sort->defaultOrder = '`category` ASC, `key` ASC';

		return new CActiveDataProvider(
			$this,
			[
				'criteria' => $criteria,
				'pagination' => false,
				'sort' => $sort,
			]
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Config the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
