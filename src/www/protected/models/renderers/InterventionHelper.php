<?php

namespace models\renderers;

use CHtml;
use Intervention;
use Yii;

/**
 * This is a rendering helper for the Intervention model.
 */
class InterventionHelper
{
	private const ANTISPAM = " verbatim &tchaa%";

	/**
	 * @var Intervention
	 */
	private $model;

	public function __construct(Intervention $intervention)
	{
		$this->model = $intervention;
	}

	public function computeSpamKey(string $str): string
	{
		return md5($str . self::ANTISPAM);
	}

	public function errors(): array
	{
		$errors = [];
		foreach ($this->model->getErrors() as $k => $e) {
			if ($k !== 'contenuJson') {
				$errors[$k] = join(" + ", $e);
			}
		}
		$hasdetailedError = false;
		foreach ($this->model->contenuJson->getErrors() as $k => $e) {
			if (is_int($k)) {
				if (!$hasdetailedError) {
					$errors[] = is_array($e) ? join(" + ", $e) : $e;
				}
			} else {
				$errors[$k] = is_array($e) ? join(" + ", $e) : $e;
				$hasdetailedError = true;
			}
		}
		return $errors;
	}

	public function getRandomAntispam($new = false): string
	{
		static $answer = null;
		if (isset($answer) && !$new) {
			return $answer;
		}
		$a = rand(10, 99);
		$b = rand(1, min([$a, 10]));
		if (rand(1, 2) == 1) {
			$this->model->spam1 = $this->computeSpamKey((string) ($a + $b));
			$answer = "Combien font <code>$a + $b</code> ?";
		} else {
			$this->model->spam1 = $this->computeSpamKey((string) ($a - $b));
			$answer =  "Combien font <code>$a - $b</code> ?";
		}
		return $answer;
	}

	public function summary(): string
	{
		if (empty($this->model->revueId) || empty($this->model->contenuJson)) {
			return "";
		}
		$actionDescription = [
			'service-D' => 'accès : suppression',
			'service-U' => 'accès : mise à jour',
			'editeur-C' => 'éditeur : création',
			'editeur-D' => 'éditeur : suppression',
			'editeur-U' => 'éditeur : mise à jour',
			'collection-C' => 'collection : création',
			'collection-D' => 'collection : suppression',
			'collection-U' => 'collection : mise à jour',
			'ressource-C' => 'ressource : création',
			'ressource-D' => 'ressource : suppression',
			'ressource-U' => 'ressource : mise à jour',
			'revue-C' => 'revue : création',
			'revue-D' => 'revue : suppression',
			'revue-U' => 'revue : mise à jour',
			'revue-I' => 'thématique : affectation',
		];
		if ($this->model->action && isset($actionDescription[$this->model->action])) {
			return $actionDescription[$this->model->action];
		}
		return $this->model->contenuJson->getSummary();
	}

	/**
	 * Returns a HTML link toward this object.
	 *
	 * @return string HTML link.
	 */
	public function link()
	{
		if (empty($this->model->id)) {
			Yii::log("Intervention : affichage d'un lien vers une intervention sans ID", CLogger::LEVEL_WARNING);
			return "<span>Intervention erronée</span>";
		}
		$title = $this->model->description;
		$url = '';
		if (!empty($this->model->titreId) && ($titre = \Titre::model()->findByPk($this->model->titreId))) {
			if (!$title) {
				$title = $titre->getFullTitle();
			}
			$url = Yii::app()->createUrl('/revue/view', ['id' => $this->model->revueId]);
		} elseif (isset($this->model->revueId) && ($revue = \Revue::model()->findByPk($this->model->revueId))) {
			if (!$title) {
				$title = $revue->getFullTitle();
			}
			$url = Yii::app()->createUrl('/revue/view', ['id' => $this->model->revueId]);
		} elseif (isset($this->model->ressourceId)) {
			$ressource = $this->model->ressource;
			if ($ressource) {
				if (!$title) {
					$title = $this->model->ressource->getFullName() . " (R)";
				}
				$url = Yii::app()->createUrl('/ressource/view', ['id' => $this->model->ressourceId]);
			} else {
				$title = "*ressource supprimée*";
			}
		} elseif (isset($this->model->editeurId)) {
			$editeur = $this->model->editeur;
			if ($editeur) {
				if (!$title) {
					$title = $this->model->editeur->getFullName() . " (E)";
				}
				$url = $this->model->editeur->getSelfUrl();
			} else {
				$title = "*éditeur supprimé*";
			}
		} else {
			Yii::log("Intervention : aucune table principale liée à {$this->model->id}", \CLogger::LEVEL_WARNING);
			if (!$title) {
				$title = "???";
			}
		}
		if (Yii::app()->user->isGuest) {
			if ($url) {
				return CHtml::link(CHtml::encode($title), $url);
			}
			return CHtml::encode($title);
		}
		return CHtml::link(CHtml::encode($title), ['/intervention/view', 'id' => $this->model->id]);
	}

	/**
	 * Returns the Intervention records that match the Suivi rights, in the last 100 days.
	 *
	 * @param int|null $partenaireId If NULL, return intv for object that have no "Suivi" record.
	 * @param string|null $status
	 * @param int $since (opt) timestamp (seconds)
	 * @param int $limit (opt)
	 * @return Intervention[]
	 */
	public static function listInterventionsSuivi(?int $partenaireId, ?string $status, int $since = 0, int $limit = 10): array
	{
		$floorTime = $since ?: mktime(0, 0, 0) - 90 * 86400; // last 90 days
		$pId = $partenaireId ?: 'NULL';
		$left = $partenaireId ? "" : "LEFT";
		$lim = $limit > 0 ? (int) $limit : 10;
		if ($status === null) {
			$cond = "";
			$sort = "ORDER BY hdateProp DESC";
		} elseif ($status === 'attente') {
			$cond = " AND i.statut = 'attente'";
			$sort = "ORDER BY hdateProp DESC";
		} elseif (in_array($status, ['accepté', 'refusé'], true)) {
			$cond = " AND i.statut = '{$status}'";
			$sort = "ORDER BY hdateVal DESC";
		} else {
			throw new \Exception("Invalid parameter. Input was not sanitized.");
		}

		$sql = <<<EOSQL
			SELECT i.*
			FROM Intervention i
			$left JOIN Suivi s ON s.cible = 'Revue' AND i.revueId = s.cibleId
			WHERE s.partenaireId <=> $pId AND hdateProp > $floorTime $cond

			UNION

			SELECT i.*
			FROM Intervention i
			$left JOIN Suivi s ON s.cible = 'Ressource' AND i.ressourceId = s.cibleId
			WHERE s.partenaireId <=> $pId AND hdateProp > $floorTime $cond

			UNION

			SELECT i.*
			FROM Intervention i
			$left JOIN Suivi s ON s.cible = 'Editeur' AND i.editeurId = s.cibleId
			WHERE s.partenaireId <=> $pId AND hdateProp > $floorTime $cond

			$sort
			LIMIT $lim
			EOSQL;
		return Intervention::model()->findAllBySql($sql);
	}

	/**
	 * Returns the Intervention records on the Editeur table.
	 *
	 * @param string $status
	 * @param int $limit (opt)
	 * @return Intervention[]
	 */
	public static function listInterventionsEditeurs(?string $status, int $limit = 10): array
	{
		$sql = "SELECT i.* "
			. "FROM Intervention i "
			. "WHERE editeurId IS NOT NULL";
		if ($status === null) {
			$sql .= " ORDER BY i.hdateProp DESC";
		} elseif ($status === 'attente') {
			$sql .= " AND i.statut = 'attente' ORDER BY i.hdateProp DESC";
		} elseif (in_array($status, ['accepté', 'refusé'], true)) {
			$sql .= " AND i.statut = '{$status}' ORDER BY i.hdateVal DESC";
		}
		$sql .= " LIMIT " . (int) $limit;
		return Intervention::model()->findAllBySql($sql);
	}

	/**
	 * Returns the Intervention records on the Editeur table.
	 *
	 * @param ?string $status
	 * @param int $limit (opt)
	 * @return Intervention[]
	 */
	public static function listInterventionsPartenairesEditeurs(?string $status, int $limit = 10): array
	{
		$sql = "SELECT i.* "
			. "FROM Intervention i JOIN Utilisateur u ON utilisateurIdProp = u.id JOIN Partenaire p ON p.id = u.partenaireId"
			. " WHERE p.type = 'editeur'";
		if ($status === null) {
			$sql .= " ORDER BY i.hdateProp DESC";
		} elseif ($status === 'attente') {
			$sql .= " AND i.statut = 'attente' ORDER BY i.hdateProp DESC";
		} elseif (in_array($status, ['accepté', 'refusé'], true)) {
			$sql .= " AND i.statut = '{$status}' ORDER BY i.hdateVal DESC";
		}
		$sql .= " LIMIT " . (int) $limit;
		return Intervention::model()->findAllBySql($sql);
	}

	/**
	 * Returns the HTML listing the last validated interventions.
	 *
	 * @param int $num
	 * @return string HTML list.
	 */
	public static function listLast(int $num): string
	{
		$limit = (int) $num;
		if (!$limit) {
			return '';
		}
		$floorTime = mktime(0, 0, 0) - 5184000; // 60 last days
		$list = Intervention::model()->findAllBySql(
			"SELECT i.* FROM Intervention i "
			. "JOIN (SELECT titreId, editeurId, ressourceId, max(hdateVal) as hdateVal "
			. "FROM Intervention i "
			. "WHERE i.statut = 'accepté' AND (i.action IS NULL OR i.action <> 'revue-C') "
			. " AND hdateVal > $floorTime "
			. "GROUP BY titreId, editeurId, ressourceId "
			. "ORDER BY hdateVal DESC LIMIT $limit) "
			. " AS t ON (i.titreId <=> t.titreId AND i.editeurId <=> t.editeurId AND i.ressourceId <=> t.ressourceId AND i.hdateVal = t.hdateVal) "
			. " WHERE i.statut = 'accepté' "
			. " ORDER BY i.hdateVal DESC, id DESC"
		);
		if (!$list) {
			$titles = ['Aucune modification'];
		} else {
			$titles = [];
			foreach ($list as $i) {
				$titles[] = '<li class="' . ($i->suivi ? 'suivi-other' : 'suivi-none') . '">'
					. $i->render()->link()
					. "</li>\n";
			}
		}
		return '<ul>' . join('', $titles) . '</ul>';
	}

	/**
	 * Returns the HTML listing the last validated interventions that created a Revue.
	 *
	 * @param int $num
	 * @return string HTML list.
	 */
	public static function listLastJournalCreations(int $num): string
	{
		$limit = (int) $num;
		if (!$limit) {
			return '';
		}
		$floorTime = mktime(0, 0, 0) - 10368000; // 120 last days
		$list = \Titre::model()->findAllBySql(
			"SELECT t.*, i.suivi AS suivre "
			. "FROM Intervention i "
			. " JOIN Titre t ON t.revueId = i.revueId AND t.obsoletePar IS NULL"
			. " WHERE i.statut = 'accepté' AND i.action <=> 'revue-C' "
			. "  AND i.hdateVal > $floorTime "
			. "GROUP BY i.titreId "
			. "ORDER BY i.hdateVal DESC LIMIT $limit"
		);
		if (!$list) {
			$titles = ['Aucune création de revue'];
		} else {
			$titles = [];
			foreach ($list as $t) {
				$titles[] = '<li class="' . ($t->suivre ? 'suivi-other' : 'suivi-none') . '">'
					. $t->getSelfLink()
					. "</li>\n";
			}
		}
		return '<ul>' . join('', $titles) . '</ul>';
	}
}
