<?php

namespace models\lang;

class Convert
{
	/**
	 * Converts a string of codes, separated par commas, into a string of languages names.
	 *
	 * @param string $joinedCodes
	 * @return string Joined names.
	 */
	public static function codesToFullNames(string $joinedCodes): string
	{
		$codes = preg_split('/[\s,]+/', $joinedCodes, -1, PREG_SPLIT_NO_EMPTY);
		$names = array_filter(array_map([self::class, 'codeToFullName'], $codes));
		return join(', ', $names);
	}

	/**
	 * Converts a single code into a language name.
	 *
	 * @param string $code
	 * @return string name or ""
	 */
	public static function codeToFullName(string $code): string
	{
		$clean = trim($code, ' ,');
		$c = Codes::ALIASES[$clean] ?? $clean;
		if (strlen($c) === 2) {
			if (isset(Codes::CODES2[$c])) {
				return Codes::CODES2[$c];
			}
		} elseif (strlen($c) === 3) {
			if (isset(Codes::CODES3[$c])) {
				return Codes::CODES3[$c];
			}
		}
		return "";
	}
}
