<?php

namespace models\lang;

class Normalize
{
	/**
	 * @param string $langs
	 * @param string $separator For joining strings
	 * @return string list of ISO 639-3
	 */
	public static function fromStr(string $langs, string $separator = ', '): string
	{
		return self::fromArr(
			preg_split('/[, ]\s*/', trim($langs), -1, PREG_SPLIT_NO_EMPTY),
			$separator
		);
	}

	/**
	 * @param array $langs
	 * @param string $separator For joining strings
	 * @return string list of ISO 639-3
	 */
	public static function fromArr(array $langs, string $separator = ', '): string
	{
		return join($separator, self::filter($langs));
	}

	public static function filter(array $langs): array
	{
		$result = [];
		foreach ($langs as $lang) {
			if ($lang === 'und' || $lang === 'mul') {
				continue;
			}
			if (isset(Codes::ALIASES[$lang])) {
				$result[] = Codes::ALIASES[$lang];
			} elseif (strlen($lang) === 3) {
				if (isset(Codes::CODES3[$lang])) {
					$result[] = $lang;
				}
			} elseif (strlen($lang) === 2) {
				if (isset(Codes::CODES2TO3[$lang])) {
					$result[] = Codes::CODES2TO3[$lang];
				}
			}
		}
		return $result;
	}
}
