<?php

/**
 * This is the model class for table "Ressource".
 *
 * The followings are the available columns in table 'Ressource':
 * @property int $id
 * @property string $nom
 * @property string $identifiant
 * @property string $prefixe
 * @property string $sigle
 * @property string $description
 * @property string $type
 * @property string $acces
 * @property string $url
 * @property string $logoUrl
 * @property string $diffuseur
 * @property string $partenaires
 * @property int $partenairesNb
 * @property string $disciplines
 * @property int $revuesNb
 * @property int $articlesNb
 * @property bool $alerteRss
 * @property bool $alerteMail
 * @property bool $exportPossible
 * @property bool $indexation
 * @property int $exhaustif
 * @property string $noteContenu
 * @property string $importIdentifications
 * @property bool $autoImport
 * @property bool $partenaire
 * @property int $hdateVerif timestamp
 * @property int $hdateModif timestamp
 *
 * @property Collection[] $collections
 * @property Identification[] $identifications
 * @property Intervention[] $interventions
 * @property Service[] $services
 */
class Ressource extends AMonitored implements IWithIndirectSuivi, WithCheckLinks
{
	public const EXHAUSTIF_NON = 0;

	public const EXHAUSTIF_OUI = 1;

	public const EXHAUSTIF_INDET = 2;

	public static $enumExhaustif = [
		self::EXHAUSTIF_INDET => '?',
		self::EXHAUSTIF_NON => 'non',
		self::EXHAUSTIF_OUI => 'oui',
	];

	public static $enumType = [
		'Inconnu' => 'Inconnu',
		'Bouquet' => 'Bouquet de revues',
		'Archive' => 'Archives de revues',
		'Sommaires' => 'Base de sommaires',
		'Biblio' => 'Base de bibliographie',
		'Catalogue' => 'Catalogue éditeur',
		'SiteWeb' => 'Site web',
	];

	public static $enumAcces = [
		'Inconnu' => 'Inconnu', 'Libre' => 'Libre', 'Mixte' => 'Mixte', 'Restreint' => 'Restreint',
	];

	public $confirm;

	/**
	 * Imports external methods into this class.
	 */
	public function behaviors()
	{
		return [
			'timestampToDate' => [
				'class' => 'application.models.behaviors.TimestampToDate',
				'fieldsDateTime' => ['hdateModif', 'hdateVerif'],
			],
			'confirmDuplicate' => [
				'class' => 'application.models.behaviors.ConfirmDuplicate',
				'field' => 'nom',
			],
		];
	}

	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @return Ressource the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Ressource';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['nom, type, acces, url', 'required'],
			['partenairesNb, exhaustif', 'numerical', 'integerOnly' => true],
			['alerteRss, alerteMail, exportPossible, indexation, autoImport, partenaire', 'boolean'],
			['nom, sigle, diffuseur, disciplines, importIdentifications', 'length', 'max' => 255],
			['partenaires', 'length', 'max' => 1024],
			['revuesNb, articlesNb', 'length', 'max' => 100],
			['description, noteContenu', 'length', 'max' => 65535],
			['url', 'url', 'allowEmpty' => true],
			['url', 'unique'],
			['url', 'length', 'max' => 512],
			['url', 'ext.validators.UrlFetchableValidator', 'on' => 'insert update'],
			['prefixe', 'length', 'max' => 25],
			['type', 'in', 'range' => array_keys(self::$enumType)], // enum
			['acces', 'in', 'range' => array_keys(self::$enumAcces)], // enum
			['confirm', 'boolean'],
			['exhaustif', 'default', 'value' => self::EXHAUSTIF_INDET, 'except' => 'search', 'setOnEmpty' => true],
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			[
				'nom, description, type, acces, diffuseur, partenaires, partenairesNb, '
				. 'disciplines, revuesNb, articlesNb, alerteRss, alerteMail, exportPossible, '
				. 'indexation, autoImport, hdateVerif, hdateModif',
				'safe',
				'on' => 'search',
			],
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'collections' => [
				self::HAS_MANY,
				'Collection',
				'ressourceId',
				'order' => 'nom',
				'index' => 'id',
			],
			'identifications' => [self::HAS_MANY, 'Identification', 'ressourceId'],
			'interventions' => [self::HAS_MANY, 'Intervention', 'ressourceId'],
			'services' => [self::HAS_MANY, 'Service', 'ressourceId'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'nom' => 'Nom',
			'prefixe' => 'Préfixe',
			'sigle' => 'Sigle',
			'description' => 'Description',
			'type' => 'Type',
			'acces' => 'Type d\'accès',
			'url' => 'Adresse web',
			'logoUrl' => 'URL du logo',
			'diffuseur' => 'Diffuseur',
			'partenaires' => 'Partenaires',
			'partenairesNb' => 'Nombre de partenaires',
			'disciplines' => 'Disciplines',
			'revuesNb' => 'Nombre de revues',
			'articlesNb' => 'Nombre d\'articles',
			'alerteRss' => 'Flux RSS',
			'alerteMail' => 'Alerte courriel',
			'exportPossible' => 'Export Possible',
			'indexation' => 'Indexation',
			'exhaustif' => "Exhaustif",
			'noteContenu' => 'Note de contenu',
			'autoImport' => 'Import auto',
			'importIdentifications' => "Méthodes d'identification de titres",
			'partenaire' => 'Partenaire Mir@bel',
			'hdateVerif' => 'Dernière vérification',
			'hdateModif' => 'Dernière modification',
			'fullName' => 'Nom complet',
			'confirm' => "Confirmer malgré l'avertissement",
			'identifiant' => 'Identifiant',
		];
	}

	/**
	 * Each array key defines a function that can be chained before a find*() method.
	 */
	public function scopes()
	{
		return [
			'sorted' => [
				'order' => 'nom ASC',
			],
		];
	}

	/**
	 * Delete related data in "Suivi".
	 *
	 * @return bool
	 */
	public function afterDelete()
	{
		if ($this->id > 0) {
			Yii::app()->db->createCommand(
				"DELETE FROM Suivi WHERE cible = '" . $this->tableName() . "' AND cibleId = " . (int) $this->id
			)->execute();
		}
		return parent::afterDelete();
	}

	/**
	 * Returns an array of parents that have a direct "Suivi": [ [table => ,  id => ], ... ]
	 *
	 * @return array
	 */
	public function listParentsForSuivi()
	{
		if (empty($this->id)) {
			return [];
		}
		return [
			['table' => 'Ressource', 'id' => $this->id],
		];
	}

	/**
	 * Returns a list of assoc (id =>, value=> , label=>) matching the requested term.
	 *
	 * @param string $term
	 * @param array $filters  array(field => array(values,to,require))
	 * @param bool $forbidAutoimport
	 * @return array Array of assoc (id=> , value=> , label=>).
	 */
	public static function completeTerm($term, $filters = [], $forbidAutoimport = false): array
	{
		if (!Yii::app()->params->contains('sphinx')) {
			return [];
		}
		Yii::import('ext.Sphinx.*');
		$sphinxClient = new Sphinx(Yii::app()->params->itemAt("sphinx"));
		$sphinxClient->indexes = 'ressources';
		$cleanTerm = str_replace(['-', '@', '+', '/'], ' ', $term);
		$sphinxClient->query = $sphinxClient->EscapeString($cleanTerm);

		$results = [];
		if ($filters) {
			foreach ($filters as $attribute => $values) {
				$sphinxClient->SetFilter($attribute, $values);
			}
		}

		$exact = self::model()->findAllByAttributes(['nom' => $term]);
		if ($exact && count($exact) === 1 && (!$filters || $exact[0]->autoImport == 0)) {
			$results[] = [
				'id' => $exact[0]->id,
				'value' => $exact[0]->getFullName(),
				'label' => $exact[0]->getFullName(),
				'forbid' => $forbidAutoimport && $exact[0]->autoImport
					? "Cette ressource est gérée automatiquement et ne peut pas être sélectionnée."
					: "",
			];
		}

		$sphinxClient->SetSortMode(SPH_SORT_EXTENDED, 'cletri ASC');
		$data = $sphinxClient->fetchData();
		if (!empty($data['matches'])) {
			foreach ($data['matches'] as $id => $row) {
				$results[] = [
					'id' => $id,
					'value' => $row['attrs']['nomcomplet'],
					'label' => $row['attrs']['nomcomplet'],
					'forbid' => $forbidAutoimport && $row['attrs']['autoimport']
						? "Cette ressource est gérée automatiquement et ne peut pas être sélectionnée."
						: "",
				];
			}
			if (count($data['matches']) < (int) $data['total_found']) {
				$results[] = [
					'id' => '',
					'value' => $term,
					'label' => '…',
				];
			}
		}
		return $results;
	}

	/**
	 * Find the instances ID that have this exact name.
	 */
	public static function findExactTerm(string $term): array
	{
		return Yii::app()->db->createCommand(
			"SELECT id FROM Ressource WHERE nom = :term"
		)->queryAll(true, [':term' => $term]);
	}

	/**
	 * Returns the full name, including the prefix with correct spacing.
	 *
	 * @codeCoverageIgnore
	 */
	public function getFullName(): string
	{
		return $this->prefixe . $this->nom
			. ($this->sigle ? ' — ' . $this->sigle : '')
			. ($this->type === 'SiteWeb' ? ' (site web)' : '')
		;
	}

	/**
	 * Returns a link toward this object.
	 *
	 * @codeCoverageIgnore
	 * @param bool $short Short title
	 * @param array $htmlOptions (opt, [])
	 * @return string HTML link.
	 */
	public function getSelfLink($short = false, $htmlOptions = [])
	{
		if ($short && $this->sigle) {
			$name = $this->sigle;
		//$options = ['title' => $this->getFullName()];
		} else {
			$name = $this->getFullName();
		}
		return CHtml::link(
			CHtml::encode($name),
			Yii::app()->createUrl('/ressource/view', ['id' => $this->id, 'nom' => Norm::urlParam($this->nom)]),
			$htmlOptions
		);
	}

	/**
	 * Returns a DataProvider object for this letter.
	 *
	 * @param string $letter In "0", "a", ..., "z"
	 * @param int $suivi (opt) Partenaire.id
	 * @param string $import (opt) '', 'ressource', 'collection'
	 * @param bool $noweb (opt, false) Exclude pure web sites.
	 * @return \SphinxDataProvider
	 */
	public static function listAlpha($letter = "0", $suivi = null, $import = '', $noweb = false)
	{
		if ($import !== 'ressource' && $import !== 'collection') {
			$import = '';
		}
		if ($suivi || $import) {
			$pager = false;
		} else {
			$pager = new PagerAlpha();
			$pager->elementName = 'ressource';
			$cond = [];
			if ($suivi > 0) {
				$cond[] = "s.partenaireId = " . ((int) $suivi);
			} elseif ($suivi < 0) {
				$cond[] = "s.partenaireId IS NOT NULL";
			}
			if ($noweb) {
				$cond[] = "r.type != 'SiteWeb'";
			}
			$joinedCond = join(' AND ', $cond);
			$sql = "SELECT FIND_IN_SET(LEFT(r.nom, 1), "
					. "'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z') AS rank, "
					. "COUNT(DISTINCT r.id) AS num "
					. "FROM Ressource r "
					. ($suivi ? "JOIN Suivi s ON (s.cible='Ressource' AND s.cibleId=r.id) " : "")
					. ($cond ? "WHERE $joinedCond " : '')
					. "GROUP BY rank";
			$pager->setLettersFilter(Tools::sqlToPairs($sql));
			$pager->setCurrentLetter($letter);
		}

		Yii::import('ext.Sphinx.*');
		$sphinxClient = new Sphinx(Yii::app()->params->itemAt("sphinx"));
		$sphinxClient->indexes = 'ressources';
		if ($suivi > 0) {
			$sphinxClient->SetFilter('suivi', [(int) $suivi]);
		} elseif ($suivi < 0) {
			$sphinxClient->SetFilterRange('suivi', 1, 99999);
		} elseif ($import === 'ressource') {
			$sphinxClient->SetFilter('autoimport', [true]);
		} elseif ($import === 'collection') {
			$sphinxClient->SetFilter('autoimportcollections', [0], true); // exclude those with 0 import collections
		} else {
			$sphinxClient->SetFilter('lettre1', [$pager->getCurrentPage()]);
		}
		if ($noweb) {
			if (!$suivi) {
				$sphinxClient->SetFilter('web', [0]);
			}
			if ($pager) {
				$pager->urlDefaultParameters = ['noweb' => 1];
			}
		}
		//echo "<pre>"; print_r($sphinxClient); die();

		$sort = new SphinxSort([]);
		$sort->defaultOrder = 'cletri ASC';

		return new SphinxDataProvider(
			$sphinxClient,
			null, // no ActiveRecord models
			[
				'pagination' => $pager,
				'sort' => $sort,
			]
		);
	}

	/**
	 * Returns the number of 'Titre' linked to this through 'Service'.
	 */
	public function countTitres(): int
	{
		$sql = "SELECT COUNT(DISTINCT s.titreId)"
			. " FROM Service s"
			. " WHERE s.ressourceId = " . $this->id;
		return Yii::app()->db->createCommand($sql)->queryScalar();
	}

	/**
	 * Returns the number of 'Revue' linked to this through 'Service'.
	 */
	public function countRevues():int
	{
		$sql = "SELECT COUNT(DISTINCT t.revueId) "
			. "FROM Service s JOIN Titre t ON (s.titreId = t.id) "
			. "WHERE t.statut = 'normal' AND s.ressourceId = " . $this->id;
		return Yii::app()->db->createCommand($sql)->queryScalar();
	}

	/**
	 * Returns a list of Partenaire linked to this (through Suivi).
	 *
	 * @return Partenaire[]
	 */
	public function getPartenairesSuivant(): array
	{
		if (!$this->id) {
			return [];
		}
		return Partenaire::model()->findAllBySql(
			"SELECT p.* FROM Partenaire p JOIN Suivi s ON s.partenaireId = p.id "
			. "WHERE s.cible = 'Ressource' AND s.cibleId = {$this->id}"
		);
	}

	/**
	 * Builds an Intervention object that can be completed later.
	 */
	public function buildIntervention(bool $direct): Intervention
	{
		$i = parent::buildIntervention($direct);
		if (isset($this->id)) {
			$i->ressourceId = $this->id;
			$i->action = 'ressource-U';
		} else {
			$i->action = 'ressource-C';
		}
		$i->suivi = null !== Suivi::isTracked($this);
		return $i;
	}

	/**
	 * Helper function that returs an IMG if possible.
	 *
	 * @codeCoverageIgnore
	 * @param bool $reduced If false, do not use the resized image but the original image.
	 * @param bool $forceRefresh
	 * @return string HTML img or plain text.
	 */
	public function getLogoImg(bool $reduced = true, $forceRefresh = false) : string
	{
		$url = $this->getLogoUrl($reduced);
		if (empty($url)) {
			return CHtml::encode($this->nom);
		}
		return CHtml::image($url, "logo " . $this->nom);
	}

	/**
	 * Helper function that returs an IMG URL if possible.
	 *
	 * @param bool $reduced If false, do not use the resized image but the original image.
	 * @param bool $forceRefresh
	 * @return string
	 */
	public function getLogoUrl(bool $reduced = true, $forceRefresh = false): string
	{
		$url = '';
		$dir = '/images/ressources-logos/' . ($reduced ? 'h55/' : '');
		$path = Yii::getPathOfAlias('webroot') . $dir;
		if (file_exists($path . sprintf('%03d.jpg', $this->id))) {
			$url = sprintf('%s%s%03d.jpg', Yii::app()->getBaseUrl(true), $dir, $this->id);
		} elseif (file_exists($path . sprintf('%03d.png', $this->id))) {
			$url = sprintf('%s%s%03d.png', Yii::app()->getBaseUrl(true), $dir, $this->id);
		}
		if ($url && $forceRefresh) {
			$url .= '?refresh=' . rand(1000, 9999);
		}
		return $url;
	}

	/**
	 * Return the array of the collections of this Ressource, each with an added field "numRevues".
	 *
	 * @return CollectionWithNums[]
	 */
	public function getCollectionsWithNumRevues(): array
	{
		if (!$this->id) {
			return [];
		}
		return CollectionWithNums::model()->findAllBySql(
			"SELECT c.*, COUNT(DISTINCT t.id) AS numTitres, COUNT(DISTINCT t.revueId) AS numRevues "
			. "FROM Collection c"
			. " LEFT JOIN Service_Collection sc ON sc.collectionId=c.id"
			. " LEFT JOIN Service s ON s.id = sc.serviceId"
			. " LEFT JOIN Titre t ON t.id = s.titreId "
			. "WHERE c.ressourceId = {$this->id} GROUP BY c.id ORDER BY c.nom"
		);
	}

	public function hasCollections(): bool
	{
		return Collection::model()->exists('ressourceId = ' . (int) $this->id);
	}

	public function hasServices(): bool
	{
		return Service::model()->exists('ressourceId = ' . (int) $this->id);
	}

	/**
	 * Checks if the object can be deleted.
	 *
	 * @return array [bool, string]
	 */
	public function isDeletable(): array
	{
		if (!$this->id) {
			return [false, "Cette ressource n'a pas d'ID."];
		}
		$acces = Service::model()->count('ressourceId=' . $this->id);
		if ($acces > 0) {
			return [false, "Cette ressource a $acces accès en ligne."];
		}
		$numAbos = Yii::app()->db
			->createCommand("SELECT count(*) FROM Abonnement WHERE collectionId IS NULL AND ressourceId = :rid")
			->queryScalar([':rid' => $this->id]);
		if ($numAbos) {
			return [false, "Cette ressource a $numAbos abonnements de partenaires."];
		}
		$numAbosColl = Yii::app()->db
			->createCommand(
				"SELECT count(*) FROM Abonnement a JOIN Collection c ON c.id = a.collectionId"
				. " WHERE c.ressourceId = :rid"
			)->queryScalar([':rid' => $this->id]);
		if ($numAbosColl) {
			return [false, "Cette ressource a $numAbosColl abonnements à ses collections."];
		}
		$links = [];
		foreach ($this->collections as $c) {
			$links[] = "collection " . $c->nom;
		}
		foreach ($this->identifications as $i) {
			/* @var $i Identification */
			$links[] = "identification du titre " . $i->titre->getSelfLink() . ' '
				. ($i->idInterne ? "[ID interne {$i->idInterne}]" : '');
		}
		if ($links) {
			return [true, "<div>Cette ressource n'a pas d'accès en ligne, mais elle est encore référencée :<ul><li>"
				. join("</li><li>", $links) . "</li></ul></div>", ];
		}
		return [true, "Cette ressource n'est pas utilisée (ni accès, ni collections, ni issn-e, ni abonnements)."];
	}

	public function getCollectionsDiffuseur($onlyVisible = false, $excludeImport = false): array
	{
		if (!$this->id) {
			return [];
		}
		return Collection::model()->findAll([
			'condition' => "ressourceId = " . (int) $this->id
				. ($onlyVisible ? " AND visible = 1" : "")
				. ($excludeImport ? " AND importee = 0" : ""),
			'index' => 'id',
		]);
	}

	public function countAbonnes(int $type = Abonnement::ABONNE): int
	{
		if (empty($this)) {
			return 0;
		}
		return Yii::app()->db
			->createCommand(
				"SELECT count(DISTINCT a.partenaireId) FROM Abonnement a LEFT JOIN Collection c ON a.collectionId = c.id"
				. " WHERE (a.ressourceId = :rid1 OR c.ressourceId = :rid2) AND mask = :type"
			)->queryScalar([':rid1' => $this->id, ':rid2' => $this->id, ':type' => $type]);
	}

	/**
	 * @param int $type (opt, Abonnement::ABONNE)
	 * @return Abonnement[]
	 */
	public function getAbonnements($type = Abonnement::ABONNE): array
	{
		if (empty($this)) {
			return [];
		}
		$criteria = new CDbCriteria;
		$criteria->join = "JOIN Partenaire p ON p.id = partenaireId";
		$criteria->order = "mask ASC, p.nom ASC";
		$criteria->addColumnCondition(['ressourceId' => $this->id]);
		if ($type) {
			if ($type !== Abonnement::ABONNE_VIA_COLLECTION) {
				$criteria->addColumnCondition(['collectionId' => null]);
			}
			$criteria->addColumnCondition(['mask' => $type]);
		}
		return Abonnement::model()->with('partenaire')->findAll($criteria);
	}

	public function proxifyUrl(string $url, ?Partenaire $partenaire, ?AbonnementSearch $abonnement): string
	{
		if ($partenaire === null) {
			$partenaireId = (int) Yii::app()->user->getState('institute');
			if (!$partenaireId) {
				return $url;
			}
			$partenaire = Partenaire::model()->findByPk($partenaireId);
		}
		$proxifiedUrl = $url;
		if (isset($abonnement)) {
			// abonnement, so try the proxies
			if (!$abonnement->readProxy($this)) {
				return $url;
			}
			$ressourceAbonnement = $abonnement->readAbonnement($this);
			if ($ressourceAbonnement) {
				$resourceProxy = $ressourceAbonnement->proxyUrl;
				if ($resourceProxy) {
					$proxifiedUrl = str_replace('URL', urlencode($url), $resourceProxy);
				}
			}
		}
		if (!empty($partenaire->proxyUrl)) {
			$proxifiedUrl = str_replace('URL', urlencode($proxifiedUrl), $partenaire->proxyUrl);
		}
		return $proxifiedUrl;
	}

	/**
	 * Check the links of the sub-objects.
	 *
	 * @param LinkChecker|null $linkChecker
	 * @return array assoc('urls'=> , 'checked'=> )
	 */
	public function checkLinks(?LinkChecker $linkChecker): array
	{
		$urls = [];
		$checks = [
			'Ressource' => [],
			'Collection' => [],
		];

		$urls[] = $this->url;
		foreach ($this->getCollectionsDiffuseur() as $c) {
			$urls[] = $c->url;
		}
		$checks['Ressource'][$this->id] = $this;

		if ($linkChecker === null) {
			$linkChecker = new LinkChecker();
		}
		return [
			'urls' => $linkChecker->checkLinks(array_filter($urls)),
			'checked' => $checks,
		];
	}

	/**
	 * Called automatically before validate().
	 *
	 * @return bool
	 */
	protected function beforeValidate()
	{
		if (strlen($this->prefixe)) {
			$this->prefixe = str_replace(["’", "´"], "'", $this->prefixe);
			if (preg_match('/[\'-]\s*$/', $this->prefixe)) {
				$this->prefixe = trim($this->prefixe);
			} elseif (!preg_match('/[\s ]$/', $this->prefixe)) {
				$this->prefixe = trim($this->prefixe) . " ";
			}
		}
		if ($this->nom) {
			$this->nom = str_replace(["’", "´"], "'", trim($this->nom));
		}
		return parent::beforeValidate();
	}

	/**
	 * Called automatically before Save().
	 *
	 * @return bool
	 */
	protected function beforeSave()
	{
		$this->hdateModif = $_SERVER['REQUEST_TIME'];
		foreach (['alerteRss', 'alerteMail', 'exportPossible', 'indexation'] as $attr) {
			if (!isset($this->{$attr}) or $this->{$attr} === '') {
				$this->{$attr} = null;
			}
		}
		if (empty($this->logoUrl)) {
			$this->logoUrl = "";
		}
		$this->nom = Tools::normalizeText($this->nom);
		$this->description = Tools::normalizeText($this->description);
		return parent::beforeSave();
	}
}
