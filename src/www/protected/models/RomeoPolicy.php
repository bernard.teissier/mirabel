<?php

/**
 * This is the model class for table "RomeoPolicy".
 *
 * The followings are the available columns in table 'RomeoPolicy':
 * @property int $titreId PK & FK
 * @property int $submitted
 * @property int $accepted
 * @property int $published
 */
class RomeoPolicy extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @return RomeoPolicy the static model class
	 */
	public static function model($className=__CLASS__): object
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName(): string
	{
		return 'RomeoPolicy';
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels(): array
	{
		return [
			'submitted' => 'soumis',
			'accepted' => 'accepté',
			'published' => 'publié',
		];
	}

	public function format(): string
	{
		$parts = [];
		foreach (['submitted', 'accepted', 'published'] as $k) {
			if ($this->{$k} > 0) {
				$parts[] = sprintf("%s (%d)", $this->getAttributeLabel($k), $this->{$k});
			}
		}
		if (!$parts) {
			return '';
		}
		return CHtml::tag(
			'span',
			[
				'class' => 'popover-toggle',
				'data-content' => "Politique des éditeurs en matière de droits, de dépôt et d'auto-archivage :\n"
					. join("\n", $parts),
				'data-trigger' => 'hover',
				'title' => "SHERPA/RoMEO",
			],
			'<i class="icon-info-sign"> </i>'
		);
	}
}
