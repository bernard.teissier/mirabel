<?php

class ExportServices
{
	public $time;

	private $filter;

	private $services;

	/**
	 * Constructor.
	 *
	 * @param array $ids of revueId
	 * @param ServiceFilter $filter
	 */
	public function __construct($ids, ServiceFilter $filter)
	{
		$safeIds = $ids ? array_map('intval', $ids) : [];
		$this->services = $this->findServices($safeIds, $filter);
		$this->time = time();
	}

	/**
	 * Returns the CSV lines for the matching services.
	 *
	 * @param string $separator between the CSV cells
	 * @throws Exception
	 * @return string
	 */
	public function exportToCsv($separator=';')
	{
		$header = [
			'titre', 'ISSN', 'ISSN-E', 'ISSN-L', 'URL Mir@bel', 'id de la revue', 'URL du service', 'ressource', 'collection',
			'type de service', "type d'accès",
			'date barrière de début', 'début', 'début (vol)', 'début (numéro)',
			'date barrière de fin', 'fin', 'fin (vol)', 'fin (numéro)',
			'id', 'dernière modification',
		];
		$result = join($separator, $header) . "\r\n";

		if ($this->services) {
			$out = fopen('php://temp', 'w');
			foreach ($this->services as $row) {
				fputcsv($out, self::toCsvArray($row), $separator);
			}
			$result .= stream_get_contents($out, -1, 0);
			fclose($out);
		}
		return $result;
	}

	/**
	 * Returns the KBART lines for the matching services.
	 *
	 * @throws Exception
	 * @return string
	 */
	public function exportToKbart()
	{
		$groupedServices = [];
		foreach ($this->services as $row) {
			$id = $row['revueId'] . ',' . $row['ressourceId'];
			if (isset($groupedServices[$id])) {
				$groupedServices[$id][] = $row;
			} else {
				$groupedServices[$id] = [$row];
			}
		}

		$header = [
			"publication_title", "print_identifier", "online_identifier",
			"date_first_issue_online", "num_first_vol_online", "num_first_issue_online",
			"date_last_issue_online", "num_last_vol_online", "num_last_issue_online",
			"title_url", "first_author", "title_id",
			"embargo_info", "coverage_depth", "coverage_notes",  "publisher_name",
			// custom fields
			"bimpe", "resource", "collections", "access_type", "Mir_title_url", "Mir_title_id",
		];
		$result = join("\t", $header) . "\n";

		$out = fopen('php://temp', 'w');
		foreach ($groupedServices as $rowGroup) {
			$row = $rowGroup[0];
			$embargo = '';
			if ($this->filter->acces === 'Libre') {
				$embargo = $this->computeEmbargo($rowGroup[0]['dateBarrFin']);
				if ($embargo) {
					$embargo = 'P' . $embargo;
				}
			}
			$row['embargoInfo'] = $embargo;
			if (count($rowGroup) === 2) {
				$row['acces'] = 'Mixte';
				$row['dateBarrFin'] = $rowGroup[1]['dateBarrFin'];
				$row['numeroFin'] = $rowGroup[1]['numeroFin'];
			} elseif (count($rowGroup) > 2) {
				$row['acces'] = 'Mixte (many services)';
			}
			fputcsv($out, self::toKbartArray($row), "\t");
		}
		$result .= stream_get_contents($out, -1, 0);
		fclose($out);
		return $result;
	}

	/**
	 * Returns the KBART II lines for the matching services.
	 *
	 * @param bool $customFields Add some Mirabel-only fields to the header and content.
	 * @throws Exception
	 * @return string
	 */
	public function exportToKbartV2(bool $customFields = true): string
	{
		$groupedServices = [];
		foreach ($this->services as $row) {
			$id = $row['revueId'] . ',' . $row['ressourceId'];
			if (isset($groupedServices[$id])) {
				$groupedServices[$id][] = $row;
			} else {
				$groupedServices[$id] = [$row];
			}
		}

		$header = [
			"publication_title", "print_identifier", "online_identifier",
			"date_first_issue_online", "num_first_vol_online", "num_first_issue_online",
			"date_last_issue_online", "num_last_vol_online", "num_last_issue_online",
			"title_url", "first_author", "title_id",
			"embargo_info", "coverage_depth", "notes",  "publisher_name",
			"publication_type",
			"date_monograph_published_print", "date_monograph_published_online", // empty fields
			"monograph_volume", "monograph_edition", "first_editor", // empty fields
			"parent_publication_title_id", "preceding_publication_title_id", // empty fields
			"access_type",
		];
		if ($customFields) {
			array_push($header, "resource", "collections", "Mir_title_url", "Mir_title_id");
		}
		$result = join("\t", $header) . "\n";

		$out = fopen('php://temp', 'w');
		foreach ($groupedServices as $rowGroup) {
			foreach ($rowGroup as $row) {
				$kbartRow = self::toKbartv2Array($row, $customFields);
				fputcsv($out, $kbartRow, "\t");
			}
		}
		$result .= stream_get_contents($out, -1, 0);
		fclose($out);
		return $result;
	}

	/**
	 * Finds the records in Service matching the ids and the filter.
	 *
	 * @param array $ids of revueId (safe)
	 * @param ServiceFilter $filter
	 * @return CDbDataReader
	 */
	protected function findServices($ids, ServiceFilter $filter)
	{
		$this->filter = $filter;
		$cond = [];
		if ($filter->baconDoaj) {
			$cond = [
				"s.acces = 'Libre'",
				"s.type = 'Intégral'",
				"s.lacunaire = 0",
				"s.selection = 0",
				"t.liensJson LIKE '%\"src\":\"DOAJ\"%'",
			];
		} elseif ($filter->baconGlobal) {
			$cond = [
				"s.type = 'Intégral'",
				"s.selection = 0",
			];
		} elseif ($filter->baconLibre) {
			$cond = [
				"s.acces = 'Libre'",
				"s.type = 'Intégral'",
				"s.lacunaire = 0",
				"s.selection = 0",
				// Cairn (3), Erudit (7), OpenEdition Journals (4), Persée (22)
				// "s.ressourceId NOT IN (3, 7, 4, 22)",
			];
		} else {
			if ($filter->acces) {
				$cond[] = "s.acces = '{$filter->acces}'";
			}
			if ($filter->type) {
				$quoter = function ($v) {
					return \Yii::app()->db->quoteValueWithType($v, PDO::PARAM_STR);
				};
				$quotedTypes = array_map($quoter, $filter->type);
				$cond[] = "s.type IN (" . join(",", $quotedTypes) . ")";
			}
			if (!$filter->lacunaire) { // filter out by default
				$cond[] = "s.lacunaire = 0";
			}
			if (!$filter->selection) { // filter out by default
				$cond[] = "s.selection = 0";
			}
		}
		if ($filter->ressourceId) {
			$cond[] = "s.ressourceId = {$filter->ressourceId}";
		}
		if ($filter->hdateModif) {
			$timestamp = strtotime($filter->hdateModif);
			if ($timestamp) {
				$cond[] = "s.hdateModif > " . $timestamp;
			}
		}

		$sql = "SELECT t.titre"
			. ", GROUP_CONCAT(DISTINCT i1.issn SEPARATOR '|') AS issn"
			. ", GROUP_CONCAT(DISTINCT i1.issnl SEPARATOR '|') AS issnl"
			. ", t.revueId"
			. ", s.* "
			. ", i.description"
			. ", GROUP_CONCAT(DISTINCT i2.issn SEPARATOR '|') AS issne"
			. ", idf.idInterne "
			. ", GROUP_CONCAT(DISTINCT CONCAT(e.prefixe, e.nom)) AS editeur "
			. ", CONCAT(r.prefixe, r.nom) AS ressource "
			. ", GROUP_CONCAT(DISTINCT c.nom SEPARATOR ' | ') AS collections "
			. ", r.url AS ressourceUrl "
			. "FROM Titre t "
			. "JOIN Service s ON t.id=s.titreId "
			. "JOIN Ressource r ON r.id=s.ressourceId "
			. " LEFT JOIN Issn i1 ON t.id = i1.titreId AND i1.support = 'papier' AND i1.statut = " . Issn::STATUT_VALIDE
			. " LEFT JOIN Issn i2 ON t.id = i2.titreId AND i2.support = 'electronique' AND i2.statut = " . Issn::STATUT_VALIDE
			. " LEFT JOIN Identification idf ON (t.id=idf.titreId AND s.ressourceId=idf.ressourceId) "
			. " LEFT JOIN Intervention i ON (i.titreId=t.id AND i.hdateVal=s.hdateModif) "
			. " LEFT JOIN Titre_Editeur te ON t.id=te.titreId "
			. " LEFT JOIN Editeur e ON te.editeurId=e.id "
			. " LEFT JOIN Service_Collection sc ON sc.serviceId=s.id "
			. " LEFT JOIN Collection c ON sc.collectionId=c.id "
			. "WHERE s.statut = 'normal' "
			. ($ids ? "AND t.revueId IN (" . join(',', $ids) . ") " : '')
			. ($cond ? " AND " . join(" AND ", $cond) : '')
			. " GROUP BY s.id"
			. " ORDER BY t.titre, r.nom, s.dateBarrDebut";
		return Yii::app()->db->createCommand($sql)->query();
	}

	private static function toCsvArray($row)
	{
		return [
			$row['titre'],
			$row['issn'],
			$row['issne'],
			$row['issnl'],
			PHP_SAPI === 'cli' ? '' : Yii::app()->createAbsoluteUrl('revue/view', ['id' => $row['revueId']]),
			$row['revueId'],
			$row['url'],
			$row['ressource'],
			$row['collections'],
			$row['type'],
			$row['acces'],
			$row['dateBarrDebut'],
			$row['numeroDebut'],
			$row['volDebut'],
			$row['noDebut'],
			$row['dateBarrFin'],
			$row['numeroFin'],
			$row['volFin'],
			$row['noFin'],
			$row['id'],
			$row['hdateModif'] ? date('Y-m-d', $row['hdateModif']) : '',
			$row['description'],
		];
	}

	/**
	 * Compute the embargo from the ending date of the first period.
	 *
	 * @param string $endingDate
	 * @return string
	 */
	private function computeEmbargo($endingDate)
	{
		$matches = [];
		if (!preg_match('/^(\d{4})\b/', $endingDate, $matches)) {
			return '';
		}
		$endingYear = $matches[1];
		$delay = date('Y', $this->time) - $endingYear;
		if ($delay) {
			return $delay . 'Y';
		}
		return '';
	}

	/**
	 * Returns a list of KBART values for a given row.
	 *
	 * @param array $row
	 * @return array
	 */
	private static function toKbartArray($row)
	{
		list($coverage, $coverageNotes) = self::buildCoverage($row);
		return [
			$row['titre'],
			$row['issn'],
			$row['issne'],
			$row['dateBarrDebut'],
			$row['volDebut'],
			$row['noDebut'],
			$row['dateBarrFin'],
			$row['volFin'],
			$row['noFin'],
			$row['url'],
			'',
			$row['idInterne'],
			$row['embargoInfo'],
			$coverage,
			trim($coverageNotes),
			$row['editeur'],
			// custom fields
			$row['bimpe'],
			$row['ressource'],
			$row['collections'],
			$row['acces'],
			PHP_SAPI === 'cli' ? '' : Yii::app()->createAbsoluteUrl('/revue/view', ['id' => $row['revueId']]),
			$row['titreId'],
		];
	}

	/**
	 * Returns a list of KBART II values for a given row.
	 *
	 * @param array $row
	 * @param bool $customFields (opt, true) Add some Mirabel-only fields (not inBacon mode).
	 * @return array
	 */
	private static function toKbartv2Array($row, $customFields = true)
	{
		list($coverage, $coverageNotes) = self::buildCoverage($row);
		$revueUrl = Yii::app()->params->itemAt('baseUrl') . '/revue/' . $row['revueId'];
		$kb = [
			$row['titre'],
			$row['issn'],
			$row['issne'],
			$row['dateBarrDebut'],
			$row['volDebut'],
			$row['noDebut'],
			$row['dateBarrFin'],
			$row['volFin'],
			$row['noFin'],
			$row['url'] ?: $row['ressourceUrl'],
			'',
			$customFields ? $row['idInterne'] : $revueUrl,
			$row['embargoInfo'],
			$coverage,
			trim($coverageNotes),
			$row['editeur'],
			'serial',
			// empty fields
			'', '', '', '', '', '', '',
			($row['acces'] === 'Libre' ? 'F' : 'P'),
		];
		if ($customFields) {
			array_push(
				$kb,
				$row['ressource'],
				$row['collections'],
				$revueUrl,
				$row['titreId']
			);
		}
		return $kb;
	}

	private static function buildCoverage($row)
	{
		$coverage = '';
		$coverageNotes = '';
		switch ($row['type']) {
			case 'Intégral':
				if ($row['selection']) {
					$coverage = 'selected articles';
				} else {
					$coverage = 'fulltext';
				}
				break;
			case 'Sommaire':
			case 'Résumé':
				$coverage = 'abstracts';
				$coverageNotes = $row['type'] . ' ';
				break;
			default:
				$coverageNotes = $row['type'] . ' ';
		}
		if ($row['lacunaire']) {
			$coverageNotes .= '(lacunaire)';
		}
		if ($row['selection']) {
			$coverageNotes .= "(sélection d'articles)";
		}
		if ($row['notes']) {
			$coverageNotes = $row['notes'];
		}
		if ($row['dateBarrInfo']) {
			$coverageNotes = ($coverageNotes ? "$coverageNotes|{$row['dateBarrInfo']}" : $row['dateBarrInfo']);
		}
		return [$coverage, $coverageNotes];
	}
}
