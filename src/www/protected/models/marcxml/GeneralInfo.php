<?php

namespace models\marcxml;

/**
 * Agregates data from leader and 100##.
 *
 * Reference doc:
 * https://www.ifla.org/files/assets/uca/publications/unimarc-holdings-format.pdf
 * leader/label: https://www.transition-bibliographique.fr/wp-content/uploads/2018/07/Bsection5-Label_notice-6-2010.pdf
 * 100: https://www.transition-bibliographique.fr/wp-content/uploads/2018/07/B100-6-2010.pdf
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class GeneralInfo
{
	public const PUBLICATIONTYPE_ONGOING = 'a';

	public const PUBLICATIONTYPE_DEAD = 'b';

	public const PUBLICATIONTYPE_MONOGA = 'f';

	public const PUBLICATIONTYPE_MONOGB = 'b';

	public const PUBLICATIONTYPE_UNKNOWN = 'c';

	public const RESOURCETYPE_PRINTED = 'a';

	public const RESOURCETYPE_ELECTRONIC = 'l';

	public const RESOURCETYPE_MULTIMEDIA = 'm';

	public const RESOURCETYPES = [
		self::RESOURCETYPE_PRINTED,
		self::RESOURCETYPE_ELECTRONIC,
		self::RESOURCETYPE_MULTIMEDIA,
	];

	public const STATUS_OK = 'c';

	public const STATUS_DELETED = 'd';

	public const STATUS_NEW = 'n';

	public const STATUSES = [self::STATUS_OK, self::STATUS_DELETED, self::STATUS_NEW];

	/**
	 * @var string YYYY-mm-dd | YYYY-mm | YYYY
	 */
	public $creationDate;

	/**
	 * @var string 3 chars
	 */
	public $lang;

	/**
	 * @var string See consts PUBLICATIONTYPE_*
	 */
	public $publicationType;

	/**
	 * @var string See const RESOURCETYPE_*
	 */
	public $resourceType;

	/**
	 * @var string See const STATUS_*
	 */
	public $status;

	/**
	 * @var ?int
	 */
	public $yearStart;

	/**
	 * @var ?int
	 */
	public $yearEnd;

	public function __construct(string $leader, ?string $control100)
	{
		$this->parseLeader($leader);
		if ($control100) {
			$this->parseControl($control100);
		}
	}

	private function parseLeader(string $l)
	{
		$this->status = substr($l, 5, 1);
		if (!in_array($this->status, self::STATUSES)) {
			throw new \Exception("Unknown notice status");
		}

		$this->resourceType = substr($l, 6, 1);
		if (!in_array($this->resourceType, self::RESOURCETYPES)) {
			throw new \Exception("Unsupported resource type");
		}
	}

	private function parseControl(string $c)
	{
		$m = [];
		if (!preg_match('/^(\d{8})([a-z])([\d ]{4})([\d ]{4})/', $c, $m)) {
			throw new \Exception('Invalid string in 100## $a');
		}
		$this->creationDate = self::formatDate($m[1]);
		switch ($m[2]) {
			case self::PUBLICATIONTYPE_ONGOING:
				$this->publicationType = self::PUBLICATIONTYPE_ONGOING;
				$this->yearStart = (int) $m[3];
				break;
			case self::PUBLICATIONTYPE_DEAD:
				$this->publicationType = self::PUBLICATIONTYPE_DEAD;
				$this->yearStart = (int) $m[3];
				$this->yearEnd = (int) $m[4];
				break;
			case self::PUBLICATIONTYPE_UNKNOWN:
			case self::PUBLICATIONTYPE_MONOGA:
			case self::PUBLICATIONTYPE_MONOGB:
				$this->publicationType = self::PUBLICATIONTYPE_UNKNOWN;
				break;
			default:
				throw new \Exception("This library can only parse MARCXML for continuous publications. Type was '{$m[2]}'.");
		}
		$this->lang = substr($c, 22, 3);
	}

	private static function formatDate(string $date): string
	{
		$firstChar = substr($date, 0, 1);
		if ($firstChar === '9' || $firstChar === 'X') {
			return "";
		}
		$m = [];
		if (preg_match('/^(\d{4})(\d\d)(\d\d)$/', $date, $m)) {
			return sprintf("%d-%02d-%02d", $m[1], $m[2], $m[3]);
		}
		if (preg_match('/^(\d{4})(\d\d)/', $date, $m)) {
			return sprintf("%d-%02d", $m[1], $m[2]);
		}
		if (preg_match('/^(\d{4})/', $date, $m)) {
			return $m[1];
		}
		throw new \Exception("Invalid date");
	}
}
