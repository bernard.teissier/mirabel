<?php

namespace models\marcxml;

class Parser
{
	/**
	 * @var \XMLReader
	 */
	private $reader;

	public function __construct()
	{
		$this->reader = new \XMLReader();
	}

	public function parse(string $xml): Record
	{
		$marc = new Record();

		$this->reader->XML($xml, null, LIBXML_COMPACT | LIBXML_NONET);
		$node = $this->reader;

		$reached = false;
		while (!$reached && $node->read()) {
			if ($node->depth === 1 && $node->nodeType === \XMLReader::ELEMENT) {
				$reached = true;
			}
		}

		// cursor is now on the first tag of depth 1
		do {
			switch ($node->name) {
				case "leader":
					$marc->setLeader($node->readString());
					break;
				case "controlfield":
					$marc->addControl($node->getAttribute('tag'), $node->readString());
					break;
				case "datafield":
					$dom = $node->expand();
					$marc->addDataField($node->getAttribute('tag'), $node->getAttribute('ind1'), $node->getAttribute('ind2'), $this->parseSubfields($dom));
					break;
				default:
					break;
			}
		} while ($node->next());

		$this->reader->close();

		return $marc;
	}

	private function parseSubfields(\DOMNode $dom): array
	{
		$subfields = [];
		foreach ($dom->childNodes as $n) {
			/* @var $n \DOMElement */
			if ($n->nodeType === \XML_ELEMENT_NODE && $n->textContent != "") {
				assert($n instanceof \DOMElement);
				$attr = $n->attributes->getNamedItem("code");
				if ($attr === null) {
					continue;
				}
				$code = $attr->nodeValue;
				if (isset($subfields[$code])) {
					// repeated field => array of values
					if (is_string($subfields[$code])) {
						$subfields[$code] = [$subfields[$code]];
					}
					$subfields[$code][] = $n->textContent;
				} else {
					$subfields[$code] = $n->textContent;
				}
			}
		}
		return $subfields;
	}
}
