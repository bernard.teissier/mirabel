<?php

namespace models\marcxml;

class Record
{
	//const ROLE_CATALOGING = 0;

	//const ROLE_TRANSCRIBING = 1;

	public const ROLE_MODIFYING = 2;

	public const ROLE_ISSUING = 3;

	public $data = [];

	private $leader = '';

	private $control = [];

	public function setLeader(string $leader)
	{
		$this->leader = $leader;
	}

	public function addControl(string $tag, string $value)
	{
		$this->control[$tag] = $value;
	}

	public function addDataField(string $tag, string $ind1, string $ind2, array $values)
	{
		if (empty($this->data["$tag-$ind1-$ind2"])) {
			$this->data["$tag-$ind1-$ind2"] = [$values];
		} else {
			$this->data["$tag-$ind1-$ind2"][] = $values;
		}
	}

	public function getLeader(): string
	{
		return $this->leader;
	}

	public function getControl(int $num): ?string
	{
		$key = sprintf("%03d", $num);
		return $this->control[$key] ?? null;
	}

	public function getDataField(string $tag, string $ind1, string $ind2): array
	{
		return $this->data["$tag-$ind1-$ind2"] ?? [];
	}

	public function getGeneralInfo(): GeneralInfo
	{
		$c100 = $this->getDataField("100", " ", " ");
		return new GeneralInfo($this->leader, empty($c100) ? null : $c100[0]["a"]);
	}

	/**
	 * Return the ISSN info (only the first ISSN block) as a marcxml\Issn instance.
	 *
	 * @return Issn|null
	 */
	public function getIssn(): ?Issn
	{
		$data = $this->getDataField("011", " ", " ");
		if (!$data) {
			return null;
		}
		$titleData = $this->getDataField("530", "0", " ");
		if (!$titleData) {
			$titleData = $this->getDataField("530", "1", " ");
		}
		foreach ($data as $d) {
			$issn = new Issn($d, $titleData);
			if ($issn->issn) {
				return $issn;
			}
		}
		return null;
	}

	/**
	 * @return array of ISO 639-2 codes
	 */
	public function getLangCodes(): array
	{
		$data = $this->getDataField("101", " ", " ");
		if (!$data) {
			return [];
		}
		$langs = [];
		foreach ($data as $d) {
			if (!isset($d['a'])) {
				continue;
			}
			if (is_string($d['a'])) {
				$langs[] = $d['a'];
			} else {
				$langs[] = array_merge($langs, $d['a']);
			}
		}
		return $langs;
	}

	/**
	 * @param int $role See class constants ROLE_*
	 * @param string $agency
	 * @return OriginatingSource|null
	 */
	public function getOriginatingSource(int $role, string $agency): ?OriginatingSource
	{
		$sources = $this->getOriginatingSources($role);
		return $sources[$agency] ?? null;
	}

	/**
	 * Return an array of OriginatingSource instances indexed by agency name.
	 *
	 * @param int $role See class constants ROLE_*
	 * @return OriginatingSource[] indexed by agency name
	 */
	public function getOriginatingSources(int $role): array
	{
		if ($role < 0 || $role > 3) {
			throw new \Exception("Invalid role");
		}
		$sources = [];
		foreach ($this->getDataField("801", " ", (string) $role) as $s) {
			$source = new OriginatingSource($s);
			$sources[$source->agency] = $source;
		}
		return $sources;
	}

	public function getOtherSystemsControlNumber(string $name): ?string
	{
		$numbers = $this->getOtherSystemsControlNumbers();
		return $numbers[$name] ?? null;
	}

	/**
	 * @return array E.g. ["OCoLC" => "61762295"]
	 */
	public function getOtherSystemsControlNumbers(): array
	{
		$data = $this->getDataField("035", " ", " ");
		$codes = [];
		$m = [];
		foreach ($data as $datafield) {
			if (!empty($datafield["a"]) && preg_match('/^\((.+)\)(.+)$/', $datafield["a"], $m)) {
				$codes[$m[1]] = $m[2];
			}
		}
		return $codes;
	}

	public function getTitle(): ?Title
	{
		$data = $this->getDataField("200", "1", " ");
		if (!$data) {
			// Use an alternative title when the main one is not declared.
			// e.g. Journal "RFSP" has 200 0#, but no 200 1#: https://www.sudoc.fr/039228460.xml
			$data = $this->getDataField("200", "0", " ");
		}
		return ($data ? new Title($data[0]) : null);
	}

	/**
	 * Return a list of URLs, ignoring those which are localized ($5).
	 *
	 * If the standard 856 4# is empty, the non-standard 999 ## is used instead.
	 *
	 * @return string[]
	 */
	public function getUrls(): array
	{
		$data = $this->getDataField("856", "4", " ");
		$urls = [];
		foreach ($data as $datafield) {
			if (!empty($datafield["5"]) || empty($datafield["u"])) {
				continue;
			}
			if (is_string($datafield["u"])) {
				$candidates = [$datafield["u"]];
			} else {
				$candidates = $datafield["u"];
			}
			foreach ($candidates as $c) {
				if (strncmp($c, "http", 4) === 0) {
					$urls[] = $c;
				}
			}
		}
		if (!$urls) {
			$data = $this->getDataField("999", " ", " ");
			foreach ($data as $datafield) {
				if (empty($datafield["u"])) {
					continue;
				}
				if (is_string($datafield["u"])) {
					$candidates = [$datafield["u"]];
				} else {
					$candidates = $datafield["u"];
				}
				foreach ($candidates as $c) {
					if (strncmp($c, "http", 4) === 0) {
						$urls[] = $c;
					}
				}
			}
		}
		return $urls;
	}
}
