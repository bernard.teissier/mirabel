<?php

namespace models\marcxml;

/**
 * 011 ISSN (International Standard Serial Number)
 * 530 Key Title
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class Issn
{
	public $issn;

	public $issnl;

	/**
	 * @var int cf \Issn::STATUT_*
	 */
	public $status;

	public $title;

	public function __construct(array $datafield011, array $datafield530)
	{
		if (!empty($datafield011["a"])) {
			$this->issn = $datafield011["a"];
			$this->status = \Issn::STATUT_VALIDE;
		} elseif (!empty($datafield011["y"])) {
			$this->issn = $datafield011["y"];
			$this->status = \Issn::STATUT_ANNULE;
		} elseif (!empty($datafield011["z"])) {
			$this->issn = $datafield011["z"];
			$this->status = \Issn::STATUT_ERRONE;
		}

		if (!empty($datafield011["f"])) {
			$this->issnl = $datafield011["f"];
		}

		// ignore the other subfields of 011

		if (!empty($datafield530[0]["a"])) {
			$this->title = $datafield530[0]["a"];
		}
	}
}
