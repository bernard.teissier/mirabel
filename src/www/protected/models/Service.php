<?php

/**
 * This is the model class for table "Service".
 *
 * The followings are the available columns in table 'Service':
 * @property int $id
 * @property int $ressourceId
 * @property int $titreId
 * @property string $type
 * @property string $acces
 * @property string $url
 * @property string $alerteRssUrl
 * @property string $alerteMailUrl
 * @property string $derNumUrl
 * @property bool $lacunaire
 * @property bool $selection
 * @property int $volDebut
 * @property int $noDebut
 * @property string $numeroDebut
 * @property int $volFin
 * @property int $noFin
 * @property string $numeroFin
 * @property string $dateBarrDebut
 * @property string $dateBarrFin
 * @property string $dateBarrInfo
 * @property ?string $embargoInfo
 * @property string $notes
 * @property int $import
 * @property int $hdateModif timestamp
 * @property int $hdateCreation timestamp
 * @property ?int $hdateImport timestamp
 * @property string $statut
 *
 * @property Ressource $ressource
 * @property Titre $titre
 * @property Collection[] $collections Ordered by Collection.nom
 */
class Service extends AMonitored implements IWithIndirectSuivi
{
	public static $enumType = [
		'Intégral' => 'Texte intégral',
		'Résumé' => 'Résumés',
		'Sommaire' => 'Sommaires',
		'Indexation' => 'Indexation',
	];

	public static $enumAcces = ['Libre' => 'Libre', 'Restreint' => 'Restreint'];

	public static $enumStatut = ['normal' => 'Normal', 'suppr' => 'Supprimé', 'attente' => 'Attente'];

	/**
	 * @var bool True if this record was produced by merging several records (to simplify display)
	 */
	public $fusion = false;

	/**
	 * Imports external methods into this class.
	 */
	public function behaviors()
	{
		return [
			'dateVersatile' => [
				'class' => 'application.models.behaviors.DateVersatile',
				'fields' => ['dateBarrDebut', 'dateBarrFin'],
				'nothingAfter' => true,      // rejects '2005-05 with a comment'
			],
		];
	}

	public function onUnsafeAttribute($name, $value)
	{
	}

	/**
	 * @param array $values
	 * @param bool $safeOnly
	 * @return array [collectionIds]
	 */
	public function setAttributes($values, $safeOnly = true)
	{
		if (isset($values['collectionIds'])) {
			$collectionIds = $values['collectionIds'];
			unset($values['collectionIds']);
		} else {
			$collectionIds = null;
		}
		parent::setAttributes($values, $safeOnly);
		if ($collectionIds !== null) {
			return $this->readCollectionIds($collectionIds);
		}
		return [];
	}

	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @return Service the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Service';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['ressourceId, titreId, type, acces, dateBarrDebut', 'required'],
			['import', 'required', 'on' => 'import'],
			['hdateImport', 'numerical', 'integerOnly' => true, 'on' => 'import'],
			['ressourceId, titreId, import', 'numerical', 'integerOnly' => true],
			['volDebut, noDebut, volFin, noFin', 'numerical', 'integerOnly' => true],
			['url, alerteRssUrl, alerteMailUrl, derNumUrl', 'url', 'allowEmpty' => true],
			[
				'url, alerteRssUrl, alerteMailUrl, derNumUrl',
				'ext.validators.UrlFetchableValidator',
				'on' => 'insert update',
			],
			['url, alerteRssUrl, alerteMailUrl, derNumUrl', 'length', 'max' => 512],
			['dateBarrDebut, dateBarrFin, dateBarrInfo', 'length', 'max' => 255],
			['embargoInfo', 'length', 'max' => 255],
			['notes', 'length', 'max' => 65535],
			['dateBarrDebut, dateBarrFin', 'safe'],  // see DateVersatile
			['numeroDebut, numeroFin', 'length', 'max' => 30],
			['type, acces', 'length', 'max' => 30],
			['lacunaire, selection', 'boolean'],
			['type', 'in', 'range' => array_keys(self::$enumType)], // enum
			['acces', 'in', 'range' => array_keys(self::$enumAcces)], // enum
			['statut', 'in', 'range' => array_keys(self::$enumStatut)], // enum
			[
				'ressourceId, titreId, type, acces, lacunaire, selection, numeroDebut, numeroFin, '
				. 'dateBarrDebut, dateBarrFin, dateBarrInfo, hdateModif, statut',
				'safe',
				'on' => 'search',
			],
		];
	}

	/**
	 * Called automatically before validate().
	 */
	public function beforeValidate()
	{
		if ($this->scenario === 'insert' || $this->scenario === 'update') {
			if ($this->volFin || $this->noFin) {
				if (empty($this->numeroFin)) {
					$this->addError('numeroFin', "Si le numéro de fin est rempli au format numérique, le format textuel est aussi requis.");
				}
				if (empty($this->dateBarrFin)) {
					$this->addError('dateBarrFin', "Si le numéro de fin est rempli au format numérique, la date de fin est aussi requise.");
				}
			} elseif ($this->numeroFin && empty($this->dateBarrFin)) {
				$this->addError('dateBarrFin', "Si le numéro de fin est rempli au format textuel, la date de fin est aussi requise.");
			}
		}
		if ($this->type && is_string($this->type)) {
			$this->type = ucfirst(strtolower($this->type));
		}
		return parent::beforeValidate();
	}

	/**
	 * Called automatically after validate().
	 */
	public function afterValidate()
	{
		if (!$this->errors) {
			if ($this->dateBarrDebut && $this->dateBarrFin && $this->dateBarrDebut > $this->dateBarrFin) {
				$this->addError('dateBarrDebut', "La date de début est supérieure à celle de fin.");
			}
		}
		return parent::afterValidate();
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'ressource' => [self::BELONGS_TO, 'Ressource', 'ressourceId'],
			'titre' => [self::BELONGS_TO, 'Titre', 'titreId'],
			'collections' => [
				self::MANY_MANY,
				'Collection',
				'Service_Collection(serviceId,collectionId)',
				'order' => 'collections.nom ASC',
			],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'ressourceId' => 'Ressource',
			'titreId' => 'Titre',
			'type' => 'Type',
			'acces' => 'Type d\'accès',
			'url' => 'Adresse web',
			'alerteRssUrl' => 'Flux RSS',
			'alerteMailUrl' => 'Alerte par courriel',
			'derNumUrl' => 'Dernier numéro',
			'lacunaire' => 'Couverture lacunaire',
			'selection' => 'Sélection d\'articles',
			'volDebut' => 'Premier volume (numérique)',
			'noDebut' => 'Premier numéro (numérique)',
			'numeroDebut' => 'Etat de collection (premier numéro)',
			'volFin' => 'Dernier volume (numérique)',
			'noFin' => 'Dernier numéro (numérique)',
			'numeroFin' => 'Etat de collection (dernier numéro)',
			'dateBarrDebut' => 'Date de début',
			'dateBarrFin' => 'Date de fin',
			'dateBarrInfo' => 'Infos internes sur la date barrière',
			'embargoInfo' => "Embargo (KBART)",
			'notes' => 'Notes',
			'import' => "ID num de l'import",
			'statut' => 'Statut',
			'hdateModif' => 'Dernière modification',
			'hdateCreation' => 'Date de création',
			'hdateImport' => 'Dernier import auto',
			'collectionIds' => 'Collections',
		];
	}

	/**
	 * Returns an array of parents that have a direct "Suivi": [ [table => ,  id => ], ... ]
	 *
	 * @return array
	 */
	public function listParentsForSuivi()
	{
		$parents = [];
		if ($this->titreId) {
			$parents[] = ['table' => 'Revue', 'id' => $this->titre->revueId];
		}
		if ($this->ressourceId) {
			$parents[] = ['table' => 'Ressource', 'id' => $this->ressourceId];
		}
		return $parents;
	}

	/**
	 * Returns a link toward this object.
	 *
	 * @codeCoverageIgnore
	 * @param null|string|array $proxyUrl (opt)
	 * @return string HTML link.
	 */
	public function getSelfLink($proxyUrl = null): string
	{
		if ($proxyUrl && is_string($proxyUrl)) {
			$proxyUrl = [$proxyUrl];
		}
		if ($this->url) {
			$url = $this->url;
			if ($proxyUrl) {
				foreach (array_reverse($proxyUrl) as $proxy) {
					$url = str_replace('URL', urlencode($url), $proxy);
				}
			}
			return CHtml::link(self::$enumType[$this->type], $url, ['title' => "Accès direct"]);
		}
		$url = $this->ressource->url;
		if ($proxyUrl) {
			foreach (array_reverse($proxyUrl) as $proxy) {
				$url = str_replace('URL', urlencode($url), $proxy);
			}
		}
		return CHtml::link(self::$enumType[$this->type], $url, ['title' => "Accès"])
				. CHtml::tag('i', ['class' => 'icon-home', 'title' => "Lien sur la page d'accueil de la ressource et non pas sur la revue"]);
	}

	/**
	 * Return an array of ID for each Collection linked to a _Ressource_.
	 *
	 * @return array
	 */
	public function getCollectionIds()
	{
		if (!$this->id) {
			return [];
		}
		return Yii::app()->db
			->createCommand(
				"SELECT c.id FROM Service_Collection sc JOIN Collection c ON c.id = sc.collectionId"
				. " WHERE sc.serviceId = {$this->id} ORDER BY c.id ASC"
			)->queryColumn();
	}

	/**
	 * Returns a date interval built from dateBarrDebut, dateBarrFin, numeroDebut, numeroFin.
	 *
	 * @return string HTML text.
	 */
	public function getPeriode()
	{
		$periode = '';
		if ($this->dateBarrDebut or $this->dateBarrFin) {
			$formatter = Yii::app()->format;
			if ($this->dateBarrDebut) {
				$periode .= $formatter->formatStrDate($this->dateBarrDebut);
				if ($this->numeroDebut) {
					$periode .= " ({$this->numeroDebut})";
				}
			} else {
				$periode = "…";
			}
			$periode .= " — ";
			if ($this->dateBarrFin) {
				$periode .= $formatter->formatStrDate($this->dateBarrFin);
				if ($this->numeroFin) {
					$periode .= " ({$this->numeroFin})";
				}
			} else {
				$periode .= "…";
			}
		}
		return $periode;
	}

	/**
	 * Returns the HTML of all the links for this object.
	 *
	 * @return string HTML text.
	 */
	public function getLinks()
	{
		$links = '';
		if ($this->alerteMailUrl) {
			$links .= ' ' . CHtml::link('[alerte par courriel]', $this->alerteMailUrl, ['title' => "Possibilité de s'abonner à une alerte par courriel"]);
		}
		if ($this->alerteRssUrl) {
			$links .= ' ' . CHtml::link('[RSS]', $this->alerteRssUrl, ['title' => "Accès au fil RSS de la revue sur le site de {$this->ressource->nom}"]);
		}
		if ($this->derNumUrl) {
			$links .= ' ' . CHtml::link('[Dernier numéro]', $this->derNumUrl, ['title' => "Accès direct au dernier numéro"]);
		}
		return trim($links);
	}

	/**
	 * Builds an Intervention object that can be completed later.
	 */
	public function buildIntervention(bool $direct): Intervention
	{
		$i = parent::buildIntervention($direct);
		if (isset($this->ressourceId)) {
			$i->ressourceId = $this->ressourceId;
		}
		if (isset($this->titreId)) {
			$i->titreId = $this->titreId;
			$i->revueId = $this->titre->revueId;
		}
		$i->suivi = null !== Suivi::isTracked($this);
		return $i;
	}

	/**
	 * Return an array Service[].
	 *
	 * @param int $ressourceId
	 * @param int $since       (opt)
	 * @param int $importId    (opt)
	 * @return Service[]
	 */
	public static function findDeletedFromImport(int $ressourceId, int $since = 0, int $importId = 0): array
	{
		if (!$since) {
			$since = self::getLastImportTimestamp($ressourceId, $importId);
		}
		$sql = "SELECT s.* FROM Service s JOIN Titre t ON s.titreId = t.id "
			. "WHERE s.statut = 'normal' AND s.ressourceId = :rid "
			. " AND s.import " . ($importId ? " = $importId" : " > 0")
			. " AND s.hdateCreation < :sincetoo"
			. " AND s.hdateImport IS NOT NULL AND s.hdateImport < :since"
			. " ORDER BY t.titre";
		return Service::model()->findAllBySql($sql, ['rid' => $ressourceId, 'since' => $since, 'sincetoo' => $since]);
	}

	public static function getLastImportTimestamp($ressourceId, $importId = 0): int
	{
		$lasts = Yii::app()->db->createCommand(
			"SELECT i.hdateProp FROM Intervention i "
			. "WHERE i.ressourceId = $ressourceId AND i.import"
			. ($importId ? " = $importId" : " > 0")
			. " ORDER BY i.hdateProp DESC LIMIT 2"
		)->queryColumn();
		return (int) array_pop($lasts);
	}

	/**
	 * @param bool $isGuest
	 * @return Collection[]
	 */
	public function getVisibleCollections(bool $isGuest): array
	{
		if ($isGuest) {
			return Collection::model()
				->findAllBySql(
					"SELECT c.* FROM Collection c JOIN Service_Collection sc ON sc.collectionId = c.id"
					. " WHERE c.visible = 1 AND sc.serviceId = " . $this->id
				);
		}
		return $this->collections;
	}

	/**
	 * Return true if the Service comes from an import source.
	 */
	public function hasImportSource(): bool
	{
		if ($this->ressource->autoImport) {
			return true;
		}
		foreach ($this->collections as $c) {
			if ($c->importee) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Return the list of cumulative proxies that applies on the URL of a Service.
	 *
	 * @param Partenaire|null $partenaire
	 * @param DisplayServices $display
	 * @return array Proxy URLs
	 */
	public function getProxies(?Partenaire $partenaire, DisplayServices $display): array
	{
		if (!$partenaire) {
			return [];
		}
		$proxyUrl = $partenaire->proxyUrl;
		if ($proxyUrl && $display->getServiceAccess($this) !== 'Abonné') {
			$proxyUrl = null;
		}
		if ($proxyUrl && !$display->hasProxy($this)) {
			$proxyUrl = null;
		}
		if ($this->ressource->hasCollections()) {
			$collectionProxy = Yii::app()->db
				->createCommand("SELECT a.proxyUrl FROM Abonnement a JOIN Service_Collection sc USING(collectionId) WHERE a.proxy = 1 AND a.partenaireId = :pid AND sc.serviceId = :sid LIMIT 1")
				->queryScalar([':pid' => $partenaire->id, ':sid' => $this->id]);
		} else {
			$collectionProxy = Yii::app()->db
				->createCommand("SELECT a.proxyUrl FROM Abonnement a JOIN Service s USING(ressourceId) WHERE a.proxy = 1 AND a.collectionId IS NULL AND a.partenaireId = :pid AND s.id = :sid LIMIT 1")
				->queryScalar([':pid' => $partenaire->id, ':sid' => $this->id]);
		}
		return array_filter([$proxyUrl, $collectionProxy]);
	}

	public function resetAttributes(): void
	{
		foreach (['alerteRssUrl', 'alerteMailUrl', 'derNumUrl'] as $attr) {
			$this->{$attr} = '';
		}
		$this->embargoInfo = null;
	}

	/**
	 * Called automatically before Save().
	 *
	 * @return bool
	 */
	protected function beforeSave()
	{
		$this->hdateModif = $_SERVER['REQUEST_TIME'];
		if (empty($this->hdateImport)) {
			if ($this->import > 0) {
				$this->hdateImport = $_SERVER['REQUEST_TIME'];
			} else {
				$this->hdateImport = null;
			}
		}
		if ($this->isNewRecord) {
			$this->hdateCreation = $_SERVER['REQUEST_TIME'];
		}
		foreach (['alerteMailUrl', 'alerteRssUrl', 'derNumUrl', 'dateBarrInfo', 'dateBarrDebut', 'dateBarrFin'] as $k) {
			if (empty($this->{$k})) {
				$this->{$k} = '';
			}
		}
		return parent::beforeSave();
	}

	/**
	 *
	 * @param array $ids
	 * @return array [collectionId, ...]
	 */
	private function readCollectionIds(array $ids): array
	{
		if (empty($this->ressourceId) || count($this->ressource->collections) === 0) {
			return [];
		}
		if (empty($ids)) {
			$collectionsIds = [];
		} else {
			$collectionsIds = array_filter($ids);
		}
		if ($collectionsIds) {
			$allowed = array_map(
				function ($x) {
					return $x->id;
				},
				$this->ressource->getCollectionsDiffuseur()
			);
			if (count(array_diff($collectionsIds, $allowed)) > 0) {
				$this->addError('collectionIds', "Certains identifiants de collections sont inconnus.");
			}
		} else {
			$this->addError('collectionIds', "L'accès doit être lié à une collection.");
		}
		return $collectionsIds;
	}
}
