<?php

/**
 * ServiceCollection.
 *
 * The followings are the available columns in table 'Service_Collection':
 * @property int $serviceId
 * @property int $collectionId
 *
 * @property Service $service
 * @property Collection $collection
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class ServiceCollection extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @return ServiceCollection the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Service_Collection';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['serviceId, collectionId', 'required'],
			['serviceId, collectionId', 'numerical', 'integerOnly' => true, 'allowEmpty' => false],
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'service' => [self::BELONGS_TO, 'Service', 'serviceId'],
			'collection' => [self::BELONGS_TO, 'Collection', 'collectionId'],
		];
	}

	public function beforeDelete()
	{
		if (count($this->service->collections) === 1) {
			$this->addError('serviceId', "Un accès ne peut être sans collection dans une ressource à collections.");
			return false;
		}
		return parent::beforeDelete();
	}
}
