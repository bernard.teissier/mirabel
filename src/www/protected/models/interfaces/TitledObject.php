<?php

/**
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
interface TitledObject
{
	public function getSelfLink();
}
