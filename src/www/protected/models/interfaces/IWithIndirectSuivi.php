<?php

/**
 * Interface IWithIndirectSuivi for objects that anyone can suggest to modify.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
interface IWithIndirectSuivi
{
	/**
	 * Returns an array of parents that have a direct "Suivi": [ [table => ,  id => ], ... ]
	 *
	 * @return array
	 */
	public function listParentsForSuivi();
}
