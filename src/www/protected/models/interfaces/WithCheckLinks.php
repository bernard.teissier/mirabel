<?php

interface WithCheckLinks
{
	public function checkLinks(?LinkChecker $linkChecker): array;

	public function getPrimaryKey();

	public function tableName();
}
