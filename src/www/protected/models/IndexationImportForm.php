<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class IndexationImportForm extends CFormModel
{
	public $vocabulaireId;

	public $csvfile;

	public $separator;

	public $simulation;

	public $clean;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return [
			['csvfile, vocabulaireId', 'required'],
			['csvfile', 'file', 'types' => 'csv'],
			['separator', 'length', 'max' => 5],
			['vocabulaireId', 'numerical', 'integerOnly' => true],
			['simulation, clean', 'boolean'],
		];
	}

	/**
	 * Declares customized attribute labels.
	 *
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return [
			'csvfile' => 'Fichier CSV à importer',
			'vocabulaireId' => 'Vocabulaire',
			'separator' => 'Séparateur de champs',
			'simulation' => 'Simulation',
			'clean' => 'Remplace les indexations de ce même vocabulaire',
		];
	}
}
