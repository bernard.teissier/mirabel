<?php

namespace models\exporters;

class Revue
{
	/**
	 * @var int
	 */
	public $partenaireId = 0;

	/**
	 * Prints the CSV lines for the objects matching the revueId list.
	 *
	 * @param int[] $ids array of revueId.
	 * @param string $separator between the CSV cells.
	 * @throws Exception
	 */
	public function toCsv(array $ids, $separator = ';'): void
	{
		$idList = join(',', array_map('intval', $ids));
		if ($this->partenaireId) {
			$header = [
				'possession', 'bouquet', 'titre', 'titre ID', 'issn', 'issne', 'issnl', 'revue ID', 'identifiant local', 'URL Mir@bel',
				'ppn', 'ppne', 'bnfark', 'bnfarke', 'date de début', 'date de fin', 'titre suivant', 'périodicité', 'langues', 'url',
			];
			$sql =
				<<<EOSQL
				SELECT
					pt.partenaireId IS NOT NULL,
					pt.bouquet,
					t.titre,
					t.id,
					GROUP_CONCAT(DISTINCT i1.issn) AS issn,
					GROUP_CONCAT(DISTINCT i2.issn) AS issne,
					GROUP_CONCAT(DISTINCT i3.issnl) AS issnl,
					t.revueId,
					pt.identifiantLocal,
					'url' AS url,
					GROUP_CONCAT(DISTINCT i1.sudocPpn) AS ppn,
					GROUP_CONCAT(DISTINCT i2.sudocPpn) AS ppne,
					GROUP_CONCAT(DISTINCT i1.bnfArk) AS bnfark,
					GROUP_CONCAT(DISTINCT i2.bnfArk) AS bnfarke,
					IFNULL(t.dateDebut, ''),
					IFNULL(t.dateFin, ''),
					IFNULL(t.obsoletePar, ''),
					t.periodicite,
					t.langues,
					t.url AS titreUrl
				FROM Titre t
					LEFT JOIN Partenaire_Titre pt ON (pt.titreId = t.id AND partenaireId = {$this->partenaireId})
					LEFT JOIN Issn i1 ON i1.titreId = t.id AND i1.support = :sp
					LEFT JOIN Issn i2 ON i2.titreId = t.id AND i2.support = :se
					LEFT JOIN Issn i3 ON i3.titreId = t.id AND i3.issnl IS NOT NULL
				WHERE
					t.revueId IN ($idList)
					AND t.obsoletePar IS NULL
				GROUP BY t.id
				ORDER BY t.titre;
				EOSQL;
		} else {
			$header = ['titre', 'ISSN', 'ISSN-E', 'URL Mir@bel'];
			$sql =
				<<<EOSQL
				SELECT
					t.titre,
					GROUP_CONCAT(DISTINCT i1.issn) AS issn,
					GROUP_CONCAT(DISTINCT i2.issn) AS issne,
					'url' AS url, t.revueId
				FROM Titre t
					LEFT JOIN Issn i1 ON i1.titreId = t.id AND i1.support = :sp
					LEFT JOIN Issn i2 ON i2.titreId = t.id AND i2.support = :se
				WHERE
					t.revueId IN ($idList)
					AND t.obsoletePar IS NULL
				GROUP BY t.id
				ORDER BY t.titre
				EOSQL;
		}

		$out = fopen('php://output', 'w');
		fputcsv($out, $header, $separator);
		$query = \Yii::app()->db->createCommand($sql)->query([
			':sp' => \Issn::SUPPORT_PAPIER,
			':se' => \Issn::SUPPORT_ELECTRONIQUE,
		]);
		foreach ($query as $row) {
			$row['url'] = \Yii::app()->createAbsoluteUrl('revue/view', ['id' => $row['revueId']]);
			if (!$this->partenaireId) {
				unset($row['revueId']);
			}
			fputcsv($out, $row, $separator);
		}
	}

}
