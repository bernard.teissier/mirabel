<?php

namespace models\verifications;

use Titre;

class Ppn
{
	/**
	 * @return Titre[]
	 */
	public function getIssnSansPpn(): array
	{
		return Titre::model()->findAllBySql(
			<<<EOSQL
				SELECT t.*, GROUP_CONCAT(i.issn) AS confirm FROM Titre t
				    JOIN Issn i ON t.id = i.titreId
				WHERE i.sudocPpn IS NULL AND i.issn IS NOT NULL
				GROUP BY t.id
				ORDER BY t.titre
				EOSQL
		);
	}

	/**
	 * @return Titre[]
	 */
	public function getPpnNoHolding(): array
	{
		return Titre::model()->findAllBySql(
			<<<EOSQL
				SELECT t.*, GROUP_CONCAT(i.sudocPpn) AS confirm
				FROM Titre t
				    JOIN Issn i ON t.id = i.titreId
				WHERE i.sudocPpn IS NOT NULL AND i.sudocNoHolding = 1
				GROUP BY t.id
				ORDER BY t.titre
				EOSQL
		);
	}

	/**
	 * @return Titre[]
	 */
	public function getPpnSansIssn(): array
	{
		return Titre::model()->findAllBySql(
			<<<EOSQL
				SELECT t.*, GROUP_CONCAT(i.sudocPpn) AS confirm FROM Titre t
				    JOIN Issn i ON t.id = i.titreId
				WHERE i.sudocPpn IS NOT NULL AND i.issn IS NULL
				GROUP BY t.id
				ORDER BY t.titre
				EOSQL
		);
	}
}
