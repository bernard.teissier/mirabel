<?php

namespace models\verifications;

use Editeur;
use Titre;

class Editeurs
{
	/**
	 * @var int
	 */
	public $relationIncomplete = 0;

	/**
	 * @var int
	 */
	public $sansIdref = 0;

	/**
	 * @var Editeur[]
	 */
	public $sansPays = [];

	/**
	 * @var int
	 */
	public $sansSherpa = 0;

	/**
	 * @var Editeur[]
	 */
	public $sansTitre = [];

	/**
	 * @var Titre[]
	 */
	public $titresRelationIncomplete = [];

	public function __construct()
	{
		$this->relationIncomplete = (int) \Yii::app()->db
			->createCommand(
				"SELECT count(DISTINCT te.editeurId) FROM Titre_Editeur te JOIN Editeur e ON te.editeurId = e.id WHERE e.paysId = 62 AND te.commercial = 0 AND te.intellectuel = 0"
			)->queryScalar();
		$this->titresRelationIncomplete = Titre::model()->findAllBySql(
			<<<EOSQL
			SELECT DISTINCT t.*
			FROM Titre t
				JOIN Titre_Editeur te ON t.id = te.titreId AND te.commercial = 0 AND te.intellectuel = 0
				JOIN Editeur e ON te.editeurId = e.id AND e.paysId = 62
			WHERE t.obsoletePar IS NULL
			ORDER BY t.titre
			LIMIT 500
			EOSQL
		);

		$this->sansPays = Editeur::model()->findAllBySql(
			"SELECT * FROM Editeur WHERE paysId IS NULL ORDER BY nom"
		);
		$this->sansTitre = Editeur::model()->findAllBySql(
			"SELECT e.* FROM Editeur e LEFT JOIN Titre_Editeur te ON te.editeurId = e.id "
			. "WHERE te.titreId IS NULL ORDER BY e.nom"
		);
		$this->sansIdref = (int) \Yii::app()->db
			->createCommand(
				"SELECT count(*) FROM Editeur WHERE idref IS NULL AND paysId = 62" // France
			)->queryScalar();
		$this->sansSherpa = (int) \Yii::app()->db
			->createCommand(
				"SELECT count(*) FROM Editeur WHERE sherpa IS NULL AND paysId = 62" // France
			)->queryScalar();
	}
}
