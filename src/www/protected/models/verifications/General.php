<?php

namespace models\verifications;

use Collection;
use Ressource;
use Titre;
use Yii;

class General
{
	/**
	 * @var array
	 */
	public $collectionsSansAcces;

	/**
	 * @var Collection[]
	 */
	public $collectionsTemporaires;

	/**
	 * @var array
	 */
	public $doublonsIdentification;

	/**
	 * @var Ressource[]
	 */
	public $ressources;

	/**
	 * @var array
	 */
	public $revuesSansAccesLibre;

	/**
	 * @var Titre[]
	 */
	public $titresMorts;

	/**
	 * @var array
	 */
	public $titresSansCouvertureAvecUrl;

	/**
	 * @var Titre[]
	 */
	public $titresSansCouverture;

	public function __construct()
	{
		$this->ressources = Ressource::model()->findAllBySql(
			"SELECT r.* FROM Ressource r LEFT JOIN Service s ON s.ressourceId = r.id "
			. "WHERE s.id IS NULL ORDER BY r.nom"
		);

		$this->collectionsTemporaires = Collection::model()->with('ressource')
			->findAll("t.`type` = :type", [':type' => Collection::TYPE_TEMPORAIRE]);

		$this->collectionsSansAcces = Yii::app()->db->createCommand(
			<<<EOSQL
				SELECT r.*, c.nom AS collection
				FROM Collection c
				    JOIN Ressource r ON c.ressourceId = r.id
				    LEFT JOIN Service_Collection sc ON (sc.collectionId = c.id)
				WHERE sc.serviceId IS NULL
				GROUP BY c.id
				ORDER BY r.nom, collection
				EOSQL
		)->queryAll();

		$this->titresMorts = Titre::model()->findAllBySql(
			"SELECT t.* FROM Titre t WHERE dateFin = '' AND obsoletePar IS NOT NULL ORDER BY titre"
		);

		$this->doublonsIdentification = Yii::app()->db->createCommand(
			<<<EOSQL
				SELECT GROUP_CONCAT(t.id) AS titreIds, GROUP_CONCAT(t.titre SEPARATOR ' / ') AS titres, i.ressourceId, r.nom as ressource, i.idInterne
				FROM Identification i
				    JOIN Titre t ON t.id=i.titreId
				    JOIN Ressource r ON r.id=i.ressourceId
				WHERE idinterne <> ''
				GROUP BY idinterne, ressourceId
				    HAVING count(*) > 1
				EOSQL
		)->queryAll();

		$this->revuesSansAccesLibre = [];
		foreach (['ROAD', 'DOAJ'] as $src) {
			$sourceId = (int) Yii::app()->db
				->createCommand("SELECT id FROM Sourcelien WHERE nom = ?")
				->queryScalar([$src]);
			$sql = <<<EOSQL
				SELECT t.revueId, MAX(t.titre) AS titre
				FROM Titre t
				    JOIN LienTitre l ON l.titreId = t.id AND l.sourceId = ?
				    JOIN Titre t2 ON t2.revueId = t.revueId
				    LEFT JOIN Service s ON s.titreId = t2.id AND s.type = 'Intégral' AND s.acces = 'Libre'
				GROUP BY t.revueId
				    HAVING count(s.id) = 0
				EOSQL;
			$this->revuesSansAccesLibre[$src] = Yii::app()->db->createCommand($sql)->queryAll(true, [$sourceId]);
		}

		$this->titresSansCouverture = $this->getTitlesMissingCover();
		$this->titresSansCouvertureAvecUrl = $this->getTitlesWithUrlMissingCover();
	}

	private function getTitlesWithUrlMissingCover(): array
	{
		$db = \Yii::app()->db;

		// List the images into a temp table.
		$db->createCommand("CREATE TEMPORARY TABLE couverture (id INT NOT NULL)")->execute();
		$path = dirname(Yii::app()->getBasePath()) . '/images/titres-couvertures';
		if (!is_dir($path)) {
			@mkdir($path);
		}
		$ids = array_map('intval', scandir($path, \SCANDIR_SORT_NONE));
		$transaction = $db->beginTransaction();
		for ($i = 0; $i < count($ids); $i += 1000) {
			$size = ($i + 999 > count($ids)) ? count($ids) - $i : 999;
			$db->createCommand("INSERT INTO couverture VALUES (" . join("),(", array_slice($ids, $i, $size)) . ")")->execute();
		}
		$transaction->commit();

		$couvertures = [];
		$titles = $db->createCommand(
			"SELECT t.* FROM Titre t LEFT JOIN couverture c ON t.id = c.id WHERE t.urlCouverture <> '' AND c.id IS NULL ORDER BY t.id LIMIT 100"
		)->queryAll();
		foreach ($titles as $t) {
			$title = Titre::model()->populateRecord($t, false);
			$couvertures[] = [$title->id, $title->getSelfLink(), $title->urlCouverture];
		}
		$db->createCommand("DROP TEMPORARY TABLE couverture")->execute();
		return $couvertures;
	}

	private function getTitlesMissingCover(): array
	{
		return Titre::model()->findAllBySql(
			"SELECT * FROM Titre"
			. " WHERE urlCouverture = '' AND obsoletePar IS NULL"
			. " ORDER BY titre"
		);
	}
}
