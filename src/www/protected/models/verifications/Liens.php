<?php

namespace models\verifications;

class Liens
{
	/**
	 * @var array
	 */
	public $domainesFauxEditeurs;

	/**
	 * @var array
	 */
	public $domainesFauxTitres;

	/**
	 * @var array
	 */
	public $nomsFauxEditeurs;

	/**
	 * @var array
	 */
	public $nomsFauxTitres;

	public function __construct()
	{
		$this->domainesFauxEditeurs = self::getDomainesFaux('Editeur');
		$this->domainesFauxTitres = self::getDomainesFaux('Titre');

		$this->nomsFauxEditeurs= self::getNomsFaux('Editeur');
		$this->nomsFauxTitres = self::getNomsFaux('Titre');
	}

	private static function getDomainesFaux(string $table): array
	{
		$keyName = strtolower($table) . "Id";
		return \Yii::app()->db
			->createCommand(
				<<<EOSQL
					SELECT
					  t.*,
					  lt.url AS linkUrl,
					  s.url AS expectedDomain
					FROM Lien{$table} lt
					  JOIN Sourcelien s ON lt.sourceId = s.id
					  JOIN {$table} t ON lt.{$keyName} = t.id
					WHERE RIGHT(lt.`domain`, LENGTH(s.url)) <> s.url
					EOSQL
			)->queryAll();
	}

	private static function getNomsFaux(string $table): array
	{
		$keyName = strtolower($table) . "Id";
		return \Yii::app()->db
			->createCommand(
				<<<EOSQL
					SELECT
					  t.*,
					  lt.`name` AS linkName,
					  s.nom AS sourceName
					FROM Lien{$table} lt
					  JOIN Sourcelien s ON lt.sourceId = s.id
					  JOIN {$table} t ON lt.{$keyName} = t.id
					WHERE lt.`name` <> s.nom
					EOSQL
			)->queryAll();
	}
}
