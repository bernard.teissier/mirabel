<?php

namespace models\verifications;

use CHtml;
use Titre;
use Yii;

class Issn
{
	private $conflictingIssnl;

	/**
	 *
	 * @var array
	 */
	private $issnSansIssnl;

	private $issnlSansIssn;

	private $issnlAbsents;

	private $titresElectroMultiIssne;

	private $supportInconnu;

	private $sharedIssnl;

	private $multipleIssnp;

	private $discordingDates;

	public function __construct()
	{
		$this->issnSansIssnl =  Yii::app()->db->createCommand(
			<<<EOSQL
			SELECT t.*, i.issn, i.statut AS issnStatut
			FROM Titre t
			  JOIN Issn i ON t.id = i.titreId
			WHERE i.issnl IS NULL AND i.issn IS NOT NULL
			GROUP BY t.id, i.issn
			ORDER BY t.titre
			EOSQL
		)->queryAll();

		$this->issnlSansIssn = Titre::model()->findAllBySql(
			<<<EOSQL
				SELECT t.*, i.issnl AS confirm FROM Titre t
				  JOIN Issn i ON t.id = i.titreId
				WHERE i.issn IS NULL AND i.issnl IS NOT NULL
				GROUP BY t.id, i.issnl
				ORDER BY t.titre
				EOSQL
		);

		$this->issnlAbsents = Titre::model()->findAllBySql(
			<<<EOSQL
				SELECT t.*, i1.issnl AS confirm FROM Titre t
				  JOIN Issn i1 ON t.id = i1.titreId AND i1.issnl IS NOT NULL
				  JOIN Issn i2 ON t.id = i2.titreId AND i2.issn IS NOT NULL
				GROUP BY t.revueId, i1.issnl
				  HAVING MAX(i1.issnl = i2.issn) = 0
				ORDER BY t.titre
				EOSQL
		);

		$titresElectro = "SELECT t.id
FROM Titre t
JOIN Issn i ON i.titreId = t.id
GROUP BY t.id, t.obsoletePar
HAVING MIN(i.support = :support) = 1 AND t.obsoletePar IS NULL";
		$this->titresElectroMultiIssne = Titre::model()->findAllBySql(
			<<<EOSQL
				SELECT t1.*, i1.issn AS confirm
				FROM Titre t1
				  JOIN ($titresElectro) AS te ON te.id = t1.id
				  JOIN Issn i1 ON t1.id = i1.titreId AND i1.issn IS NOT NULL
				  JOIN Titre t2 ON t1.revueId = t2.revueId
				  JOIN Issn i2 ON t2.id = i2.titreId AND i2.issn IS NOT NULL
				WHERE t1.id <> t2.id AND i1.issn = i2.issn
				GROUP BY t1.id
				ORDER BY t1.titre
				EOSQL,
			[':support' => \Issn::SUPPORT_ELECTRONIQUE]
		);

		$this->supportInconnu = Titre::model()->findAllBySql(
			"SELECT t.*, i.issn AS confirm FROM Titre t"
			. " JOIN Issn i ON t.id = i.titreId"
			. " WHERE i.support = :support AND i.statut = :statut"
			. " ORDER BY t.titre",
			[':support' => \Issn::SUPPORT_INCONNU, ':statut' => \Issn::STATUT_VALIDE]
		);

		$this->sharedIssnl = Titre::model()->findAllBySql(
			"SELECT t1.*, i1.issnl AS confirm FROM Titre t1"
			. " JOIN Issn i1 ON t1.id = i1.titreId AND i1.issnl IS NOT NULL"
			. " JOIN Titre t2 ON t1.revueId = t2.revueId"
			. " JOIN Issn i2 ON t2.id = i2.titreId AND i2.issnl IS NOT NULL"
			. " WHERE t1.id <> t2.id AND i1.issnl = i2.issnl"
			. " GROUP BY t1.id"
			. " ORDER BY t1.titre"
		);

		$this->conflictingIssnl = Yii::app()->db->createCommand(
			<<<EOSQL
				SELECT i1.issnl AS issnl1, i2.issnl AS issnl2, t.revueId, t.titre
				FROM Issn i1 JOIN Issn i2 ON i1.titreId = i2.titreId AND i1.id < i2.id
				  JOIN Titre t ON t.id = i1.titreId
				WHERE i1.issnl IS NOT NULL AND i2.issnl IS NOT NULL AND i1.issnl <> i2.issnl
				GROUP BY i1.titreId HAVING count(*) > 1
				EOSQL
		)->queryAll();

		$this->multipleIssnp = Titre::model()->findAllBySql(
			"SELECT t.*, i1.issn AS confirm FROM Titre t"
			. " JOIN Issn i1 ON t.id = i1.titreId AND i1.issn <> '' AND i1.support = :support1"
			. " JOIN Issn i2 ON t.id = i2.titreId AND i2.issn <> '' AND i2.support = :support2"
			. " WHERE i1.id <> i2.id AND i1.issn = i2.issn AND i1.support = i2.support"
			. " GROUP BY t.id"
			. " ORDER BY t.titre",
			[':support1' => \Issn::SUPPORT_PAPIER, ':support2' => \Issn::SUPPORT_PAPIER]
		);

		$this->discordingDates = Yii::app()->db->createCommand(
			"SELECT t.id, t.revueId, t.titre, t.dateDebut, i1.dateDebut AS 'ISSN début', t.dateFin, i2.dateFin AS 'ISSN fin'"
			. " FROM Titre t"
			. " JOIN (SELECT titreId, MIN(dateDebut) AS dateDebut FROM Issn WHERE dateDebut <> '' GROUP BY titreId) i1 ON t.id = i1.titreId"
			. " JOIN (SELECT titreId, MAX(dateFin) AS dateFin FROM Issn WHERE dateFin <> '' GROUP BY titreId) i2 ON t.id = i2.titreId"
			. " WHERE (t.dateDebut <> '' AND RIGHT(i1.dateDebut, 1) <> 'X' AND i1.dateDebut <> LEFT(t.dateDebut, 4))"
			. "    OR (t.dateFin <> '' AND RIGHT(i2.dateFin, 1) AND i2.dateFin <> LEFT(t.dateFin, 4))"
			. " ORDER BY t.titre"
		)->queryAll();
	}

	public function getValues()
	{
		return [
			'titresIssneSansAcces' => self::getTitresIssneSansAcces(),
			'issnSansIssnl' => $this->issnSansIssnl,
			'issnlSansIssn' => $this->issnlSansIssn,
			'issnlAbsents' => $this->issnlAbsents,
			'titresElectroMultiIssne' => $this->titresElectroMultiIssne,
			'supportInconnu' => $this->supportInconnu,
			'conflictingIssnl' => array_map(
				function ($x) {
					return [
						"ISSN-L 1" => $x['issnl1'],
						"ISSN-L 2" => $x['issnl2'],
						"Revue" => CHtml::link(CHtml::encode($x['titre']), ['/revue/view', 'id' => $x['revueId']]),
					];
				},
				$this->conflictingIssnl
			),
			'sharedIssnl' => $this->sharedIssnl,
			'multipleIssnp' => $this->multipleIssnp,
			'discordingDates' => array_map(
				function ($x) {
					return [
						"Titre" => CHtml::link($x['id'], ['/titre/view', 'id' => $x['id']]),
						"Revue" => CHtml::link(CHtml::encode($x['titre']), ['/revue/view', 'id' => $x['revueId']]),
						"Date de début" => $x['dateDebut'],
						"ISSN début" => $x['ISSN début'],
						"Date de fin" => $x['dateFin'],
						"ISSN fin" => $x['ISSN fin'],
					];
				},
				$this->discordingDates
			),
			'sansIssn' => self::getSansIssn(),
			'issnEncours' => self::getIssnEncours(),
		];
	}

	private static function getTitresIssneSansAcces()
	{
		$revueIds = "SELECT t.revueId
FROM Titre t
JOIN Issn i ON t.id = i.titreId AND i.issn <> '' AND i.support = :support
GROUP BY t.revueId";

		return Titre::model()->findAllBySql(
			"SELECT * FROM Titre WHERE obsoletePar IS NULL AND revueId IN ("
			. "SELECT Titre.revueId FROM Titre"
			. " LEFT JOIN Service s ON s.titreId = Titre.id AND s.`type` = 'Intégral'"
			. " WHERE Titre.revueId IN ($revueIds)"
			. " GROUP BY Titre.revueId"
			. " HAVING MAX(s.id) IS NULL"
			. ") ORDER BY titre",
			[':support' => \Issn::SUPPORT_ELECTRONIQUE]
		);
	}

	private static function getSansIssn()
	{
		return Titre::model()->findAllBySql(
			"SELECT t.* FROM Titre t"
			. " JOIN Issn i ON t.id = i.titreId"
			. " GROUP BY t.id"
			. " HAVING MAX(i.issn) IS NULL OR MAX(i.issn) = ''"
			. " ORDER BY t.titre"
		);
	}

	private static function getIssnEncours()
	{
		return Titre::model()->findAllBySql(
			"SELECT t.*, i.issn AS confirm FROM Titre t"
			. " JOIN Issn i ON t.id = i.titreId"
			. " WHERE i.statut = :st"
			. " ORDER BY t.titre",
			[':st' => \Issn::STATUT_ENCOURS]
		);
	}
}
