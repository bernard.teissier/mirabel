<?php

namespace models\views\titre;

use Intervention;
use Titre;

class Editeurs
{
	/**
	 * @var int
	 */
	private $userId;

	/**
	 * @var Titre
	 */
	public $titre;

	/**
	 * @var \TitreEditeur[]
	 */
	public $roles = [];

	/**
	 * @var \Editeur[]
	 */
	public $editeurs = [];

	public function __construct(int $userId, Titre $titre)
	{
		$this->userId = $userId;
		$this->titre = $titre;

		$this->roles = \Tools::sqlToPairsObject(
			"SELECT * FROM Titre_Editeur WHERE titreId = {$this->titre->id}",
			'TitreEditeur',
			'editeurId'
		);
		$this->editeurs = \Tools::sqlToPairsObject(
			"SELECT e.* FROM Editeur e JOIN Titre_Editeur te ON te.editeurId = e.id WHERE titreId = {$this->titre->id} ORDER BY e.nom",
			'Editeur'
		);
	}

	public function createIntervention(array $post): ?Intervention
	{
		$i = new Intervention();
		$i->action = 'revue-U';
		$i->description = "Rôles des éditeurs du titre « {$this->titre->titre} »";
		$i->titreId = $this->titre->id;
		$i->import = 0;
		$i->utilisateurIdProp = $this->userId;
		$i->statut = 'attente';
		$i->contenuJson = new \InterventionDetail();

		$valid = true;
		foreach ($this->roles as $te) {
			if (isset($post[$te->editeurId])) {
				$old = clone($te);
				$te->setAttributes($post[$te->editeurId]);
				if ($old->attributes === $te->attributes) {
					continue;
				}
				if ($te->validate()) {
					$i->contenuJson->update($old, $te);
				} else {
					$valid = false;
				}
			}
		}
		if (!$valid) {
			return null;
		}
		return $i;
	}
}
