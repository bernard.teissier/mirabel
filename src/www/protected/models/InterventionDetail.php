<?php

/**
 * Intervention Details.
 *
 * The models submitted must have already passed validation.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class InterventionDetail implements \ArrayAccess
{
	/**
	 * @var bool Content has been confirmed by the user. If false, content will be fully validated.
	 */
	public $confirm = false;

	/**
	 * @var bool Content is imported. If false, content will be fully validated.
	 */
	public $isImport = false;

	public $lastInsertId = [];

	/** @var array */
	private $details;

	private $errors = [];

	public function __construct(array $details=[])
	{
		$this->details = $details;
		if ($details) {
			foreach ($details as $d) {
				if (isset($d['id'])) {
					$this->lastInsertId[$d['model']] = (int) $d['id'];
				}
			}
		}
	}

	/**
	 * Adds to the details a table record creation.
	 *
	 * @param CActiveRecord $model
	 * @param string $msg (opt)
	 * @return bool
	 */
	public function create(CActiveRecord $model, $msg='')
	{
		if (!$model->isNewRecord) {
			return false;
		}
		$d = [
			'model' => get_class($model),
			'operation' => 'create',
			'after' => $model->getAttributes(null),
		];
		if ($msg) {
			$d['msg'] = $msg;
		}
		$this->details[] = $d;
		return true;
	}

	/**
	 * Adds to the details a table record deletion.
	 *
	 * @param CActiveRecord $model
	 * @param string $msg (opt)
	 * @return bool
	 */
	public function delete(CActiveRecord $model, $msg='')
	{
		if ($model->isNewRecord) {
			return false;
		}
		$d = [
			'model' => get_class($model),
			'operation' => 'delete',
			'id' => $model->getPrimaryKey(),
			'before' => $model->getAttributes(),
		];
		if ($msg) {
			$d['msg'] = $msg;
		}
		$this->details[] = $d;
		return true;
	}

	/**
	 * Adds to the details a table record change.
	 *
	 * @param CActiveRecord $old Record before change
	 * @param CActiveRecord $new Record after change
	 * @param string $msg (opt)
	 * @param bool $addIfNoChange (opt) Defaults to true.
	 * @return bool True if there are changes.
	 */
	public function update(CActiveRecord $old, CActiveRecord $new, $msg='', $addIfNoChange=true)
	{
		if ($old->isNewRecord or ($old->tableName() !== $new->tableName()) or ($old->getPrimaryKey() != $new->getPrimaryKey())) {
			return false;
		}
		$changedAttributes = [];
		foreach ($new->attributes as $k => $v) {
			// normalize old value
			if (!isset($old->attributes[$k])) {
				$oldValue = null;
			} elseif (is_array($old->attributes[$k]) || is_object($old->attributes[$k])) {
				$oldValue = json_encode($old->attributes[$k]);
			} else {
				$oldValue = $old->attributes[$k];
			}
			// normalize new value
			if ($v === null) {
				if (!isset($old->attributes[$k]) || $old->attributes[$k] === '' || $old->attributes[$k] === '[]' || $old->attributes[$k] === '{}') {
					continue;
				}
				$value = null;
			} elseif (is_object($v)) {
				if (method_exists($v, 'jsonSerialize')) {
					$value = json_encode($v);
				} elseif (method_exists($v, 'serialize')) {
					$value = $v->serialize();
				} else {
					$value = serialize($v);
				}
			} elseif (!is_string($v)) {
				$value = json_encode($v);
			} else {
				$value = $v;
			}
			// compare
			if (($oldValue === "" || $oldValue === "[]" || $oldValue === "{}")
				&& ($value === "" || $value === '[]' || $value === '{}')
				) {
				// no real change
			} elseif ($value !== $oldValue && !($oldValue === null && $value === '')) {
				$changedAttributes[$k] = $value;
			}
		}
		if (!$addIfNoChange && empty($changedAttributes)) {
			return false;
		}
		$names = array_keys($changedAttributes);
		$d = [
			'model' => get_class($old),
			'operation' => 'update',
			'id' => $old->getPrimaryKey(),
			'before' => $old->getAttributes($names),
			'after' => $changedAttributes,
		];
		if (!is_array($d['id'])) {
			$d['id'] = (int) $d['id'];
		}
		if ($msg) {
			$d['msg'] = $msg;
		}
		$this->details[] = $d;
		if (empty($names)) {
			return false;
		}
		return true;
	}

	/**
	 * @param CActiveRecord $old
	 * @param array $newValues [attrName => newValue, ...]
	 * @return bool
	 */
	public function updateByAttributes(CActiveRecord $old, array $newValues): bool
	{
		$new = clone ($old);
		$new->setAttributes($newValues, false);
		return $this->update($old, $new, '', false);
	}

	/**
	 * Returns an array of details that can be serialized into the DB.
	 *
	 * @return array
	 */
	public function toArray()
	{
		return $this->details;
	}

	/**
	 * Serialize the object into a string.
	 *
	 * @return string Serialize object ready to be saved into the DB.
	 */
	public function serialize()
	{
		return json_encode(
			[
				'class' => __CLASS__,
				'content' => $this->details,
			]
		);
	}

	/**
	 * Apply the intervention changes to the DB.
	 *
	 * @return bool Success?
	 */
	public function apply()
	{
		if (empty($this->details)) {
			return true;
		}
		try {
			$transaction = Yii::app()->db->beginTransaction();
		} catch (\PDOException $_) {
		}
		$this->errors = [];
		foreach ($this->details as &$detail) {
			if (!$this->applySingleChange($detail)) {
				$transaction->rollback();
				$this->errors[] = "Erreur en appliquant " . print_r($detail, true);
				return false;
			}
			unset($detail);
		}
		if (!empty($this->lastInsertId['Service'])) {
			// if a Service is created, check that it references a Collection when the Ressource has some.
			$service = Service::model()->findByPk($this->lastInsertId['Service']);
			if ($service && !$service->collections && $service->ressource->getCollectionsDiffuseur()) {
				if (!empty($transaction)) {
					$transaction->rollback();
				}
				$this->errors['collections'] = "Erreur : l'accès qui devrait être créé ne serait pas rattaché à une collection.";
				return false;
			}
		}
		if (!empty($transaction)) {
			$transaction->commit();
		}
		return true;
	}

	/**
	 * Revert an intervention that was applied to the DB.
	 *
	 * @return bool Success?
	 */
	public function revert()
	{
		if (empty($this->details)) {
			return false;
		}
		try {
			$transaction = Yii::app()->db->beginTransaction();
		} catch (\PDOException $_) {
		}
		$this->errors = [];
		foreach (array_reverse(array_keys($this->details)) as $num) {
			if (!$this->applySingleChange($this->details[$num], true)) {
				if (!empty($transaction)) {
					$transaction->rollback();
				}
				$this->errors[] = "Erreur en appliquant " . print_r($this->details[$num], true);
				return false;
			}
		}
		if (!empty($transaction)) {
			$transaction->commit();
		}
		return true;
	}

	/**
	 * Returns an assoc array of errors.
	 *
	 * @return array assoc array of errors.
	 */
	public function getErrors()
	{
		return $this->errors;
	}

	/**
	 * Translates an internal name into a human label.
	 *
	 * @codeCoverageIgnore
	 * @param string $text to translate.
	 * @return string text translated.
	 */
	public function getLabel($text)
	{
		switch ($text) {
			case 'create':
				return 'Création';
			case 'update':
				return 'Modification';
			case 'delete':
				return 'Suppression';
			default:
				return $text;
		}
	}

	/**
	 * Returns true if there are changes to apply.
	 */
	public function isEmpty()
	{
		if (empty($this->details)) {
			return true;
		}
		foreach ($this->details as $detail) {
			if (isset($detail)) {
				if ($detail['operation'] !== 'update') {
					return false;
				}
				if (!empty($detail['after'])) {
					// non empty update
					return false;
				}
			}
		}
		return true;
	}

	public function hasOnlySpecialFields()
	{
		$attrSkip = [
			'Ressource.autoImport' => true,
			'Ressource.partenaire' => true,
		];
		foreach ($this->details as $detail) {
			if (isset($detail)) {
				if ($detail['operation'] !== 'update') {
					return false;
				}
				if (!empty($detail['after'])) {
					foreach (array_keys($detail['after']) as $attr) {
						$fullAttr = $detail['model'] . '.' . $attr;
						if (!isset($attrSkip[$fullAttr])) {
							return false;
						}
					}
				}
			}
		}
		return true;
	}

	/**
	 * @return bool
	 */
	public function hasUrlCoverChange()
	{
		foreach ($this->details as $detail) {
			if (isset($detail) && $detail['operation'] !== 'delete') {
				if (isset($detail['after']['urlCouverture'])) {
					return true;
				}
			}
		}
		return false;
	}

	public function getSummary()
	{
		if (empty($this->details[0])) {
			return "";
		}
		$codeToMsg = [
			'create' => 'création',
			'update' => 'mise à jour',
			'delete' => 'suppression',
		];
		$operation = '';
		foreach ($this->details as $d) {
			$model = strtolower($d['model']);
			$newOperation = $d['operation'];
			switch ($model) {
				case 'service':
					$msg = "accès en ligne";
					break;
				case 'servicecollection':
					$msg = "accès en ligne";
					$newOperation = 'update';
					break;
				default:
					$msg = $model;
			}
			if ($newOperation === 'create') {
				$operation = 'create';
				break;
			}
			if ($newOperation === 'update') {
				$operation = 'update';
			} elseif ($operation !== 'update') {
				$operation = $d['operation'];
			}
		}
		return $msg . " : " . $codeToMsg[$operation];
	}

	// interface: ArrayAccess

	public function offsetSet($offset, $value)
	{
		if (null === $offset) {
			$this->details[] = $value;
		} else {
			$this->details[$offset] = $value;
		}
	}

	public function offsetExists($offset): bool
	{
		return isset($this->details[$offset]);
	}

	public function offsetUnset($offset)
	{
		unset($this->details[$offset]);
	}

	public function offsetGet($offset)
	{
		return $this->details[$offset] ?? null;
	}

	/**
	 * Modify the first operation matching the given model name.
	 *
	 * @param string $modelName
	 * @param array $changes [ attrName => [before, after] ]
	 */
	public function modifyDetail($modelName, $changes)
	{
		if (!$changes) {
			return;
		}
		foreach ($this->details as $rank => $detail) {
			if ($detail['model'] === $modelName) {
				foreach ($changes as $k => $beforeAfter) {
					$before = $beforeAfter[0];
					$after = $beforeAfter[1];
					if (isset($detail['before'])) {
						$detail['before'][$k] = $before;
					}
					$detail['after'][$k] = $after;
				}
				$this->details[$rank] = $detail;
				return;
			}
		}
	}

	/**
	 * Apply a mono-table change.
	 *
	 * @param array $detail In case of createion, an 'id' field is added.
	 * @param bool $revert (opt, false) Apply the revert transformation.
	 * @return bool Success?
	 */
	protected function applySingleChange(array &$detail, $revert=false)
	{
		if (empty($detail['model']) or empty($detail['operation'])) {
			$this->errors['fatal'] = "Le format de stockage est invalide.";
			return false;
		}

		// auto fill FK
		if (!$revert && isset($detail['after'])) {
			foreach ($detail['after'] as $k => $v) {
				$m = [];
				if (preg_match('/([a-z]+)Id$/', $k, $m) && ($v === '0' || $v === 0)) {
					$foreignModel = ucfirst($m[1]);
					if (empty($this->lastInsertId[$foreignModel])) {
						throw new Exception("lastInsertId error with $foreignModel");
					}
					$detail['after'][$k] = $this->lastInsertId[$foreignModel];
				}
			}
		}

		$operation = $detail['operation'];
		if ($revert) {
			$before = 'after';
			$after = 'before';
			if ($operation === 'create' && empty($detail['id']) && !empty($detail['after']['id'])) {
				$detail['id'] = $detail['after']['id'];
			}
			if ($operation !== 'update') {
				$operation = ($operation === 'delete' ? 'create' : 'delete');
			}
		} else {
			$before = 'before';
			$after = 'after';
		}
		$before = ($detail[$before] ?? []);
		$after = ($detail[$after] ?? []);

		try {
			/* @var $model CActiveRecord */
			switch ($operation) {
			case 'create':
				return $this->applyOperationCreate($detail, $after);
			case 'delete':
				return $this->applyOperationDelete($detail);
			case 'update':
				return $this->applyOperationUpdate($detail, $before, $after);
			default:
				return false;
			}
		} catch (CDbException $exc) {
			$this->errors['SQL'] = $exc->getMessage();
			return false;
		}
	}

	protected function addModelErrors(CActiveRecord $model)
	{
		$errors = $model->getErrors();
		foreach ($errors as $key => $value) {
			$this->errors[get_class($model) . "." . $key] = $value;
		}
	}

	/**
	 * Applies a "create" operation.
	 *
	 * @param array $detail (modified)
	 * @param array $after
	 * @return bool
	 */
	private function applyOperationCreate(&$detail, $after)
	{
		/* @var $model CActiveRecord */
		$model = new $detail['model']('import');
		if ($this->isImport) {
			$model->setScenario('import');
		}
		if ($this->isImport || $this->confirm) {
			if (property_exists($model, 'confirm')) {
				$model->confirm = true;
			}
		}
		$model->setAttributes($after);
		if (!empty($detail['id']) && !is_array($detail['id'])) {
			$model->id = (int) $detail['id'];
		}
		if (!$model->validate()) {
			$this->addModelErrors($model);
			return false;
		}
		if (strpos($model->tableName(), '_') !== false) {
			// relation table, skip insert if duplicate
			$existing = $model->findByAttributes($model->attributes);
			if ($existing) {
				return false;
			}
		}
		if (!$model->save(false)) {
			$this->addModelErrors($model);
			return false;
		}
		if (!empty($model->primaryKey)) {
			if ($model->hasAttribute('id') && empty($detail['id'])) {
				$detail['id'] = $model->primaryKey;
				$this->lastInsertId[$detail['model']] = (int) $model->primaryKey;
			} elseif (is_array($model->primaryKey)) {
				foreach ($model->primaryKey as $k => $v) {
					if (empty($detail[$k])) {
						$detail[$k] = $v;
					}
					// do not fill up lastInsertId
				}
			}
		}
		return true;
	}

	/**
	 * Applies a "delete" operation.
	 *
	 * @param array $detail
	 * @return bool
	 */
	private function applyOperationDelete($detail)
	{
		$modelName = $detail['model'];
		if (!isset($detail['id'])) {
			//$this->errors[$modelName] = "Le format de stockage est invalide (pas d'ID).";
			return true;
		}
		$model = call_user_func([$modelName, 'model'])->findByPk($detail['id']);
		if ($model === null) {
			$this->errors[$modelName] = "L'enregistrement à supprimer n'existe pas.";
			return false;
		}
		if (!$model->delete()) {
			$this->addModelErrors($model);
			return false;
		}
		return true;
	}

	/**
	 * Applies an "update" operation.
	 *
	 * @param array $detail
	 * @param array $before
	 * @param array $after
	 * @return bool
	 */
	private function applyOperationUpdate($detail, $before, $after)
	{
		if (empty($detail['id'])) {
			$this->errors[$detail['model']] = "Le format de stockage est invalide (pas d'ID).";
			return false;
		}
		$model = call_user_func([$detail['model'], 'model'])->findByPk($detail['id']);
		if ($model === null) {
			$this->errors[$detail['model']] = "L'enregistrement à modifier {$detail['id']}) n'existe pas.";
			return false;
		}
		assert($model instanceof CActiveRecord);
		if ($this->isImport) {
			$model->setScenario('import');
		}
		if ($this->isImport || $this->confirm) {
			if (property_exists($model, 'confirm')) {
				$model->confirm = true;
			}
		} else {
			// Check if if any to-be-modified content has changed since the proposition was created.
			$diff = [];
			foreach ($before as $attr => $val) {
				$currentVal = ($model->{$attr} === null) || is_scalar($model->{$attr})
					? $model->{$attr}
					: json_encode($model->{$attr});
				if ($currentVal != $val) {
					$diff[] = '"' . $model->getAttributeLabel($attr) . '"';
				}
			}
			if ($diff) {
				$this->errors['confirm'] = "Les champs suivants ont été modifiés depuis cette proposition : " . join(', ', $diff);
				return false;
			}
		}
		$model->setAttributes($after, false);
		// only validate the modified keys
		if (!$model->validate(array_keys($after))) {
			$this->addModelErrors($model);
			return false;
		}
		if (!$model->save(false)) {
			$this->addModelErrors($model);
			return false;
		}
		return true;
	}
}
