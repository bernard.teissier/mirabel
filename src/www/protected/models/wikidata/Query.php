<?php

namespace models\wikidata;

class Query
{
	public $verbose = 0; //0 to 2

	public $time = 0; //timestamp constant for the whole process

	public $lang = '';

	public $mimeFormat = '';

	private const SPARQL_URL = 'https://query.wikidata.org/sparql';

	private const ENTITY_URL = 'http://www.wikidata.org/entity';

	private const PROPERTY_URL = 'https://www.wikidata.org/wiki/Property';

	private const ENTITYDATA_URL = "https://www.wikidata.org/wiki/Special:EntityData";

	public function __construct(int $verbose, string $lang, string $format)
	{
		$this->verbose = $verbose;
		$this->lang = $lang;
		$this->mimeFormat = $format;
		$this->time = time();
	}

	/**
	 * return the result of Wikidata records
	 * @param string $sparql
	 * @return string|array selon le format
	 */
	public function querySparql(string $sparql)
	{
		$qurl = self::SPARQL_URL . '?query=' . rawurlencode($sparql);
		return $this->queryRaw($qurl);
	}

	/**
	 * return the elaborate array of Wikidata records
	 * @param string $sparql
	 * @return array
	 */
	public function queryRefined(string $sparql): array
	{
		$qurl = self::SPARQL_URL . '?query=' . rawurlencode($sparql);
		$raw = json_decode($this->queryRaw($qurl), false);
		$res = [];
		$header = $raw->head->vars;

		foreach ($raw->results->bindings as $item) {
			$element = [];
			foreach ($header as $key) {
				if (isset($item->{$key})) {
					$element[$key] = $item->{$key}->value;
				}
			}
			$res[] = $element;
		}
		return $res;
	}

	/**
	 * return the entity extracted from an url
	 * @param string $url Wikidata entity url  eg. "http://www.wikidata.org/entity/Q42"
	 * @return string|null eg. "Q42", "P31"
	 */
	public function getEntity(string $url): ?string
	{
		if (preg_match('@' . self::ENTITY_URL . '/([A-Z]\d+)$@', $url, $matches)) {
			return $matches[1];
		}
		return null;
	}

	public function queryRaw(string $qurl): string
	{
		$request = new \Curl;
		$request->setopt(CURLOPT_HTTPHEADER, ["Accept: {$this->mimeFormat}"]);
		$request->setopt(CURLOPT_TIMEOUT, 30); // seconds
		return $request->get($qurl)->getContent();
	}

	public function getPropertyDetails(string $prop): array
	{
		$qurl = sprintf("%s/%s.json", self::ENTITYDATA_URL, $prop);
		$raw = $this->queryRaw($qurl);
		$details = json_decode($raw, false)->entities->{$prop};
		return [
			'wikidata' => sprintf("%s:%s", self::PROPERTY_URL, $prop),
			'name' => $details->labels->{$this->lang}->value,
			'desc' => $details->descriptions->{$this->lang}->value,
			'modified' => $details->modified,
			'datatype' => $details->datatype,
			'urlformat' => (isset($details->claims->P1630) ? $details->claims->P1630[0]->mainsnak->datavalue->value : ''),
		];
	}

	/**
	 * Teste chaque url interrogée avec une requête minimale
	 *
	 * @return array ['url' => 'message' | 'OK' ]
	 */
	public function testNetwork(): array
	{
		$diagnostic = [];
		$url1 = self::SPARQL_URL;
		$diagnostic[$url1] = 'OK';
		try {
			$this->querySparql('SELECT ?i WHERE {?i wdt:P31 wd:Q146.} LIMIT 1');
		} catch(\Exception $e) {
			$diagnostic[$url1] = $e->getMessage();
		}

		$url2 = self::ENTITYDATA_URL;
		$diagnostic[$url2] = 'OK';
		try {
			$this->getPropertyDetails('P31');
		} catch(\Exception $e) {
			$diagnostic[$url2] = $e->getMessage();
		}

		return $diagnostic;
	}
}
