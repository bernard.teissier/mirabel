<?php

namespace models\wikidata;

use Yii;

class Compare
{
	use \models\traits\VerbosityEcho;

	public const PROPTYPE_WIKIPEDIA = 0;    // lien Wikipédia

	public const PROPTYPE_LIEN = 1;         // comparable à la table LienTitre

	public const PROPTYPE_ISSN = 2;         // comparable à la table Issn

	public const PROPTYPE_AUTRESOURCE = 3;  // source imports, eg. Cairn (fr), DOAJ

	public const PROPTYPE_TITRE = 4;        // propriété chaîne de caractères non comparable

	/**
	 * Propriétés comparables aux *lignes* de `name` correspondant dans la table LienTitre
	 */
	public const LIEN_PROPERTIES = [
		'P2013' => 'Facebook',  //+ P4003
		'P2002' => 'Twitter',  //+P6552
		'P3127' => 'Latindex',
		'P7368' => 'JournalBase',
		'P2397' => 'Youtube',
		'P4264' => 'Linkedin',  //+ P6634
		'P2003' => 'Instagram',
		'P1581' => 'Blog',
		'P8100' => 'JournalTOCs',
	];

	/**
	 * Propriétés dérivées des colonnes de la table Issn
	 */
	public const ISSN_PROPERTIES = [
		'P236' => 'issn',
		'P7363' => 'issnl',
		'P269' => 'sudocPpn',
		'P243' => 'worldcatOcn',
		'P268' => 'bnfArk',
	];

	public const AUTRESOURCE_PROPERTIES = [
		'P4700' => 'Cairn', //Cairn francophone, pas Cairn International
		'P6773' => 'HAL',
		'P5115' => 'DOAJ',
		'P3434' => 'ERIH PLUS',
	];

	public const TITRE_PROPERTIES = [
		'P1476' => 'Titre',
		'P1680' => 'Sous-titre',
	];

	public const PROPERTIES_TREE = [
		//// self::PROPTYPE_WIKIPEDIA  => [],
		self::PROPTYPE_LIEN => self::LIEN_PROPERTIES,
		self::PROPTYPE_ISSN => self::ISSN_PROPERTIES,
		self::PROPTYPE_AUTRESOURCE => self::AUTRESOURCE_PROPERTIES,
		self::PROPTYPE_TITRE => self::TITRE_PROPERTIES,
	];

	/**
	 * codes for comparisons between the Mirabel value and the Wikidata cache
	 */
	public const COMP_BOTH_EMPTY = 0;

	public const COMP_EQUAL = 1;

	public const COMP_ONLY_MIRABEL = 10;

	public const COMP_ONLY_WIKIDATA = 11;

	public const COMP_DIFFERENT = 20;

	public const COMP_MULTIVALUED = 30; // Multivalué dans Wikidata, valeur simple dans Mirabel

	public const COMP_DEFAULT_ORDER = '11,20,30,10,1,0'; // Affichage de vérification

	public const COMP_TITRE_OLD = 80;          // le titre porteur des propriétés n'est pas le dernier de la revue

	public const COMP_MULTIPLE_TITLES = 81;    // le rapprochement avec les ISSN renvoie plusieurs titres

	public const COMP_MULTIPLE_QIDS = 82; 	    // une même revue Mir@abel est retournée par plusieurs éléments Wikidata

	public const COMP_MULTIPLE_REVUES = 83;    // un même élément Wikidata est associé à plusieurs revues Mir@bel

	public const COMP_ISSN_INCONSISTENT = 98;  // aucun ISSN commun entre Mir@bel et Wikidata

	public const COMP_INEXISTENT = 99;         // Mir@belId dans Wikidata n'existe pas dans Mir@bel  (revue supprimée)

	public const COMP_UNKNOWN = 100;           // catchall => probablement le calcul des comparaisons n'a pas été lancé

	public const COMP_INCONSISTENCIES = 80;      // seuil des données incohérentes (inclus)

	public const COMP_INCONSISTENCIES_CRIT = 95; // seuil des données incohérentes graves (inclus)

	public const COMP_CODES = [
		self::COMP_BOTH_EMPTY => 'Vides',
		self::COMP_EQUAL => 'Égales',
		self::COMP_ONLY_MIRABEL => 'Seulement Mirabel',
		self::COMP_ONLY_WIKIDATA => 'Seulement Wikidata',
		self::COMP_DIFFERENT => 'Différentes',
		self::COMP_MULTIVALUED => 'Multivaluée',

		self::COMP_TITRE_OLD => 'Titre porteur obsolète',
		self::COMP_MULTIPLE_TITLES => 'Plusieurs titres par revue',
		self::COMP_MULTIPLE_QIDS => 'Plusieurs Qids par revue',
		self::COMP_MULTIPLE_REVUES => 'Plusieurs revues par Qid',
		self::COMP_ISSN_INCONSISTENT => 'ISSN introuvable',
		self::COMP_INEXISTENT => 'Disparue',
		self::COMP_UNKNOWN => 'Inconnue (erreur)',

	];

	public const COMP_LEVELS = [
		self::COMP_BOTH_EMPTY => 'success',
		self::COMP_EQUAL => 'success',
		self::COMP_ONLY_MIRABEL => 'info',
		self::COMP_ONLY_WIKIDATA => 'warning',
		self::COMP_DIFFERENT => 'error',
		self::COMP_MULTIVALUED => 'warning',

		self::COMP_TITRE_OLD => 'warning',
		self::COMP_MULTIPLE_TITLES => 'warning',
		self::COMP_MULTIPLE_QIDS => 'warning',
		self::COMP_MULTIPLE_REVUES => 'warning',
		self::COMP_ISSN_INCONSISTENT => 'error',
		self::COMP_INEXISTENT => 'error',
		self::COMP_UNKNOWN => 'error',
	];

	public $verbose = 0; //0 to 2

	public $simulation = 0; //0 to 2

	public $time = 0; //timestamp constant for the whole process

	public $revueId = null;

	public function __construct($verbose, $simulation, $revueId)
	{
		$this->verbose = $verbose;
		$this->simulation = $simulation;
		$this->revueId = $revueId;
		$this->time = time();
	}

	/**
	 * toutes les propriétés
	 * @return array
	 */
	public static function getWdProperties()
	{
		return array_merge(self::ISSN_PROPERTIES, self::AUTRESOURCE_PROPERTIES, self::LIEN_PROPERTIES, self::TITRE_PROPERTIES);
	}

	/**
	 * les propriétés non soumises à la comparaison Wikidata / Mirabel
	 * @return array
	 */
	public static function getIncomparableProperties()
	{
		return array_merge(self::ISSN_PROPERTIES, self::AUTRESOURCE_PROPERTIES, self::TITRE_PROPERTIES);
	}

	public static function getCompCodes()
	{
		return self::COMP_CODES;
	}

	/**
	 *
	 * @param int $numeric  Wikidata.comparison
	 * @return string
	 */
	public static function getCompCode($numeric)
	{
		if (isset(self::COMP_CODES[$numeric])) {
			return sprintf("%s", self::COMP_CODES[$numeric]);
		}
		return sprintf("%s", $numeric);
	}

	/**
	 *
	 * @param int $numeric  Wikidata.comparison
	 * @return string ('success', 'warning', 'error', 'info')
	 */
	public static function getCompLevel($numeric)
	{
		if (isset(self::COMP_LEVELS[$numeric])) {
			return self::COMP_LEVELS[$numeric];
		}
		return 'error';
	}

	/**
	 *
	 * @param int $numeric  Wikidata.propcode
	 * @return string
	 */
	public static function getPropCode($numeric)
	{
		$properties = self::getWdProperties();
		if (isset($properties[$numeric])) {
			return sprintf("%s-%s", $numeric, $properties[$numeric]);
		}
		return sprintf("%s", $numeric);
	}

	/**
	 * @return array dropdownlist of properties, as an associative array, eg. ['WP:fr' => 'WP:fr (1234)', 'P268' => 'bnfArk (42)' ...]
	 */
	public static function getWdPropertyDDL(): array
	{
		$sql = " SELECT propertyType, property, COUNT(DISTINCT revueId) AS cnt "
				. "FROM Wikidata WHERE propertyType > 0 "
				. "GROUP BY property ORDER BY propertyType, property";
		$res = [];
		$previousType = 1;
		foreach (Yii::app()->db->createCommand($sql)->queryAll() as $record) {
			$prop = $record['property'];
			if ($record['propertyType'] != $previousType) { // intercalaire quand on change de type de propriété
				$res['inter' . $previousType] = '-----';
				$previousType = $record['propertyType'];
			}
			$res[$prop] = sprintf("%s (%d revues)", self::getPropCode($prop), $record['cnt']);
		}
		return $res;
	}

	/**
	 * @param bool  $inconsistent : seulement les icohérences (>= seuil) ou seulement les comparaisons standard
	 * @return array dropdownlist of comparison results as an associative array, eg. [11 => 'Seulement Wikidata (52)']
	 *
	 */
	public static function getComparisonDDL(bool $inconsistent = false): array
	{
		$sql = " SELECT DISTINCT(comparison), COUNT(DISTINCT revueId) AS cnt FROM Wikidata "
				. "WHERE property LIKE 'P%' AND comparison IS NOT NULL "
				. sprintf("AND comparison %s %d ", ($inconsistent ? '>=' : '<'), self::COMP_INCONSISTENCIES)
				. "GROUP BY comparison";
		$res = [];
		foreach (Yii::app()->db->createCommand($sql)->queryAll() as $record) {
			$comp = $record['comparison'];
			// $res[$comp] = sprintf("%s (%d revues)", self::getCompCode($comp), $record['cnt']);
			/** @todo comptage contextuel (propriété sélectionnée **/
			$res[$comp] = sprintf("%s", self::getCompCode($comp));
		}
		return $res;
	}

	public function compareInexistent()
	{
		$sql = "SELECT W.id, qId, revueId, MAX(I.id) AS interventionId "
		. "FROM Wikidata W LEFT JOIN Intervention I USING (revueId) "
		. "WHERE revueId NOT IN (SELECT id FROM Revue) GROUP BY W.id";
		$rows = Yii::app()->db->createCommand($sql)->queryAll();

		foreach ($rows as $row) {
			$this->setConsistencyErrorByQid($row['qId'], self::COMP_INEXISTENT, "revueId=" . $row['revueId']);
		}
	}

	/**
	 * Comparaisons complémentaires
	 */
	public function compareComplement()
	{
		// revues multiples
		$sql1 = "SELECT qId, COUNT(DISTINCT revueId) AS cnt, GROUP_CONCAT(DISTINCT revueId) AS gc "
				. "FROM Wikidata WHERE comparison IS NULL GROUP BY qId HAVING cnt>1";
		$rows = Yii::app()->db->createCommand($sql1)->queryAll();
		foreach ($rows as $row) {
			$this->setConsistencyErrorByQid($row['qId'], self::COMP_MULTIPLE_REVUES, "revueId=" . $row['gc']);
		}

		// éléments multiples
		$sql2 = "SELECT revueId, COUNT(DISTINCT qId) AS cnt, GROUP_CONCAT(DISTINCT qId) AS gc "
				. "FROM Wikidata WHERE comparison IS NULL GROUP BY revueId HAVING cnt>1";
		$rows = Yii::app()->db->createCommand($sql2)->queryAll();
		foreach ($rows as $row) {
			$this->setConsistencyErrorByRevue($row['revueId'], self::COMP_MULTIPLE_QIDS, "qId=" . $row['gc']);
		}

		// titres porteurs dépassés
		$sql3 = "UPDATE Wikidata W JOIN Titre T ON (W.titreId=T.id) SET comparison=?, compQid=?"
				. " WHERE W.comparison IS NULL AND T.obsoletePar IS NOT NULL";
		Yii::app()->db->createCommand($sql3)->execute([self::COMP_TITRE_OLD, self::COMP_TITRE_OLD]);
	}

	/**
	 * Dans la table Wikidata, met à jour les champs titreId devinables à partir d'un ISSN
	 * et repère deux erreurs de cohérence possibles
	 * @return bool
	 */
	public function updateTitres(): bool
	{
		Yii::app()->db->createCommand("CREATE INDEX IF NOT EXISTS tmp_prop_value ON Wikidata (`property`, `value` (10));")->execute();
		$this->detectTitreIds();
		$this->detectMultipleTitres();

		$sql = "SELECT qId, COUNT(DISTINCT I.titreId) AS nbtitres FROM Wikidata W "
				. "LEFT JOIN Issn I ON (I.issn = W.value AND W.property='P236') WHERE W.titreId=0 "
				. "GROUP BY qId HAVING nbtitres=0";
		$rows = Yii::app()->db->createCommand($sql)->queryAll();

		foreach ($rows as $row) {
			$this->setConsistencyErrorByQid($row['qId'], self::COMP_ISSN_INCONSISTENT, '');
			$this->vecho(2, "0");
		}

		Yii::app()->db->createCommand("DROP INDEX IF EXISTS tmp_prop_value ON Wikidata")->execute();
		return true;
	}

	/**
	 * pour les erreurs globales (comp > self::COMP_INCONSISTENCIES),
	 * on ne garde que la propriété ISSN (P236) pour alimenter les logs
	 */
	public function cleanupProperties(): void
	{
		$sql = "DELETE FROM Wikidata WHERE comparison >= ? AND property != ?";
		Yii::app()->db->createCommand($sql)->execute([self::COMP_INCONSISTENCIES_CRIT, 'P236']);
	}

	/**
	 * Compare toute les propriétés
	 * @param bool $refresh seulement si la dernière comparaison est plus vieille que la modification du titre
	 */
	public function compareProperties(bool $refresh): void
	{
		try {
			$this->vecho(1, "equal + different + missing.\n");
			foreach (self::LIEN_PROPERTIES as $property => $name) {
				$this->vecho(1, "$property ($name)... ");
				$diag = $this->compareProperty($property, $name, $refresh);
				$this->vecho(1, join(' + ', $diag) . "\n");
			}
		} catch (\Exception $_) {
			return; // si la table Wikidata n'existe pas
		}
	}

	/**
	 * provide a structured array of comparison between Mirabel and Wikidata
	 * @param int $revueId
	 * @return array of array ; each row is an array with the following keys: ['text', 'wdurl', 'murl', 'comp', ...]
	 */
	public static function getRevueComparison(int $revueId): array
	{
		$res = [];

		// Properties
		$propname = self::LIEN_PROPERTIES;
		$sqlProp = "SELECT qId, property, value AS wdval, url AS wdUrl, comparison, compUrl "
				. "FROM Wikidata "
				. "WHERE property LIKE 'P%' AND revueId=? ";
		$rows = Yii::app()->db->createCommand($sqlProp)->queryAll(true, [$revueId]);
		foreach ($rows as $row) {
			if (!isset($propname[$row['property']])) {
				continue;
			}
			$comp = ($row['comparison'] ?? self::COMP_UNKNOWN) ;
			$res[] = [
				'text' => $propname[$row['property']],
				'qidLink' => \CHtml::link($row['qId'], sprintf('%s/%s', \Wikidata::WD_BASE_URL, $row['qId'])),
				'wdVal' => $row['wdval'],
				'wdUrl' => $row['wdUrl'],
				'mUrl' => $row['compUrl'],
				'compLevel' => self::COMP_LEVELS[$comp],
				'compMsg' => self::COMP_CODES[$comp],
			];
		}

		return $res;
	}

	/**
	 * prépare le tableau des statistiques de comparaison :
	 * nombre d'enregistrements, revues, titres et éléments-WD par code comparaison
	 * @param bool $inconsistent
	 * @return array
	 */
	public static function getComparisonStatistics(bool $inconsistent): array
	{
		$res = [];
		$sql = "SELECT comparison, COUNT(DISTINCT id) AS cid, COUNT(DISTINCT revueId) AS crevue, "
			. "COUNT(DISTINCT titreId) AS ctitre, COUNT(DISTINCT qId) AS cqid "
			. "FROM Wikidata WHERE comparison BETWEEN ? AND ? GROUP BY comparison";
		$comprange = $inconsistent ? [self::COMP_INCONSISTENCIES, self::COMP_UNKNOWN] : [0, self::COMP_INCONSISTENCIES - 1];
		$rows = Yii::app()->db->createCommand($sql)->queryAll(true, $comprange);
		foreach ($rows as $row) {
			$nrow = array_values($row);
			$nrow[0] = self::getCompCode($row['comparison']);
			$res[] = $nrow;
		}
		return $res;
	}

	/**
	 * Met à jour le champ titreId quand on peut le déterminer sans erreur à partir des ISSN
	 * @return int
	 */
	private function detectTitreIds(): int
	{
		$sql = <<< SQLSetTitres
			UPDATE Wikidata
				JOIN (
					SELECT W.qId, MAX(I.TitreId) AS titreId
					FROM Wikidata W
					  JOIN Issn I ON (I.issn = W.value AND W.property = 'P236')
					GROUP BY W.qId
					HAVING MIN(I.TitreId) = MAX(I.TitreId)
				) AS SingleTitle USING(qId)
			SET Wikidata.titreId = SingleTitle.titreId
			SQLSetTitres;
		return Yii::app()->db->createCommand($sql)->execute();
	}

	private function detectMultipleTitres(): int
	{
		$sql = <<<SQLMultipleTitres
			UPDATE Wikidata
				JOIN (
					SELECT W.qId, GROUP_CONCAT(DISTINCT I.titreId) AS titreids, COUNT(DISTINCT I.titreId) AS nbtitres
					FROM Wikidata W
					  JOIN Issn I ON (I.issn = W.value AND W.property = 'P236')
					GROUP BY W.qId
					HAVING nbtitres > 1
				) AS Wanalyse USING(qId)
			SET comparison=?, compQid=?, compDetails=Wanalyse.titreids, compDate=?
			SQLMultipleTitres;
		return Yii::app()->db->createCommand($sql)->execute(
			[self::COMP_MULTIPLE_TITLES, self::COMP_MULTIPLE_TITLES, $this->time]
		);
	}

	private function setConsistencyErrorByQid(string $qid, int $compCode, string $compDetails): void
	{
		$sql = "UPDATE Wikidata SET comparison=?, compDetails=?, compDate=?, compQid=? WHERE qId=?";
		Yii::app()->db->createCommand($sql)->execute([$compCode, $compDetails, $this->time, $compCode, $qid]);
	}

	private function setConsistencyErrorByRevue(string $revueId, int $compCode, string $compDetails): void
	{
		$sql = "UPDATE Wikidata SET comparison=?, compDetails=?, compDate=?, compQid=? WHERE revueId=?";
		Yii::app()->db->createCommand($sql)->execute([$compCode, $compDetails, $this->time, $compCode, $revueId]);
	}

	/**
	 *
	 * @param string $property Wikidata.property
	 * @param string $name  LienTitre.name
	 * @param bool  $refresh seulement si la dernière comparaison est plus vieille que la modification du titre
	 * @return array
	 */
	private function compareProperty($property, $name, $refresh)
	{
		$sqlbase = "SELECT W.id AS wid, W.qid, W.url AS wurl, L.id AS lid, L.url AS lurl "
			. "FROM Wikidata W "
			. ($refresh ? "JOIN Titre ON (Titre.id = W.titreId AND W.compDate < Titre.hdateModif) " : "")
			. "LEFT JOIN LienTitre L ON (L.titreId = W.titreId AND L.name = '%s') "
			. "WHERE W.property='%s' ";
		if (isset($this->revueId)) {
			$sqlbase .= sprintf("AND W.revueId = %d ", $this->revueId);
		}
		$sqlbase = sprintf($sqlbase, $name, $property);

		$sql = $sqlbase . " AND W.url = L.url";
		$rows = Yii::app()->db->createCommand($sql)->queryAll();
		$cntequal = $this->saveComparison($rows, self::COMP_EQUAL);

		$sql = $sqlbase . " AND W.url != L.url";
		$rows = Yii::app()->db->createCommand($sql)->queryAll();
		$cntdiff = $this->saveComparison($rows, self::COMP_DIFFERENT);

		$sql = $sqlbase . " AND L.id IS NULL";
		$rows = Yii::app()->db->createCommand($sql)->queryAll();
		$cntmiss = $this->saveComparison($rows, self::COMP_ONLY_WIKIDATA);

		return [$cntequal, $cntdiff, $cntmiss];
	}

	/**
	 *
	 * @param array $rows
	 * @param int $compcode
	 * @return int
	 */
	private function saveComparison($rows, $compcode)
	{
		$cnt = 0;
		foreach ($rows as $row) {
			$wdar = \Wikidata::model()->findByPk($row['wid']);
			$wdar->comparison = $compcode;
			$wdar->compUrl = ($row['lurl'] ?? '');
			$wdar->compDate = $this->time;
			if ($wdar->save()) {
				$cnt++;
			}
		}
		return $cnt;
	}
}
