<?php

namespace models\wikidata;

use Yii;

/**
 * Toutes les méthodes pour importer des données Wikidata et des liens Wikipédia dans Mir@bel
 */
class Export
{
	public $verbose = 0; // 0 to 2

	public $time = 0; // timestamp constant for the whole process

	public function __construct(int $verbose)
	{
		$this->verbose = $verbose;
		$this->time = time();
	}

	/**
	 * generate csv file for Quickstatements (version 1)
	 * @param resource $handler
	 * @return int  number of rows
	 */
	public function genQuickstatements($handler): int
	{
		$rows = $this->joinIssn("WHERE WI.revueId IS NULL GROUP BY WI.qId HAVING nbrevues=1");
		foreach ($rows as $row) {
			fputcsv(
				$handler,
				[
					$row['qId'],
					'P4730',
					'"' . (string) $row['revueids'] . '"',
					sprintf('/* Titres WD=%s  M=%s */', $row['WdTitres'], $row['MTitres']),
				],
				"\t",
				"'"
			);
		}
		return count($rows);
	}

	public function logHeaders($handler)
	{
		fputcsv(
			$handler,
			[
				'Élement Wikidata',
				'Revues-id',
				'Titres-id',
				'Issn',
				'Titres-Wikidata',
				'Titres-Mirabel',
			],
			";",
			'"'
		);
	}

	/**
	 * generate Warnings as CSV logs
	 * @param type $handler
	 * @param string $multiple nbrevues|nbtitres
	 * @return int  number of rows
	 */
	public function logWarning($handler, $multiple): int
	{
		$multipleParams = ['nbrevues', 'nbtitres'];
		if (!in_array($multiple, $multipleParams)) {
			\Yii::log("logWarning() '$multiple' est incohérent", \CLogger::LEVEL_ERROR, 'wikidata');
			return 0;
		}
		$rows = $this->joinIssn(sprintf("WHERE WI.revueId IS NULL GROUP BY WI.qId HAVING %s>1", $multiple));
		foreach ($rows as $row) {
			fputcsv(
				$handler,
				[
					$row['qId'], $row['revueids'], $row['titreids'], $row['issns'],
					$row['WdTitres'], $row['MTitres'],
				],
				";",
				'"'
			);
		}
		return count($rows);
	}

	/**
	 * generate CSV logs (notice) for a MirabelID recorded in multiple WD elements (QIDs)
	 * @param resource $handler
	 * @return int  number of rows
	 */
	public function logMultielements($handler): int
	{
		$sql = <<< 'SQLINFO'
				SELECT GROUP_CONCAT(DISTINCT WI.qId) AS qIds, COUNT(DISTINCT WI.qId) AS nbitems, T.revueId,
					GROUP_CONCAT(DISTINCT WI.titre SEPARATOR '|') AS WdTitres, GROUP_CONCAT(DISTINCT T.titre separator '|') AS MTitres,
			        GROUP_CONCAT(DISTINCT I.titreId)  as titreids, COUNT(DISTINCT I.titreId) as nbtitres
				FROM  WikidataIssn WI
			        JOIN Issn I ON (WI.issn = I.issn)
					JOIN Titre T ON (I.titreId = T.id)
				WHERE WI.revueId IS NULL
				GROUP BY T.revueId HAVING nbitems > 1
			SQLINFO;
		$rows = Yii::app()->db->createCommand($sql)->queryAll();

		foreach ($rows as $row) {
			fputcsv(
				$handler,
				[
					$row['qIds'], $row['revueId'], $row['titreids'], "",
					$row['WdTitres'], $row['MTitres'],
				],
				";",
				'"'
			);
		}
		return count($rows);
	}

	private function joinIssn(string $sqlcomp): array
	{
		$sql = <<< 'SQLREFINE'
				SELECT WI.qId, GROUP_CONCAT(DISTINCT WI.issn) AS issns, 
					GROUP_CONCAT(DISTINCT WI.titre) AS WdTitres, GROUP_CONCAT(DISTINCT T.titre) AS MTitres,
					GROUP_CONCAT(DISTINCT I.titreId) as titreids, COUNT(DISTINCT I.titreId) as nbtitres,
					GROUP_CONCAT(DISTINCT T.revueId) as revueids, COUNT(DISTINCT T.revueId) AS nbrevues
				FROM  WikidataIssn WI
				JOIN Issn I ON (WI.issn = I.issn)
				JOIN Titre T ON (I.titreId = T.id)
			SQLREFINE;

		$sql1 = $sql . " " . $sqlcomp ;
		return Yii::app()->db->createCommand($sql1)->queryAll();
	}
}
