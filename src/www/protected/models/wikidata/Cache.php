<?php

namespace models\wikidata;

use Yii;

class Cache
{
	use \models\traits\VerbosityEcho;

	/**
	 * @var array [$alpha3 => $alpha2]
	 */
	public $MirabelLanguages = [];

	public $verbose; //0 to 2

	public $simulation; //0 to 2

	public $time; //timestamp constant for the whole process

	public $limit;  // for Sparql and SQL queries

	public function __construct($verbose, $simulation, $limit=0)
	{
		$this->verbose = (int) $verbose;
		$this->simulation = (int) $simulation;
		$this->limit = (int) $limit;
		$this->time = (int) time();
	}

	/** get all languages linked to Mirabel revues
	 * note: alpha3 is used by Mirabel, whereas alpha2 is used for Wikipedia links
	 * @set $this->MirabelLanguages
	 */
	public function setMirabelLanguages(): void
	{
		$revlangues = [];

		$sql = "SELECT DISTINCT langues FROM Titre";
		$titreslangues = Yii::app()->db->createCommand($sql)->queryColumn();
		foreach ($titreslangues as $strlangues) {
			$arlangues = array_filter(array_map('trim', explode(',', $strlangues)));
			$revlangues = array_unique(array_merge($revlangues, $arlangues));
		}

		$this->MirabelLanguages = [];
		$codes = $this->getCodesLanguages();
		foreach ($revlangues as $langue) {
			if (isset($codes[$langue])) {
				$this->MirabelLanguages[$langue] = $codes[$langue];
			}
		}

		asort($this->MirabelLanguages);
	}

	/**
	 * fetch languages codes from Wikidata (independant from Mirabel data)
	 * @return array [ $alpha2 => $alpha3 ]
	 * note P218 = ISO 639-1 apha2
	 * note P219 = ISO 639-2 apha3 multiple
	 * note P220 = ISO 639-3 apha3 unique
	 * @see \models\validators\LangIso639Validator
	 */
	public function getCodesLanguages(): array
	{
		$sparql = "SELECT ?alpha2 ?alpha3
		   WHERE {
			   ?item wdt:P218 ?alpha2 ;
					 wdt:P219 ?alpha3 .
		   }";
		$WQuery = new Query($this->verbose, '', 'application/sparql-results+json');
		$codeslangues = $WQuery->queryRefined($sparql);

		$res = [];
		foreach ($codeslangues as $codelangue) {
			$res[$codelangue['alpha3']] = $codelangue['alpha2'];
		}
		return $res;
	}

	/**
	 * fill the Wikidata table with Wikipedia links in one language
	 * @param string $alpha3 language
	 * @return int or false
	 */
	public function saveWikipediaLinks(string $alpha3): int
	{
		$records = $this->getWikipediaLinks($alpha3);

		foreach ($records as $record) {
			$wdrecord = new \Wikidata();
			$wdrecord->setAttributes($record, false);
			if ($this->simulation == 0) {
				$wdrecord->save(false);
			} elseif ($this->simulation == 1) {
				$wdrecord->validate();
			}
		}
		return count($records);
	}

	/**
	 * Retrieve Wikipedia links and save them for all languages
	 * @return int nombre de liens Wikipedia récupérés
	 */
	public function fetchWikipediaLinks(): int
	{
		foreach (array_keys($this->MirabelLanguages) as $alpha3) {
			$timestart = microtime(true);
			$this->vecho(1, "$alpha3... ");
			$count = sprintf("%4d", $this->saveWikipediaLinks($alpha3));
			$dtime = sprintf("%6.3f", (microtime(true) - $timestart));
			$this->vecho(1, "$count  $dtime s.\n");
		}
		return Yii::app()->db->createCommand("SELECT COUNT(*) FROM Wikidata WHERE property LIKE 'WP:%'")->queryScalar();
	}

	public function getWikipediaLanguageStats(): array
	{
		$sql = "SELECT property, COUNT(id) AS Nb FROM Wikidata WHERE property LIKE 'WP:%' GROUP BY property ORDER BY Nb DESC";
		$rows = Yii::app()->db->createCommand($sql)->queryAll();

		$result = [];
		foreach ($rows as $row) {
			$result[$row['property']] = $row['Nb'];
		}
		return $result;
	}

	/**
	 * return the list of properties to fetch into cache
	 * @return array
	 */
	public function getProperties(): array
	{
		return Compare::getWdProperties();
	}

	/**
	 * Retrieve values and links and save them for all defined properties
	 * @return int nombre de propriétés enregistrées
	 */
	public function fetchPropertyValues(): int
	{
		foreach (Compare::PROPERTIES_TREE as $proptype => $Properties) {
			foreach ($Properties as $prop => $name) {
				$timestart = microtime(true);
				$this->vecho(1, "$prop ($name)... ");
				$count = sprintf("%5d", $this->savePropertyValues($prop, $proptype));
				$dtime = sprintf("%6.3f", (microtime(true) - $timestart));
				$this->vecho(1, "$count  $dtime s.\n");
			}
		}
		return Yii::app()->db->createCommand("SELECT COUNT(*) FROM Wikidata WHERE property LIKE 'P%'")->queryScalar();
	}

	public function getPropertyStats(): array
	{
		$sql = "SELECT propertyType, property, COUNT(id) AS Nb, FROM_UNIXTIME(hdate) as Date "
			. "FROM Wikidata WHERE propertyType > 0 "
			. "GROUP BY propertyType, property ORDER BY propertyType, COUNT(id) DESC";
		return Yii::app()->db->createCommand($sql)->queryAll();
	}

	/**
	 * remplit la table cache WikidataIssn
	 * @return int nombre d'entrées Wikidata récupérées
	 */
	public function fillWikidataIssn(): int
	{
		$zero = microtime(true);
		$rows = $this->fetchWikidataIssn();

		Yii::app()->db->createCommand('TRUNCATE TABLE WikidataIssn')->execute();
		$tr = Yii::app()->db->beginTransaction();
		$cmd = Yii::app()->db->createCommand("INSERT INTO WikidataIssn VALUES (null, ?, ?, ?, ?, ?)");
		foreach ($rows as $row) {
			$cmd->execute(array_merge($row, [$this->time]));
		}
		$tr->commit();

		$this->vecho(1, sprintf("fill WikidataIssn  %f s\n", (microtime(true)-$zero)));
		return (int) Yii::app()->db->createCommand("SELECT COUNT(*) FROM WikidataIssn")->queryScalar();
	}

	/**
	 * @param string $alpha3 language  eg. "eng", "fre"
	 * @return array
	 */
	private function getWikipediaLinks(string $alpha3): array
	{
		$alpha2 = $this->MirabelLanguages[$alpha3];
		$sparql = <<< SPARQLWP
			SELECT ?item ?idMirabel ?linkwp ?itemLabel
				WHERE
				{
					?item wdt:P4730 ?idMirabel .
					?linkwp schema:about ?item .
					?linkwp schema:isPartOf <https://%s.wikipedia.org/> .
					OPTIONAL { SERVICE wikibase:label { bd:serviceParam wikibase:language "%s". } }
				}
			SPARQLWP;

		$sparql .= ($this->limit > 0 ? "LIMIT {$this->limit}" : "");
		$req = sprintf($sparql, $alpha2, $alpha2);
		$WQuery = new Query($this->verbose, '', 'application/sparql-results+json');
		$wdresult = $WQuery->queryRefined($req);
		$timestamp = time();

		$result = [];
		foreach ($wdresult as $wdrow) {
			if (!$this->isLanguageInRevue($alpha3, (int) $wdrow['idMirabel'])) {
				// uniquement les langues officielles de la revue
				continue;
			}
			$result[] = [
				'revueId' => $wdrow['idMirabel'],
				'titreId' => 0,
				'qId' => $WQuery->getEntity($wdrow['item']),
				'property' => sprintf('WP:%s', $alpha2),
				'propertyType' => Compare::PROPTYPE_WIKIPEDIA,
				'value' => '', //not applicable
				'url' => $wdrow['linkwp'],
				'hdate' => $timestamp,
			];
		}

		return $result;
	}

	/**
	 * return true if $alpha3 is an official language for this Revue
	 * @param string $alpha3 language
	 * @param int $revId
	 * @return bool
	 */
	private function isLanguageInRevue(string $alpha3, int $revId): bool
	{
		$sql = "SELECT LOCATE(?, GROUP_CONCAT(langues)) FROM Titre WHERE revueId = ?";
		$res = Yii::app()->db->createCommand($sql)->queryScalar([$alpha3, $revId]);
		return (bool) $res;
	}

	/**
	 * @param string $prop property eg. "P2002"
	 * @param int $proptype  property type
	 * @return array
	 */
	private function getPropertyValues(string $prop, int $proptype): array
	{
		$this->checkProperty($prop);
		$sparql = <<< SPARQLPROP
			SELECT ?item ?idMirabel ?extId ?extUrl
			WHERE
			{
				OPTIONAL { wd:%s wdt:P1630 ?formatterUrl . }
				?item wdt:P4730 ?idMirabel .
				?item wdt:%s ?extId .
				BIND(IRI(REPLACE(?extId, '^(.+)$', ?formatterUrl)) AS ?extUrl).
			}
			SPARQLPROP;
		$sparql .= ($this->limit > 0 ? "LIMIT {$this->limit}" : "");

		$req = sprintf($sparql, $prop, $prop);
		$WQuery = new Query($this->verbose, 'en', 'application/sparql-results+json');
		$wdresult = $WQuery->queryRefined($req);
		$timestamp = time();

		$result = [];
		foreach ($wdresult as $wdrow) {
			$result[] = [
				'revueId' => $wdrow['idMirabel'],
				'titreId' => 0,
				'qId' => $WQuery->getEntity($wdrow['item']),
				'property' => $prop,
				'propertyType' => $proptype,
				'value' => ($wdrow['extId'] ?? ''),
				'url' => ($wdrow['extUrl'] ?? ''),
				'hdate' => $timestamp,
			];
		}
		return $result;
	}

	private function checkProperty(string $prop): void
	{
		$properties = $this->getProperties();
		if (!isset($properties[$prop])) {
			throw new \Exception("$prop: unknown property.");
		}
	}

	/**
	 * fill the Wikidata table with the Property values for one property
	 * @param string $prop eg. "P2002"
	 * @param int $proptype
	 * @return int
	 */
	private function savePropertyValues(string $prop, int $proptype): int
	{
		$records = $this->getPropertyValues($prop, $proptype);

		foreach ($records as $record) {
			$wdrecord = new \Wikidata();
			$wdrecord->setAttributes($record, false);
			if ($this->simulation == 0) {
				$wdrecord->save(false);
			} elseif ($this->simulation == 1) {
				$wdrecord->validate();
			}
		}
		return count($records);
	}

	/**
	 * récupère toutes les données Wikidata ISSN
	 * @return array (of array)
	 */
	private function fetchWikidataIssn(): array
	{
		$sparql= <<< 'SPARQL'
			SELECT ?item ?issn ?revueId ?titre
			WHERE
			{
			  ?item wdt:P236 ?issn .
			  OPTIONAL { ?item wdt:P4730 ?revueId . }
			  OPTIONAL { ?item wdt:P1476 ?titre . }
			}
			SPARQL;
		$sparql .= ($this->limit > 0 ? "LIMIT {$this->limit}" : "");

		$zero = microtime(true);
		$WQuery = new Query($this->verbose, 'en', 'text/csv');
		$wdrows = explode("\n", $WQuery->querySparql($sparql));
		$this->vecho(1, sprintf("Sparql query  %f s\n", (microtime(true)-$zero)));

		$zero = microtime(true);
		array_shift($wdrows);  // première ligne = en-tête
		array_pop($wdrows); // la dernière ligne reçue est vide
		$res = [];

		// prétraitement, reformatage et nettoyage des données récupérées
		foreach ($wdrows as $wdrow) {
			$row = str_getcsv($wdrow);
			$row[0] = $WQuery->getEntity($row[0]);
			$row[1] = mb_substr(iconv('UTF-8', 'ASCII//TRANSLIT', $row[1]), 0, 9); /** @todo diagnostic ? **/
			$row[2] = empty($row[2]) ? null : (int) $row[2];
			$res[] = $row;
		}
		$this->vecho(1, sprintf("Prétraitement %d enregistrements. %f s\n", count($wdrows), (microtime(true)-$zero)));
		return $res;
	}
}
