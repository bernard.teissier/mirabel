<?php

namespace models\wikidata;

use Titre;
use Yii;
use commands\models\LinkUpdater;

/**
 * Toutes les méthodes pour importer des données Wikidata et des liens Wikipédia dans Mir@bel
 */
class Import
{
	public $verbose = 0; //0 to 2

	public $simulation = 0; //0 to 2

	public $time = 0; //timestamp constant for the whole process

	public $limit;

	/**
	 * @var LinkUpdater global au processus
	 */
	public $linkUpdater;

	public function __construct(int $verbose, int $simulation, int $limit)
	{
		$this->verbose = (int) $verbose;
		$this->simulation = (int) $simulation;
		$this->time = (int) time();
		$this->limit = (int) $limit;
	}

	public function getLinkUpdater(): LinkUpdater
	{
		if ($this->linkUpdater === null) {
			$this->linkUpdater = new LinkUpdater();
			$this->linkUpdater->verbose = $this->verbose;
			$sources = \Sourcelien::model()->findAllBySql("SELECT * FROM Sourcelien WHERE nomcourt LIKE 'wikipedia-__'");
			foreach ($sources as $s) {
				$this->linkUpdater->addSource($s);
			}
		}
		return $this->linkUpdater;
	}

	public function importWplinks(): string
	{
		$cmd = Yii::app()->db->createCommand(
			"SELECT DISTINCT titreId FROM Wikidata WHERE property LIKE 'WP:%' AND titreId > 0"
			. ($this->limit ? " LIMIT $this->limit" : "")
		);

		$output = LinkUpdater::getCsvlogHeader();
		foreach ($cmd->queryColumn() as $titreId) {
			$titre = Titre::model()->findByPk($titreId);
			$output .= $this->importWplinksTitre($titre);
		}
		return $output;
	}

	public function getWikipediaLinksStats(): array
	{
		$sql = "SELECT COUNT(id) AS Nb, domain FROM LienTitre WHERE url LIKE '%.wikipedia.org%' "
				. "GROUP BY domain ORDER BY Nb DESC";
		$rows = Yii::app()->db->createCommand($sql)->queryAll();

		$result = [];
		foreach ($rows as $row) {
			$result[$row['domain']] = $row['Nb'];
		}
		return $result;
	}

	/**
	 * Méthode à usage unique (ou rare) pour remplacer les liens de name "Wikipedia" par "Wikipedia (langue)"
	 * conformément à la table Sourcelien
	 */
	public function updateWplinksChangeName(): void
	{
		$sql = "SELECT titreId, url, SUBSTR(domain,1,2) AS lang2  FROM LienTitre WHERE name='Wikipedia' AND domain LIKE '%.wikipedia.org'";
		$rows = Yii::app()->db->createCommand($sql)->queryAll(true);
		$count = ['ok' => 0, 'error' => 0];
		echo count($rows) . " enregistrements LienTitre à transformer... \n";
		echo LinkUpdater::getCsvlogHeader();

		foreach ($rows as $row) {
			$titre = \Titre::model()->findByPk($row['titreId']);
			$sourcename = sprintf("wikipedia-%s", $row['lang2']);
			$updater = $this->getLinkUpdater();
			echo $updater->updateLinks($titre, '', [ [$sourcename, $row['url']] ]);
			$count['ok']++;
		}
		print_r($count);
	}

	private function importWplinksTitre(Titre $titre): string
	{
		$sql = "SELECT id, qId, SUBSTRING(property,4,2) AS lang, url FROM Wikidata WHERE property LIKE 'WP:%' AND titreId=?";
		$rows = Yii::app()->db->createCommand($sql)->queryAll(true, [$titre->id]);

		$urls = [];
		$logqids = [];
		foreach ($rows as $row) {
			$urls[] = ["wikipedia-" . $row['lang'], $row['url']];
			$logqids[] = $row['id'] . ':' . $row['qId'];
		}

		try {
			$csvRow = $this->getLinkUpdater()->updateLinks(
				$titre,
				implode(',', $logqids), // arbitrairement la 1ère ligne
				$urls
			);
		} catch (\Exception $e) {
			Yii::log($e->getMessage() . "\n--\nTRACE:\n" . $e->getTraceAsString(), \CLogger::LEVEL_WARNING, 'wikidata');
			$csvRow = join(';', [$titre->revueId, $titre->id, $titre->titre, "", "", $e->getMessage(), "ERREUR"]) . "\n";
		}
		return $csvRow;
	}
}
