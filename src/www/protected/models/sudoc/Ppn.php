<?php

namespace models\sudoc;

class Ppn
{
	public $ppn;

	public $noHolding;

	public function __construct(string $ppn, bool $noHolding)
	{
		$this->ppn = $ppn;
		$this->noHolding = $noHolding;
	}
}
