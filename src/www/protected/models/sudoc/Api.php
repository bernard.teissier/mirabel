<?php

namespace models\sudoc;

use Issn;
use models\marcxml\Parser as MarcxmlParser;

class Api
{
	private $curl;

	public function __construct()
	{
		$this->curl = curl_init();
		curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($this->curl, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($this->curl, CURLOPT_TIMEOUT, 10);
	}

	/**
	 * Return a Sudoc Notice by querying the MarcXML API of Sudoc.
	 *
	 * @param string $ppn
	 * @return Notice|null
	 */
	public function getNotice(string $ppn): ?Notice
	{
		if (!$ppn) {
			return null;
		}
		$xml = $this->getXmlNotice($ppn);
		if (!$xml) {
			return null;
		}

		$parser = new MarcxmlParser();
		$marc = $parser->parse($xml);

		$notice = new Notice();
		$notice->fromMarcXml($marc);

		// TMP while the MarcXML hides the data and the RDF doesn't
		if ($notice->issn && !$notice->dateDebut) {
			$this->fillWithRdfNotice($notice);
		}

		return $notice;
	}

	/**
	 * Return a Sudoc Notice (first one matching) by querying the ISSN2PPN API then the MarcXML API.
	 *
	 * @param string $issn
	 * @return Notice|null
	 */
	public function getNoticeByIssn(string $issn): ?Notice
	{
		$ppns = $this->getPpn($issn);
		if (!$ppns) {
			return null;
		}
		// take the first valid notice from the PPN list
		foreach ($ppns as $ppn) {
			$notice = $this->getNotice($ppn->ppn);
			if ($notice !== null) {
				return $notice;
			}
		}
		// ISSN, but empty response for every PPN => no holding
		$noticeNoHolding = new Notice();
		$noticeNoHolding->issn = $issn;
		$noticeNoHolding->support = Issn::SUPPORT_INCONNU;
		$noticeNoHolding->ppn = $ppns[0]->ppn;
		$noticeNoHolding->sudocNoHolding = true;
		return $noticeNoHolding;
	}

	/**
	 * Return a list of sudoc\Ppn associated with this ISSN.
	 *
	 * @param string $issn
	 * @throws \Exception
	 * @return Ppn[]
	 */
	public function getPpn(string $issn): array
	{
		$xml = $this->issn2ppn($issn);
		if (!$xml) {
			return [];
		}

		$result = [];
		$dom = new \DOMDocument();
		$dom->loadXML($xml);
		foreach ($dom->getElementsByTagName("result") as $node) {
			assert($node instanceof \DOMElement);
			if ($node->childNodes->length > 0) {
				foreach ($node->childNodes as $subnode) {
					if ($subnode->nodeType !== XML_ELEMENT_NODE) {
						continue;
					}
					assert($subnode instanceof \DOMElement);
					if ($subnode->tagName === 'ppn') {
						$result[] = new Ppn(trim($subnode->textContent), false);
					}
				}
			}
		}
		foreach ($dom->getElementsByTagName("resultNoHolding") as $node) {
			assert($node instanceof \DOMElement);
			if ($node->childNodes->length > 0) {
				foreach ($node->childNodes as $subnode) {
					if ($subnode->nodeType !== XML_ELEMENT_NODE) {
						continue;
					}
					assert($subnode instanceof \DOMElement);
					if ($subnode->tagName === 'ppn') {
						$result[] = new Ppn(trim($subnode->textContent), true);
					}
				}
			}
		}
		return $result;
	}

	/**
	 * Returns the Sudoc PPN number, using the ISSN and a webservice.
	 *
	 * @param string $issn
	 * @return string XML
	 */
	private function issn2ppn(string $issn): string
	{
		if (!$issn) {
			return "";
		}
		list($code, $response) = $this->download("https://www.sudoc.fr/services/issn2ppn/$issn");
		if ($code === 404) {
			return "";
		}
		return $response;
	}

	private function getXmlNotice(string $ppn): string
	{
		list($code, $response) = $this->download("https://www.sudoc.fr/$ppn.xml");
		if ($code === 404) {
			return "";
		}
		return $response;
	}

	private function download(string $url): array
	{
		curl_setopt($this->curl, CURLOPT_URL, $url);
		$response = curl_exec($this->curl);
		$code = (int) curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
		if ($code === 404) {
			return [404, ''];
		}
		$error = curl_error($this->curl);
		if ($error || $code !== 200) {
			throw new \Exception("Sudoc: erreur HTTP, code $code: $error");
		}
		return [$code, $response];
	}

	private function fillWithRdfNotice(Notice $notice)
	{
		if (!$notice->ppn) {
			return;
		}
		$rdf = $this->getRdfNotice($notice->ppn);
		if (!$rdf) {
			return;
		}
		$doc = new \DOMDocument();
		$doc->loadXML($rdf);
		// dates
		$nodes = $doc->getElementsByTagNameNS("http://purl.org/dc/elements/1.1/", 'date');
		if ($nodes->length > 0) {
			$dates = explode('-', preg_replace('/[.? ]/', 'X', $nodes->item(0)->textContent), 2);
			if ($dates[1] === '9999' || strncmp($dates[1], 'XX', 2) === 0) {
				$dates[1] = '';
			}
			$notice->dateDebut = $dates[0];
			$notice->dateFin = $dates[1];
		}
		// lang
		$x = $doc->getElementsByTagNameNS("http://purl.org/dc/terms/", 'language');
		if ($x->length) {
			$langs = [];
			$matches = [];
			foreach ($x as $l) {
				assert($l instanceof \DOMNode);
				$lang = $l->getAttribute('rdf:resource');
				if ($lang) {
					if (preg_match('/iso639-3\/(...)$/', trim($lang), $matches)) {
						$lang = $matches[1];
					}
					if ($lang !== 'mul' && strlen($lang) === 3) {
						$langs[] = $lang;
					}
				}
			}
			$notice->langues = \models\lang\Normalize::fromArr($langs);
		}
	}

	private function getRdfNotice(string $ppn): string
	{
		try {
			list($code, $response) = $this->download("https://www.sudoc.fr/$ppn.rdf");
		} catch (\Exception $_) {
			return "";
		}
		return ($code == 200 ? $response : "");
	}
}
