<?php

namespace models\sudoc;

use Issn;
use models\marcxml\GeneralInfo;
use models\marcxml\Record;

class Notice
{
	/**
	 * @var string
	 */
	public $bnfArk;

	/**
	 * @var string
	 */
	public $dateDebut;

	/**
	 * @var string
	 */
	public $dateFin;

	/**
	 * @var string
	 */
	public $issn;

	/**
	 * @var string
	 */
	public $issnl;

	/**
	 * @var string e.g. "fre, eng"
	 */
	public $langues;

	/**
	 * @var string 2 uppercase letters
	 */
	public $pays;

	/**
	 * @var string
	 */
	public $ppn;

	/**
	 * @var int See Issn::STATUT_*
	 */
	public $statut;

	/**
	 * @var bool
	 */
	public $sudocNoHolding;

	/**
	 * @var string
	 */
	public $support;

	/**
	 * @var string
	 */
	public $titre;

	public $titreIssn;

	/**
	 * @var string
	 */
	public $url;

	/**
	 * @var string
	 */
	public $worldcat;

	public function fromMarcXml(Record $marc)
	{
		$this->ppn = $marc->getControl(1);
		$this->sudocNoHolding = self::getSudocNoHolding($marc);

		$this->bnfArk = self::getBnfArk($marc);

		$info = $marc->getGeneralInfo();
		$this->dateDebut = self::normalizeDate((string) $info->yearStart);
		$this->dateFin = self::normalizeDate((string) $info->yearEnd);

		$this->support = $this->getSupport($info);

		$issn = $marc->getIssn();
		if ($issn) {
			$this->issn = $issn->issn;
			$this->issnl = $issn->issnl;
			$this->titreIssn = $issn->title;
			$this->statut = $issn->status;
		}

		$this->langues = $this->getLangCodes($marc);

		$this->pays = self::getCountry($marc);

		$title = $marc->getTitle();
		$this->titre = $title ? $title->title : null;

		$urls = $marc->getUrls();
		if ($urls) {
			$this->url = $marc->getUrls()[0];
		}

		$this->worldcat = $marc->getOtherSystemsControlNumber("OCoLC");
	}

	/**
	 * A MarcXML notice has a localization (returns false) if:
	 * - it has a 002 control field
	 * - or it has a 930$5 field
	 *
	 * @param Record $marc
	 * @return bool
	 */
	private static function getSudocNoHolding(Record $marc): bool
	{
		if (!empty($marc->getControl(2))) {
			// localisation
			return false;
		}
		$data = $marc->getDataField("930", " ", " ");
		if (!$data) {
			// no localisation
			return true;
		}
		foreach ($data as $d) {
			if ($d["5"]) {
				// localisation (the missing control2 is a data bug)
				return false;
			}
		}
		return true;
	}

	private function getSupport(GeneralInfo $info) : string
	{
		switch ($info->resourceType) {
			case GeneralInfo::RESOURCETYPE_ELECTRONIC:
				return Issn::SUPPORT_ELECTRONIQUE;
			case GeneralInfo::RESOURCETYPE_PRINTED:
				return Issn::SUPPORT_PAPIER;
			case GeneralInfo::RESOURCETYPE_MULTIMEDIA:
				self::log("Sudoc PPN {$this->ppn} a le type de ressource 'multimedia'");
				return Issn::SUPPORT_INCONNU;
			default:
				return Issn::SUPPORT_INCONNU;
		}
	}

	private static function getBnfArk(Record $marc): ?string
	{
		$data = $marc->getDataField("033", " ", " ");
		$m = [];
		foreach ($data as $d) {
			if (!empty($d["a"]) && preg_match('#^https://catalogue.bnf.fr/ark:/12148/([^/]+)$#', $d["a"], $m)) {
				return $m[1];
			}
		}
		return null;
	}

	/**
	 * 2 letters country code, upper case. See http://documentation.abes.fr/sudoc/formats/unmb/zones/102.htm
	 *
	 * @param Record $marc
	 * @return string|null
	 */
	private static function getCountry(Record $marc): ?string
	{
		$data = $marc->getDataField("102", " ", " ");
		if (empty($data[0]["a"])) {
			return null;
		}
		if ($data[0]["a"] === 'XX') {
			// unknown country
			return null;
		}
		// special value 'ZZ' (multiple countries) is kept
		$code = $data[0]["a"];
		if (is_array($code)) {
			$code = array_unique(array_filter($code));
			if (count($code) > 1) {
				self::log(sprintf("ppn=%s pays %s %s", $marc->getControl(1), $code[0], $code[1]));
			}
			return $code[0];
		}
		return $code;
	}

	/**
	 * Convert the array of ISO 639-2 (MarcXML) into a string of ISO 639-3.
	 *
	 * @param Record $marc
	 * @return string|null
	 */
	private function getLangCodes(Record $marc): ?string
	{
		$langs = \models\lang\Normalize::filter($marc->getLangCodes());
		return $langs ? join(", ", $langs) : null;
	}

	private static function log(string $msg)
	{
		$dir = rtrim(\Yii::app()->getRuntimePath(), '/');
		error_log("$msg\n", 3, "$dir/sudoc.txt");
		@chmod("$dir/sudoc.txt", 0666);
	}

	private static function normalizeDate(?string $date) : ?string
	{
		if (!$date || \strlen(\trim($date)) < 4) {
			return null;
		}
		$x = str_replace(['?', '.'], ['X', 'X'], $date);
		if (!preg_match('/^\d\d/', $x)) {
			return null;
		}
		return $x;
	}
}
