<?php

namespace models\sudoc;

use Issn;
use Titre;
use Yii;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Import
{
	public $simulation = false;

	public $verbosity = 0;

	public function file(string $fileIn, string $fileOut): void
	{
		$reader = IOFactory::createReader('Xlsx');
		$reader->setReadDataOnly(true);
		$spreadsheet = $reader->load($fileIn);

		$sheet = $spreadsheet->getActiveSheet();
		$sheet->removeColumn('AZ');

		foreach ($sheet->getRowIterator() as $row) {
			/* @var $row \PhpOffice\PhpSpreadsheet\Worksheet\Row */
			$cells = $row->getCellIterator('A', 'C');
			$journalName = trim($cells->current());
			if (empty($journalName)) {
				continue;
			}
			$cells->next();
			$issnExisting = trim($cells->current());
			if (empty($issnExisting)) {
				continue;
			}
			$cells->next();
			$comment = trim($cells->current());
			$cells->next();

			// issne
			$m = [];
			if (!preg_match('/^([ep])-issn.*:\s+(\d{4}-\d{3}[\dX])\b/', $comment, $m)) {
				$this->echo(2, "IGNORE $issnExisting : $comment\n");
				continue;
			}
			$support = $m[1] === 'e' ? Issn::SUPPORT_ELECTRONIQUE : Issn::SUPPORT_PAPIER;
			$issnMissing = $m[2];

			// The missing issn is a duplicate?
			$titre = Titre::model()->findBySql("SELECT t.* FROM Titre t JOIN Issn i ON t.id = i.titreId WHERE i.issn = ?", [$issnExisting]);
			if (!$titre) {
				$this->echo(1, "ERR Le titre censé être associé à l'issn $issnExisting est introuvable\n");
				$cells->current()->setValue("ERR Le titre censé être associé à l'issn $issnExisting est introuvable");
				continue;
			}
			$exists = Yii::app()->db->createCommand("SELECT titreId FROM Issn WHERE issn = ?")->queryScalar([$issnMissing]);
			if ($exists) {
				if ($exists == $titre->id) {
					$this->echo(1, "INFO Cet issn $issnMissing est déjà associé à ce même titre\n");
					$cells->current()->setValue("INFO Cet issn est déjà associé à ce même titre");
				} else {
					$this->echo(1, "ERR Cet issn $issnMissing est présent mais associé à un autre titre\n");
					$cells->current()->setValue("WARN Cet issn est présent mais associé à un autre titre");
				}
				continue;
			}

			if ($this->simulation) {
				$this->echo(1, "INFO L'issn $issnMissing sera ajouté au titre {$titre->id}, revue {$titre->revueId}\n");
				$cells->current()->setValue("INFO L'issn sera ajouté au titre {$titre->id}, revue {$titre->revueId}\n");
				continue;
			}

			// insert the missing issn
			$intervention = $this->createIntervention($titre);
			$issnRecord = new Issn();
			$issnRecord->setAttributes(['titreId' => (int) $titre->id, 'issn' => $issnMissing], false);
			$issnRecord->fillBySudoc();
			if (!$issnRecord->sudocPpn) {
				$this->echo(1, "ERR Cet issn $issnMissing n'a pas de PPN\n");
				$cells->current()->setValue("ERR Cet issn $issnMissing n'a pas de PPN");
				continue;
			}
			$issnRecord->support = $support;
			$intervention->contenuJson->create($issnRecord);
			if ($intervention->accept()) {
				$this->echo(1, "OK L'issn $issnMissing est ajouté au titre {$titre->id}, revue {$titre->revueId}\n");
				$cells->current()->setValue("OK issn ajouté au titre {$titre->id}, revue {$titre->revueId}");
			} else {
				$this->echo(
					1,
					"ERR L'issn $issnMissing n'a pu être ajouté au titre {$titre->id}, revue {$titre->revueId}: "
					. print_r($intervention->getErrors(), true) . "\n"
				);
				$cells->current()->setValue("L'issn n'a pu être ajouté au titre {$titre->id}, revue {$titre->revueId}");
			}
		}

		$writer = IOFactory::createWriter($spreadsheet, "Xlsx");
		$writer->save($fileOut);
	}

	private function createIntervention(Titre $titre)
	{
		$i = new \Intervention();
		$i->setAttributes(
			[
				'ressourceId' => null,
				'revueId' => $titre->revueId,
				'titreId' => $titre->id,
				'editeurId' => null,
				'statut' => 'attente',
				'import' => \ImportType::getSourceId('SUDOC'),
				'ip' => '',
				'contenuJson' => new \InterventionDetail(),
				'suivi' => null !== \Suivi::isTracked($titre),
				'description' => "Modification du titre « {$titre->titre} »",
			],
			false
		);
		return $i;
	}

	private function echo(int $level, string $msg): void
	{
		if ($this->verbosity >= $level) {
			echo $msg;
		}
	}
}
