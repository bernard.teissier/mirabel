<?php

/**
 * This is the model class for table "Abonnement".
 *
 * The followings are the available columns in table 'Abonnement':
 *
 * @property int $id
 * @property int $partenaireId
 * @property int $ressourceId
 * @property ?int $collectionId
 * @property ?int $mask
 * @property bool $proxy
 * @property string $proxyUrl
 *
 * @property Partenaire $partenaire
 * @property Collection $collection
 * @property Ressource $ressource
 */
class Abonnement extends CActiveRecord
{
	public const PAS_ABONNE = 0;

	public const ABONNE = 1;

	public const MASQUE = 2;

	public const ABONNE_VIA_COLLECTION = 3;

	public const PATTERN_URL = 'URL';

	/**
	 * @var array
	 */
	public static $enumMask = ['non-abonné', 'abonné', 'masqué', 'abonné aux collections'];

	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @return Abonnement the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Abonnement';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['partenaireId', 'required'],
			['partenaireId, ressourceId, collectionId', 'numerical', 'integerOnly' => true],
			['mask', 'in', 'range' => [self::ABONNE, self::MASQUE]],
			['proxy', 'boolean'],
			['proxyUrl', 'length', 'max' => 255],
			['proxyUrl', 'filter', 'filter' => function ($x) {
				return trim($x, " \t \n ​");
			}],
			['proxyUrl', 'default', 'value' => null],
		];
	}

	public function beforeValidate()
	{
		if ($this->partenaireId) {
			if ($this->collectionId) {
				$sth = $this->dbConnection->createCommand(
					"SELECT 1 FROM Abonnement a"
					. " WHERE a.partenaireId = :pid AND a.ressourceId = :rid AND collectionId IS NULL"
					. " LIMIT 1"
				);
				if ($sth->queryScalar([':pid' => $this->partenaireId, ':rid' => $this->collection->ressourceId])) {
					$this->addError("collectionId", "Vous êtes déjà abonné à cette ressource, hors collections.");
				} else {
					$this->ressourceId = $this->collection->ressourceId;
				}
			} elseif ($this->partenaireId && $this->ressourceId && !$this->collectionId) {
				$sth = $this->dbConnection->createCommand(
					"SELECT 1 FROM Abonnement a JOIN Collection c ON c.id = a.collectionId"
					. " WHERE a.partenaireId = :pid AND c.ressourceId = :rid"
					. " LIMIT 1"
				);
				if ($sth->queryScalar([':pid' => $this->partenaireId, ':rid' => $this->ressourceId])) {
					$this->addError("ressourceId", "Vous êtes déjà abonné à une collection de cette ressource.");
				}
			}
			if (!$this->errors && $this->isNewRecord) {
				$sth = $this->dbConnection->createCommand(
					"SELECT 1 FROM Abonnement a"
					. " WHERE a.partenaireId = :pid AND collectionId <=> :cid AND ressourceId <=> :rid"
					. " LIMIT 1"
				);
				if ($sth->queryScalar([':pid' => $this->partenaireId, ':cid' => $this->collectionId, ':rid' => $this->ressourceId])) {
					if ($this->collectionId) {
						$this->addError("collectionId", "Vous êtes déjà abonné à cette collection.");
					} else {
						$this->addError("ressourceId", "Vous êtes déjà abonné à cette ressource.");
					}
				}
			}
		}
		return parent::beforeValidate();
	}

	/**
	 * Validates the attribute "proxyUrl".
	 * @param string $attr
	 */
	public function validateProxyUrl($attr)
	{
		if (!$this->hasErrors() && $this->{$attr} && strpos($this->{$attr}, self::PATTERN_URL) === false) {
			$this->addError(
				$attr,
				"L'URL doit contenir \"" . self::PATTERN_URL . "\" qui sera remplacé à chaque usage du proxy."
			);
		}
	}

	public function beforeSave()
	{
		if ($this->collectionId) {
			$this->ressourceId = (int) $this->collection->ressourceId;
		}
		return parent::beforeSave();
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'partenaire' => [self::BELONGS_TO, 'Partenaire', 'partenaireId'],
			'collection' => [self::BELONGS_TO, 'Collection', 'collectionId'],
			'ressource' => [self::BELONGS_TO, 'Ressource', 'ressourceId'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'partenaireId' => 'Partenaire',
			'ressourceId' => 'Ressource',
			'collectionId' => 'Collection',
			'mask' => 'Type',
		];
	}

	/**
	 * Return "Globale" for an abonnement on a ressource, else the collection type.
	 *
	 * @return string
	 */
	public function getType()
	{
		if ($this->ressourceId) {
			return "Maîtresse";
		}
		if (isset($this->collectionType)) {
			return $this->collectionType;
		}
		if ($this->collectionId) {
			return $this->collection->type;
		}
		return "?";
	}
}
