<?php

/**
 * Iterable, so foreach() can be used.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class Liens implements \IteratorAggregate, \JsonSerializable
{
	/**
	 * @var Lien[]
	 */
	private $content = [];

	private $errors = [];

	private $hasUrlError = false;

	private $validated = false;

	/**
	 * Called automatically on `echo $list` and such.
	 */
	public function __toString(): string
	{
		if (!$this->content) {
			return '';
		}
		$html = '<ul class="autresliens">';
		foreach ($this->content as $item) {
			$html .= "<li>" . $item . "</li>\n";
		}
		$html .= '</ul>';
		return $html;
	}

	/**
	 * @return array
	 */
	public function getContent()
	{
		return $this->content;
	}

	/**
	 * @param string|array $items
	 * @return Liens
	 */
	public function setContent($items)
	{
		// reset
		$this->content = [];

		if (!$items) {
			return $this;
		}

		if (is_string($items)) {
			$items = json_decode($items);
		}
		if (is_array($items)) {
			// prepare each valid link
			$objects = [];
			foreach ($items as $item) {
				if (empty($item)) {
					continue;
				}
				if ($item instanceof Lien) {
					$new = $item;
				} elseif (is_array($item) || $item instanceof \stdClass) {
					$new = new Lien();
					$new->setAttributes((array) $item, false);
				} else {
					throw new \Exception("Liens.setContent(): Unknown type of item to add: " . print_r($item, true));
				}
				if (!$new->isEmpty()) {
					$objects[] = $new;
				}
			}

			// first pass for external links
			foreach ($objects as $o) {
				if (!$o->isInternal()) {
					$this->content[] = $o;
				}
			}
			// second pass for internal links
			foreach ($objects as $o) {
				if ($o->isInternal()) {
					$this->content[] = $o;
				}
			}
		}
		return $this;
	}

	/**
	 * Add an Item.
	 *
	 * @param mixed $item
	 * @return Liens
	 */
	public function add($item): Liens
	{
		if (!$item) {
			return $this;
		}
		if ($item instanceof Lien) {
			$new = $item;
		} elseif (is_array($item) || ($item instanceof \stdClass)) {
			$new = new Lien();
			$new->setAttributes((array) $item, false);
		} else {
			throw new Exception("Unknown type of item to add.");
		}
		if (!$new->isEmpty()) {
			if ($new->isInternal() || count($this->content) === 0) {
				$this->content[] = $new;
			} else {
				// find the first internal link
				$firstInternal = null;
				for ($i = 0; $i < count($this->content); $i++) {
					if ($this->content[$i]->isInternal()) {
						$firstInternal = $i;
						break;
					}
				}
				// insert the external link
				if ($firstInternal === null) {
					$this->content[] = $new;
				} else {
					array_splice($this->content, $firstInternal, 0, [$new]);
				}
			}
		}
		return $this;
	}

	/**
	 * Remove an Item.
	 *
	 * @param Lien $item
	 * @return bool
	 */
	public function remove(Lien $item): bool
	{
		foreach ($this->content as $pos => $link) {
			if ($link === $item) {
				unset($this->content[$pos]);
				$this->content = array_values($this->content);
				return true;
			}
		}
		return false;
	}

	/**
	 * @param bool $urlValidation
	 * @return bool
	 */
	public function validate(bool $urlValidation = true): bool
	{
		$this->validated = true;
		foreach ($this->content as $l) {
			if (!$urlValidation) {
				$l->disableUrlValidation();
			}
			$l->validate();
			if ($l->hasErrors()) {
				if ($l->src) {
					$this->hasUrlError = true;
					$this->errors[] = "[{$l->src}] : " . $l->getError('url');
				} else {
					$this->errors[] = "Un lien comporte une URL mais pas de source.";
				}
			}
		}
		return empty($this->errors);
	}

	/**
	 * @param string $url
	 * @return bool
	 */
	public function containsUrl($url)
	{
		foreach ($this->content as $c) {
			if ($c->url === $url) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @param string|int $source
	 * @return bool
	 */
	public function containsSource($source)
	{
		foreach ($this->content as $c) {
			if (is_int($source)) {
				if ((int) $c->sourceId === $source) {
					return true;
				}
			} else {
				if ($c->src === $source) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Sort the links by alphabetical order of their sources.
	 */
	public function sort()
	{
		usort($this->content, function ($a, $b) {
			return strcmp($a->src, $b->src);
		});
	}

	/**
	 * @return bool
	 */
	public function hasUrlError(bool $validate = true): bool
	{
		if (!$this->validated && $validate) {
			$this->validate();
		}
		return $this->hasUrlError;
	}

	/**
	 * @return string[]
	 */
	public function getErrors(bool $validate = true): array
	{
		if (!$this->validated && $validate) {
			$this->validate();
		}
		return $this->errors;
	}

	/**
	 * Applying `foreach` on this object will iterate on items (in `$this->content`).
	 *
	 * @return ArrayIterator
	 */
	public function getIterator()
	{
		return new ArrayIterator($this->content);
	}

	/**
	 * Called automatically on json_encode($list) and such.
	 */
	public function jsonSerialize(): array
	{
		return array_values($this->content);
	}

	/**
	 * Save a record for each link into Lien$parentTable.
	 *
	 * @param string $parentTable
	 * @param int $parentId
	 * @return int #affected rows
	 */
	public function save(string $parentTable, int $parentId): int
	{
		$db = Yii::app()->db;
		$table = "Lien$parentTable";
		$fkColumn = strtolower($parentTable) . "Id";
		$db->createCommand("DELETE FROM $table WHERE $fkColumn = :id")->execute([':id' => (int) $parentId]);
		if (empty($this->content)) {
			return 0;
		}
		$sql = "INSERT INTO $table ($fkColumn, sourceId, domain, name, url) VALUES";
		$placeholders = [];
		$values = [];
		foreach ($this->content as $link) {
			/* @var $link Lien */
			$placeholders[] = " (?, ?, ?, ?, ?)";

			// @todo Better domain extraction from the URL
			$domain = '';
			$m = [];
			if ($link->url[0] === '/') {
				$domain = "interne à Mir@bel";
			} elseif (preg_match('#^https?://(.+?)(?:/|$)#', $link->url, $m)) {
				$domain = preg_replace('#^www\.#', '', $m[1]);
			} elseif (preg_match('#^([A-Za-z.-]+)(?:/|$)#', $link->url, $m)) {
				$domain = preg_replace('#^www\.#', '', $m[1]);
			} else {
				$domain = "format inconnu";
			}

			array_push(
				$values,
				$parentId,
				$link->sourceId,
				$domain,
				$link->src,
				$link->url
			);
		}
		$affected = $db->createCommand($sql . join(",", $placeholders))->execute($values);
		if ($affected !== count($placeholders)) {
			Yii::log("SQL error while inserting links INTO Lien$parentTable / $parentId", CLogger::LEVEL_WARNING);
		}
		return $affected;
	}
}
