<?php

class TitreLiens implements ArrayAccess
{
	private $content = [];

	public function __construct($json = '')
	{
		if ($json) {
			if (is_array($json)) {
				$this->content = $json;
			} else {
				$this->content = json_decode($json, true);
			}
		}
	}

	public function toHtml()
	{
		if (empty($this->content)) {
			return '';
		}
		$html = '<ul>';
		foreach ($this->content as $link) {
			if (is_array($link) && isset($link['url'])) {
				$url = CHtml::encode($link['url']);
				$html .= sprintf("<li>%s : <a href=\"%s\">%s</a></li>\n", CHtml::encode($link['src']), $url, $url);
			} else {
				\Yii::log("Format de liens non-valide : " . print_r($this->content, true), CLogger::LEVEL_ERROR);
			}
		}
		$html .= '</ul>';
		return $html;
	}

	public static function htmlFormat($json)
	{
		$new = new TitreLiens($json);
		return $new->toHtml();
	}

	public function offsetSet($offset, $value)
	{
		$this->content[$offset] = $value;
	}

	public function offsetExists($offset)
	{
		return isset($this->content[$offset]);
	}

	public function offsetGet($offset)
	{
		return $this->content[$offset];
	}

	public function offsetUnset($offset)
	{
		unset($this->content[$offset]);
	}
}
