<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class CollectionListItem
{
	/**
	 * @var Collection
	 */
	public $collection;

	/**
	 * @var string
	 */
	public $ressourceNom;

	/**
	 * @var bool
	 */
	public $linked;

	public function __construct(array $row)
	{
		if (isset($row['linked'])) {
			$this->linked = (bool) $row['linked'];
			unset($row['linked']);
		} else {
			$this->linked =false;
		}
		if (isset($row['ressourceNom'])) {
			$this->ressourceNom = $row['ressourceNom'];
			unset($row['ressourceNom']);
		} else {
			$this->ressourceNom = '???';
		}
		$this->collection = Collection::model()->populateRecord($row, false);
	}

	/**
	 * Returns an array of CollectionListItem.
	 */
	public static function getPossibleCollections(int $titreId): array
	{
		if (!$titreId) {
			return [];
		}
		$sql = "SELECT c.*, r.nom AS ressourceNom, tc.titreId AS linked "
			. "FROM Collection c "
			. "JOIN Service s USING (ressourceId) JOIN Ressource r ON r.id=s.ressourceId "
			. "LEFT JOIN Titre_Collection tc ON (tc.collectionId=c.id AND tc.titreId={$titreId}) "
			. "WHERE s.titreId = {$titreId} "
			. "GROUP BY c.id ORDER BY c.nom";
		$list = Yii::app()->db->createCommand($sql)->queryAll(true);
		return self::toSelfArray($list);
	}

	/**
	 * Returns an array of array(collection.*=>, ressourceNom, linked).
	 */
	public static function getErroneousCollections(int $titreId): array
	{
		if (!$titreId) {
			return [];
		}
		$sql = "SELECT c.*, r.nom AS ressourceNom, 1 AS linked FROM Collection c "
			. "JOIN Titre_Collection tc ON (tc.collectionId=c.id AND tc.titreId={$titreId}) "
			. "JOIN Ressource r ON r.id=c.ressourceId "
			. "LEFT JOIN Service s ON (s.ressourceId = c.ressourceId AND s.titreId = {$titreId}) "
			. "WHERE s.id IS NULL "
			. "GROUP BY c.id ORDER BY c.nom";
		$list = Yii::app()->db->createCommand($sql)->queryAll(true);
		return self::toSelfArray($list);
	}

	/**
	 * Returns true if some collections have no service.
	 */
	public static function hasErroneousCollections(int $titreId): bool
	{
		if (!$titreId) {
			return false;
		}
		$sql = "SELECT 1 FROM Collection c "
			. "JOIN Titre_Collection tc ON (tc.collectionId=c.id AND tc.titreId={$titreId}) "
			. "LEFT JOIN Service s ON (s.ressourceId = c.ressourceId AND s.titreId = {$titreId}) "
			. "WHERE s.id IS NULL ";
		$list = Yii::app()->db->createCommand($sql)->queryAll(true);
		return (count($list) > 0);
	}

	protected static function toSelfArray(?array $rows): array
	{
		$result = [];
		foreach ($rows as $row) {
			$result[] = new CollectionListItem($row);
		}
		return $result;
	}
}
