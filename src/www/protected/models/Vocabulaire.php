<?php

/**
 * This is the model class for table "Vocabulaire".
 *
 * The followings are the available columns in table 'Vocabulaire':
 * @property string $id
 * @property string $titre
 * @property string $commentaire
 * @property string $hdateModif
 *
 * @property CategorieAlias[] $categorieAliases
 */
class Vocabulaire extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @return Vocabulaire the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Vocabulaire';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['titre, hdateModif', 'required'],
			['titre', 'length', 'max' => 255],
			['titre', 'unique', 'on' => 'insert'],
			['commentaire', 'safe'],
			// The following rule is used by search().
			['id, titre, commentaire, hdateModif', 'safe', 'on' => 'search'],
		];
	}

	public function beforeValidate()
	{
		$this->titre = trim($this->titre);
		return parent::beforeValidate();
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'categorieAliases' => [self::HAS_MANY, 'CategorieAlias', 'vocabulaireId'],
			'aliasCount' => [self::STAT, 'CategorieAlias', 'vocabulaireId'],
		];
	}

	public function getIndexationCount()
	{
		return $this->dbConnection->createCommand(
			"SELECT count(*) FROM CategorieAlias ca JOIN CategorieAlias_Titre ct ON ct.categorieAliasId = ca.id"
				. " WHERE ca.vocabulaireId = " . (int) $this->id
		)->queryScalar();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'titre' => 'Titre',
			'commentaire' => 'Commentaire',
			'hdateModif' => 'Hdate Modif',
			'aliasCount' => "Nombre d'alias",
			'indexationCount' => "Nombre d'indexations",
		];
	}

	/**
	 * @return array hints for the attributes (name=>hint)
	 */
	public function getHints()
	{
		return [
			'id' => "",
			'titre' => "",
			'commentaire' => "",
			'hdateModif' => "",
		];
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @param int|bool $pageSize Number of items for pagination (false to disable pagination).
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($pageSize=25)
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('titre', $this->titre, true);
		$criteria->compare('commentaire', $this->commentaire, true);
		$criteria->compare('hdateModif', $this->hdateModif, true);

		$sort = new CSort();
		$sort->attributes = ['id', 'titre', 'commentaire', 'hdateModif'];
		//$sort->defaultOrder = 'id ASC';

		return new CActiveDataProvider(
			$this,
			[
				'criteria' => $criteria,
				'pagination' => ['pageSize' => $pageSize],
				'sort' => $sort,
			]
		);
	}

	/**
	 * Return the CSV as a string.
	 *
	 * @param string $separator
	 * @return string
	 */
	public static function exportToCsv($separator = ';')
	{
		$vocabulaires = Vocabulaire::model()->findAll();
		if (!$vocabulaires) {
		}
		$selects = ['c.id', 'c.categorie'];
		$tables = ["Categorie c"];
		$columns = ["ID", "Thème"];
		$k = 1;
		foreach ($vocabulaires as $vocab) {
			$selects[] = "GROUP_CONCAT(DISTINCT ca{$k}.alias SEPARATOR '%%%')";
			$tables[] = " CategorieAlias ca{$k} "
				. "ON (ca{$k}.categorieId = c.id AND ca{$k}.vocabulaireId = {$vocab->id}) ";
			$columns[] = $vocab->titre;
			$k++;
		}
		$sql = "SELECT " . join(',', $selects)
			. " FROM " . join(" LEFT JOIN ", $tables)
			. " WHERE c.id > 1 "
			. " GROUP BY c.id "
			. " ORDER BY c.id";
		$cmd = Yii::app()->db->createCommand($sql);
		$out = fopen('php://temp', 'w');
		fputcsv($out, $columns, $separator);
		foreach ($cmd->queryAll(false) as $row) {
			$demult = [ [$row[0], $row[1]] ];
			$maxSubs = 1;
			// denormalize
			for ($j = 2; $j < count($row); $j++) {
				$subs = explode('%%%', $row[$j]);
				if (count($subs) > $maxSubs) {
					$maxSubs = count($subs);
				}
				$k = 0;
				foreach ($subs as $sub) {
					if (!isset($demult[$k])) {
						$demult[$k] = [$row[0], $row[1]];
					}
					$demult[$k][$j] = $sub;
					$k++;
				}
			}
			// fill up empty cells
			for ($i = 0; $i < $maxSubs; $i++) {
				for ($j = 2; $j < count($row); $j++) {
					if (!isset($demult[$i][$j])) {
						$demult[$i][$j] = '';
					}
				}
			}
			// write denormalized lines
			foreach ($demult as $d) {
				fputcsv($out, $d, $separator);
			}
		}
		$result = stream_get_contents($out, -1, 0);
		fclose($out);
		return $result;
	}
}
