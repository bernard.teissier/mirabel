<?php

/**
 * This is the model class for table "Partenaire".
 *
 * @todo auto-fill 'hash' on insert
 * @todo declare relations 'utilisateurs' and 'suivis' (or getSuivis() to be more versatile)
 * @todo create function getTitres($bouquet=null)
 *
 * The followings are the available columns in table 'Partenaire':
 * @property int $id
 * @property string $nom
 * @property string $sigle
 * @property string $type
 * @property string $description
 * @property string $prefixe
 * @property string $url
 * @property string $email
 * @property ?string $importEmail
 * @property string $logoUrl
 * @property string $phrasePerso
 * @property ?int   $paysId
 * @property string $geo
 * @property string $latitude float
 * @property string $longitude float
 * @property string $hash
 * @property string $ipAutorisees
 * @property string $possessionUrl
 * @property string $proxyUrl
 * @property ?int   $editeurId null unless partenaire-editeur
 * @property bool   $fusionAcces
 * @property int $hdateCreation timestamp
 * @property int $hdateModif timestamp
 * @property string $statut
 *
 * @property string $fullName
 * @property PartenaireTitre $partenaireTitre
 * @property Utilisateur[] $utilisateurs
 * @property Possession[] $possessions
 * @property ?Editeur $editeur
 * @property ?Pays $pays
 *
 * @method Partenaire ordered() Cf scopes
 */
class Partenaire extends CActiveRecord
{
	public const PATTERN_IDLOCAL = 'IDLOCAL';

	public const PATTERN_URL = 'URL';

	public static $enumType = [
		'normal' => 'Normal', 'import' => 'Import', 'editeur' => 'Éditeur',
	];

	public static $enumStatut = [
		'actif' => 'Actif', 'inactif' => 'Inactif', 'provisoire' => 'Provisoire', 'démo' => 'Démonstration',
	];

	public function behaviors()
	{
		return [
			'timestampToDate' => [
				'class' => 'application.models.behaviors.TimestampToDate',
				'fieldsDateTime' => ['hdateCreation', 'hdateModif'],
			],
		];
	}

	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @return Partenaire the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Partenaire';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['nom', 'required'],
			['email', 'required', 'on' => 'partenaire-editeur'],
			['nom, sigle, prefixe, ipAutorisees, email, geo, phrasePerso', 'length', 'max' => 255],
			['url, logoUrl, possessionUrl', 'url'],
			['url, logoUrl, possessionUrl', 'length', 'max' => 512],
			['email, importEmail', 'length', 'max' => 255],
			['email, importEmail', 'ext.validators.EmailsValidator'],
			['possessionUrl', 'validateUrlPossession'],
			['proxyUrl', 'validateUrlProxy'],
			['fusionAcces', 'boolean'],
			['description', 'length', 'max' => 65535],
			['type', 'in', 'range' => array_keys(self::$enumType)], // enum
			['statut', 'in', 'range' => array_keys(self::$enumStatut)], // enum
			['paysId', 'numerical', 'integerOnly' => true],
			['latitude, longitude', 'numerical', 'integerOnly' => false],
			['hash, url, logoUrl, possessionUrl', 'default', 'value' => ""],
			['email, importEmail', 'filter', 'filter' => 'trim'],
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			['nom, type, description, email, hdateCreation, hdateModif, statut', 'safe', 'on' => 'search'],
		];
	}

	/**
	 * Validates the attribute "possessionUrl".
	 * @param string $attr
	 */
	public function validateUrlPossession($attr)
	{
		if (!$this->hasErrors() && $this->{$attr} && strpos($this->{$attr}, self::PATTERN_IDLOCAL) === false) {
			$this->addError(
				$attr,
				"L'URL doit contenir le texte \"" . self::PATTERN_IDLOCAL . "\" que Mir@bel remplacera ensuite par l'identifiant local de chaque titre possédé."
			);
		}
	}

	/**
	 * Validates the attribute "proxyUrl".
	 * @param string $attr
	 */
	public function validateUrlProxy($attr)
	{
		if (!$this->hasErrors() && $this->{$attr} && strpos($this->{$attr}, self::PATTERN_URL) === false) {
			$this->addError(
				$attr,
				"L'URL doit contenir \"" . self::PATTERN_URL . "\" qui sera remplacé à chaque usage du proxy."
			);
		}
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'utilisateurs' => [
				self::HAS_MANY,
				'Utilisateur',
				'partenaireId',
				'order' => 'utilisateurs.nom, utilisateurs.prenom, utilisateurs.login',
			],
			'possessions' => [self::HAS_MANY, 'Possession', 'partenaireId'],
			'editeur' => [self::BELONGS_TO, 'Editeur', 'editeurId'],
			'pays' => [self::BELONGS_TO, 'Pays', 'paysId'],
		];
	}

	/**
	 * Cf <http://www.yiiframework.com/doc/guide/1.1/en/database.ar#named-scopes>
	 */
	public function scopes(): array
	{
		return [
			'ordered' => [
				'order' => 'nom ASC',
			],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'nom' => 'Nom',
			'sigle' => 'Nom court',
			'type' => 'Type',
			'description' => 'Détail',
			'prefixe' => 'Préfixe',
			'url' => 'Site web',
			'email' => 'Courriel',
			'importEmail' => "Courriel des imports",
			'logoUrl' => 'URL du logo',
			'phrasePerso' => "Phrase de personnalisation",
			'paysId' => "Pays",
			'geo' => "Ville ou lieu",
			'latitude' => "Latitude",
			'longitude' => "Longitude",
			'hash' => 'Clé API',
			'ipAutorisees' => 'IP locales',
			'possessionUrl' => 'URL des possessions',
			'proxyUrl' => 'URL du proxy',
			'editeurId' => "Éditeur associé",
			'fusionAcces' => 'Fusion des accès',
			'statut' => 'Statut',
			'hdateCreation' => 'Création',
			'hdateModif' => 'Dernière modification',
		];
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @param int|bool $pageSize Number of items for pagination (false to disable pagination).
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($pageSize=25)
	{
		$criteria = new CDbCriteria;

		$criteria->compare('nom', $this->nom, true);
		$criteria->compare('type', $this->type);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('email', $this->email, true);
		$criteria->addCondition(DateVersatile::buildTsConditionFromDateValue('hdateCreation', (string) $this->hdateCreation));
		$criteria->addCondition(DateVersatile::buildTsConditionFromDateValue('hdateModif', (string) $this->hdateModif));
		$criteria->compare('statut', $this->statut);

		$sort = new CSort();
		$sort->attributes = ['nom', 'type', 'hdateCreation', 'hdateModif', 'statut'];
		$sort->defaultOrder = 'nom ASC';

		return new CActiveDataProvider(
			$this,
			[
				'criteria' => $criteria,
				'pagination' => ['pageSize' => $pageSize],
				'sort' => $sort,
			]
		);
	}

	/**
	 * Returns the full name, including the prefix with correct spacing.
	 *
	 * @return string Full title.
	 */
	public function getFullName()
	{
		return $this->prefixe . $this->nom;
	}

	public function getShortName()
	{
		return empty($this->sigle) ? $this->nom : $this->sigle;
	}

	/**
	 * Returns a HTML link.
	 *
	 * @codeCoverageIgnore
	 * @return string HTML link.
	 */
	public function getSelfLink($short = false)
	{
		return CHtml::link(
			$short ? $this->getShortName() : $this->nom,
			['/partenaire/view', 'id' => $this->id]
		);
	}

	/**
	 * Returns an assoc array('Suivi' => array(1, 2, 5), 'Ressource' => , 'Editeur' => ).
	 *
	 * @param bool $includeEditeurs If true (suiviPartenairesEditeurs), shows 'Editeur' data.
	 * @return array
	 */
	public function getRightsSuivi($includeEditeurs = true)
	{
		if (empty($this->id)) {
			return [];
		}
		$rows = $this->getDbConnection()->createCommand(
			"SELECT cible, cibleId FROM Suivi WHERE partenaireId = {$this->id} "
			. ($includeEditeurs ? '' : " AND cible != 'Editeur'")
			. " ORDER BY cible, cibleId"
		)->queryAll();
		$suivi = [];
		foreach ($rows as $row) {
			if (!isset($suivi[$row['cible']])) {
				$suivi[$row['cible']] = [];
			}
			$suivi[$row['cible']][] = (int) $row['cibleId'];
		}
		return $suivi;
	}

	/**
	 * Checks if this Partenaire can have records in Partenaire_Titre.
	 *
	 * @return bool
	 */
	public function canOwnTitles()
	{
		if ($this->type === 'normal') {
			return true;
		}
		return false;
	}

	/**
	 * Returns nombre de revues suivi par le partenaire
	 *
	 * @return int
	 */
	public function countRevuesSuivies()
	{
		if (empty($this->id)) {
			return 0;
		}
		$sql = "SELECT count(*) AS nbRevues "
			. "FROM Suivi s "
			. "WHERE s.cible = 'Revue' AND s.partenaireId = {$this->id}";
		$cmd = $this->getDbConnection()->createCommand($sql)->queryRow();
		return $cmd['nbRevues'];
	}

	/**
	 * Returns a assoc array(<type> => array(partenaire1, partenaire2...)).
	 *
	 * @return array assoc array(<type> => array(partenaire1, partenaire2...)).
	 */
	public static function getListByType()
	{
		$criteria = [
			'condition' => "statut = 'actif'",
			'order' => 'type, nom',
		];
		$partenaires = self::model()->findAll($criteria);
		$ordered = [];
		foreach ($partenaires as $p) {
			if (!isset($ordered[$p->type])) {
				$ordered[$p->type] = [];
			}
			$ordered[$p->type][] = $p;
		}
		return $ordered;
	}

	/**
	 * Returns the ID of the first Partenaire whose IP ranges matches the given one.
	 *
	 * @return int partenaireId or 0.
	 */
	public static function findInstituteByIp($ip)
	{
		$masks = Yii::app()->db
			->createCommand("SELECT id, ipAutorisees FROM Partenaire WHERE statut <> 'inactif' AND ipAutorisees != ''")
			->query();
		foreach ($masks as $mask) {
			if (Tools::matchIpFilter($ip, $mask['ipAutorisees'])) {
				return (int) $mask['id'];
			}
		}
		return 0;
	}

	/**
	 * Returns a link for the current Partenaire and this Titre.
	 * @param Titre $titre
	 * @return string URL
	 */
	public function buildPossessionUrl($titre, $idLocal=null)
	{
		if (empty($this->id) || empty($titre->id)) {
			return "";
		}
		if (!empty($this->possessionUrl) && $titre->belongsTo($this->id)) {
			if (empty($idLocal)) {
				$idLocal = $this->dbConnection->createCommand(
					"SELECT identifiantLocal FROM Partenaire_Titre WHERE partenaireId = {$this->id}"
					. " AND titreId = {$titre->id} LIMIT 1"
				)->queryScalar();
			}
			if ($idLocal) {
				return str_replace(self::PATTERN_IDLOCAL, $idLocal, $this->possessionUrl);
			}
		}
		return "";
	}

	/**
	 * Helper function that returns an IMG if possible (checks if the file exists).
	 *
	 * @codeCoverageIgnore
	 * @param bool $reduced
	 * @param array $htmlOptions
	 * @return string HTML img or plain text.
	 */
	public function getLogoImg($reduced = true, $htmlOptions = [])
	{
		$url = $this->getLogoUrl($reduced);
		if ($url) {
			return CHtml::image($url, "logo " . $this->nom, $htmlOptions);
		}
		if ($this->logoUrl) {
			return CHtml::image($this->logoUrl, "logo " . $this->nom, $htmlOptions);
		}
		return '';
	}

	/**
	 * Helper function that returns the URL or an empty string.
	 *
	 * @codeCoverageIgnore
	 * @param bool $reduced
	 * @return string URL
	 */
	public function getLogoUrl($reduced = true)
	{
		$relativePath = '/images/logos/' . ($reduced ? 'h55/' : '');
		$path = dirname(Yii::app()->basePath) . $relativePath;
		$url = '';
		if (file_exists($path . sprintf('%03d.png', $this->id))) {
			$url = sprintf('%s%s%03d.png', Yii::app()->getBaseUrl(true), $relativePath, $this->id);
		} elseif (file_exists($path . sprintf('%03d.jpg', $this->id))) {
			$url = sprintf('%s%s%03d.jpg', Yii::app()->getBaseUrl(true), $relativePath, $this->id);
		}
		return $url;
	}

	/**
	 * @return bool
	 */
	public function hasAbonnements()
	{
		return Abonnement::model()->exists('partenaireId = ' . (int) $this->id);
	}

	public function listConventionFiles(): array
	{
		$uploadDir = Yii::app()->getBasePath() . '/data/upload';
		if (!is_dir($uploadDir) || !is_readable($uploadDir)) {
			return [];
		}
		$cwd = getcwd();
		chdir($uploadDir);
		$files = glob(sprintf("conventions/%03d*", $this->id));
		chdir($cwd);
		return $files;
	}

	public static function getRandomHash()
	{
		return md5(str_shuffle(md5(microtime())));
	}

	public function onUnsafeAttribute($name, $value)
	{
		// do nothing
	}

	/**
	 * Called automatically before validate().
	 *
	 * @return bool
	 */
	protected function beforeValidate()
	{
		if (strlen($this->prefixe)) {
			$this->prefixe = str_replace(["’", "´"], "'", $this->prefixe);
			if (preg_match('/[\'-]\s*$/', $this->prefixe)) {
				$this->prefixe = trim($this->prefixe);
			} elseif (!preg_match('/[\s ]$/', $this->prefixe)) {
				$this->prefixe = trim($this->prefixe) . " ";
			}
		}
		if ($this->statut === 'inactif') {
			$users = $this->utilisateurs;
			if ($users) {
				foreach ($users as $u) {
					/* @var $u Utilisateur */
					if ($u->actif) {
						$this->addError('statut', "Un partenaire ayant des utilisateurs actifs ne peut être inactif.");
						break;
					}
				}
			}
			$suivi = $this->getRightsSuivi();
			if ($suivi) {
				$this->addError('statut', "Un partenaire ayant des suivis ne peut être inactif.");
			}
		}
		if ($this->nom) {
			$this->nom = str_replace(["’", "´"], "'", trim($this->nom));
		}
		if ($this->editeurId && $this->statut !== 'inactif' && empty($this->email)) {
			$this->addError('email', "Un éditeur-partenaire doit avoir une adresse électronique de contact.");
		}
		if ($this->latitude) {
			$this->latitude = str_replace(',', '.', trim($this->latitude));
			if ($this->latitude < -90 || $this->latitude > 90) {
				$this->addError('latitude', "La latitude doit être comprise entre -90 et 90°.");
			}
		} else {
			$this->latitude = null;
		}
		if ($this->longitude) {
			$this->longitude = str_replace(',', '.', trim($this->longitude));
			if ($this->longitude < -180 || $this->longitude > 180) {
				$this->addError('longitude', "La longitude doit être comprise entre -180 et 180°.");
			}
		} else {
			$this->longitude = null;
		}
		return parent::beforeValidate();
	}

	/**
	 * Called automatically before Save().
	 *
	 * @return bool
	 */
	protected function beforeSave()
	{
		if ($this->getIsNewRecord()) {
			$this->hdateCreation = $_SERVER['REQUEST_TIME'];
		}
		$this->hdateModif = $_SERVER['REQUEST_TIME'];
		$this->nom = Tools::normalizeText($this->nom);
		$this->description = Tools::normalizeText($this->description);
		if ($this->paysId === '') {
			$this->paysId = null;
		}
		if ($this->importEmail === '') {
			$this->importEmail = null;
		}
		return parent::beforeSave();
	}
}
