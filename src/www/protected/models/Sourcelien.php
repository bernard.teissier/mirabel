<?php

/**
 * This is the model class for table "Sourcelien".
 *
 * The followings are the available columns in table 'Editeur':
 * @property int $id
 * @property string $nom
 * @property string $nomcourt
 * @property string $nomlong
 * @property string $url
 * @property bool $saisie
 * @property int $hdateCrea timestamp
 * @property int $hdateModif timestamp
 */
class Sourcelien extends CActiveRecord
{
	public const CACHE_DURATION = 3600;

	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @return Sourcelien the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Sourcelien';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['nom', 'required'],
			['nom, nomlong', 'length', 'max' => 255],
			['url', 'length', 'max' => 512],
			['nomcourt', 'length', 'max' => 50],
			['saisie', 'boolean'],
			// The following rule is used by search().
			['nom, nomlong, nomcourt, url, hdateCrea, hdateModif', 'safe', 'on' => 'search'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'nom' => 'Nom',
			'nomlong' => 'Nom long',
			'nomcourt' => 'Nom court',
			'url' => "Domaine d'URL",
			'saisie' => 'Proposer en saisie',
			'hdateCrea' => 'Date de création',
			'hdateModif' => 'Dernière modification',
		];
	}

	/**
	 * Each array key defines a function that can be chained before a find*() method.
	 */
	public function scopes()
	{
		return [
			'sorted' => [
				'order' => 'nom ASC',
			],
		];
	}

	/**
	 * @return bool
	 */
	public function hasLogo()
	{
		$path = dirname(Yii::app()->getBasePath()) . '/images/liens';
		return glob(sprintf('%s/%06d.*', $path, $this->id))
			|| glob(sprintf('%s/%s.*', $path, $this->nomcourt));
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @param int|bool $pageSize Number of items for pagination (false to disable pagination).
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($pageSize = 25)
	{
		$criteria = new CDbCriteria;

		$criteria->compare('nom', $this->nom, true);
		$criteria->compare('nomlong', $this->nomlong, true);
		$criteria->compare('nomcourt', $this->nomcourt, true);
		$criteria->compare('url', $this->nom, true);
		$criteria->addCondition(DateVersatile::buildTsConditionFromDateValue('hdateCrea', (string) $this->hdateCrea));
		$criteria->addCondition(DateVersatile::buildTsConditionFromDateValue('hdateModif', (string) $this->hdateModif));

		$sort = new CSort();
		$sort->attributes = ['nom', 'hdateCrea', 'hdateModif'];
		$sort->defaultOrder = 'nom ASC';

		return new CActiveDataProvider(
			$this,
			[
				'criteria' => $criteria,
				'pagination' => ['pageSize' => $pageSize],
				'sort' => $sort,
			]
		);
	}

	public static function listNames()
	{
		return Yii::app()->db
			->createCommand(
				"SELECT IF(nomlong != '' AND nomlong != nom, CONCAT(nom, ' — ', nomlong), nom) AS label, nom AS value"
				. " FROM Sourcelien WHERE saisie = 1 ORDER BY nom"
			)
			->queryAll(true);
	}

	/**
	 * @return stdClass { names: [], domains: [] }
	 */
	public static function listFrequenciesEditeurs()
	{
		$cacheName = "Sourcelien::listFrequencies_Editeurs";
		$db = Yii::app()->db;
		$value = Yii::app()->cache->get($cacheName);
		if ($value === false) {
			$value = (object) [
				'names' => $db
					->createCommand(
						"SELECT name, count(*) as num FROM LienEditeur GROUP BY name HAVING count(*) > 1 ORDER BY count(*) DESC, name"
					)->queryAll(),
				'domains' => $db
					->createCommand(
						"SELECT domain, count(*) as num FROM LienEditeur GROUP BY domain HAVING count(*) > 1 ORDER BY count(*) DESC, domain"
					)->queryAll(),
			];
			Yii::app()->cache->set($cacheName, $value, self::CACHE_DURATION);
		}
		return $value;
	}

	/**
	 * @return stdClass { names: [], domains: [] }
	 */
	public static function listFrequenciesTitres()
	{
		$cacheName = "Sourcelien::listFrequencies_Titres";
		$db = Yii::app()->db;
		$value = Yii::app()->cache->get($cacheName);
		if ($value === false) {
			$value = (object) [
				'names' => $db
					->createCommand(
						<<<EOSQL
						SELECT l.name, count(*) AS num, count(DISTINCT revueId) AS numRevues
						FROM LienTitre l
							JOIN Titre t ON l.titreId = t.id
						GROUP BY l.name
							HAVING count(*) > 1
						ORDER BY count(*) DESC, l.name
						EOSQL
					)->queryAll(),
				'domains' => $db
					->createCommand(
						<<<EOSQL
						SELECT l.domain, count(*) AS num, count(DISTINCT revueId) AS numRevues
						FROM LienTitre l
							JOIN Titre t ON l.titreId = t.id
						GROUP BY l.domain
							HAVING count(*) > 1
						ORDER BY count(*) DESC, l.domain
						EOSQL
					)->queryAll(),
			];
			Yii::app()->cache->set($cacheName, $value, self::CACHE_DURATION);
		}
		return $value;
	}

	/**
	 * @param string $table
	 * @param string $domain
	 * @return array Each row is an array with keys revueId, name, domain, url
	 */
	public static function listByDomain($table, $domain)
	{
		$cacheName = "Sourcelien::listByDomain_$table$domain";
		$value = Yii::app()->cache->get($cacheName);
		if ($value === false) {
			if ($table === 'Titre') {
				$sql = "SELECT l.name, l.url, t.titre, t.revueId FROM Lien$table l JOIN Titre t ON l.titreId = t.id WHERE domain LIKE ? ORDER BY t.titre, l.name";
			} else {
				$sql = "SELECT l.name, l.url, e.nom, e.id AS editeurId FROM Lien$table l JOIN Editeur e ON l.editeurId = e.id WHERE domain LIKE ? ORDER BY e.nom, l.name";
			}
			$value = Yii::app()->db
				->createCommand($sql)
				->queryAll(true, ['%' . $domain]);
			Yii::app()->cache->set($cacheName, $value, self::CACHE_DURATION);
		}
		return $value;
	}

	/**
	 * @param string $table
	 * @param string $name
	 * @return array Each row is an array with keys revueId, name, domain, url
	 */
	public static function listByName($table, $name)
	{
		$cacheName = "Sourcelien::listByName_$table$name";
		$value = Yii::app()->cache->get($cacheName);
		if ($value === false) {
			if ($table === 'Titre') {
				$sql = "SELECT l.name, l.url, t.titre, t.revueId FROM Lien$table l JOIN Titre t ON l.titreId = t.id WHERE name = ? ORDER BY t.titre, l.name";
			} else {
				$sql = "SELECT l.name, l.url, e.nom, e.id AS editeurId FROM Lien$table l JOIN Editeur e ON l.editeurId = e.id WHERE name LIKE ? ORDER BY e.nom, l.name";
			}
			$value = Yii::app()->db
				->createCommand($sql)
				->queryAll(true, [$name]);
			Yii::app()->cache->set($cacheName, $value, self::CACHE_DURATION);
		}
		return $value;
	}

	public static function identifyUrl(string $url): ?Sourcelien
	{
		$m = [];
		if (preg_match('#^(?:https?://)?([^/]+)(?:/|$)#', $url, $m)) {
			$host = $m[1];
			$domain = join('.', array_slice(explode('.', $host), -2));
			return Sourcelien::model()->find("url = ? OR url = ?", [$host, $domain]);
		}
		return null;
	}

	public function getDomain(): string
	{
		if (strpos($this->url, '/') === false) {
			return $this->url;
		}
		$m = [];
		if (preg_match('#//([^/]+)(/|$)#', $this->url, $m)) {
			return $m[1];
		}
		return '';
	}

	/**
	 * @return string HTML img
	 */
	public function getLogo()
	{
		$image = null;
		$path = dirname(Yii::app()->getBasePath()) . '/images/liens';
		$url = Yii::app()->getBaseUrl() . '/images/liens';

		$byId = glob(sprintf('%s/%06d.*', $path, $this->id));
		if ($byId) {
			$image = "$url/{$byId[0]}";
		} else {
			$byName = glob(sprintf('%s/%s.*', $path, $this->id));
			if ($byName) {
				$image = "$url/{$byName[0]}";
			}
		}
		return ($image ? CHtml::image($image, $this->nom) : '');
	}

	/**
	 * Called automatically before Save().
	 *
	 * @return bool
	 */
	protected function beforeSave()
	{
		if ($this->isNewRecord) {
			$this->hdateCrea = $_SERVER['REQUEST_TIME'];
		}
		$this->hdateModif = $_SERVER['REQUEST_TIME'];
		$this->nom = Norm::text($this->nom);
		if (!$this->nomcourt) {
			$this->nomcourt = strtolower(preg_replace('/[\[(].+|[^a-zA-Z0-9]+/', '', Norm::unaccent($this->nom)));
		} else {
			$this->nomcourt = strtolower(Norm::unaccent($this->nomcourt));
		}
		if (strpos($this->url, "://") !== false && preg_match('#://(.+?)(?:/|$)#', $this->url, $m)) {
			$this->url = $m[1];
		}
		return parent::beforeSave();
	}
}
