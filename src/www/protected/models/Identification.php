<?php

/**
 * This is the model class for table "Identification".
 *
 * The followings are the available columns in table 'Identification':
 * @property int $id
 * @property int $titreId
 * @property int $ressourceId
 * @property string $idInterne
 *
 * @property Titre $titre
 * @property Ressource $ressource
 */
class Identification extends CActiveRecord implements IWithIndirectSuivi
{
	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @return Identification the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Identification';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['titreId, ressourceId', 'required'],
			['titreId, ressourceId', 'numerical', 'integerOnly' => true],
			['idInterne', 'length', 'max' => 255],
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'titre' => [self::BELONGS_TO, 'Titre', 'titreId'],
			'ressource' => [self::BELONGS_TO, 'Ressource', 'ressourceId'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'titreId' => 'Titre',
			'ressourceId' => 'Ressource',
			'idInterne' => 'Id Interne',
		];
	}

	/**
	 * Returns an array of parents that have a direct "Suivi": [ [table => ,  id => ], ... ]
	 *
	 * @return array
	 */
	public function listParentsForSuivi()
	{
		return [
			['table' => 'Titre', 'id' => $this->titreId],
			['table' => 'Ressource', 'id' => $this->ressourceId],
		];
	}
}
