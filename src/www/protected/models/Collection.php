<?php

/**
 * This is the model class for table "Collection".
 *
 * The followings are the available columns in table 'Collection':
 * @property int $id
 * @property int $ressourceId
 * @property string $nom
 * @property string $identifiant
 * @property string $description
 * @property string $url
 * @property string $type
 * @property bool $visible
 * @property bool $importee
 * @property int $exhaustif
 * @property string $hdateCreation
 * @property string $hdateModif
 *
 * @property Ressource $ressource
 * @property array $services Service[]
 */
class Collection extends AMonitored implements IWithIndirectSuivi
{
	public const EXHAUSTIF_NON = 0;

	public const EXHAUSTIF_OUI = 1;

	public const EXHAUSTIF_INDET = 2;

	public const TYPE_COURANT = "courant";

	public const TYPE_ARCHIVE = "archive";

	public const TYPE_LICNATFRANCE = "licence nationale France";

	public const TYPE_TEMPORAIRE = "temporaire";

	public static $enumExhaustif = [
		self::EXHAUSTIF_INDET => '?',
		self::EXHAUSTIF_NON => 'non',
		self::EXHAUSTIF_OUI => 'oui',
	];

	/**
	 * @var array
	 */
	public static $types = [self::TYPE_COURANT, self::TYPE_ARCHIVE, self::TYPE_LICNATFRANCE, self::TYPE_TEMPORAIRE];

	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @return Collection the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Collection';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['nom, type, ressourceId', 'required'],
			['ressourceId, exhaustif', 'numerical', 'integerOnly' => true],
			['nom, identifiant', 'length', 'max' => 255],
			['url', 'url'],
			['url', 'length', 'max' => 512],
			['type', 'in', 'range' => self::$types],
			['visible, importee', 'boolean'],
			['url', 'ext.validators.UrlFetchableValidator', 'on' => ['insert', 'update']],
			['description', 'length', 'max' => 65535],
			[
				'nom',
				'unique',
				'criteria' => [
					'condition' => 'ressourceId = :rid',
					'params' => [':rid' => $this->ressourceId],
				],
			],
			['exhaustif', 'default', 'value' => self::EXHAUSTIF_INDET, 'except' => 'search', 'setOnEmpty' => true],
			// The following rule is used by search().
			['ressourceId, nom, description, type, visible, hdateCreation, hdateModif', 'safe', 'on' => 'search'],
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'ressource' => [self::BELONGS_TO, 'Ressource', 'ressourceId'],
			'services' => [self::HAS_MANY, 'ServiceCollection', 'collectionId'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'ressourceId' => 'Ressource',
			'nom' => 'Nom',
			'identifiant' => "Identifiant d'import",
			'description' => 'Description',
			'url' => 'URL',
			'type' => 'Type',
			'visible' => 'Collection publique',
			'importee' => "Importée",
			'exhaustif' => "Exhaustif",
		];
	}

	/**
	 * Returns a list of assoc (id =>, value=> , label=>) matching the requested term.
	 *
	 * @param string $term
	 * @param int $ressourceId (opt)
	 * @return array Array of assoc (id => , value=> , label=>).
	 */
	public static function completeTerm($term, $ressourceId = 0)
	{
		$collections = Collection::model()->findAll(
			[
				'condition' => "nom LIKE :term" . ($ressourceId > 0 ? " AND ressourceId = " . (int) $ressourceId : '')
					. " AND visible = 1",
				'order' => 'nom ASC',
				'params' => [ ':term' => '%' . $term . '%' ],
			]
		);
		$results = [];
		foreach ($collections as $c) {
			$results[] = [ 'id' => $c->id, 'label' => $c->nom, 'value' => $c->nom ];
		}
		return $results;
	}

	/**
	 * Returns the name suffixed with the type if non-standard.
	 *
	 * @return string
	 */
	public function getName()
	{
		$name = $this->nom;
		if ($this->type !== self::TYPE_COURANT) {
			$name .= " ({$this->type})";
		}
		if ($this->importee) {
			$name .= " (importée)";
		}
		return $name;
	}

	/**
	 * Returns the name prefixed with the resource's name.
	 *
	 * @return string
	 */
	public function getFullName()
	{
		if ($this->ressourceId) {
			return ($this->ressource->sigle ?: $this->ressource->nom)
				. ' — ' . $this->nom;
		}
		return $this->nom;
	}

	/**
	 * @param bool $withParentName (opt, true)
	 * @return string HTML
	 */
	public function getSelfLink($withParentName = true)
	{
		return CHtml::link(
			CHtml::encode($withParentName ? $this->getFullName() : $this->nom),
			['/collection/view', 'id' => $this->id]
		);
	}

	/**
	 * Builds an Intervention object that can be completed later.
	 */
	public function buildIntervention(bool $direct): Intervention
	{
		if (!$this->ressourceId) {
			throw new \Exception("Collection sans ressource.");
		}
		$i = parent::buildIntervention($direct);
		$i->ressourceId = $this->ressourceId;
		return $i;
	}

	/**
	 * @param int $type (opt, Abonnement::ABONNE)
	 * @return array Abonnement[]
	 */
	public function getAbonnements($type = Abonnement::ABONNE)
	{
		if (empty($this)) {
			return [];
		}
		$criteria = new CDbCriteria;
		$criteria->join = "JOIN Partenaire p ON p.id = partenaireId";
		$criteria->order = "mask ASC, p.nom ASC";
		$criteria->addColumnCondition(['collectionId' => $this->id]);
		if ($type) {
			$criteria->addColumnCondition(['mask' => $type]);
		}
		return Abonnement::model()->with('partenaire')->findAll($criteria);
	}

	public function listParentsForSuivi()
	{
		return [
			['table' => 'Ressource', 'id' => $this->ressourceId],
		];
	}

	/**
	 * Called automatically before Save().
	 *
	 * @return bool
	 */
	protected function beforeSave()
	{
		$this->nom = Tools::normalizeText($this->nom);
		$this->description = Tools::normalizeText($this->description);
		if (!$this->ressourceId) {
			$this->ressourceId = null;
		}
		if ($this->type === self::TYPE_TEMPORAIRE) {
			$this->visible = 0;
		} else {
			$this->visible = 1;
		}
		if ($this->isNewRecord) {
			$this->hdateCreation = $_SERVER['REQUEST_TIME'];
		} else {
			$this->hdateModif = $_SERVER['REQUEST_TIME'];
		}
		return parent::beforeSave();
	}
}
