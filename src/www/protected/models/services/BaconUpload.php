<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class BaconUpload
{
	private $dirUrl;

	private $username;

	private $password;

	public function __construct($dirUrl, $username, $password)
	{
		$this->dirUrl = rtrim($dirUrl, '/');
		$this->username = $username;
		$this->password = $password;
	}

	public function run()
	{
		$this->upload(self::getLibreKbartContent(), self::getLibreFileName());
		$this->upload(self::getDoajKbartContent(), self::getDoajFileName());
		$this->upload(self::getGlobalKbartContent(), self::getGlobalFileName());
	}

	private function upload($content, $filename)
	{
		$file = self::createTempFile($content);
		$curl = curl_init();
		curl_setopt_array($curl, [
			// config for an upload
			CURLOPT_PUT => true,
			CURLOPT_UPLOAD => true,
			// destination (URL to a file, not a directory)
			CURLOPT_URL => "{$this->dirUrl}/{$filename}",
			// content
			CURLOPT_INFILE => $file,
			CURLOPT_INFILESIZE => strlen($content),
			// auth
			CURLOPT_USERNAME => $this->username,
			CURLOPT_PASSWORD => $this->password,
			// 10 minutes max to download the file
			CURLOPT_TIMEOUT => 600,
			// tmp
			CURLOPT_RETURNTRANSFER => true,
		]);
		curl_exec($curl);
		if (curl_errno($curl) !== 0) {
			echo "Erreur en déposant le fichier KBART dans le webdav de Bacon\n";
			print_r(curl_getinfo($curl));
		}
		curl_close($curl);
		fclose($file);
	}

	/**
	 * Return a file handler for a temporary file of up to 1Mb.
	 *
	 * @return resource
	 */
	private static function createTempFile(string $content)
	{
		$fh = fopen('php://temp/maxmemory:1024000', 'w');
		fwrite($fh, $content);
		fseek($fh, 0);
		return $fh;
	}

	private static function getDoajKbartContent(): string
	{
		// criteria on Service
		$filter = new ServiceFilter();
		$filter->setScenario('filter');
		$filter->baconDoaj = true;

		$export = new ExportServices([], $filter);
		return $export->exportToKbartV2(false);
	}

	private static function getDoajFileName(): string
	{
		return sprintf("MIRABEL_GLOBAL_DOAJ_PARTIEL_%s.txt", date('Y-m-d'));
	}

	private static function getGlobalKbartContent(): string
	{
		// criteria on Service
		$filter = new ServiceFilter();
		$filter->setScenario('filter');
		$filter->baconGlobal = true;

		$export = new ExportServices([], $filter);
		return $export->exportToKbartV2(false);
	}

	private static function getGlobalFileName(): string
	{
		return sprintf("MIRABEL_GLOBAL_TEXTEINTEGRAL_%s.txt", date('Y-m-d'));
	}

	private static function getLibreKbartContent(): string
	{
		// criteria on Service
		$filter = new ServiceFilter();
		$filter->setScenario('filter');
		$filter->baconLibre = true;

		$export = new ExportServices([], $filter);
		return $export->exportToKbartV2(false);
	}

	private static function getLibreFileName(): string
	{
		return sprintf("MIRABEL_GLOBAL_LIBRESACCES_%s.txt", date('Y-m-d'));
	}
}
