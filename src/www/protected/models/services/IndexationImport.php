<?php

/**
 * Import indexation by loading (issn, categoriealias) lines.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class IndexationImport extends CComponent
{
	public const TYPE_ERROR = 0;

	public const TYPE_SIMPLE = 1;

	public const TYPE_KBART = 2;

	/**
	 * @var bool Remove old links to this Vocabulaire
	 */
	public $removeOld = true;

	/**
	 * @var Vocabulaire
	 */
	public $vocabulaire;

	/**
	 * @var string Input separator
	 */
	private $separator = "\t";

	/**
	 * @var int TYPE_SIMPLE | TYPE_KBART
	 */
	private $type;

	private $header = [];

	private $indexation = [];

	private $errors = [];

	private $lastWarning = "";

	private $log = [
		"Titre non trouvé" => [],
		"Alias non déclaré" => [],
		"Indexation ajoutée" => [],
		"lignes lues" => 0,
		"lignes ok" => 0,
		"indexations pré-existantes" => 0,
		"indexations effacées" => 0,
		"indexations enregistrées" => 0,
	];

	/**
	 * @var string CSV content
	 */
	private $parsedCsv;

	public function setSeparator($s)
	{
		if ($s === '\t') {
			$this->separator = "\t";
		} else {
			$this->separator = $s;
		}
	}

	/**
	 * Read the CSV file and return the number of valid indexations found.
	 *
	 * @param string $csv File name
	 * @param mixed $output Filehandle (STDOUT, etc)
	 * @return int # valid indexations
	 */
	public function parseCsvFile($csv, $output = null)
	{
		if (isset($output)) {
			$outputFile = $output;
		} else {
			$outputFile = fopen('php://memory', "w");
		}
		$count = 0;
		$expected = 0;
		$handle = fopen($csv, "r");
		if (!$handle) {
			die("Could not read the CSV file.");
		}
		$this->type = $this->detectFileType($handle);
		if ($this->type === self::TYPE_ERROR) {
			return 0;
		}
		$line = 0;
		if ($this->header) {
			$line++;
			if (!mb_check_encoding(join(' ', $this->header), 'UTF-8')) {
				$this->errors[] = "La ligne d'en-tête n'est pas en UTF-8. Exportez à nouveau ce CSV en vérifiant que le jeu de caractères est <b>UTF-8</b>.";
				return 0;
			}
		}
		while (($row = fgetcsv($handle, 0, $this->separator)) !== false) {
			$line++;
			if (!mb_check_encoding(join(' ', $row), 'UTF-8')) {
				$this->errors[] = "La ligne $line n'est pas en UTF-8. Exportez à nouveau ce CSV en vérifiant que le jeu de caractères est <b>UTF-8</b>.";
				continue;
			}
			if (!$this->validateRow($row, $line)) {
				return 0;
			}
			$couple = $this->extractData($row);
			if ($this->parse($couple[0], $couple[1])) {
				$expected++;
				if ($output) {
					fputcsv($outputFile, [join(';', $couple[0]), join(';', $couple[1]), "OK"], "\t");
				}
			} else {
				fputcsv($outputFile, [join(';', $couple[0]), join(';', $couple[1]), $this->getLastWarning()], "\t");
			}
			$count++;
		}
		$this->log["lignes lues"] = $count;
		$this->log["lignes ok"] = $expected;
		if (!isset($output)) {
			rewind($outputFile);
			$this->parsedCsv = fread($outputFile, 2048*1024);
			fclose($outputFile);
		}
		return $expected;
	}

	/**
	 * @return string
	 */
	public function getParsedCsv()
	{
		return $this->parsedCsv;
	}

	/**
	 * Cross the parameters with the known journals and the Vocabulaire. Does not write anything.
	 *
	 * @param array $idents list of ISSN or titreId
	 * @param array $mots
	 * @return bool Success?
	 */
	public function parse($idents, $mots)
	{
		$titreId = null;
		foreach ($idents as $ident) {
			if (ctype_digit($ident)) {
				$titreId = (int) $ident;
			} elseif (preg_match('/^\d{4}-\d{3}[X\d]$/', $ident)) {
				$titre = Titre::findByIssn($ident);
				if ($titre) {
					$titreId = (int) $titre->id;
				}
			} else {
				$this->addToLog($ident, "Format d'identifiant non reconnu (ni nombre, ni ISSN)");
				return false;
			}
			if ($titreId) {
				break;
			}
		}
		if (!$titreId) {
			$this->addToLog(join('|', $idents), "Titre non trouvé");
			return false;
		}
		if (!isset($this->indexation[$titreId])) {
			$this->indexation[$titreId] = [];
		}
		$titreAR = Titre::model()->findByPk($titreId);
		if ($titreAR) {
			foreach ($mots as $mot) {
				$info = $this->getCategorieInfo($mot);
				if (!$info) {
					$this->addToLog($mot, "Alias non déclaré");
					continue;
				}
				$this->indexation[$titreId][] = $info;
				$this->addToLog("$titreId | {$titreAR->titre} | {$titreAR->revueId} | {$info->categorie} ({$info->categorieId}) | [" . join('|', $idents) . ";$mot]");
			}
		} else {
			$this->addToLog("$titreId |  |  | | [" . join('|', $idents) . ";" . join(" + ", $mots) . "]");
		}
		return true;
	}

	/**
	 * Insert the indexation into the DB (table CategorieAlias_Titre).
	 *
	 * @param int $utilisateurId
	 * @return int #relations
	 */
	public function save($utilisateurId = null)
	{
		$values = [];
		foreach ($this->indexation as $titreId => $infos) {
			foreach ($infos as $info) {
				// categorieAliasId, titreId
				$values[] = sprintf("(%d, %d, %s)", $info->aliasId, $titreId, $utilisateurId ? (int) $utilisateurId : "NULL");
			}
		}
		$db = Yii::app()->db;
		$this->log["indexations pré-existantes"] = $db
			->createCommand(
				"SELECT count(*) FROM CategorieAlias_Titre"
				. " WHERE categorieAliasId IN (SELECT id FROM CategorieAlias WHERE vocabulaireId = ?)"
			)->queryScalar([$this->vocabulaire->id]);
		if (!$values) {
			$this->errors[] = "Aucune indexation à ajouter. L'opération est annulée.";
			return 0;
		}
		if ($this->removeOld) {
			$this->log["indexations effacées"] = $db
				->createCommand(
					"DELETE FROM CategorieAlias_Titre"
					. " WHERE categorieAliasId IN (SELECT id FROM CategorieAlias WHERE vocabulaireId = ?)"
				)->execute([$this->vocabulaire->id]);
		}
		$saved = $db
			->createCommand(
				"INSERT IGNORE INTO CategorieAlias_Titre (categorieAliasId, titreId, modifPar) VALUES " . join(", ", $values)
			)->execute();
		$this->log["indexations enregistrées"] = $saved;
		return $saved;
	}

	/**
	 * @return array
	 */
	public function getLog()
	{
		return $this->log;
	}

	/**
	 * @return array
	 */
	public function getLastWarning()
	{
		return $this->lastWarning;
	}

	/**
	 * @return array
	 */
	public function getErrors()
	{
		return $this->errors;
	}

	/**
	 * @return array
	 */
	public function getUnknownWords()
	{
		setlocale(LC_COLLATE, "fr_FR.utf8");
		$words = array_unique($this->log["Alias non déclaré"]);
		sort($words, SORT_LOCALE_STRING);
		return $words;
	}

	/**
	 * @param resource $handle
	 * @return int
	 */
	protected function detectFileType($handle)
	{
		$header = array_map('trim', fgetcsv($handle, 0, $this->separator));
		if (in_array("print_identifier", $header)) {
			$this->header = array_flip($header);
			if (!isset($this->header['discipline'])) {
				$this->errors[] = "Ce fichier KBART n'a pas de colonne 'discipline'.";
				return self::TYPE_ERROR;
			}
			return self::TYPE_KBART;
		}
		if (ctype_digit($header[0]) || preg_match('/^\d{4}-\d{3}[X\d]$/', $header[0])) {
			rewind($handle);
		}
		$this->header = [];
		return self::TYPE_SIMPLE;
	}

	/**
	 * @param array $row
	 * @param int $linenum
	 * @return bool
	 */
	protected function validateRow($row, $linenum)
	{
		if ($this->type === self::TYPE_SIMPLE) {
			if (count($row) !== 2) {
				$this->errors[] = sprintf("Deux colonnes attendues en ligne %d (démarrant à 0), %d trouvées. Est-ce le bon séparateur ?", $linenum, count($row));
				return false;
			}
		}
		return true;
	}

	/**
	 *
	 * @param array $row
	 * @throws Exception
	 * @return array
	 */
	protected function extractData($row)
	{
		if ($this->type === self::TYPE_SIMPLE) {
			return [
				[$row[0]],
				[$row[1]],
			];
		}
		if ($this->type === self::TYPE_KBART) {
			return [
				[
					$row[$this->header['online_identifier']],
					$row[$this->header['print_identifier']],
				],
				array_map('trim', explode(';', $row[$this->header['discipline']])),
			];
		}
		throw new Exception("invalid type");
	}

	private function addToLog($message, $error = "")
	{
		if ($error) {
			$this->lastWarning = $error;
			$this->log[$error][] = $message;
		} else {
			$this->lastWarning = "";
			$this->log["Indexation ajoutée"][] = $message;
		}
	}

	/**
	 * Return null or { aliasId: ?, categorieId: ?, categorie: ? }
	 *
	 * @staticvar array $known
	 * @param string $mot
	 * @return ?stdClass
	 */
	private function getCategorieInfo($mot)
	{
		static $known;
		if (!isset($known)) {
			$known = [];
			$sql = "SELECT ca.alias AS alias, ca.id AS aliasId, ca.categorieId, c.categorie"
				. " FROM CategorieAlias ca JOIN Categorie c ON c.id = ca.categorieId"
				. " WHERE vocabulaireId = " . (int) $this->vocabulaire->id;
			$rows = Yii::app()->db->getPdoInstance()->query($sql, PDO::FETCH_OBJ);
			foreach ($rows as $row) {
				$known[$row->alias] = $row;
			}
		}
		return ($known[$mot] ?? null);
	}
}
