<?php

/**
 * SearchNavigation
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class SearchNavigation extends CComponent
{
	/**
	 * @var array
	 */
	private $search;

	private $hash = '';

	private $rank;

	/**
	 * @param string $action controller/action
	 * @param array $criteria
	 * @param string $modelName
	 * @param array $results
	 * @return string
	 */
	public function save($action, array $criteria, $modelName, array $results, $pagination = null)
	{
		foreach ($results as $rank => $r) {
			$v = null;
			if (is_scalar($r)) {
				$v = $r;
			} elseif (isset($r['id'])) {
				$v = $r['id'];
			} elseif (isset($r->id)) {
				$v = $r->id;
			} elseif (isset($r['attrs']['revueid'])) {
				$v = $r['attrs']['revueid'];
			} elseif (isset($r['attrs']['id'])) {
				$v = $r['attrs']['id'];
			}
			if ($v) {
				$results[$rank]['id'] = $v;
			}
		}
		$previousPage = '';
		$nextPage = '';
		if ($pagination) {
			if ($pagination->currentPage > 0) {
				$previousPage = Yii::app()->createUrl(
					$action,
					array_merge($criteria, [$pagination->pageVar => $pagination->currentPage])
				);
			}
			if ($pagination->currentPage < $pagination->pageCount - 1) {
				$nextPage = Yii::app()->createUrl(
					$action,
					array_merge($criteria, [$pagination->pageVar => $pagination->currentPage + 2])
				);
			}
		}
		$this->search = [
			'action' => $action,
			'criteria' => array_filter(
				array_map(
					function ($x) {
						if (is_array($x)) {
							return array_filter($x);
						}
						return $x;
					},
					$criteria
				)
			),
			'modelname' => $modelName,
			'results' => $results,
			'previousPage' => $previousPage,
			'nextPage' => $nextPage,
		];
		$hash = $this->buildHash($this->search);
		Yii::app()->session->add($hash, $this->search);
		$lasts = Yii::app()->session->get('last_searches', []);
		if (!in_array($hash, $lasts)) {
			$lasts[] = $hash;
			Yii::app()->session->add('last_searches', $lasts);
		}
		$this->hash = $hash;
		return $hash;
	}

	/**
	 * @param string $hash
	 * @return \SearchNavigation
	 */
	public function loadByHash($hash)
	{
		$parts = explode('-', $hash);
		$this->hash = $parts[0];
		if (isset($parts[1])) {
			$this->rank = (int) $parts[1];
		} else {
			$this->rank = null;
		}
		$this->search = Yii::app()->session->get($this->hash);
		return $this;
	}

	/**
	 * @param CActiveRecord $record
	 * @return \SearchNavigation
	 */
	public function loadByRecord(\CActiveRecord $record)
	{
		$lasts = Yii::app()->session->get('last_searches');
		if ($lasts) {
			$class = get_class($record);
			foreach ($lasts as $last) {
				$this->search = Yii::app()->session[$last];
				if (!$this->search || $this->search['model'] !== $class || !in_array($record->getPrimaryKey(), $this->search['results'])) {
					$this->search = [];
				}
			}
		}
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isEmpty()
	{
		return empty($this->search);
	}

	/**
	 * @param int $current ID
	 * @return mixed Search result, as saved.
	 */
	public function getNextResult($current = null)
	{
		if (isset($this->rank)) {
			if (isset($this->search['results'][$this->rank + 1])) {
				return $this->search['results'][$this->rank + 1];
			}
			return null;
		}
		return $this->getNeighbours($current)[1];
	}

	/**
	 * @param int $current ID
	 * @return mixed Search result, as saved.
	 */
	public function getPreviousResult($current = null)
	{
		if (isset($this->rank)) {
			if ($this->rank > 0 && isset($this->search['results'][$this->rank - 1])) {
				return $this->search['results'][$this->rank - 1];
			}
			return null;
		}
		return $this->getNeighbours($current)[0];
	}

	/**
	 * @param int $id
	 * @return array [prev, next]
	 */
	public function getNeighbours($id = null)
	{
		if (isset($this->rank)) {
			return [$this->getPreviousResult(), $this->getNextResult()];
		}
		$found = false;
		$prev = null;
		$next = null;
		$results = $this->search['results'];
		foreach ($results as $r) {
			if ($found) {
				$next = $r;
				break;
			}
			if (isset($r['id']) && $r['id'] == $id) {
				$found = true;
				continue;
			}
			$prev = $r;
		}
		if (!$found) {
			return [null, null];
		}
		return [$prev, $next];
	}

	public function getPreviousPage()
	{
		if ($this->getPreviousResult() === null) {
			return $this->search['previousPage'];
		}
		return null;
	}

	public function getNextPage()
	{
		if ($this->getNextResult() === null) {
			return $this->search['nextPage'];
		}
		return null;
	}

	/**
	 * @param int $pos (opt)
	 * @return string
	 */
	public function getHash($pos = null)
	{
		if (isset($pos)) {
			return $this->hash . "-" . $pos;
		}
		return $this->hash;
	}

	/**
	 * @return string
	 */
	public function getNextHash()
	{
		if (isset($this->rank)) {
			return $this->hash . "-" . ($this->rank + 1);
		}
		return $this->hash;
	}

	/**
	 * @return string
	 */
	public function getPreviousHash()
	{
		if ($this->rank > 0) {
			return $this->hash . "-" . ($this->rank - 1);
		}
		return $this->hash;
	}

	/**
	 * @param string $anchor e.g. "id-something" (with or without "#")
	 * @return array
	 */
	public function getSearchUrl($anchor = "")
	{
		$params = $this->search['criteria'];
		array_unshift($params, $this->search['action']);
		if ($anchor) {
			$params['#'] = ltrim($anchor, "#");
		}
		return $params;
	}

	/**
	 * @param array $search
	 * @return string
	 */
	private function buildHash(array $search)
	{
		return base_convert(
			hash('crc32b', Yii::app()->name . ' search ' . $search['modelname'] . serialize($search['criteria'])),
			16,
			36
		);
	}
}
