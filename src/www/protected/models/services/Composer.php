<?php

namespace models\services;

/**
 * Extract info from PHP Composer about required and installed libraries
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class Composer
{
	private $main;

	private $lock;

	public function __construct(string $path)
	{
		$mainFile = rtrim($path, "/") . "/composer.json";
		if (!file_exists($mainFile) || !is_readable($mainFile)) {
			throw new \Exception("Could not read $mainFile");
		}
		$this->main = json_decode(file_get_contents($mainFile));

		$lockFile = rtrim($path, "/") . "/composer.lock";
		if (!file_exists($lockFile) || !is_readable($lockFile)) {
			throw new \Exception("Could not read $lockFile");
		}
		$this->lock = json_decode(file_get_contents($lockFile));
	}

	public function getLibraries(string $category = null)
	{
		if (!$category) {
			$require = array_keys((array) $this->main->require);
			$requireDev = array_keys((array) $this->main->{"require-dev"});
			$libs = array_merge($require, $requireDev);
		} elseif ($category === 'require' || $category === 'require-dev') {
			$libs = array_keys((array) $this->main->{$category});
		} else {
			throw new \Exception("Coding error: composer category must be 'require', 'require-dev', or null.");
		}
		return $this->fetchInfo($libs);
	}

	private function fetchInfo($libs)
	{
		$info = [];
		foreach ($this->lock->packages as $p) {
			if (in_array($p->name, $libs)) {
				$info[$p->name] = $p;
			}
		}
		foreach ($this->lock->{"packages-dev"} as $p) {
			if (in_array($p->name, $libs)) {
				$info[$p->name] = $p;
			}
		}
		return $info;
	}
}
