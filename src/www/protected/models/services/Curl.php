<?php

/**
 * @todo Split into CurlRequest and CurlResponse?
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class Curl
{
	private $content;

	private $curl;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->curl = curl_init();
		curl_setopt_array(
			$this->curl,
			[
				\CURLOPT_FOLLOWLOCATION => true,    // follow redirection (301, etc)
				\CURLOPT_MAXREDIRS => 4,
				\CURLOPT_SSL_VERIFYHOST => false,
				\CURLOPT_SSL_VERIFYPEER => false,
				\CURLOPT_COOKIEFILE => "",          // store cookies
				\CURLOPT_USERAGENT => 'Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0',
				\CURLOPT_HEADER => false,
				\CURLOPT_TIMEOUT => 5, // seconds
			]
		);
	}

	/**
	 * Destructor
	 */
	public function __destruct()
	{
		curl_close($this->curl);
	}

	/**
	 * Set an CURL option
	 *
	 * @param string $option CURLOPT_foobar
	 * @param mixed $value
	 */
	public function setopt(string $option, $value): self
	{
		if (curl_setopt($this->curl, $option, $value) === false) {
			throw new Exception("curl_setopt() failed. Probably a code error.");
		}
		return $this;
	}

	/**
	 * Fetch an URL. Then use getContent() to read the response's body.
	 *
	 * @param string $url
	 * @throws Exception
	 * @return $this
	 */
	public function get(string $url): self
	{
		$ok = curl_setopt_array(
			$this->curl,
			[
				\CURLOPT_FILE => null,
				\CURLOPT_HTTPGET => true,
				\CURLOPT_RETURNTRANSFER => true,
				\CURLOPT_URL => $url,
			]
		);
		if (!$ok) {
			throw new Exception("Erreur en configurant curl.");
		}
		$this->content = curl_exec($this->curl);
		if (curl_errno($this->curl) !== 0 || $this->content === false) {
			throw new Exception("Erreur de téléchargement : " . curl_error($this->curl));
		}
		return $this;
	}

	/**
	 * Fetch an URL into a file handler.
	 *
	 * @param string $url
	 * @param resource $fh
	 * @throws Exception
	 * @return $this
	 */
	public function getFile(string $url, $fh): self
	{
		if (!is_resource($fh)) {
			throw new Exception("Le second paramètre de Curl::getFile() doit être un file handler.");
		}
		$ok = curl_setopt_array(
			$this->curl,
			[
				\CURLOPT_RETURNTRANSFER => true, // ORDER MATTERS! (required for FILE, though undocumented)
				\CURLOPT_FILE => $fh,
				\CURLOPT_HTTPGET => true,
				\CURLOPT_TIMEOUT => 60,
				\CURLOPT_URL => $url,
			]
		);
		if (!$ok) {
			throw new Exception("Erreur en configurant curl.");
		}
		curl_exec($this->curl);
		if (curl_errno($this->curl) !== 0 || $this->content === false) {
			throw new Exception("Erreur de téléchargement : " . curl_error($this->curl));
		}
		return $this;
	}

	/**
	 * Return the timestamp of the remote file, or null if unknown.
	 *
	 * @param string $url
	 * @return int|null
	 */
	public function getFileTime(string $url): ?int
	{
		$ok = curl_setopt_array(
			$this->curl,
			[
				\CURLOPT_RETURNTRANSFER => true, // ORDER MATTERS! (if missing, header will be printed on stdout)
				\CURLOPT_NOBODY => true,
				\CURLOPT_FILETIME => true,
				\CURLOPT_TIMEOUT => 5, // seconds
				\CURLOPT_URL => $url,
			]
		);
		if (!$ok) {
			throw new Exception("Erreur en configurant curl.");
		}
		curl_exec($this->curl);
		curl_setopt_array(
			$this->curl,
			[
				\CURLOPT_NOBODY => false,
				\CURLOPT_FILETIME => false,
			]
		);
		$ts = curl_getinfo($this->curl, \CURLINFO_FILETIME);
		if ($ts < 0) {
			return null;
		}
		return $ts;
	}

	public function getContent()
	{
		return $this->content;
	}

	public function getContentType()
	{
		return curl_getinfo($this->curl, \CURLINFO_CONTENT_TYPE);
	}

	public function getHttpCode()
	{
		return curl_getinfo($this->curl, \CURLINFO_HTTP_CODE);
	}

	public function getSize()
	{
		return curl_getinfo($this->curl, \CURLINFO_SIZE_DOWNLOAD);
	}
}
