<?php

/**
 * Description of TitreCouverture
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class TitreCouverture extends ImageDownloader
{
	public function __construct()
	{
		$webroot = Yii::getPathOfAlias('webroot');
		if (!glob($webroot . "/images")) {
			$webroot = dirname($webroot);
			if (!glob($webroot . "/images")) {
				throw new Exception("Chemin vers les images introuvable");
			}
		}
		parent::__construct(
			$webroot . '/images/titres-couvertures'
		);
	}
}
