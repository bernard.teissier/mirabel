<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class ImageDownloader
{
	public $boundingbox = "128x128";

	/**
	 * @var string For sprintf use, without the file extension.
	 */
	public $filePattern = '%09d';

	protected $path;

	protected $pathRaw;

	protected $curl;

	/**
	 * @param string $path Where to put the final image (after resizing).
	 * @param string $pathRaw Where to put the raw image (before resizing).
	 */
	public function __construct(string $path, string $pathRaw = "")
	{
		$this->path = $path;
		if ($pathRaw) {
			$this->pathRaw = $pathRaw;
		} else {
			$this->pathRaw = $path . '/raw';
		}

		$this->checkAccess($this->path);
		$this->checkAccess($this->pathRaw);

		$this->curl = curl_init();
		curl_setopt_array(
			$this->curl,
			[
				CURLOPT_FOLLOWLOCATION => true,    // follow redirection (301, etc)
				CURLOPT_MAXREDIRS => 4,
				CURLOPT_TIMEOUT => 5,              // 5 seconds max
				CURLOPT_SSL_VERIFYHOST => false,
				CURLOPT_SSL_VERIFYPEER => false,
				CURLOPT_COOKIEFILE => "",          // store cookies
				CURLOPT_USERAGENT => 'Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0',
			]
		);
	}

	public function __destruct()
	{
		curl_close($this->curl);
	}

	/**
	 * Get the name of the file where to download the raw image, before resizing.
	 *
	 * @param int $identifier
	 * @return string full path
	 */
	public function getRawImageFileName($identifier): string
	{
		return $this->pathRaw . '/' . sprintf($this->filePattern, $identifier);
	}

	/**
	 * Download the image from a distant source.
	 *
	 * @param int|string $identifier
	 * @param string $url
	 * @return bool Image downloaded?
	 */
	public function download($identifier, string $url): bool
	{
		$filename = $this->getRawImageFileName($identifier);
		$oldFiles = glob($filename . ".*");
		$filenameTmp = $filename . ".tmp";
		$fh = @fopen($filenameTmp, "w");
		if (!$fh) {
			throw new Exception("Impossible de créer ou modifier le fichier en écriture !");
		}
		curl_setopt($this->curl, CURLOPT_FILE, $fh);
		curl_setopt($this->curl, CURLOPT_URL, $this->encodeUrl($url));
		if (curl_exec($this->curl) === false) {
			throw new Exception("Erreur de téléchargement : " . curl_error($this->curl));
		}
		$code = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
		if ($code < 200 || $code >= 300) {
			throw new Exception("Permission refusée ? Code HTTP " . $code);
		}
		fclose($fh);
		if (!@chmod($filenameTmp, 0664)) {
			Yii::log("Could not chmod on '$filenameTmp' in download().", CLogger::LEVEL_WARNING);
		}
		$size = filesize($filenameTmp);
		Yii::log("ImageDownloader: téléchargement de l'image $filenameTmp de $size octets", "info");
		if ($size < 400) {
			Yii::log("ImageDownloader: suppression de l'image vide $filenameTmp", "error");
			unlink($filenameTmp);
			throw new Exception("Image de $size octets, forcément vide.");
		}
		if ($oldFiles) {
			if ($size === filesize($oldFiles[0]) && md5_file($filenameTmp) === md5_file($oldFiles[0])) {
				Yii::log("ImageDownloader: image $filename identique à l'ancienne", "info");
				unlink($filenameTmp);
				return true;
			}
			foreach ($oldFiles as $oldFile) {
				Yii::log("ImageDownloader: suppression de l'ancienne image $oldFile", "info");
				unlink($oldFile);
			}
		}
		if (!rename($filenameTmp, $filename)) {
			throw new Exception("Erreur de permission de fichiers, impossible de renommer.");
		}
		$newname = $this->addImageExtension($filename, curl_getinfo($this->curl, CURLINFO_CONTENT_TYPE), $url);
		if (!$newname) {
			throw new Exception("L'enregistrement du fichier a échoué. Problème de permissions ?");
		}
		if (!@chmod($newname, 0664)) {
			Yii::log("Could not chmod on '$newname'.", CLogger::LEVEL_WARNING);
		}
		return true;
	}

	/**
	 * Resize all the downloaded bitmap image files.
	 */
	public function resizeAll()
	{
		$images = glob($this->path . "/raw/*.{jpg,gif,png,svg}", GLOB_BRACE);
		printf("Parcourt %d images pour traiter les changements...\n", count($images));
		$actions = [
			"vignette à jour" => 0,
			"vignette créée" => 0,
			"vignette mise à jour" => 0,
			"erreurs" => 0,
		];
		foreach ($images as $source) {
			try {
				$a = $this->resizeFile($source);
				$actions[$a]++;
			} catch (\Error $e) {
				Yii::log($e->getMessage(), 'error');
				$actions['erreurs']++;
			} catch (\Exception $e) {
				Yii::log($e->getMessage(), 'error');
				$actions['erreurs']++;
			}
		}
		return $actions;
	}

	/**
	 * Resize a bitmap file into PNG, or copies SVG vectorial files.
	 *
	 * @param string $sourcePath
	 * @param bool $force Force a resize, even if the source file is older.
	 * @return string Description of the operation
	 */
	public function resizeFile(string $sourcePath, bool $force = false): string
	{
		$name = basename($sourcePath);
		$oldFiles = glob(preg_replace('/\.\w+$/', '.*', "{$this->path}/$name"));

		if (preg_match('/\.svg$/', $name)) {
			if ($oldFiles) {
				foreach ($oldFiles as $f) {
					if (!preg_match('/\.svg$/', $f)) {
						unlink($f);
					}
				}
			}
			$out = "{$this->path}/$name";
			if (!$force && file_exists($out) && filemtime($out) > filemtime($sourcePath)) {
				return "vignette à jour";
			}
			$maj = file_exists($out);
			copy($sourcePath, $out);
			if (!@chmod($out, 0664)) {
				Yii::log("Could not chmod on '$out'.", CLogger::LEVEL_WARNING);
			}
			return ($maj ? "vignette mise à jour" : "vignette créée");
		}

		if ($oldFiles) {
			foreach ($oldFiles as $f) {
				if (!preg_match('/\.png$/', $f)) {
					unlink($f);
				}
			}
		}
		$out = $this->path . "/" . preg_replace('/\.\w+$/', '', $name) . '.png';
		if (!$force && file_exists($out) && filemtime($out) > filemtime($sourcePath)) {
			return "vignette à jour";
		}
		$maj = file_exists($out);
		$size = filesize($sourcePath);
		if ($size < 400) {
			copy($sourcePath, $out);
		} else {
			// ImageMagick
			system("convert $sourcePath -resize {$this->boundingbox} $out");
			if (!file_exists($out)) {
				throw new Exception("Erreur de traitement d'image. Imagemagick est-il installé ?");
			}
			$multiple = glob(preg_replace('/\.png$/', '-*.png', $out));
			if ($multiple) {
				rename(array_shift($multiple), $out);
				array_map('unlink', $multiple);
			}
		}
		if (!@chmod($out, 0664)) {
			Yii::log("Could not chmod on '$out'.", CLogger::LEVEL_WARNING);
		}
		return ($maj ? "vignette mise à jour" : "vignette créée");
	}

	/**
	 * Resize gif|jpg|png, copies svg.
	 *
	 * @param int|string $identifier Identifier of the image (for filePattern).
	 * @param bool $force Force a resize, even if the source file is older.
	 * @return bool Success?
	 */
	public function resizeByIdentifier($identifier, $force = false)
	{
		$globPattern = $this->pathRaw . '/' . sprintf($this->filePattern, $identifier) . '.{jpg,gif,png,svg}';
		$filename = glob($globPattern, GLOB_BRACE);
		if ($filename) {
			$this->resizeFile($filename[0], $force);
			return true;
		}
		return false;
	}

	/**
	 * @param int|string $identifier
	 * @return bool Success?
	 */
	public function delete($identifier)
	{
		$globPattern = $this->pathRaw . sprintf('/' . $this->filePattern, $identifier) . '.{jpg,gif,png,svg}';
		$filenames = glob($globPattern, GLOB_BRACE);
		if ($filenames) {
			foreach ($filenames as $f) {
				Yii::log("ImageDownloader: suppression de l'ancienne image $f", "info");
				unlink($f);
			}
		}
		$final = glob($this->path . '/' . sprintf($this->filePattern, $identifier) . '.*');
		if ($final) {
			Yii::log("ImageDownloader: suppression de l'ancienne image {$final[0]}", "info");
			unlink($final[0]);
		}
		return true;
	}

	protected function checkAccess($dir)
	{
		if (!is_dir($dir)) {
			if (!mkdir($dir, 0777, true)) {
				throw new Exception("Impossible de créer le répertoire où les images seront écrites.");
			}
		}
		if (!is_writable($dir) || !is_executable($dir)) {
			$user = posix_getpwuid(posix_geteuid())['name'];
			throw new Exception("Erreur de droit d'accès pour '$user' sur le répertoire où les images seront écrites: $dir");
		}
	}

	/**
	 * Rename a file with its image extension.
	 *
	 * @param string $filename
	 * @param string|bool $mimeType cURL will set it to false if no MIME header in the HTTP response
	 * @param string $url (only used if non mime-type is given)
	 * @return string New file name
	 */
	protected function addImageExtension(string $filename, $mimeType, string $url = ''): string
	{
		$ext = "";
		if ($mimeType === false) {
			// no mime-type, guess from extension
			$m = [];
			if (preg_match('/\.(png|jpg|gif|svg)(\?|$)/i', $url, $m)) {
				$ext = strtolower($m[1]);
			}
		} else {
			// some websites (Persée) append a charset to images!
			$mimeType = strtok($mimeType, ';');
			switch ($mimeType) {
				case 'image/jpeg':
				case 'image/jpg':
					$ext = "jpg";
					break;
				case 'image/gif':
					$ext = "gif";
					break;
				case 'image/png':
					$ext = "png";
					break;
				case 'image/svg+xml':
					$ext = "svg";
					break;
				case 'text/html':
					rename($filename, $filename . ".html");
					throw new Exception("\tErreur: reçu du HTML et non une image");
				default:
					throw new Exception("Mime-type inconnu: $mimeType");
			}
		}
		$dest = $filename . "." . $ext;
		rename($filename, $dest);
		return $dest;
	}

	/**
	 * Encode an URL that may contain non-ASCII characters (e.g. http://www.openedition.org/iss_600×281.png).
	 *
	 * @param string $url
	 * @return string
	 */
	protected function encodeUrl(string $url): string
	{
		if (strpos($url, '%') !== false) {
			return $url; // no double-encoding
		}
		$parts = parse_url($url);
		if (!empty($parts['path'])) {
			$parts['path'] = join('/', array_map('rawurlencode', explode('/', $parts['path'])));
		}
		$query = [
			isset($parts['scheme']) ? $parts['scheme'] . '://' : '',
			$parts['host'] ?? '',
			isset($parts['port']) ? ':' . $parts['port'] : '',
			$parts['path'] ?? '',
			isset($parts['query']) ? '?' . $parts['query'] : '',
		];
		return implode('', array_filter($query));
	}
}
