<?php

namespace models\validators;

/**
 * Validates an ISSN.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class IssnValidator extends \CValidator
{
	public const TYPE_ISSN = "ISSN"; // ISSN-P or ISSN-E

	public const TYPE_ISSNL = "ISSN-L";

	/**
	 * @var bool whether the attribute value can be null or empty. Defaults to true,
	 * meaning that if the attribute is empty, it is considered valid.
	 */
	public $allowEmpty = true;

	/**
	 * @var array These values are invalid ISSN but are accepted nevertheless.
	 * Defaults to array('en cours').
	 */
	public $specialValues = ['en cours'];

	/**
	 * @var bool If true, then a 7 characters ISSN will be allowed.
	 */
	public $allowEmptyChecksum = false;

	/**
	 * @var bool If true, then a ISSN without caret will be allowed.
	 */
	public $allowMissingCaret = false;

	/**
	 * @var bool Will correct some errors (missing caret, etc)
	 */
	public $autoFix = false;

	/**
	 * @var string TYPE_ISSN or TYPE_ISSNE or TYPE_ISSNL
	 */
	public $type = self::TYPE_ISSN;

	public $lastIssn;

	private $localErrors = [];

	private $localSuffix = '';

	public function validateString($issn)
	{
		$this->localErrors = [];
		$this->localSuffix = '';
		if ($this->allowEmpty && empty($issn)) {
			return true;
		}
		if (in_array($issn, $this->specialValues)) {
			return true;
		}
		$printableIssn = htmlspecialchars($issn);
		if (strpos($issn, '–') !== false) {
			$this->localErrors[] = "L'ISSN utilise un tiret long au lieu de '-' : '$printableIssn'";
			return false;
		}
		if (strlen($issn) < 7) {
			$this->localErrors[] = "Un ISSN doit avoir 8 caractères, tirets non compris : '$printableIssn'" ;
			return false;
		}
		if (!$this->allowMissingCaret && $issn[4] !== '-') {
			$this->localErrors[] = "Un ISSN doit avoir un tiret en cinquième position : '$printableIssn'";
			return false;
		}
		$issn = str_replace('-', '', $issn);
		if ($this->allowEmptyChecksum && strlen($issn) === 7) {
			$key = self::computeKey($issn);
			$this->localSuffix = $key;
			$this->lastIssn = substr($issn, 0, 4) . '-' . substr($issn, 4) . $key;
			return true;
		}
		if (strlen($issn) === 8) {
			try {
				$sum = self::computeKey($issn);
			} catch (\Exception $e) {
				$this->localErrors[] = $e->getMessage();
				return false;
			}
			if ($sum == $issn[7]) {
				$this->lastIssn = substr($issn, 0, 4) . '-' . substr($issn, 4);
				return true;
			}
		} else {
			$this->localErrors[] = "Un ISSN doit avoir 8 caractères, tirets non compris : '$printableIssn'" ;
			return false;
		}
		$this->localErrors[] = "Ce numéro ISSN n'est pas valide : '$printableIssn'";
		return false;
	}

	public static function computeKey($issn): string
	{
		$sum = 0;
		for ($i=0; $i < 7; $i++) {
			if (!ctype_digit($issn[$i])) {
				throw new \Exception("Cet ISSN contient un caractère non valide: '{$issn[$i]}'.");
			}
			$sum += (8 - $i) * (int) $issn[$i];
		}
		$sumMod = $sum % 11;
		if ($sumMod === 0) {
			$final = '0';
		} elseif ($sumMod === 1) {
			$final = 'X';
		} else {
			$final = (string) (11 - $sumMod);
		}
		return $final;
	}

	public function getErrors()
	{
		return $this->localErrors;
	}

	/**
	 * Validates the attribute of the object.
	 *
	 * If there is any error, the error message is added to the object.
	 *
	 * @param \CModel $object the object being validated.
	 * @param string $attr the attribute being validated.
	 */
	protected function validateAttribute($object, $attr)
	{
		if ($this->allowEmpty && $this->isEmpty($object->{$attr})) {
			return;
		}
		if ($this->autoFix) {
			$issn = $object->{$attr};
			if (trim($issn) !== $issn) {
				$issn = trim($issn);
			}
			if (strpos($issn, '–') !== false) {
				$issn = str_replace('–', '-', $issn);
			}
			if (!$this->allowMissingCaret && $issn[4] !== '-' && strlen($issn) === 8) {
				$issn = substr($issn, 0, 4) . "-" . substr($issn, 4);
			}
			if ($object->{$attr} !== $issn) {
				$object->{$attr} = $issn;
			}
		}
		if ($this->validateString($object->{$attr})) {
			if ($this->localSuffix) {
				$object->{$attr} .= $this->localSuffix;
			}
		}
		if (!empty($this->localErrors)) {
			foreach ($this->localErrors as $error) {
				$object->addError($attr, $error);
			}
		}
	}
}
