<?php

namespace models\validators;

use models\lang\Codes;

/**
 * Validates an ISO-639 language code.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class LangIso639Validator extends \CValidator
{
	/**
	 * @var bool whether the attribute value can be null or empty. Defaults to true,
	 * meaning that if the attribute is empty, it is considered valid.
	 */
	public $allowEmpty = true;

	/**
	 * @var bool If true, 'mul' will be ignored (removed from the attribute value).
	 */
	public $removeMul = true;

	/**
	 * @var bool whether the attribute can contain multiple languages separated by commas.
	 */
	public $multiple = true;

	/**
	 * @var bool Full English names will be converted to ISO codes.
	 */
	public $allowFullNames = true;

	/**
	 * @var string Regexp separator
	 */
	public $separator = '/,\s*/';

	// perl -M5.10.0 -ne 'chomp; @a = split /\|/; say "\"$a[3]\" => \"$a[0]\"," if $a[2];' ISO-639-2_utf-8.txt
	protected static $fullNamesEn = [
		"Abkhazian" => "abk",
		"Afrikaans" => "afr",
		"Akan" => "aka",
		"Albanian" => "alb",
		"Amharic" => "amh",
		"Arabic" => "ara",
		"Aragonese" => "arg",
		"Armenian" => "arm",
		"Assamese" => "asm",
		"Avaric" => "ava",
		"Avestan" => "ave",
		"Aymara" => "aym",
		"Azerbaijani" => "aze",
		"Bashkir" => "bak",
		"Bambara" => "bam",
		"Basque" => "baq",
		"Belarusian" => "bel",
		"Bengali" => "ben",
		"Bihari languages" => "bih",
		"Bislama" => "bis",
		"Bosnian" => "bos",
		"Breton" => "bre",
		"Bulgarian" => "bul",
		"Burmese" => "bur",
		"Catalan" => "cat",
		"Valencian" => "cat",
		"Chamorro" => "cha",
		"Chechen" => "che",
		"Chinese" => "chi",
		"Chuvash" => "chv",
		"Cornish" => "cor",
		"Corsican" => "cos",
		"Cree" => "cre",
		"Czech" => "cze",
		"Danish" => "dan",
		"Dutch" => "dut",
		"Flemish" => "dut",
		"Dzongkha" => "dzo",
		"English" => "eng",
		"Esperanto" => "epo",
		"Estonian" => "est",
		"Ewe" => "ewe",
		"Faroese" => "fao",
		"Fijian" => "fij",
		"Finnish" => "fin",
		"French" => "fre",
		"Fulah" => "ful",
		"Georgian" => "geo",
		"German" => "ger",
		"Gaelic" => "gla",
		"Irish" => "gle",
		"Galician" => "glg",
		"Manx" => "glv",
		"Greek" => "gre",
		"Guarani" => "grn",
		"Gujarati" => "guj",
		"Haitian" => "hat",
		"Hausa" => "hau",
		"Hebrew" => "heb",
		"Herero" => "her",
		"Hindi" => "hin",
		"Hiri Motu" => "hmo",
		"Croatian" => "hrv",
		"Hungarian" => "hun",
		"Igbo" => "ibo",
		"Icelandic" => "ice",
		"Ido" => "ido",
		"Inuktitut" => "iku",
		"Indonesian" => "ind",
		"Inupiaq" => "ipk",
		"Italian" => "ita",
		"Javanese" => "jav",
		"Japanese" => "jpn",
		"Kannada" => "kan",
		"Kashmiri" => "kas",
		"Kanuri" => "kau",
		"Kazakh" => "kaz",
		"Central Khmer" => "khm",
		"Kinyarwanda" => "kin",
		"Komi" => "kom",
		"Kongo" => "kon",
		"Korean" => "kor",
		"Kurdish" => "kur",
		"Lao" => "lao",
		"Latin" => "lat",
		"Latvian" => "lav",
		"Lingala" => "lin",
		"Lithuanian" => "lit",
		"Luba-Katanga" => "lub",
		"Ganda" => "lug",
		"Macedonian" => "mac",
		"Marshallese" => "mah",
		"Malayalam" => "mal",
		"Maori" => "mao",
		"Marathi" => "mar",
		"Malay" => "may",
		"Malagasy" => "mlg",
		"Maltese" => "mlt",
		"Mongolian" => "mon",
		"Nauru" => "nau",
		"Ndonga" => "ndo",
		"Nepali" => "nep",
		"Norwegian" => "nor",
		"Occitan" => "oci",
		"Provençal" => "oci",
		"Ojibwa" => "oji",
		"Oriya" => "ori",
		"Oromo" => "orm",
		"Persian" => "per",
		"Pali" => "pli",
		"Polish" => "pol",
		"Portuguese" => "por",
		"Quechua" => "que",
		"Romansh" => "roh",
		"Romanian" => "rum",
		"Moldavian" => "rum",
		"Rundi" => "run",
		"Russian" => "rus",
		"Sango" => "sag",
		"Sanskrit" => "san",
		"Slovak" => "slo",
		"Slovenian" => "slv",
		"Northern Sami" => "sme",
		"Samoan" => "smo",
		"Shona" => "sna",
		"Sindhi" => "snd",
		"Somali" => "som",
		"Sotho, Southern" => "sot",
		"Spanish" => "spa",
		"Castilian" => "spa",
		"Sardinian" => "srd",
		"Serbian" => "srp",
		"Swati" => "ssw",
		"Sundanese" => "sun",
		"Swahili" => "swa",
		"Swedish" => "swe",
		"Tahitian" => "tah",
		"Tamil" => "tam",
		"Tatar" => "tat",
		"Telugu" => "tel",
		"Tajik" => "tgk",
		"Tagalog" => "tgl",
		"Thai" => "tha",
		"Tibetan" => "tib",
		"Tigrinya" => "tir",
		"Tswana" => "tsn",
		"Tsonga" => "tso",
		"Turkmen" => "tuk",
		"Turkish" => "tur",
		"Twi" => "twi",
		"Ukrainian" => "ukr",
		"Urdu" => "urd",
		"Uzbek" => "uzb",
		"Venda" => "ven",
		"Vietnamese" => "vie",
		"Volapük" => "vol",
		"Welsh" => "wel",
		"Walloon" => "wln",
		"Wolof" => "wol",
		"Xhosa" => "xho",
		"Yiddish" => "yid",
		"Yoruba" => "yor",
		"Zhuang; Chuang" => "zha",
		"Zulu" => "zul",
	];

	// perl -M5.10.0 -ne 'chomp; @a = split /\|/; say "\"$a[4]\" => \"$a[0]\"," if $a[2];' ISO-639-2_utf-8.txt
	protected static $fullNamesFr = [
		"abkhaze" => "abk",
		"afrikaans" => "afr",
		"akan" => "aka",
		"albanais" => "alb",
		"amharique" => "amh",
		"arabe" => "ara",
		"aragonais" => "arg",
		"arménien" => "arm",
		"azéri" => "aze",
		"bambara" => "bam",
		"basque" => "baq",
		"biélorusse" => "bel",
		"bengali" => "ben",
		"bosniaque" => "bos",
		"breton" => "bre",
		"bulgare" => "bul",
		"birman" => "bur",
		"catalan" => "cat",
		"tchétchène" => "che",
		"chinois" => "chi",
		"corse" => "cos",
		"cree" => "cre",
		"tchèque" => "cze",
		"danois" => "dan",
		"néerlandais" => "dut",
		"flamand" => "dut",
		"anglais" => "eng",
		"espéranto" => "epo",
		"estonien" => "est",
		"finnois" => "fin",
		"français" => "fre",
		"peul" => "ful",
		"géorgien" => "geo",
		"allemand" => "ger",
		"gaélique" => "gla",
		"irlandais" => "gle",
		"galicien" => "glg",
		"grec" => "gre",
		"guarani" => "grn",
		"goudjrati" => "guj",
		"haïtien" => "hat",
		"hébreu" => "heb",
		"hindi" => "hin",
		"croate" => "hrv",
		"hongrois" => "hun",
		"islandais" => "ice",
		"inuktitut" => "iku",
		"interlingue" => "ile",
		"interlingua" => "ina",
		"indonésien" => "ind",
		"italien" => "ita",
		"javanais" => "jav",
		"japonais" => "jpn",
		"groenlandais" => "kal",
		"kazakh" => "kaz",
		"khmer" => "khm",
		"kikuyu" => "kik",
		"rwanda" => "kin",
		"kirghiz" => "kir",
		"kongo" => "kon",
		"coréen" => "kor",
		"kurde" => "kur",
		"lao" => "lao",
		"latin" => "lat",
		"letton" => "lav",
		"limbourgeois" => "lim",
		"lingala" => "lin",
		"lituanien" => "lit",
		"luxembourgeois" => "ltz",
		"macédonien" => "mac",
		"maori" => "mao",
		"malais" => "may",
		"malgache" => "mlg",
		"maltais" => "mlt",
		"mongol" => "mon",
		"népalais" => "nep",
		"norvégien" => "nor",
		"occitan" => "oci",
		"provençal" => "oci",
		"ossète" => "oss",
		"pendjabi" => "pan",
		"persan" => "per",
		"polonais" => "pol",
		"portugais" => "por",
		"quechua" => "que",
		"romanche" => "roh",
		"roumain" => "rum",
		"rundi" => "run",
		"sanskrit" => "san",
		"singhalais" => "sin",
		"slovaque" => "slo",
		"slovène" => "slv",
		"samoan" => "smo",
		"shona" => "sna",
		"sindhi" => "snd",
		"somali" => "som",
		"sotho du Sud" => "sot",
		"espagnol" => "spa",
		"castillan" => "spa",
		"sarde" => "srd",
		"serbe" => "srp",
		"soundanais" => "sun",
		"swahili" => "swa",
		"suédois" => "swe",
		"tahitien" => "tah",
		"tamoul" => "tam",
		"tatar" => "tat",
		"tadjik" => "tgk",
		"thaï" => "tha",
		"tibétain" => "tib",
		"tswana" => "tsn",
		"turkmène" => "tuk",
		"turc" => "tur",
		"twi" => "twi",
		"ouïgour" => "uig",
		"ukrainien" => "ukr",
		"ourdou" => "urd",
		"ouszbek" => "uzb",
		"vietnamien" => "vie",
		"volapük" => "vol",
		"gallois" => "wel",
		"wallon" => "wln",
		"wolof" => "wol",
		"xhosa" => "xho",
		"yiddish" => "yid",
		"yoruba" => "yor",
		"zoulou" => "zul",
	];

	public static function getAutocompleteListFr(): array
	{
		$list = [
			['label' => 'allemand', 'value' => 'ger'],
			['label' => 'anglais', 'value' => 'eng'],
			['label' => 'arabe', 'value' => 'ara' ],
			['label' => 'catalan', 'value' => 'cat'],
			['label' => 'chinois', 'value' => 'chi'],
			['label' => 'espagnol', 'value' => 'spa'],
			['label' => 'français', 'value' => 'fre'],
			['label' => 'grec', 'value' => 'ell'],
			['label' => 'hollandais', 'value' => 'dut'],
			['label' => 'italien', 'value' => 'ita'],
			['label' => 'japonais', 'value' => 'jpn'],
			['label' => 'portugais', 'value' => 'por'],
			['label' => 'russe', 'value' => 'rus'],
			['label' => 'slaves', 'value' => 'sla'],
			['label' => 'turque', 'value' => 'tur'],
			['label' => '—', 'value' => ''],
		];
		$dup = array_map(function ($x) {
			return $x['label'];
		}, $list);
		foreach (self::$fullNamesFr as $name => $code) {
			if (!in_array($name, $dup)) {
				$list[] = ['label' => $name, 'value' => $code];
			}
		}
		return $list;
	}

	/**
	 * Validates the attribute of the object.
	 *
	 * If there is any error, the error message is added to the object.
	 *
	 * @param \CModel $object the object being validated.
	 * @param string $attr the attribute being validated.
	 */
	protected function validateAttribute($object, $attr)
	{
		if ($this->allowEmpty && $this->isEmpty($object->{$attr})) {
			return;
		}
		if (!is_string($object->{$attr})) {
			$object->addError($attr, "Should be a string.");
			return;
		}
		$object->{$attr} = trim($object->{$attr}, ', ');
		$seen = [];
		if ($this->multiple) {
			$languages = preg_split($this->separator, $object->{$attr});
			foreach ($languages as &$lang) {
				if (!$this->validateLang($lang)) {
					$object->addError($attr, "Le langage '$lang' n'est pas valide.");
					return;
				}
				if ($lang) {
					if (isset($seen[$lang])) {
						$object->addError($attr, "Le langage '$lang' est attribué 2 fois.");
						return;
					}
					$seen[$lang] = 1;
				}
				unset($lang);
			}
			$object->{$attr} = join(', ', array_filter($languages));
		} else {
			if (!$this->validateLang($object->{$attr})) {
				$object->addError($attr, "Le langage '{$object->{$attr}}' n'est pas valide.");
				return;
			}
		}
	}

	protected function validateLang(&$lang): bool
	{
		$lang = trim($lang);
		if ($lang === 'und') {
			$lang = '';
			return true;
		}
		if ($lang === 'mul') {
			if ($this->removeMul) {
				$lang = '';
			}
			return true;
		}
		$upLang = ucfirst($lang);
		if ($this->allowFullNames && isset(self::$fullNamesEn[$upLang])) {
			$lang = self::$fullNamesEn[$upLang];
		} elseif ($this->allowFullNames && isset(self::$fullNamesFr[$lang])) {
			$lang = self::$fullNamesFr[$lang];
		} elseif (isset(Codes::ALIASES[$lang])) {
			$lang = Codes::ALIASES[$lang];
		}
		if (isset(Codes::CODES3[$lang])) {
			return true;
		}
		return false;
	}
}
