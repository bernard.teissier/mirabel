<?php

/**
 * This is the model class for table "Categorie_Revue".
 *
 * The followings are the available columns in table 'Categorie_Revue':
 * @property int $categorieId
 * @property int $revueId
 * @property string $hdateModif
 * @property int $modifPar
 *
 * @property Categorie $categorie
 * @property Revue $revue
 * @property Utilisateur $modifiePar
 */
class CategorieRevue extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @return CategorieRevue the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Categorie_Revue';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['categorieId, revueId', 'required'],
			['categorieId, revueId, modifPar', 'numerical', 'integerOnly' => true, 'allowEmpty' => true],
			['hdateModif',
				'default', 'value' => date("Y-m-d H:i:s"),
				'setOnEmpty' => false, 'on' => 'insert, update', ],
			// The following rule is used by search().
			['categorieId, revueId, hdateModif, modifPar', 'safe', 'on' => 'search'],
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'categorie' => [self::BELONGS_TO, 'Categorie', 'categorieId'],
			'revue' => [self::BELONGS_TO, 'Revue', 'revueId'],
			'modifiePar' => [self::BELONGS_TO, 'Utilisateur', 'modifPar'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'categorieId' => 'Thème',
			'revueId' => 'Revue',
			'hdateModif' => 'Date modif.',
			'modifPar' => 'Modif. par',
		];
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @param int|bool $pageSize Number of items for pagination (false to disable pagination).
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($pageSize = 25)
	{
		$criteria = new CDbCriteria;

		$criteria->compare('categorieId', $this->categorieId);
		$criteria->compare('revueId', $this->revueId);
		$criteria->compare('hdateModif', $this->hdateModif, true);
		$criteria->compare('modifPar', $this->modifPar);

		$sort = new CSort();
		$sort->attributes = ['categorieId', 'revueId', 'hdateModif', 'modifPar'];
		$sort->defaultOrder = 'hdateModif DESC';

		return new CActiveDataProvider(
			$this,
			[
				'criteria' => $criteria,
				'pagination' => ($pageSize ? ['pageSize' => $pageSize] : false),
				'sort' => $sort,
			]
		);
	}
}
