<?php

use models\validators\IssnValidator;

Yii::import('ext.Sphinx.*');

/**
 * Searches all objects through Sphinx
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class SearchGlobal extends CFormModel
{
	public $q;

	/**
	 * @var bool Display only the title that are freely accessible.
	 */
	public $accesLibre = false;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return [
			['q', 'length', 'max' => 255],
			['q', 'filter', 'filter' => 'trim'],

		];
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return [
			'query' => 'Requête',
		];
	}

	public function search($index, $pageSize=25)
	{
		if (!in_array($index, ['titres', 'ressources', 'editeurs'])) {
			throw new Exception('Paramètre non valide.');
		}
		$sphinxClient = new Sphinx(Yii::app()->params->itemAt('sphinx'));
		$sphinxClient->indexes = $index;
		if ($index === 'titres') {
			$this->q = preg_replace('/([A-Z])\.\s?(?=[A-Z]\b)/', '$1', $this->q);
			$sphinxClient->SetGroupBy('revueid', SPH_GROUPBY_ATTR, 'cletri ASC');
			$sphinxClient->SetSortMode(SPH_SORT_ATTR_ASC, 'obsolete');
		} else {
			$sphinxClient->SetSortMode(SPH_SORT_EXTENDED, 'cletri ASC');
		}

		if ($index === 'titres' && $this->accesLibre) {
			$sphinxClient->SetFilter('acceslibre', [1, 2, 3, 4, 5]);
		}

		if ($this->q) {
			$issn = $this->readIssn($this->q);
			if ($issn) {
				if ($index === 'titres') {
					$sphinxClient->SetFilter('issn', [$issn]);
				} else {
					return null;
				}
			} else {
				$sphinxClient->query = $sphinxClient->EscapeString($this->q);
			}
		}

		$params = $_GET;
		$params['type'] = $index;

		return new SphinxDataProvider(
			$sphinxClient,
			null, // no ActiveRecord models
			[
				'pagination' => ['pageSize' => $pageSize, 'params' => $params],
			]
		);
	}

	protected function readIssn($candidate)
	{
		if (!preg_match('/^\d{4}-?\d{3}[\dX]$/', $candidate)) {
			return '';
		}
		$v = new IssnValidator();
		$v->allowEmpty = false;
		$v->allowEmptyChecksum = true;
		$v->allowMissingCaret = true;
		$v->specialValues = [];
		if ($v->validateString($candidate)) {
			return substr(str_replace('-', '', $candidate), 0, 7);
		}
		return '';
	}
}
