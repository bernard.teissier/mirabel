<?php

require_once __DIR__ . '/Log.php';
require_once __DIR__ . '/Normalize.php';
require_once __DIR__ . '/Reader.php';
require_once __DIR__ . '/TitleIdentifier.php';

/**
 * Abstract class Import
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
abstract class Import
{
	public $verbose = 1;

	public $simulation = false;

	/** @var bool Create the titles automatically */
	public $directCreation = false;

	public $ignoreUnknownTitles = false;

	/** @var bool Query SUDOC on new titles? */
	public $sudoc = true;

	/**
	 * @var int Auto-filled. Overwrite for testing purposes.
	 */
	public $time;

	/**
	 * @var import\Log
	 */
	public $log;

	/**
	 * @var import\Normalize
	 */
	public $normalize;

	/** @var import\Reader */
	protected $reader;

	protected $name;

	protected $url;

	protected $header;

	/**
	 * @var Ressource
	 */
	protected $ressource;

	/**
	 * @var int Will set Service.collectionId on every service imported.
	 */
	protected $collectionId;

	/**
	 * @var Partenaire
	 */
	protected $partenaire;

	/**
	 * @var CDbConnection
	 */
	protected $db;

	/**
	 * @var array [colnum => collectionId]
	 */
	protected $collections = [];

	/**
	 * @var string Default encoding (overwritten in subclasses)
	 */
	protected $encoding = 'UTF-8';

	/**
	 * @var array
	 */
	protected $interventions = [];

	private $deadTitles = [];

	private $ignoredTitles = [];

	public function __set($name, $value)
	{
		echo "Undefined object attr: $name = " . print_r($value, true);
		debug_print_backtrace(0, 2);
		die();
	}

	public function init()
	{
		if ($this->log) {
			return;
		}
		set_time_limit(300);
		ini_set("default_socket_timeout", 120);
		$this->time = $_SERVER['REQUEST_TIME'];

		$this->db = Yii::app()->db; /// CDbConnection
		$this->log = new import\Log($this->verbose);
		$this->normalize = new import\Normalize($this);

		if (empty($this->reader)) {
			throw new Exception("Erreur dans le code source pour la ressource '{$this->name}.");
		}
		$this->reader->setLog($this->log);
		if ($this->url) {
			if (!$this->reader->readUrl($this->url, $this->header, $this->encoding)) {
				throw new Exception("Erreur lors de la lecture du fichier source '{$this->url}' : ." . $this->log->format('string'));
			}
		}

		if (empty($this->ressource)) {
			$this->ressource = Ressource::model()->findByAttributes(
				['identifiant' => $this->name, 'autoImport' => 1]
			);
		}
		if (empty($this->ressource)) {
			throw new Exception(
				"Erreur : la ressource '{$this->name}' n'a pu être trouvée. "
				. "Vérifiez qu'une ressource de cet identifiant existe, avec autoImport activé."
			);
		}

		// cleanup previous run
		/*
		$this->db->createCommand(
			"DELETE FROM Intervention WHERE import = {$this->id} AND ressourceId = {$this->ressource->id} "
			. "AND utilisateurIdVal IS NULL AND statut = 'attente'"
		)->execute();
		 */

		$this->afterInit();
	}

	/**
	 * Imports everything.
	 *
	 * @return string "info", "warning", "error".
	 */
	public function importAll()
	{
		if (empty($this->time) || empty($this->log)) {
			throw new Exception("Coding error: no init called for Import.");
		}
		$level = 'info';
		$lines = $this->reader->readLines();
		if ($lines && !$this->validateColMap($this->reader->colMap, $this->header)) {
			$this->log->addGlobal(
				'error',
				'separator',
				"Erreur en lisant l'entête du fichier (err. colMap)."
			);
			$lines = false;
		}
		if ($lines === false) {
			$level = 'error';
		} else {
			if (empty($lines)) {
				$this->log->addGlobal(
					'error',
					'*INIT*',
					"Le fichier d'entrée ne contient pas de données."
				);
			} else {
				foreach ($lines as $line) {
					if ($line) {
						if (!$this->importOnce($line)) {
							$level = "warning";
						}
					}
				}
			}
		}
		if ($level === 'error') {
			Yii::log($this->log->format('string'), CLogger::LEVEL_ERROR, "import");
		}
		return $level;
	}

	/**
	 * Returns the list of interventions.
	 * @return array of Intervention
	 */
	public function getInterventions()
	{
		return $this->interventions;
	}

	/**
	 * Returns the title attributes: titre, issn... and some extra: key, issne...
	 *
	 * @param array $line Splitted data line.
	 * @return array Assoc array with keys: "titre", "issn", "issne"...
	 */
	abstract public function extractTitleData($line);

	/**
	 * Returns a list of Collection IDs.
	 *
	 * @param array $line Splitted data line.
	 * @return array
	 */
	abstract public function extractCollections($line);

	/**
	 * Returns a list of Service attributes.
	 *
	 * @param array $line Splitted data line.
	 * @return array of assoc arrays.
	 */
	abstract public function extractServicesData($line);

	/**
	 * Returns a integer that is unique to this import type.
	 *
	 * @return int
	 */
	abstract public function getId();

	/**
	 * Getter for the ressource.
	 *
	 * @return Ressource
	 */
	public function getRessource()
	{
		return $this->ressource;
	}

	/**
	 * @return array
	 */
	public function getDeadTitles()
	{
		return $this->deadTitles;
	}

	/**
	 * @return array
	 */
	public function getIgnoredTitles()
	{
		return $this->ignoredTitles;
	}

	/**
	 * Called by init().
	 */
	protected function afterInit()
	{
		$this->initCollections();
	}

	/**
	 * Returns a new instance of Titre.
	 *
	 * @param array $attributes
	 * @param Intervention $i Will be modified.
	 * @return Titre new instance of Titre.
	 */
	protected function createTitle($attributes, &$i)
	{
		if (!empty($attributes['urlLocal'])) {
			$i->commentaire .= '<div><a href="' . CHtml::encode($attributes['urlLocal'])
				. '">Accès à la revue dans cette ressource</a></div>';
			unset($attributes['urlLocal']);
		}
		$title = new Titre();
		$title->setScenario('import');
		$title->setAttributes($attributes, true);
		$title->confirm = 1;
		$revue = new Revue();
		$revue->attributes = ['statut' => 'normal'];
		$i->contenuJson->create($revue);
		$i->action = 'revue-C';
		$title->revueId = 0;
		if (!empty($attributes['url'])) {
			$i->commentaire .= '<div><a href="' . CHtml::encode($attributes['url'])
				. '">Site web de la revue</a></div>';
		}
		if (!empty($attributes['sudocNoHolding'])) {
			$i->commentaire .= "<div>Sans notice Sudoc publique</div>";
		} elseif (!empty($attributes['sudoc'])) {
			$i->commentaire .= '<div><a href="' . sprintf(Titre::URL_SUDOC, $attributes['sudoc'])
				. '">Notice Sudoc</a></div>';
		} elseif (!empty($attributes['issn'])) {
			$i->commentaire .= '<div><a href="' . sprintf(Titre::URL_SUDOC_ISSN, $attributes['issn'])
				. '">Notice Sudoc par ISSN</a></div>';
		} elseif (!empty($attributes['issne'])) {
			$i->commentaire .= '<div><a href="' . sprintf(Titre::URL_SUDOC_ISSN, $attributes['issne'])
				. '">Notice Sudoc par ISSN-E</a></div>';
		}
		if (!empty($attributes['worldcat'])) {
			$i->commentaire .= '<div><a href="' . sprintf(Titre::URL_WORLDCAT, $attributes['worldcat'])
				. '">Worldcat</a></div>';
		}
		$i->contenuJson->create($title);
		return $title;
	}

	/**
	 * @param int $titleId
	 * @param array $attrs
	 * @return Identification|null
	 */
	protected function createIdentification($titleId, $attrs)
	{
		if ($titleId) {
			$identification = Identification::model()->findByAttributes(
				['titreId' => $titleId, 'ressourceId' => $this->ressource->id]
			);
		}
		if (empty($identification)) {
			$identification = new Identification();
			$identification->titreId = $titleId;
			$identification->ressourceId = $this->ressource->id;
		}
		if (isset($attrs['idInterne']) && $identification->idInterne != $attrs['idInterne']) {
			$identification->idInterne = $attrs['idInterne'];
		} else {
			// pas d'identification
			return null;
		}
		$identExists = Identification::model()->find(
			"idInterne = :idInterne AND ressourceId = :rid" . ($titleId ? " AND titreId <> $titleId" : ""),
			[':idInterne' => $identification->idInterne, ':rid' => $this->ressource->id]
		);
		if ($identExists) {
			$this->log->addLocal(
				'warning',
				"L'identifiant interne {$identification->idInterne} est déjà affecté à un autre titre de cette ressource."
			);
			return null;
		}
		return $identification;
	}

	/**
	 * Update the records in the table Identification.
	 *
	 * The changes are applied immediately (unless simulation is on)
	 * and do not appear in the Intervention.
	 *
	 * @param Titre $title
	 * @param array $attrs
	 * @return bool Success?
	 */
	protected function updateIdentification($title, $attrs)
	{
		$identification = $this->createIdentification($title->id, $attrs);
		if ($identification) {
			$this->log->addLocal(
				'info',
				"Identification mise à jour (ID interne)"
			);
			if (!$identification->validate()) {
				$this->log->addLocal(
					'warning',
					"Problème rencontré en validant les données d'identification (idInterne) : " . print_r($identification->getErrors(), true)
				);
				return false;
			}
			if (!$this->simulation) {
				if (!$identification->save(false)) {
					$this->log->addLocal("error", "Erreur en enregistrant l'identification.");
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Update the Intervention with the changes on ISSN and ISSN-E data.
	 *
	 * @param Titre $title
	 * @param array $attrs
	 * @param bool $write
	 * @param Intervention $i Will be modified.
	 */
	protected function updateIssns($title, $attrs, $write, &$i)
	{
		$changes = ['insert' => [], 'update' => [], 'delete' => []];
		if ($title->isNewRecord) {
			$existingIssns = [];
		} else {
			$existingIssns = $title->issns;
		}
		$finalIssns = $existingIssns;
		foreach (['issn', 'issne'] as $issnName) {
			if (empty($attrs[$issnName])) {
				continue;
			}
			$newAttributes = [
				'titreId' => $title->isNewRecord ? 0 : $title->id,
				'issn' => $attrs[$issnName],
				'support' => ($issnName === 'issn' ? Issn::SUPPORT_PAPIER : Issn::SUPPORT_ELECTRONIQUE),
				//'statut' => Issn::STATUT_VALIDE,
			];
			if ($issnName === 'issn' || empty($attrs['issn'])) {
				if (!empty($attrs['sudoc'])) {
					$newAttributes['sudocPpn'] = $attrs['sudoc'];
					$newAttributes['sudocNoHolding'] = $attrs['sudocNoHolding'];
				}
				if (!empty($attrs['worldcat'])) {
					$newAttributes['worldcatOcn'] = $attrs['worldcat'];
				}
			}

			$toInsert = true;
			if ($existingIssns) {
				foreach ($existingIssns as $existing) {
					/* @var $existing Issn */
					if ($existing->issn == $newAttributes['issn']) {
						$toInsert = false;
						if ($newAttributes !== array_intersect_key($newAttributes, $existing->attributes)) {
							$previous = clone ($existing);
							$existing->setAttributes($newAttributes, false);
							$changes['update'][] = [
								"before" => $previous,
								"after" => $existing,
							];
						}
					}
				}
			}
			if ($toInsert) {
				$newIssn = new Issn();
				$newIssn->setAttributes($newAttributes, false);
				try {
					$newIssn->fillBySudoc();
				} catch (\Exception $e) {
					\Yii::log("Import fillBySudoc(), " . $e->getMessage(), \CLogger::LEVEL_WARNING);
				}
				if ($newIssn->validate() && $newIssn->validateUnicity($title)) {
					$changes['insert'][] = [
						"after" => $newIssn,
					];
					$finalIssns[] = $newIssn;
					$this->log->addLocal(
						'info',
						"ISSN {$newIssn->issn} " . ($newIssn->sudocNoHolding ? "(NoHolding) " : "") . ($write ? "" : "peut être ") . "ajouté."
					);
				} else {
					if ($write) {
						$this->log->addLocal(
							'warning',
							"L'ISSN {$newIssn->issn} n'a pu être ajouté. "
							. join("\n", call_user_func_array('array_merge', $newIssn->errors))
						);
					} else {
						$this->log->addLocal(
							'warning',
							"L'ISSN {$newIssn->issn} proposé n'est pas valide. "
							. join("\n", call_user_func_array('array_merge', $newIssn->errors))
						);
					}
				}
			}
		}
		if ($write) {
			if ($finalIssns !== $existingIssns) {
				$changedTitleAttributes = $title->updateDatesFromIssn($finalIssns);
				if ($changedTitleAttributes && $i->contenuJson) {
					$i->contenuJson->modifyDetail('Titre', $changedTitleAttributes);
				}
			}
			$i->addChanges(array_filter($changes));
		}
	}

	/**
	 * Update the records in the table Service and signals any creation.
	 *
	 * Delete the duplicates services, that have the same:
	 *   * titreId
	 *   * ressourceId
	 *   * type
	 *   * acces
	 *
	 * @param Titre $title
	 * @param array $sData Array of Service Attrs
	 * @param array $collectionIds List of collectionId.
	 * @param Intervention $i Will be modified.
	 * @return array ['creations' => (number of service creations), 'modifications' => (int), 'wasEmpty' => (bool)]
	 */
	protected function updateServices($title, $sData, $collectionIds, &$i)
	{
		if ($collectionIds) {
			$actions = ['creations' => 0, 'modifications' => 0, 'wasEmpty' => true];
			$res = Yii::app()->db
				->createCommand(
					"SELECT `type`, GROUP_CONCAT(`id` SEPARATOR ',') "
					. " FROM Collection WHERE id IN (" . join(',', $collectionIds) . ")"
					. " GROUP BY `type`"
				)
				->queryAll(false);
			foreach ($res as $row) {
				$more = $this->updateServicesByCollectionType($title, $sData, explode(',', $row[1]), $row[0], $i);
				$actions['creations'] += $more['creations'];
				$actions['modifications'] += $more['modifications'];
				$actions['wasEmpty'] = $actions['wasEmpty'] && $more['wasEmpty'];
			}
		} else {
			$actions = $this->updateServicesByCollectionType($title, $sData, [], "", $i);
		}
		return $actions;
	}

	/**
	 * Update the records in the table Service and signals any creation.
	 *
	 * Delete the duplicates services, that have the same:
	 *   * titreId
	 *   * ressourceId
	 *   * type
	 *   * acces
	 *
	 * @param Titre $title
	 * @param array $sData Array of Service Attrs
	 * @param array $collectionIds List of collectionId.
	 * @param string $collType
	 * @param Intervention $i Will be modified.
	 * @return array ['creations' => (number of service creations), 'modifications' => (int), 'wasEmpty' => (bool)]
	 */
	protected function updateServicesByCollectionType($title, $sData, $collectionIds, $collType, &$i)
	{
		/**
		 * @todo Optimize with raw SQL fetching GROUP_CONCAT(sc.collectionId)?
		 */
		if ($collType) {
			$existing = Service::model()->findAllBySql(
				"SELECT s.* "
				. "FROM Service s"
				. "  JOIN Service_Collection sc ON sc.serviceId = s.id"
				. "  JOIN Collection c ON c.id = sc.collectionId"
				. " WHERE s.titreId = :tid AND s.ressourceId = :rid AND c.type = :ctype"
				. " GROUP BY s.id",
				[':tid' => $title->id, ':rid' => $this->ressource->id, ':ctype' => $collType]
			);
		} else {
			$existing = Service::model()->findAllByAttributes(
				['titreId' => $title->id, 'ressourceId' => $this->ressource->id]
			);
		}

		$actions = ['creations' => 0, 'modifications' => 0, 'wasEmpty' => empty($existing)];
		if (empty($sData)) {
			$this->log->addLocal('info', "Aucun accès déclaré");
			return $actions;
		}
		// update or create services
		foreach ($sData as $sd) {
			$old = null;
			foreach ($existing as $pos => $service) {
				/* @var $service Service */
				if ($service->acces == $sd['acces'] && $service->type == $sd['type']) {
					if (!$old) {
						$old = $service;
					} else {
						foreach (['dateBarrDebut', 'dateBarrFin'] as $x) {
							$oldDate = DateVersatile::convertdateToTs($old->{$x}, false);
							$newDate = DateVersatile::convertdateToTs($service->{$x}, false);
							$wantedDate = DateVersatile::convertdateToTs($sd[$x], false);
							if (abs($newDate - $wantedDate) < abs($oldDate - $wantedDate)) {
								// swap to keep the most similar one
								$x = $service;
								$service = $old;
								$old = $x;
								break;
							}
						}
						$this->log->addLocal('info', "Suppression de l'ancien accès {$service->id}, car doublon de même type, même accès");
						if (!isset($i->commentaire)) {
							$i->commentaire = "";
						}
						$i->commentaire .= " Suppression de l'accès doublon {$service->id} (même type et même accès)";
						$i->contenuJson->delete($service);
						if (empty($i->action)) {
							$i->action = 'service-U';
						}
					}
					unset($existing[$pos]);
				}
			}
			if ($old) {
				// update existing or keep it unchanged
				$new = clone ($old);
				$new->setScenario('import');
				$new->resetAttributes();
				$new->setAttributes($sd);
				$new->import = $this->getId();
				if ($i->contenuJson->update($old, $new, '', false)) {
					$this->log->addLocal('debug', "Modification de l'accès {$old->id}");
				}
				$existingCollectionIds = Yii::app()->db->createCommand(
					"SELECT c.id FROM Service_Collection sc"
						. " JOIN Collection c ON c.id = sc.collectionId"
						. " WHERE sc.serviceId = :oid"
						. ($collType ? " AND c.type = " . Yii::app()->db->quoteValue($collType) : "")
						. " ORDER BY c.id ASC"
				)->queryColumn([':oid' => $old->id]);
				if ($this->collectionId && $existingCollectionIds) {
					$collectionIds = array_unique(array_merge($existingCollectionIds, $collectionIds));
				}
				$i->updateCollections($old, $existingCollectionIds, $collectionIds, $this->log);
				$old->updateByPk($old->id, ['hdateImport' => $this->time]);
				if (empty($i->action)) {
					$i->action = 'service-U';
				}
				$actions['modifications']++;
			} else {
				// create a new service
				$new = new Service();
				$new->setScenario('import');
				$new->attributes = $sd;
				$new->titreId = $title->id;
				$new->ressourceId = $this->ressource->id;
				$new->import = $this->getId();
				$new->hdateImport = $this->time;
				$i->contenuJson->create($new);
				$this->log->addLocal('debug', "Création d'un accès {$new->acces}");
				$i->updateCollections($new, [], $collectionIds, $this->log);
				if (empty($i->action)) {
					$i->action = 'service-C';
				}
				$actions['creations']++;
			}
		}
		return $actions;
	}

	/**
	 * Links to an editor, or add an intervent to create this Editeur.
	 *
	 * @param Titre $title
	 * @param array $attrs With the field 'editeur'
	 * @param Intervention $i Will be modified.
	 * @return bool True if there was some change.
	 */
	protected function updateEditors($title, $attrs, $i)
	{
		if (empty($attrs['editeur'])) {
			return false;
		}
		foreach (explode(';', $attrs['editeur']) as $e) {
			$this->updateEditor($title, trim($e), $i);
		}
		return true;
	}

	/**
	 * Links to an editor, or add an intervent to create this Editeur.
	 *
	 * @param Titre $title
	 * @param string $editeur
	 * @param Intervention $i Will be modified.
	 * @return bool True if there was some change.
	 */
	protected function updateEditor($title, $editeur, $i)
	{
		if (!$editeur) {
			return false;
		}
		if ($title->id) {
			// Do not update anything if the title already exists
			return false;
			/*
			// Do not update anything if the title already has an editor
			if ($title->editeurs) {
				return false;
			}
			// Does the link title-editor already exist? Yes => stop
			foreach ($title->editeurs as $ed) {
				if (strcasecmp($ed->nom, $attrs['editeur']) === 0 || strcasecmp($ed->longName, $attrs['editeur']) === 0) {
					return false;
				}
			}
			 */
		}
		// Does the editor exist? Yes => create the link unless it already exists
		$m = [];
		if (preg_match('/^(Le |La |Les |L\')(.+)/', $editeur, $m)) {
			$editeurPrefixe = $m[1];
			$editeurNom = $m[2];
			$editors = Editeur::model()->findAllBySql(
				"SELECT * FROM Editeur WHERE nom = :nom OR (nom = :shortname AND prefixe = :prefix)",
				['nom' => $editeur, 'shortname' => $editeurNom, 'prefix' => $editeurPrefixe]
			);
		} else {
			$editeurPrefixe = '';
			$editeurNom = $editeur;
			$editors = Editeur::model()->findAllByAttributes(['nom' => $editeur]);
		}
		if (count($editors) === 1) {
			$link = new TitreEditeur();
			$link->editeurId = $editors[0]->id;
			$link->titreId = $title->id ?: 0;
			if (TitreEditeur::model()->findByAttributes($link->attributes)) {
				return false;
			}
			$i->contenuJson->create($link);
			$this->log->addLocal('info', "Création du lien avec l'éditeur $editeurNom");
			return true;
		}
		if (count($editors) > 1) {
			$this->log->addLocal('warning', "Plusieurs éditeurs homonymes : {$editeur}.");
			return false;
		}
		// Propose to create the editor (and apply if DirectCreation and Title created)
		$editor = new Editeur();
		$editor->nom = $editeurNom;
		$editor->prefixe = $editeurPrefixe;
		$intervention = new Intervention('import');
		$intervention->import = $this->getId();
		$intervention->statut = 'attente';
		$intervention->commentaire = "Import du titre « {$title->titre} » "
			. "dans la ressource « {$this->ressource->nom} ».\n"
			. "Source : " . get_class($this);
		$intervention->description = "Création de l'éditeur « {$editor->nom} »";
		$intervention->contenuJson = new InterventionDetail();
		$intervention->contenuJson->create($editor);
		$intervention->action = 'editeur-C';
		$intervention->ressourceId = $this->ressource->id;
		if ($this->simulation) {
			$known = 0;
		} else {
			$known = Intervention::model()->deleteAllByAttributes(
				[
					'action' => $intervention->action,
					'description' => $intervention->description,
					'statut' => 'attente',
				],
				'import > 0'
			);
			// Insert link with Editeur only if it is a _new_ title DIRECTLY CREATED
			if ($this->directCreation && !$title->id) {
				$intervention->accept(false);
				$link = new TitreEditeur();
				$link->editeurId = $intervention->contenuJson->lastInsertId['Editeur'];
				$link->titreId = 0;
				$i->contenuJson->create($link);
			}
			$intervention->save();
		}
		$this->interventions[] = $intervention;
		$this->log->addLocal(
			'info',
			"Création de l'éditeur « {$editor->nom} »" . ($known ? ' (déjà proposé)' : '')
		);
		return true;
	}

	/**
	 * Import one line read from the source file.
	 *
	 * @param string $line
	 * @return bool Success?
	 */
	protected function importOnce($line)
	{
		if (empty($this->time) || empty($this->log)) {
			throw new Exception("Coding error: no init called for Import.");
		}
		$this->log->verbose = $this->verbose;
		$this->normalize->verbose = $this->verbose;
		$this->log->initLocal();
		$row = $this->reader->splitRawLine($line);
		if (count($row) < 5) {
			$this->log->addGlobal(
				'warning',
				'"_données_"',
				"Format étrange de données, " . count($row) . " colonnes seulement sur cette ligne."
			);
			return false;
		}

		try {
			$titleAttributes = array_map(
				function ($x) {
					return $x ? trim($x, "\t \n ​") : $x;
				}, // remove odd characters, keep plain spaces
				$this->extractTitleData($row)
			);
		} catch (\Exception $e) {
			$this->log->addGlobal(
				'error',
				'Erreur sur un titre (données incorrectes ?)',
				$e->getMessage() . "\nDonnées traitées : " . join("|", $row)
			);
		}
		if (empty($titleAttributes['titre'])) {
			return false;
		}
		if (!empty($titleAttributes['issne']) && preg_match('/^en/', $titleAttributes['issne'])) {
			unset($titleAttributes['issne']); // pas d'issn-e "en cours"
		}
		$titleIdentifier = new TitleIdentifier($this->log);
		$titleIdentifier->setRessource($this->ressource);
		$title = $titleIdentifier->identify($titleAttributes);
		if ($title) {
			$title->setScenario('import');
		}
		$this->log->incAction(\import\Log::ACTION_REVUE);

		if ($this->ignoreUnknownTitles && !$title) {
			$this->log->addLocal(
				'info',
				"Revue inconnue, donc accès ignorés.\n" . print_r($titleAttributes, true)
			);
			$this->ignoredTitles[] = $titleAttributes;
			return true;
		}

		$intervention = $this->createIntervention();

		$creation = false;
		if (!$title) {
			$title = $this->importUnknownTitle($titleAttributes, $intervention);
			$this->interventions[] = $intervention;
			$this->updateIssns($title, $titleAttributes, true, $intervention);
			$this->updateEditors($title, $titleAttributes, $intervention);
			if (!$this->simulation) {
				if ($this->directCreation) {
					$intervention->commentaire .= ' Création directe.';
					$intervention->accept(false);
				}
				if (!$intervention->save()) {
					$this->log->incAction(\import\Log::ACTION_ERREUR);
					throw new Exception(
						"Erreur lors de l'enregistrement de l'intervention : "
						. print_r($intervention->getErrors(), true)
					);
				}
				if ($this->directCreation) {
					$title = $titleIdentifier->identify($titleAttributes);
					$intervention = $this->createIntervention();
					$creation = true;
				}
			}
		}
		if ($title && $title->id) {
			$intervention->titreId = $title->id;
			$intervention->revueId = $title->revueId;

			$this->updateIdentification($title, $titleAttributes);
			$this->updateIssns($title, $titleAttributes, false, $intervention);

			if ($this->directCreation) {
				if ($creation && $title->dateFin > 0) {
					$this->deadTitles[$title->id] = $title->titre;
				}
				$this->updateEditors($title, $titleAttributes, $intervention);
			}

			try {
				$servicesData = $this->extractServicesData($row);
				$collectionIds = array_filter($this->extractCollections($row));
				if ($this->collectionId && !$collectionIds) {
					$collectionIds = [$this->collectionId];
				}
				$serviceActions = $this->updateServices($title, $servicesData, $collectionIds, $intervention);
			} catch (\Exception $e) {
				$this->log->addGlobal(
					'error',
					"Erreur sur des accès du titre {$title->id} (données incorrectes ?)",
					$e->getMessage() . "\nDonnées traitées : " . join("|", $row)
				);
				$this->log->addLocal(
					'error',
					"Données incorrectes ?\n" . $e->getMessage() . "\nDonnées traitées : " . join("|", $row)
				);
			}

			if ($intervention->contenuJson->isEmpty()) {
				$this->log->addLocal('info', "Accès « {$title->titre} » inchangés.");
			} else {
				$isCreation = $serviceActions['wasEmpty'] && $serviceActions['creations'];
				$intervention->description = ($isCreation ? "Création" : "Modification")
					. " d'accès en ligne, revue « "
					. $title->titre . " » / « " . $this->ressource->nom . " »";
				$this->log->addLocal(
					'info',
					($isCreation ? "Création" : "Modification") . " des accès"
				);
				if ($this->simulation) {
					if ($isCreation) {
						$this->log->incAction(\import\Log::ACTION_CREATION);
					} elseif ($serviceActions['modifications']) {
						$this->log->incAction(\import\Log::ACTION_MODIFICATION);
					}
				} else {
					if ($intervention->accept()) {
						if ($isCreation) {
							$this->log->incAction(\import\Log::ACTION_CREATION);
						} elseif ($serviceActions['modifications']) {
							$this->log->incAction(\import\Log::ACTION_MODIFICATION);
						}
					} else {
						$this->log->incAction(\import\Log::ACTION_ERREUR);
						$this->log->addLocal(
							'error',
							"Erreur (intervention rejetée) : " . print_r($intervention->errors, true)
						);
					}
				}
				$this->interventions[] = $intervention;
			}
		}
		$interventionUrl = Yii::app()->params['baseUrl'] . '/intervention/' . $intervention->getPrimaryKey();
		if ($title && !empty($title->revueId)) {
			$this->log->flushLocal(
				$title->titre,
				$title->id ? Yii::app()->createAbsoluteUrl('/revue/view', ['id' =>  $title->revueId, 'nom' => Norm::urlParam($title->getFullTitle())]) : ''
			);
		} elseif (!empty($title->titre)) {
			$this->log->flushLocal($title->titre, $this->simulation ? '' : $interventionUrl);
		} else {
			// no URL, so we give the raw input line
			$this->log->flushLocal($titleAttributes['titre'], $this->simulation ? '' : $interventionUrl);
		}
		return true;
	}

	/**
	 * Adds the new Revue/Titre to the Intervention and saves it.
	 *
	 * @param array $titleAttributes
	 * @param Intervention $intervention
	 * @throws Exception
	 * @return Titre
	 */
	protected function importUnknownTitle($titleAttributes, &$intervention)
	{
		if ($this->sudoc) {
			$api = new \models\sudoc\Api();
			if (!empty($titleAttributes['issn']) && empty($titleAttributes['sudoc'])) {
				$ppnResult = $api->getPpn($titleAttributes['issn']);
				if ($ppnResult) {
					$titleAttributes['sudoc'] = $ppnResult[0]->ppn;
					$titleAttributes['sudocNoHolding'] = (boolean) $ppnResult[0]->noHolding;
				}
			}
			if (!empty($titleAttributes['sudoc'])) {
				$notice = $api->getNotice($titleAttributes['sudoc']);
				if ($notice !== null) {
					if ($notice->worldcat && empty($titleAttributes['worldcat'])) {
						$titleAttributes['worldcat'] = $notice->worldcat;
					}
					foreach (['dateDebut', 'dateFin'] as $k) {
						if ($notice->{$k} && empty($titleAttributes[$k]) && preg_match('/^\d{4}(-\d\d){0,2}$/', $notice->{$k})) {
							$titleAttributes[$k] = $notice->{$k};
						}
					}
				}
			}
		}
		$title = $this->createTitle($titleAttributes, $intervention);
		$intervention->description = "Création de la revue « " . $title->titre . " »";
		if (!$title->validate(['titre', 'prefixe', 'sigle', 'dateDebut', 'dateFin', 'issn', 'url'])) {
			$intervention->statut = 'refusé';
			$this->log->addLocal('error', "Nouveau titre non-valide : " . print_r($title->errors, true));
			$this->log->incAction(\import\Log::ACTION_ERREUR);
		} else {
			if ($this->simulation) {
				$known = 0;
			} else {
				$known = Intervention::model()->deleteAllByAttributes(
					[
						'action' => $intervention->action,
						'description' => $intervention->description,
						'import' => $this->getId(),
						'ressourceId' => $this->ressource->id,
						'statut' => 'attente',
					]
				);
			}
			$this->log->addLocal('warning', "Nouveau titre" . ($known ? ' (déjà proposé).' : '.'));
			$this->log->incAction(\import\Log::ACTION_NOUVEAUTITRE);
			$identification = $this->createIdentification(0, $titleAttributes);
			if ($identification) {
				$intervention->contenuJson->create($identification);
			}
		}
		return $title;
	}

	protected function getCol($line, $name, $default='')
	{
		return $this->reader->getCol($line, $name, $default);
	}

	/**
	 * Validates the map : colName => position.
	 *
	 * @param array $colMap
	 * @param array $header
	 * @return bool
	 */
	protected function validateColMap($colMap, $header)
	{
		if (count($colMap) !== count($header)) {
			$this->log->addGlobal(
				'error',
				'En-tête des données',
				"L'en-tête des données a changé, les colonnes suivantes n'existent plus :"
				. join(', ', array_diff($header, array_keys($colMap)))
			);
			return false;
		}
		return true;
	}

	/**
	 * Called by init().
	 */
	protected function initCollections()
	{
	}

	protected function createIntervention()
	{
		$intervention = new Intervention('import');
		$intervention->contenuJson = new InterventionDetail();
		$intervention->statut = 'attente';
		$intervention->import = $this->getId();
		$intervention->ressourceId = $this->ressource->id;
		$intervention->commentaire = '';
		return $intervention;
	}
}
