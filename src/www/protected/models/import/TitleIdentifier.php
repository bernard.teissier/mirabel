<?php

/**
 * Description of TitleIdentifier
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class TitleIdentifier
{
	/**
	 * @var Ressource
	 */
	private $ressource;

	private $log;

	public function __construct(import\Log $logger)
	{
		$this->log = $logger;
	}

	public function setRessource(Ressource $ressource)
	{
		$this->ressource = $ressource;
		return $this;
	}

	/**
	 * Identify a Titre from its attributes.
	 *
	 * By default (overwritten by $this->ressource->importIdentifications) :
	 * - ISSN (et ISSNL)
	 * - ISSN-E
	 * - ressource + idInterne
	 * - URL
	 * - titre + editeur
	 *
	 * @param array $attributes
	 * @return ?Titre Existing Titre, or null if not found.
	 */
	public function identify($attributes)
	{
		if (!$this->ressource) {
			throw new Exception("Ressource not declared");
		}

		foreach ($this->getMethods() as $method) {
			if (method_exists($this, "identifyBy" . $method)) {
				$title = call_user_func([$this, "identifyBy" . $method], $attributes);
				if ($title) {
					return $title;
				}
			} else {
				$this->log->addGlobal("warning", "Erreur de programmation", "La méthode d'identification de titre '$method' est inconnue.");
			}
		}
		return null;
	}

	/**
	 * List of strings that describe identification methods
	 *
	 * @return array
	 */
	protected function getMethods()
	{
		if (!empty($this->ressource->importIdentifications)) {
			return array_filter(preg_split('/\s*,\s*/', trim($this->ressource->importIdentifications, " ,")));
		}
		return ['issn', 'issne', 'idInterne', 'url', 'editeur'];
	}

	protected function identifyByIssn($attributes)
	{
		if (!empty($attributes['issn'])) {
			$title = Titre::model()->findBySql(
				"SELECT t.* FROM Titre t JOIN Issn i ON t.id = i.titreId WHERE i.issn = :issn ORDER BY (t.obsoletePar IS NULL) DESC, dateFin DESC",
				[':issn' => $attributes['issn']]
			);
			if ($title) {
				$this->log->addLocal('debug', 'Titre identifié par issn');
				return $title;
			}
			$issnlTitle = Titre::model()->findBySql(
				"SELECT t.* FROM Titre t JOIN Issn i ON t.id = i.titreId WHERE i.issnl = :issn",
				[':issn' => $attributes['issn']]
			);
			if ($issnlTitle) {
				$this->log->addLocal('debug', 'Titre identifié par issn (issnl)');
				return $issnlTitle;
			}
		}
		return null;
	}

	protected function identifyByIssne($attributes)
	{
		if (empty($attributes['issne'])) {
			return null;
		}
		$title = Titre::model()->findBySql(
			"SELECT t.* FROM Titre t JOIN Issn i ON t.id = i.titreId WHERE i.issn = :issn ORDER BY (t.obsoletePar IS NULL) DESC, dateFin DESC",
			[':issn' => $attributes['issne']]
		);
		if ($title) {
			$this->log->addLocal('debug', 'Titre identifié par issne');
			return $title;
		}
		$issnlTitle = Titre::model()->findBySql(
			"SELECT t.* FROM Titre t JOIN Issn i ON t.id = i.titreId WHERE i.issnl = :issn",
			[':issn' => $attributes['issne']]
		);
		if ($issnlTitle) {
			$this->log->addLocal('debug', 'Titre identifié par issne (issnl)');
			return $issnlTitle;
		}
		return null;
	}

	protected function identifyByIdInterne($attributes)
	{
		$title = null;
		if (!empty($attributes['idInterne'])) {
			$title = Titre::model()->findBySql(
				"SELECT t.* FROM Titre t JOIN Identification i ON (t.id = i.titreId AND i.ressourceId = :rid) "
				. " WHERE idInterne = :idint",
				[':rid' => $this->ressource->id, ':idint' => $attributes['idInterne']]
			);
			if ($title) {
				$this->log->addLocal('debug', 'Titre identifié par ID interne ' . $attributes['idInterne']);
			}
		}
		return $title;
	}

	protected function identifyByUrl($attributes)
	{
		$title = null;
		if (!empty($attributes['url'])) {
			$title = Titre::model()->findByAttributes(
				['titre' => $attributes['titre'], 'url' => $attributes['url']]
			);
			if ($title) {
				$this->log->addLocal('debug', 'Titre identifié par son URL');
			}
		}
		return $title;
	}

	protected function identifyByEditeur($attributes)
	{
		$title = null;
		if (!empty($attributes['editeur'])) {
			$sql = "SELECT t.* "
				. "FROM Titre t "
				. "JOIN Titre_Editeur te ON te.titreId = t.id "
				. "JOIN Editeur e ON e.id = te.editeurId "
				. "WHERE t.titre = :titre AND e.nom = :editeur";
			$titles = Titre::model()->findAllBySql(
				$sql,
				['titre' => $attributes['titre'], 'editeur' => $attributes['editeur']]
			);
			if (count($titles) == 1) {
				$title = $titles[0];
				$this->log->addLocal('debug', 'Titre identifié par nom+éditeur');
			}
		}
		return $title;
	}
}
