<?php

namespace import;

/**
 * Ancestor for an import input source.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
abstract class Reader
{
	/**
	 * @var array Maps a colum name to its position in the CSV.
	 */
	public $colMap;

	/**
	 * @var bool
	 */
	public $ignoreHeaderCase = true;

	/** @var Log */
	protected $log;

	/**
	 * @var array
	 */
	protected $header;

	/**
	 * @param Log $log
	 */
	public function setLog($log)
	{
		$this->log = $log;
	}

	/**
	 * @param string $name
	 * @return int|null
	 */
	public function getHeaderRank($name)
	{
		if ($this->ignoreHeaderCase) {
			$name = strtolower($name);
		}
		return ($this->colMap[$name] ?? null);
	}

	/**
	 * @param array $line
	 * @param string $name
	 * @param mixed $default
	 * @return string
	 */
	public function getCol($line, $name, $default='')
	{
		if (!isset($this->colMap[$name])) {
			return $default;
		}
		$position = $this->colMap[$name];
		if (!isset($line[$position])) {
			throw new \Exception("Pas de colonne '$name' ($position) dans les données lues");
		}
		// trim all kinds of spaces
		$val = preg_replace('/^[\pZ\pC]+|[\pZ\pC]+$/u', '', $line[$position]);
		return ($val === '' ? $default : $val);
	}

	/**
	 * @return array
	 */
	public function getHeader()
	{
		return $this->header;
	}

	abstract public function readUrl($url);

	abstract public function readLines(): iterable;

	abstract public function splitRawLine(string $line): array;

	protected function flattenColumns($cols)
	{
		foreach ($cols as $k => $c) {
			if (is_scalar($c) && strpos($c, '|') !== false) {
				$c = array_filter(explode('|', $c));
			}
			if (is_array($c)) {
				unset($cols[$k]);
				$cols = array_merge($cols, $c);
			}
		}
		return $cols;
	}
}
