<?php

use models\validators\IssnValidator;

require_once __DIR__ . '/Import.php';
require_once __DIR__ . '/ReaderCsv.php';

/**
 * Import a CSV source with the format KBART.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class ImportKbart extends Import
{
	public static $ignoredFields = [
		'first_author',
		'date_monograph_published_print',
		'date_monograph_published_online',
		'monograph_volume',
		'monograph_edition',
		'first_editor',
		'parent_publication_title_id',
		'preceding_publication_title_id',
	];

	protected $separator = "\t";

	protected $encoding = 'UTF-8';

	protected $acces = 'Libre';

	protected $lacunaire = 0;

	protected $selection = 0;

	protected $defaultType = '';

	protected $ignoreUrl = false;

	/**
	 * @var array Names prefixed by "t." will be used by extractTitleData()
	 */
	protected $fields = [
		"t.titre" => "publication_title",
		"t.issn" => "print_identifier",
		"t.issne" => "online_identifier",
		"deb_date" => "date_first_issue_online",
		"deb_volume" => "num_first_vol_online",
		"deb_numero" => "num_first_issue_online",
		"fin_date" => "date_last_issue_online",
		"fin_volume" => "num_last_vol_online",
		"fin_numero" => "num_last_issue_online",
		"url" => "title_url",
		"t.idInterne" => "title_id",
		"barriere" => "embargo_info",
		"couverture" => "coverage_depth",
		"notes" => ["notes", "coverage_notes"], // unused (notes read at service level)
		"t.editeur" => "publisher_name",
		"publication_type" => "publication_type",
		"access_type" => "access_type",
		'local_RSS_URL',
		'local_alertes_URL',
	];

	protected $line;

	/**
	 * Constructor.
	 */
	public function __construct($parameters)
	{
		$this->ressource = Ressource::model()->findByPk($parameters['ressourceId']);
		foreach (['acces', 'defaultType', 'lacunaire', 'selection', 'url', 'simulation', 'directCreation', 'ignoreUnknownTitles', 'ignoreUrl', 'collectionId'] as $k) {
			if (isset($parameters[$k])) {
				$this->{$k} = $parameters[$k];
			}
		}
		$this->acces = ucfirst(strtolower($this->acces));
		$this->header = array_values($this->fields);
		$this->reader = new import\ReaderCsv(
			$this->separator,
			array_merge($this->header, self::$ignoredFields),
			$this->encoding
		);
		$this->init();
	}

	/**
	 * Returns a integer that is unique to this import type.
	 *
	 * @return int
	 */
	public function getId()
	{
		return ImportType::getSourceId('KBART');
	}

	/**
	 * Returns the title attributes: titre, issn... and some extra: key, issne...
	 *
	 * @param array $line Splitted CSV line.
	 * @return array Assoc array with keys: "titre", "issn", "issne"...
	 */
	public function extractTitleData($line)
	{
		$type = $this->getCol($line, "publication_type");
		if ($type && strtolower($type) !== 'serial') { // KBARTv2: only serial publications (journals)
			return [];
		}
		$titleData = [];
		foreach ($this->fields as $name => $col) {
			if (strncmp($name, "t.", 2) === 0) {
				$attr = substr($name, 2);
				if (is_array($col)) {
					foreach ($col as $c) {
						if (empty($titleData[$attr])) {
							$titleData[$attr] = $this->getCol($line, $c);
						}
					}
				} else {
					$titleData[$attr] = $this->getCol($line, $col);
				}
			}
		}
		$titleData['urlLocal'] = $this->getCol($line, 'title_url');
		$m = [];
		if (preg_match('/^(L\'|Le |La |Les )(\S.+)$/', $titleData['titre'], $m)) {
			$titleData['prefixe'] = $m[1];
			$titleData['titre'] = $m[2];
		} else {
			$titleData['prefixe'] = '';
		}
		foreach (['issn', 'issne'] as $issn) {
			$v = new IssnValidator();
			if (!empty($titleData[$issn]) && !$v->validateString($titleData[$issn])) {
				unset($titleData[$issn]);
				$this->log->addLocal("warning", $this->fields["t." . $issn] . " n'est pas un ISSN.");
			}
		}
		return import\Normalize::cleanTitleData($titleData);
	}

	/**
	 * Returns a list of Service objects.
	 *
	 * @param array $line Splitted CSV line.
	 * @return array of Service records
	 */
	public function extractServicesData($line)
	{
		$acces = $this->getAccessType($line);
		$this->line = $line;
		$template = $this->getTitleCoverage($line);
		if (empty($template)) {
			return [];
		}
		if (!$this->ignoreUrl) {
			$template['url'] = $this->getCol($line, 'title_url');
		}

		$dateDebut = $this->getDateCol('date_first_issue_online');
		$dateFin = $this->getDateCol('date_last_issue_online');
		$numeroDebut = import\Normalize::buildVolNum($this->getCol($line, 'num_first_vol_online'), $this->getCol($line, 'num_first_issue_online'));
		$numeroFin = import\Normalize::buildVolNum($this->getCol($line, 'num_last_vol_online'), $this->getCol($line, 'num_last_issue_online'));
		$embargo = $this->getCol($line, 'embargo_info');
		$service = array_merge(
			$template,
			[
				'acces' => $acces,
				'volDebut' => import\Normalize::toNum($this->getCol($line, 'num_first_vol_online')),
				'noDebut' => import\Normalize::toNum($this->getCol($line, 'num_first_issue_online')),
				'numeroDebut' => $numeroDebut,
				'volFin' => import\Normalize::toNum($this->getCol($line, 'num_last_vol_online')),
				'noFin' => import\Normalize::toNum($this->getCol($line, 'num_last_issue_online')),
				'numeroFin' => $numeroFin,
				'dateBarrDebut' => $dateDebut,
				'dateBarrFin' => $dateFin,
			]
		);

		if (strpos($embargo, ';') !== false) {
			// service entre ces dates
			$split = explode(';', $embargo);
			$embargoBefore = $this->normalize->convKbartToDate($split[0]);
			$embargoAfter = $this->normalize->convKbartToDate($split[1]);
			$service['dateBarrDebut'] = $embargoBefore['fulldate'] > $dateDebut ? $embargoBefore['date'] : $dateDebut;
			$service['dateBarrFin'] = $embargoAfter['fulldate'] < $dateFin ? $embargoAfter['date'] : $dateFin;
		} else {
			$dateBarr = $this->normalize->convKbartToDate($embargo);

			if ($dateBarr['type'] === 'P') {
				// service avant cette date
				if ($dateBarr['fulldate'] < $dateFin) {
					$this->log->addLocal(
						"info",
						"Attention, la date de fin d'accès {$dateFin} est postérieure à la date barrière {$dateBarr['date']}."
					);
					//$service['dateBarrFin'] = $dateBarr['date'];
				}
			} elseif ($dateBarr['type'] === 'R') {
				// service après cette date
				if ($dateBarr['fulldate'] > $dateDebut) {
					$this->log->addLocal(
						"info",
						"Attention, la date de début d'accès {$dateDebut} est antérieure à la date barrière {$dateBarr['date']}."
					);
					//$service['dateBarrDebut'] = $dateBarr['date'];
				}
			}
		}
		return [$service];
	}

	/**
	 * Returns a list of Collection IDs.
	 *
	 * @param array $line Splitted CSV line.
	 * @return array
	 */
	public function extractCollections($line)
	{
		return [];
	}

	/**
	 * Validates the map : colName => position.
	 * Replaces the parent method.
	 *
	 * @param array $colMap
	 * @param array $header
	 * @return bool
	 */
	protected function validateColMap($colMap, $header)
	{
		$required = [
			'publication_title', 'print_identifier', 'online_identifier', 'title_url', 'embargo_info',
		];
		$valid = true;
		foreach ($required as $col) {
			if (!isset($colMap[$col])) {
				$valid = false;
				$this->log->addGlobal(
					'error',
					'En-tête CSV',
					"La colonne requise '{$col}' n'est pas présente dans la ligne d'en-tête CSV."
				);
			}
		}
		return $valid;
	}

	protected function getDateCol($colName)
	{
		$rawDate = $this->getCol($this->line, $colName);
		$isoDate = import\Normalize::toIsoDate($rawDate);
		if (!$isoDate) {
			return null;
		}
		if ($rawDate !== $isoDate) {
			$this->log->addLocal("warning", "La date '$rawDate' dans $colName n'est pas au format ISO. '$isoDate' a été utilisé.");
		}
		return $isoDate;
	}

	protected function getTitleCoverage($line)
	{
		$notes = $this->getCol($line, 'notes');
		if (!$notes) {
			$notes = $this->getCol($line, 'coverage_notes');
		}
		$service = [
			'selection' => $this->selection,
			'lacunaire' => $this->lacunaire,
			'notes' => $notes,
			'embargoInfo' => $this->getCol($line, 'embargo_info'),
		];
		$rssUrl = $this->getCol($line, 'local_RSS_URL');
		if ($rssUrl) {
			$service['alerteRssUrl'] = $rssUrl;
		}
		$alertesUrl = $this->getCol($line, 'local_alertes_URL');
		if ($alertesUrl) {
			$service['alerteMailUrl'] = $alertesUrl;
		}

		$type = strtolower($this->getCol($line, 'coverage_depth'));
		switch ($type) {
			case 'fulltext':
			case 'full text':
				$service['type'] = 'Intégral';
				break;
			case 'selected articles':
				$service['type'] = $this->defaultType;
				$service['selection'] = 1;
				break;
			case 'abstract':
			case 'abstracts':
				$service['type'] = 'Résumé';
				break;
			default:
				$service['type'] = $this->defaultType;
				break;
		}
		if (empty($service['type'])) {
			$this->log->addLocal(
				"error",
				"Le 'coverage_depth' est à '$type', sans type d'accès par défaut déclaré, donc cette ligne n'est pas importée."
			);
			return null;
		}
		return $service;
	}

	/**
	 * Read the access defined by the record, or use the default one.
	 *
	 * @param array $line
	 * @return string
	 */
	private function getAccessType($line)
	{
		$byLine = $this->getCol($line, 'access_type');
		switch ($byLine) {
			case 'F':
				return 'Libre';
			case 'P':
				return 'Restreint';
			default:
				if (empty($this->acces)) {
					throw new Exception("'access_type' inconnu, et pas de valeur par défaut pour le type d'accès (libre, restreint).");
				}
				return $this->acces;
		}
	}
}
