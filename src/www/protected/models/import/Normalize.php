<?php

namespace import;

\Yii::import('ext.validators.*');

/**
 * class import\Normalize
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class Normalize
{
	public $verbose;

	protected $log;

	protected $parent;

	public function __construct($parent = null)
	{
		$this->parent = $parent;
		if ($parent && isset($parent->log)) {
			$this->log = $parent->log;
		}
	}

	/**
	 * @param string $embargo
	 * @return string
	 */
	public static function getReadableKbartEmbargo($embargo)
	{
		$date = (new self)->convKbartToDate($embargo);
		return $date['info'];
	}

	/**
	 * Returns an array('date' => , 'type' => P|R, 'info' => ).
	 *
	 * @param string $input
	 * @param int $time (opt) timestamp
	 * @return array
	 */
	public function convKbartToDate($input, $time=null)
	{
		if (!$time) {
			if ($this->parent) {
				$time = $this->parent->time;
			} else {
				$time = $_SERVER['REQUEST_TIME'];
			}
		}
		if (!$input) {
			return ['date' => '', 'type' => '', 'info' => ''];
		}
		if (!preg_match('/^\s*([PR])(\d+)([DMY])\s*$/', $input, $matches)) {
			if ($this->log) {
				$this->log->addLocal('error', "Champ dateBarrière invalide (K-Bart attendu) : " . $input);
			}
			return ['date' => '', 'fulldate' => '', 'type' => '', 'info' => ''];
		}
		$date = [
			'date' => '',
			'fulldate' => '',
			'type' => $matches[1],
			'info' => 'Accès ' . (($matches[1] === 'P') ? 'avant ' : 'après ') . 'une date barrière mobile de ',
		];
		$plural = $matches[2] > 1 ? 's' : '';
		$step = ($matches[1] === 'P') ? 1 : 0;
		switch ($matches[3]) {
			case 'M':
				$yesterday = $time - 24*3600;
				$date['info'] .= "{$matches[2]} mois calendaire{$plural}";
				$step += $matches[2] - 1;
				if ($step) {
					$date['date'] = date('Y-m', strtotime("-$step month", $yesterday));
				} else {
					$date['date'] = date('Y-m', $yesterday);
				}
				break;
			case 'D':
				$years = $matches[2] % 365 ? 0 : $matches[2] / 365;
				$months = $matches[2] % 30 ? 0 : $matches[2] / 30;
				if ($years) {
					$plural = $years > 1 ? 's' : '';
					$date['info'] .= "$years année{$plural} glissante{$plural}";
					$date['date'] = date('Y-m-d', strtotime("-{$years} year{$plural}", $time));
				} elseif ($months) {
					$plural = $months > 1 ? 's' : '';
					$date['info'] .= "$months mois glissant{$plural}";
					$date['date'] = date('Y-m-d', strtotime("-{$months} month{$plural}", $time));
				} else {
					$date['info'] .= "{$matches[2]} jours";
					$date['date'] = date('Y-m-d', strtotime("-{$matches[2]} days", $time));
				}
				break;
			case 'Y':
				$date['info'] .= "{$matches[2]} année{$plural} calendaire{$plural}";
				$step += $matches[2] - 1;
				if ($step) {
					$date['date'] = date('Y', strtotime("-$step years", $time));
				} else {
					$date['date'] = date('Y', $time);
				}
				break;
		}
		switch (strlen($date['date'])) {
			case 10:
				$date['fulldate'] = $date['date'];
				break;
			case 7:
				$date['fulldate'] = $date['date'] . (($matches[1] === 'P') ? '-30' : '-01');
				break;
			case 4:
				$date['fulldate'] = $date['date'] . (($matches[1] === 'P') ? '-12-31' : '-01-01');
				break;
		}
		return $date;
	}

	/**
	 * Cleans the output of extractTitleData().
	 *
	 * @param array $attr
	 * @return array
	 */
	public static function cleanTitleData($attr)
	{
		foreach ($attr as $k => $v) {
			if ($k != 'prefixe') {
				$attr[$k] = trim($v);
			}
		}

		// detect a prefix in the title
		if (empty($attr['prefixe'])) {
			$prefixes = array_filter(explode('/', \Config::read('import.prefixes')));
			foreach ($prefixes as $p) {
				if (strpos($attr['titre'], $p) === 0 || strpos($attr['titre'], ucfirst($p)) === 0) {
					$attr['prefixe'] = substr($attr['titre'], 0, strlen($p));
					$attr['titre'] = substr($attr['titre'], strlen($p));
					break;
				}
			}
		}

		if (!empty($attr['issn']) && preg_match('/\s*en cours\s*/i', $attr['issn'])) {
			unset($attr['issn']);
		}
		if (!empty($attr['issn']) && strpos($attr['issn'], ' ') !== false) {
			$attr['issn'] = trim(strtok($attr['issn'], "  \t"));
		}
		if (!empty($attr['issne']) && strpos($attr['issne'], ' ') !== false) {
			$attr['issne'] = trim(strtok($attr['issne'], "  \t"));
		}
		return array_filter($attr);
	}

	/**
	 * Cleans the output of extractServicesData().
	 *
	 * @param array $services
	 * @return array
	 */
	public static function cleanServicesData($services)
	{
		$filtered = [];
		foreach ($services as $s) {
			if ($s['dateBarrDebut'] <= $s['dateBarrFin']) {
				$filtered[] = $s;
			}
		}
		return $filtered;
	}

	public static function toNum($text, $fail = false)
	{
		if ($text === null || $text === '') {
			return null;
		}
		if (ctype_digit($text)) {
			return (int) $text;
		}
		if (preg_match('/^\d/', $text)) {
			return (int) $text;
		}
		if (preg_match('/^[IVXLCDM]+$/', $text)) {
			return self::arabizeRomanNumber($text);
		}
		if ($fail) {
			throw new \Exception("Could not recognize the format of this number: '$text'.");
		}
		return null;
	}

	/**
	 * Returns a VolNo field built from $vol and $no.
	 *
	 * @param string $vol
	 * @param string $no
	 * @param string $volType (opt)
	 * @return string
	 */
	public static function buildVolNum($vol, $no, $volType='')
	{
		$v = 'Vol. ';
		if ($volType) {
			if ($volType[0] === 'T') {
				$v = 'Tome ';
			} elseif ($volType[0] === 'A') {
				$v = 'Année ';
			}
		}
		if ($vol) {
			$vol = $v . $vol;
		} else {
			$vol = '';
		}
		if ($no) {
			$no = "no " . $no;
		} else {
			$no = '';
		}
		if ($vol && $no) {
			return $vol . ', ' . $no;
		}
		return $vol . $no;
	}

	/**
	 * Convert a string to an ISO date, as much as possible.
	 *
	 * @param string $strDate
	 * @return string
	 */
	public static function toIsoDate($strDate)
	{
		$m = [];
		if (preg_match('/^\d{4}-\d\d-\d\d$|^\d{4}$/', $strDate)) {
			// ISO or year
			return $strDate;
		}
		if (preg_match('#^(\d{4})\W?(\d\d)\W?(\d\d)$#', $strDate, $m)) {
			// ISO, except for separators
			return sprintf("%d-%02d-%02d", $m[1], $m[2], $m[3]);
		}
		if (preg_match('#^(\d{4})[-/](\d\d)$#', $strDate, $m)) {
			// incomplete ISO
			return $m[1] . "-" . $m[2];
		}
		if (preg_match('#^(\d\d)\W(\d\d)\W(\d{4})$#', $strDate, $m)) {
			// european or US, can't guess
			return $m[3];
		}
		return null;
	}

	protected static function arabizeRomanNumber($text)
	{
		$romans = [
			'M' => [
				'M' => 1000,
			],
			'C' => [
				'CM' => 900,
				'CD' => 400,
				'C' => 100,
			],
			'D' => [
				'D' => 500,
			],
			'L' => [
				'L' => 50,
			],
			'X' => [
				'XC' => 90,
				'XL' => 40,
				'X' => 10,
			],
			'V' => [
				'V' => 5,
			],
			'I' => [
				'IX' => 9,
				'IV' => 4,
				'I' => 1,
			],
		];
		$result = 0;
		$pos = 0;
		$oldPos = -1;
		$length = strlen($text);
		while ($pos < $length && $pos > $oldPos) {
			$oldPos = $pos;
			foreach ($romans[$text[$pos]] as $key => $value) {
				while (strpos($text, $key, $pos) === $pos) {
					$result += $value;
					$pos += strlen($key);
					if ($pos === $length) {
						break;
					}
				}
			}
		}
		if ($pos !== $length) {
			return null;
		}
		return $result;
	}
}
