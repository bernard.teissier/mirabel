<?php

namespace import;

require_once __DIR__ . '/Reader.php';

/**
 * class import\Reader
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class ReaderExcel extends Reader
{
	protected $expectedColumns = [];

	/** @var \PHPExcel */
	protected $excel;

	/** @var \PHPExcel_Worksheet */
	protected $worksheet;

	protected $columnsNum = 0;

	/**
	 * Constructor.
	 */
	public function __construct(array $expectedColumns)
	{
		$this->expectedColumns = $expectedColumns;
		if (!\PHPExcel_Settings::setLocale()) {
			$this->log->addGlobal('warning', 'Locale "fr" pour Excel', "Could not set Excel locale to 'fr'");
		}
	}

	public function __set($name, $value)
	{
		die("Undefined object attr: $name = $value");
	}

	/**
	 * Reads the whole input file.
	 *
	 * @param string $sourceFile Filename to read.
	 * @return bool Success?
	 */
	public function readUrl($sourceFile)
	{
		if (!file_exists($sourceFile) || !is_readable($sourceFile)) {
			$this->log->addGlobal('error', 'url', "Fichier introuvable ou sans droit d'accès");
		}

		$inputFileType = \PHPExcel_IOFactory::identify($sourceFile);
		$reader = \PHPExcel_IOFactory::createReader($inputFileType);
		//$reader->setReadDataOnly(true);
		$this->excel = $reader->load($sourceFile);
		$this->worksheet = $this->excel->getActiveSheet();
		if (!$this->worksheet) {
			$this->log->addGlobal('error', 'file', "Pas de feuille active dans le tableur.");
			return false;
		}

		$realHeaderList = $this->worksheet->rangeToArray('A1:Z1');
		$realHeader = array_filter($realHeaderList[0]);
		$this->columnsNum = count($realHeader);
		$this->header = $realHeader;

		$this->colMap = $this->parseHeader($realHeader);
		if (!$this->colMap) {
			$this->log->addGlobal(
				'error',
				'file',
				"Erreur en lisant l'entête du fichier."
			);
			return false;
		}
		return true;
	}

	/**
	 * Returns an iterator (might be an array) on the lines to import.
	 *
	 * @return Iterable Array of splitted rows, or false if there was an error.
	 */
	public function readLines(): iterable
	{
		return $this->worksheet->getRowIterator(2); // !! Excel starts at line 1 !!
	}

	/**
	 * Returns the cell's content on the current line in the column selected by its name.
	 *
	 * @param \PHPExcel_Worksheet_Row|array $line The current line from the input file.
	 * @param string $name     The column name.
	 * @param string $default  (opt) Default value.
	 * @return string
	 */
	public function getCol($line, $name, $default='')
	{
		if (!isset($this->colMap)) {
			throw new \Exception("uninitialized");
		}
		if (!isset($this->colMap[$name])) {
			//echo "colMap:"; print_r($this->colMap);
			return $default;
		}
		if (is_array($line)) {
			return parent::getCol($line, $name, $default);
		}
		$val = trim(
			$this->worksheet->getCellByColumnAndRow(
				$line->getRowIndex(),
				$this->colMap[$name]
			)
		);
		return (null === $val || $val === '' ? $default : $val);
	}

	/**
	 * Splits a line into columns.
	 *
	 * @param \PHPExcel_Worksheet_Row $line
	 * @return array
	 */
	public function splitRawLine(string $line): array
	{
		$cells = array_fill(0, $this->columnsNum, '');
		/* @var $cell \PHPExcel_Cell */
		foreach ($line->getCellIterator() as $cell) {
			$col = ord($cell->getColumn()) - ord('A');
			$cells[$col] = $cell->getValue();
		}
		return array_map('trim', $cells);
	}

	/**
	 * Return an assoc array { "Expected col name" => pos }.
	 *
	 * @param array $realColumns
	 * @return array
	 */
	protected function parseHeader($realColumns)
	{
		if (empty($this->expectedColumns)) {
			return array_flip($realColumns);
		}

		$expectedColumns = $this->flattenColumns($this->expectedColumns);
		$unknownColumns = array_diff($realColumns, $expectedColumns);
		if ($unknownColumns) {
			$this->log->addGlobal(
				'warning',
				'separator',
				"L'entête reçu contient des colonnes inconnues : " . join(', ', $unknownColumns)
			);
		}
		return array_flip(array_intersect($realColumns, $expectedColumns));
	}
}
