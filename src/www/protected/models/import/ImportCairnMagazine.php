<?php

require_once __DIR__ . '/ImportCairn.php';
require_once __DIR__ . '/ReaderCsv.php';

/**
 * Import from Cairn Magazine.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class ImportCairnMagazine extends ImportCairn
{
	protected $collectionId;

	/**
	 * Constructor.
	 */
	public function __construct($parameters=[])
	{
		$this->name = 'Cairn';
		if (empty($parameters['url'])) {
			$parameters['url'] = 'http://dedi.cairn.info/NL/magazines_cairn_csv_2015.php';
		}
		foreach (['url', 'verbose', 'simulation'] as $k) {
			if (isset($parameters[$k])) {
				$this->{$k} = $parameters[$k];
			}
		}
		$this->header = explode(
			';',
			"MAGAZINE;EDITEUR;ISSN;ISSN VERSION EN LIGNE;PERIODICITE;URL DU MAGAZINE;PREMIER NUMERO DISPO;DERNIER NUMERO DISPO"
		);
		$this->reader = new import\ReaderCsv(';', $this->header, $this->encoding);
		$this->init();
	}

	/**
	 * Returns a integer that is unique to this import type.
	 *
	 * @return int
	 */
	public function getId()
	{
		return ImportType::getSourceId('Cairn Magazine');
	}

	/**
	 * Returns a list of Collection objects.
	 *
	 * @param array $line Splitted CSV line.
	 * @return array
	 */
	public function extractCollections($line)
	{
		return [$this->collectionId];
	}

	/**
	 * Returns a list of Service objects.
	 *
	 * @param array $line Splitted CSV line.
	 * @return array
	 */
	public function extractServicesData($line)
	{
		$begin = $this->parseVolNum($this->getCol($line, 'PREMIER NUMERO DISPO'));
		$end = $this->parseVolNum($this->getCol($line, 'DERNIER NUMERO DISPO'));
		return [
			[
				'type' => 'Intégral',
				'acces' => 'Restreint',
				'lacunaire' => 0,
				'selection' => 0,
				'url' => $this->getCol($line, 'URL DU MAGAZINE'),
				'volDebut' => $begin['vol'],
				'noDebut' => $begin['num'],
				'numeroDebut' => import\Normalize::buildVolNum($begin['vol'], $begin['num']),
				'volFin' => $end['vol'],
				'noFin' => $end['num'],
				'numeroFin' => import\Normalize::buildVolNum($end['vol'], $end['num']),
				'dateBarrDebut' => $begin['date'],
				'dateBarrFin' => $end['date'],
				'dateBarrInfo' => '',
			],
		];
	}

	public function extractTitleData($line)
	{
		$result = parent::extractTitleData($line);

		if (preg_match('#https?://www\.cairn\.info/(.+)\.htm#', $this->getCol($line, 'URL DU MAGAZINE'), $m)) {
			$result['idInterne'] = $m[1];
		}

		return $result;
	}

	/**
	 * Initializes the list of collections.
	 *
	 * @throws Exception
	 */
	protected function initCollections()
	{
		$this->collectionId = Collection::model()
			->findByAttributes(['ressourceId' => $this->ressource->id, 'nom' => 'Cairn Magazine'])
			->id;
		if (!$this->collectionId) {
			throw new Exception("Erreur : la collection « Cairn Magazine » est introuvable.");
		}
	}
}
