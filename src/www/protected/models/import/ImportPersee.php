<?php

require_once __DIR__ . '/Import.php';
require_once __DIR__ . '/ReaderCsv.php';

/**
 * Import from Persee.fr
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class ImportPersee extends Import
{
	protected $separator = ';';

	protected $encoding = 'Windows-1252';

	/**
	 * Constructor.
	 */
	public function __construct($parameters=[])
	{
		if (empty($parameters['url'])) {
			$parameters['url'] = 'http://tools.persee.fr/Mirabel/revues_persee.csv';
		}
		foreach (['url', 'simulation'] as $k) {
			if (isset($parameters[$k])) {
				$this->{$k} = $parameters[$k];
			}
		}
		$this->name = 'Persée';
		$this->header = explode(
			';',
			"REVUE;REVUE_KEY;EDITEUR ACTUEL;ISSNs PAPIER;ISSN NUM;URL PERSEE;COUVERTURE PERSEE;"
			. "PREMIER NUMERO;DERNIER NUMERO;RSS REVUE;RESUMES"
		);
		$this->reader = new import\ReaderCsv(';', $this->header, $this->encoding);
		$this->init();
	}

	/**
	 * Returns a integer that is unique to this import type.
	 *
	 * @return int
	 */
	public function getId()
	{
		return ImportType::getSourceId('Persée');
	}

	/**
	 * Returns the title attributes: titre, issn... and some extra: key, issne...
	 *
	 * @param array $line Splitted CSV line.
	 * @return array Assoc array with keys: "titre", "issn", "issne"...
	 */
	public function extractTitleData($line)
	{
		return import\Normalize::cleanTitleData(
			[
				'titre' => $line[0],
				'editeur' => $line[2],
				'issn' => $line[3],
				'issne' => $line[4],
				'idInterne' => $line[1],
				'urlLocal' => $line[5],
			]
		);
	}

	/**
	 * Returns a list of Service objects.
	 *
	 * @param array $line Splitted CSV line.
	 * @return array
	 */
	public function extractServicesData($line)
	{
		$range = explode('-', $line[6]);
		$begin = self::parseVolNum($line[7], $range[0]);
		$end = self::parseVolNum($line[8], $range[1]);
		return [
			[
				'type' => 'Intégral',
				'acces' => 'Libre',
				'lacunaire' => 0,
				'selection' => 0,
				'url' => $line[5],
				'alerteRssUrl' => $line[9],
				'volDebut' => import\Normalize::toNum($begin['vol']),
				'noDebut' => import\Normalize::toNum($begin['num']),
				'numeroDebut' => import\Normalize::buildVolNum($begin['vol'], $begin['num']),
				'volFin' => import\Normalize::toNum($end['vol']),
				'noFin' => import\Normalize::toNum($end['num']),
				'numeroFin' => import\Normalize::buildVolNum($end['vol'], $end['num']),
				'dateBarrDebut' => $begin['date'],
				'dateBarrFin' => $end['date'],
				'dateBarrInfo' => '',
			],
		];
	}

	public function extractCollections($line)
	{
		return [];
	}

	/**
	 * Parse a field and extract: date, vol num.
	 *
	 * @param string $text
	 * @param string $year
	 * @return array assoc array with keys: vol, num, date (YYYY-MM or YYYY)
	 */
	public static function parseVolNum($text, $year = ''): array
	{
		$numero = [];
		$mois_str2num = [
			'janvier' => 1,
			'février' => 2,
			'fevrier' => 2,
			'mars' => 3,
			'avril' => 4,
			'mai' => 5,
			'juin' => 6,
			'juillet' => 7,
			'août' => 8,
			'aout' => 8,
			'septembre' => 9,
			'octobre' => 10,
			'novembre' => 11,
			'décembre' => 12,
			'decembre' => 12,
		];
		$mois_regexp = '(' . join('|', array_keys($mois_str2num)) . ')';
		$regexp = '/(?:Vol\. ?|Volume |Tome |t\. ?)(\d+), (?:n° ?|n\. ?|n |numéro )(\d+), ' . $mois_regexp . ' (\d{4})/i';
		if (preg_match($regexp, $text, $match)) {
			$numero = [
				'vol' => $match[1],
				'num' => $match[2],
				'date' => sprintf('%d-%02d', $match[4], $mois_str2num[strtolower($match[3])]),
			];
		} else {
			$numero = ['vol' => '', 'num' => '', 'date' => ''];
			if (preg_match("/$mois_regexp ([12]\\d{3})/i", $text, $match)) {
				$numero['date'] = sprintf('%d-%02d', $match[2], $mois_str2num[strtolower($match[1])]);
			} elseif (preg_match('/\b(\d\d?)[\/-]([12]\d{3})\b/i', $text, $match)) {
				$numero['num'] = $match[1];
				$numero['date'] = $match[2];
			} elseif (preg_match('/\b([12]\d{3})\b/i', $text, $match)
				&& $match[1] > 1800 && $match[1] <= date('Y')) {
				$numero['date'] = sprintf('%d', $match[1]);
			}
			if (empty($numero['date']) || $numero['date'] < $year) {
				// on utilise le champ "Couverture à la place"
				$numero['date'] = (string) $year;
			}
			// on complète la date trouvée avec les infos de volume et n°
			if (preg_match('/\b(?:n° ?|n\. ?|n |numéro )([\d-]+)/i', $text, $match)) {
				$numero['num'] = $match[1];
			}
			if (preg_match('/(?:Vol. ?|Volume |Tome |t\. ?)(\d+(?:-\d+)?)/i', $text, $match)) {
				$numero['vol'] = $match[1];
			}
			if (!$numero['vol'] && preg_match('/\b(\d+)[e°] année\b/', $text, $match)) {
				$numero['vol'] = $match[1];
			}
		}
		return $numero;
	}
}
