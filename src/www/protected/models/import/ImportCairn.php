<?php

require_once __DIR__ . '/Import.php';
require_once __DIR__ . '/ReaderCsv.php';

/**
 * Import from Cairn.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class ImportCairn extends Import
{
	protected $encoding = 'UTF-8';

	/**
	 * Constructor.
	 */
	public function __construct($parameters=[])
	{
		$this->name = 'Cairn';
		if (empty($parameters['url'])) {
			$parameters['url'] = 'http://dedi.cairn.info/NL/revues_cairn_csv_2015.php';
		}
		foreach (['url', 'verbose', 'simulation'] as $k) {
			if (isset($parameters[$k])) {
				$this->{$k} = $parameters[$k];
			}
		}
		$this->header = explode(
			';',
			"REVUE;EDITEUR;ISSN;ISSN VERSION EN LIGNE;PERIODICITE;URL DE LA REVUE;PREMIER NUMERO DISPO;DERNIER NUMERO DISPO;"
			. "BARRIERE MOBILE;BOUQUET GENERAL;BOUQUET PSYCHOLOGIE;BOUQUET ECO-SOC-POL;"
			. "BOUQUET SCIENCES DE L'EDUCATION;BOUQUET TRAVAIL SOCIAL;BOUQUET HUMANITES;BOUQUET SANTE PUBLIQUE;BOUQUET ECO-GESTION"
			. ";BOUQUET HUMANITIES AND SOCIAL SCIENCE;BOUQUET DOCUMENTATION HOSPITALIERE"
			. ";BOUQUET GEOGRAPHIE ENVIRONNEMENT;BOUQUET AFRIQUE;BOUQUET AFFAIRES PUBLIQUES"
		);
		$this->reader = new import\ReaderCsv(';', $this->header, $this->encoding);
		$this->init();
	}

	/**
	 * Returns a integer that is unique to this import type.
	 *
	 * @return int
	 */
	public function getId()
	{
		return ImportType::getSourceId('Cairn');
	}

	/**
	 * Returns the title attributes: titre, issn... and some extra: key, issne...
	 *
	 * @param array $line Splitted CSV line.
	 * @return array Assoc array with keys: "titre", "issn", "issne"...
	 */
	public function extractTitleData($line)
	{
		if (preg_match('#https?://www\.cairn\.info/(.+)\.htm#', $this->getCol($line, 'URL DE LA REVUE'), $m)) {
			$idInterne = $m[1];
		} else {
			$idInterne = null;
		}
		return import\Normalize::cleanTitleData(
			[
				'titre' => $this->getCol($line, $this->header[0]),
				'editeur' => $this->getCol($line, 'EDITEUR'),
				'issn' => $this->getCol($line, 'ISSN'),
				'issne' => $this->getCol($line, 'ISSN VERSION EN LIGNE'),
				'idInterne' => $idInterne,
				'urlLocal' => $this->getCol($line, 'URL DE LA REVUE'),
			]
		);
	}

	/**
	 * Returns a list of Collection objects.
	 *
	 * @param array $line Splitted CSV line.
	 * @return array
	 */
	public function extractCollections($line)
	{
		$collections = [];
		foreach ($this->collections as $colnum => $collectionId) {
			if (isset($line[$colnum]) && $line[$colnum] === 'X') {
				$collections[] = $collectionId;
			}
		}
		return $collections;
	}

	/**
	 * Returns a list of Service objects.
	 *
	 * @param array $line Splitted CSV line.
	 * @return array
	 */
	public function extractServicesData($line)
	{
		$begin = $this->parseVolNum($this->getCol($line, 'PREMIER NUMERO DISPO'));
		$end = $this->parseVolNum($this->getCol($line, 'DERNIER NUMERO DISPO'));
		$barrMob = strtolower($this->getCol($line, 'BARRIERE MOBILE'));
		if ($barrMob === 'accès libre intégral') {
			$services = [
				[
					'type' => 'Intégral',
					'acces' => 'Libre',
					'lacunaire' => 0,
					'selection' => 0,
					'derNumUrl' => '',
					'url' => $this->getCol($line, 'URL DE LA REVUE'),
					'volDebut' => import\Normalize::toNum($begin['vol']),
					'noDebut' => import\Normalize::toNum($begin['num']),
					'numeroDebut' => import\Normalize::buildVolNum($begin['vol'], $begin['num'], $begin['type']),
					'volFin' => import\Normalize::toNum($end['vol']),
					'noFin' => import\Normalize::toNum($end['num']),
					'numeroFin' => import\Normalize::buildVolNum($end['vol'], $end['num'], $end['type']),
					'dateBarrDebut' => $begin['date'],
					'dateBarrFin' => $end['date'],
					'dateBarrInfo' => '',
				],
			];
		} elseif (preg_match('/^\s*(\d+) +ans?/', $barrMob, $m)) {
			$date = (string) (date('Y', $this->time) - $m[1]);
			$dateInfo = "Barrière mobile de $m[1] an" . ($m[1] > 1 ? 's' : '');
			$services = [
				[
					'type' => 'Intégral',
					'acces' => 'Libre',
					'lacunaire' => 0,
					'selection' => 0,
					'derNumUrl' => '',
					'url' => $this->getCol($line, 'URL DE LA REVUE'),
					'volDebut' => import\Normalize::toNum($begin['vol']),
					'noDebut' => import\Normalize::toNum($begin['num']),
					'numeroDebut' => import\Normalize::buildVolNum($begin['vol'], $begin['num'], $begin['type']),
					'volFin' => null,
					'noFin' => null,
					'numeroFin' => '',
					'dateBarrDebut' => $begin['date'],
					'dateBarrFin' => min([$date - 1, $end['date']]),
					'dateBarrInfo' => $dateInfo,
				],
				[
					'type' => 'Intégral',
					'acces' => 'Restreint',
					'lacunaire' => 0,
					'selection' => 0,
					'derNumUrl' => '',
					'url' => $this->getCol($line, 'URL DE LA REVUE'),
					'volDebut' => null,
					'noDebut' => null,
					'numeroDebut' => '',
					'volFin' => import\Normalize::toNum($end['vol']),
					'noFin' => import\Normalize::toNum($end['num']),
					'numeroFin' => import\Normalize::buildVolNum($end['vol'], $end['num'], $end['type']),
					'dateBarrDebut' => max([$date, $begin['date']]),
					'dateBarrFin' => $end['date'],
					'dateBarrInfo' => $dateInfo,
				],
			];
		} elseif ($barrMob === 'non') {
			$services = [
				[
					'type' => 'Intégral',
					'acces' => 'Restreint',
					'lacunaire' => 0,
					'selection' => 0,
					'derNumUrl' => '',
					'url' => $this->getCol($line, 'URL DE LA REVUE'),
					'volDebut' => import\Normalize::toNum($begin['vol']),
					'noDebut' => import\Normalize::toNum($begin['num']),
					'numeroDebut' => import\Normalize::buildVolNum($begin['vol'], $begin['num'], $begin['type']),
					'volFin' => import\Normalize::toNum($end['vol']),
					'noFin' => import\Normalize::toNum($end['num']),
					'numeroFin' => import\Normalize::buildVolNum($end['vol'], $end['num'], $end['type']),
					'dateBarrDebut' => $begin['date'],
					'dateBarrFin' => $end['date'],
					'dateBarrInfo' => '',
				],
			];
		} else {
			$this->log->addLocal('warning', "Format de date barrière incompris : " . $this->getCol($line, 'BARRIERE MOBILE'));
			$this->errors[] = "Format de date barrière incompris : "
				. $this->getCol($line, 'BARRIERE MOBILE');
			return [];
		}
		return import\Normalize::cleanServicesData($services);
	}

	/**
	 * Initializes the list of collections.
	 *
	 * @throws Exception
	 */
	protected function initCollections()
	{
		$collections = Tools::sqlToPairs(
			"SELECT identifiant, id FROM Collection WHERE identifiant <> '' AND ressourceId = " . $this->ressource->id
		);
		foreach ($collections as $colname => $cid) {
			$colnum = $this->reader->getHeaderRank($colname);
			if (!$colnum) {
				$this->log->addGlobal(
					"warning",
					"Erreur, collection supprimée",
					"Attention, la collection « {$colname} » de la base de données est introuvable dans le CSV."
				);
			} else {
				$this->collections[$colnum] = $cid;
			}
		}
		foreach (array_keys($this->reader->colMap) as $col) {
			if (strncmp($col, "BOUQUET ", 8) === 0 && !isset($collections[$col])) {
				$this->log->addGlobal(
					"warning",
					"Erreur, collection inconnue",
					"Attention, la collection « {$col} » du CSV est introuvable dans la base de données."
				);
			}
		}
	}

	/**
	 * Parse a Cairn field and extract: date, vol num.
	 *
	 * @return Array  assoc array: vol, num, date (YYYY-MM or YYYY)
	 */
	protected function parseVolNum($text)
	{
		$numero = [];
		$text = trim($text);
		if (preg_match('/^(\d{4})\/(\d+),[ \s]+(Vol\.?|Volume|Tome)[ \s]+(\d+),[ \s]+(?:n°|numéro)[ \s]*(\d+)/i', $text, $m)) {
			$numero = [
				'type'   => $m[3],
				'vol'   => $m[4],
				'num'   => $m[5],
			];
		} elseif (preg_match('/^(\d{4})\/(\d+),[ \s]+(?:n°|numéro)[ \s]*(\d+)/i', $text, $m)) {
			$numero = [
				'type'   => '',
				'vol'   => null,
				'num'   => $m[3],
			];
		} elseif (preg_match('/^(\d{4})\/(\d+),[ \s]+(Vol\.?|Volume|Tome)[ \s]+(\d+|[IVXLCDM]+)/', $text, $m)) {
			$numero = [
				'type'   => $m[3],
				'vol'   => $m[4],
				'num'   => null,
			];
		} elseif (preg_match('/^(\d{4})[^\d]/', $text, $m)) {
			$m[2] = 0;
			$numero = [
				'type'   => '',
				'vol'   => null,
				'num'   => null,
			];
		} else {
			$this->log->addLocal('error', "Format du numéro incompréhensible, champ='$text'");
			return false;
		}
		$numero['date'] = $m[1];
		return $numero;
	}
}
