<?php

namespace import;

/**
 * class import\Log
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class Log
{
	public const ACTION_NOUVEAUTITRE = 'nouveaux titres';

	public const ACTION_REVUE = 'revues';

	public const ACTION_MODIFICATION = 'modifications';

	public const ACTION_ERREUR = 'erreurs';

	public const ACTION_CREATION = "création d'accès";

	public $verbose;

	protected $local;

	protected $global;

	protected $numActions = [
		self::ACTION_REVUE => 0,
		self::ACTION_NOUVEAUTITRE => 0,
		self::ACTION_MODIFICATION => 0,
		self::ACTION_ERREUR => 0,
		self::ACTION_CREATION => 0,
	];

	protected $levels = [
		'debug' => 4,
		'info' => 3,
		'warning' => 2,
		'error' => 1,
	];

	/**
	 * Constructor.
	 *
	 * @param int $verbose Verbosity level.
	 */
	public function __construct($verbose)
	{
		$this->verbose = $verbose;
		$this->initGlobal();
		$this->initLocal();
	}

	/**
	 * Init the local log.
	 */
	public function initLocal()
	{
		$this->local = [];
	}

	/**
	 * Init the global log.
	 */
	public function initGlobal()
	{
		$this->global = [];
		$this->numActions = [
			self::ACTION_REVUE => 0,
			self::ACTION_NOUVEAUTITRE => 0,
			self::ACTION_MODIFICATION => 0,
			self::ACTION_ERREUR => 0,
			self::ACTION_CREATION => 0,
		];
	}

	/**
	 * Are there any changes? Or just journals with no updates?
	 *
	 * @return bool
	 */
	public function isEmpty()
	{
		return count(array_filter($this->numActions)) <= 1;
	}

	/**
	 * Add a local log entry.
	 *
	 * @param string $level
	 * @param string $msg
	 */
	public function addLocal($level, $msg)
	{
		if (isset($this->levels[$level])) {
			$l = $this->levels[$level];
		} else {
			$l = $this->levels['info'];
		}
		$this->local[] = [$l, $msg];
	}

	/**
	 * Add a global log entry.
	 *
	 * @param string $level
	 * @param string $title
	 * @param string $msg
	 * @param string $url
	 */
	public function addGlobal($level, $title, $msg, $url='')
	{
		$this->global[] = ['level' => $level, 'title' => $title, 'msg' => $msg, 'url' => $url];
	}

	/**
	 * Merge the local logs into a global log entry, before deleting them.
	 *
	 * @param string $title Title of the new global entry
	 * @param string $url URL of the new global entry
	 */
	public function flushLocal($title, $url)
	{
		// merge logs of this title
		$msgs = [];
		$level = 4;
		foreach ($this->local as $l) {
			if ($l[0] <= (2 + $this->verbose)) { // verbosity=1 <=> up to 'info'=3
				$msgs[] = self::getLevelMark($l[0]) . " " . $l[1];
				$level = $l[0] < $level ? $l[0] : $level;
			}
		}
		$level = array_search($level, $this->levels);
		if ($msgs) {
			$this->addGlobal(
				$level, // max level of the local levels
				$title,
				join("\n\t", $msgs),
				$url
			);
		}
		$this->initLocal();
	}

	public function incAction($name)
	{
		if (!isset($this->numActions[$name])) {
			throw new \Exception("Unknown action in the logs");
		}
		$this->numActions[$name]++;
	}

	public function getLocal()
	{
		return $this->local;
	}

	/**
	 * Returns the list of logs.
	 *
	 * @param string $as (opt, defaults to "array") "array", "string" or "html" accepted.
	 * @return array|string of logs
	 */
	public function format($as = "array")
	{
		$summary = vsprintf(
			"%7d revues rencontrées, dont %d nouveautés, %d modifications, %d erreurs, et %d créations d'un premier accès.\n\n",
			array_values($this->numActions)
		);
		switch ($as) {
			case 'array':
				return $this->filter();
			case 'string':
				$baseUrl = '';
				if (isset(\Yii::app()->params['baseUrl'])) {
					$baseUrl = \Yii::app()->params['baseUrl'];
				}
				$output = $summary;
				$rowCounter = 1;
				foreach ($this->filter() as $log) {
					$output .= sprintf(
						"%3s %3d. %s : \n\t%s\n",
						self::getLevelMark($log['level']),
						$rowCounter,
						strip_tags($log['title']),
						trim($log['msg'], " \t\n")
					);
					if (!empty($log['url'])) {
						if ($log['url'][0] === '/') {
							$url = $baseUrl . $log['url'];
						} else {
							$url = $log['url'];
						}
						$output .= "         " . $url . "\n";
					}
					$rowCounter++;
				}
				$output .= "\n" . $summary;
				break;
			default:
				$summary = "<div><b>Bilan</b><p>$summary</p></div>";
				$output = "<div>$summary<div><ol>";
				foreach ($this->filter() as $log) {
					$title = \CHtml::encode($log['title']);
					if (!empty($log['url']) && strncmp($log['url'], 'http', 4) === 0) {
						$title = \CHtml::link($title, $log['url']);
					}
					$output .= sprintf(
						"<li class=\"row-%s\"><strong>%s</strong> : <div>%s</div></li>\n",
						$log['level'],
						$title,
						nl2br(\CHtml::encode($log['msg']))
					);
				}
				$output .= "</ol></div>$summary</div>";
		}
		return $output;
	}

	protected static function getLevelMark($level)
	{
		switch ($level) {
			case 'debug':
			case 4:
				return '-';
			case 'info':
			case 3:
				return '*';
			case 'warning':
			case 2:
				return '**';
			default:
				return '***';
		}
	}

	/**
	 * Filters the global logs according to the obect verbosity.
	 *
	 * @return array
	 */
	protected function filter()
	{
		$logs = [];
		foreach ($this->global as $log) {
			if ($this->verbose > 1 || ($this->verbose > 0 && $log['level'] !== 'debug') || $log['level'] !== 'info') {
				$logs[] = $log;
			}
		}
		return $logs;
	}
}
