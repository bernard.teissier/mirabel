<?php

class ImportType
{
	/**
	 * @var array import number => printed text
	 */
	private static $ids = [
		'Intervention manuelle',
		'Cairn',
		'Cairn Magazine',
		'Erudit',
		'Persée',
		'Revues.org',
		'Import Standard',
		'KBART',
		'SUDOC',
		'ROAD',
		'DOAJ', // 10
		'Héloïse',
		'URL couverture',
		'HAL',
		'Cairn international',
		'Sherpa',
		'Wikidata', // 16
		'Erih+',
		'issn.org',
		'idref.fr',
	];

	/**
	 * @var array name of source (ImportX for access import, Sourcelien.nomcourt for link import, or special value) => import number
	 */
	private static $naming = [
		'Intervention manuelle' => 0,
		'Cairn' => 1,
		'Cairn Magazine' => 2,
		'Erudit' => 3,
		'Persée' => 4,
		'Revues.org' => 5,
		'Import Standard' => 6,
		'KBART' => 7,
		'SUDOC' => 8,
		'road' => 9, // SourceLien
		'doaj' => 10, // SourceLien
		'heloise' => 11, // SourceLien
		'URL couverture' => 12,
		'hal' => 13, // SourceLien
		'cairnint' => 14, // SourceLien
		'romeo' => 15, // SourceLien
		'erihplus' => 17,
		'issn.org' => 18,
		'idref.fr' => 19,
	];

	public static function getSourceName(int $id): string
	{
		return (self::$ids[$id] ?? 'Type inconnu');
	}

	public static function getSourceId(string $name): int
	{
		if (strncmp('wikipedia-', $name, 10) === 0) {
			return 16;
		}
		return (self::$naming[$name] ?? 0);
	}

	public static function getImportList(): array
	{
		$a = self::$ids;
		asort($a);
		return $a;
	}
}
