<?php

namespace import;

/**
 * class import\Reader
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class ReaderCsv extends Reader
{
	protected $separator;

	protected $expectedColumns = [];

	protected $encoding = 'UTF-8';

	protected $lines;

	/**
	 * Constructor.
	 */
	public function __construct($separator, $expectedColumns, $encoding = 'UTF-8')
	{
		if ($separator === '\t' || strncasecmp($separator, 'tab', 3) === 0) {
			$separator = "\t";
		}

		$this->separator = $separator;
		$this->expectedColumns = $expectedColumns;
		$this->encoding = $encoding;
	}

	public function __set($name, $value)
	{
		die("Undefined object attr: $name = $value");
	}

	/**
	 * Reads the whole input file.
	 *
	 * @param string $url
	 * @return bool
	 */
	public function readUrl($url)
	{
		$csv = @file_get_contents($url);
		if (!$csv) {
			$this->log->addGlobal('error', '*INIT*', "Erreur d'accès à l'URL d'import : " . $url);
			return false;
		}
		// convert EOL
		$csv = str_replace("\x0D\x0A", "\n", $csv); // Windows
		$csv = str_replace("\x0D", "\n", $csv);     // Mac (must be after Windows)

		if (!mb_check_encoding($csv, $this->encoding)) {
			$this->log->addGlobal(
				'error',
				'csvFile',
				"Le fichier n'est pas encodé en {$this->encoding} comme attendu."
			);
			return false;
		}
		if ($this->encoding !== 'UTF-8') {
			$csv = mb_convert_encoding($csv, 'UTF-8', $this->encoding);
		}

		$lines = explode("\n", $csv);
		$this->colMap = $this->parseCsvHeader($lines[0]);
		if (!$this->colMap) {
			$this->log->addGlobal(
				'error',
				'separator',
				"Erreur en lisant l'entête du fichier."
			);
			return false;
		}
		array_shift($lines);
		$this->lines = $lines;
		return true;
	}

	/**
	 * Returns an array on the lines to import.
	 *
	 * @return Iterable Array of splitted rows, or false if there was an error.
	 */
	public function readLines(): iterable
	{
		return $this->lines;
	}

	public function parseCsvHeader($headerLine)
	{
		$expectedColumns = $this->flattenColumns($this->expectedColumns);
		if (is_array($headerLine)) {
			$fileHeader = $headerLine;
		} else {
			$line = trim($headerLine, $this->separator);
			if ($this->encoding === 'UTF-8') {
				if (!mb_detect_encoding($line, 'UTF-8')) {
					$this->log->addGlobal(
						'error',
						'separator',
						"L'encodage du fichier n'est pas de l'UTF-8 valide."
					);
					return null;
				}
			}
			$fileHeader = str_getcsv($line, $this->separator);
		}
		if ($this->encoding === 'UTF-8') {
			/* Sometimes an UTF-8 CSV file begins with a BOM */
			$fileHeader[0] = trim($fileHeader[0], '﻿"');
		}
		$fileHeader = array_filter(
			array_map(
				function ($a) {
					return trim($a, '"');
				},
				$fileHeader
			)
		);
		if ($fileHeader[count($fileHeader)-1] == "") {
			array_pop($fileHeader);
		}
		// separator
		if (count($fileHeader) === 1) {
			$this->log->addGlobal(
				'error',
				'separator',
				"Le séparateur '{$this->separator}' n'est pas présent dans la ligne d'en-tête."
			);
			return null;
		}
		if ($this->ignoreHeaderCase) {
			$fileHeader = array_map('strtolower', $fileHeader);
			$expectedColumns = array_map('strtolower', $expectedColumns);
		}
		$unknownColumns = array_diff($fileHeader, $expectedColumns);
		if ($unknownColumns) {
			$this->log->addGlobal(
				'warning',
				'separator',
				"L'entête reçu contient des colonnes inconnues : " . join(', ', $unknownColumns)
			);
		}
		return array_flip(array_intersect($fileHeader, $expectedColumns));
	}

	/**
	 * Returns the cell's content on the current line in the column selected by its name.
	 *
	 * @param array  $line     The current line from the CSV.
	 * @param string $name     The column name.
	 * @param string $default  (opt) Default value.
	 * @return string
	 */
	public function getCol($line, $name, $default='')
	{
		if (!isset($this->colMap)) {
			$this->colMap = $this->parseCsvHeader($this->expectedColumns, $this->expectedColumns);
		}
		if ($this->ignoreHeaderCase) {
			$name = strtolower($name);
		}
		return parent::getCol($line, $name, $default);
	}

	/**
	 * Splits a line into columns.
	 *
	 * @param string $line
	 * @return array
	 */
	public function splitRawLine(string $line): array
	{
		return array_map('trim', str_getcsv($line, $this->separator));
	}
}
