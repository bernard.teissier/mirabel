<?php

/**
 * This is the model class for table "Titre_Editeur".
 *
 * The followings are the available columns in table 'Titre_Editeur':
 * @property int $titreId
 * @property int $editeurId
 * @property string $ancien ?bool ""|"0"|"1"
 * @property string $commercial ?bool ""|"0"|"1"
 * @property string $intellectuel ?bool ""|"0"|"1"
 * @property ?string $role
 * @property string $hdateModif timestamp MySQL
 *
 * @property Titre $titre
 * @property Editeur $editeur
 */
class TitreEditeur extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @return TitreEditeur the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Titre_Editeur';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['titreId, editeurId', 'required'],
			['titreId, editeurId', 'numerical', 'integerOnly' => true, 'allowEmpty' => false],
			['ancien, commercial, intellectuel', 'boolean'],
			['role', 'validateRole'],
		];
	}

	public function validateRole(string $attribute)
	{
		$value = $this->{$attribute};
		if ($value) {
			$allowed = self::getPossibleRoles();
			if (!isset($allowed[$value])) {
				$this->addError($attribute, "rôle inconnu");
			}
		} else {
			$value = null;
		}
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'titre' => [self::BELONGS_TO, 'Titre', 'titreId'],
			'editeur' => [self::BELONGS_TO, 'Editeur', 'editeurId'],
		];
	}

	public function attributeLabels()
	{
		return [
			'ancien' => "Éditeur précédent",
			'commercial' => "rôle de direction & rédaction",
			'intellectuel' => "rôle de publication &amp; diffusion",
			'role' => "Fonction éditoriale",
			'hdateModif' => "Dernière modification",
		];
	}

	public function afterFind()
	{
		foreach (['ancien', 'commercial', 'intellectuel'] as $k) {
			if ($this->{$k} === null) {
				$this->{$k} = '';
			} else {
				$this->{$k} = (string) $this->{$k};
			}
		}
		return parent::afterFind();
	}

	public function beforeSave()
	{
		foreach (['ancien', 'commercial', 'intellectuel'] as $k) {
			if ($this->{$k} === '') {
				$this->{$k} = null;
			} else {
				$this->{$k} = (int) !empty($this->{$k});
			}
		}
		if ($this->role === '') {
			$this->role = null;
		}
		return parent::beforeSave();
	}

	public static function getPossibleRoles(): array
	{
		return Config::read('sherpa.publisher_role');
	}

   public function isComplete(): bool
	{
		return $this->ancien !== null
			&& $this->commercial !== null
			&& $this->intellectuel !== null
			&& $this->role !== null;
	}

}
