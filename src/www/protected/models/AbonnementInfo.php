<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class AbonnementInfo
{
	public $count = 0;

	public $countRessources = 0;

	public $countCollections = 0;

	public $countRevues = 0;

	public function __construct($partenaireId)
	{
		$db = Yii::app()->db;
		$this->countRessources = $db
			->createCommand("SELECT count(*) FROM Abonnement WHERE partenaireId = ? AND collectionId IS NULL AND mask = " . Abonnement::ABONNE)
			->queryScalar([$partenaireId]);
		$this->countCollections = $db
			->createCommand("SELECT count(*) FROM Abonnement WHERE partenaireId = ? AND collectionId IS NOT NULL AND mask = " . Abonnement::ABONNE)
			->queryScalar([$partenaireId]);
		$this->count = $this->countRessources + $this->countCollections;
		if ($this->count) {
			$this->countRevues = $db
				->createCommand(
					"SELECT count(DISTINCT t.revueId)
FROM
    Titre t
    JOIN Service s ON s.titreId = t.id
    JOIN Abonnement a ON s.ressourceId = a.ressourceId AND a.partenaireId = ? AND a.mask = " . Abonnement::ABONNE . "
    LEFT JOIN Service_Collection sc ON s.id = sc.serviceId
WHERE
    (a.collectionId IS NULL) OR (sc.collectionId = a.collectionId)"
				)
				->queryScalar([$partenaireId]);
		}
	}
}
