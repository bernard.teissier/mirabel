<?php

/**
 * ContactServiceForm class.
 *
 * To comment on an imported Service (special contact form)
 */
class ContactServiceForm extends CFormModel
{
	use Honeypot;

	public const FAKE_DATA = "not used";

	public $name;

	public $emailr;

	public $subjectr;

	public $body;

	public $verifyCode;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return [
			// subject is hardcoded
			// name, email and body are required
			['name, emailr, body', 'required'],
			// email has to be a valid email address
			['emailr', 'email'],
			// verifyCode needs to be entered correctly
			['verifyCode', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements(), 'captchaAction' => 'site/captcha', 'enableClientValidation' => false],
			// fakes
			['email', 'validateFakeEmail'],
			['subject', 'validateFakeSubject'],
		];
	}

	/**
	 * Declares customized attribute labels.
	 *
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return [
			'name' => 'Votre nom',
			'emailr' => 'Votre courriel',
			'subjectr' => 'Sujet',
			'body' => 'Message',
			'verifyCode' => 'Code de vérification',
		];
	}
}
