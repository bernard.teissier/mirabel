<?php

/**
 * This is the model class for table "CategorieStats".
 *
 * The followings are the available columns in table 'CategorieStats':
 * @property int $id
 * @property int $numFils
 * @property int $numDescendants
 * @property int $numRevues
 * @property int $numRevuesRec
 * @property int $numRevuesInd
 * @property int $numRevuesIndRec
 * @property int $racine Id de la catégorie-racine
 * @property string $hdateModif
 *
 * @property Categorie $categorie
 */
class CategorieStats extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @return CategorieStats the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'CategorieStats';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'categorie' => [self::BELONGS_TO, 'Categorie', 'id'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'numFils' => 'Nombre de fils',
			'numDescendants' => 'Nombre de descendants',
			'numRevues' => 'Nombre de revues',
			'numRevuesRec' => 'Nombre de revues récursivement',
			'racine' => 'Racine',
			'hdateModif' => 'Date modif.',
		];
	}

	public static function fill()
	{
		$db = Yii::app()->db;

		$db->createCommand("UPDATE Categorie SET profondeur = 999, chemin = '' WHERE parentId IS NOT NULL")->execute();
		$sql = "UPDATE Categorie c JOIN Categorie p ON c.parentId = p.id"
			. " SET c.profondeur = %d, c.chemin = CONCAT(p.chemin, '/', LPAD(c.parentId, %d, '0'))"
			. " WHERE c.profondeur = 999 AND p.profondeur <> 999";
		for ($depth = 1; $depth <= Categorie::MAX_DEPTH; $depth++) {
			$db->createCommand(sprintf($sql, $depth, Categorie::ID_PADDING))->execute();
		}

		$sqls = [
			"CREATE TEMPORARY TABLE Thematique AS (
    SELECT DISTINCT t.revueId, IFNULL(cr.categorieId, ca.categorieId) AS categorieId
    FROM Titre t
        LEFT JOIN Categorie_Revue cr ON t.revueId = cr.revueId
        LEFT JOIN CategorieAlias_Titre ct ON ct.titreId = t.id
        LEFT JOIN CategorieAlias ca ON ct.categorieAliasId = ca.id
)",
			// Work around a MySQL limitation: 1137 Can't reopen table
			// "You cannot refer to a TEMPORARY table more than once in the same query."
			"CREATE TEMPORARY TABLE ThematiqueParent AS (
    SELECT th.revueId, c.parentId
    FROM Thematique th
        JOIN Categorie c ON th.categorieId = c.id
)",
			"TRUNCATE CategorieStats",
			// racine
			"INSERT INTO CategorieStats (
                SELECT id, 0, 0, 0, 0, 0, 0, IF(parentId <> '', CAST(parentId AS UNSIGNED), 0), NOW() FROM (
                    SELECT id, SUBSTRING_INDEX(SUBSTRING(chemin FROM " . (Categorie::ID_PADDING + 3) . "), '/', 1) AS parentId FROM Categorie ORDER BY id) AS t)",
			// numFils
			"UPDATE CategorieStats cs"
			. " JOIN (SELECT parentId, count(*) as numFils FROM Categorie WHERE role = 'public' GROUP BY parentId) t ON cs.id = t.parentId"
			. " SET cs.numFils = t.numFils",
			// numRevues
			"UPDATE CategorieStats cs"
			. " JOIN (SELECT categorieId, count(*) as numRevues FROM Categorie_Revue GROUP BY categorieId) t ON cs.id = t.categorieId"
			. " SET cs.numRevues = t.numRevues",
			// numRevuesInd
			<<<EOSQL
				UPDATE CategorieStats cs
				JOIN (
				    SELECT categorieId, count(DISTINCT revueId) as numRevuesInd
				    FROM Thematique
				    GROUP BY categorieId
				    ) t ON cs.id = t.categorieId
				SET cs.numRevuesInd = t.numRevuesInd
				EOSQL,
			// numRevuesRec et numRevuesIndRec pour les feuilles
			"UPDATE CategorieStats cs JOIN Categorie c USING(id)"
			. " SET cs.numRevuesRec = cs.numRevues, cs.numRevuesIndRec = cs.numRevuesInd"
			. " WHERE profondeur = " . Categorie::MAX_DEPTH,
		];
		foreach ($sqls as $sql) {
			$db->createCommand($sql)->execute();
		}
		$sqlsRec = [
			// numRevuesRec et numDescendants
			<<<EOSQL
				UPDATE CategorieStats cs
				JOIN (
				    SELECT
				        c.parentId,
				        count(*) as numFils,
				        SUM(cs2.numDescendants) AS numDescendants,
				        SUM(cs2.numRevuesRec) AS numRevuesRec,
				        SUM(cs2.numRevuesIndRec) AS numRevuesIndRec
				    FROM Categorie c
				    JOIN CategorieStats cs2 ON c.id = cs2.id
				    WHERE c.role = 'public' AND c.profondeur = %d
				    GROUP BY c.parentId
				    ) t
				    ON t.parentId = cs.id
				SET cs.numDescendants = t.numFils + t.numDescendants
				    , cs.numRevuesRec = cs.numRevues + t.numRevuesRec
				    , cs.numRevuesIndRec = cs.numRevuesInd + t.numRevuesIndRec
				EOSQL,
		];
		for ($depth = Categorie::MAX_DEPTH; $depth > 0; $depth--) {
			foreach ($sqlsRec as $sql) {
				$db->createCommand(sprintf($sql, $depth))->execute();
			}
		}

		$sql = <<<EOSQL
			UPDATE CategorieStats cs
			JOIN (
			    SELECT categorieId, count(DISTINCT revueId) as numRevuesIndRec
			    FROM (
			        SELECT categorieId, revueId
			            FROM Thematique
			        UNION
			        SELECT parentId, revueId
			            FROM ThematiqueParent
			        ) u
			    GROUP BY categorieId
			    ) t ON cs.id = t.categorieId
			SET cs.numRevuesIndRec = t.numRevuesIndRec
			EOSQL;
		$db->createCommand(sprintf($sql))->execute();
	}
}
