<?php

use models\sudoc\Api;
use models\sudoc\Notice;

/**
 * This is the model class for table "Issn".
 *
 * The followings are the available columns in table 'Issn':
 * @property int $id
 * @property int $titreId
 * @property ?string $issn
 * @property string $support
 * @property int $statut
 * @property string $titreReference
 * @property string $issnl
 * @property string $dateDebut
 * @property string $dateFin
 * @property string $pays 2 letters code (ISO 3166-1:2006)
 * @property string $sudocPpn
 * @property bool $sudocNoHolding
 * @property int $worldcatOcn
 * @property string $bnfArk
 *
 * @property Titre $titre
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class Issn extends CActiveRecord
{
	public const SUPPORT_ELECTRONIQUE = 'electronique';

	public const SUPPORT_INCONNU = 'inconnu';

	public const SUPPORT_PAPIER = 'papier';

	public const STATUT_VALIDE = 0;

	public const STATUT_ERRONE = 1;

	public const STATUT_ENCOURS = 2;

	public const STATUT_ANNULE = 3;

	public const STATUT_SANS = 4;

	public static $enumSupport = [
		self::SUPPORT_PAPIER => "Papier (ISSN)",
		self::SUPPORT_ELECTRONIQUE => "Électronique (ISSN-E)",
		self::SUPPORT_INCONNU => "Indéterminé",
	];

	public static $enumStatut = [
		self::STATUT_VALIDE => "Valide",
		self::STATUT_ERRONE => "Erroné",
		self::STATUT_ANNULE => "Annulé",
		self::STATUT_ENCOURS => "En cours",
		self::STATUT_SANS => "Sans ISSN",
	];

	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @return Issn the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Issn';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['issn', 'filter', 'filter' => 'strtoupper'],
			['issn, dateDebut, dateFin, issnl, sudocPpn', 'filter', 'filter' => function ($x) {
				return trim($x, " \t \n ​");
			}],
			['issnl, sudocPpn, worldcatOcn', 'default', 'value' => null],
			['support', 'required'],
			['issnl', '\models\validators\IssnValidator', 'type' => 'ISSN-L'],
			['worldcatOcn, titreId', 'numerical', 'integerOnly' => true, 'allowEmpty' => true],
			['sudocPpn', 'length', 'max' => 9],
			['pays', 'length', 'max' => 2],
			['bnfArk', 'length', 'max' => 100],
			['sudocNoHolding', 'boolean'],
			['titreReference', 'length', 'max' => 512],
			['support', 'in', 'range' => array_keys(self::$enumSupport)],
			['statut', 'in', 'range' => array_keys(self::$enumStatut)],
			['dateDebut, dateFin', 'length', 'max' => 10],
		];
	}

	public function afterValidate()
	{
		if (!$this->id) {
			$this->id = null;
		}
		if (strpos($this->sudocPpn, '/') !== false) {
			$this->sudocPpn = preg_replace('/^.+\//', '', $this->sudocPpn);
		}
		if ((int) $this->statut === self::STATUT_SANS) {
			if ($this->issn) {
				$this->addError('issn', "Le numéro doit être vide quand le statut est à <em>sans ISSN</em>.");
			} else {
				$this->issn = null;
			}
		} elseif ((int) $this->statut === self::STATUT_ENCOURS) {
			if ($this->issn) {
				$this->addError('issn', "Le numéro doit être vide quand le statut est à <em>en cours</em>.");
			} else {
				$this->issn = null;
			}
		} elseif (empty($this->issn)) {
			$this->addError('issn', "L'ISSN doit être rempli si le statut n'est ni 'en cours' ni 'sans ISSN'.");
		} else {
			if (in_array($this->statut, [self::STATUT_VALIDE, self::STATUT_ANNULE])) {
				$validator = CValidator::createValidator('\models\validators\IssnValidator', $this, 'issn', ['type' => 'ISSN']);
				$validator->validate($this);
			}
		}
		foreach (['dateDebut', 'dateFin'] as $attr) {
			if ($this->{$attr}) {
				$this->{$attr} = str_replace(['–', '—', '‒', '−', '‐'], '-', $this->{$attr});
				$this->{$attr} = trim(preg_replace('/[?.]$/', 'X', $this->{$attr}));
				if (!preg_match('/^(\d{4}(-\d\d){0,2}|\d\d[\dX]{2})$/', $this->{$attr})) {
					$this->addError($attr, "Une date de validité doit respecter soit le format AAAA[[-MM[-DD]] (2017-04), soit au plus deux chiffres indéterminés (201X).");
				}
				if ($this->{$attr} < "1500") {
					$this->addError($attr, "Une date de validité ne peut être antérieure à 1500.");
				}
			}
		}
		if ($this->dateDebut && $this->dateFin && str_replace('X', '0', $this->dateDebut) > str_replace('X', '9', $this->dateFin)) {
			$this->addError($attr, "La date de validité de début doit être antérieure à celle de fin.");
		}
		return parent::afterValidate();
	}

	/**
	 * Return false and add errors if the ISSN is not unique in the DB.
	 *
	 * @param Titre $titre
	 */
	public function validateUnicity(Titre $titre)
	{
		if (empty($this)) {
			return true;
		}
		if (!in_array($this->support, [Issn::SUPPORT_ELECTRONIQUE, Issn::SUPPORT_PAPIER])) {
			return true;
		}
		if ($this->statut != Issn::STATUT_VALIDE) {
			return true;
		}

		if (isset($this->support) && in_array($this->support, [Issn::SUPPORT_ELECTRONIQUE, Issn::SUPPORT_PAPIER])) {
			$dup = Issn::model()->findByAttributes([
				'issn' => $this->issn,
				'support' => ($this->support == Issn::SUPPORT_ELECTRONIQUE ? Issn::SUPPORT_PAPIER : Issn::SUPPORT_ELECTRONIQUE),
			]);
			if ($dup && (empty($this->id) || $dup->id != $this->id)) {
				$this->addError('issn', "Cet ISSN {$this->issn} est déjà déclaré comme ISSN de type différent (papier|électronique).");
				return false;
			}
		}

		if ($this->support == Issn::SUPPORT_ELECTRONIQUE) {
			// May be duplicated, but only in the same Revue
			$byIssn = Titre::model()
				->findBySql(
					"SELECT t.* FROM Issn i JOIN Titre t ON t.id = i.titreId WHERE i.issn = :issn AND t.revueId != :revueId",
					[':issn' => $this->issn, ':revueId' => (int) $titre->revueId]
				);
		} else {
			// Cannot be duplicated
			$byIssn = Titre::model()
				->findBySql(
					"SELECT t.* FROM Issn i JOIN Titre t ON t.id = i.titreId WHERE i.issn = :issn AND t.id != :titreId",
					[':issn' => $this->issn, ':titreId' => (int) $titre->id]
				);
		}
		if ($byIssn) {
			$this->addError('issn', "Cet ISSN {$this->issn} est en conflit avec l'ISSN du titre « {$byIssn->titre} » [{$byIssn->id}], revue d'ID {$byIssn->revueId}.");
			return false;
		}

		// ISSN = ISSN-L is okay, but only in the same Revue
		$byIssnl = Titre::model()
			->findBySql(
				"SELECT t.* FROM Issn i JOIN Titre t ON t.id = i.titreId WHERE i.issnl = :issn AND t.revueId != :revueId",
				[':issn' => $this->issn, ':revueId' => (int) $titre->revueId]
			);
		if ($byIssnl) {
			$this->addError('issn', "Cet ISSN {$this->issn} est en conflit avec l'ISSN-L du titre « {$byIssnl->titre} » [{$byIssnl->id}], revue d'ID {$byIssnl->revueId}.");
			return false;
		}

		return true;
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'titre' => [self::BELONGS_TO, 'Titre', 'titreId'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'titreId' => 'Titre',
			'issn' => 'ISSN',
			'support' => "Support",
			'statut' => "Statut",
			'titreReference' => 'Titre de référence',
			'issnl' => 'ISSN de liaison',
			'dateDebut' => "Date de début d'ISSN",
			'dateFin' => "Date de fin d'ISSN",
			'pays' => "Pays notice ISSN",
			'country' => "Pays notice ISSN",
			'sudocPpn' => 'PPN (SUDOC)',
			'sudocNoHolding' => 'Sans notice SUDOC',
			'worldcatOcn' => 'OCLC (Worldcat)',
			'bnfArk' => "BNF Ark",
		];
	}

	/**
	 * Overload setAttributes() so that invalid country codes are ignored.
	 *
	 * @staticvar null|array $countries
	 * @param array $values
	 * @param bool $safeOnly
	 */
	public function setAttributes($values, $safeOnly = true)
	{
		static $countries = null;
		if ($countries === null) {
			$countries = Tools::sqlToPairs("SELECT code2, 1 FROM Pays");
			$countries[''] = '';
			$countries['ZZ'] = "Plusieurs pays";
		}

		parent::setAttributes($values, $safeOnly);

		if (!isset($countries[$this->pays])) {
			$this->pays = '';
		}
	}

	/**
	 * Modify the array by loading the POST data into existing and new records.
	 *
	 * New records must have the key "new...",
	 * deleted record must have a field "_delete" with non-empty value.
	 *
	 * self.isEmpty() is used to ignore the records to add that are empty.
	 *
	 * @param array $array
	 * @param array $post
	 * @return array {insert: [], update: [], delete: []}
	 */
	public static function loadMultiple(&$array, $post)
	{
		$className = __CLASS__;
		if (!isset($post[$className])) {
			return [];
		}
		$newData = $post[$className];
		$changes = ['insert' => [], 'update' => [], 'delete' => []];
		foreach ($array as $id => $existingRecord) {
			if (!empty($newData[$id]['_delete'])) {
				$changes['delete'][] = [
					'before' => $existingRecord,
				];
				unset($array[$id]);
			} elseif (isset($newData[$id])) {
				$previous = clone ($existingRecord);
				$array[$id]->setAttributes($newData[$id]);
				if ($array[$id]->attributes != $previous->attributes) {
					$changes['update'][] = [
						"before" => $previous,
						"after" => $array[$id],
					];
				}
			}
		}
		foreach ($newData as $id => $newAttr) {
			if (strncmp('new', $id, 3) === 0 && empty($newData[$id]['_delete'])) {
				$newRecord = new self;
				$newRecord->issn = $newAttr['issn'];
				$newRecord->fillBySudoc();
				$newRecord->setAttributes($newAttr);
				if (!method_exists($newRecord, 'isEmpty') || !$newRecord->isEmpty()) {
					$array[$id] = $newRecord;
					$changes['insert'][] = [
						"after" => $newRecord,
					];
				}
			}
		}
		return array_filter($changes);
	}

	/**
	 * @param Issn[] $array
	 * @param Titre $titre
	 * @return bool
	 */
	public static function validateMultiple($array, Titre $titre)
	{
		$status = true;
		if ($array) {
			foreach ($array as $a) {
				$status = $a->validate() && $a->validateUnicity($titre) && $status;
			}
		}
		return $status;
	}

	/**
	 * @return bool
	 */
	public function isEmpty()
	{
		return (
			(int) $this->statut !== self::STATUT_ENCOURS
			&& (int) $this->statut !== self::STATUT_SANS
			&& !trim($this->issn)
			&& empty($this->sudocPpn)
			&& empty($this->worldcatOcn)
		);
	}

	/**
	 * @return string
	 */
	public function getHtml($microdata = false)
	{
		$options = ['title' => "International Standard Serial Number - " . self::$enumSupport[$this->support]];
		$text = "";
		if ($this->statut == self::STATUT_ERRONE || $this->statut == self::STATUT_ANNULE) {
			$text = $this->issn;
			$options['class'] = 'issn-bad';
			$options['title'] .= " - " . Issn::$enumStatut[$this->statut];
		} elseif ($this->statut != self::STATUT_VALIDE) {
			$text = self::$enumStatut[$this->statut];
		} else {
			$text = $this->issn;
			$options['class'] = 'issn-number';
			if ($microdata) {
				$options['itemprop'] = 'issn';
			}
		}
		return CHtml::tag('span', $options, $text);
	}

	/**
	 * @param bool $icon
	 * @return string HTML
	 */
	public function getSudocLink($icon = true)
	{
		if (!$this->sudocPpn) {
			return '';
		}
		if ($icon && $this->sudocNoHolding) {
			return '';
		}
		$url = sprintf(Titre::URL_SUDOC, $this->sudocPpn);
		if ($icon) {
			$text = CHtml::image(
				Yii::app()->baseUrl . '/images/icons/sudoc.png',
				'SUDOC',
				['title' => 'SUDOC : Catalogue du Système Universitaire de Documentation - France']
			);
		} else {
			$text = $this->sudocPpn;
		}
		if ($this->sudocNoHolding) {
			return CHtml::tag('abbr', ['class' => 'sudoc-noholding', 'title' => "Pas de notice publique dans le Sudoc (no holding)"], $text);
		}
		return CHtml::link($text, $url, ['target' => '_blank', 'title' => "Notice SUDOC (nouvel onglet)"]);
	}

	public function getCountry()
	{
		if (!$this->pays) {
			return '';
		}
		if (strtoupper($this->pays) === 'ZZ') {
			return "multiples";
		}
		$name = $this->dbConnection
			->createCommand("SELECT nom FROM Pays WHERE code2 = ?")
			->queryScalar([strtoupper($this->pays)]);
		return $name ?: $this->pays;
	}

	public function getBnfLink()
	{
		if (!$this->bnfArk) {
			return '';
		}
		return CHtml::link(
			$this->bnfArk,
			sprintf(Titre::URL_BNF, $this->bnfArk),
			['target' => '_blank', 'title' => "Notice à la Bibliothèque Nationale de France (nouvel onglet)"]
		);
	}

	public function getWorldcatLink()
	{
		if (!$this->worldcatOcn) {
			return '';
		}
		return CHtml::link(
			(string) $this->worldcatOcn,
			sprintf(Titre::URL_WORLDCAT, $this->worldcatOcn),
			['target' => '_blank', 'title' => "Notice Worldcat (nouvel onglet)"]
		);
	}

	/**
	 * Query the Sudoc web services twice, first by ISSN, then by PPN, and fill in the attributes.
	 */
	public function fillBySudoc(?Api $sudocApi = null)
	{
		if (!$this->issn) {
			return;
		}
		if ($sudocApi === null) {
			$sudocApi = new Api();
		}
		$notice = $sudocApi->getNoticeByIssn($this->issn);
		if ($notice) {
			$this->fillBySudocNotice($notice);
		}
	}

	public function fillBySudocNotice(Notice $notice)
	{
		$this->setAttributes(
			array_filter(
				[
					'bnfArk' => $notice->bnfArk,
					'dateDebut' => $notice->dateDebut,
					'dateFin' => $notice->dateFin,
					'issn' => $notice->issn,
					'issnl' => $notice->issnl,
					'pays' => $notice->pays,
					'support' => $notice->support,
					'statut' => $notice->statut,
					'sudocPpn' => $notice->ppn,
					'sudocNoHolding' => $notice->sudocNoHolding,
					'titreReference' => $notice->titreIssn,
					'worldcatOcn' => $notice->worldcat,
				],
				function ($x) {
					return $x !== null && $x !== '';
				}
			),
			false
		);
	}
}
