<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class AbonnementDecorator
{
	/**
	 * @var int
	 */
	private $abonnementType;

	/**
	 * @var string
	 */
	private $collectionType;

	public function __construct($abonnementType, $collectionType = "")
	{
		$this->abonnementType = (int) $abonnementType;
		$this->collectionType = $collectionType;
	}

	public function getHtmlButton($formHiddenInputs = [])
	{
		$isRessource = empty($formHiddenInputs['collectionId']);
		$html = '';
		$hiddenFields = '';
		foreach ($formHiddenInputs as $k => $v) {
			$hiddenFields .= CHtml::hiddenField($k, $v);
		}
		if ($this->abonnementType === Abonnement::ABONNE) {
			$html = CHtml::form(['/abonnement/delete'])
				. $hiddenFields
				. CHtml::htmlButton("Se désabonner", ['type' => 'submit', 'class' => "btn btn-small"])
				. CHtml::endForm();
		} elseif ($this->abonnementType === Abonnement::MASQUE) {
			$html = CHtml::form(['/abonnement/delete'])
				. $hiddenFields
				. CHtml::htmlButton("Démasquer", ['type' => 'submit', 'class' => "btn btn-small"])
				. CHtml::endForm();
		} elseif ($this->collectionType === Collection::TYPE_TEMPORAIRE) {
			// nothing
		} elseif ($this->abonnementType === Abonnement::PAS_ABONNE) {
			$html = $html = CHtml::form(['/abonnement/hide'])
					. $hiddenFields
					. CHtml::htmlButton(
						'<i class="icon-ban-circle"></i>',
						[
							'type' => 'submit',
							'class' => "btn btn-small",
							'title' => ($isRessource ?
								"Masquer la ressource (collection maîtresse) et tous ses accès"
								: "Masquer cette collection et tous ses accès"),
						]
					)
					. CHtml::endForm() . " ";
			$html .= CHtml::form(['/abonnement/subscribe'])
				. $hiddenFields
				. CHtml::htmlButton(
					"S'abonner",
					[
						'type' => 'submit',
						'class' => "btn btn-small",
						"title" => "S'abonner à cette " . ($isRessource ? "ressource" : "collection"),
					]
				)
				. CHtml::endForm();
		}   // ABONNE_VIA_COLLECTION

		return $html;
	}

	/**
	 * @return string HTML
	 */
	public function getHtmlStatus($suffix = "")
	{
		if ($this->abonnementType === Abonnement::ABONNE) {
			return CHtml::tag(
				'span',
				['class' => 'abonne-statut abonne', 'title' => "institution abonnée"],
				Abonnement::$enumMask[$this->abonnementType] . ($suffix ? CHtml::encode(" ($suffix)") : "")
			);
		}
		if ($this->abonnementType === Abonnement::MASQUE) {
			return CHtml::tag(
				'span',
				[
					'class' => 'abonne-statut masque',
					'title' => "Ces accès sont par défaut masqués dans Mir@bel pour les utilisateurs" . ($suffix ? " \"$suffix\"" : ""),
				],
				Abonnement::$enumMask[$this->abonnementType] . ($suffix ? CHtml::encode(" ($suffix)") : "")
			);
		}
		return '';
	}
}
