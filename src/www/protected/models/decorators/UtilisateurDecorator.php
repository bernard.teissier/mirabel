<?php

/**
 * Description of UtilisateurDecorator
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class UtilisateurDecorator extends CComponent
{
	/**
	 * @var ?Utilisateur
	 */
	private $utilisateur;

	/**
	 * Custom constructor.
	 *
	 * @param int $utilisateurId
	 * @return \UtilisateurDecorator
	 */
	public static function fromId($utilisateurId)
	{
		$new = new self();
		$new->utilisateur = Utilisateur::model()->findByPk($utilisateurId);
		return $new;
	}

	/**
	 * Custom constructor.
	 *
	 * @param \Utilisateur $utilisateur
	 * @return \UtilisateurDecorator
	 */
	public static function fromInstance($utilisateur)
	{
		$new = new self();
		$new->utilisateur = Utilisateur::model()->findByPk($utilisateur);
		return $new;
	}

	/**
	 * @param string $default Text to display when the model is not set.
	 * @return string HTML
	 */
	public function getFullName(string $default = "-"): string
	{
		if ($this->utilisateur) {
			if ($this->utilisateur->nomComplet) {
				return $this->utilisateur->nomComplet;
			}
			return $this->utilisateur->nom . " " . $this->utilisateur->prenom;
		}
		return $default;
	}

	/**
	 * @param string $default Text to display when the model is not set.
	 * @param bool $partenaire
	 * @return string HTML
	 */
	public function getFullHtmlLink($default = "-", $partenaire = false): string
	{
		if ($this->utilisateur) {
			return
				CHtml::link(
					CHtml::encode($this->getFullName($default)),
					['/utilisateur/view', 'id' => $this->utilisateur->id]
				)
				. ($partenaire ?
					" / " . PartenaireDecorator::fromId($this->utilisateur->partenaireId)->getHtmlLink(false)
					: "");
		}
		return $default;
	}
}
