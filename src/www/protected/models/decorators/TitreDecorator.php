<?php

/**
 * Description of TitreDecorator
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class TitreDecorator extends CComponent
{
	/**
	 * @var Titre
	 */
	private $titre;

	/**
	 * @var string|null cache
	 */
	private $frontPath = null;

	/**
	 * @param Titre $titre
	 */
	public function __construct(Titre $titre = null)
	{
		$this->titre = $titre;
	}

	/**
	 * @param bool $forceRefresh
	 * @return string HTML
	 */
	public function displayFront($forceRefresh = false)
	{
		if ($this->hasFront()) {
			$url = $this->getFrontUrl(true)
				. ($forceRefresh ? '?refresh=' . rand(1000, 9999) : '');
			return '<div class="overlay" title="Source : ' . CHtml::encode($this->getFrontSource()) . '"> </div>'
				. '<div class="titre-couverture">'
				. CHtml::image($url, $this->titre->titre)
				. "</div>\n";
		}
		return '';
	}

	/**
	 * Return the domain part of the front URL, e.g. http://cairn.info
	 *
	 * @return string
	 */
	public function getFrontSource()
	{
		if ($this->titre->urlCouverture) {
			$m = [];
			if (preg_match('#^((?:https?://)[^/]+)#', $this->titre->urlCouverture, $m)) {
				return $m[1];
			}
		}
		return '';
	}

	/**
	 * @return string Empty string if none.
	 */
	public function hasFront()
	{
		if ($this->frontPath === null) {
			$base = dirname(Yii::app()->basePath);
			$path = $this->getFrontRelPath(true);
			$glob = glob("$base$path");
			$this->frontPath = ($glob ? substr($glob[0], strlen($base)) : "");
		}
		return $this->frontPath;
	}

	/**
	 * @param bool $absolute (opt, false)
	 * @return string
	 */
	public function getFrontUrl($absolute = false)
	{
		if ($this->hasFront()) {
			return Yii::app()->getBaseUrl($absolute) . $this->getFrontRelPath();
		}
		return '';
	}

	/**
	 * @param bool $globbing (opt, false)
	 * @return string
	 */
	private function getFrontRelPath($globbing = false)
	{
		if ($globbing) {
			return sprintf('/images/titres-couvertures/%09d.*', $this->titre->id);
		}
		if ($this->frontPath === null) {
			$this->hasFront();
		}
		return $this->frontPath;
	}
}
