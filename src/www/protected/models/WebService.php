<?php

/**
 * Handles the web service (for Koha).
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class WebService extends CModel
{
	public $suppr;

	public $partenaire;

	public $ressource;

	public $collection;

	public $mesressources;

	public $issn;

	public $issnl;

	public $issne;

	public $type;

	public $acces;

	public $lacunaire;

	public $selection;

	public $revue = false;

	private $abonnementSearch;

	public function attributeNames()
	{
		return [
			'partenaire', 'ressource', 'issn', 'issnl', 'issne', 'suppr',
			'type', 'acces', 'lacunaire', 'selection', 'mesressources',
		];
	}

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return [
			['partenaire', 'numerical', 'integerOnly' => true],
			['ressource, collection', 'ext.validators.ArrayOfIntValidator'],
			['issn, issne, issnl', 'length', 'max' => 9],
			['issn, issne, issnl', '\models\validators\IssnValidator'],
			['suppr', 'date', 'format' => 'yyyy-MM-dd'],
			// criteria
			['type', 'in', 'range' => array_keys(Service::$enumType)], // enum
			['acces', 'in', 'range' => array_keys(Service::$enumAcces)], // enum
			['lacunaire, selection, mesressources, revue', 'boolean'],
		];
	}

	public function beforeValidate()
	{
		foreach (['ressource', 'collection'] as $attr) {
			if ($this->{$attr} && !is_array($this->{$attr})) {
				if (preg_match('/^(\d+,)+\d+$/', $this->{$attr})) {
					$this->{$attr} = explode(',', $this->{$attr});
				} else {
					$this->{$attr} = [$this->{$attr}];
				}
			}
		}
		foreach (['type', 'acces'] as $field) {
			if ($this->{$field}) {
				$this->{$field} = ucfirst(strtolower($this->{$field}));
			}
		}
		if ($this->type) {
			if ($this->type === 'Resume') {
				$this->type = 'Résumé';
			} elseif ($this->type === 'Integral') {
				$this->type = 'Intégral';
			} elseif ($this->type === 'Texte') {
				$this->type = 'Intégral';
			}
		}
		return parent::beforeValidate();
	}

	public function afterValidate()
	{
		if ($this->mesressources) {
			if (!$this->partenaire) {
				$this->addError('mesressources', 'Le partenaire doit être fourni');
			} elseif ($this->ressource) {
				$this->addError('ressource', 'Incompatible avec le paramètre "mesressources"');
			} else {
				$this->ressource = $this->getRessourceIds();
				if (empty($this->ressource)) {
					$this->addError('mesressources', 'Aucune ressource pour ce partenaire');
				}
			}
		}
		parent::afterValidate();
	}

	public function listDeletedServices()
	{
		$sql = "SELECT * FROM Intervention "
			. "WHERE hdateVal >= " . strtotime($this->suppr . ' 00:00:00')
			. " AND action = 'service-D'";
		$interventions = Intervention::model()->findAllBySql($sql);
		$xml = "<services_suppr>\n";
		/* @var $i Intervention */
		foreach ($interventions as $i) {
			$content = $i->contenuJson->toArray();
			foreach ($content as $c) {
				if ($c['model'] === 'Service' && $c['operation'] === 'delete') {
					$xml .= "\t<service>" . $c['before']['id'] . "</service>\n";
				}
			}
		}
		$xml .= '</services_suppr>';
		return $xml;
	}

	public function listMatchingTitles()
	{
		$servicesCmd = Yii::app()->db->createCommand(
			"SELECT s.id, s.url, s.alerteRssUrl, s.alerteMailUrl, s.derNumUrl"
			. ", s.type, s.acces, s.lacunaire, s.selection"
			. ", s.volDebut, s.noDebut, s.numeroDebut, s.volFin, s.noFin, s.numeroFin"
			. ", s.dateBarrDebut, s.dateBarrFin, s.dateBarrInfo, s.embargoInfo"
			. ", s.notes, s.statut, s.import, s.hdateCreation, s.hdateModif"
			. ", s.ressourceId"
			. ", r.nom, r.url AS rUrl "
			. "FROM Service s JOIN Ressource r ON s.ressourceId = r.id "
			. ($this->collection ? " JOIN Service_Collection sc ON s.id = sc.serviceId AND sc.collectionId IN (" . join(',', $this->collection) . ")" : '')
			. "WHERE s.titreId = :titreId "
			. ($this->type ? " AND s.type= '{$this->type}' " : '')
			. ($this->acces ? " AND s.acces = '{$this->acces}' " : '')
			. (isset($this->lacunaire) ? " AND s.lacunaire = " . $this->lacunaire : '')
			. (isset($this->selection) ? " AND s.selection = " . $this->selection : '')
			. ($this->ressource ? " AND s.ressourceId IN (" . join(',', $this->ressource) . ")" : '')
			. " GROUP BY s.id, r.id"
		);

		$xml = "<revues>\n";
		foreach ($this->findTitles() as $row) {
			$services = $servicesCmd->queryAll(true, ['titreId' => $row['id']]);
			if (!$services) {
				continue;
			}
			$xml .= "<revue>\n" . self::xmlForTitle($row);
			$xml .= "\t<services>\n";
			foreach ($services as $s) {
				$xml .= $this->xmlForService($s);
			}
			$xml .= "\t</services>\n";
			$xml .= "</revue>\n";
		}
		$xml .= "</revues>\n";
		return $xml;
	}

	/**
	 * Builds the XML response: error, list of suppr, or list of titles.
	 *
	 * @return string XML response.
	 */
	public function run()
	{
		if ($this->partenaire > 0) {
			$this->abonnementSearch = new AbonnementSearch();
			$this->abonnementSearch->partenaireId = (int) $this->partenaire;
		}
		echo '<' . '?xml version="1.0" encoding="UTF-8"?' . ">\n";
		if (!$this->validate()) {
			$xml = "<erreurs>\n";
			foreach ($this->errors as $attr => $e) {
				$tag = "\t<msg critere=\"$attr\">%s</msg>\n";
				foreach ($e as $msg) {
					$xml .= sprintf($tag, CHtml::encode($msg));
				}
			}
			$xml .= "</erreurs>\n";
		} else {
			if ($this->suppr) {
				$xml = $this->listDeletedServices();
			} else {
				$xml = $this->listMatchingTitles();
			}
		}
		return $xml;
	}

	protected function findTitles()
	{
		$q = [
			'select' => ['t.id', "GROUP_CONCAT(Issn.issn) AS issn", 't.url'],
			'where' => [],
			'join' => ['Issn' => " LEFT JOIN Issn ON Issn.titreId = t.id "],
			'params' => [],
		];
		if ($this->partenaire) {
			$q['select'][] = 'pt.identifiantLocal';
			$q['join']['pt'] = ' LEFT JOIN Partenaire_Titre pt ON (pt.titreId = t.id) ';
			$q['where'][] = "partenaireId = :partenaire";
			$q['params'][":partenaire"] = (int) $this->partenaire;
		} else {
			$q['select'][] = "NULL AS identifiantLocal";
		}
		if ($this->ressource) {
			$q['where'][] = "s.ressourceId IN (" . join(',', $this->ressource) . ")";
			$q['join']['s'] = ' JOIN Service s ON s.titreId = t.id ';
		}
		if ($this->collection) {
			$q['where'][] = "sc.collectionId IN (" . join(',', $this->collection) . ")";
			$q['join']['s'] = ' JOIN Service s ON s.titreId = t.id ';
			$q['join']['sc'] = ' JOIN Service_Collection sc ON s.id = sc.serviceId ';
		}
		foreach (['type', 'acces', 'selection', 'lacunaire'] as $c) {
			if (isset($this->{$c})) {
				$q['join']['s'] = ' JOIN Service s ON s.titreId = t.id ';
				$q['where'][] = "s.$c = :$c";
				$q['params'][":" . $c] = $this->{$c};
			}
		}
		$qNoIssn = $q;

		if ($this->issn) {
			$q['where'][] = "Issn.issn = :issn AND Issn.support = :support";
			$q['params'][':issn'] = $this->issn;
			$q['params'][':support'] = Issn::SUPPORT_PAPIER;
		} elseif ($this->issne) {
			$q['where'][] = "Issn.issn = :issn AND Issn.support = :support";
			$q['params'][':issn'] = $this->issne;
			$q['params'][':support'] = Issn::SUPPORT_ELECTRONIQUE;
		} elseif ($this->issnl) {
			$q['where'][] = "Issn.issnl = :issnl";
			$q['params'][':issnl'] = $this->issnl;
		}

		if ($this->revue) {
			$sql = "SELECT DISTINCT t.revueId "
			. " FROM Titre t " . join(' ', $q['join'])
			. ($q['where'] ? " WHERE " . join(' AND ', $q['where']) : '');
			$ids = Yii::app()->db->createCommand($sql)->queryColumn($q['params']);
			if (!$ids) {
				return [];
			}
			$q = $qNoIssn;
			$q['where'][] = "t.revueId IN (" . join(',', $ids) . ')';
		}

		$sql = "SELECT " . join(', ', $q['select'])
			. " FROM Titre t " . join(' ', $q['join'])
			. ($q['where'] ? " WHERE " . join(' AND ', $q['where']) : '')
			. ' GROUP BY t.id';
		return Yii::app()->db->createCommand($sql)->queryAll(true, $q['params']);
	}

	protected function getRessourceIds()
	{
		if (empty($this->partenaire)) {
			return [];
		}
		return Yii::app()->db
			->createCommand(
				"SELECT cibleId FROM Suivi WHERE cible='Ressource' AND partenaireId = "
				. $this->partenaire
			)->queryColumn();
	}

	protected static function xmlForTitle($t)
	{
		$xml = sprintf(
			"\t<issn>%s</issn>\n\t<url>%s</url>\n\t<idmirabel>%d</idmirabel>\n",
			$t['issn'],
			CHtml::encode($t['url']),
			$t['id']
		);
		if (!empty($t['identifiantLocal'])) {
			$xml .= "\t<idpartenairerevue>" . CHtml::encode($t['identifiantLocal'])
				. "</idpartenairerevue>\n";
		}
		return $xml;
	}

	protected function xmlForService($s)
	{
		$couv = [];
		if ($s['lacunaire']) {
			$couv[] = 'Lacunaire';
		}
		if ($s['selection']) {
			$couv[] = "Sélection d'articles";
		}

		$template = '	<service>
		<id>%s</id>
		<nom>%s</nom>
		<acces>%s</acces>
		<type>%s</type>
		<couverture>%s</couverture>%s%s
		<urlservice>%s</urlservice>
		<urldirecte>%s</urldirecte>
		<debut>%s</debut>
		<fin>%s</fin>
	</service>
';
		if ($s['acces'] === 'Restreint' && $this->abonnementSearch) {
			$service = Service::model()->populateRecord($s);
			$acces = ($this->abonnementSearch->readStatus($service) === Abonnement::ABONNE ? 'Abonné' : 'Restreint');
		} else {
			$acces = $s['acces'];
		}
		$args = [
			$s['id'],
			htmlspecialchars($s['nom']),
			$acces,
			$s['type'],
			join(' - ', $couv),
			$s['lacunaire'] ? "\n\t\t<lacunaire />" : '',
			$s['selection'] ? "\n\t\t<selection />" : '',
			htmlspecialchars($s['rUrl']),
			htmlspecialchars($s['url']),
			$s['dateBarrDebut'],
			$s['dateBarrFin'],
		];
		return vsprintf($template, $args);
	}
}
