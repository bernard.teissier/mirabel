<?php

class m210104_130135_m4702_suppr_heloise extends \CDbMigration
{
	public function safeup(): bool
	{
		$heloise = Sourcelien::model()->findByAttributes(['nom' => "Héloïse"]);
		if (!$heloise) {
			echo "RIEN À FAIRE : Héloise n'existe pas dans les sources.\n";
			return true;
		}
		$titres = Titre::model()->findAllBySql(
			"SELECT t.* FROM Titre t JOIN LienTitre lt ON lt.titreId = t.id WHERE lt.sourceId = {$heloise->id} GROUP BY t.id"
		);
		foreach ($titres as $t) {
			/** @var Titre $t */
			$liens = json_decode($t->liensJson);
			$liensFilt = array_values(array_filter(
				$liens,
				function ($l) use($heloise) {
					return empty($l->sourceId) || ($l->sourceId != $heloise->id);
				}
			));
			if (count($liens) !== count($liensFilt)) {
				$i = $t->buildIntervention(true);
				$i->description = "Suppression du lien Héloïse";
				$i->contenuJson->updateByAttributes($t, ['liensJson' => json_encode($liensFilt)]);
				$i->contenuJson->isImport = true;
				$i->import = ImportType::getSourceId("heloise");
				$i->accept(true);
				echo "{$t->revueId}\t{$t->id}\tLien Héloïse supprimé\n";
			}
		}
		$this->delete("ImportedLink", "sourceId = {$heloise->id}");
		$this->delete("LienTitre", "sourceId = {$heloise->id}");
		$heloise->delete();
		return true;
	}

	public function safedown(): bool
	{
		echo "m210104_130135_m4702_suppr_heloise does not support migrating down.\n";
		return false;
	}
}
