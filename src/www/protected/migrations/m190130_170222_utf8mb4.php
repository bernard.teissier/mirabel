<?php

class m190130_170222_utf8mb4 extends CDbMigration
{
	/**
	 * Applies this migration.
	 */
	public function up()
	{
		$sqls = [
			"ALTER TABLE Titre CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci",
			"ALTER TABLE Titre MODIFY url varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL",
			"ALTER TABLE Titre MODIFY urlCouverture varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL",
			"ALTER TABLE Categorie CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci",
			"ALTER TABLE CategorieAlias CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci",
			"ALTER TABLE Cms
  MODIFY `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
, MODIFY `pageTitle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT ''
, MODIFY `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL
",
			"ALTER TABLE Editeur CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci",
			"ALTER TABLE Editeur MODIFY url varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL",
			"ALTER TABLE Editeur MODIFY logoUrl varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL",
			"ALTER TABLE Ressource CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci",
			"ALTER TABLE Ressource MODIFY url varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL",
			"ALTER TABLE Partenaire CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci",
			"ALTER TABLE Partenaire MODIFY url varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL",
			"ALTER TABLE Partenaire MODIFY logoUrl varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL",
			"ALTER TABLE Pays CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci",
			"ALTER TABLE Vocabulaire CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci",
		];
		foreach ($sqls as $sql) {
			$this->execute($sql);
		}
		if (in_array('Ville', $this->dbConnection->schema->tableNames)) {
			$this->execute("ALTER TABLE Ville CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci");
		}
	}

	/**
	 * Reverts this migration.
	 *
	 * @return bool False if it failed.
	 */
	public function down()
	{
		echo "m190130_170222_utf8mb4 does not support migrating down.\n";
		return false;
	}
}
