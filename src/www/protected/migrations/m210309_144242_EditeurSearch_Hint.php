<?php

class m210309_144242_EditeurSearch_Hint extends CDbMigration
{
	public function up()
	{
		$search = "<p>Recherche par dernière date de l'opération dans Mir@bel.</p>

<ul>
<li><code>2006</code> en 2006</li>
<li><code>&gt;2006</code> ou <code>2006-</code> : postérieur à janvier 2006 inclus</li>
<li><code>&lt;2006-09</code> ou <code>-2006-09</code> : antérieur à septembre 2006 inclus</li>
<li><code>2006 2008-09</code> : entre janvier 2006 et septembre 2008 inclus</li>
</ul>";
		$now = time();
		$this->insertMultiple(
			"Hint",
			[
				['model' => 'editeur/search', 'attribute' => 'idref', 'name' => 'IdRef', 'description' => "", 'hdateModif' => $now],
				['model' => 'editeur/search', 'attribute' => 'sherpa', 'name' => 'ID SHerpa-Romeo', 'description' => "", 'hdateModif' => $now],
				['model' => 'editeur/search', 'attribute' => 'nom', 'name' => 'Nom', 'description' => "", 'hdateModif' => $now],
				['model' => 'editeur/search', 'attribute' => 'pays', 'name' => 'Pays', 'description' => "", 'hdateModif' => $now],
				['model' => 'editeur/search', 'attribute' => 'vivants', 'name' => 'Nb titres vivants', 'description' => "", 'hdateModif' => $now],
				['model' => 'editeur/search', 'attribute' => 'hdateModif', 'name' => 'Date de modification', 'description' => $search, 'hdateModif' => $now],
				['model' => 'editeur/search', 'attribute' => 'hdateVerif', 'name' => 'Date de vérification', 'description' => $search, 'hdateModif' => $now],
			]
		);
	}

	public function down()
	{
		$this->delete("Hint", "model = 'editeur/search'");
		return true;
	}
}
