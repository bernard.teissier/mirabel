<?php

class m210329_071049_config_accueilvideo extends \CDbMigration
{
	public function safeUp()
	{
		$this->insert(
			'Cms',
			[
				'name' => "accueil-video",
				'singlePage' => 0,
				'type' => 'html',
				'content' => '<video controls preload="none" poster="/images/videos/presentation.png">
	<source src="/images/videos/presentation-HD.mp4" type="video/mp4">
	<source src="/images/videos/presentation-HD.webm" type="video/webm">
</video>',
				'hdateCreat' => time(),
			]
		);
		return true;
	}

	public function safeDown()
	{
		$this->delete("Cms", "name = 'accueil-video'");
		return true;
	}
}
