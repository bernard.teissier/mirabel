<?php

class m201216_084011_sherpa_fr_relationship extends \CDbMigration
{
	public function up(): bool
	{
		$this->insert(
			'Config',
			[
				'createdOn' => time(),
				'category' => 'sherpa',
				'type' => 'assoc',
				'description' => "<p>Traduction de la relation titre-éditeur (<code>publishers[].relationship_type</code>). <p>CSV à deux colonnes, avec sur chaque ligne :<pre>le texte de Sherpa à traduire;sa traduction en français.</pre>",
				'key' => 'sherpa.fr.relations',
				'value' =>
					<<<EOVALUE
					"associate_organisation";
					"client_organisation";
					"commercial_publisher";
					"copublisher";
					"copyright_holder";
					"former_imprint";
					"former_publisher";
					"governmental_publisher";
					"imprint";
					"independent_journal";
					"primary_copublisher";
					"society_publisher";
					"university_publisher";
					EOVALUE
			]
		);
		return true;
	}

	public function down(): bool
	{
		$this->delete('Config', "`key` = 'sherpa.fr.relations'");
		return true;
	}
}
