<?php

class m201013_102635_sherpa_config extends CDbMigration
{
	public function up()
	{
		$this->alterColumn("Config", "type", "enum('boolean','integer','float','string','text','html','json', 'assoc', 'csv') COLLATE ascii_bin NOT NULL");

		$this->insert(
			'Config',
			[
				'createdOn' => time(),
				'category' => 'sherpa',
				'type' => 'html',
				'description' => "HTML d'aide affichée à côté de la licence",
				'key' => 'sherpa.aide.licence',
				'value' => '<a href="https://creativecommons.fr/licences/">https://creativecommons.fr/licences/</a>',
			]
		);
		$this->insert(
			'Config',
			[
				'createdOn' => time(),
				'category' => 'sherpa',
				'type' => 'html',
				'description' => "HTML d'aide affichée à côté de l'embargo",
				'key' => 'sherpa.aide.embargo',
				'value' => '<a href="https://scienceouverte.couperin.org/versions-et-embargos/">https://scienceouverte.couperin.org/versions-et-embargos/</a>',
			]
		);
		$this->insert(
			'Config',
			[
				'createdOn' => time(),
				'category' => 'sherpa',
				'type' => 'html',
				'description' => "HTML d'aide affichée à côté des versions",
				'key' => 'sherpa.aide.versions',
				'value' => '<a href="https://scienceouverte.couperin.org/versions-et-embargos/">https://scienceouverte.couperin.org/versions-et-embargos/</a>',
			]
		);

		$this->insert(
			'Config',
			[
				'createdOn' => time(),
				'category' => 'sherpa',
				'type' => 'assoc',
				'description' => "CSV à deux colonnes, avec sur chaque ligne :<pre>le texte de Sherpa à traduire;sa traduction en français.</pre>",
				'key' => 'sherpa.fr.conditions',
				'value' => <<<EOVALUE
					"Must link to publisher version";"Un lien vers la version publiée doit apparaitre"
					"Must link to publisher version with DOI";"Le lien DOI vers la version publiée doit apparaitre"
					"Published source must be acknowledged with citation";"La version publiée doit être citée avec la source originale"
					"Publisher copyright and source must be acknowledged with citation";"Les droits d'éditeur et la source originale doivent apparaitre"
					"Set statement to accompany deposit";"Le détenteur des droits doit apparaitre"
					"Written permission must be obtained from the publisher";"Une autorisation écrite exprès doit être obtenue auprès de l'éditeur"
					EOVALUE
			]
		);
		$this->insert(
			'Config',
			[
				'createdOn' => time(),
				'category' => 'sherpa',
				'type' => 'assoc',
				'description' => "CSV à deux colonnes, avec sur chaque ligne :<pre>le texte de Sherpa à traduire;sa traduction en français.</pre>",
				'key' => 'sherpa.fr.copyrightowner',
				'value' => <<<EOVALUE
					"authors";"Auteur"
					"journal";"Revue"
					"learned_society";"Société savante"
					"publishers";"Éditeur"
					"shared_authors_and_journal";"Partagés entre l'auteur et la revue"
					"shared_authors_and_learned_society";"Partagés entre l'auteur et la société savante"
					"shared_authors_and_publishers";"Partagés entre l'auteur et l'éditeur"
					EOVALUE
			]
		);
		$this->insert(
			'Config',
			[
				'createdOn' => time(),
				'category' => 'sherpa',
				'type' => 'assoc',
				'description' => "CSV à deux colonnes, avec sur chaque ligne :<pre>le texte de Sherpa à traduire;HTML de sa traduction en français.</pre>",
				'key' => 'sherpa.fr.license',
				'value' => <<<EOVALUE
					cc_by;<a href="http://creativecommons.org/licenses/by/3.0/fr/" title="attribution">Creative Commons BY</a>
					cc_by_nc;<a href="http://creativecommons.org/licenses/by-nc/3.0/fr/" title="attribution + pas d'utilisation commerciale">Creative Commons BY NC</a>
					cc_by_nc_nd;<a href="http://creativecommons.org/licenses/by-nc-nd/3.0/fr/" title="attribution + pas d'utilisation commerciale + pas de modification">Creative Commons BY NC ND</a>
					cc_by_nc_sa;<a href="http://creativecommons.org/licenses/by-sa/3.0/fr/" title="attribution + pas d'utilisation commerciale + partage dans les mêmes conditions">Creative Commons BY NC SA</a>
					cc_by_sa;<a href="http://creativecommons.org/licenses/by-sa/3.0/fr/" title="attribution + partage dans les mêmes conditions">Creative Commons BY SA</a>
					cc_by_nd;<a href="http://creativecommons.org/licenses/by/3.0/fr/" title="attribution + pas de modification">Creative Commons BY ND</a>
					cc_gnu_gpl;<a ref="https://www.gnu.org/licenses/gpl-3.0.fr.html">GNU GPL</a>
					EOVALUE
			]
		);
		$this->insert(
			'Config',
			[
				'createdOn' => time(),
				'category' => 'sherpa',
				'type' => 'assoc',
				'description' => "CSV à deux colonnes, avec sur chaque ligne :<pre>le texte de Sherpa à traduire;sa traduction en français.</pre>",
				'key' => 'sherpa.fr.locations',
				'value' => <<<EOVALUE
					"academic_social_network";"Réseau social académique"
					"any_website";"Tout site web"
					"any_repository";"Toute archive"
					"authors_homepage";"Page personnelle de l'auteur"
					"funder_designated_location";"Dépôt désigné par le financeur"
					"institutional_repository";"Archive institutionnelle"
					"institutional_website";"Site web institutionnel"
					"named_academic_social_network";"Réseau social académique spécifique"
					"named_repository";"Archive spécifique"
					"non_commercial_social_network";"Réseau social non commercial"
					"non_commercial_website";"Site web non commercial"
					"non_commercial_institutional_repository";"Archive institutionnelle non commerciale"
					"non_commercial_repository";"Archive non commerciale "
					"non_commercial_subject_repository";"Archive thématique non commerciale"
					"preprint_repository";"Archive d'articles prépubliés"
					"subject_repository";"Archive thématique"
					"this_journal";"Accès ouvert sur le site de l'éditeur"
					EOVALUE
			]
		);
	}

	/**
	 * Reverts this migration.
	 *
	 * @return bool False if it failed.
	 */
	public function down()
	{
		$this->execute("DELETE FROM Config WHERE category = 'sherpa'");
		return true;
	}
}
