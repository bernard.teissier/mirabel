<?php

class m200415_094012_liens_cascade extends CDbMigration
{
	/**
	 * Applies this migration.
	 */
	public function up()
	{
		$this->dropForeignKey('fk_Editeur', "LienEditeur");
		$this->addForeignKey("fk_Editeur", "LienEditeur", "editeurId", "Editeur", "id", "CASCADE", "CASCADE");

		$this->dropForeignKey('fk_Titre', "LienTitre");
		$this->addForeignKey("fk_Titre", "LienTitre", "titreId", "Titre", "id", "CASCADE", "CASCADE");
	}

	/**
	 * Reverts this migration.
	 *
	 * @return bool False if it failed.
	 */
	public function down()
	{
		return true;
	}
}
