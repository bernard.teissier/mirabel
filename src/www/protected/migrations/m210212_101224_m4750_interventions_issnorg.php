<?php

class m210212_101224_m4750_interventions_issnorg extends \CDbMigration
{
	public function safeUp()
	{
		$this->execute(
			<<<EOSQL
			UPDATE Intervention JOIN Titre t ON Intervention.titreId = t.id
			  SET description = CONCAT('Modification du titre « ', t.titre, ' »')
			  WHERE Intervention.import = 18 AND Intervention.description = ''
			EOSQL
		);
		return true;
	}

	public function safeDown()
	{
		return true;
	}
}
