<?php

class m201125_171028_m4692_nettoie_sourcelien extends CDbMigration
{
	public function safeUp()
	{
		$sqls = [
			"UPDATE Sourcelien SET url = 'pinterest.com' WHERE id = 6",
			"UPDATE Sourcelien SET url = 'jurisguide.fr' WHERE id = 16",
			"UPDATE Sourcelien SET url = 'scoop.it' WHERE id = 20",
			"UPDATE Sourcelien SET url = 'isidore.science' WHERE id = 24",
			"UPDATE Sourcelien SET url = 'miar.ub.edu' WHERE id = 26",
			"UPDATE Sourcelien SET url = 'cairn-int.info' WHERE id = 27",
			"UPDATE Sourcelien SET url = 'entrevues.org' WHERE id = 29",
			"UPDATE Sourcelien SET url = 'scopus.com' WHERE id = 30",
			"UPDATE Sourcelien SET url = 'v2.sherpa.ac.uk' WHERE id = 31",
		];
		foreach ($sqls as $sql) {
			$this->execute($sql);
		}
		return true;
	}

	public function safeDown()
	{
		echo "m201125_171028_m4692_nettoie_sourcelien does not support migrating down.\n";
		return false;
	}
}
