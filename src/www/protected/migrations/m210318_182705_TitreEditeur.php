<?php

class m210318_182705_TitreEditeur extends \CDbMigration
{
	public function up(): bool
	{
		$this->addColumn("Titre_Editeur", "ancien", "BOOLEAN NULL DEFAULT NULL");
		$this->addColumn("Titre_Editeur", "commercial", "BOOLEAN NOT NULL DEFAULT 0");
		$this->addColumn("Titre_Editeur", "intellectuel", "BOOLEAN NOT NULL DEFAULT 0");
		$this->addColumn("Titre_Editeur", "role", "VARCHAR(255) COLLATE ascii_bin NULL DEFAULT NULL");
		if (!$this->getDbConnection()->getSchema()->getTable("Titre_Editeur")->getColumn("hdateModif")) {
			$this->addColumn("Titre_Editeur", "hdateModif", "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");
		}

		// ancien=0 if single Editeur for this Titre
		$this->execute("CREATE TEMPORARY TABLE Single SELECT titreId, MAX(editeurId) AS editeurId FROM `Titre_Editeur` GROUP BY titreId HAVING count(*) = 1");
		$this->execute("UPDATE `Titre_Editeur` t JOIN Single s ON s.titreId = t.titreId AND s.editeurId = t.editeurId SET ancien = 0");

		$this->insert(
			"Config",
			[
				'key' => 'sherpa.publisher_role',
				'type' => 'assoc',
				'value' =>
					<<<EOCSV
					associate_organisation; "organisation associée"
					client_organisation; "organisation bénéficiaire"
					commercial_publisher; "éditeur commercial"
					copublisher; "co-éditeur"
					copyright_holder; "détenteur des droits"
					governmental_publisher; "organisme gouvernemental"
					imprint; "marque commerciale"
					independent_journal; "revue auto-éditée"
					primary_copublisher; "co-éditeur principal"
					society_publisher; "société académique"
					university_publisher; "organisme universitaire"
					EOCSV,
				'category' => 'sherpa',
				'description' =>
					<<<EOHTML
					<p>Liste des valeurs possibles pour décrire le <strong>rôle d'un éditeur sur un titre</strong>.</p>
					<p>
					La première colonne contient les noms internes, stockés en base de données (initialement une sélection parmi les valeurs utilisées par Sherpa).
					La seconde colonne donne les correspondances en français, affichées dans les pages web.
					</p>
					<p>
					Si une ligne est retirée, les enregistrements déjà présents ne seront pas modifiés,
					mais ils afficheront la valeur brute, et non la correspondance en français.
					</p>
					EOHTML,
				'required' => 0,
				'createdOn' => time(),
				'updatedOn' => time(),
			]
		);

		$this->insert(
			"Hint",
			[
				'model' => 'Titre',
				'attribute' => 'editeurs',
				'name' => "Éditeurs du titre",
				'description' => '',
				'hdateModif' => time(),
			]
		);
		return true;
	}

	public function down(): bool
	{
		$this->delete("Hint", "model = 'Titre' AND attribute = 'editeurs'");
		$this->delete("Config", "`key` = 'sherpa.publisher_role'");

		$this->dropColumn("Titre_Editeur", "ancien");
		$this->dropColumn("Titre_Editeur", "commercial");
		$this->dropColumn("Titre_Editeur", "intellectuel");
		$this->dropColumn("Titre_Editeur", "role");
		return true;
	}
}
