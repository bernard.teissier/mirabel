<?php

class m200928_174022_Sourcelien_multi_wikipedia extends CDbMigration
{
	private $langs = [
		"ar" => "arabe",
		"ca" => "catalan",
		"cs" => "tchèque",
		"de" => "allemand",
		"el" => "grec",
		"en" => "anglais",
		"es" => "espagnol",
		"et" => "estonien",
		"eu" => "basque",
		"gl" => "galicien",
		"he" => "hébreu",
		"hu" => "hongrois",
		"it" => "italien",
		"nl" => "néerlandais",
		"no" => "norvégien",
		"pl" => "polonais",
		"pt" => "portugais",
		"ro" => "roumain",
		"ru" => "russe",
		"wa" => "wallon",
		"zh" => "chinois",
	];

	/**
	 * Applies this migration.
	 */
	public function up()
	{
		$this->update(
			'Sourcelien',
			[
				'nom' => 'Wikipedia (français)',
				'nomlong' => 'Encyclopédie libre',
				'nomcourt' => 'wikipedia-fr',
				'url' => 'fr.wikipedia.org',
				'hdateModif' => time(),
			],
			"nomcourt = 'wikipedia'"
		);

		foreach ($this->langs as $alpha2 => $langue) {
			$this->insert(
				'Sourcelien',
				[
					'nom' => "Wikipedia ($langue)",
					'nomlong' => "Encyclopédie libre",
					'nomcourt' => "wikipedia-{$alpha2}",
					'url' => "{$alpha2}.wikipedia.org",
					'saisie' => 1,
					'hdateCrea' => time(),
					'hdateModif' => time(),
				]
			);
		}
		return true;
	}

	/**
	 * Reverts this migration.
	 *
	 * @return bool False if it failed.
	 */
	public function down()
	{
		$this->update('Sourcelien', ['nomcourt' => 'wikipedia'], "nomcourt = 'wikipedia-fr'");
		foreach (array_keys($this->langs) as $alpha2) {
			$this->delete('Sourcelien', "nomcourt = 'wikipedia-{$alpha2}'");
		}
		return true;
	}
}
