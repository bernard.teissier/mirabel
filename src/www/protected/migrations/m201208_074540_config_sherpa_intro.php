<?php

class m201208_074540_config_sherpa_intro extends CDbMigration
{
	public function up(): bool
	{
		$this->insert(
			'Config',
			[
				'createdOn' => time(),
				'category' => 'sherpa',
				'type' => 'html',
				'description' => "HTML affiché en haut de la page pour introduire les données de Sherpa.",
				'key' => 'sherpa.intro',
				'value' => <<<EOHTML
					Mir@bel affiche les données issues de la base <a href="https://v2.sherpa.ac.uk/romeo/about.html">Sherpa Romeo</a>, adaptées en français.
					Pour comprendre ce qu'est l'<em>Accès ouvert</em>, référez-vous aux
					<a href="https://scienceouverte.couperin.org/category/open-access/">pages dédiées sur le site Science ouverte de Couperin</a>.
					EOHTML
			]
		);
		return true;
	}

	public function down(): bool
	{
		$this->delete('Config', "`key` = 'sherpa.intro'");
		return true;
	}
}
