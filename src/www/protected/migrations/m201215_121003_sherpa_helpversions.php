<?php

class m201215_121003_sherpa_helpversions extends \CDbMigration
{
	public function safeUp()
	{
		$time = time();
		$sqls = [
			<<<EOSQL
			INSERT INTO Config
				SELECT NULL, 'sherpa.aide.version.accepted' AS k, c.value, c.type, c.category, 'HTML d´aide affiché à côté de la version acceptée' AS description, 0, $time, null
				FROM Config c WHERE c.key = 'sherpa.aide.versions'
			EOSQL
			,
			<<<EOSQL
			INSERT INTO Config
				SELECT NULL, 'sherpa.aide.version.published' AS k, c.value, c.type, c.category, 'HTML d´aide affiché à côté de la version publiée' AS description, 0, $time, null
				FROM Config c WHERE c.key = 'sherpa.aide.versions'
			EOSQL
			,
			<<<EOSQL
			INSERT INTO Config
				SELECT NULL, 'sherpa.aide.version.submitted' AS k, c.value, c.type, c.category, 'HTML d´aide affiché à côté de la version soumise' AS description, 0, $time, null
				FROM Config c WHERE c.key = 'sherpa.aide.versions'
			EOSQL
			,
			"DELETE FROM Config WHERE `key` = 'sherpa.aide.versions'",
			"UPDATE Config SET description = '<p>HTML affiché en haut de la page pour introduire les données de Sherpa.</p> "
				. "<p><b>{{SHERPA_URL}}</b> sera remplacé par l´URL du titre dans Sherpa-Romeo, et <b>{{SHERPA_LINK xyz}}</b> sera remplacé par un lien avec ce texte xyz.</p>'"
				. " WHERE `key` = 'sherpa.intro'",
		];
		foreach ($sqls as $sql) {
			$this->execute($sql);
		}
		return true;
	}

	public function safeDown()
	{
		echo "m201215_121003_sherpa_helpversions does not support migrating down.\n";
		return false;
	}
}
