<?php

class m201125_150304_m4694_cms_content extends CDbMigration
{
	public function up()
	{
		$this->alterColumn("Cms", "content", "MEDIUMTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL");
		return true;
	}

	public function down()
	{
		echo "m201125_150304_m4694_cms_content does not support migrating down.\n";
		return false;
	}
}
