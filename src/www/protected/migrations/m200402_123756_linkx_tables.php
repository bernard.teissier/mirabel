<?php

class m200402_123756_linkx_tables extends CDbMigration
{
	/**
	 * Applies this migration.
	 */
	public function up()
	{
		$this->create("Titre");
		$liensTitres = Yii::app()->db
			->createCommand("SELECT id, liensJson FROM Titre WHERE liensJson <> '' ORDER BY id ASC");
		$count = 0;
		$titres = 0;
		foreach ($liensTitres->query() as $row) {
			$titres++;
			$expected = substr_count($row['liensJson'], '"url":');
			$liens = new Liens();
			try {
				$liens->setContent($row['liensJson']);
				$saved = $liens->save('Titre', (int) $row['id']);
				if ($saved != $expected) {
					echo $row['id'] . ": saved $saved / $expected\n";
				}
				$count += $saved;
			} catch (\Exception $e) {
				echo "Ignored a link, titreId={$row['id']}: {$e->getMessage()}\n";
			}
		}
		echo "LienTitre: $count links inserted for $titres titles.\n";

		$this->create("Editeur");
		$liensEditeurs = Yii::app()->db
			->createCommand("SELECT id, liensJson FROM Editeur WHERE liensJson <> '' ORDER BY id ASC");
		$count = 0;
		foreach ($liensEditeurs->query() as $row) {
			$liens = new Liens();
			try {
				$liens->setContent($row['liensJson']);
				$count += count($liens->getContent());
				$liens->save('Editeur', (int) $row['id']);
			} catch (\Exception $e) {
				echo "Ignored a link, editeurId={$row['id']}: {$e->getMessage()}\n";
			}
		}
		echo "LienEditeur: $count links inserted.\n";
	}

	/**
	 * Reverts this migration.
	 *
	 * @return bool False if it failed.
	 */
	public function down()
	{
		$this->dropTable("LienEditeur");
		$this->dropTable("LienTitre");
		return true;
	}

	private function create($baseTable)
	{
		$table = "Lien$baseTable";
		$fk = strtolower($baseTable) . "Id";
		$this->createTable(
			$table,
			[
				'id' => "BIGINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT",
				$fk => "INT UNSIGNED NOT NULL COMMENT 'FK $baseTable.id'",
				'sourceId' => "INT UNSIGNED NULL DEFAULT NULL",
				'domain' => "VARBINARY(255) NOT NULL COMMENT 'URL domain'",
				'name' => "VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci",
				'url' => "VARBINARY(511) NOT NULL",
				'hdate' => "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP",
			]
		);
		$this->addForeignKey("fk_$baseTable", $table, $fk, $baseTable, "id");
		$this->addForeignKey("fk_source_$table", $table, "sourceId", "Sourcelien", "id");
		$this->createIndex("idx_domain_$table", $table, 'domain');
	}
}
