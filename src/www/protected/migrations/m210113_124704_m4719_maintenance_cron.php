<?php

class m210113_124704_m4719_maintenance_cron extends \CDbMigration
{
	public function up(): bool
	{
        if (Config::read('maintenance.cron.arret') !== null) {
            return true;
        }
		$this->insert(
			"Config",
			[
				'key' => 'maintenance.cron.arret',
				'value' => '0',
				'type' => 'boolean',
				'category' => 'maintenance',
				'description' => "La valeur '1 / Oui' bloque l'exécution de la plupart des commandes du cron. '0 / Non' est le fonctionnement normal.",
				'required' => 0,
				'createdOn' => time(),
			]
		);
		return true;
	}

	public function down(): bool
	{
		echo "m210113_124704_m4719_maintenance_cron does not support migrating down.\n";
		return false;
	}
}
