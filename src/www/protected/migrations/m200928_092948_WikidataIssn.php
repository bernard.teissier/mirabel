<?php

class m200928_092948_WikidataIssn extends CDbMigration
{
	/**
	 * Applies this migration.
	 */
	public function up()
	{
		if (!$this->getDbConnection()->getSchema()->getTable('WikidataIssn')) {
			$this->createTable(
				"WikidataIssn",
				[
					'id' => "INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY",
					"qId" => "VARCHAR(20) DEFAULT '' NOT NULL",
					"issn" => "CHAR(9) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL",
					'revueId' => "INT UNSIGNED NULL DEFAULT NULL",
					"titre" => "VARCHAR(1024) DEFAULT '' NOT NULL",
				],
				"ENGINE=InnoDB"
			);
		}
		return true;
	}

	/**
	 * Reverts this migration.
	 *
	 * @return bool False if it failed.
	 */
	public function down()
	{
		$this->dropTable('WikidataIssn');
		return true;
	}
}
