<?php

class m210112_182146_titre_editeur_cleanup extends \CDbMigration
{
	public function up(): bool
	{
		$sqls = [
			// The following line works with MariaDB 10.5
			//"DELETE FROM Titre_Editeur WHERE (titreId, editeurId) IN (SELECT te.titreId, te.editeurId FROM `Titre_Editeur` te LEFT JOIN Titre t ON t.id = te.`titreId` WHERE t.id IS NULL)",
			//"DELETE FROM Titre_Editeur WHERE (titreId, editeurId) IN (SELECT te.titreId, te.editeurId FROM `Titre_Editeur` te LEFT JOIN Editeur e ON e.id = te.`editeurId` WHERE e.id IS NULL)",

			// The following lines should work with MariaDB 10.3
			"CREATE TEMPORARY TABLE cleanup (SELECT te.titreId, te.editeurId FROM `Titre_Editeur` te LEFT JOIN Titre t ON t.id = te.`titreId` WHERE t.id IS NULL)",
			"INSERT INTO cleanup SELECT te.titreId, te.editeurId FROM `Titre_Editeur` te LEFT JOIN Editeur e ON e.id = te.`editeurId` WHERE e.id IS NULL",
			"DELETE FROM Titre_Editeur WHERE (titreId, editeurId) IN (SELECT * FROM cleanup)",
		];
		foreach ($sqls as $sql) {
			$this->execute($sql);
		}

		$this->addForeignKey('te_editeur_fk', 'Titre_Editeur', 'titreId', 'Titre', 'id', 'CASCADE');
		$this->addForeignKey('te_titre_fk', 'Titre_Editeur', 'editeurId', 'Editeur', 'id', 'CASCADE');
		return true;
	}

	public function down(): bool
	{
		$this->dropForeignKey('te_titre_fk', 'Titre_Editeur');
		$this->dropForeignKey('te_editeur_fk', 'Titre_Editeur');
		return true;
	}
}
