<?php

class m210212_095555_m4763_editeur extends \CDbMigration
{
	public function up(): bool
	{
		$this->dropColumn("Editeur", "nbPublications");
		$this->dropColumn("Editeur", "nbPublicationsDate");
		$this->dropColumn("Editeur", "specialite");
		return true;
	}

	public function down(): bool
	{
		echo "m210212_095555_m4763_editeur does not support migrating down.\n";
		return false;
	}
}
