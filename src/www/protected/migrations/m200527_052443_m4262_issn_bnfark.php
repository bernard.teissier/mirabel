<?php

class m200527_052443_m4262_issn_bnfark extends CDbMigration
{
	public function safeUp()
	{
		$this->addColumn("Issn", "bnfArk", "VARCHAR(255) COLLATE ascii_bin DEFAULT NULL COMMENT 'identifiant ARK attribué par la BNF, sans son préfixe 12148/ (autorité BNF)'");
		return true;
	}

	public function safeDown()
	{
		$this->dropColumn("Issn", "bnfArk");
		return true;
	}
}
