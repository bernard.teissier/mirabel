<?php

class m210111_180529_Config_maintenance extends \CDbMigration
{
	/**
	 * Applies this migration.
	 *
	 * @return bool False if it failed.
	 */
	public function up(): bool
	{
        if (Config::read('maintenance.authentification.inactive') !== null) {
            return true;
        }
		$this->insert("Config", [
			'key' => 'maintenance.authentification.inactive',
			'value' => "0",
			'type' => 'boolean',
			'category' => 'maintenance',
			'description' => "Désactiver l'authentification des utilisateurs non administrateurs.",
			'required' => 0,
			'createdOn' => time(),
		]);

		$this->insert("Config", [
			'key' => 'maintenance.message.invite',
			'value' => "0",
			'type' => 'boolean',
			'category' => 'maintenance',
			'description' => "Affiche le message de maintenance pour les invités (utilisateurs non authentifiés).",
			'required' => 0,
			'createdOn' => time(),
		]);

		$this->insert("Config", [
			'key' => 'maintenance.message.authentifie',
			'value' => "0",
			'type' => 'boolean',
			'category' => 'maintenance',
			'description' => "Affiche le message de maintenance pour les utilisateurs authentifiés.",
			'required' => 0,
			'createdOn' => time(),
		]);

		$this->insert("Config", [
			'key' => 'maintenance.message.texte',
			'value' => "",
			'type' => 'html',
			'category' => 'maintenance',
			'description' => "Texte en HTML du message de maintenance.",
			'required' => 0,
			'createdOn' => time(),
		]);

		return true;
	}

	/**
	 * Reverts this migration.
	 *
	 * @return bool False if it failed.
	 */
	public function down(): bool
	{
		$this->delete('Config', ['category' => 'maintenance']);
		return true;
	}
}
