<?php

class m200701_092643_Wikidata_comparison extends CDbMigration
{
	/**
	 * Applies this migration.
	 */
	public function up()
	{
		if ($this->getDbConnection()->schema->getTable('Wikidata')->getColumn('difference')) {
			$this->renameColumn('Wikidata', 'difference', 'comparison');
			$this->renameColumn('Wikidata', 'reference', 'compUrl');
			$this->renameColumn('Wikidata', 'details', 'compDetails');

			$this->addColumn('Wikidata', 'titreId', "INT UNSIGNED NOT NULL AFTER `revueId` ");
		}
		return true;
	}

	/**
	 * Reverts this migration.
	 *
	 * @return bool False if it failed.
	 */
	public function down()
	{
		$this->dropColumn('Wikidata', 'titreId');

		$this->renameColumn('Wikidata', 'comparison', 'difference');
		$this->renameColumn('Wikidata', 'compUrl', 'reference');
		$this->renameColumn('Wikidata', 'compDetails', 'details');

		return true;
	}
}
