<?php

class m201008_100331_WikidataIssn_hdate extends CDbMigration
{
	/**
	 * Applies this migration.
	 */
	public function up()
	{
		$this->addColumn('WikidataIssn', 'hdate', "INT UNSIGNED NULL DEFAULT NULL AFTER `titre` ");
		return true;
	}

	/**
	 * Reverts this migration.
	 *
	 * @return bool False if it failed.
	 */
	public function down()
	{
		$this->dropColumn('WikidataIssn', 'hdate');
		return true;
	}
}
