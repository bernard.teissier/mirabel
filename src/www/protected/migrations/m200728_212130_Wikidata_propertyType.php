<?php

class m200728_212130_Wikidata_propertyType extends CDbMigration
{
	/**
	 * Applies this migration.
	 */
	public function up()
	{
		$this->addColumn('Wikidata', 'propertyType', "INT UNSIGNED NOT NULL AFTER `property` ");
		return true;
	}

	/**
	 * Reverts this migration.
	 *
	 * @return bool False if it failed.
	 */
	public function down()
	{
		$this->dropColumn('Wikidata', 'propertyType');
		return true;
	}
}
