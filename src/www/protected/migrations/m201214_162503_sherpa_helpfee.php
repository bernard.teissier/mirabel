<?php

class m201214_162503_sherpa_helpfee extends \CDbMigration
{
	public function up(): bool
	{
		$this->insert(
			'Config',
			[
				'createdOn' => time(),
				'category' => 'sherpa',
				'type' => 'html',
				'description' => "HTML d'aide affichée à côté des frais additionnels",
				'key' => 'sherpa.aide.frais',
				'value' => '',
			]
		);
		return true;
	}

	public function down(): bool
	{
		$this->delete('Config', "`key` = 'sherpa.aide.frais'");
		return true;
	}
}
