<?php

class m200429_142643_Wikidata_table extends CDbMigration
{
	/**
	 * Applies this migration.
	 */
	public function up()
	{
		if (!$this->getDbConnection()->getSchema()->getTable('Wikidata')) {
			$this->createTable(
				"Wikidata",
				[
					'id' => "INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY",
					'revueId' => "INT UNSIGNED NOT NULL",
					"qId" => "VARCHAR(20) DEFAULT '' NOT NULL",
					"property" => "VARCHAR(20) DEFAULT '' NOT NULL",
					"value" => "VARCHAR(4095) DEFAULT '' NOT NULL",
					'url' => "VARCHAR(4095) DEFAULT '' NOT NULL",
					'hdate' => "INT UNSIGNED NULL DEFAULT NULL",
					'difference' => "INT UNSIGNED NULL DEFAULT NULL",
					'reference' => "VARCHAR(511) DEFAULT '' NOT NULL COMMENT 'valeur de référence dans Mirabel à comparer'",
					'details' => "VARCHAR(500) DEFAULT '' NOT NULL COMMENT 'url interne pour pointer sur des détails'",
				],
				"ENGINE=InnoDB"
			);
		}
		// NON ! Wikidata peut contenir des numéros de revue fausses et il faut les garder dans le cache pour les analyser
		// $this->addForeignKey('wikidata_revue_fk', "Wikidata", "revueId", "Revue", "id");
		return true;
	}

	/**
	 * Reverts this migration.
	 *
	 * @return bool False if it failed.
	 */
	public function down()
	{
		$this->dropTable("Wikidata");
		return true;
	}
}
