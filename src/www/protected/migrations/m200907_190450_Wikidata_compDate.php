<?php

class m200907_190450_Wikidata_compDate extends CDbMigration
{
	/**
	 * Applies this migration.
	 */
	public function up()
	{
		$this->addColumn('Wikidata', 'compDate', "INT UNSIGNED NULL DEFAULT NULL AFTER `compDetails` ");
		return true;
	}

	/**
	 * Reverts this migration.
	 *
	 * @return bool False if it failed.
	 */
	public function down()
	{
		$this->dropColumn('Wikidata', 'compDate');
		return true;
	}
}
