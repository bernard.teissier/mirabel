<?php

class m210210_130025_m4706_utilisateur_listediffusion extends \CDbMigration
{
	public function up(): bool
	{
		$this->addColumn("Utilisateur", "listeDiffusion", "BOOL NOT NULL DEFAULT 0 COMMENT 'liste de diffusion mirabel-contenu'");
		$this->insert(
			"Hint",
			[
				'model' => 'Utilisateur',
				'attribute' => 'listeDiffusion',
				'name' => 'Liste de diffusion',
				'description' => '',
				'hdateModif' => time(),
			]
		);
		$this->execute("UPDATE Utilisateur JOIN Partenaire p ON Utilisateur.partenaireId = p.id SET listeDiffusion = 1 WHERE p.type = 'normal'");
		return true;
	}

	public function down(): bool
	{
		$this->delete("Hint", "model = 'Utilisateur' AND attribute = 'listeDiffusion'");
		$this->dropColumn("Utilisateur", "listeDiffusion");
		return true;
	}
}
