<?php

class m210204_174036_editeur_idref extends \CDbMigration
{
	public function up(): bool
	{
		$this->addColumn("Editeur", "idref", "VARCHAR(9) NULL COLLATE ascii_bin AFTER presentation");
		$this->insert(
			"Hint",
			[
				'model' => 'Editeur',
				'attribute' => 'idref',
				'name' => 'idref',
				'description' => '',
				'hdateModif' => time(),
			]
		);
		return true;
	}

	public function down(): bool
	{
		$this->dropColumn("Editeur", "idref");
		$this->delete("Hint", "model = 'Editeur' AND name = 'idref'");
		return true;
	}
}
