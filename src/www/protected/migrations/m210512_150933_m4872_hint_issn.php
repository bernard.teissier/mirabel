<?php

class m210512_150933_m4872_hint_issn extends \CDbMigration
{
	public function safeUp()
	{
		$now = time();
		$this->insertMultiple("Hint", [
			[
				'model' => 'Issn',
				'attribute' => "statut",
				'name' => "Statut",
				'description' => "",
				'hdateModif' => $now,
			],
			[
				'model' => 'Issn',
				'attribute' => "pays",
				'name' => "Pays notice ISSN",
				'description' => "",
				'hdateModif' => $now,
			],
			[
				'model' => 'Issn',
				'attribute' => "bnfArk",
				'name' => "BNF Ark",
				'description' => "",
				'hdateModif' => $now,
			],
		]);
		return true;
	}

	public function safeDown()
	{
		return false;
	}
}
