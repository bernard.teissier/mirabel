<?php

class m201011_221944_Wikidata_CompQid extends CDbMigration
{
	/**
	 * Applies this migration.
	 */
	public function up()
	{
		$this->addColumn('Wikidata', 'compQid', "INT UNSIGNED NULL DEFAULT NULL AFTER `compDate` ");
		return true;
	}

	/**
	 * Reverts this migration.
	 *
	 * @return bool False if it failed.
	 */
	public function down()
	{
		$this->dropColumn('Wikidata', 'CompQid');
		return true;
	}
}
