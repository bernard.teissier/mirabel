<?php

class m190918_143213_categorie_dates extends CDbMigration
{
	/**
	 * Applies this migration.
	 */
	public function up()
	{
		$sqls = [
			// remove auto update
			"ALTER TABLE Categorie CHANGE `hdateCreation` `hdateCreation` timestamp NOT NULL DEFAULT current_timestamp()",
			// swap timestamps
			"UPDATE Categorie c1, Categorie c2 SET c1.hdateModif = c1.hdateCreation, c1.hdateCreation = c2.hdateModif WHERE c1.id = c2.id AND c1.hdateModif < c1.hdateCreation",
		];
		foreach ($sqls as $sql) {
			$this->execute($sql);
		}
	}

	/**
	 * Reverts this migration.
	 *
	 * @return bool False if it failed.
	 */
	public function down()
	{
		echo "m190918_143213_categorie_dates does not support migrating down.\n";
		return false;
	}
}
