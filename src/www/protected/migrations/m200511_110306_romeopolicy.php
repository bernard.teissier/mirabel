<?php

class m200511_110306_romeopolicy extends CDbMigration
{
	public function up()
	{
		$this->createTable(
			"RomeoPolicy",
			[
				"titreId INT UNSIGNED NOT NULL PRIMARY KEY",
				"submitted TINYINT UNSIGNED NOT NULL DEFAULT 0",
				"accepted TINYINT UNSIGNED NOT NULL DEFAULT 0",
				"published TINYINT UNSIGNED NOT NULL DEFAULT 0",
			]
		);
		$this->addForeignKey("fk_romeo_titre", "RomeoPolicy", "titreId", "Titre", "id", "CASCADE", "CASCADE");

		if (!$this->dbConnection->createCommand("SELECT 1 FROM Sourcelien WHERE nomcourt = 'romeo'")->queryScalar()) {
			$this->insert("Sourcelien", [
				'nom' => "RoMEO",
				'nomlong' => "Sherpa Romeo",
				'nomcourt' => 'romeo',
				'url' => 'https://v2.sherpa.ac.uk/romeo/',
				'saisie' => 0,
				'hdateCrea' => $_SERVER['REQUEST_TIME'],
				'hdateModif' => $_SERVER['REQUEST_TIME'],
			]);
		}
	}

	public function down()
	{
		$this->dropTable("RomeoPolicy");
		return true;
	}
}
