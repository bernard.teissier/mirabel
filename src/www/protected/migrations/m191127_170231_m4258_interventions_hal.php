<?php

class m191127_170231_m4258_interventions_hal extends CDbMigration
{
	/**
	 * Applies this migration.
	 */
	public function up()
	{
		$sqls = [
			"UPDATE Intervention SET import = 13"
			. " WHERE hdateVal BETWEEN unix_timestamp('2019-04-04 02:00:00') AND unix_timestamp('2019-04-04 05:00:00') AND import = 0 AND ip =''",
		];
		foreach ($sqls as $sql) {
			$this->execute($sql);
		}
	}

	/**
	 * Reverts this migration.
	 *
	 * @return bool False if it failed.
	 */
	public function down()
	{
		return true;
	}
}
