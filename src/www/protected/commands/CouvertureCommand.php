<?php

/**
 * Handle Journal pictures.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class CouvertureCommand extends CConsoleCommand
{
	public $verbose = 1;

	public $limit = 0;

	public $maj = false;

	public $majMemeSource = false;

	public $interventions = 0;

	/**
	 * Utilise l'ID interne attribué par Persée pour en déduire la couverture.
	 */
	public function actionPersee()
	{
		$persee = Ressource::model()->findByAttributes(['identifiant' => 'Persée']);
		if ($this->maj) {
			$critCouv = "";
		} elseif ($this->majMemeSource) {
			$critCouv = "(t.urlCouverture = '' OR  t.urlCouverture LIKE 'http://www.persee.fr/%') AND ";
		} else {
			$critCouv = "t.urlCouverture = '' AND ";
		}
		$titres = Titre::model()->findAllBySql(
			"SELECT t.*, i.idInterne AS confirm"
			. " FROM Titre t"
			. " JOIN Identification i ON i.titreId = t.id AND i.ressourceId = {$persee->id}"
			. " WHERE $critCouv i.idInterne <> ''"
			. " GROUP BY t.id"
		);
		$count = 0;
		foreach ($titres as $titre) {
			$count++;
			$code = $titre->confirm;
			unset($titre->confirm);
			if ($this->limit && $count > $this->limit) {
				if ($this->verbose) {
					echo "Limit reached\n";
				}
				break;
			}
			$newUrl = 'http://www.persee.fr/renderCollectionCover/' . $code . '.png';
			if ($this->verbose) {
				printf("%4d;%s;%s;%s;", $titre->id, $titre->titre, $newUrl, $titre->obsoletePar ? 'obsolète' : '');
			}
			$success = $this->updateCouverture($titre->attributes, $newUrl, 'http://www.persee.fr/renderCollectionCover/');
			if ($this->verbose) {
				echo $success . "\n";
			}
		}
	}

	/**
	 * Utilise un fichier CSV fourni par Revues.org, sans intervention.
	 * En-tête attendu : titre en col 0, URL-titre en col 9, URL-couverture en col 14
	 *
	 * @param string $file Chemin vers le CSV, par défaut URL chez openedition.org
	 * @param string $separator Séparateur de colonnes, par défaut ","
	 */
	public function actionRevuesOrgCsv($file = 'http://www.openedition.org/index.html?page=coverage&pubtype=revue&export=csv', $separator = ',')
	{
		$csv = file_get_contents($file);
		$revuesOrg = Ressource::model()->findByAttributes(['identifiant' => 'Revues.org']);
		$findTitre = Yii::app()->db->createCommand(
			"SELECT t.* FROM Titre t JOIN Service s ON t.id = s.titreId "
			. " WHERE s.ressourceId = {$revuesOrg->id} AND TRIM(TRAILING '/' FROM s.url) = :url LIMIT 1"
		);
		$count = 0;
		foreach (explode("\n", $csv) as $line) {
			$row = str_getcsv($line, $separator, '"');
			$count++;
			if ($this->limit && $count > $this->limit) {
				if ($this->verbose) {
					echo "Limit reached\n";
				}
				break;
			}
			if (count($row) > 13 && preg_match('/\.(png|jpe?g|gif)$/', $row[14])) {
				if ($this->verbose) {
					printf("%s;%s;%s", $row[0], $row[9], $row[14]);
				}
				$titre = $findTitre->queryRow(true, [':url' => rtrim($row[9], '/')]);
				if ($titre) {
					$success = $this->updateCouverture($titre, $row[14], 'http://www.openedition.org/');
					$success .= ';' . $titre['id'] . ($titre['obsoletePar'] ? ';obsolète' : ';');
				} else {
					$success = "ERREUR, titre non trouvé;;";
				}
				if ($this->verbose) {
					echo ";" . $success . "\n";
				}
			}
		}
	}

	/**
	 * Utilise l'ISSN-e de la revue pour affecter une couverture chez Erudit.org, sans intervention.
	 */
	public function actionErudit()
	{
		$erudit = Ressource::model()->findByAttributes(['identifiant' => 'Erudit']);
		$cmd = Yii::app()->db->createCommand(
			"SELECT t.*, i.issne"
			. " FROM Titre t"
			. " JOIN Identification i ON i.titreId = t.id AND i.ressourceId = {$erudit->id}"
			. " WHERE i.issne <> ''"
			. " GROUP BY t.id"
		);
		$count = 0;
		foreach ($cmd->queryAll() as $row) {
			$count++;
			if ($this->limit && $count > $this->limit) {
				if ($this->verbose) {
					echo "Limit reached\n";
				}
				break;
			}
			$url = 'http://www.erudit.org/getcover/' . $row['issne'];
			if ($this->verbose) {
				printf("%4d;%s;%s;%s;", $row['id'], $row['titre'], $url, $row['obsoletePar'] ? 'obsolète' : '');
			}
			$success = $this->updateCouverture($row, $url, 'http://www.erudit.org/');
			if ($this->verbose) {
				echo ";" . $success . "\n";
			}
		}
	}

	/**
	 * Utilise un fichier CSV fourni par Erudit, sans intervention.
	 * En-tête attendu : titre;issn;URLcouverture;URLlogo
	 *
	 * @param string $csv Chemin vers le CSV
	 * @param string $separator Séparateur de colonnes, par défaut ";"
	 */
	public function actionEruditCsv($csv, $separator = ";")
	{
		$erudit = Ressource::model()->findByAttributes(['identifiant' => 'Erudit']);
		$findTitre = Yii::app()->db->createCommand(
			"SELECT t.*"
			. " FROM Titre t"
			. " LEFT JOIN Identification i ON i.titreId = t.id AND i.ressourceId = {$erudit->id}"
			. " WHERE i.issne = :issne"
			. " GROUP BY t.id"
		);
		$findTitreByName = Yii::app()->db->createCommand(
			"SELECT t.* FROM Titre t WHERE t.titre = :titre"
		);
		$handle = fopen($csv, 'r');
		if (!$handle) {
			die("Erreur d'ouverture du CSV.\n");
		}

		$count = 0;
		while (($row= fgetcsv($handle, 1024, $separator)) !== false) {
			$count++;
			if (count($row) < 3) {
				continue;
			}
			if ($this->limit && $count > $this->limit) {
				if ($this->verbose) {
					echo "Limit reached\n";
				}
				break;
			}
			if ($row[1]) {
				$titre = $findTitre->queryRow(true, [':issne' => $row[1]]);
			} else {
				$titre = $findTitreByName->queryRow(true, [':titre' => $row[0]]);
			}
			$url = '';
			if ($titre) {
				// couverture || logo
				$success = $this->updateCouverture($titre, ($row[2] ?: $row[3]), 'http://www.erudit.org/');
				$success .= ';' . ($titre['obsoletePar'] ? 'obsolète' : '');
			} else {
				$titre = ['id' => '', 'titre' => $row[0]];
				$success = "ERREUR, titre non trouvé;";
			}
			if ($this->verbose) {
				printf("%d;%s;%s;%s\n", $titre['id'], $titre['titre'], $url, $success);
			}
		}
	}

	/**
	 * Télécharge la page de la revue chez Cairn.info, extrait la couverture, sans intervention.
	 */
	public function actionCairnWeb()
	{
		$cairn = Ressource::model()->findByAttributes(['nom' => 'Cairn.info']);
		$cmd = Yii::app()->db->createCommand(
			"SELECT t.*, s.url AS serviceUrl"
			. " FROM Titre t"
			. " JOIN Service s ON s.titreId = t.id AND s.ressourceId = {$cairn->id}"
			. " GROUP BY t.id"
		);
		$count = 0;
		$curl = new Curl;
		foreach ($cmd->queryAll() as $row) {
			$count++;
			if ($this->limit && $count > $this->limit) {
				if ($this->verbose) {
					echo "Limit reached\n";
				}
				break;
			}
			try {
				$dld = $curl->get($row['serviceUrl']);
			} catch (Exception $e) {
				sleep(1);
				try {
					$dld = $curl->get($row['serviceUrl']);
				} catch (Exception $e) {
					printf("%4d;%s;%s;%s;ERREUR\n", $row['id'], $row['titre'], $e->getMessage(), $row['obsoletePar'] ? 'obsolète' : '');
					continue;
				}
			}
			assert($dld instanceof Curl);
			if ($curl->getHttpCode() != 200) {
				printf("%4d;%s;%s;%s;ERREUR\n", $row['id'], $row['titre'], "ERREUR de téléchargement, code " . $curl->getHttpCode(), $row['obsoletePar'] ? 'obsolète' : '');
				continue;
			}
			if ($this->verbose > 1) {
				fprintf(STDERR, "%d %s  %d octets\n", $curl->getHttpCode(), $row['serviceUrl'], $curl->getSize());
			}
			$m = [];
			if (preg_match('!src="([^"]+/vign[^"]+)"!', $dld->getContent(), $m)) {
				$imageUrl = $m[1];
				if ($imageUrl[0] === '/') {
					$imageUrl = 'https://www.cairn.info' . $imageUrl;
				} elseif (!preg_match('/^http/', $imageUrl)) {
					$imageUrl = 'https://www.cairn.info/' . $imageUrl;
				}
				$status = $this->updateCouverture($row, $imageUrl, 'https://www.cairn.info/') . "\n";
				if ($this->verbose) {
					printf(
						"%4d;%s;%s;%s;%s\n",
						$row['id'],
						$row['titre'],
						$imageUrl,
						$row['obsoletePar'] ? 'obsolète' : '',
						$status
					);
				}
			}
		}
	}

	/**
	 * Utilise un fichier CSV fourni par Cairn.
	 * Passe par une intervention.
	 * En-tête attendu : ID_REVUE;REVUE;EDITEUR;ISSN;e-ISSN
	 *
	 * @param string $csv Chemin vers le CSV
	 * @param string $separator Séparateur de colonnes, par défaut ";"
	 */
	public function actionCairnCsv($csv, $separator = ";")
	{
		$idCairn = Yii::app()->db->createCommand("SELECT id FROM Ressource WHERE nom = 'Cairn.info'")->queryScalar();
		$sqlTitleByIssne = "SELECT * FROM Titre t"
			. "  JOIN Identification i ON (i.ressourceId = $idCairn AND i.titreId = t.id)"
			. " WHERE i.issne = :issne"
			. " GROUP BY t.id";
		$sqlTitleByName = "SELECT * FROM Titre t"
			. "  JOIN Identification i ON (i.ressourceId = $idCairn AND i.titreId = t.id)"
			. " WHERE CONCAT(t.prefixe, t.titre) = :name"
			. " GROUP BY t.id";
		$getNumero = Yii::app()->db
			->createCommand(
				"SELECT MAX(s.noFin) FROM Service s"
				. " WHERE s.ressourceId = $idCairn AND s.titreId = :titreId"
			);

		$handle = fopen($csv, 'r');
		if (!$handle) {
			die("Erreur d'ouverture du CSV.\n");
		}
		// ID_REVUE;REVUE;EDITEUR;ISSN;e-ISSN
		$header = fgetcsv($handle, 1024, $separator);
		if (!$header) {
			die("Erreur, CSV vide.\n");
		}

		$count = 0;
		while (($data = fgetcsv($handle, 1024, $separator)) !== false) {
			$count++;
			if ($this->limit && $count > $this->limit) {
				if ($this->verbose) {
					echo "Limit reached\n";
				}
				break;
			}
			list($code, $nom, , $issn, $issne) = $data;
			echo $nom . "\n";
			$titre = null;
			if ($issn) {
				$titre = Titre::model()->findByAttributes(['issn' => $issn]);
			}
			if (!$titre && $issne) {
				$titre = Titre::model()->findBySql($sqlTitleByIssne, [':issne' => $data[4]]);
			}
			if (!$titre) {
				$titre = Titre::model()->findBySql($sqlTitleByName, [':name' => $data[1]]);
			}
			if ($titre) {
				$numero = $getNumero->queryScalar([':titreId' => $titre->id]);
				if ($numero) {
					$url = sprintf("https://www.cairn.info/vign_rev/%s/%s_%03d_L204.jpg", $code, $code, $numero);
					if ($url != $titre->urlCouverture) {
						echo "\tURL: $url\n";
						$this->updateCouvertureUrl($titre, $url, "Traitement automatique de données de Cairn");
					} else {
						echo "\tURL inchangée: $url\n";
					}
				} else {
					echo "\tErreur : pas de numéro trouvé\n";
				}
			} else {
				echo "\tErreur : titre non identifié\n";
			}
		}

		fclose($handle);
	}

	/**
	 * Download from the distant sources.
	 */
	public function actionDownload($urlPattern = '', $removeBroken = false)
	{
		$titles = Titre::model()->findAllBySql(
			"SELECT * FROM Titre WHERE urlCouverture <> '' "
			. ($urlPattern ? "AND urlCouverture LIKE '$urlPattern' " : "")
			. " ORDER BY id"
		);
		printf("Téléchargement des couvertures de %d titres.\n", count($titles));
		if ($this->verbose) {
			echo "Titre ID\tURL\tErreur\n";
		}

		$tc = new TitreCouverture();
		$count = 0;
		foreach ($titles as $title) {
			$count++;
			if ($this->limit && $count > $this->limit) {
				if ($this->verbose) {
					echo "Limit reached\n";
				}
				break;
			}
			/* @var $title Titre */
			$msg = sprintf("%9d\t%s", $title->id, $title->urlCouverture);
			try {
				if (!$tc->download($title->id, $title->urlCouverture) && $removeBroken) {
					$title->urlCouverture = '';
					$title->save(false);
					if ($this->verbose) {
						echo $msg . "\tOK\n";
					}
				}
			} catch (Exception $e) {
				echo $msg . "\tERREUR " . $e->getMessage() . "\n";
				if ($removeBroken) {
					$title->urlCouverture = '';
					$title->save(false);
					Intervention::model()->deleteAllByAttributes([
						'titreId' => $title->id,
						'statut' => 'accepté',
						'description' => "Ajout d'une URL de couverture",
					]);
				}
			}
		}
	}

	/**
	 * Redimensionne les images téléchargées, si plus récentes
	 */
	public function actionResize()
	{
		$tc = new TitreCouverture();
		$actions = $tc->resizeAll();
		echo "Résultats :\n";
		foreach ($actions as $a => $num) {
			if ($num) {
				printf(" - %s : %d\n", $a, $num);
			}
		}
	}

	/**
	 * Liste les images absentes malgré une URL de couverture
	 */
	public function actionMissing()
	{
		$titles = Titre::model()->findAllBySql(
			"SELECT * FROM Titre WHERE urlCouverture <> '' "
			. " ORDER BY id"
		);
		$path = dirname(Yii::app()->getBasePath()) . '/images/titres-couvertures/';
		foreach ($titles as $title) {
			$img = $path . sprintf('%09d.png', $title->id);
			if (!file_exists($img)) {
				echo "{$title->id}\t{$title->urlCouverture}\n";
			}
		}
	}

	private function updateCouvertureUrl(Titre $titre, $newUrl, $commentaire = "")
	{
		if ($this->interventions) {
			$new = clone ($titre);
			$new->urlCouverture = $newUrl;

			$id = new InterventionDetail();
			$id->update($titre, $new);

			$i = new Intervention();
			$i->titreId = $titre->id;
			$i->revueId = $titre->revueId;
			$i->contenuJson = $id;
			$i->statut = 'attente';
			$i->hdateProp = $_SERVER['REQUEST_TIME'];
			if ($titre->urlCouverture) {
				$i->description = "Mise à jour d'une URL de couverture";
			} else {
				$i->description = "Ajout d'une URL de couverture";
			}
			$i->commentaire = $commentaire;
			$i->import = ImportType::getSourceId('URL couverture');

			if (!$i->accept(true)) {
				print_r($i->getErrors());
				echo "\tErreur, intervention refusée\n";
			}
			return 1;
		}
		$titre->urlCouverture = $newUrl;
		return $titre->updateByPk($titre->id, ['urlCouverture' => $newUrl]);
	}

	private function updateCouverture(array $titre, $url, $sameUrl)
	{
		$model = Titre::model()->populateRecord($titre);
		if (empty($titre['urlCouverture'])) {
			if ($this->updateCouvertureUrl($model, $url)) {
				$success = "ajoutée";
			} else {
				$success = "ERREUR/ajoutée";
			}
		} elseif ($titre['urlCouverture'] === $url) {
			$success = "identique";
		} elseif ($this->maj || ($this->majMemeSource && strncmp($titre['urlCouverture'], $sameUrl, strlen($sameUrl)) === 0)) {
			if ($this->updateCouvertureUrl($model, $url)) {
				$success = "modifiée";
			} else {
				$success = "ERREUR/modifiée";
			}
		} else {
			$success = "conservée";
		}
		return $success;
	}
}
