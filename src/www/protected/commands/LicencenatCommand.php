<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class LicencenatCommand extends CConsoleCommand
{
	/**
	 * Move the content of each Ressource into a new Collection of a destination Ressource.
	 */
	public function actionMigrate($csv, $separator = ';')
	{
		$csvLines = file($csv);
		if (!$csvLines) {
			echo "Fichier CSV vide ou non-trouvé\n";
			exit(1);
		}
		$firstRow = str_getcsv($csvLines[0], $separator, '"');
		if (count($firstRow) !== 3) {
			echo "3 colonnes attendues dans le CSV, ce n'est pas le cas dans la première ligne\n";
			exit(2);
		}
		foreach ($csvLines as $k => $line) {
			if (!$line) {
				continue;
			}
			if (!preg_match('/^\d/', $line)) {
				fprintf(STDERR, "La ligne $k du CSV est ignorée car elle ne commence pas par un chiffre.\n");
				continue;
			}
			$row = str_getcsv($line, $separator);
			if (count($row) !== 3) {
				fprintf(STDERR, "Mauvais nombre de colonnes en ligne $k du CSV.\n");
				continue;
			}
			$srcRessource = Ressource::model()->findByPk($row[0]);
			$dstRessource = Ressource::model()->findByPk($row[2]);
			echo "## Ressource {$srcRessource->nom}\n";
			self::createDefaultCollection($dstRessource);
			$dstCollection = self::createLicenceCollection($dstRessource, "Licence Nationale France");
			self::migrateServices($srcRessource, $dstCollection);
		}
	}

	private static function createDefaultCollection(Ressource $r)
	{
		$c = new Collection;
		$c->ressourceId = $r->id;
		$c->nom = $r->nom;
		$c->description = "Collection par défaut";
		$c->type = Collection::TYPE_COURANT;
		$c->identifiant = '';
		if (!$c->save()) {
			echo "Erreur en enregistrant la nouvelle collection :\n";
			print_r($c->getErrors());
			exit(3);
		}
		echo "\t{$r->nom} : Collection homonyme créée, ID {$c->id}\n";

		$num = Yii::app()->db
			->createCommand(
				"INSERT IGNORE INTO Service_Collection SELECT id, {$c->id} FROM Service WHERE ressourceId = {$r->id}"
			)->execute();
		echo "\t$num accès rattachés à la collection homonyme\n";
	}

	private static function createLicenceCollection(Ressource $r, $name)
	{
		$c = new Collection;
		$c->ressourceId = $r->id;
		$c->nom = $name;
		$c->description = "";
		$c->type = Collection::TYPE_LICNATFRANCE;
		$c->identifiant = '';
		if (!$c->save()) {
			echo "Erreur en enregistrant la nouvelle collection :\n";
			print_r($c->getErrors());
			exit(4);
		}
		echo "\t{$r->nom} : Collection \"$name\" créée, ID {$c->id}\n";
		return $c;
	}

	private static function migrateServices(Ressource $srcR, Collection $dstC)
	{
		$num2 = Yii::app()->db
			->createCommand(
				"INSERT IGNORE INTO Service_Collection SELECT id, {$dstC->id} FROM Service WHERE ressourceId = {$srcR->id}"
			)->execute();
		echo "\t$num2 accès rattachés à la collection LicenceNat\n";

		$num1 = Yii::app()->db
			->createCommand(
				"UPDATE Service SET ressourceId = {$dstC->ressourceId} WHERE ressourceId = {$srcR->id}"
			)->execute();
		echo "\t$num1 accès changent de ressource\n";

		try {
			Yii::app()->db
				->createCommand(
					"DELETE FROM Identification WHERE ressourceId = {$srcR->id}"
				)->execute();
			if ($srcR->delete()) {
				echo "\tRessource {$srcR->nom} (ID {$srcR->id}) supprimée.\n\n";
			} else {
				echo "\tErreur en supprimant la ressource {$srcR->nom} (ID {$srcR->id})\n";
				print_r($srcR->getErrors());
				echo "\n";
			}
		} catch (Exception $e) {
			echo "\tErreur en supprimant la ressource {$srcR->nom} (ID {$srcR->id})\t\n";
			print_r($e->getMessage());
			echo "\tSupprimez la ressource dans l'interface web pour gérer les contraintes.\n\n";
		}
	}
}
