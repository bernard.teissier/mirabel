<?php
require_once Yii::getPathOfAlias('application.models.import') . '/ReaderExcel.php';
require_once Yii::getPathOfAlias('application.models.import') . '/SimpleLogger.php';

/**
 * Read indexation tags.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class IndexationCommand extends CConsoleCommand
{
	public const SUDOC_RDF_URL = 'https://www.sudoc.fr/%s.rdf';

	public const INNER_SEPARATOR = ' ## ';

	public $verbose = 0;

	/**
	 * @var int
	 */
	public $limit = 0;

	/**
	 * @var string Only Titre whose id is in this list (comma separated)
	 */
	public $ids = "";

	/**
	 * @var int Only those where titre.id is greater or equal this.
	 */
	public $start = 0;

	private $skosCache = [];

	private $curl;

	private $parsedTagNames = [
		'subject' => 1,
		'spatial' => 1,
		'temporal' => 1,
	];

	/**
	 * Fill the table CategorieStats.
	 */
	public function actionUpdateStats()
	{
		CategorieStats::fill();
	}

	public function actionImport($vocabulaire, $csv)
	{
		if (!file_exists($csv)) {
			echo "Fichier CSV '$csv' non trouvé.\n";
			return 1;
		}

		$import = new IndexationImport();
		if (ctype_digit($vocabulaire)) {
			$import->vocabulaire = Vocabulaire::model()->findByPk($vocabulaire);
		} else {
			$import->vocabulaire = Vocabulaire::model()->findByAttributes(['titre' => $vocabulaire]);
		}
		if (!$import->vocabulaire) {
			echo "Vocabulaire non trouvé, par titre comme par ID.\n";
			return 1;
		}

		$expected = $import->parseCsvFile($csv, STDOUT);

		fprintf(STDERR, "## Mots-clés inconnus de Mir@bel :\n" . join("\n", $import->getUnknownWords()) . "\n");

		$inserts = $import->save();
		fprintf(STDERR, "## Indexations enregistrées : %d / %d\n", $inserts, $expected);
	}

	public function actionFix()
	{
		$cmd = Yii::app()->db->createCommand(
			"UPDATE Categorie c LEFT JOIN Categorie p ON c.parentId = p.id"
			. " SET c.chemin = IF(c.parentId IS NULL, '', CONCAT(p.chemin, '/', LPAD(c.parentId,5,'0'))) "
			. " WHERE c.profondeur = ?"
		);
		for ($depth = 0; $depth <= Categorie::MAX_DEPTH; $depth++) {
			$cmd->execute([$depth]);
		}
	}

	/**
	 * Action "all" that imports from every source of SUDOC.
	 */
	public function actionRead()
	{
		if ($this->verbose) {
			fprintf(STDERR, "titre\ttitre ID\tissn\trdf\trdf:about\trameau\tdewey\tcollectivité\n");
		}
		$titles = Titre::model()->findAllBySql(
			"SELECT * FROM Titre WHERE sudoc <> ''"
			. ($this->ids ? " AND id IN (" . $this->ids . ")" : "")
			. ($this->start ? " AND id >= " . (int) $this->start : "")
			. " ORDER BY id"
			. ($this->limit ? " LIMIT " . (int) $this->limit : "")
		);
		$this->curl = curl_init();
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
		foreach ($titles as $title) {
			/* @var $title Titre */
			$line = $this->fetchTitleInfo($title);
			$this->printDetails($title->id, $line);
		}
	}

	/**
	 * @param string $file
	 */
	public function actionCairnXls($file)
	{
		$reader = new \import\ReaderExcel(["ID_REV", "TITRE", "ISSN"]);
		$reader->setLog(new \import\SimpleLogger());
		$reader->readUrl($file);

		$themes = $reader->getHeader();
		array_shift($themes);
		array_shift($themes);
		array_shift($themes);

		$lines = $reader->readLines();
		if (empty($lines)) {
			echo "Empty\n";
			return 1;
		}
		$issnRank = $reader->getHeaderRank("ISSN");
		foreach ($lines as $line) {
			$row = $reader->splitRawLine($line);
			$issn = $row[$issnRank];
			if ($issn) {
				foreach ($themes as $pos => $t) {
					if ($row[3 + $pos]) {
						fputcsv(STDOUT, [$issn, $t], ";");
					}
				}
			} else {
				fprintf(STDERR, "ISSN non trouvé en ligne %d.\n", $line->getRowIndex());
			}
		}
	}

	/**
	 * @param Titre $title
	 */
	protected function fetchTitleInfo(Titre $title)
	{
		$url = sprintf(self::SUDOC_RDF_URL, $title->sudoc);
		if ($this->verbose > 1) {
			fprintf(STDERR, "Titre \"{$title->titre}\", URL $url\n");
		}
		curl_setopt($this->curl, CURLOPT_URL, $url);
		$xml = curl_exec($this->curl);
		$line = [
			$title->titre,
			$title->id,
			$title->issn,
			$url,
			preg_replace('/\.rdf$/', '', $url),
			'keywords' => [
				'rameau' => [],
				'dewey' => [],
				'coll' => [],
			],
			'comment' => '',
		];

		$rdf = new DOMDocument("1.0", "UTF-8");
		set_error_handler(function () {
		});
		if (!$rdf->loadXML($xml, LIBXML_NOBLANKS)) {
			$line['comment'] = "ERREUR : XML invalide";
			return $line;
		}
		restore_error_handler();
		$rdf->formatOutput = true;
		//echo $rdf->saveXML();

		$dctermsUrl = "http://purl.org/dc/terms/";
		$rdfUrl = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";

		$subjects = [];
		foreach ($rdf->getElementsByTagNameNS($dctermsUrl, "subject") as $node) {
			$text = trim(str_replace('-- Périodiques', '', $node->textContent));
			$subjects[$text] = 1;
		}

		$groupedKeywords = [];
		foreach ($rdf->getElementsByTagNameNS($dctermsUrl, "*") as $node) {
			/* @var $node DOMElement */
			$tagName = preg_replace('/^dcterms:/', '', $node->tagName);
			if (empty($this->parsedTagNames[$tagName])) {
				if ($this->verbose > 1) {
					fprintf(STDERR, "    Ignore la balise RDF $tagName\n");
				}
				continue;
			}
			$resource = $node->getAttributeNS($rdfUrl, "resource");
			if ($resource) {
				$sourceKeyword = $this->parseResource($resource);
				if ($sourceKeyword) {
					if (!empty($sourceKeyword['keyword'])) {
						$line['keywords'][$sourceKeyword['name']][] = $sourceKeyword['keyword'];
						$groupedKeywords[] = $sourceKeyword['keyword'];
					} elseif ($this->verbose) {
						fprintf(STDERR, "\t Empty keyword for {$sourceKeyword['name']}: $resource\n");
					}
					if ($node->nextSibling) {
						$nextTagName = ($node->nextSibling->nodeType === XML_ELEMENT_NODE ? preg_replace('/^dcterms:/', '', $node->nextSibling->tagName) : '');
						if (empty($this->parsedTagNames[$nextTagName])) {
							$group = join(' -- ', $groupedKeywords);
							if (!isset($subjects[$group])) {
								$line['comment'] .= "Incohérence : DCTERMS '$group' n'a pas de subject équivalent. ";
							}
						}
					}
				} elseif ($this->verbose > 1) {
					fprintf(STDERR, "    Pas de mot-clé dans $tagName - $resource\n");
				}
			} elseif ($this->verbose > 1) {
				fprintf(STDERR, "    Pas de ressource dans $tagName\n");
			}
		}
		$line['keywords']['coll'] = $this->parseMarcrel($rdf);

		foreach ($line['keywords'] as $src => $words) {
			$line['keywords'][$src] = array_unique($words);
		}
		return $line;
	}

	private function printDetails($titleId, $line)
	{
		$keywords = $line['keywords'];
		if ($this->verbose) {
			unset($line['keywords']);
			foreach ($keywords as $ks) {
				$line[] = join(self::INNER_SEPARATOR, $ks);
			}
			fprintf(STDERR, join("\t", $line) . "\n");
		}
		if (preg_match('/^ERREUR/', $line['comment'])) {
			return;
		}
		foreach ($keywords['rameau'] as $word) {
			if (strpos($word, " -- ") !== false) {
				$subwords = explode(" -- ", $word);
				foreach ($subwords as $w) {
					self::printLine($titleId, $w);
				}
			} else {
				self::printLine($titleId, $word);
			}
		}
	}

	private static function printLine($titleId, $word)
	{
		if (strpos($word, ";") !== false) {
			$word = '"' . addcslashes($word, "\"\n") . '"';
		}
		fprintf(STDOUT, "%d;%s\n", $titleId, $word);
	}

	/**
	 * @param string $url
	 * @return array [name: sourceName, keyword: keyword]
	 */
	private function parseResource($url)
	{
		if ($url === 'http://www.idref.fr/02724640X/id') {
			// "Périodiques"
			return [];
		}
		$m = [];
		if (!preg_match('#https?://([^/]+)/#', $url, $m)) {
			fprintf(STDERR, "    Could not detect the domain in URL $url\n");
			return [];
		}
		$domain = $m[1];
		switch ($domain) {
			case 'www.idref.fr':
				// http://www.idref.fr/027248321/id ---(303 + alternate)---> http://www.idref.fr/027248321.rdf
				$skosUrl = preg_replace_callback(
					'#/([^/]+)/id$#',
					function ($m) {
						return "/" . strtoupper($m[1]) . ".rdf";
					},
					$url
				);
				return [
					'name' => 'rameau',
					'keyword' => $this->parseSkos($skosUrl),
				];
			case 'dewey.info':
				// DISABLED!
				return [];
				/*
				// http://dewey.info/class/900/ ---(303)---> http://dewey.info/class/900/about
				if (preg_match('#/([^/]+)/$#', $url, $m)) {
					$code = $m[1] . ": ";
				} else {
					$code = "";
				}
				return array(
					'name' => 'dewey',
					'keyword' => $code . $this->parseSkos($url . 'about')
				);
				 */
			case 'lexvo.org':
				// ignore language indexation
				return [];
			default:
				fprintf(STDERR, "    Could not identify the source of the URL $url\n");
				return [];
		}
	}

	/**
	 * Return the keyword that this URL points to.
	 *
	 * @param string $url
	 * @return string
	 */
	private function parseSkos($url)
	{
		if (isset($this->skosCache[$url])) {
			return $this->skosCache[$url];
		}
		if ($this->verbose) {
			fprintf(STDERR, "    SKOS: reading URL $url\n");
		}
		curl_setopt($this->curl, CURLOPT_URL, $url);
		$xml = curl_exec($this->curl);
		if (!$xml) {
			fprintf(STDERR, "    Erreur sur l'URL '$url' " . curl_error($this->curl) . "\n");
			return "";
		}
		$dom = new DOMDocument("1.0", "UTF-8");
		$dom->loadXML($xml);

		// <rdf:Description rdf:about="http://dewey.info/class/900/2009/08/about.fr"> / skos:prefLabel
		// rdf  = http://www.w3.org/1999/02/22-rdf-syntax-ns#
		$descriptions = $dom->getElementsByTagNameNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "Description");
		if ($descriptions) {
			foreach ($descriptions as $d) {
				$lang = $d->getAttributeNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "about");
				if ($lang && preg_match('/about\.fr$/', $lang)) {
					$fr = $d;
				} elseif ($lang && preg_match('/about\.en$/', $lang)) {
					$en = $d;
				}
			}
			if (isset($fr)) {
				$base = $fr;
			} elseif (isset($en)) {
				$base = $en;
			} else {
				$base = $dom->documentElement;
			}
		} else {
			$base = $dom->documentElement;
		}

		// 1 skos:prefLabel + n skos:altLabel
		// skos = http://www.w3.org/2004/02/skos/core#
		$labels = $base->getElementsByTagNameNS("http://www.w3.org/2004/02/skos/core#", "prefLabel");
		if (!$labels) {
			fprintf(STDERR, "    SKOS / Could not find a label within the URL $url\n");
			return "";
		}
		$this->skosCache[$url] = $labels->item(0)->nodeValue;
		return $labels->item(0)->nodeValue;
	}

	private function parseMarcrel($dom)
	{
		// marcrel = http://id.loc.gov/vocabulary/relators
		// foaf    = http://xmlns.com/foaf/0.1/
		$results = [];
		$authors = $dom->getElementsByTagNameNS("http://id.loc.gov/vocabulary/relators", "aut");
		if ($authors) {
			foreach ($authors as $a) {
				$names = $a->getElementsByTagNameNS("http://xmlns.com/foaf/0.1/", "name");
				if ($names) {
					$results[] = "auteur: " . $names->item(0)->nodeValue;
				}
			}
		}
		$editors = $dom->getElementsByTagNameNS("http://id.loc.gov/vocabulary/relators", "edt");
		if ($editors) {
			foreach ($editors as $e) {
				$names = $e->getElementsByTagNameNS("http://xmlns.com/foaf/0.1/", "name");
				if ($names) {
					$results[] = "éditeur: " . $names->item(0)->nodeValue;
				}
			}
		}
		return $results;
	}
}
