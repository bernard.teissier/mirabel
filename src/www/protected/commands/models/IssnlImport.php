<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class IssnlImport
{
	public const FILE_ISSNL_ISSN = 'ISSN-L_to_ISSN.txt';

	public const FILE_ISSN_ISSNL = 'ISSN_to_ISSN-L.txt';

	public const CSV_SEPARATOR = "\t";

	public $verbose;

	/**
	 * @var Pdo
	 */
	private $pdo;

	/**
	 * @var string
	 */
	private $filepath;

	public function __construct()
	{
		$this->filepath = dirname(__DIR__) . '/data/';
		$this->pdo = new PDO('sqlite:' . $this->filepath . 'issnl.sqlite');
	}

	/**
	 * Import the CSV files into the sqlite DB (used as a K/V store).
	 */
	public function import()
	{
		$this->pdo->exec("PRAGMA synchronous = OFF");
		$this->pdo->exec("PRAGMA journal_mode = WAL");
		$this->pdo->exec("DROP TABLE IF EXISTS issnl");
		$this->pdo->exec("DROP TABLE IF EXISTS issn");
		$this->pdo->exec("CREATE TABLE issnl (k TEXT, v TEXT)");
		$this->pdo->exec("CREATE TABLE issn (k TEXT, v TEXT)");
		$this->insertCsvAsKeyValue($this->filepath . self::FILE_ISSNL_ISSN, 'issnl');
		$this->insertCsvAsKeyValue($this->filepath . self::FILE_ISSN_ISSNL, 'issn');
		$this->pdo->exec("CREATE INDEX issnlk ON issnl (k)");
		$this->pdo->exec("CREATE INDEX issnk ON issn (k)");
		$this->pdo->exec("PRAGMA wal_checkpoint");
	}

	/**
	 * Return the list of issn which are linked to this issnl.
	 *
	 * @param string $issnl
	 * @return array|null Array of ISSN (string), or null if the issnl was not found
	 */
	public function searchByIssnl($issnl)
	{
		$search = $this->pdo->prepare("SELECT v FROM issnl WHERE k = ?");
		$search->execute([$issnl]);
		$value = $search->fetch(PDO::FETCH_ASSOC);
		if ($value) {
			return explode("\t", $value['v']);
		}
		return null;
	}

	/**
	 * Return the issnl which is linked to this issn.
	 *
	 * @param string $issn
	 * @return string|null ISSN-L, or null if the issn was not found
	 */
	public function searchByIssn($issn)
	{
		$search = $this->pdo->prepare("SELECT v FROM issn WHERE k = ?");
		$search->execute([$issn]);
		return $search->fetchColumn();
	}

	/**
	 * Return an assoc array from a CSV file, where the keys are in the first column.
	 *
	 * @param string $filename
	 * @param string $table
	 * @return array
	 */
	protected function insertCsvAsKeyValue($filename, $table)
	{
		if (!is_readable($filename)) {
			throw new Exception("$filename n'est pas lisible.");
		}
		if ($this->verbose > 0) {
			fprintf(STDERR, "Import de $filename en cours...\n");
		}
		$handle = fopen($filename, 'r');
		if (!$handle) {
			throw new Exception("$filename n'est pas accessible.");
		}

		$header = fgetcsv($handle, 1024, self::CSV_SEPARATOR);
		if (!$header) {
			throw new Exception("En-tête CSV absent de $filename.");
		}

		$kv = [];
		$insert = $this->pdo->prepare("INSERT INTO $table VALUES (?, ?)");
		while (($data = fgetcsv($handle, 1024, self::CSV_SEPARATOR)) !== false) {
			$key = array_shift($data);
			$value = join("\t", $data);
			if ($key) {
				$insert->execute([$key, $value]);
			}
		}
		fclose($handle);
		return $kv;
	}
}
