<?php

namespace commands\models\linkimport;

use Titre;
use commands\models\LinksObsolescence;
use commands\models\LoadIssns;

class Road extends DefaultImport
{
	protected function importLinks(): void
	{
		$filename = \Yii::getPathOfAlias('application.runtime') . '/ROAD.xml.gz';
		$this->refreshFile("ftp://ROAD:open_access@ftp.issn.org/ROAD.xml.gz", $filename);

		$countIssns = LoadIssns::readFromMarcxml($filename);
		if ($this->verbose >= 2) {
			fprintf(STDERR, "ISSN lus : %d\n", $countIssns);
		}

		$source = $this->findSource('ROAD');
		$titres = Titre::model()->findAllBySql(
			"SELECT * FROM (
SELECT t.*, k.id AS confirm FROM Titre t JOIN Issn i ON (i.titreId = t.id) JOIN knownissn k ON (i.issn = k.issn)
UNION
SELECT t.*, k.id AS confirm FROM Titre t JOIN Issn i ON (i.titreId = t.id) JOIN knownissn k ON (i.issnl = k.issn)
) t
 GROUP BY id"
		);
		if ($this->verbose >= 2) {
			fprintf(STDERR, "Titres de Mir@bel concernés : %d\n\n", count($titres));
		}
		$changes = "";
		foreach ($titres as $titre) {
			$issn = $titre->confirm;
			$titre->confirm = true;
			$url = sprintf('http://road.issn.org/issn/%s', $issn);
			$changes .= $this->getLinkUpdater()->updateLink($titre, $issn, $url);
		}
		if ($this->getLinkUpdater()->hasChanges()) {
			echo $this->getLinkUpdater()->getStats(); // 1. summary of changes
			echo $changes; // 2. details of changes
		}

		if ($this->obsoleteSince) {
			echo (new LinksObsolescence($source))->listObsoleteLinks($this->obsoleteSince); // 3. obsolete links
		}
	}

	protected function getSubject(): string
	{
		return "Mises à jour ROAD";
	}
}
