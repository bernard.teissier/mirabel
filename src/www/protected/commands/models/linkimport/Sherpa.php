<?php

namespace commands\models\linkimport;

use Titre;
use Yii;
use commands\models\LinksObsolescence;
use models\sherpa\Api;

class Sherpa extends DefaultImport
{
	/**
	 * @var Api
	 */
	private $api;

	protected function importLinks(): void
	{
		$this->initApi();

		$source = $this->findSource('romeo');
		$cmd = Yii::app()->db->createCommand(
			"SELECT titreId, GROUP_CONCAT(issn SEPARATOR ',') AS issns FROM Issn WHERE issn IS NOT NULL GROUP BY titreId"
			. ($this->limit ? " LIMIT {$this->limit}" : "")
		);

		$changes = "";
		foreach ($cmd->queryAll() as $row) {
			$titreId = (int) $row['titreId'];
			$issns = explode(',', $row['issns']);
			try {
				$changes .= $this->querySherpa($this->getLinkUpdater(), $titreId, $issns);
			} catch (\Throwable $e) {
				fprintf(STDERR, $e->getMessage());
				Yii::log($e->getMessage() . "\n" . $e->getTraceAsString(), \CLogger::LEVEL_ERROR, 'sherpa');
			}
		}
		if ($this->getLinkUpdater()->hasChanges()) {
			echo $this->getLinkUpdater()->getStats(); // 1. summary of changes
			echo $changes; // 2. details of changes
		}

		if ($this->obsoleteSince) {
			echo (new LinksObsolescence($source))->listObsoleteLinks($this->obsoleteSince); // 3. obsolete links
		}
	}

	protected function getSubject(): string
	{
		return "Mises à jour Sherpa-Romeo";
	}

	private function initApi()
	{
		$key = Yii::app()->params->itemAt('sherpa')['api-key'];
		$this->api = new Api($key);
	}

	private function insertPolicy(int $titreId, array $policy): void
	{
		if (!$policy) {
			return;
		}
		$insert = Yii::app()->db->createCommand(
			"REPLACE INTO RomeoPolicy VALUES (:titreId, :submitted, :accepted, :published)"
		);
		$insert->execute([
			':titreId' => $titreId,
			':submitted' => $policy['submitted'],
			':accepted' => $policy['accepted'],
			':published' => $policy['published'],
		]);
	}

	private function querySherpa(\commands\models\LinkUpdater $updater, int $titreId, array $issns): string
	{
		$pub = $this->api->fetchPublication($titreId, $issns);
		if ($pub === null) {
			if ($this->verbose >= 2) {
				fprintf(STDERR, "## Titre %d: ISSNs %s => pas d'entrée Romeo\n", $titreId, join(",", $issns));
			}
			return "";
		}
		$url = $pub->getRomeoUrl();
		$this->insertPolicy((int) $titreId, $pub->getPoliciesSummary());
		if ($url) {
			$titre = Titre::model()->findByPk($titreId);
			return $updater->updateLink($titre, join(",", $issns), $url);
		}
		return "";
	}
}
