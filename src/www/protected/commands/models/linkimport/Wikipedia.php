<?php

namespace commands\models\linkimport;

use commands\models\LinksObsolescence;
use models\wikidata\Import;

class Wikipedia extends DefaultImport
{
	protected function importLinks(): void
	{
		$WImport = new Import($this->verbose, $this->simulation, $this->limit);
		$changes = $WImport->importWplinks();
		if ($WImport->getLinkUpdater()->hasChanges()) {
			echo $WImport->getLinkUpdater()->getStats(); // 1. summary of changes
			echo $changes; // 2. details of changes
		}

		$sourceswp = \Sourcelien::model()->findAll("nomcourt LIKE 'wikipedia-%'");
		if ($this->obsoleteSince) {
			// 3. obsolete links
			foreach ($sourceswp as $source) {
				echo (new LinksObsolescence($source))->listObsoleteLinks($this->obsoleteSince);
			}
		}
	}

	protected function getSubject(): string
	{
		return "Mises à jour Wikipedia";
	}
}
