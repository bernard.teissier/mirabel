<?php

namespace commands\models\linkimport;

use Titre;
use commands\models\LinksObsolescence;
use commands\models\LoadIssns;

class CairnInt extends DefaultImport
{
	public $csvFile;

	protected function importLinks(): void
	{
		// Load the ISSN numbers from the CSV into the tmp table `knownissn`
		$columnIssnp = 3;
		$columnIssne = 4;
		$countIssns = LoadIssns::readFromCsv($this->csvFile, $columnIssnp, $columnIssne, $columnIssne, "\t");
		if ($this->verbose >= 2) {
			fprintf(STDERR, "ISSN lus (MàJ) : %d\n", $countIssns);
		}

		// get the source (created if missing)
		\Yii::app()->db
			->createCommand(
				"INSERT IGNORE INTO Sourcelien VALUES (NULL, 'Cairn international', '', 'cairnint', 'www.cairn-int.info', 1, UNIX_TIMESTAMP(), UNIX_TIMESTAMP())"
			)->execute();
		$source = $this->findSource('cairnint');

		// identify by issn(e|p|l) == issn(e|p)
		$titres = Titre::model()->findAllBySql(
			"SELECT * FROM (
SELECT t.*, k.issn AS confirm FROM Titre t JOIN Issn i ON (i.titreId = t.id) JOIN knownissn k ON (i.issn = k.issn)
UNION
SELECT t.*, k.id AS confirm FROM Titre t JOIN Issn i ON (i.titreId = t.id) JOIN knownissn k ON (i.issn = k.id)
UNION
SELECT t.*, k.issn AS confirm FROM Titre t JOIN Issn i ON (i.titreId = t.id) JOIN knownissn k ON (i.issnl = k.issn)
UNION
SELECT t.*, k.id AS confirm FROM Titre t JOIN Issn i ON (i.titreId = t.id) JOIN knownissn k ON (i.issnl = k.id)
) t
GROUP BY id"
		);
		if ($this->verbose >= 2) {
			fprintf(STDERR, "Titres de Mir@bel concernés : %d\n\n", count($titres));
		}

		// read the URL associated to each issn
		// URLs: {<issn>: <URL>}
		$urls = [];
		$handle = fopen($this->csvFile, "r");
		$header = fgetcsv($handle, 0, "\t");
		$columnUrl = array_search("URL Cairn international", $header, true);
		while (($data = fgetcsv($handle, 0, "\t")) !== false) {
			if ($data[$columnIssnp]) {
				$urls[$data[$columnIssnp]] = $data[$columnUrl];
			}
			if ($data[$columnIssne]) {
				$urls[$data[$columnIssne]] = $data[$columnUrl];
			}
		}
		fclose($handle);

		// apply the changes
		$changes = "";
		if (!$this->simulation) {
			foreach ($titres as $titre) {
				$issn = $titre->confirm;
				$titre->confirm = true;
				if ($urls[$issn]) {
					$changes .= $this->getLinkUpdater()->updateLink($titre, $issn, $urls[$issn]);
				}
			}
		}
		if ($this->getLinkUpdater()->hasChanges()) {
			echo $this->getLinkUpdater()->getStats(); // 1. summary of changes
			echo $changes; // 2. details of changes
		}

		if ($this->obsoleteSince) {
			echo (new LinksObsolescence($source))->listObsoleteLinks($this->obsoleteSince); // 3. obsolete links
		}
	}

	protected function getSubject(): string
	{
		return "Mises à jour Cairn international";
	}
}
