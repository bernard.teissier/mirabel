<?php

namespace commands\models\linkimport;

use Config;
use Curl;
use Mailer;
use Yii;
use Sourcelien;
use commands\models\LinkUpdater;

abstract class DefaultImport
{
	/**
	 * @var array
	 */
	protected $email = [];

	/**
	 * @var int If set to a positive value, will stop after this many iterations.
	 */
	protected $limit = 0;

	/**
	 * @var int timestamp
	 */
	protected $obsoleteSince;

	/**
	 * @var bool
	 */
	protected $simulation = false;

	/**
	 * @var int
	 */
	protected $verbose = 0;

	/**
	 * @var LinkUpdater
	 */
	private $linkUpdater;

	/**
	 * @param array $config Possibke keys: email, limit, obsoleteSince, simulation, verbose
	 */
	public function __construct(array $config)
	{
		if (!empty($config['email'])) {
			$this->setEmail($config['email']);
		}
		if (!empty($config['obsoleteSince'])) {
			$this->setObsoleteSince($config['obsoleteSince']);
		}
		if (isset($config['limit'])) {
			$this->limit = (int) $config['limit'];
		}
		if (isset($config['simulation'])) {
			$this->simulation = (bool) $config['simulation'];
		}
		if (isset($config['verbose'])) {
			$this->verbose = (int) $config['verbose'];
		}
	}

	public function import(): void
	{
		$this->onImportStart();
		$this->importLinks();
		$this->onImportEnd();
	}

	/**
	 * Download an URL into the local file-system and return its temporary file name.
	 *
	 * @param string $url
	 * @return string E.g. "/tmp/Erih_2020-12-14_QFXpHS"
	 */
	protected function downloadFile(string $url): string
	{
		if ($this->verbose > 0) {
			fprintf(STDERR, "Téléchargement de $url\n");
		}
		$className = substr(get_class($this), 1 + strrpos(get_class($this), '\\'));
		$filename = tempnam(sys_get_temp_dir(), sprintf("%s_%s_", $className, date('Y-m-d')));
		$fh = fopen($filename, "w");
		if ($fh === false) {
			throw new \Exception("Impossible d'écrire dans le fichier temporaire '$filename'.");
		}
		$curl = new Curl();
		$curl->getFile($url, $fh);
		if ($curl->getSize() === 0) {
			throw new \Exception("Le téléchargement n'a aucun contenu.");
		}
		unset($curl);
		fclose($fh);
		if (stat($filename)[7] === 0) {
			throw new \Exception("Le fichier téléchargé est vide.");
		}
		return $filename;
	}

	/**
	 * If the remote file is fresher than the local one, download it.
	 *
	 * @param string $url
	 * @param string $filePath
	 */
	protected function refreshFile(string $url, string $filePath): void
	{
		if (is_file($filePath)) {
			$localTime = stat($filePath)[10];
			$curl = new Curl();
			$remoteTime = $curl->getFileTime($url);
			if ($remoteTime && $remoteTime <= $localTime) {
				if ($this->verbose > 0) {
					fprintf(STDERR, "La copie '%s' est à jour, pas de téléchargement.\n", $filePath);
				}
				return;
			}
		}
		$tmpFile = $this->downloadFile($url);
		if (!rename($tmpFile, $filePath)) {
			throw new \Exception("Impossible d'écrire le téléchargement dans '$filePath'.");
		}
	}

	protected function findSource(string $sourceName): Sourcelien
	{
		$source = Sourcelien::model()->findByAttributes(['nomcourt' => $sourceName]);
		if (!$source) {
			throw new \Exception("Source '$sourceName' introuvable. Erreur dans le code source.");
		}
		if ($this->linkUpdater === null) {
			$this->linkUpdater = new LinkUpdater($source);
			$this->linkUpdater->simulation = $this->simulation;
			$this->linkUpdater->verbose = $this->verbose;
		} else {
			$this->linkUpdater->addSource($source);
		}
		return $source;
	}

	protected function getLinkUpdater()
	{
		if ($this->linkUpdater === null) {
			throw new \Exception("linkUpdater was not set. The code must call findSource() first.");
		}
		return $this->linkUpdater;
	}

	/**
	 * @return string The subject of the email.
	 */
	abstract protected function getSubject(): string;

	/**
	 * The main import process, called by import().
	 */
	abstract protected function importLinks(): void;

	protected function onImportEnd(): void
	{
		if ($this->email) {
			$body = ob_get_clean();
			if ($body) {
				$message = Mailer::newMail()
					->setSubject(sprintf("[%s] %s", Yii::app()->name, $this->getSubject()))
					->setFrom(Config::read('email.from'))
					->setTo($this->email)
					->setBody($body);
				Mailer::sendMail($message);
			}
		}
	}

	protected function onImportStart(): void
	{
		if ($this->email) {
			ob_start();
		}
	}

	/**
	 * @param array|string $to
	 * @throws \Exception
	 * @return void
	 */
	protected function setEmail($to): void
	{
		if (is_array($to)) {
			$this->email = $to;
		} elseif (is_string($to) && strpos($to, '@') !== false) {
			$this->email = [$to];
		} else {
			throw new \Exception("Invalid 'email' value. Array|string expected, got " . var_export($to, true));
		}
	}

	/**
	 * @param string $since
	 * @throws \Exception
	 * @return void
	 */
	protected function setObsoleteSince($since): void
	{
		$timestamp = null;
		if (ctype_digit($since)) {
			$timestamp = (int) $since;
		} elseif (is_string($since)) {
			$timestamp = strtotime($since);
		}
		if (empty($timestamp)) {
			throw new \Exception("Invalid time value. Expected timestamp or e.g. '3 days ago', got " . var_export($since, true));
		}
		$this->obsoleteSince = $timestamp;
	}
}
