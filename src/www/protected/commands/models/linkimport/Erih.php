<?php

namespace commands\models\linkimport;

use commands\models\LinksObsolescence;
use commands\models\LoadIssns;

class Erih extends DefaultImport
{
	protected function importLinks(): void
	{
		$csvFilename = $this->downloadFile("https://dbh.nsd.uib.no/publiseringskanaler/erihplus/periodical/listApprovedAsCsv");
		$countIssns = LoadIssns::readFromCsv($csvFilename, 1, 2, 0, ";");
		if ($this->verbose >= 2) {
			fprintf(STDERR, "ISSN lus (MàJ) : %d\n", $countIssns);
		}

		ob_start();
		$source = $this->findSource('erihplus');
		$this->getLinkUpdater()->updateByIssn('https://dbh.nsd.uib.no/publiseringskanaler/erihplus/periodical/info.action?id=%d');
		$changes = ob_get_clean();
		if ($this->getLinkUpdater()->hasChanges()) {
			echo $this->getLinkUpdater()->getStats(); // 1. summary of changes
			echo $changes; // 2. details of changes
		}

		if ($this->obsoleteSince) {
			echo (new LinksObsolescence($source))->listObsoleteLinks($this->obsoleteSince); // 3. obsolete links
		}
	}

	protected function getSubject(): string
	{
		return "Mises à jour ERIH+";
	}
}
