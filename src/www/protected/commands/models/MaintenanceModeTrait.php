<?php

namespace commands\models;

trait MaintenanceModeTrait
{
	public function beforeAction($action, $params)
	{
		if (\Config::read('maintenance.cron.arret')) {
			echo "Cette commande est bloquée par le mode de maintenance du cron.\n";
			exit(1); // send a failure state to the shell
		}
		return parent::beforeAction($action, $params);
	}
}
