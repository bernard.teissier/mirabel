<?php

namespace commands\models;

use Service;

class VanishedServices
{
	public $defaultTo = '';

	public $verbose = 0;

	private $baseUrl = '';

	private $emailTo = [];

	private $sinceTs = 0;

	public function  __construct()
	{
		$this->emailTo = [];
		if (\Yii::app()->params->contains('baseUrl')) {
			$this->baseUrl = \Yii::app()->params->itemAt('baseUrl');
		}
	}

	public function find(\Ressource $r, ?string $collectionIds): void
	{
		$body = "";
		if ($this->sinceTs) {
			$fromDate = $this->sinceTs;
		} else {
			$fromDate = Service::getLastImportTimestamp($r->id);
			$body .= "  Avant-dernière intervention d'import : " . date('Y-m-d H:s', $fromDate) . "\n";
		}
		$services = $this->findDeletedFromImport($r->id, $collectionIds, $fromDate);
		if ($services) {
			foreach ($services as $s) {
				$body .= sprintf("  * %s  —  %s/titre/view/%d\n\taccès %d\n", $s->titre->titre, $this->baseUrl, $s->titreId, $s->id);
			}
		} elseif ($this->verbose > 1) {
			$body .= "  - tous les accès ont été actualisés.\n";
		}
		if ($this->verbose) {
			$list = $this->findNeverImported((int) $r->id, $collectionIds, $fromDate);
			if ($list) {
				$body .= " ### Accès jamais importés\n";
				foreach ($list as $s) {
					\Yii::log(
						sprintf(
							"Accès jamais importé)\nRessource ID=%d\nService (ID=%d, import='%d', hdateImport=%s)\n",
							$r->id,
							$s->id,
							$s->import,
							$s->hdateImport === null ? "NULL" : $s->hdateImport
						),
						\CLogger::LEVEL_INFO,
						'import'
					);
					$body .= sprintf("  * %s\n\t%s/titre/view/%d  accès %d\n", $s->titre->titre, $this->baseUrl, $s->titreId, $s->id);
				}
			}
		}
		if ($this->verbose > 1) {
			$sql = "SELECT s.* FROM Service s "
				. ($collectionIds ? "JOIN Service_Collection sc ON sc.serviceId = s.id " : "")
				. "WHERE s.statut = 'normal' AND s.ressourceId = :rid "
				. ($collectionIds ? "AND sc.collectionId IN ($collectionIds) " : "")
				. " AND s.hdateCreation > :since";
			$list = Service::model()->findAllBySql($sql, ['rid' => $r->id, ':since' => $fromDate]);
			if ($list) {
				$body .= " ### Accès nouveaux\n";
				foreach ($list as $s) {
					$body .= sprintf("  * %s\n\t%s/titre/view/%d  accès %d\n", $s->titre->titre, $this->baseUrl, $s->titreId, $s->id);
				}
			}
		}
		if (empty($body)) {
			return;
		}
		$emailBody = "\n## Ressource {$r->nom}\n$body";
		$importEmails = array_filter(
			array_map(
				function ($x) {
					return trim($x->importEmail);
				},
				$r->getPartenairesSuivant()
			)
		);
		if (!$importEmails && $this->defaultTo) {
			$importEmails = [$this->defaultTo];
		}
		foreach ($importEmails as $e) {
			if (isset($this->emailTo[$e])) {
				$this->emailTo[$e][] = $emailBody;
			} else {
				$this->emailTo[$e] = [$emailBody];
			}
		}
	}

	public function getEmails(): array
	{
		return $this->emailTo;
	}

	public function setSince(string $since): void
	{
		$this->sinceTs = $this->toTimestamp($since);
	}

	/**
	 * @param int $ressourceId
	 * @param null|string $collectionIds
	 * @param int $since
	 * @return Service[]
	 */
	private function findDeletedFromImport(int $ressourceId, ?string $collectionIds, int $since): array
	{
		$sql = "SELECT s.*
FROM
    Service s
    JOIN Titre t ON s.titreId = t.id
	". ($collectionIds ? "JOIN Service_Collection sc ON sc.serviceId = s.id " : "") ."
WHERE
    s.statut = 'normal' AND s.ressourceId = :rid
	" . ($collectionIds ? "AND sc.collectionId IN ($collectionIds) " : "") . "
    AND s.hdateCreation < :sincetoo
    AND s.hdateImport IS NOT NULL AND s.hdateImport < :since
ORDER BY t.titre";
		return Service::model()->findAllBySql($sql, ['rid' => $ressourceId, 'since' => $since, 'sincetoo' => $since]);
	}

	private function findNeverImported(int $ressourceId, ?string $collectionIds): array
	{
			$sql = "SELECT s.*
FROM
    Service s
    JOIN Titre t ON s.titreId = t.id
    " . ($collectionIds ? "JOIN Service_Collection sc ON sc.serviceId = s.id " : "") . "
WHERE
    s.statut = 'normal' AND s.ressourceId = :rid
	" . ($collectionIds ? "AND sc.collectionId IN ($collectionIds) " : "") . "
	AND s.hdateImport IS NULL
ORDER BY t.titre";
			return Service::model()->findAllBySql($sql, ['rid' => $ressourceId]);
	}

	/**
	 * Converts a string date to a timestamp at 00:00 (defaults to 0).
	 *
	 * @param string $since
	 * @throws Exception
	 * @return int
	 */
	private function toTimestamp(string $since): int
	{
		if ($since === '') {
			return 0;
		}
		if (!ctype_digit($since)) {
			$since = strtotime($since);
			if ($since === false) {
				throw new Exception("Mauvais format de date (YYYY-MM-AA attendu), ou mauvais 'timestamp'.");
			}
			if ($this->verbose > 1) {
				echo "Accès supprimés depuis : " . date("Y-m-d H:i", $since) . "\n";
			}
		}
		return $since;
	}
}
