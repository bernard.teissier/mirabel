<?php

namespace commands\models;

use ImportedLink;
use ImportType;
use Titre;
use Yii;

class LinkUpdater
{
	/**
	 * @var int
	 */
	public $simulation = 0;

	/**
	 * @var int
	 * verbose = 1: CSV lists updated but unchanged links
	 * verbose = 2: CSV lists unrecognized Héloïse links
	 */
	public $verbose = 0;

	private $importType;

	/**
	 * @var ?Sourcelien
	 */
	private $defaultSource = null;

	/**
	 * @var Sourcelien[]
	 */
	private $sources = [];

	private $linksErrors = 0;

	private $linksUpdated = 0;

	private $linksCreated = 0;

	private $linksRemoved = 0;

	private $titlesChanged = 0;

	public function __construct(\Sourcelien $source = null)
	{
		if ($source) {
			$this->addSource($source);
		}
	}

	public function addSource(\Sourcelien $source): void
	{
		$this->sources[$source->id] = $source;
		if ($this->defaultSource === null) {
			$this->defaultSource = $source;
		}
		if ($this->importType) {
			if ($this->importType !== ImportType::getSourceId($source->nomcourt)) {
				throw new \Exception("The sources must all have the same import type!");
			}
		} else {
			$this->importType = ImportType::getSourceId($source->nomcourt);
		}
	}

	public function hasChanges(): bool
	{
		return $this->linksCreated > 0 || $this->linksErrors > 0 || $this->linksRemoved || $this->linksUpdated > 0;
	}

	public function getStats(): string
	{
		if ($this->verbose === 0 && !$this->hasChanges()) {
			return "";
		}
		$output = "";
		$multiple = count($this->sources) > 1;
		$output .= sprintf("\n## Import des liens (%s)\n", $multiple ? "sources multiples" : $this->defaultSource->nom);
		if ($multiple && $this->titlesChanged > 0) {
			$output .= sprintf("Titres modifiés : %d\n", $this->titlesChanged);
		}
		if ($this->linksUpdated) {
			$output .= sprintf("Modifications : %d\n", $this->linksUpdated);
		}
		if ($this->linksCreated) {
			$output .= sprintf("Créations : %d\n", $this->linksCreated);
		}
		if ($this->linksRemoved) {
			$output .= sprintf("Suppressions : %d\n", $this->linksRemoved);
		}
		if (!$this->linksUpdated && !$this->linksCreated && !$this->linksRemoved) {
			$output .= sprintf("Aucun changement (ni modification, ni création, ni suppression)\n");
		}
		if ($this->linksErrors) {
			$output .= sprintf("Erreurs : %d\n", $this->linksErrors);
		}
		$output .= sprintf("\n");
		return $output;
	}

	public function removeByIssn(string $csv, string $urlPattern): int
	{
		$countIssns = LoadIssns::readFromCsv($csv, 1, 2, 2, "\t");
		if ($this->verbose >= 2) {
			fprintf(STDERR, "ISSN lus (suppression) : %d\n", $countIssns);
		}
		$titres = Titre::model()->findAllBySql(
			"SELECT t.*, k.id AS confirm FROM Titre t"
			. " JOIN Issn i ON (i.titreId = t.id)"
			. " JOIN knownissn k ON (i.issn = k.issn OR i.issnl = k.issn)"
			. " WHERE k.id != ''"
			. " GROUP BY t.id"
		);
		fprintf(STDERR, "Suppressions possibles : %d\n", count($titres));
		if (!$this->simulation) {
			foreach ($titres as $titre) {
				$issn = $titre->confirm;
				$titre->confirm = true;
				if ($this->removeLink($titre, $issn, sprintf($urlPattern, $issn))) {
					$this->linksRemoved++;
				}
			}
		}
		return count($titres);
	}

	/**
	 * Uses the temp table created by LoadIssn::readFromCsv().
	 *
	 * @param string $urlPattern
	 * @return int number of Titre records updated
	 */
	public function updateByIssn(string $urlPattern): int
	{
		// Find by ISSN and ISSN-L
		$titres = Titre::model()->findAllBySql(
			"SELECT * FROM (
SELECT t.*, k.id AS confirm FROM Titre t JOIN Issn i ON (i.titreId = t.id) JOIN knownissn k ON (i.issn = k.issn) WHERE i.issn IS NOT NULL
UNION
SELECT t.*, k.id AS confirm FROM Titre t JOIN Issn i ON (i.titreId = t.id) JOIN knownissn k ON (i.issnl = k.issn) WHERE i.issn IS NOT NULL
) t
GROUP BY id"
		);
		if ($this->verbose >= 2) {
			fprintf(STDERR, "Titres de Mir@bel concernés : %d\n\n", count($titres));
		}
		if (!$this->simulation) {
			foreach ($titres as $titre) {
				$issn = $titre->confirm;
				$titre->confirm = true;
				if ($issn) {
					echo $this->updateLink($titre, $issn, sprintf($urlPattern, $issn));
				} else {
					Yii::log("Utilisation d'un ISSN vide pour le lien du titre {$titre->id}", \CLogger::LEVEL_WARNING);
				}
			}
		}
		return count($titres);
	}

	/**
	 * @param Titre $titre
	 * @param string $identifier Only used in logs (e.g. list of ISSN numbers)
	 * @param string $url URL attached to this Titre and $this->defaultSource
	 * @return string CSV rows
	 */
	public function updateLink(Titre $titre, string $identifier, string $url): string
	{
		return $this->updateLinks($titre, $identifier, [[(int) $this->defaultSource->id, $url]]);
	}

	/**
	 * @param Titre $titre
	 * @param string $identifier Only used in logs (e.g. list of ISSN numbers)
	 * @param array $urls e.g. [ [sourceID1, URL1], [sourceName2, URL2] ]
	 * @return string CSV rows
	 */
	public function updateLinks(Titre $titre, string $identifier, array $urls): string
	{
		if (empty($urls)) {
			return "";
		}
		if (!isset($urls[0]) || !is_array($urls[0]) || count($urls[0]) !== 2) {
			throw new \Exception("updateLinks() expects URLs like [ [sourceID1, URL1], [sourceID2, URL2] ]");
		}

		$remainingUrls = [];
		foreach ($urls as [$sourceId, $url]) {
			$source = $this->getSource($sourceId);
			$remainingUrls[] = [
				(int) $source->id,
				$url,
				"//" . $source->getDomain() . "/",
			];

			$importedLink = new ImportedLink();
			$importedLink->url = $url;
			$importedLink->sourceId = (int) $source->id;
			$importedLink->titreId = (int) $titre->id;
			$importedLink->save();
		}

		$liens = $titre->getLiens();
		$output = "";
		$linksCreated = 0;
		$linksUpdated = 0;
		foreach ($liens as $lien) {
			/* @var $lien Lien */
			$lien->disableUrlValidation();
			foreach ($remainingUrls as $i => [$sourceId, $url, $pattern]) {
				if ($lien->url !== $url && strpos($lien->url, $pattern) === false) {
					continue;
				}
				$source = $this->getSource($sourceId);
				if ($lien->url === $url && $lien->src === $source->nom) {
					if ($this->verbose > 1) {
						$output .= $this->getCsvlogRow($titre, $identifier, $url, "Lien inchangé", "OK");
					}
				} else {
					// (same URL but different source name) OR (same domain but different URL)
					// So we update in-place the Lien object.
					$lien->src = $source->nom;
					$lien->sourceId = (int) $source->id;
					$lien->url = $url;
					$linksUpdated++;
					$output .= $this->getCsvlogRow($titre, $identifier, $url, "Lien modifié", "OK");
				}
				unset($remainingUrls[$i]);
			}
		}
		// adding the remaining links
		foreach ($remainingUrls as [$sourceId, $url, $_]) {
			$source = $this->getSource($sourceId);
			$liens->add([
				'src' => $source->nom,
				'sourceId' => (int) $source->id,
				'url' => $url,
			]);
			$output .= $this->getCsvlogRow($titre, $identifier, $url, "Lien ajouté", "OK");
			$linksCreated++;
		}
		if ($linksCreated || $linksUpdated) {
			$success = $this->updateTitle($titre, $liens);
			if ($success !== 'OK') {
				$output .= $this->getCsvlogRow($titre, $identifier, '', "enregistrement", $success);
			}
			$this->linksUpdated += $linksUpdated;
			$this->linksCreated += $linksCreated;
			$this->titlesChanged++;
		}
		return $output;
	}

	public function getCsvlogRow(Titre $titre, string $identifier, string $url, string $message, string $status): string
	{
		return join(';', [$titre->revueId, $titre->id, $titre->titre, $identifier, $url, $message, $status]) . "\n";
	}

	public static function getCsvlogHeader(): string
	{
		return join(';', ['Revue-id', 'Titre-id', 'Titre-alpha', 'Identifiant', 'Url', 'Message', 'État']) . "\n";
	}

	private function getSource($idOrName): \Sourcelien
	{
		if (isset($this->sources[$idOrName])) {
			return $this->sources[$idOrName];
		}
		foreach ($this->sources as $s) {
			if ($s->nomcourt === $idOrName) {
				return $s;
			}
		}
		throw new \Exception("updateLinks() received an unexpected source ID '$idOrName'.");
	}

	private function removeLink(Titre $titre, string $issn, string $url): bool
	{
		$liens = $titre->getLiens();
		foreach ($liens as $lien) {
			/* @var $lien Lien */
			if ($lien->url === $url) {
				$liens->remove($lien);
				$success = $this->updateTitle($titre, $liens);
				echo $this->getCsvlogRow($titre, $issn, '', "Lien supprimé", $success);
				$this->linksRemoved++;
				return true;
			}
		}
		return false;
	}

	/**
	 * Update the DB record (with an Intervention), then return "OK" or an error message.
	 */
	private function updateTitle(Titre $titre, \Liens $liens): string
	{
		$titre->setScenario('import');
		$oldTitle = clone $titre;
		$titre->setLiens($liens);
		$success = "OK";
		if (!$this->simulation) {
			$i = $titre->buildIntervention(true);
			$i->setImport($this->importType);
			$i->description = "Modification du titre « {$titre->titre} »";
			$i->contenuJson->update($oldTitle, $titre);
			$i->setScenario('import');
			try {
				if (!$i->accept(true)) {
					$success = "ERREUR " . print_r($i->getErrors(), true);
					$this->linksErrors++;
				}
			} catch (\Exception $e) {
				Yii::app()->db->createCommand("ROLLBACK")->execute();
				Yii::log("updateLink() failed with Titre {$titre->id}: {$e->getMessage()}\nliens: " . print_r($liens, true), \CLogger::LEVEL_ERROR);
				$success = "ERREUR (exception)";
				$this->linksErrors++;
			}
		}
		return $success;
	}
}
