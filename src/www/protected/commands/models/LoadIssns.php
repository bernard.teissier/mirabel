<?php

namespace commands\models;

use Curl;
use Yii;

/**
 * Methods that create and fill a tmp table "knownissn".
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class LoadIssns
{
	public static $verbose = 0;

	public static $maxResults = PHP_INT_MAX;

	/**
	 * Create and fill the temporary table "knownissn".
	 *
	 * @param string $csvFile File name of the CSV data.
	 * @param int $mainCol Column containing the main ISSN
	 * @param int $secondCol Column containing another ISSN
	 * @param int|null $identifierCol Column containing the identifier, or null for using the matching ISSN.
	 * @param string $separator
	 * @return int #ISSN
	 */
	public static function readFromCsv(string $csvFile, int $mainCol, int $secondCol, ?int $identifierCol, $separator): int
	{
		Yii::app()->db
			->createCommand("DROP TABLE IF EXISTS knownissn")
			->execute();
		Yii::app()->db
			->createCommand("CREATE TEMPORARY TABLE knownissn (id VARCHAR(255) NOT NULL COLLATE ascii_bin, issn CHAR(9) NOT NULL COLLATE ascii_bin)")
			->execute();
		$transaction = Yii::app()->db->beginTransaction();
		$insert = Yii::app()->db->createCommand("INSERT INTO knownissn VALUES (?, ?)");
		$count = 0;
		$handle = fopen($csvFile, "r");
		if (!$handle) {
			die("Could not read the CSV file.");
		}
		fgetcsv($handle, 0, $separator); // skip header line
		while (($rawdata = fgetcsv($handle, 0, $separator)) !== false) {
			$data = array_map('trim', $rawdata);
			if ($identifierCol === null) {
				if (!empty($data[$mainCol])) {
					$count++;
					$insert->execute([$data[$mainCol], $data[$mainCol]]);
				}
				if (!empty($data[$secondCol])) {
					$count++;
					$insert->execute([$data[$secondCol], $data[$secondCol]]);
				}
			} else {
				$id = $data[$identifierCol];
				if (empty($id)) {
					if (!empty($data[$secondCol])) {
						$id = $data[$secondCol];
					} elseif (!empty($data[$mainCol])) {
						$id = $data[$mainCol];
					} else {
						continue;
					}
				}
				if (!empty($data[$mainCol])) {
					$count++;
					$insert->execute([$id, $data[$mainCol]]);
				}
				if (!empty($data[$secondCol])) {
					$count++;
					$insert->execute([$id, $data[$secondCol]]);
				}
			}
		}
		$transaction->commit();
		Yii::app()->db->createCommand("CREATE INDEX knownissn_issn ON knownissn (issn)")->execute();
		fclose($handle);
		return $count;
	}

	/**
	 * Create and fill the temporary table "knownissn".
	 *
	 * @param string $xmlfile File name of the MARC21 XML data.
	 * @return int #ISSN
	 */
	public static function readFromMarcxml($xmlfile)
	{
		Yii::app()->db->createCommand("CREATE TEMPORARY TABLE knownissn (id CHAR(9) NOT NULL COLLATE ascii_bin, issn CHAR(9) NOT NULL COLLATE ascii_bin)")
			->execute();
		$count = 0;
		$reader = new \XMLReader();
		if (substr($xmlfile, -3) === ".gz") {
			$xmlstream = "compress.zlib://$xmlfile";
		} else {
			$xmlstream = $xmlfile;
		}
		$reader->open($xmlstream, null, LIBXML_COMPACT | LIBXML_NONET);
		// Cf MARC21: https://www.loc.gov/marc/bibliographic/bd022.html
		$usefulAttributes = ["a" => 1, "l" => 1];
		$transaction = \Yii::app()->db->beginTransaction();
		$insert = Yii::app()->db->createCommand("INSERT INTO knownissn VALUES (?, ?)");
		do {
			$unfinished = true;
			while ($unfinished && $reader->name !== 'ns0:datafield' && $reader->getAttribute("tag") !== '022') {
				$unfinished = $reader->read();
			}
			if ($unfinished) {
				$node = $reader->expand();
				$id = null;
				$issns = [];
				foreach ($node->childNodes as $child) {
					if ($child->nodeType !== XML_ELEMENT_NODE || !($child instanceof \DOMElement)) {
						continue;
					}
					if (
							isset($usefulAttributes[$child->getAttribute("code")])
							&& preg_match('/^\d{4}-\d{3}[\dX]$/', trim($child->textContent))) {
						$count++;
						$issns[] = trim($child->textContent);
						if ($child->getAttribute("code") === "a") {
							$id = trim($child->textContent);
						}
					}
				}
				foreach ($issns as $issn) {
					$insert->execute([$id, $issn]);
				}
			}
		} while ($unfinished && $reader->next('record'));
		$transaction->commit();
		Yii::app()->db->createCommand("CREATE INDEX knownissn_issn ON knownissn (issn)")->execute();
		$reader->close();
		return $count;
	}

	/**
	 * Create and fill the temporary table "knownissn".
	 *
	 * @param Traversable $metadatas
	 * @return int #ISSN
	 */
	public static function readFromArray($metadatas)
	{
		Yii::app()->db
			->createCommand("CREATE TEMPORARY TABLE knownissn (id VARCHAR(30) NOT NULL COLLATE ascii_bin, issn CHAR(9) NOT NULL COLLATE ascii_bin)")
			->execute();
		$transaction = \Yii::app()->db->beginTransaction();
		$insert = Yii::app()->db->createCommand("INSERT INTO knownissn VALUES (?, ?)");
		$count = 0;
		foreach ($metadatas as $metadata) {
			if (isset($metadata['issn'])) {
				$count++;
				foreach ($metadata['issn'] as $issn) {
					$insert->execute([$metadata['id'], $issn]);
				}
			}
		}
		$transaction->commit();
		Yii::app()->db->createCommand("CREATE INDEX knownissn_issn ON knownissn (issn)")->execute();
		return $count;
	}
}
