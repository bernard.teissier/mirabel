<?php

namespace commands\models;

use Sourcelien;
use Yii;

class LinksObsolescence
{
	/**
	 * @var \Sourcelien|null
	 */
	private $sourceLien;

	public function __construct(?Sourcelien $s)
	{
		$this->sourceLien = $s;
	}

	public function detectObsolete(int $since): string
	{
		$condSourcelien = $this->sourceLien === null ? "" : "AND l.sourceId = {$this->sourceLien->id}";
		$query = Yii::app()->db->createCommand(
			<<<EOSQL
			SELECT
				t.*,
				s.nom,
				l.id AS ilId, l.url as lastUrl, FROM_UNIXTIME(l.lastSeen) AS lastSeen,
				lt.hdate AS actualDate
			FROM Titre t
				JOIN ImportedLink l ON l.titreId = t.id
				JOIN Sourcelien s ON s.id = l.sourceId
				LEFT JOIN LienTitre lt ON l.sourceId = lt.sourceId AND l.titreId = lt.titreId AND l.url = lt.url
			WHERE
				l.lastSeen < :timestamp
				$condSourcelien
			ORDER BY t.titre
			EOSQL
		)->query([':timestamp' => $since]);

		$exportedKeys = array_flip(['revueId', 'id', 'titre', 'nom', 'lastUrl', 'lastSeen']);
		$deleteLink = Yii::app()->db->createCommand("DELETE FROM ImportedLink WHERE id = :id");
		$out = fopen('php://temp', 'w');
		foreach ($query as $row) {
			$export = array_intersect_key($row, $exportedKeys);
			if ($row['actualDate']) {
				$export[] = "Supprimé de la source";
			} else {
				$export[] = "Supprimé de Mirabel";
				$deleteLink->execute([':id' => $row['ilId']]);
			}
			fputcsv($out, $export, ";");
		}
		$content = stream_get_contents($out, -1, 0);
		fclose($out);
		return $content;
	}

	/**
	 * Return a list (with title) of links not imported since this timestamp, or "" if none.
	 *
	 * @param int $timestamp
	 * @return string
	 */
	public function listObsoleteLinks(int $timestamp): string
	{
		$content = $this->detectObsolete($timestamp);
		if (!$content) {
			return "";
		}
		return sprintf("\n## Liens obsolètes (pas importés depuis %s)\n%s", date('Y-m-d', $timestamp), $content);
	}

	public function printDetectObsolete(string $since): void
	{
		if (ctype_digit($since)) {
			$timestamp = (int) $since;
		} else {
			$timestamp = strtotime($since);
		}
		if (!$timestamp) {
			return;
		}

		$content = $this->detectObsolete($timestamp);
		if ($content) {
			printf("\n## Liens obsolètes (pas importés depuis %s)\n", date('Y-m-d', $timestamp));
			echo $content;
		}
	}
}
