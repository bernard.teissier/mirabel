<?php

class ExportCommand extends CConsoleCommand
{
	/**
	 * Default action, called when no argument is given.
	 */
	public function actionIndex()
	{
		$sql = "SELECT
	t.id, t.titre, t.dateDebut, t.dateFin, MAX(i.sudocPpn) AS sudoc, MAX(i.worldcatOcn) AS worldcat, GROUP_CONCAT(i.issn) AS issn, GROUP_CONCAT(i.issnl) AS issnl, t.periodicite, t.langues
	, GROUP_CONCAT(DISTINCT CONCAT(r.nom, ' (', s.acces, ' : ', s.dateBarrDebut, '...', s.dateBarrFin, ')') ORDER BY r.nom SEPARATOR ' | ') accès
	, IFNULL(categories.nom, aliases.nom) thématique
FROM
	Titre t
	LEFT JOIN Issn i ON t.id = i.titreId
	LEFT JOIN `Titre_Editeur` te ON te.titreId = t.id
	LEFT JOIN `Editeur` e ON e.id = te.editeurId
	LEFT JOIN Pays p ON e.paysId = p.id
	LEFT JOIN Service s ON s.titreId = t.id AND s.type = 'Intégral'
	LEFT JOIN Ressource r ON r.id = s.ressourceId
	LEFT JOIN (SELECT catr.revueId, GROUP_CONCAT(c.categorie ORDER BY c.categorie SEPARATOR ' | ') AS nom FROM Categorie_Revue catr JOIN Categorie c ON catr.categorieId = c.id GROUP BY catr.revueId) categories USING (revueId)
	LEFT JOIN (SELECT catt.titreId, GROUP_CONCAT(c.categorie ORDER BY c.categorie SEPARATOR ' | ') AS nom FROM CategorieAlias_Titre catt JOIN CategorieAlias cata ON cata.id = catt.categorieAliasId JOIN Categorie c ON cata.categorieId = c.id GROUP BY catt.titreId) aliases ON aliases.titreId = t.id
WHERE
	t.statut = 'normal'
	AND t.langues LIKE '%fre%'
	-- AND paysId = 62 -- France
GROUP BY t.id
ORDER BY revueId, t.titre
   ";
		$titres = Yii::app()->db->createCommand($sql)->queryAll();
		$findEditeurs = Yii::app()->db->createCommand(
			"SELECT
	e.nom, p.nom AS pays
FROM
	Titre_Editeur te
	JOIN Editeur e ON e.id = te.editeurId
	LEFT JOIN Pays p ON e.paysId = p.id
WHERE te.titreId = :titreId
LIMIT 3
"
		);
		fputcsv(STDOUT, array_merge(array_keys($titres[0]), ['editeur1', 'pays1', 'editeur2', 'pays2', 'editeur3', 'pays3']), "\t");
		foreach ($titres as $titre) {
			$editeurs = $findEditeurs->queryAll(true, [':titreId' => $titre['id']]);
			$pos = 1;
			foreach ($editeurs as $e) {
				$titre["editeur$pos"] = $e['nom'];
				$titre["pays$pos"] = $e['pays'];
				$pos++;
			}
			while ($pos < 4) {
				$titre["editeur$pos"] = '';
				$titre["pays$pos"] = '';
				$pos++;
			}
			fputcsv(STDOUT, $titre, "\t");
		}
	}

	public function actionBacon()
	{
		$bacon = Yii::app()->params->itemAt('bacon');
		if (empty($bacon) || !isset($bacon['url']) || !isset($bacon['username']) || !isset($bacon['password'])) {
			die("Les paramètres 'bacon' => ['url' => '', 'username' => '', 'password' => ''] doivent être déclarés dans config/local.php.\n");
		}
		(new BaconUpload($bacon['url'], $bacon['username'], $bacon['password']))->run();
	}
}
