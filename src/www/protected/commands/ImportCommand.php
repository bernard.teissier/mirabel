<?php

/**
 * Import
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class ImportCommand extends CConsoleCommand
{
	use \commands\models\MaintenanceModeTrait;

	public const EMAIL_DEFAULT = 'mirabel_admin@listes.sciencespo-lyon.fr';

	public $verbose = 1;

	public $simulation = false;

	public $url = '';

	private $sources = ['Cairn', 'CairnMagazine', 'Persee'];

	public function getHelp()
	{
		$sources = join(' | ', $this->sources);
		return <<<EOL
			./yii import <source> [--verbose=1] [--simulation=0]
			    avec <source> : all | $sources"

			./yii import kbart --ressource=1 <fichier.tsv>
			    avec les params requis :
			        --ressource=<id>
			        --defautType=<Intégral|Résumé|Sommaire|Indexation>
			    et les params facultatifs (valeurs par défaut) :
			        --collection=<id>
			        --lacunaire=0
			        --selection=0
			        --checkDeletion=1
			        --ssiConnu=0
			        --acces=Libre (ou Restreint)
			        --ignoreUrl=0

			./yii import delete [--ressource=1] [--since=YYYY-MM-DD]
			    avec :
			        --ressource=<id>   Si absent, toutes les ressources en autoImport
			        --since=YYYY-MM-DD Si absent, date du dernier import de chaque ressource

			Paramètres communs à tous les cas :
			        --verbose=1     : 0-concis 1-normal 2-debug
			        --simulation=0
			EOL;
	}

	/**
	 * Default action, called when no argument is given.
	 */
	public function actionIndex()
	{
		echo $this->getHelp();
	}

	/**
	 * Action "all" that imports from every source.
	 */
	public function actionAll()
	{
		foreach ($this->sources as $src) {
			$this->import($src);
		}
	}

	/**
	 * ImportKbart caller.
	 *
	 * @param int $ressource
	 */
	public function actionKbart($defautType='', $ressource=0, $collection=0, $lacunaire=0, $selection=0, $acces='Libre', $ssiConnu=false, $checkDeletion=1, $ignoreUrl=0, $args=[])
	{
		if ($collection > 0) {
			$coll = Collection::model()->findByPk((int) $collection);
			if (!$coll) {
				echo "La collection avec cet ID n'a pas été trouvée !\n";
				return 8;
			}
			if (!$ressource) {
				$ressourceId = $coll->ressourceId;
			} elseif ($ressource == $coll->ressourceId) {
				$ressourceId = (int) $ressource;
			} else {
				echo "Incohérence : la collection est rattachée à la ressource {$coll->ressourceId} et non $ressource !\n";
				return 9;
			}
			$collectionId = (int) $coll->id;
		} else {
			$collectionId = null;
			$ressourceId = (int) $ressource;
		}
		if (!$ressourceId) {
			echo "Il faut un paramètre numérique '--ressource=<id>'.\n\n";
			$this->actionIndex();
			return 1;
		}
		if (!$args) {
			echo "Il manque un fichier en paramètre.\n\n";
			$this->actionIndex();
			return 1;
		}
		$filename = $args[0];
		if (strncmp($filename, 'http', 4) !== 0 && (!file_exists($filename) || !is_readable($filename))) {
			echo "Le fichier '$filename' ne peut pas être ouvert.\n\n";
			$this->actionIndex();
			return 1;
		}

		Yii::import('application.models.import.ImportKbart');
		$params = [
			'url' => $filename,
			'ressourceId' => $ressourceId,
			'collectionId' => $collectionId,
			'defaultType' => $defautType,
			'lacunaire' => $lacunaire,
			'selection' => $selection,
			'ignoreUnknownTitles' => $ssiConnu,
			'acces' => $acces,
			'checkDeletion' => $checkDeletion,
			'ignoreUrl' => $ignoreUrl,
		];
		$import = new ImportKbart($params);
		$import->verbose = $this->verbose;
		$import->simulation = $this->simulation;
		$import->importAll();
		if ($this->verbose > 1 || !$import->log->isEmpty()) {
			echo "Import KBART v2...\n";
			if (strncmp($filename, 'http', 4) === 0) {
				echo "Source : $filename\n";
			}
			echo $import->log->format('string');
			echo "Import KBART...FINI\n";
		}
	}

	/**
	 * Handle accesses that were not imported.
	 */
	public function actionDelete(int $ressource = 0, $since = '', bool $emails = false)
	{
		$finder = new commands\models\VanishedServices();
		$finder->defaultTo = self::EMAIL_DEFAULT;
		$finder->verbose = $this->verbose;
		$finder->setSince($since);

		if ($ressource > 0) {
			$ressources = [Ressource::model()->findByPk((int) $ressource)];
		} else {
			$ressources = Yii::app()->db->createCommand(
				<<<EOSQL
				SELECT r.*, GROUP_CONCAT(c.id) AS collectionIds
				FROM Ressource r
					LEFT JOIN Collection c ON c.ressourceId = r.id
				WHERE r.autoImport = 1 OR c.importee = 1
				GROUP BY r.id
				ORDER BY r.nom ASC
				EOSQL
			)->queryAll();
		}
		foreach ($ressources as $row) {
			$ressource = Ressource::model()->populateRecord($row);
			$collectionIds = $row['collectionIds'];
			$finder->find($ressource, $collectionIds);
		}
		if ($emails) {
			foreach ($finder->getEmails() as $toStr => $bodies) {
				$toList = array_filter(array_map('trim', explode("\n", $toStr)));
				if (count($toList) > 1) {
					$to = $toList;
				} elseif (count($toList) === 1) {
					$to = $toList[0];
				} else {
					$to = self::EMAIL_DEFAULT;
				}
				if ($this->verbose > 1) {
					printf(STDERR, "To: %s\n", join(", ", $toList));
				}
				$message = Mailer::newMail()
					->setSubject(Yii::app()->name . " : import et accès supprimés")
					->setFrom([self::EMAIL_DEFAULT])
					->setTo($to)
					->setBody(join("\n", $bodies));
				if ($to !== self::EMAIL_DEFAULT) {
					$message->setCc(self::EMAIL_DEFAULT);
				}
				Mailer::sendMail($message);
			}
		} else {
			foreach ($finder->getEmails() as $to => $bodies) {
				echo Yii::app()->name . " : import et accès supprimés\n\n# To: $to\n" . join("\n", $bodies) . "\n-----\n\n";
			}
		}
	}

	public function actionRevert($where = "")
	{
		$interventions = Intervention::model()->findAll($where);
		if (!$interventions) {
			echo "Aucune intervention ne correspond.\n";
			return 0;
		}
		if (!$this->confirm("Annuler puis supprimer " . count($interventions) . " interventions ? ")) {
			echo "Annulation.\n";
			return 0;
		}
		foreach ($interventions as $i) {
			/** @var Intervention $i */
			if ($i->revert()) {
				if ($i->delete()) {
					fputcsv(STDOUT, [$i->id, $i->revueId, "Annulée et supprimée"], ';');
				} else {
					fputcsv(STDOUT, [$i->id, $i->revueId, "Annulée SANS suppression"], ';');
				}
			} else {
				fputcsv(STDOUT, [$i->id, $i->revueId, "ERREUR: " . print_r($i->getErrors(), true)], ';');
			}
		}
		return 0;
	}

	/**
	 * Intercepts the action call so that source names can be redirected to import().
	 *
	 * @param array $args
	 */
	public function run($args)
	{
		list($action, $options) = $this->resolveRequest($args);
		if ($action) {
			$this->verbose = $options['verbose'] ?? 1;
			$this->simulation = isset($options['simulation']) ? (boolean) $options['simulation'] : false;
			$this->url = $options['url'] ?? null;
			$source = ucfirst(strtolower($action));
			foreach ($this->sources as $s) {
				if (strcasecmp($source, $s) == 0) {
					return $this->import($s);
				}
			}
		}
		return parent::run($args);
	}

	/**
	 * The actual instantiation and call of import.
	 *
	 * @param string $source Among $this->sources.
	 */
	protected function import($source)
	{
		Yii::import('application.models.import.Import' . $source);
		$className = 'Import' . $source;
		$params = [
			'url' => $this->url,
		];
		$import = new $className($params);
		/* @var $import Import */
		$import->verbose = $this->verbose;
		$import->simulation = $this->simulation;
		$import->importAll();
		if ($this->verbose > 1 || !$import->log->isEmpty()) {
			echo $source, "...\n";
			echo $import->log->format('string');
			echo $source, "...FINI\n";
		}
	}
}
