<?php

/**
 * Yiic command to delete assets and cache
 */
class ClearCacheCommand extends CConsoleCommand
{
	/**
	 * clear cache and assets folder
	 *
	 * @param string $cacheID =cache name of property in configuration, comma seperated for multiple caches
	 * @param string $assetPath =null defaults to /assets
	 * @return void
	 */
	public function actionAll($cacheID = 'cache', $assetPath = null)
	{
		$this->actionCache($cacheID);
		$this->actionAssets($assetPath);
	}

	/**
	 * flush cache
	 *
	 * @param string $cacheID =cache name of property in configuration, comma seperated for multiple caches
	 * @return void
	 */
	public function actionCache($cacheID = 'cache')
	{
		printf("Running action cache...\n");
		Yii::app()->db->schema->refresh();
		printf("\tSchema cache cleared...\n");
		$cacheIDs = explode(',', $cacheID);
		foreach ($cacheIDs as $cache) {
			$cache = trim($cache);
			if (Yii::app()->hasComponent($cache)) {
				if (Yii::app()->{$cache} instanceof CApcCache) {
					printf("\tCache '%s' is CApcCache and therefore unsupported.\n", $cache);
				} elseif (Yii::app()->{$cache}->flush()) {
					printf("\tCache '%s' cleared.\n", $cache);
				} else {
					printf("\tClear '%s' cache failed.\n", $cache);
				}
			} else {
				printf("\tNo cache configured for '%s'.\n", $cache);
			}
		}
	}

	/**
	 * clear assets folder
	 *
	 * @param string $assetPath =null defaults to /assets
	 * @return void
	 */
	public function actionAssets($assetPath = null)
	{
		printf("Running action assets...\n");
		if ($assetPath === null) {
			// CLI, so the webroot alias is not what we want.
			$assetPath = realpath(dirname(Yii::getPathOfAlias('webroot')) . DIRECTORY_SEPARATOR . 'assets');
			printf("No asset path submitted. Assuming '%s'.\n", $assetPath);
		}
		if (!is_dir($assetPath)) {
			printf("Submitted asset path '%s' is invalid.\n", $assetPath);
		} else {
			$dirs = self::collectAssetTargets($assetPath);
			printf("Found %d directories.\n", count($dirs));
			if (!empty($dirs)) {
				foreach ($dirs as $dir) {
					echo "    - $dir\n";
				}
				if (!$this->confirm("Continue deleting assets?\n")) {
					return; // no exit, otherwise chained command will not be executed
				}
				// inverse order of directories to avoid can not delete non-empty directory
				foreach ($dirs as $dir) {
					if (!self::unlinkRecursive($dir)) {
						printf("Deleting of asset dir '%s' failed.\n", $dir);
					}
				}
			}
		}
		printf("Done.\n");
	}

	/**
	 * recurse into given path and collect files and folders
	 *
	 * @param string $assetPath
	 * @return array dirs
	 */
	protected static function collectAssetTargets($assetPath)
	{
		$assetfiles = glob($assetPath . DIRECTORY_SEPARATOR . "*");
		$dirs = [];
		foreach ($assetfiles as $assetFile) {
			if (strncmp($assetFile, ".", 1) === 0 || basename($assetFile) === 'misc') {
				continue;
			}
			if (is_dir($assetFile)) {
				$dirs[] = $assetFile;
			}
		}
		return $dirs;
	}

	protected static function unlinkRecursive($path)
	{
		if (is_dir($path)) {
			$dh = @opendir($path);
			if (!$dh) {
				return false;
			}
			$success = true;
			while (($obj = readdir($dh)) !== false) {
				$subpath = $path . DIRECTORY_SEPARATOR . $obj;
				if ($obj !== '.' && $obj !== '..') {
					if (is_dir($subpath)) {
						$success = $success && self::unlinkRecursive($subpath);
					} else {
						$success = $success && unlink($subpath);
					}
				}
			}
			closedir($dh);
			return $success && rmdir($path);
		}
		return unlink($path);
	}
}
