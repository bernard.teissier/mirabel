<?php
require_once Yii::getPathOfAlias('application.models.import') . '/Normalize.php';

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class UpdateCommand extends CConsoleCommand
{
	public $verbose = 1;

	public $simulation = false;

	public $limit;

	/**
	 * Corrige les interventions de création qui ne sont pas liées aux objets créés.
	 */
	public function actionFixInterventions()
	{
		$interventions = Intervention::model()
			->findAll("action = 'revue-C' AND `revueId` IS NULL AND hdateVal IS NOT NULL AND statut = 'accepté'");
		foreach ($interventions as $i) {
			/* @var $i Intervention */
			$log = [$i->id, ""];
			if (isset($i->contenuJson->lastInsertId['Revue']) && Revue::model()->exists("id = " . $i->contenuJson->lastInsertId['Revue'])) {
				$i->revueId = (int) $i->contenuJson->lastInsertId['Revue'];
				$log[1] .= "Revue {$i->revueId}, ";
			}
			if (isset($i->contenuJson->lastInsertId['Titre']) && Titre::model()->exists("id = " . $i->contenuJson->lastInsertId['Titre'])) {
				$i->titreId = (int) $i->contenuJson->lastInsertId['Titre'];
				$log[1] .= "Titre {$i->titreId}, ";
			}
			if ($log[1]) {
				if (!$i->update()) {
					fprintf(STDERR, print_r($i->getErrors(), true));
				}
				echo join(";", $log) . "\n";
			}
		}
	}

	/**
	 */
	public function actionFindServiceUpdates()
	{
		echo "revueId;serviceId\n";
		$interventions = Intervention::model()
			->findAll("import > 0 AND (action IS NULL OR action <=> 'service-U') AND contenuJson LIKE '%\"import\":\"0\"%' ORDER BY revueId, titreId");
		foreach ($interventions as $i) {
			/* @var $i Intervention */
			foreach ($i->contenuJson->toArray() as $operation) {
				if ($operation['model'] === 'Service' && $operation['operation'] === 'update'
					&& isset($operation['before']['import']) && $operation['before']['import'] == '0'
					&& isset($operation['after']['import']) && $operation['after']['import'] > 0
					) {
					echo $i->revueId, ";", $operation['id'], "\n";
				}
			}
		}
	}
}
