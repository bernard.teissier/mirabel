<?php

/**
 * Alert by e-mail
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class AlertCommand extends CConsoleCommand
{
	public $verbose = 1;

	public $simulation = false;

	public $since = 0;

	public $before = 0;

	public $cc = '';

	public $ccToPartEd = '';

	public $ccToPartVe = '';

	/**
	 * Default action, called when no argument is given.
	 */
	public function actionIndex()
	{
		echo $this->getHelp();
	}

	public function getHelp()
	{
		return <<<EOL
			./yii alert who
			    Liste les partenaires faisant du suivi.
			./yii alert all [--simulation=1] [--since=2012-10-25]
			    Envoie un courriel à chaque établissement ayant des interventions en attente.
			    Idem pour les objets non suivis.
			./yii alert <nom|id> [--simulation=1] [--since=2012-10-25]
			    Envoie un courriel à l'établissement s'il a des interventions en attente.
			./yii alert nonsuivi
			    Envoie un courriel aux utilisateurs surveillant les interventions en attente non suivies.
			./yii alert partenairesEditeurs
			    Envoie un courriel aux utilisateurs surveillant les interventions des partenaires-éditeurs.

			Options :
			    --simulation=1 : ne pas envoyer de courriel, afficher dans le terminal.
			    --since=<date> : interventions postérieures à cette date (par défaut, les dernières 24h).
			    --verbose=<?>  : fixe la verbosité des messages dans la console (par défaut, 1).
			    --ccToPartVe=<email> : (suivi/part) copie des alertes dont le destinataire est un partenaire veilleur
			    --ccToPartEd=<email> : (suivi/part) copie des alertes dont le destinataire est un partenaire éditeur
			    --cc=<email>   : copie des alertes, inactif pour le suivi si un des --ccTo* est défini.

			Exemples :
			* Pour envoyer à chaque partenaire (ayant une adresse e-mail)
			  les propositions en attente créée dans la journée :
			      ./yii alert all
			  équivalent à :
			      php yii alert all --since="1 day ago"
			* Pour voir dans la console ce qui serait envoyé à Sciences Po Lyon
			  si on demandait les propositions depuis la mi-octobre 2012 :
			      ./yii alert "Sciences Po Lyon" --simulation=1 --verbose=2 --since="2012-10-15 12:00"

			EOL;
	}

	public function beforeAction($action, $params)
	{
		$this->since = $this->toTimestamp($this->since);
		$this->before = $this->before ? $this->toTimestamp($this->before) : '';
		return parent::beforeAction($action, $params);
	}

	/**
	 * Action "all" that alerts everyone.
	 */
	public function actionAll()
	{
		$this->actionSuivi();
		$this->actionNonSuivi();
		$this->actionPartenairesEditeurs();
	}

	public function actionSuivi()
	{
		$byInstitute = Suivi::listInterventionsByInstitute((int) $this->since, (int) $this->before);
		if ($this->verbose > 1 || ($byInstitute && $this->verbose > 0)) {
			echo "## Alertes pour les objets suivis.\n";
			echo "Établissements concernés : " . count($byInstitute) . "\n";
		}
		foreach ($byInstitute as $pid => $interventions) {
			$partenaire = Partenaire::model()->findByPk($pid);
			if ($this->ccToPartEd || $this->ccToPartVe) {
				if ($this->ccToPartEd && $partenaire->editeurId) {
					$cc = $this->ccToPartEd;
				} elseif ($this->ccToPartVe && !$partenaire->editeurId) {
					$cc = $this->ccToPartVe;
				} else {
					$cc = "";
				}
			} else {
				$cc = $this->cc;
			}
			$this->sendEmail([$partenaire], $interventions, "default", $cc);
		}
	}

	/**
	 * Action "who" that lists the possible names.
	 */
	public function actionWho()
	{
		echo "Partenaires faisant du suivi :\n";
		$cmdSuivi = Yii::app()->db->createCommand(
			"SELECT p.id, p.nom "
			. "FROM Partenaire p JOIN Suivi s ON (s.partenaireId = p.id) "
			. "GROUP BY p.id ORDER BY p.nom"
		);
		foreach ($cmdSuivi->query() as $row) {
			printf(" * %3d - %s\n", $row['id'], $row['nom']);
		}
		echo "Utilisateurs en charge des objets non suivis :\n";
		$cmdNonSuivi = Yii::app()->db->createCommand(
			"SELECT u.id, u.login, u.nomComplet "
			. "FROM Utilisateur u "
			. "WHERE suiviNonSuivi = 1 ORDER BY u.nom"
		);
		foreach ($cmdNonSuivi->query() as $row) {
			printf(" * %20s - %s\n", $row['login'], $row['nomComplet']);
		}
	}

	/**
	 * Action "nonsuivi".
	 */
	public function actionNonSuivi()
	{
		$interventions = Suivi::listInterventionsNotTracked("attente", (int) $this->since, (int) $this->before);
		if (!$interventions) {
			if ($this->verbose > 1) {
				echo "\n## Alertes pour les objets non suivis.\n";
				echo "Pas d'intervention en attente qui ne soit suivie.\n";
				return 0;
			}
		}
		if ($this->verbose > 1) {
			echo "\n## Alertes pour les objets non suivis.\n";
		}

		$users = Utilisateur::model()->findAllByAttributes(['suiviNonSuivi' => 1, 'actif' => 1]);
		if (!$users) {
			if ($this->verbose > 0) {
				echo "Pas de destinataire.\n";
			}
			return 1;
		}
		if ($this->verbose > 1) {
			echo "Utilisateurs concernés : " . count($users) . "\n";
		}

		$this->sendEmail($users, $interventions, "non-suivi", $this->cc);
		return 0;
	}

	/**
	 * Action "partenaires-editeurs" will send an email about each recent Intervention pending validation
	 * that was proposed by an user belonging to a Partenaire-Éditeur.
	 */
	public function actionPartenairesEditeurs()
	{
		$interventions = Suivi::listInterventionsPartenairesEditeurs((int) $this->since, (int) $this->before);
		if (!$interventions) {
			if ($this->verbose > 1) {
				echo "\n## Alertes pour les propositions des partenaires-éditeurs.\n";
				echo "Pas d'intervention en attente.\n";
			}
			return 0;
		}
		if ($this->verbose) {
			echo "\n## Alertes pour les propositions des partenaires-éditeurs.\n";
		}

		$users = Utilisateur::model()->findAllByAttributes(['suiviPartenairesEditeurs' => 1, 'actif' => 1]);
		if (!$users) {
			if ($this->verbose) {
				echo "Pas de destinataire.\n";
			}
			return 1;
		}
		if ($this->verbose > 0) {
			echo "Utilisateurs concernés : " . count($users) . "\n";
		}

		$this->sendEmail($users, $interventions, "partenaires-editeurs", $this->cc);
		return 0;
	}

	/**
	 * Intercepts the action call so that Partenaire name/ID can be redirected.
	 *
	 * @param array $args
	 */
	public function run($args)
	{
		list($action, $options) = $this->resolveRequest($args);
		if ($action) {
			if (method_exists($this, 'action' . $action)) {
				return parent::run($args);
			}
			if (ctype_digit($action)) {
				$p = Partenaire::model()->findByPk($action);
			} else {
				$p = Partenaire::model()->findByAttributes(['nom' => $action]);
			}
			if (!$p) {
				echo "Aucun partenaire ne correspond.\n\n";
				echo $this->help;
				return 1;
			}
			$this->verbose = $options['verbose'] ?? 1;
			$this->simulation = isset($options['simulation']) ? (boolean) $options['simulation'] : false;
			$this->since = $options['since'] ?? '';
			$this->before = $options['before'] ?? '';
			return $this->alertOnly($p);
		}
		return parent::run($args);
	}

	protected function alertOnly(Partenaire $partenaire): int
	{
		if ($this->verbose) {
			echo "Alertes pour {$partenaire->nom}.\n";
		}
		$byInstitute = Suivi::listInterventionsByInstitute((int) $this->since, (int) $this->before);
		if (empty($byInstitute[$partenaire->id])) {
			if ($this->verbose > 1) {
				echo "Rien de nouveau.\n";
			}
			return 0;
		}
		$this->sendEmail([$partenaire], $byInstitute[$partenaire->id]);
		return 0;
	}

	protected function toTimestamp($since)
	{
		if (!$since) {
			$since = time() - 86400; // = 24*60*60 = s/day
		}
		if (!is_integer($since) && !ctype_digit($since)) {
			$since = strtotime($since);
			if ($since === false) {
				throw new Exception("Wrong date or timestamp.");
			}
		}
		return $since;
	}

	/**
	 * Sends an email about these interventions.
	 *
	 * @param Partenaire[]|Utilisateur[] $dest Array of Partenaire|Utilisateur, required attributes: "email" and "nom".
	 * @param Intervention[] $interventions
	 * @param string $msgCategory
	 * @param string|array $cc
	 * @return bool
	 */
	protected function sendEmail($dest, $interventions, $msgCategory = "default", $cc = null)
	{
		if (!$dest || !$interventions) {
			return false;
		}
		$emails = [];
		foreach ($dest as $d) {
			assert(($d instanceof Partenaire) || ($d instanceof Utilisateur));
			if (empty($d->email)) {
				echo "Pas d'adresse e-mail pour {$d->nom}, alerte annulée pour ce destinataire.\n";
				continue;
			}
			$destEmails = strpos($d->email, "\n") ? array_filter(array_map('trim', explode("\n", $d->email))) : [$d->email];
			$destName = null;
			if (!empty($d->nomComplet)) {
				$destName = $d->nomComplet;
			} elseif (!empty($d->sigle)) {
				$destName = $d->sigle;
			} elseif (!empty($d->nom)) {
				$destName = $d->nom;
			}
			foreach ($destEmails as $e) {
				if ($destName) {
					$emails[$e] = $destName;
				} else {
					$emails[] = $e;
				}
			}
		}
		if (empty($emails)) {
			echo "Aucun destinataire pour cette alerte, annulation.\n";
			return false;
		}

		$subject = Config::read("alerte.$msgCategory.subject");
		if (!$subject) {
			$subject = "Propositions à traiter dans Mir@bel : ";
		}
		$subject = "[" . Yii::app()->name . "] " . $subject
			. count($interventions)
			. " ("
			. ($this->before ? 'rappel' : date('Y-m-d', $this->since))
			. ")";
		$body = Config::read("alerte.$msgCategory.body");
		if (!$body) {
			$body = "Bonjour,\n
une proposition de mise à jour a été faite sur une revue ou ressource que vous suivez dans Mir@bel.

%INTERVENTIONS%
Merci d'avance du suivi de cette proposition (validation ou rejet et réponse éventuelle).\n
Ceci est un message automatique généré par Mir@bel. Pour toute question : contact@reseau-mirabel.info
";
		}
		$content = '';
		foreach ($interventions as $i) {
			$content .= $this->getInterventionText($i);
		}
		$body = str_replace('%INTERVENTIONS%', $content, $body);

		if ($this->simulation) {
			$to = $emails[0] ?? reset($emails);
			if (count($emails) > 1) {
				$to .= "… (" . count($emails) . " destinataires)";
			}
			if ($this->verbose) {
				echo "To: {$to}\n"
					. ($cc ? "Cc: $cc\n" : "")
					. "$subject\n~~~~\n$body~~~~\n";
			} else {
				echo "To: {$to}\n"
					. ($cc ? "Cc: $cc\n" : "")
					. "    $subject\n";
			}
			return true;
		}
		$from = Config::read('email.from.alertes');
		if (!$from) {
			$from = Config::read('email.from');
		}
		$message = Mailer::newMail()
			->setSubject($subject)
			->setFrom([$from => "Utilisateur Mir@bel"])
			->setTo($emails)
			->setBody($body);
		if ($cc) {
			$message->setCc($cc);
		}
		return Mailer::sendMail($message);
	}

	protected function getInterventionText(Intervention $i)
	{
		if (isset($_SERVER['SERVER_NAME'])) {
			$url = Yii::app()->createAbsoluteUrl('intervention/view', ['id' => $i->id]);
		} elseif (Yii::app()->params->contains('baseUrl')) {
			$url = Yii::app()->params->itemAt('baseUrl') . '/intervention/view?id=' . $i->id;
		} else {
			$url = 'Intervention : ' . $i->id;
		}
		return $i->description . ($i->import ? ' (import)' : '')
			. "\n$url\n"
			. Yii::app()->getDateFormatter()->formatDateTime($i->hdateProp) . "\n\n";
	}
}
