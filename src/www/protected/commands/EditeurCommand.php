<?php

class EditeurCommand extends CConsoleCommand
{
	public function actionExportForIdref($asCsv = false)
	{
		$sql = <<<EOSQL
			SELECT
			    e.id, CONCAT(e.prefixe, TRIM(SUBSTRING_INDEX(SUBSTRING_INDEX(e.nom, '=', 1), ' - ', 1))) AS nomComplet, e.prefixe, e.nom, e.sigle, e.url, e.idref,
			    Pays.nom AS pays, Pays.code2 AS paysAlpha2, Pays.code AS paysAlpha3,
			    SUM(t.obsoletePar IS NULL AND dateFin = '') AS nbTitresVivants,
			    count(DISTINCT t.id) AS nbTitres,
			    count(DISTINCT t.revueId) AS nbRevues
			FROM Editeur e
			    LEFT JOIN Pays ON Pays.id = e.paysId
			    LEFT JOIN Titre_Editeur te ON te.editeurId = e.id
			    LEFT JOIN Titre t ON t.id = te.titreId
			WHERE e.statut = 'normal'
			GROUP BY e.id
			EOSQL;
		$header = false;
		$result = [];
		$reader = Yii::app()->db->createCommand($sql)->query();
		foreach ($reader as $row) {
			$row['id'] = (int) $row['id'];
			$row['nom'] = preg_split('/\s*=\s*|\s+-\s+/', $row['nom']);
			$row['titresTousMorts'] = !$row['nbTitresVivants'];
			$row['nbRevues'] = (int) $row['nbRevues'];
			$row['nbTitres'] = (int) $row['nbTitres'];
			unset($row['nbTitresVivants']);
			$row['liensDeTitres'] = Yii::app()->db
				->createCommand(
					<<<EOSQL
					SELECT lt.name
					FROM Editeur e
						JOIN Titre_Editeur te ON te.editeurId = e.id
						JOIN LienTitre lt USING(titreId)
					WHERE
						e.id = {$row['id']}
						AND lt.sourceId IN (1, 13, 21, 31)
					GROUP BY lt.name
					EOSQL
				)->queryColumn();
			$row['acces'] = Yii::app()->db
				->createCommand(
					<<<EOSQL
					SELECT r.nom
					FROM Editeur e
						JOIN Titre_Editeur te ON te.editeurId = e.id
						JOIN Service s USING(titreId)
						JOIN Ressource r ON s.ressourceId = r.id
					WHERE
						e.id = {$row['id']}
						AND r.id IN (22, 3, 4, 1864)
					GROUP BY r.id
					EOSQL
				)->queryColumn();
			$row['ppnTitresVivants'] = Yii::app()->db
				->createCommand(
					<<<EOSQL
					SELECT DISTINCT i.sudocPpn
					FROM Editeur e
						JOIN Titre_Editeur te ON te.editeurId = e.id
						JOIN Titre t ON t.id = te.titreId AND t.obsoletePar IS NULL AND t.dateFin = ''
						JOIN Issn i USING(titreId)
					WHERE
						e.id = {$row['id']}
						AND i.sudocPpn IS NOT NULL
					GROUP BY te.titreId
					EOSQL
				)->queryColumn();
			$row['ppnTitresMorts'] = Yii::app()->db
				->createCommand(
					<<<EOSQL
					SELECT DISTINCT i.sudocPpn
					FROM Editeur e
						JOIN Titre_Editeur te ON te.editeurId = e.id
						JOIN Titre t ON t.id = te.titreId AND (t.obsoletePar IS NOT NULL OR t.dateFin <> '')
						JOIN Issn i USING(titreId)
					WHERE
						e.id = {$row['id']}
						AND i.sudocPpn IS NOT NULL
					GROUP BY te.titreId
					EOSQL
				)->queryColumn();
			if ($asCsv) {
				if (empty($header)) {
					fputcsv(STDOUT, array_keys($row), "\t");
					$header = true;
				}
				foreach($row as $k => $v) {
					if (is_array($v)) {
						$row[$k] = join(";", $v);
					}
				}
				fputcsv(STDOUT, $row, "\t");
			} else {
				$result[] = array_filter($row);
			}
		}
		if (!$asCsv) {
			echo json_encode($result, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
		}
	}

	public function actionExportJson()
	{
		$sql = <<<EOSQL
			SELECT
			    e.id, e.prefixe, e.nom, e.sigle, e.description, e.url,
			    Pays.nom AS pays,
			    e.geo,
			    count(DISTINCT t.id) AS nbTitres_M,
			    count(DISTINCT t.revueId) AS nbRevues_M,
			    GROUP_CONCAT(t.id) AS titres_M
			FROM Editeur e
			    LEFT JOIN Pays ON Pays.id = e.paysId
			    LEFT JOIN Titre_Editeur te ON te.editeurId = e.id
			    LEFT JOIN Titre t ON t.id = te.titreId
			WHERE e.statut = 'normal'
			GROUP BY e.id
			EOSQL;
		$result = [];
		$reader = Yii::app()->db->createCommand($sql)->query();
		foreach ($reader as $row) {
			$row['id'] = (int) $row['id'];
			$row['nbTitres_M'] = (int) $row['nbTitres_M'];
			$row['nbRevues_M'] = (int) $row['nbRevues_M'];
			$row['titres_M'] = array_map('intval', explode(',', $row['titres_M']));
			$result[] = array_filter($row);
		}
		echo json_encode($result, JSON_PRETTY_PRINT);
	}

	/**
	 * Identifie les éditeurs par le webservice d'idref.fr et produit un CSV.
	 */
	public function actionIdref()
	{
		$editeurs = Yii::app()->db
			->createCommand(
				<<<EOSQL
				SELECT e.id, LOWER(e.nom) AS nom, p.code2 AS alpha2 FROM Editeur e JOIN Pays p ON e.paysId = p.id WHERE e.idref IS NULL ORDER BY e.nom
				EOSQL
			)->queryAll();
		foreach ($editeurs as $e) {
			$countryCode = $e['alpha2'];
			$name = str_replace(' ', '\ ', $e['nom']);
			$q = rawurlencode(join(
				" AND ",
				[
					"corpname_s:{$name}", // le nom contient exactement
					"d102_s:{$countryCode}",
					"recordtype_z:b", // type Collectivité
				]
			));
			$url = "https://www.idref.fr/Sru/Solr?wt=json&indent=on&rows=2&fl=ppn_z,affcourt_z,&q=($q)";
			$raw = json_decode(file_get_contents($url));
			if (empty($raw->response)) {
				fputcsv(STDOUT, [$e['id'], "NO RESPONSE", $e['nom']], ";");
				continue;
			}
			$response = $raw->response;
			if (empty($response->numFound)) {
				fputcsv(STDOUT, [$e['id'], '', "NOT FOUND", $e['nom']], ";");
			} elseif ($response->numFound > 1) {
				fputcsv(STDOUT, [$e['id'], '', "MULTIPLE", $e['nom']], ";");
			} else {
				fputcsv(STDOUT, [$e['id'], $response->docs[0]->ppn_z, "OK", $e['nom']], ";");
			}
			usleep(10000);
		}
	}

	/**
	 * Update Editeur.idref from a CSV "Editeur.id;Editeur.idref".
	 *
	 * @param string $csv file name
	 */
	public function actionIdrefInterventions(string $csv)
	{
		$handle = fopen($csv, "r");
		$count = 0;
		while ($row = fgetcsv($handle, 0, ";")) {
			$idref = $row[1];
			if (!$idref) {
				continue;
			}

			$editeur = Editeur::model()->findByPk($row[0]);
			if ($editeur === null || $editeur->idref === $idref) {
				// no editeur or same idref
				continue;
			}
			if ($editeur->idref) {
				// idref changes!
				fputcsv(STDOUT, [$editeur->id, $editeur->idref, $idref], ";");
			} else {
				$editeur->idref = null;
			}

			$i = $editeur->buildIntervention(true);
			$i->commentaire = "idref.fr";
			$now = time();
			$i->hdateProp = $now;
			$i->contenuJson->updateByAttributes($editeur, ['idref' => $idref]);
			$i->import = ImportType::getSourceId('idref.fr');
			$i->accept(true);
			$count++;
		}
		fprintf(STDERR, "$count enregistrements\n");
	}

	public function actionRelationsIncompletes()
	{
		fputcsv(STDOUT, ['titre', 'ID titre', 'ID revue', 'ISSNP', 'ISSNE', 'PPN', 'obsolète par', 'date debut', 'date fin', 'url titre', 'ID éditeur', 'éditeur', 'français', 'autres éditeurs'], ";");
		$dataReader = Yii::app()->db->createCommand(
			<<<EOSQL
			SELECT
				t.titre,
				t.id AS 'ID titre',
				t.revueId AS 'ID revue',
				ip.issn AS ISSNP,
				ie.issn AS ISSNE,
				IFNULL(ip.sudocPpn, ie.sudocPpn) AS 'PPN',
				t.obsoletePar AS 'obsolète par',
				t.dateDebut,
				t.dateFin,
				t.url AS 'url titre',
				e.id AS 'ID editeur',
				e.nom AS editeur,
				IF(e.paysId = 62, 'français', '') AS 'français',
				GROUP_CONCAT(e2.nom SEPARATOR ' + ') AS 'autres éditeurs'
			FROM
				Titre_Editeur te
				JOIN Titre t ON t.id = te.titreId
				JOIN Editeur e ON e.id = te.editeurId
				LEFT JOIN Issn ip ON ip.titreId = te.titreId AND ip.support = 'papier'
				LEFT JOIN Issn ie ON ie.titreId = te.titreId AND ie.support = 'electronique'
				LEFT JOIN Titre_Editeur te2 ON te2.titreId = te.titreId AND te2.editeurId <> te.editeurId
				LEFT JOIN Editeur e2 ON e2.id = te2.editeurId
			WHERE
				te.ancien IS NULL
			GROUP BY te.titreId, te.editeurId
			ORDER BY t.titre
			EOSQL
		)->query();
		$dataReader->setFetchMode(PDO::FETCH_NUM);
		foreach ($dataReader as $row) {
			fputcsv(STDOUT, $row, ";");
		}
	}
}
