<?php

use models\sudoc\Api;
use models\sudoc\Notice;

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class SudocCommand extends CConsoleCommand
{
	use \commands\models\MaintenanceModeTrait;

	public $verbose = 1;

	public $simulation = false;

	public $limit;

	private static $csvColumns = 0;

	private static $csvSeparator = "\t";

	/**
	 * Interroge le service "meta" du SUDOC et produit un CSV d'informations.
	 *
	 * @param string $ids (facultatif) Une liste d'ID (ex: 1-22) de revues sur lesquelles appliquer l'interrogation du SUDOC.
	 */
	public function actionMeta($ids = '')
	{
		fputcsv(STDOUT, ["Revue ID", "PPN", "ISSN", "Titre", "Dans Mir@bel ?", "Relations"], ";");
		$sql = "SELECT t.revueId, GROUP_CONCAT(i.issn SEPARATOR ',') AS titres, MAX(IF(t.obsoletePar IS NULL, MAX(i.sudocPpn), 0)) AS ppn"
			. " FROM Titre t LEFT JOIN Issn i ON i.titreId = t.id GROUP BY t.id";
		$m = [];
		if ($ids && preg_match('/^(\d+)-(\d+)$/', $ids, $m)) {
			$sql .= " WHERE t.revueId BETWEEN $m[1] AND $m[2]";
		}
		$sql .= " GROUP BY t.revueId";
		$query = Yii::app()->db->createCommand($sql);
		foreach ($query->query() as $row) {
			$issns = [];
			foreach (explode(',', $row['titres']) as $issn) {
				$issns[$issn] = true;
			}
			$graph = extensions\gefx\GefxGraph::fromPpn(trim($row['ppn']));
			if (!$graph) {
				sleep(1);
				continue;
			}
			foreach ($graph->nodes as $node) {
				/* @var $node extensions\gefx\GefxNode */
				if ($node->getIssn() && !isset($issns[$node->getIssn()])) {
					// Ce titre n'est pas dans cette revue de Mir@bel
					$titre = Titre::findByIssn($node->getIssn());
					$relations = $graph->getRelations($node->id);
					$log = [
						$row['revueId'],
						$row['ppn'],
						$node->getIssn(),
						$node->label,
						($titre ? "ISSN présent, revue " . $titre->revueId : "ISSN inconnu de M"),
						join(" // ", $relations),
					];
					fputcsv(STDOUT, $log, ";");
				}
			}
			usleep(10000);
		}
	}

	/**
	 * Check that the PPN in M is right and detects duplicate PPNs.
	 */
	public function actionCheck()
	{
		self::$csvColumns = 4;
		$sudocApi = new Api();
		$rows = Yii::app()->db->createCommand(
			"SELECT issn, sudocPpn FROM Issn WHERE issn IS NOT NULL" . ($this->limit ? " LIMIT {$this->limit}" : "")
		)->query();
		foreach ($rows as $row) {
			$issn = $row['issn'];
			$ppn = $row['sudocPpn'];
			try {
				$info = $sudocApi->getPpn($issn);
			} catch (\Exception $e) {
				self::csv($issn, $ppn, "ERR", $e->getMessage());
				continue;
			}

			if (count($info) === 0) {
				if ($ppn) {
					self::csv($issn, $ppn, "WARN", "Aucun PPN dans le Sudoc pour cet ISSN");
				} elseif ($this->verbose > 1) {
					self::csv($issn, $ppn, "OK", "Aucun PPN dans le Sudoc pour cet ISSN");
				}
			} elseif (count($info) === 1) {
				if (empty($ppn)) {
					$titre = Titre::model()->findBySql("SELECT * FROM Titre t JOIN Issn i ON t.id = i.titreId WHERE i.issn = ?", [$issn]);
					$intervention = $this->createIntervention($titre);
					$before = Issn::model()->findByAttributes(['issn' => $issn]);
					$after = clone ($before);
					$after->sudocPpn = $info[0]->ppn;
					$after->sudocNoHolding = (int) $info[0]->noHolding;
					$intervention->contenuJson->update($before, $after);
					if ($intervention->accept(true)) {
						self::csv($issn, $ppn, "ADD", "Le PPN {$info[0]->ppn} a été ajouté à cet ISSN (intervention {$intervention->id})");
					} else {
						self::csv($issn, $ppn, "ERR", "Erreur lors de l'ajout du PPN {$info[0]->ppn} : " . print_r($intervention->getErrors(), true));
					}
				} elseif ($info[0]->ppn != $ppn) {
					self::csv($issn, $ppn, "WARN", "Le PPN $ppn pour cet ISSN devrait être {$info[0]->ppn}");
				} elseif ($this->verbose > 1) {
					self::csv($issn, $ppn, "OK", "Le PPN de M correspond à celui du Sudoc pour cet ISSN");
				}
			} else { // count($info) > 1
				self::csv($issn, $ppn, "WARN", "Multiples PPN pour cet ISSN dans le Sudoc");
				if (!empty($ppn)) {
					$found = false;
					$sudocPpns = [];
					foreach ($info as $i) {
						if ($i->ppn === $ppn) {
							$found = true;
						}
						$sudocPpns[] = $i->ppn;
					}
					if (!$found) {
						self::csv($issn, $ppn, "WARN", "Le PPN selon M ne figure pas dans ceux de cet ISSN pour le Sudoc : " . join(", ", $sudocPpns));
					}
				}
			}
		}
	}

	/**
	 * Updates records in Issn with data from the SUDOC webservice.
	 *
	 * See Mantis #2003.
	 *
	 * @param bool $all (facultatif) Par défaut, s'applique à tout, sinon se limite aux enregistrements suspects.
	 */
	public function actionImport($all = true)
	{
		self::$csvColumns = 6;
		self::$csvSeparator = "\t";
		self::csv("Erreur", "issn", "ppn", "Titre.id", "titre", "Modifications");

		$api = new Api();
		$cmd = Yii::app()->db->createCommand(
			"SELECT i.*, t.titre, t.revueId FROM Issn i JOIN Titre t ON i.titreId = t.id"
			. " WHERE i.sudocPpn IS NOT NULL AND i.sudocPpn <> ''"
			. ($all ? "" : " AND (i.worldcatOcn IS NULL OR i.sudocNoHolding = 1 OR i.support = 'inconnu' OR i.dateDebut = '' OR i.dateFin = '')")
			. ($this->limit ? " LIMIT {$this->limit}" : '')
		)->setFetchMode(PDO::FETCH_ASSOC);
		foreach ($cmd->query() as $row) {
			try {
				$notice = $api->getNotice($row['sudocPpn']);
			} catch (\Throwable $e) {
				self::csv("ERR", $row['issn'], $row['sudocPpn'], $row['titreId'], $row['titre'], $e->getMessage());
				continue;
			}
			if ($this->verbose > 1) {
				fprintf(STDERR, "SUDOC for titreId %d: %s\n", $row['titreId'], json_encode($notice, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
			}
			if ($notice) {
				$this->applyPpnInfo($row, $notice);
			} elseif ($row['issn']) {
				$notice = new Notice();
				$notice->issn = $row['issn'];
				$notice->ppn = $row['sudocPpn'];
				$notice->sudocNoHolding = true;
				$this->applyPpnInfo($row, $notice);
			} else {
				self::csv("ERR", '', $row['sudocPpn'], $row['titreId'], $row['titre'], "notice non-trouvée");
			}
		}
	}

	public function actionImportFile($file)
	{
		$sudocImport = new \models\sudoc\Import();
		$sudocImport->simulation = (bool) $this->simulation;
		$sudocImport->verbosity = $this->verbose;
		$newfile = preg_replace('/\.xlsx/i', '_imported.xlsx', $file);
		$sudocImport->file($file, $newfile);
	}

	/**
	 * List ISSNs that have no PPN, according to the Sudoc ISSN2PPN web-service.
	 */
	public function actionIssnsMissingInSudoc()
	{
		self::$csvColumns = 4;
		self::$csvSeparator = ";";
		self::csv("ISSN", "dans le SUDOC ?", "PPN", "Sudoc (no)holding");
		$api = new Api();
		$issns = Yii::app()->db->createCommand("SELECT issn FROM Issn WHERE issn IS NOT NULL AND sudocPpn IS NULL")->queryColumn();
		foreach ($issns as $issn) {
			$sudocPpns = $api->getPpn($issn);
			if ($sudocPpns) {
				$ppn = join(
					",",
					array_map(function ($x) {
						return $x->ppn;
					}, $sudocPpns)
				);
				$info = join(
					",",
					array_map(function ($x) {
						return $x->noHolding ? "noHolding" : "";
					}, $sudocPpns)
				);
				self::csv($issn, 1, $ppn, $info);
			} else {
				self::csv($issn, 0, "", "");
			}
		}
	}

	/**
	 * List of ISSNs in M that have a PPN different from what the Sudoc PPN answer
	 */
	public function actionIssnsWithWrongPpn()
	{
		self::$csvColumns = 4;
		self::$csvSeparator = ";";
		self::csv("ISSN", "ppnM correct", "PPN de M", "ppnSudoc (web-service)");
		$api = new Api();
		$rows = Yii::app()->db
			->createCommand(
				"SELECT DISTINCT i.issn, i.sudocPpn AS sudoc"
				. " FROM Titre t"
				. " JOIN Issn i ON t.id = i.titreId AND i.support = 'electronique'"
				. " WHERE i.issn IS NOT NULL AND i.sudocPpn IS NOT NULL"
				. " ORDER BY t.id"
			)
			->queryAll();
		foreach ($rows as $row) {
			$ppns = [];
			$issn = $row['issn'];
			$sudocPpns = $api->getPpn($issn);
			foreach ($sudocPpns as $sudocPpn) {
				$ppns[] = $sudocPpn->ppn;
			}
			$status = (in_array($row['sudoc'], $ppns) ? "OK" : "ERR?");
			if ($status !== 'OK' || $this->verbose > 1) {
				self::csv($issn, $status, $row['sudoc'], join(' ', $ppns));
			}
			sleep(rand(1, 3));
		}
	}

	/**
	 * Display a Sudoc notice for each ISSN|PPN passed as argument.
	 *
	 * @param array $args list of ISSN or PPN
	 */
	public function actionQuery($args = [])
	{
		$api = new Api();
		foreach ($args as $identifier) {
			if (strpos($identifier, '-') !== false) {
				$notice = $api->getNoticeByIssn($identifier);
			} else {
				$notice = $api->getNotice($identifier);
			}
			echo "## $identifier\n", json_encode($notice, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE), "\n";
		}
	}

	/**
	 * @param array $row Mirabel data
	 * @param Notice $notice SUDOC data
	 */
	protected function applyPpnInfo(array $row, Notice $notice)
	{
		$titre = new Titre();
		$titre->setAttributes(
			[
				'id' => (int) $row['titreId'],
				'titre' => $row['titre'],
				'revueId' => (int) $row['revueId'],
			],
			false
		);

		$conflicts = [];
		$fields = [
			'bnfArk' => 'bnfArk',
			'pays' => 'pays',
			'sudocNoHolding' => 'sudocNoHolding',
			'support' => 'support',
			'worldcat' => 'worldcatOcn',
		];

		// normalize dates
		if ($notice->dateDebut) {
			if (preg_match('/^\s*\d\d[\dX][\dX]$/', $notice->dateDebut)) {
				if ($row['dateDebut'] && $notice->dateDebut !== $row['dateDebut']) {
					$conflicts[] = "[dateDebut : SUDOC={$notice->dateDebut} M={$row['dateDebut']}]";
				} else {
					$fields['dateDebut'] = 'dateDebut';
				}
			} else {
				self::csv("WARNING", $row['issn'], $row['sudocPpn'], $titre->id, $titre->titre, sprintf("Date de début '%s' ignorée : format incorrect", $notice->dateDebut));
				$notice->dateDebut = '';
			}
		}
		if ($notice->dateFin) {
			if (preg_match('/^\s*\d\d[\dX][\dX]$/', $notice->dateFin)) {
				if ($notice->dateFin && $row['dateFin'] && $notice->dateFin !== $row['dateFin']) {
					$conflicts[] = "[dateFin : SUDOC={$notice->dateFin} M={$row['dateFin']}]";
				} else {
					$fields['dateFin'] = 'dateFin';
				}
			} else {
				self::csv("WARNING", $row['issn'], $row['sudocPpn'], $titre->id, $titre->titre, sprintf("Date de fin '%s' ignorée : format incorrect", $notice->dateFin));
				$notice->dateFin = '';
			}
		}

		if ($conflicts) {
			self::csv("WARNING", $row['issn'], $row['sudocPpn'], $titre->id, $titre->titre, "Champs ignorés car conflits : " . join(" ", $conflicts));
		}

		// detect changes
		// sudoc\Notice => Issn
		$oldRow = $row;
		$changedAttributes = [];
		$details = [];
		foreach ($fields as $k => $v) {
			if ($k === 'support' && $notice->{$k} === Issn::SUPPORT_INCONNU) {
				continue;
			}
			if ($notice->{$k} !== null && $notice->{$k} !== "" && $row[$v] != $notice->{$k}) {
				$changedAttributes[] = $v;
				$row[$v] = is_bool($notice->{$k}) ? (int) $notice->{$k} : $notice->{$k};
				$details[] = "[$v={$row[$v]}]";
			}
		}

		// update record
		if ($changedAttributes) {
			// fix existing bad dates
			if ($oldRow['dateDebut'] && !ctype_digit($oldRow['dateDebut'][0]) && !$row['dateDebut']) {
				$oldRow['dateDebut'] = '';
				$row['dateDebut'] = '';
			}
			if ($oldRow['dateFin'] && !ctype_digit($oldRow['dateFin'][0]) && !$row['dateFin']) {
				$oldRow['dateFin'] = '';
				$row['dateFin'] = '';
			}

			if ($this->simulation) {
				self::csv("OK", $row['issn'], $row['sudocPpn'], $titre->id, $titre->titre, join(", ", $details));
			} else {
				$success = $this->updateIssn($oldRow, $row, array_values($fields), $titre);
				self::csv(($success ? "OK" : "ERR"), $row['issn'], $row['sudocPpn'], $titre->id, $titre->titre, join(", ", $details));
			}
		} else {
			if ($this->verbose > 1) {
				self::csv("NOP", $row['issn'], $row['sudocPpn'], $titre->id, $titre->titre, "");
			}
		}
	}

	protected function updateIssn(array $oldValues, array $newValues, array $fields, Titre $titre)
	{
		$newIssn = Issn::model()->populateRecord($oldValues, false);
		/* @var $newIssn Issn */
		$newIssn->scenario = 'import';
		$oldIssn = clone $newIssn;
		$newIssn->setAttributes($newValues, false);
		if ($newIssn->validate($fields)) {
			$i = $this->createIntervention($titre);
			$i->contenuJson->update($oldIssn, $newIssn);
			if (!$i->accept(true)) {
				fprintf(
					STDERR,
					"Could not save Intervention on « %s »",
					$titre->titre,
					print_r($newValues, true),
					print_r($i->errors, true)
				);
				return false;
			}
		} else {
			fprintf(
				STDERR,
				"Les données à importer pour le titre « %s » ne sont pas valides: %s\n%s\n",
				$newIssn->titre->titre,
				print_r($newValues, true),
				print_r($newIssn->errors, true)
			);
			return false;
		}
		return true;
	}

	private function createIntervention(Titre $titre)
	{
		$i = new Intervention();
		$i->setAttributes(
			[
				'ressourceId' => null,
				'revueId' => $titre->revueId,
				'titreId' => $titre->id,
				'editeurId' => null,
				'statut' => 'attente',
				'import' => ImportType::getSourceId('SUDOC'),
				'ip' => '',
				'contenuJson' => new InterventionDetail(),
				'suivi' => null !== Suivi::isTracked($titre),
				'description' => "Modification du titre « {$titre->titre} »",
			],
			false
		);
		return $i;
	}

	private static function csv(...$data)
	{
		if (count($data) !== self::$csvColumns) {
			throw new Exception("format incompatible pour le CSV");
		}
		fputcsv(STDOUT, $data, self::$csvSeparator);
	}
}
