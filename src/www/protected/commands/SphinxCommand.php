<?php

/**
 * Tools for configuring Sphinx.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class SphinxCommand extends CConsoleCommand
{
	public $verbose = 1;

	public function init()
	{
		if (empty(Yii::app()->params->itemAt('sphinx')['host'])) {
			echo "Sphinx n'est pas configuré dans config/local.php\n.";
			return 2;
		}
		if (!Yii::app()->db->active) {
			echo "MySQL n'est pas configuré dans config/local.php\n.";
			return 3;
		}
		return parent::init();
	}

	/**
	 * Default action, called when no argument is given.
	 */
	public function actionIndex()
	{
		echo "./yii sphinx <action> [--verbose=?]\n";
		echo "\t avec <action> : conf | confLocal | confCommon\n";
	}

	/**
	 * Action "conf" that concatenates local and common.
	 */
	public function actionConf()
	{
		$this->actionConfLocal();
		$this->actionConfCommon();
	}

	/**
	 * Action "confLocal" that configures the sources and indexes.
	 */
	public function actionConfLocal()
	{
		$db = [
			'host' => 'localhost',
		];
		foreach (explode(';', Yii::app()->db->connectionString) as $term) {
			if (strpos($term, '=') !== false) {
				list($k, $v) = explode('=', $term);
				$k = preg_replace('/^mysql:/', '', $k);
				$db[$k] = $v;
			}
		}
		$db['user'] = Yii::app()->db->username;
		$db['pass'] = Yii::app()->db->password;
		unset($term, $k, $v);

		$sphinx = Yii::app()->params->itemAt('sphinx');
		if (empty($sphinx)) {
			echo "Sphinx n'est pas configuré. Complétez config/local.php.";
			return 1;
		}
		$prefix = empty($sphinx['prefix']) ? '' : $sphinx['prefix'];

		require __DIR__ . '/views/sphinx/conf.php';
	}

	/**
	 * Action "confCommon" that configures the searchd server.
	 */
	public function actionConfCommon(): int
	{
		$sphinx = Yii::app()->params->itemAt('sphinx');
		if (empty($sphinx)) {
			echo "Sphinx n'est pas configuré. Complétez config/local.php.";
			return 1;
		}
		// $sphinx will be used by this include
		require __DIR__ . '/views/sphinx/common.php';
		fprintf(
			STDERR,
			"Attention, le socket est déclaré dans /tmp\n"
			. "Pour utiliser un autre emplacement, modifier la ligne 'listen=...:mysql41' avec un autre *chemin absolu*.\n"
			. "\nTester le serveur searchd de Sphinx avec :\n\tmysql --protocol=SOCKET --socket=/tmp/sphinx-mysql.socket\n"
		);
		return 0;
	}
}
