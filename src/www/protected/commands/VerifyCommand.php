<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class VerifyCommand extends CConsoleCommand
{
	use \commands\models\MaintenanceModeTrait;

	/**
	 * @var int
	 */
	public $verbose = 0;

	/**
	 * @var int If set to a positive value, will stop after this many iterations.
	 */
	public $limit;

	/**
	 * Search for bad URLs, and optionaly create a CSV file .
	 */
	public function actionLinksRevues($partiel = false, $csv = ""): int
	{
		if ($csv) {
			$handle = fopen($csv, "w");
			fputcsv($handle, ["revueId", "URL", "Erreur"], "\t");
		} else {
			$handle = null;
		}

		$linkChecker = $this->initLinkChecker((bool) $partiel);

		if ($partiel) {
			$this->checkBrokenLinks("Revue", $linkChecker, $handle);
		} else {
			$criteria = $this->limit > 0 ? ['limit' => (int) $this->limit] : [];
			$this->checkAllLinks(Revue::model()->findAll($criteria), $linkChecker, $handle);
		}

		$this->printLinkCheckerStats("Revue");
		return 0;
	}

	/**
	 * Search for bad URLs.
	 */
	public function actionLinksEditeurs($partiel = false): int
	{
		$linkChecker = $this->initLinkChecker((bool) $partiel);

		if ($partiel) {
			$this->checkBrokenLinks("Editeur", $linkChecker);
		} else {
			$criteria = $this->limit > 0 ? ['limit' => (int) $this->limit] : [];
			$this->checkAllLinks(Editeur::model()->findAll($criteria), $linkChecker);
		}

		$this->printLinkCheckerStats("Editeur");
		return 0;
	}

	/**
	 * Search for bad URLs.
	 */
	public function actionLinksRessources($partiel = false): int
	{
		$linkChecker = $this->initLinkChecker((bool) $partiel);

		if ($partiel) {
			$this->checkBrokenLinks("Ressource", $linkChecker);
		} else {
			$criteria = $this->limit > 0 ? ['limit' => (int) $this->limit] : [];
			$this->checkAllLinks(Ressource::model()->findAll($criteria), $linkChecker);
		}

		$this->printLinkCheckerStats("Ressource");
		return 0;
	}

	/**
	 * @param WithCheckLinks[] $toCheck
	 * @param LinkChecker $linkChecker
	 * @param ressource $handle
	 * @return void
	 */
	private function checkAllLinks(array $toCheck, LinkChecker $linkChecker, $handle = null): void
	{
		if (!$toCheck) {
			return;
		}

		$tableName = $toCheck[0]->tableName();
		$fk = sprintf("%sId", strtolower($tableName));

		$db = Yii::app()->db;
		$db->createCommand("TRUNCATE VerifUrl$tableName")->execute();

		foreach ($toCheck as $source) {
			$inserts = [];
			if ($this->verbose > 1) {
				fprintf(STDERR, "%s %5d...\n", $tableName, $source->getPrimaryKey());
			}
			$results = $source->checkLinks($linkChecker);

			// analyze and save the results for this source
			foreach ($results['urls'] as $url => $result) {
				list($status, $statusMessage) = $result;
				$success = in_array($status, [LinkChecker::RESULT_SUCCESS, LinkChecker::RESULT_IGNORED], true);
				if ($handle) {
					if ($this->verbose > 1) {
						fputcsv($handle, [$source->getPrimaryKey(), $url, ($status === LinkChecker::RESULT_SUCCESS ? 'OK' : $statusMessage)], "\t");
					} elseif (!$success) {
						fputcsv($handle, [$source->getPrimaryKey(), $url, $statusMessage], "\t");
					}
				}
				// hand-built SQL for performance (bulk insert)
				$inserts[] = "("
					. join(
						", ",
						[
							(int) $source->getPrimaryKey(),
							$db->pdoInstance->quote($url),
							(int) $success,
							($success ? "''" : $db->pdoInstance->quote($statusMessage)),
						]
					) . ")";
			}
			if ($inserts) {
				$db->createCommand(
					"INSERT INTO VerifUrl$tableName ($fk, url, success, msg) VALUES "
					. join(', ', $inserts)
				)->execute();
			}
		}
	}

	private function checkBrokenLinks(string $source, LinkChecker $linkChecker, $handle = null): void
	{
		$tableName = "VerifUrl{$source}";
		$fk = sprintf("%sId", strtolower($source));
		$db = Yii::app()->db;

		$toCheckCmd = $db
			->createCommand(
				"SELECT id, $fk AS sourceId, url FROM $tableName WHERE success = 0"
				. ($this->limit ? " LIMIT {$this->limit}" : "")
			);
		$toCheck = [];
		foreach ($toCheckCmd->query() as $row) {
			if (strncmp($row['url'], 'http', 4) === 0) {
				$toCheck[$row['url']] = ['id' => (int) $row['id'], 'sourceId' => (int) $row['sourceId']];
			}
		}
		unset($toCheckCmd);
		if (!$toCheck) {
			return;
		}

		$results = $linkChecker->checkLinks(array_keys($toCheck));

		$transaction = $db->beginTransaction();
		$update = $db->createCommand("UPDATE $tableName SET success = :success, msg = :msg, hdate = NOW() WHERE id = :id AND $fk = :sourceId");
		$batch = 0;
		foreach ($results as $url => $result) {
			$id = $toCheck[$url]['id'];
			$sourceId = $toCheck[$url]['sourceId'];

			list($status, $statusMessage) = $result;
			$success = in_array($status, [LinkChecker::RESULT_SUCCESS, LinkChecker::RESULT_IGNORED], true);
			if ($handle) {
				if ($this->verbose > 1) {
					fputcsv($handle, [$sourceId, $url, ($status === LinkChecker::RESULT_SUCCESS ? 'OK' : $statusMessage)], "\t");
				} elseif (!$success) {
					fputcsv($handle, [$sourceId, $url, $statusMessage], "\t");
				}
			}
			$update->execute([
				':success' => (int) $success,
				':msg' => $success ? '' : $statusMessage,
				':id' => $id,
				':sourceId' => $sourceId,
			]);
			$batch++;
			if ($batch >= 200) {
				$batch = 0;
				$transaction->commit();
				$transaction = $db->beginTransaction();
			}
		}
		$transaction->commit();
	}

	private function initLinkChecker(bool $slow = false): LinkChecker
	{
		$linkChecker = new LinkChecker();
		if ($slow) {
			$linkChecker->maxThreads = 2;
			$linkChecker->wait = 100; // ms
		}
		$linkChecker->setTimeout(5);
		return $linkChecker;
	}

	private function printLinkCheckerStats(string $source): void
	{
		$stats = LinkChecker::getStats();
		if ($this->verbose > 0) {
			fprintf(STDERR, " stats:\n%s\n", print_r($stats, true));
		}
		fprintf(STDERR, "Liens ($source) : %3d liens testés, %3d erreurs\n", $stats['urlsNum'], $stats['errorsNum']);
	}
}
