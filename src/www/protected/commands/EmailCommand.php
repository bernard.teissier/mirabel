<?php

class EmailCommand extends CConsoleCommand
{
	public function actionSend($to, $subject, $bodyFile, $attachFile = "")
	{
		if (!$bodyFile || !file_exists($bodyFile)) {
			die("Missing: --bodyFile=/path/to/XXX\n");
		}
		if (strpos($to, "@") === false) {
			if (ctype_digit($to)) {
				$user = Utilisateur::model()->findByPk($to);
			} elseif (strpos($to, "@") === false) {
				$user = Utilisateur::model()->findByAttributes(['login' => $to]);
			}
			if (!isset($user)) {
				die("Destinataire incorrect : ID | log | email\n");
			}
			assert($user instanceof Utilisateur);
			$to = $user->email;
		}
		$message = Mailer::newMail()
			->setSubject(sprintf("[%s] %s", Yii::app()->name, $subject))
			->setFrom(Config::read('email.from'))
			->setTo($to)
			->setBody(file_get_contents($bodyFile));
		if ($attachFile && file_exists($attachFile)) {
			$message->attach(Swift_Attachment::fromPath($attachFile));
		}
		$replyTo = Config::read('email.replyTo');
		if ($replyTo) {
			$message->setReplyTo($replyTo);
		}
		Mailer::sendMail($message);
	}
}
