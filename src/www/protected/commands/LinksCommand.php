<?php

use commands\models\LinksObsolescence;
use commands\models\LinkUpdater;
use commands\models\linkimport;

Yii::import('application.commands.models.*');

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class LinksCommand extends CConsoleCommand
{
	use \commands\models\MaintenanceModeTrait;

	/**
	 * @var array
	 */
	public $email = [];

	public $simulation = 0;

	/**
	 * @var int
	 * verbose = 1: CSV lists updated but unchanged links
	 */
	public $verbose = 0;

	/**
	 * @var int If set to a positive value, will stop after this many iterations.
	 */
	public $limit;

	/**
	 * @var string
	 */
	public $obsoleteSince;

	/**
	 * Create CSV files that list potential errors for DOAJ and ROAD. Not for cron.
	 */
	public function actionCheckDoajRoad(): int
	{
		foreach (['ROAD', 'DOAJ'] as $src) {
			$filename = $src . "_revues-sans-accès-libre-txtintegral.csv";
			echo $filename, "\n";
			$handle = fopen($filename, "w");
			fputcsv($handle, ["revueId"], "\t");
			$sourceId = (int) Yii::app()->db
				->createCommand("SELECT id FROM Sourcelien WHERE nom = ?")
				->queryScalar([$src]);
			$sql = "SELECT t.revueId, MAX(t.titre) AS titre"
				. " FROM Titre t"
				. "   JOIN LienTitre l ON l.titreId = t.id AND l.sourceId = $sourceId"
				. "   LEFT JOIN Service s ON s.titreId = t.id AND s.type = 'Intégral' AND s.acces = 'Libre'"
				. " GROUP BY t.revueId"
				. " HAVING count(s.id) = 0";
			$sth = Yii::app()->db->createCommand($sql)->query();
			$sth->setFetchMode(PDO::FETCH_NUM);
			foreach ($sth as $row) {
				fputcsv($handle, $row, "\t");
			}
		}
		return 0;
	}

	public function actionCairnInt(string $csv): int
	{
		if (!file_exists($csv)) {
			echo "Fichier CSV '$csv' non trouvé.\n";
			return 1;
		}
		$importer = new linkimport\CairnInt(get_object_vars($this));
		$importer->csvFile = $csv;
		$importer->import();
		return 0;
	}

	/**
	 * Import the DOAJ links by downloading a CSV file.
	 *
	 * @return int
	 */
	public function actionDoaj()
	{
		$importer = new linkimport\Doaj(get_object_vars($this));
		$importer->import();
		return 0;
	}

	/**
	 * Remove the DOAJ links using a CSV file.
	 *
	 * @param string $tsv File path
	 * @return int
	 */
	public function actionDoajRemove($tsv)
	{
		if (!file_exists($tsv)) {
			echo "Fichier TSV '$tsv' non trouvé.\n";
			return 1;
		}

		$linkUpdater = new LinkUpdater(Sourcelien::model()->findByAttributes(['nom' => 'DOAJ']));
		$linkUpdater->simulation = $this->simulation;
		$linkUpdater->verbose = $this->verbose;

		$linkUpdater->removeByIssn($tsv, 'https://doaj.org/toc/%s');
		fprintf(STDERR, $linkUpdater->getStats());
		return 0;
	}

	/**
	 * Import the ROAD links from a XML MARC21 file, downloaded if not up to date.
	 *
	 * @return int
	 */
	public function actionRoad()
	{
		$importer = new linkimport\Road(get_object_vars($this));
		$importer->import();
		return 0;
	}

	public function actionErih(): int
	{
		$importer = new linkimport\Erih(get_object_vars($this));
		$importer->import();
		return 0;
	}

	/**
	 * Import the HAL links from their REST API.
	 *
	 * @return int
	 */
	public function actionHal(): int
	{
		$importer = new linkimport\Hal(get_object_vars($this));
		$importer->import();
		return 0;
	}

	/**
	 * Lists links imported but not seen recently, for all the sources.
	 *
	 * @param string $since
	 * @return int
	 */
	public function actionRemoved($since)
	{
		if (ctype_digit($since)) {
			$timestamp = $since;
		} else {
			$timestamp = strtotime($since);
		}
		$content = (new LinksObsolescence(null))->detectObsolete($timestamp);
		if ($content) {
			printf("\n## Liens obsolètes (pas importés depuis %s)\n", date('Y-m-d', $timestamp));
			echo $content;
		}
		return 0;
	}

	/**
	 * Used exceptionally to set the import date of some links. Not for the cron nor unmodified usage.
	 *
	 * @param string $date Set the import date to this.
	 * @return int
	 */
	public function actionResetImported($date)
	{
		$timestamp = strtotime($date);
		$sourcesImported = Yii::app()->db
			->createCommand("SELECT id FROM Sourcelien WHERE nom IN ('DOAJ', 'Héloïse', 'ROAD')")
			->queryColumn();
		foreach (Titre::model()->findAll() as $titre) {
			/* @var $titre Titre */
			foreach ($titre->getLiens() as $lien) {
				/* @var $lien Lien */
				if (in_array($lien->sourceId, $sourcesImported)) {
					Yii::app()->db
						->createCommand("INSERT IGNORE INTO ImportedLink VALUES (UNHEX(:hash), :sid, :tid, :url, :ts)")
						->execute([
							':hash' => sha1($lien->url),
							':sid' => $lien->sourceId,
							':tid' => $titre->id,
							':url' => $lien->url,
							':ts' => $timestamp,
						]);
				}
			}
		}
		return 0;
	}
}
