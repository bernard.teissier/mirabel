<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class PartenaireCommand extends CConsoleCommand
{
	public $verbose = 1;

	/**
	 * Check (and resize) one or each logo.
	 */
	public function actionCouvertures($partenaireId)
	{
		$partenaire = Partenaire::model()->findByPk($partenaireId);
		if (!$partenaire) {
			echo "Partenaire non trouvé.\n";
			return 1;
		}
		if (!is_dir("vignettes_partenaire-" . $partenaire->id)) {
			mkdir("vignettes_partenaire-" . $partenaire->id);
		}
		fputcsv(STDOUT, ["Identifiant local", "message", "ID de titre Mir@bel"], ';');
		$titres = Yii::app()->db
			->createCommand("SELECT titreId, identifiantLocal FROM Partenaire_Titre WHERE partenaireId = :pid AND identifiantLocal != ''")
			->query([':pid' => $partenaire->id]);
		foreach ($titres as $row) {
			$imgFile = sprintf(dirname(Yii::app()->basePath) . '/images/titres-couvertures/%09d.png', $row['titreId']);
			if (file_exists($imgFile)) {
				copy($imgFile, sprintf("vignettes_partenaire-%d/%s.png", $partenaire->id, $row['identifiantLocal']));
			} else {
				fputcsv(STDOUT, [$row['identifiantLocal'], "sans vignette", $row['titreId']], ';');
			}
		}
	}

	/**
	 * Check (and resize) one or each logo.
	 */
	public function actionLogo($partenaireId = 0, $resize = false)
	{
		if ($partenaireId) {
			$partenaires = Partenaire::model()->findAllByPk($partenaireId);
		} else {
			$partenaires = Partenaire::model()->findAll("statut != 'inactif'");
		}
		$upload = new Upload('');
		foreach ($partenaires as $p) {
			/* @var $p Partenaire */
			echo $p->id, "\t", $p->nom, "\t";
			$logoRaw = glob(sprintf(dirname(Yii::app()->basePath) . '/images/logos/%03d.*', $p->id));
			if ($logoRaw && file_exists($logoRaw[0])) {
				echo $logoRaw[0] . "\t";
				if ($resize) {
					$upload->resizeLogo($logoRaw[0]);
					if ($upload->hasErrors()) {
						echo join(' ## ', $upload->getErrors());
					} else {
						echo "resized";
					}
				}
			} else {
				echo "NO LOGO\t";
			}
			echo "\n";
		}
	}

	/**
	 * Give a partenaire a random hash.
	 *
	 * @param int $partenaireId
	 * @return int
	 */
	public function actionRandomHash($partenaireId = 0)
	{
		$partenaire = Partenaire::model()->findByPk($partenaireId);
		if (!$partenaire) {
			echo "Partenaire non trouvé.\n";
			return 1;
		}
		if ($partenaire->hash) {
			if (!$this->confirm("Si vous modifiez le hash, il faudra changer la configuration des scripts qui l'utilisent. Continuer ?")) {
				return 2;
			}
		}
		$partenaire->hash = Partenaire::getRandomHash();
		if ($partenaire->save()) {
			echo "Nouveau hash : {$partenaire->hash}\n";
		} else {
			echo "Erreur en enregistrant le nouveau hash.\n";
		}
		return 0;
	}

	public function actionMetalogo()
	{
		$criteria = [
			'partenaires' => "statut = 'actif'",
			'partenaires-veilleurs' => "statut = 'actif' AND type = 'normal'",
			'partenaires-editeurs' => "statut = 'actif' AND type = 'editeur'",
			'partenaires-import' => "statut = 'actif' AND type = 'import'",
		];
		foreach ($criteria as $name => $condition) {
			$this->buildMetalogo($name, $condition);
		}
	}

	private function buildMetalogo($name, $condition)
	{
		$ids = Yii::app()->db
			->createCommand("SELECT id FROM Partenaire WHERE $condition ORDER BY id")
			->queryColumn();
		$filePattern = dirname(Yii::app()->basePath) . '/images/logos/h55/%03d.png';
		$images = [];
		foreach ($ids as $id) {
			$file = sprintf($filePattern, $id);
			if (file_exists($file)) {
				if (is_readable($file)) {
					$images[] = $file;
				} else {
					printf("	\tPartenaire ID=%d erreur de lecture du logo (permission ?)\n", $id);
				}
			} else {
				printf("	\tPartenaire ID=%d sans logo\n", $id);
			}
		}
		if ($images) {
			$this->mixImages($images, $name);
			shuffle($images);
			$this->mixImages($images, $name . '_random');
		}
	}

	private function mixImages($images, $finalName)
	{
		$numVert = ceil(sqrt(count($images)));
		$command = "/usr/bin/montage "
			. join(" ", array_map("escapeshellarg", $images))
			. " -tile x$numVert -geometry 100x55+2+2 "
			. escapeshellarg(dirname(Yii::app()->basePath) . '/images/logos/' . $finalName . '.png');
		exec($command);
	}
}
