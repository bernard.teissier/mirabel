<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class MatomoCommand extends CConsoleCommand
{
	public function actionTest()
	{
		$matomoConfig = Yii::app()->params->itemAt('matomo');
		if ($matomoConfig && !empty($matomoConfig['tokenAuth'])) {
			echo "Response: " . self::recordInMatomo($matomoConfig) . "\n";
		} else {
			echo "Missing config matomo.tokenAuth\n";
		}
	}

	private static function recordInMatomo($config)
	{
		MatomoTracker::$URL = $config['rootUrl'];
		$matomoTracker = new MatomoTracker($config['siteId']);
		$matomoTracker->setTokenAuth($config['tokenAuth']);
		return $matomoTracker->doTrackEvent("CLI", 'test');
	}
}
