<?php

use commands\models\linkimport;
use models\wikidata\Cache;
use models\wikidata\Compare;
use models\wikidata\Export;
use models\wikidata\Import;
use models\wikidata\Query;

/**
 * Wikidata cache, import and export
 *
 * @author Guillaume Allègre <guillaume.allegre@silecs.info>
 */
class WikidataCommand extends CConsoleCommand
{
	use \commands\models\MaintenanceModeTrait;
	use \models\traits\VerbosityEcho;

	public $verbose = 0;

	public $simulation = 0;

	public $limit = 0;

	private $startTime = 0;

	public function getHelp()
	{
		return <<<EOL
			Ce script alimente le cache Wikidata et calcule les différences avec les données Mir@bel.
			Voir la documentation technique à l'adresse web  .../doc/wikidata.md

			== CRON ==
			yii wikidata cronFetchCompare   enchaine tous les fetch et toutes les comparaisons automatisables
			yii wikidata cronImportWplinks  importe les liens Wikipédia (provenant de Wikidata) avec détection des liens obsolètes

			== Exports ==
			yii wikidata exportQuickstatements          crée le fichier exportable à saisir dans QS

			== Imports ==
			yii wikidata updateWplinksName (ONE-SHOT)   met à jour les liens "Wikipédia" manuels en "Wikipédia (langue)"
			yii wikidata wplinksStats                   affiche le nombre de liens Wikipédia par langue dans LienTitre

			== Diagnostic / Statistiques ==
			yii wikidata propStats          affiche le nombre de liens externes par propriété stockés dans le cache Wikidata
			yii wikidata showProperties     affiche toutes les propriétés disponibles
			yii wikidata showProperty       affiche les détails d'une propriété (--prop=)
			yii wikidata testNetwork        teste les urls de Query avec des requêtes minimales
			yii wikidata wpStats            affiche le nombre de liens Wikipédia par langue stockés dans le cache Wikidata

			Paramètres communs :
				--verbose=1     : 0-concis 1-normal 2-debug
				--simulation=0  : 0-save 1-validate 2-nothing  (pour la mise en cache seulement)
				--limit=0       : limite optionnelle, sparql ou sql selon le contexte
				--obsoleteSince=... pour les liens Wikipedia

			EOL;
	}

	public function init()
	{
		$this->startTime = time();
	}

	/**
	 * Default action, called when no argument is given.
	 */
	public function actionIndex()
	{
		echo $this->getHelp();
	}

	public function actionTestNetwork()
	{
		$WQ = new Query(2, 'en', 'application/sparql-results+json');
		print_r($WQ->testNetwork());
	}

	public function actionCronFetchCompare()
	{
		$WCache = new Cache($this->verbose, $this->simulation, $this->limit);

		$this->vecho(1, "\nRécupération des liens Wikipédia...\n");
		try {
			$WCache->setMirabelLanguages();
		} catch(\Exception $e) {
			echo "Une erreur est survenue dans la résolution des langues : " . $e->getMessage();
			return 1;
		}

		$transaction1 = Yii::app()->db->beginTransaction();
		try {
			$WCache->fetchWikipediaLinks();
			$this->rollbackIfIncomplete('Wikidata', 'Fetch Wikipedia Links', "WHERE property LIKE 'WP:%'", $transaction1);
		} catch(\Exception $e) {
			$transaction1->rollback();
			echo "Une erreur est survenue : " . $e->getMessage();
			Yii::log($e->getMessage() . "\n" . $e->getTraceAsString(), CLogger::LEVEL_ERROR, 'wikidata');
			return 1;
		}

		$this->vecho(1, "\nRécupération des propriétés Wikidata...\n");
		$transaction2 = Yii::app()->db->beginTransaction();
		try {
			$WCache->fetchPropertyValues();
			$this->rollbackIfIncomplete('Wikidata', 'Fetch Wikidata Properties', "WHERE property LIKE 'P%'", $transaction2);
		} catch(\Exception $e) {
			$transaction2->rollback();
			echo "Une erreur est survenue : " . $e->getMessage();
			Yii::log($e->getMessage() . "\n" . $e->getTraceAsString(), CLogger::LEVEL_ERROR, 'wikidata');
			return 1;
		}

		$WComp = new Compare($this->verbose, $this->simulation, null);
		$this->vecho(1, "\nDétermination des identifiants de titres à partir des ISSN...\n");
		$WComp->updateTitres();
		$this->vecho(1, "\nDétection des incohérences globales Wikidata / Mir@bel...\n");
		$WComp->compareInexistent();
		$WComp->compareComplement();
		$WComp->cleanupProperties();
		$this->vecho(1, "\nComparaisons Wikidata / Mir@bel...\n");
		$WComp->compareProperties(false);

		$WCache->fillWikidataIssn();

		return 0;
	}

	public function actionCronImportWplinks(array $email = [], string $obsoleteSince = "")
	{
		$params = array_merge(
			get_object_vars($this),
			[
				'email' => $email,
				'obsoleteSince' => $obsoleteSince,
			]
		);
		$importer = new linkimport\Wikipedia($params);
		$importer->import();
		return 0;
	}

	public function actionWpStats()
	{
		$WCache = new Cache($this->verbose, $this->simulation);
		print_r($WCache->getWikipediaLanguageStats());
	}

	public function actionShowProperties()
	{
		print_r(Compare::PROPERTIES_TREE);
	}

	public function actionShowProperty($prop)
	{
		printf("%s  (%s) :\n", $prop, Compare::getWdProperties()[$prop]);
		$WQuery = new Query($this->verbose, 'fr', '');
		print_r($WQuery->getPropertyDetails($prop));
	}

	public function actionPropStats()
	{
		$WCache = new Cache($this->verbose, $this->simulation);
		$prev = 0;
		foreach ($WCache->getPropertyStats() as $row) {
			if ($row['propertyType'] != $prev) {
				echo "\n";
				$prev = $row['propertyType'];
			}
			printf(
				"%2d  %6s %-12s  %5d  %20s \n",
				$row['propertyType'],
				$row['property'],
				Compare::getWdProperties()[$row['property']],
				$row['Nb'],
				$row['Date'],
			);
		}
		echo "\n";
	}

	public function actionUpdateWplinksName()
	{
		$this->fixLinkSourcesForEditors();

		$WImport = new Import($this->verbose, $this->simulation, $this->limit);
		$WImport->updateWplinksChangeName();
		return 0;
	}

	public function actionWplinksStats()
	{
		$WImport = new Import($this->verbose, $this->simulation, $this->limit);
		print_r($WImport->getWikipediaLinksStats());
	}

	public function actionExportQuickstatements()
	{
		$WExport = new Export($this->verbose);

		$fhandler = fopen('quickstatements.csv', 'w');
		$rows = $WExport->genQuickstatements($fhandler);
		echo "quickstatements.csv : $rows lignes.\n";
		fclose($fhandler);

		$this->notifyCsvFile('log-multiple-revues.csv', 'logWarning', 'nbrevues');
		$this->notifyCsvFile('log-multiple-titres.csv', 'logWarning', 'nbtitres');
		$this->notifyCsvFile('log-multiple-elements.csv', 'logMultielements', 'nbtitres');
	}

	private function rollbackIfIncomplete(string $table, string $action, string $where, CDbTransaction $transaction): void
	{
		$seuil = 90; // si le nouveau cache a moins de seuil % d'entrées par rapport au cache Backup on annule la mise à jour.

		$newrecords = Yii::app()->db
			->createCommand("SELECT COUNT(*) FROM $table $where AND hdate >= {$this->startTime}")
			->queryScalar();
		$oldrecords = Yii::app()->db
			->createCommand("SELECT COUNT(*) FROM $table $where AND hdate < {$this->startTime}")
			->queryScalar();
		if ($newrecords < $seuil / 100 * $oldrecords) {
			$this->vecho(0, sprintf(
				"ERREUR Cache [%s]. %d enregistrements vs %d anciens.\n",
				$action,
				$newrecords,
				$oldrecords
			));
			$this->vecho(0, "Annulation de la synchro de $table.\n");
			$transaction->rollback();
			// Just in case the transaction is not enough
			Yii::app()->db
				->createCommand("DELETE FROM $table $where AND hdate >= {$this->startTime}")
				->execute();
			exit(1);
		}
		Yii::app()->db
			->createCommand("DELETE FROM $table $where AND hdate < {$this->startTime}")
			->execute();
		$transaction->commit();
	}

	private function notifyCsvFile(string $filename, string $method, string $param): void
	{
		$WExport = new Export($this->verbose);
		$fhandler = fopen($filename, 'w');
		$WExport->logHeaders($fhandler);
		$rows = $WExport->{$method}($fhandler, $param);
		echo "$filename : $rows lignes.\n";
		fclose($fhandler);
	}

	private function fixLinkSourcesForEditors(): void
	{
		$sources = Tools::sqlToPairsObject("SELECT * FROM Sourcelien WHERE nomcourt LIKE 'wikipedia-__'", Sourcelien::class, 'url');

		$rows = Yii::app()->db
			->createCommand(
				<<<EOSQL
				SELECT e.id, e.liensJson, l.domain, l.url
				FROM LienEditeur l
				  JOIN Editeur e ON l.editeurId = e.id
				WHERE l.name = 'Wikipedia' AND l.domain LIKE '%.wikipedia.org'
				EOSQL
			)->queryAll();
		$updateEditeur = Yii::app()->db->createCommand("UPDATE Editeur SET liensJson = ? WHERE id = ?");
		foreach ($rows as $row) {
			$newSource = $sources[$row['domain']];
			$decoded = json_decode($row['liensJson']);
			foreach ($decoded as $i => $link) {
				if ($link->src === 'Wikipedia' && $link->url === $row['url']) {
					$decoded[$i] = [
						'sourceId' => (int) $newSource->id,
						'src' => $newSource->nom,
						'url' => $link->url,
					];
				}
			}
			$updateEditeur->execute([json_encode($decoded), $row['id']]);
		}

		$update = Yii::app()->db->createCommand("UPDATE LienEditeur SET sourceId = ?, name = ? WHERE domain = ?");
		foreach ($sources as $source) {
			$update->execute([$source->id, $source->nom, $source->url]);
		}
	}
}
