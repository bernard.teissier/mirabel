<?php

/**
 * Remove old logs
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class LogsCommand extends CConsoleCommand
{
	public $verbose = 0;

	public $all = '1 year ago';

	public $minor = '1 month ago';

	/**
	 * Default action, called when no argument is given.
	 */
	public function actionIndex()
	{
		echo $this->getHelp();
	}

	public function getHelp()
	{
		return <<<EOL
			./yii logs clear [--all=1y] [--minor=1m]

			Options :
			    --all=<date  > : supprime les logs antérieurs à <date> (1 year ago)
			    --minor=<date> : supprime les logs mineurs (debug, etc) antérieurs à <date> (1 month ago)
			    --verbose=<?>  : fixe la verbosité des messages dans la console (par défaut, 0).

			EOL;
	}

	/**
	 * Action "clear".
	 */
	public function actionClear()
	{
		$this->all = $this->toTimestamp($this->all);
		$this->minor = $this->toTimestamp($this->minor);

		$all = Yii::app()->db->createCommand(
			"DELETE FROM LogExt WHERE logtime < :all"
		)->execute([':all' => $this->all]);
		if ($this->verbose) {
			echo $all . " old logs removed since " . date('Y-m-d', $this->all) . "\n";
		}

		$minor = Yii::app()->db->createCommand(
			"DELETE FROM LogExt WHERE level NOT IN ('warning', 'error') AND logtime < :minor"
		)->execute([':minor' => $this->minor]);
		if ($this->verbose) {
			echo $minor . " old minor logs removed since " . date('Y-m-d', $this->minor) . "\n";
		}
	}

	protected function toTimestamp($since)
	{
		if (!$since) {
			throw new Exception("Date missing.");
		}
		if (!is_integer($since) && !ctype_digit($since)) {
			$since = strtotime($since);
			if ($since === false) {
				throw new Exception("Wrong date or timestamp.");
			}
		}
		return $since;
	}
}
