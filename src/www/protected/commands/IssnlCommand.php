<?php

Yii::import("application.commands.models.*");

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class IssnlCommand extends CConsoleCommand
{
	use \commands\models\MaintenanceModeTrait;

	public $verbose = 0;

	public $simulation = false;

	public $limit;

	private $lastError = '';

	/**
	 * Recherche les titres de Mirabel dans la base ISSN en utilisant leur ISSN-L.
	 * Liste les données manquant dans Mirabel.
	 */
	public function actionCheck()
	{
		$issnlImport = new IssnlImport();
		echo "titreId\tmessage\tissnl\tissn\n";
		$cmd = Yii::app()->db->createCommand(
			"SELECT titreId, issn, issnl FROM Issn"
			. " WHERE issnl IS NOT NULL AND issnl != ''"
			. ($this->limit ? " LIMIT {$this->limit}" : '')
		)->setFetchMode(PDO::FETCH_ASSOC);
		foreach ($cmd->query() as $row) {
			$matchingIssnList = $issnlImport->searchByIssnl($row['issnl']);
			if ($matchingIssnList) {
				if (empty($row['issn'])) {
					$row['issn'] = join(', ', $matchingIssnList);
					self::printCsvLine($row, 'ISSN-L sans ISSN dans Mirabel');
				} elseif (in_array($row['issn'], $matchingIssnList)) {
					if ($this->verbose) {
						self::printCsvLine($row, 'OK');
					}
				} else {
					self::printCsvLine($row, "L'ISSN dans Mirabel n'est pas dans les données ISSN");
				}
			} else {
				self::printCsvLine($row, 'ISSNL absent de la base ISSN');
			}
		}
	}

	public function actionCron($download = false)
	{
		if ($download) {
			$downloaded = $this->downloadIssnorgZip(true);
			$tries = 1;
			while (!$downloaded && $tries < 3) {
				// wait 1 minute and download again
				sleep(60);
				$downloaded = $this->downloadIssnorgZip(false);
				$tries++;
			}
			if ($downloaded) {
				$this->actionPrepare();
			} else {
				echo "Le téléchargement du zip d'issn.org a échoué ($tries tentatives) :\n{$this->lastError}\n";
				if (!$this->restoreZip()) {
					die("Arrêt faute de zip restant d'un précédent téléchargement.");
				}
			}
		}
		echo "## Incohérences entre M et issn.org\n";
		$this->actionCheck();
		echo "\n\n## Ajout de données issn.org dans M\n";
		$this->actionImport();
	}

	public function actionImport()
	{
		echo "titreId;message;issnl;issn\n";
		$this->fillIssnl();
		$this->fillIssn();
	}

	public function actionPrepare()
	{
		$i = new IssnlImport();
		$i->verbose = (int) $this->verbose;
		$i->import();
	}

	private function restoreZip(): bool
	{
		$path = __DIR__ . '/data/';
		if (file_exists("$path/issntables.OLD.zip")) {
			return rename("$path/issntables.OLD.zip", "$path/issntables.zip");
		}
		return false;
	}

	private function downloadIssnorgZip($backup): bool
	{
		$path = __DIR__ . '/data/';

		$targetFileName = "$path/issntables.zip";
		if ($backup && file_exists($targetFileName)) {
			if (file_exists("$path/issntables.OLD.zip")) {
				unlink("$path/issntables.OLD.zip");
			}
			rename($targetFileName, "$path/issntables.OLD.zip");
		}
		$fh = @fopen($targetFileName, "w");
		if (!$fh) {
			$this->lastError = "Impossible de créer ou modifier le fichier en écriture !";
			return false;
		}

		// download
		$curl = curl_init();
		curl_setopt_array(
			$curl,
			[
				\CURLOPT_FILE => $fh,
				\CURLOPT_FOLLOWLOCATION => true,
				\CURLOPT_MAXREDIRS => 3,
				\CURLOPT_SSL_VERIFYHOST => false,
				\CURLOPT_SSL_VERIFYPEER => false,
				\CURLOPT_TIMEOUT => 120, // 2 minutes max to download the file
				\CURLOPT_URL => 'https://www.issn.org/wp-content/uploads/2014/03/issnltables.zip',
			]
		);
		if (curl_exec($curl) === false) {
			$this->lastError = curl_error($curl);
			return false;
		}
		$code = curl_getinfo($curl, \CURLINFO_HTTP_CODE);
		if ($code < 200 || $code >= 300) {
			$this->lastError = "Permission refusée ? Code HTTP $code";
			return false;
		}
		$size = filesize($targetFileName);
		if ($size < 1000000) {
			$this->lastError = "Le zip pèse moins de 1Mo.";
			return false;
		}

		// extract
		if (extension_loaded('zip')) {
			$zip = new \ZipArchive;
			if ($zip->open($targetFileName) !== true) {
				$this->lastError = "L'ouverture du zip a échoué.";
				return false;
			}
			chdir($path);
			if ($zip->extractTo(".") === false) {
				$this->lastError = "Zip error: " . $zip->getStatusString();
				return false;
			}
			$zip->close();
			unset($zip);
		} else {
			chdir($path);
			system("unzip issntables.zip");
		}

		// rename files
		$issnFile = glob("$path/*ISSN-to-ISSN-L.txt");
		$issnlFile = glob("$path/*ISSN-L-to-ISSN.txt");
		if (count($issnFile) !== 1 || count($issnlFile) !== 1) {
			$this->lastError = "Impossible de trouver les fichiers txt des ISSN.";
			return false;
		}
		rename($issnFile[0], IssnlImport::FILE_ISSN_ISSNL);
		rename($issnlFile[0], IssnlImport::FILE_ISSNL_ISSN);
		return true;
	}

	private function fillIssnl(): void
	{
		$issnlImport = new IssnlImport();
		$cmd = Yii::app()->db
			->createCommand(
				"SELECT issn, titreId FROM Issn "
				. "WHERE issn IS NOT NULL AND issnl IS NULL AND statut = " . Issn::STATUT_VALIDE
				. ($this->limit ? " LIMIT {$this->limit}" : '')
			)->setFetchMode(PDO::FETCH_ASSOC);
		foreach ($cmd->query() as $row) {
			$issn = $row['issn'];
			$issnl = $issnlImport->searchByIssn($issn);
			if ($issnl) {
				$issnRecord = Issn::model()->findBySql(
					"SELECT * FROM Issn WHERE issn = :issn",
					[':issn' => $issn]
				);
				$titre = Titre::model()->findByPk($issnRecord->titreId);
				$intervention = $this->createIntervention($titre);
				$intervention->contenuJson->updateByAttributes($issnRecord, ['issnl' => $issnl]);
				if (!$intervention->accept()) {
					printf("%s;%s;%s;\n", $row['titreId'], "ERREUR lors de l'ajout d'ISSN-L : " . print_r($intervention->errors, true), $issnl);
				}
				printf("%s;%s;%s;%s\n", $row['titreId'], "ISSNL ajouté (MàJ)", $issnl, $issn);
			} else {
				printf("%s;%s;%s;%s\n", $row['titreId'], 'ISSN absent du fichier importé', "", $issn);
			}
		}
	}

	private function fillIssn(): void
	{
		$issnlImport = new IssnlImport();
		$cmd = Yii::app()->db
			->createCommand(
				"SELECT issnl, GROUP_CONCAT(issn SEPARATOR ',') AS issns, titreId FROM Issn "
				. "WHERE issnl IS NOT NULL AND issnl <> ''"
				. " GROUP BY issnl"
				. ($this->limit ? " LIMIT {$this->limit}" : '')
			)->setFetchMode(PDO::FETCH_ASSOC);
		foreach ($cmd->query() as $row) {
			$issnl = $row['issnl'];
			$issns = explode(",", $row['issns']);
			$matchingIssnList = $issnlImport->searchByIssnl($issnl);
			if (!$matchingIssnList) {
				if ($this->verbose > 1) {
					printf("%s;%s;%s;%s\n", '', 'ISSNL absent du fichier importé', $issnl, "");
				}
				continue;
			}
			foreach ($matchingIssnList as $importedIssn) {
				if (in_array($importedIssn, $issns)) {
					// nothing to update
					continue;
				}
				if (count($matchingIssnList) > 1) {
					printf("%s;%s;%s;%s\n", $row['titreId'], "Cet ISSN-L a plusieurs ISSN. Insertion d'ISSN impossible.", $issnl, $importedIssn);
					continue;
				}
				$titre = Titre::model()->findByPk($row['titreId']);
				$intervention = $this->createIntervention($titre);
				$issnRecord = Issn::model()->findBySql(
					"SELECT * FROM Issn WHERE issnl = :issnl AND issn = ''",
					[':issnl' => $issnl]
				);
				if ($issnRecord) {
					$newRecord = clone ($issnRecord);
					$newRecord->issn = $importedIssn;
					$newRecord->support = Issn::SUPPORT_INCONNU;
					$intervention->contenuJson->update($issnRecord, $newRecord);
					printf("%s;%s;%s;%s\n", $row['titreId'], "ISSN ajouté (MàJ id {$issnRecord->id})", $issnl, $importedIssn);
				} else {
					$newRecord = new Issn();
					$newRecord->titreId = $titre->id;
					$newRecord->issn = $importedIssn;
					$newRecord->statut = Issn::STATUT_VALIDE;
					$newRecord->support = Issn::SUPPORT_INCONNU;
					$newRecord->issnl = $importedIssn;
					$intervention->contenuJson->create($newRecord);
					printf("%s;%s;%s;%s\n", $row['titreId'], "ISSN ajouté (nouvelle entrée)", $issnl, $importedIssn);
				}
				if ($this->simulation) {
					continue;
				}
				try {
					if (!$intervention->accept()) {
						printf("%s;%s;%s;%s\n", $row['titreId'], "ERREUR lors de l'ajout d'ISSN : " . print_r($intervention->errors, true), $issnl, $importedIssn);
					}
				} catch (\Exception $e) {
					printf("%s;%s;%s;%s\n", $row['titreId'], "ERREUR fatale lors de l'ajout d'ISSN. {$e->getMessage()}", $issnl, $importedIssn);
				}
			}
		}
	}

	private function createIntervention(Titre $titre)
	{
		$i = new Intervention();
		$i->setAttributes(
			[
				'ressourceId' => null,
				'revueId' => (int) $titre->revueId,
				'titreId' => (int) $titre->id,
				'editeurId' => null,
				'statut' => 'attente',
				'ip' => '',
				'contenuJson' => new InterventionDetail(),
				'suivi' => null !== Suivi::isTracked($titre),
				'description' => "Modification du titre « {$titre->titre} »",
			],
			false
		);
		$i->setImport(ImportType::getSourceId('issn.org'));
		return $i;
	}

	private static function printCsvLine(array $row, string $msg): void
	{
		printf("%d\t%s\t%s\t%s\n", $row['titreId'], $msg, $row['issnl'], $row['issn']);
	}
}
