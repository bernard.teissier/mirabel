<?php
/* @var $db array */
/* @var $prefix string */
?>
source <?= $prefix ?>titres
{
	type      = mysql
	sql_host  = <?= $db['host'] . "\n" ?>
	sql_user  = <?= $db['user'] . "\n" ?>
	sql_pass  = <?= $db['pass'] . "\n" ?>
	sql_db    = <?= $db['dbname'] . "\n" ?>

	sql_query_pre = SET NAMES utf8
	sql_query_pre = SET SESSION query_cache_type=OFF
	sql_query_pre = SET SESSION max_heap_table_size=268435456
	sql_query_pre = SET SESSION tmp_table_size=268435456
	sql_query_pre = CREATE TEMPORARY TABLE TitreTmp \
		(\
		cletri INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, \
		id INT(11) UNSIGNED NOT NULL, \
		sigle VARCHAR(255) NOT NULL, \
		titrecomplet VARCHAR(1023) NOT NULL, \
		titrerecherche VARCHAR(1023) NOT NULL, \
		lettre1 TINYINT UNSIGNED NOT NULL, \
		revueid INT(11) UNSIGNED NOT NULL, \
		obsolete BOOLEAN NOT NULL, \
		langues VARCHAR(255) NOT NULL, \
		revuesanslangue BOOLEAN NOT NULL, \
		hdatemodif INT(11) UNSIGNED NOT NULL, \
		hdateverif INT(11) UNSIGNED NOT NULL, \
		nbacces SMALLINT UNSIGNED NOT NULL, \
		vivant BOOLEAN NOT NULL \
		) DEFAULT CHARACTER SET utf8 \
		SELECT NULL AS cletri, \
			t.id, \
			t.sigle, \
			CONCAT(t.prefixe,t.titre, IF(t.sigle<>'', CONCAT(' — ', t.sigle), '')) as titrecomplet, \
			'' AS titrerecherche, \
			FIND_IN_SET(LEFT(REPLACE(t.titre, '[', ''), 1), 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z') AS lettre1, \
			t.revueId AS revueid, \
			(t.obsoletePar IS NOT NULL) AS obsolete, \
			REPLACE(t.langues, ',', ' ') AS langues, \
			IF(t.obsoletePar IS NULL AND t.langues = '', 1, 0) AS revuesanslangue, \
			GREATEST(t.hdateModif, MAX(t.hdateModif)) AS hdatemodif, \
			IFNULL(r.hdateVerif, 0) AS hdateverif, \
			COUNT(s.id) AS nbacces, \
			(t.dateFin = '') AS vivant \
		FROM Titre AS t \
		JOIN Revue AS r ON t.revueId=r.id \
		JOIN Titre AS t2 ON (t.revueId=t2.revueId AND t2.statut='normal') \
		LEFT JOIN Service AS s ON (t2.id=s.titreId AND s.statut='normal') \
		WHERE t.statut = 'normal' \
		GROUP BY t.id \
		ORDER BY REPLACE(t.titre, '[', '')
	sql_query_pre = ALTER TABLE TitreTmp ADD INDEX i_tmp (id)
	sql_query_pre = UPDATE TitreTmp \
		SET titrerecherche = REGEXP_REPLACE(titrecomplet COLLATE utf8_bin, '([A-Z])\\.\\s?(?=[A-Z]\\b)', '\\1')
	sql_ranged_throttle	= 0

	sql_query = SELECT id, sigle, titrecomplet, titrerecherche, lettre1, revueid, cletri, obsolete, langues, revuesanslangue, \
		hdatemodif, hdateverif, nbacces, vivant \
		FROM TitreTmp

	sql_attr_uint = revueid
	sql_attr_uint = lettre1
	sql_attr_uint = nbacces
	sql_attr_uint = cletri

	sql_attr_bool = obsolete
	sql_attr_bool = vivant
	sql_attr_bool = revuesanslangue

	sql_attr_timestamp = hdatemodif
	sql_attr_timestamp = hdateverif

	sql_attr_multi = uint paysid from query;\
		SELECT te.titreId, e.paysId FROM Titre_Editeur te JOIN Editeur e ON te.editeurId = e.id ORDER BY te.titreId ASC
	sql_attr_multi = uint editeurid from query;\
		SELECT titreId, editeurId FROM Titre_Editeur ORDER BY titreId ASC
	sql_attr_multi = uint ressourceid from query; \
		SELECT titreId, ressourceId FROM Service ORDER BY titreId ASC
	sql_attr_multi = uint collectionid from query; \
		SELECT s.titreId, sc.collectionId FROM Service s JOIN Service_Collection sc ON sc.serviceId = s.id ORDER BY s.titreId ASC
	sql_attr_multi = uint suivi from query; \
		SELECT t.id, s.partenaireId FROM Suivi s JOIN Titre t ON (s.cible = 'Revue' AND s.cibleId = t.revueId) ORDER BY t.id ASC
	sql_attr_multi = uint detenu from query; \
		SELECT titreId, partenaireId FROM Partenaire_Titre ORDER BY titreId ASC
	sql_attr_multi = uint acces from query; \
		SELECT DISTINCT titreId, \
			CASE type WHEN 'Integral' THEN 2 WHEN 'Resume' THEN 3 WHEN 'Sommaire' THEN 4 WHEN 'Indexation' THEN 5 ELSE 1 END AS type \
		FROM Service ORDER BY titreId ASC
	sql_attr_multi = uint acceslibre from query; \
		SELECT DISTINCT titreId, \
			CASE type WHEN 'Integral' THEN 2 WHEN 'Resume' THEN 3 WHEN 'Sommaire' THEN 4 WHEN 'Indexation' THEN 5 ELSE 1 END AS type \
		FROM Service WHERE acces = 'libre' ORDER BY titreId ASC
	sql_attr_multi = bigint issn from query; \
		(SELECT titreId, CONVERT(LEFT(REPLACE(issn , '-', ''), 7), UNSIGNED) FROM Issn WHERE issn != '') \
		UNION \
		(SELECT titreId, CONVERT(LEFT(REPLACE(issnl , '-', ''), 7), UNSIGNED) FROM Issn WHERE issnl != '') \
		ORDER BY titreId ASC
	# categorie : pour chaque titre, categorieId associé soit via la revue soit par un alias
	sql_attr_multi = bigint categorie from query; \
		SELECT DISTINCT * FROM ( \
			SELECT t.id, IFNULL(cr.categorieId, ca.categorieId) AS categorieId FROM Titre t \
			LEFT JOIN Categorie_Revue cr ON t.revueId = cr.revueId \
			LEFT JOIN CategorieAlias_Titre ct ON ct.titreId = t.id LEFT JOIN CategorieAlias ca ON ct.categorieAliasId = ca.id \
		) t WHERE categorieId IS NOT NULL ORDER BY t.id, t.categorieId
	# revuecategorie : pour chaque titre, categorieId associé soit via la revue soit par un alias sur un des titres de la revue
	sql_attr_multi = bigint revuecategorie from query; \
		SELECT DISTINCT * FROM ( \
			SELECT t.id, IFNULL(cr.categorieId, ca.categorieId) AS categorieId FROM Titre t \
			LEFT JOIN Categorie_Revue cr ON t.revueId = cr.revueId \
			JOIN Titre t2 ON t.revueId = t2.revueId LEFT JOIN CategorieAlias_Titre ct ON ct.titreId = t2.id LEFT JOIN CategorieAlias ca ON ct.categorieAliasId = ca.id \
		) t WHERE categorieId IS NOT NULL ORDER BY t.id, t.categorieId
	# abonnement : liste de partenaireId abonnés à une ressource ou une collections des accès de ce titre
	sql_attr_multi = uint abonnement from query; \
		SELECT * FROM ( \
			SELECT s.titreId, a.partenaireId \
				FROM Abonnement a \
				JOIN Service s ON s.ressourceId = a.ressourceId \
				WHERE a.collectionId IS NULL AND a.partenaireId IS NOT NULL \
			UNION \
			SELECT s.titreId, a.partenaireId \
				FROM Abonnement a \
				JOIN Service_Collection sc ON sc.collectionId = a.collectionId \
				JOIN Service s ON sc.serviceId = s.id \
				WHERE a.partenaireId IS NOT NULL \
		) t GROUP BY titreId, partenaireId
	# lien : liste de sourceId associés à ce titre
	sql_attr_multi = uint lien from query; \
		SELECT DISTINCT titreId, sourceId FROM LienTitre WHERE sourceId IS NOT NULL ORDER BY titreId

	sql_attr_string = titrecomplet
}
index <?= $prefix ?>titres
{
	source = <?= $prefix ?>titres
	path   = indexes/<?= $prefix ?>titres
	charset_table    = <?php include(__DIR__ . '/charsetTable.txt'); echo "\n"; ?>
	morphology       = libstemmer_fr
	min_stemming_len = 2
	stopwords        = <?= dirname(__DIR__, 5) ?>/sphinx/stopwords.txt
	docinfo = extern
	mlock   = 0
	min_word_len   = 1
	min_prefix_len = 3
	html_strip = 0
	index_exact_words = 1
	expand_keywords   = 1
}

source <?= $prefix ?>ressources
{
	type      = mysql
	sql_host  = <?= $db['host'] . "\n" ?>
	sql_user  = <?= $db['user'] . "\n" ?>
	sql_pass  = <?= $db['pass'] . "\n" ?>
	sql_db    = <?= $db['dbname'] . "\n" ?>

	sql_query_pre = SET NAMES utf8
	sql_query_pre = SET SESSION query_cache_type=OFF
	sql_query_pre = CREATE TEMPORARY TABLE RessourceTmp \
		(\
		cletri INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, \
		id INT(11) UNSIGNED NOT NULL, \
		nbrevues SMALLINT UNSIGNED NOT NULL \
		) ENGINE=InnoDB DEFAULT CHARACTER SET utf8 \
		SELECT NULL AS cletri, r.id AS id, COUNT(DISTINCT t.revueId) AS nbrevues \
		FROM Ressource r LEFT JOIN Service s ON s.ressourceId = r.id LEFT JOIN Titre t ON (s.titreId=t.id) \
		GROUP BY r.id ORDER BY r.nom
	sql_query_pre = ALTER TABLE RessourceTmp ADD INDEX i_tmp (id)
	sql_ranged_throttle	= 0

	sql_query = SELECT \
			r.id, \
			r.sigle, \
			CONCAT(r.prefixe, r.nom, IF(r.sigle<>'', CONCAT(' — ', r.sigle), '')) as nomcomplet, \
			r.description, r.noteContenu, r.disciplines, \
			FIND_IN_SET(LEFT(r.nom, 1), 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z') AS lettre1, \
			cletri, \
			r.autoImport AS autoimport, \
			COUNT(c.id) AS autoimportcollections, \
			(r.type = 'SiteWeb') AS web, \
			r.hdateModif AS hdatemodif, \
			r.hdateVerif AS hdateverif, \
			nbrevues \
		FROM Ressource r JOIN RessourceTmp USING (id) \
			LEFT JOIN Collection c ON c.ressourceId = r.id AND c.importee = 1 \
		GROUP BY r.id \
		ORDER BY cletri

	sql_joined_field = collections from query; \
		SELECT ressourceId, nom FROM Collection WHERE visible = 1 ORDER BY ressourceId ASC

	sql_attr_uint = lettre1
	sql_attr_uint = nbrevues
	sql_attr_uint = cletri
	sql_attr_bool = autoimportcollections

	sql_attr_bool = autoimport
	sql_attr_bool = web

	sql_attr_timestamp = hdatemodif
	sql_attr_timestamp = hdateverif

	sql_attr_multi = uint suivi from query; \
		SELECT cibleId, partenaireId FROM Suivi WHERE cible = 'Ressource' ORDER BY cibleId ASC

	sql_field_string = nomcomplet
}
index <?= $prefix ?>ressources : <?= $prefix ?>titres
{
	source = <?= $prefix ?>ressources
	path   = indexes/<?= $prefix ?>ressources
	charset_table = <?php include(__DIR__ . '/charsetTable.txt'); ?>, U+2E->.
}

source <?= $prefix ?>editeurs
{
	type      = mysql
	sql_host  = <?= $db['host'] . "\n" ?>
	sql_user  = <?= $db['user'] . "\n" ?>
	sql_pass  = <?= $db['pass'] . "\n" ?>
	sql_db    = <?= $db['dbname'] . "\n" ?>

	sql_query_pre = SET NAMES utf8
	sql_query_pre = SET SESSION query_cache_type=OFF
	sql_query_pre = CREATE TEMPORARY TABLE EditeurTmp \
		(\
		cletri INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, \
		id INT UNSIGNED NOT NULL, \
		nomcomplet VARCHAR(255) NOT NULL, \
		lettre1 TINYINT UNSIGNED NOT NULL, \
		nbrevues INT UNSIGNED NOT NULL, \
		nbtitresmorts INT UNSIGNED NOT NULL DEFAULT 0, \
		nbtitresvivants INT UNSIGNED NOT NULL DEFAULT 0 \
		) DEFAULT CHARACTER SET utf8 \
		SELECT NULL AS cletri, \
			e.id, \
			CONCAT(e.prefixe, e.nom, IF(e.sigle<>'', CONCAT(' — ', e.sigle), '')) as nomcomplet, \
			FIND_IN_SET(LEFT(e.nom, 1), 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z') AS lettre1, \
			COUNT(DISTINCT t.revueId) AS nbrevues, \
			0 AS nbtitresmorts, \
			0 AS nbtitresvivants\
		FROM Editeur e \
			LEFT JOIN Titre_Editeur te ON te.editeurId = e.id \
			LEFT JOIN Titre t ON t.id = te.titreId \
		WHERE e.statut = 'normal' \
		GROUP BY e.id \
		ORDER BY e.nom
	sql_query_pre = ALTER TABLE EditeurTmp ADD INDEX i_tmp (id)
	sql_query_pre = UPDATE EditeurTmp \
		JOIN (SELECT te.editeurId AS id, count(DISTINCT t.id) AS nb FROM Titre_Editeur te JOIN Titre t ON t.id = te.titreId WHERE t.obsoletePar IS NULL AND t.dateFin = '' AND IFNULL(te.ancien, 0) = 0 GROUP BY te.editeurId) t USING(id) \
		SET nbtitresvivants = t.nb
	sql_query_pre = UPDATE EditeurTmp \
		JOIN (SELECT te.editeurId AS id, count(DISTINCT t.id) AS nb FROM Titre_Editeur te JOIN Titre t ON t.id = te.titreId GROUP BY te.editeurId) t USING(id) \
		SET nbtitresmorts = t.nb - nbtitresvivants
	sql_ranged_throttle	= 0

	sql_query = SELECT \
			id, \
			sigle, \
			nomcomplet, \
			description, geo, \
			lettre1, \
			cletri, \
			LEFT(idref, 8) AS idref, \
			nbrevues, \
			nbtitresmorts, \
			nbtitresvivants, \
			paysId AS paysid, \
			sherpa, \
			hdateModif AS hdatemodif, \
			hdateVerif AS hdateverif \
		FROM Editeur JOIN EditeurTmp USING (id) \
		WHERE statut = 'normal' \
		ORDER BY cletri

	sql_attr_uint = lettre1
	sql_attr_uint = cletri
	sql_attr_uint = idref
	sql_attr_uint = nbrevues
	sql_attr_uint = nbtitresmorts
	sql_attr_uint = nbtitresvivants
	sql_attr_uint = paysid
	sql_attr_uint = sherpa

	sql_attr_timestamp = hdatemodif
	sql_attr_timestamp = hdateverif

	sql_attr_multi	= uint suivi from query; \
		SELECT cibleId, partenaireId FROM Suivi WHERE cible = 'Editeur' ORDER BY cibleId ASC

	sql_field_string	= nomcomplet
}
index <?= $prefix ?>editeurs : <?= $prefix ?>titres
{
	source = <?= $prefix ?>editeurs
	path   = indexes/<?= $prefix ?>editeurs
}
