<?php
/* @var $sphinx array */
?>

indexer
{
	mem_limit = 64M
}
searchd
{
	listen = <?php echo $sphinx['port']; ?>:sphinx
	listen = /tmp/sphinx-mirabel-mysql.socket:mysql41

	log         = log/searchd.log
	binlog_path =
	#query_log  = log/query.log
	pid_file    = run/searchd.pid

	read_timeout    = 5
	client_timeout  = 300
	max_children    = 30
	seamless_rotate = 1
	preopen_indexes = 1
	unlink_old      = 1
	mva_updates_pool = 1M
	max_packet_size	 = 8M
	max_filters       = 256
	max_filter_values = 4096
	max_batch_queries = 32

	collation_server      = libc_ci
	collation_libc_locale = fr_FR.utf8
}
