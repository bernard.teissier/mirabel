<?php

/**
 * Description of CategorieCommand
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class CategorieCommand extends CConsoleCommand
{
	public $separator = "\t";

	public $dryRun = false;

	public function actionLoad($csv)
	{
		if (!file_exists($csv)) {
			echo "Fichier '$csv' non trouvé.\n";
			return 1;
		}
		$handle = fopen($csv, "r");
		if (!$handle) {
			echo "Fichier '$csv' non lisible.\n";
			return 2;
		}
		while ($row = fgetcsv($handle, 0, $this->separator)) {
			if (trim($row[0]) === 'Parent') {
				continue;
			}
			if (!$row[0]) {
				$row[0] = "racine";
			}
			$parent = Categorie::model()->findByAttributes(['categorie' => $row[0]]);
			if ($parent) {
				echo "Ajoute {$row[1]} sous {$parent->categorie}\n";
				if (!$this->dryRun) {
					$categorie = new Categorie();
					$categorie->parentId = $parent->id;
					$categorie->categorie = trim($row[1]);
					$categorie->description = trim($row[2]);
					$categorie->role = $row[3];
					$categorie->modifPar = 4;
					$categorie->save();
				}
			} else {
				echo "Erreur : parent non trouvé pour {$row[1]}.\n";
			}
		}
	}

	public function actionStats()
	{
		CategorieStats::fill();
	}
}
