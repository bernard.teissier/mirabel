<?php

use commands\models\linkimport;
use models\sherpa\Api;

class SherpaCommand extends CConsoleCommand
{
	use \commands\models\MaintenanceModeTrait;

	public $verbose = 1;

	public function actionTest(string $issn = '1179-3163'): int
	{
		$key = Yii::app()->params->itemAt('sherpa')['api-key'];
		$api = new Api($key);
		$json = $api->fetchByIssn($issn);
		$publications = json_decode($json); // into \stdClass
		if (empty($publications->items)) {
			echo "Empty response, unknown ISSN.\n";
			exit(0);
		}
		$item = new models\sherpa\Item($publications->items[0]);
		echo json_encode($item, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
		return 1;
	}

	public function actionImport(array $email = [], int $limit = 0, string $obsoleteSince = ''): int
	{
		$params = [
			'email' => $email,
			'limit' => $limit,
			'obsoleteSince' => $obsoleteSince,
			'verbose' => $this->verbose,
		];
		$importer = new linkimport\Sherpa($params);
		$importer->import();
		return 0;
	}

	public function actionIdentifyPublishers()
	{
		$jsonPath = (string) Yii::getPathOfAlias('application.runtime/sherpa');
		foreach (glob("$jsonPath/*.json") as $file) {
			$data = json_decode(file_get_contents($file));
			if (empty($data->items[0]->publishers)) {
				continue;
			}
			$publishers = $data->items[0]->publishers;
			$issn = $data->items[0]->issns[0]->issn;
			$editeurs = Editeur::model()->findAllBySql(
				<<<EOSQL
				SELECT e.*
				FROM Editeur e
					JOIN Titre_Editeur te ON e.id = te.editeurId
					JOIN Issn i USING(titreId)
				WHERE i.issn = :issn AND e.sherpa IS NULL
				EOSQL,
				[':issn' => $issn]
			);
			foreach ($editeurs as $e) {
				/** @var Editeur $e */
				foreach ($publishers as $publisher) {
					$p = $publisher->publisher;
					if (
						$e->nom === $p->name[0]->name
						|| (isset($p->url) && rtrim($e->url, "/") === rtrim($p->url, "/"))
					) {
						$i = $e->buildIntervention(true);
						$i->action = 'editeur-U';
						$i->contenuJson->updateByAttributes($e, ['sherpa' => $p->id]);
						$i->accept(true);
						$e->sherpa = $p->id;
						fputcsv(STDOUT, [$e->id, $e->nom, $e->sherpa, $issn], ";");
					}
				}
			}
		}
	}
}
