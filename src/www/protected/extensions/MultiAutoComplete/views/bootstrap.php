<?php
/* @var $model object */
/* @var $attribute string */
/* @var $htmlOptions array */

$labelOptions = [
	'for' => 'Complete' . $htmlOptions['id'],
	'class' => 'control-label',
];
if (isset($htmlOptions['class'])) {
	$labelOptions['class'] .=  ' ' . $htmlOptions['class'];
}

?>
<div class="control-group">
	<?php
	echo CHtml::activeLabelEx($model, $attribute, $labelOptions);
	?>
	<div class="controls">
		<div class="hidden" id="<?php echo $htmlOptions['id'] . 'Template'; ?>">
			<label>
				<input type="checkbox" name="<?php echo $htmlOptions['name'] ?>[]" value="" />
			</label>
		</div>
		<?php
		if (!empty($foreign)) {
			foreach ($model->{$attribute} as $fk) {
				if (!isset($foreign[$fk])) {
					continue;
				}
				echo '<div>'
					. '<label>'
					. '<input type="checkbox" name="' . $htmlOptions['name']
					. '[]" value="' . $fk . '" checked="checked" /> '
					. CHtml::encode($foreign[$fk])
					. '</label>'
					. '</div>';
			}
		}
		$htmlOptions['name'] = 'Complete' . $htmlOptions['name'];
		$htmlOptions['id'] = 'Complete' . $htmlOptions['id'];
		echo CHtml::textField($htmlOptions['name'], '', $htmlOptions);
		if (!empty($htmlOptions['hint'])) {
			echo CHtml::tag(
				'a',
				[
					'href' => '#',
					'data-title' => $model->getAttributeLabel($attribute),
					'data-content' => $htmlOptions['hint'],
					'data-html' => true,
					'data-trigger' => 'hover',
					'rel' => 'popover',
				],
				'?'
			);
		}
		echo CHtml::error($model, $attribute, $htmlOptions);
		?>
	</div>
</div>
