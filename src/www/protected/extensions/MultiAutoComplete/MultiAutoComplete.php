<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */

Yii::import('zii.widgets.jui.CJuiInputWidget');

class MultiAutoComplete extends CJuiInputWidget
{
	// Useful inherited attribute: model, attribute, htmlOptions.

	/**
	 * @var string URL where to send a GET request with "term=?"
	 */
	public $ajaxUrl;

	/**
	 * @var string Name of the AR class (for displaying statically the selected elements).
	 */
	public $foreignModelName;

	/**
	 * @var string Name of the AR attribute (for displaying statically the selected elements).
	 */
	public $foreignAttribute;

	/**
	 * @var string Name of the view file to use. Default value is "bootstrap".
	 */
	public $viewFile = 'bootstrap';

	public function init()
	{
		parent::init();
		$url = Yii::app()->getAssetManager()->publish(__DIR__ . '/assets/mva.js');
		Yii::app()->getClientScript()->registerScriptFile($url);
	}

	public function run()
	{
		list($name, $id) = $this->resolveNameID();
		if (isset($this->htmlOptions['id'])) {
			$id = $this->htmlOptions['id'];
		} else {
			$this->htmlOptions['id'] = $id;
		}
		if (isset($this->htmlOptions['name'])) {
			$name = $this->htmlOptions['name'];
		} else {
			$this->htmlOptions['name'] = $name;
		}

		$foreign = $this->buildForeignAssoc();

		$this->options['source'] = 'js:function(request, response) {
	$.ajax({
		url: "' . CHtml::normalizeUrl($this->ajaxUrl) . '",
		data: { term: request.term },
		success: function(data) { response(data); }
	});
}
';
		$this->options['select'] = "js:function(event, ui) {
	if (ui.item.id) {
		addNewCompleteItem('{$id}', ui.item.id, ui.item.label);
		jQuery('#Complete{$id}').val('');
	}
	return false;
}";
		//$this->name = $name . 'Complete';

		$this->renderFile(
			__DIR__ . '/views/' . $this->viewFile . '.php',
			[
				'model' => $this->model,
				'attribute' => $this->attribute,
				'htmlOptions' => $this->htmlOptions,
				'foreign' => $foreign,
			]
		);

		$options = CJavaScript::encode($this->options);
		$js = "jQuery('#Complete{$id}').autocomplete($options);";
		$cs = Yii::app()->getClientScript();
		$cs->registerScript(__CLASS__ . '#Complete' . $id, $js);
	}

	protected function buildForeignAssoc()
	{
		$model = $this->model;
		$attribute = $this->attribute;
		if (empty($model->{$attribute}) or !isset($this->foreignModelName)) {
			return [];
		}
		$foreignModel = call_user_func([$this->foreignModelName, 'model']);
		$foreign = $foreignModel->findAllByPk($model->{$attribute});
		$assoc = [];
		foreach ($foreign as $f) {
			$assoc[$f->primaryKey] = $f->{$this->foreignAttribute};
		}
		return $assoc;
	}
}
