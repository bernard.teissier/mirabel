<?php

namespace extensions\gefx;

/**
 * Cf <https://punktokomo.abes.fr/2014/06/26/metarevues-un-outil-dedie-au-traitement-des-periodiques/>
 * et <http://documentation.abes.fr/sudoc/manuels/administration/aidewebservices/Metarevue.html>
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class GefxGraph
{
	public $nodes = [];

	public $edges = [];

	private static $labelTexts = [
		440 => "Devient",
		441 => "Devient partiellement",
		442 => "Remplacé par",
		443 => "Remplacé partiellement par",
		444 => "Absorbé par",
		445 => "Absorbé partiellement par",
		446 => "Scindé en … et en …",
		447 => "Fusionne avec … pour donner…",
		431 => "Suite partielle de",
		432 => "Remplace",
		433 => "Remplace partiellement",
		434 => "Absorbe",
		435 => "Absorbe partiellement",
		436 => "Fusion de … et de …",
		437 => "Séparé de",
		452 => "support différent",
		/*
		430 => "… a pour suite …",
		431 => "… a pour suite partielle …",
		432 => "… remplace …",
		433 => "… remplace partiellement …",
		434 => "… absorbe …",
		435 => "… absorbe partiellement …",
		436 => "Fusion de … (et de) en …",
		437 => "… séparé de …",
		440 => "… devient …",
		441 => "… devient partiellement …",
		442 => "… remplacé par …",
		443 => "… remplacé partiellement par …",
		444 => "… absorbé par …",
		445 => "… absorbé partiellement par …",
		446 => "… scindé en … (et en)",
		447 => "… fusionne (avec) pour donner …",
		452 => "support différent",
		 */
	];

	/**
	 * Build an instance from the first "graph" node of the XML string.
	 *
	 * @param string $xml
	 */
	public static function fromXml($xml)
	{
		\libxml_use_internal_errors(true);
		try {
			$dom = new \DOMDocument();
			$dom->loadXML($xml);
		} catch (\Exception $e) {
			return null;
		}
		if (!$dom || $dom->childNodes->length === 0) {
			return null;
		}
		\libxml_use_internal_errors(false);
		$new = new self;
		$graphs = $dom->getElementsByTagName('graph');
		if ($graphs) {
			$attributes = [];
			foreach ($graphs->item(0)->childNodes as $node) {
				if ($node->nodeType !== XML_ELEMENT_NODE) {
					continue;
				}
				switch ($node->tagName) {
					case 'attributes':
						foreach ($node->childNodes as $a) {
							if ($a->nodeType !== XML_ELEMENT_NODE) {
								continue;
							}
							$attributes[$a->getAttribute('id')] = $a->getAttribute('title');
						}
						break;
					case 'nodes':
						foreach ($node->childNodes as $a) {
							if ($a->nodeType !== XML_ELEMENT_NODE) {
								continue;
							}
							$journal = GefxNode::fromDom($a, $attributes);
							$new->nodes[$journal->id] = $journal;
						}
						break;
					case 'edges':
						foreach ($node->childNodes as $e) {
							if ($e->nodeType !== XML_ELEMENT_NODE) {
								continue;
							}
							$new->edges[] = [
								'source' => $e->getAttribute('source'),
								'target' => $e->getAttribute('target'),
								'label' => $e->getAttribute('label'),
								'text' => self::getEdgeText($e->getAttribute('label')),
							];
						}
						break;
					default:
						break;
				}
			}
		}
		return $new;
	}

	public static function fromPpn($ppn)
	{
		$url = 'https://www.sudoc.fr/services/metaperiodical/' . $ppn . '.gexf';
		try {
			$xml = (new \Curl)->get($url)->getContent();
		} catch (\Exception $e) {
			fprintf(STDERR, "CURL: $url\n\t" . $e->getMessage() . "\n");
		}
		if (empty($xml)) {
			return null;
		}
		return self::fromXml($xml);
	}

	public function getRelations($id)
	{
		$relations = [];
		foreach ($this->edges as $e) {
			$relation = "";
			$fstr = '[%s] ' . "({$e['label']}) {$e['text']}" . ' [%s]';
			if ($e['source'] == $id) {
				$relation = sprintf($fstr, "ce titre", $this->nodes[$e['target']]->label);
			} elseif ($e['target'] == $id) {
				$relation = sprintf($fstr, $this->nodes[$e['source']]->label, "ce titre");
			} else {
				$relation = sprintf($fstr, $this->nodes[$e['source']]->label, $this->nodes[$e['target']]->label);
			}
			$relations[] = $relation;
		}
		return $relations;
	}

	private static function getEdgeText($label)
	{
		if (isset(self::$labelTexts[(int) $label])) {
			return self::$labelTexts[(int) $label];
		}
		return '';
	}
}
