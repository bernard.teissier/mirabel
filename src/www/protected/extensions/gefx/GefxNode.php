<?php

namespace extensions\gefx;

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class GefxNode
{
	public $id;

	public $label;

	public $attributes = [];

	public static function fromDom(\DOMElement $dom, array $attributes)
	{
		$new = new self;
		$new->id = $dom->getAttribute('id');
		$new->label = $dom->getAttribute('label');
		$subnodes = $dom->getElementsByTagName('attvalues');
		if ($subnodes) {
			foreach ($subnodes->item(0)->childNodes as $attvalue) {
				$for = $attvalue->getAttribute('for');
				$value = $attvalue->getAttribute('value');
				$new->attributes[$attributes[$for]] = $value;
			}
		}
		return $new;
	}

	public function getPublicationDate()
	{
		if (isset($this->attributes['publicationDate'])) {
			return $this->attributes['publicationDate'];
		}
		return null;
	}

	public function getIssn()
	{
		if (isset($this->attributes['issn'])) {
			return $this->attributes['issn'];
		}
		return null;
	}
}
