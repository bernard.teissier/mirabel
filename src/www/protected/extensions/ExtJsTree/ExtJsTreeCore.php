<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class ExtJsTreeCore extends CComponent
{
	/**
	 * @var array See http://www.jstree.com/api/#/?f=$.jstree.defaults.core.data
	 */
	public $data;

	/**
	 * @var int|bool The open / close animation duration in milliseconds.
	 */
	public $animation = false;

	/**
	 * @var bool|string Allow modifying operations on the tree.
	 *                     To enable editing, set it to true or to a JS function.
	 */
	public $checkCallback = false;

	/**
	 * @var bool Web workers will be used to parse incoming JSON data,
	 *    so that the UI will not be blocked by large requests.
	 *    Workers are however about 30% slower.
	 */
	public $worker = true;

	public $themes;

	/**
	 * @var array
	 */
	public $strings;

	public function translateToFr()
	{
		$this->strings = [
			"Loading ..." => "Chargement en cours",
			"New node" => "Nouveau nœud",
		];
	}

	public function toJs()
	{
		$config = [
			"data" => $this->data,
			"animation" => $this->animation,
			"worker" => $this->worker,
			"check_callback" => $this->checkCallback,
		];
		if ($this->strings) {
			$config["strings"] = $this->strings;
		}
		if ($this->themes) {
			$config["themes"] = $this->themes;
		}
		return CJavaScript::encode($config);
	}
}
