<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class ExtJsTree extends CComponent
{
	/**
	 * @var ExtJsTreeCore
	 */
	public $core;

	/**
	 * @var array
	 */
	public $types;

	/**
	 * @var array
	 */
	public $plugins;

	/**
	 * @param ExtJsTreeCore $core
	 * @param array $plugins
	 */
	public function __construct(ExtJsTreeCore $core, array $plugins = [])
	{
		$this->core = $core;
		$this->plugins = $plugins;
	}

	/**
	 * @param string $selector (opt, ".jstree")
	 */
	public function init($selector = ".jstree")
	{
		$this->registerAssets();
		$this->registerJsInit($selector);
	}

	private function registerAssets()
	{
		$dir = __DIR__ . DIRECTORY_SEPARATOR;
		$am = Yii::app()->getAssetManager();
		/* @var $am CAssetManager */
		$cs = Yii::app()->clientScript;
		/* @var $cs CClientScript */

		// CSS
		if (YII_DEBUG) {
			$cssFile = '/default/style.css';
		} else {
			$cssFile = '/default/style.min.css';
		}
		$themesDir = $am->publish($dir . 'assets/themes');
		$cs->registerCssFile($themesDir . $cssFile);

		// JS
		$cs->registerCoreScript('jquery');
		if (YII_DEBUG) {
			$file = $dir . 'assets/jstree.js';
		} else {
			$file = $dir . 'assets/jstree.min.js';
		}
		$cs->registerScriptFile($am->publish($file));
	}

	/**
	 * @param string $selector
	 */
	private function registerJsInit($selector)
	{
		Yii::app()->clientScript->registerScript('initTree', '
$(' . CJavaScript::encode($selector) . ').jstree({
	"core": ' . $this->core->toJs() . ',
	"types": ' . CJavaScript::encode($this->types) . ',
	"plugins": ' . CJavaScript::encode($this->plugins) . '
});
');
	}
}
