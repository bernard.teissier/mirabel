<?php

/**
 * This extension saves log entries in the DB in a richer format than CDbLogRoute.
 *
 * Enable by putting in the config:
 *	components => array(
 * 		'log' => array(
 *			'class' => 'CLogRouter',
 *			'routes' => array(
 *				array(
 *					'class' => 'ext.ExtDbLogRoute.ExtDbLogRoute',
 *					'levels' => 'error, warning, info',
 *					'connectionID' => 'db', // a component key (if not set, SQLite is used)
 *				),
 *			),
 *		),
 *	)
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */

class ExtDbLogRoute extends CDbLogRoute
{
	public $logTableName = 'LogExt';

	/**
	 * Creates the DB table for storing log messages.
	 *
	 * @param CDbConnection $db the database connection
	 * @param string $tableName the name of the table to be created
	 */
	protected function createLogTable($db, $tableName)
	{
		$driver=$db->getDriverName();
		if ($driver==='mysql') {
			$logID='id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY';
		} elseif ($driver==='pgsql') {
			$logID='id SERIAL PRIMARY KEY';
		} else {
			$logID='id INTEGER NOT NULL PRIMARY KEY';
		}

		$sql="
CREATE TABLE $tableName
(
	$logID,
	level VARCHAR(128) NOT NULL,
	category VARCHAR(128) NOT NULL,
	logtime INTEGER NOT NULL,
	message TEXT NOT NULL,
	controller VARCHAR( 128 ) NOT NULL ,
	action VARCHAR( 128 ) NOT NULL ,
	modelId INT NULL ,
	ip VARCHAR( 40 ) BINARY NOT NULL ,
	userId INT NULL ,
	session VARCHAR( 128 ) BINARY NOT NULL
)";
		$db->createCommand($sql)->execute();
	}

	/**
	 * Stores log messages into database.
	 * @param array $logs list of log messages
	 */
	protected function processLogs($logs)
	{
		$sql = "
INSERT INTO {$this->logTableName}
(level, category, logtime, message, controller, action, modelId, ip, userId, session) VALUES
(:level, :category, :logtime, :message, :controller, :action, :modelId, :ip, :userId, :session)
";
		$command = $this->getDbConnection()->createCommand($sql);

		$controller = Yii::app()->getController();
		if (isset($controller)) {
			$cname = $controller->getUniqueId();
			$aname = isset($controller->action) ? $controller->action->getId() : '';
		} else {
			if ((Yii::app() instanceof CWebApplication) && !empty(Yii::app()->request->pathInfo)) {
				if (strpos(Yii::app()->request->pathInfo, '/') !== false) {
					list($cname, $aname) = explode('/', Yii::app()->request->pathInfo);
				} else {
					$cname = Yii::app()->request->pathInfo;
					$aname = "";
				}
			} else {
				$cname = "";
				$aname = "";
			}
		}
		$command->bindValue(':controller', $cname);
		$command->bindValue(':action', $aname);
		$command->bindValue(':modelId', isset($_REQUEST['id']) ? (int) $_REQUEST['id'] : null);
		if (Yii::app() instanceof CWebApplication) {
			$command->bindValue(':ip', $_SERVER['REMOTE_ADDR'] ?? '');
			if (Yii::app()->user->isGuest) {
				$command->bindValue(':userId', null);
			} else {
				$command->bindValue(':userId', (int) Yii::app()->user->getId());
			}
			$command->bindValue(':session', session_id());
		} else {
			$command->bindValue(':ip', '');
			$command->bindValue(':userId', null);
			$command->bindValue(':session', '');
		}
		foreach ($logs as $log) {
			$command->bindValue(':level', $log[1]);
			$command->bindValue(':category', $log[2]);
			$command->bindValue(':logtime', (int) $log[3]);
			$command->bindValue(':message', $log[0]);
			$command->execute();
		}
	}
}
