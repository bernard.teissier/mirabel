<?php

namespace extensions\format;

/**
 * Formatter with French localization and added types.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class ExtFormatter extends \CFormatter
{
	public $strDateFormats = [
		'datetime' => '%d %B %Y à %R',
		'date' => '%d %B %Y',
		'month' => '%B %Y',
	];

	public $dateFormats = [
		'datetime' => 'd F Y à H:s',
		'date' => 'd F Y',
		'month' => 'F Y',
	];

	public static $translate = [
		'January' => 'janvier',
		'February' => 'février',
		'March' => 'mars',
		'April' => 'avril',
		'May' => 'mai',
		'June' => 'juin',
		'July' => 'juillet',
		'August' => 'août',
		'September' => 'septembre',
		'October' => 'octobre',
		'November' => 'novembre',
		'December' => 'décembre',
	];

	public static function toFrench(string $date, string $format): string
	{
		return \str_replace(
			\array_keys(self::$translate),
			\array_values(self::$translate),
			\date_create($date)->format($format)
		);
	}

	/**
	 * Formats the value as a date and time.
	 * @param mixed $value the value to be formatted
	 * @return string the formatted result
	 * @see datetimeFormat
	 */
	public function formatDatetime($value)
	{
		if (\strncmp($value, "0000-00-00", 10) === 0) {
			return "-";
		}
		return \date($this->datetimeFormat, $this->normalizeDateValue($value));
	}

	public function formatStrDate(string $value): string
	{
		$m = [];
		if (\strncmp($value, "0000-00-00", 10) === 0) {
			return "-";
		}
		if (preg_match('/^\d{4}-\d\d-\d\d \d\d:\d\d( |$)/', $value)) {
			return self::toFrench($value, $this->dateFormats['datetime']);
		}
		if (preg_match('/^(\d{4}-\d\d-\d\d)( |$)/', $value, $m)) {
			return self::toFrench($m[1], $this->dateFormats['date']);
		}
		if (preg_match('/^(\d{4}-\d\d)( |$)/', $value, $m)) {
			return self::toFrench($m[1] . '-01', $this->dateFormats['month']);
		}
		return $value;
	}

	/**
	 * Pretty-print URL by displaying latin characters instead of their URL-encoded equivalent ("é" for "%C3%A9")
	 *
	 * @param string $value
	 * @return string HTML
	 */
	public function formatUrl($value)
	{
		$conversions = [
			// extracted from http://www.utf8-chartable.de/
			'À' => '%C3%80',
			'Á' => '%C3%81',
			'Â' => '%C3%82',
			'Ã' => '%C3%83',
			'Ä' => '%C3%84',
			'Å' => '%C3%85',
			'Æ' => '%C3%86',
			'Ç' => '%C3%87',
			'È' => '%C3%88',
			'É' => '%C3%89',
			'Ê' => '%C3%8A',
			'Ë' => '%C3%8B',
			'Ì' => '%C3%8C',
			'Í' => '%C3%8D',
			'Î' => '%C3%8E',
			'Ï' => '%C3%8F',
			'Ð' => '%C3%90',
			'Ñ' => '%C3%91',
			'Ò' => '%C3%92',
			'Ó' => '%C3%93',
			'Ô' => '%C3%94',
			'Õ' => '%C3%95',
			'Ö' => '%C3%96',
			'×' => '%C3%97',
			'Ø' => '%C3%98',
			'Ù' => '%C3%99',
			'Ú' => '%C3%9A',
			'Û' => '%C3%9B',
			'Ü' => '%C3%9C',
			'Ý' => '%C3%9D',
			'Þ' => '%C3%9E',
			'ß' => '%C3%9F',
			'à' => '%C3%A0',
			'á' => '%C3%A1',
			'â' => '%C3%A2',
			'ã' => '%C3%A3',
			'ä' => '%C3%A4',
			'å' => '%C3%A5',
			'æ' => '%C3%A6',
			'ç' => '%C3%A7',
			'è' => '%C3%A8',
			'é' => '%C3%A9',
			'ê' => '%C3%AA',
			'ë' => '%C3%AB',
			'ì' => '%C3%AC',
			'í' => '%C3%AD',
			'î' => '%C3%AE',
			'ï' => '%C3%AF',
			'ð' => '%C3%B0',
			'ñ' => '%C3%B1',
			'ò' => '%C3%B2',
			'ó' => '%C3%B3',
			'ô' => '%C3%B4',
			'õ' => '%C3%B5',
			'ö' => '%C3%B6',
			'÷' => '%C3%B7',
			'ø' => '%C3%B8',
			'ù' => '%C3%B9',
			'ú' => '%C3%BA',
			'û' => '%C3%BB',
			'ü' => '%C3%BC',
			'ý' => '%C3%BD',
			'þ' => '%C3%BE',
			'ÿ' => '%C3%BF',
		];
		$printedUrl = \str_replace(array_values($conversions), array_keys($conversions), $value);

		$url = $value;
		if (\strpos($url, 'http://') !== 0 && \strpos($url, 'https://') !== 0) {
			$url = 'http://' . $url;
		}
		return \CHtml::link(\CHtml::encode($printedUrl), $url);
	}

	/**
	 * @param string $value
	 * @return string HTML
	 */
	public function formatEmails(string $value): string
	{
		$list = \array_filter(\array_map('trim', \explode("\n", $value)));
		return \join(" ", \array_map(['\CHtml', 'mailto'], $list));
	}

	public function formatDate($value)
	{
		if ($value === "" || (int) $value === 0) {
			return "";
		}
		return parent::formatDate($value);
	}
}
