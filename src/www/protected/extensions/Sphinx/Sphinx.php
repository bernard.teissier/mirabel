<?php

require __DIR__ . '/lib/sphinxapi.php';

/**
 * Provides an easier access to SphinxClient from a Yii application.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class Sphinx extends SphinxClient
{
	/** never return more results than this. */
	public const MAX_RESULTS = 500;

	/** string list of indexes. E.g. "main delta" or "that, thatstemmmed" or "*". */
	public $indexes = "*";

	/** string Prefix for the indexes */
	public $prefix = '';

	/** string Search query. */
	public $query = '';

	/** array Result of a query. */
	public $result;

	/** array default columns weights (can be changed with Sphinx::setFieldWeights(). */
	private $_defaultWeights = [];

	/**
	 * Constructor with optional array(host=>, port=>, prefix=>).
	 */
	public function __construct($config=[])
	{
		parent::__construct();
		$this->setConnectTimeout(1);
		$this->setArrayResult(false); // matches is an array: id => attributes
		if (isset($config['weights'])) {
			$this->_defaultWeights = $config['weights'];
		}
		if (!empty($this->_defaultWeights)) {
			$this->setFieldWeights($this->_defaultWeights);
		}
		if (isset($config['prefix'])) {
			$this->prefix = $config['prefix'];
		}
		if (isset($config['host'], $config['port'])) {
			$this->setServer($config['host'], $config['port']);
			$status = $this->status();
			if ($status === false) {
				Yii::log("Error: Could not connect to server", CLogger::LEVEL_ERROR, 'sphinx');
				throw new Exception("Could not connect to Sphinx server.");
			}
		}
	}

	/**
	 * Apply a CSort object to the next Sphinx searches.
	 *
	 * @param object|false $sort A CSort instance, or false.
	 */
	public function applySort($sort)
	{
		if ($this->_groupby) {
			return;
		}
		if ($sort !== false && $sort->getOrderBy()) {
			Yii::log("sort: '{$sort->orderBy}'", CLogger::LEVEL_TRACE, 'sphinx');
			$this->setSortMode(SPH_SORT_EXTENDED, $sort->orderBy);
		} else {
			$this->setSortMode(SPH_SORT_RELEVANCE);
		}
	}

	/**
	 * Apply a CPagination object to the next Sphinx searches.
	 *
	 * @param object|false $pagination A CPagination instance, or false.
	 */
	public function applyPagination($pagination)
	{
		if ($pagination === false) {
			$this->setLimits(0, 5000);
			return;
		}
		if (!empty($pagination->offset) and !empty($pagination->limit)) {
			$this->setLimits($pagination->offset, $pagination->limit, 5000);
		} else {
			$this->setLimits(0, $pagination->pageSize, 5000);
		}
	}

	/**
	 * Search using $this->query.
	 */
	public function fetchData()
	{
		Yii::log("Sphinx " . $this->getQueryLogMsg(), CLogger::LEVEL_INFO, 'sphinx');
		if (!isset($this->result)) {
			$this->result = $this->query($this->query, $this->getIndexes());
			if ($this->result === false) {
				Yii::log(
					"Error: " . $this->getLastError() . "\n'{$this->query}'",
					CLogger::LEVEL_ERROR,
					'sphinx'
				);
				throw new Exception(YII_DEBUG || defined('YII_TEST') ? $this->getLastError() : "Erreur lors de la recherche");
			}
		}
		return $this->result;
	}

	/**
	 * Validate $this->query.
	 */
	public function validateQuery()
	{
		$this->result = $this->query($this->query, $this->getIndexes());
		Yii::log(
			"query validated: '{$this->query}' " . ($this->result !== false ? 'YES' : 'NO'),
			CLogger::LEVEL_TRACE,
			'sphinx'
		);
		return ($this->result !== false);
	}

	/*
	 * $this->setGroupBy ( $groupby, SPH_GROUPBY_ATTR, $groupsort );
	 */

	/**
	 * Returns the index list, after applying the prefix.
	 *
	 * @return string
	 */
	public function getIndexes()
	{
		if ($this->indexes == '*') {
			return '*';
		}
		if ($this->prefix) {
			$list = preg_split('/[^\w-]+/', $this->indexes);
			foreach ($list as $k => $v) {
				$list[$k] = $this->prefix . $v;
			}
			return join(' ', $list);
		}
		return $this->indexes;
	}

	protected function getQueryLogMsg()
	{
		$filters = [];
		foreach ($this->_filters as $f) {
			if ($f['type'] === SPH_FILTER_VALUES) {
				$filters[] = ($f['exclude'] ? '-' : '')
					. $f['attr'] . '=' . join(',', $f['values']);
			} else {
				$filters[] = ($f['exclude'] ? '-' : '')
					. $f['attr'] . '=[' . $f['min'] . '-' . $f['min'] . ']';
			}
		}
		return "({$this->indexes})"
			. " QUERY: '" . $this->query
			. "' FILTERS: " . join(' + ', $filters);
	}
}
