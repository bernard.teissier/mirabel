<?php
/**
 * SphinxDataProvider implements a data provider based on Sphinx.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class SphinxDataProvider extends CDataProvider
{
	/**
	 * @var object A Sphinx instance.
	 */
	public $sphinxClient;

	/**
	 * @var string the primary ActiveRecord class name.
	 * The {@link getData()} method will return a list of objects of this class. Each object
	 *    will have a "metadata" attribute (assoc array) with metadata from the search.
	 * If null, then the raw results will be returned.
	 */
	public $modelClass = null;

	public $mapAttributes = [];

	/** @var CBdCriteria|array Used to filter the models, when modelClass is given */
	protected $criteria;

	/** @var array Result derived from the result of SphinxClient */
	protected $result = null;

	/**
	 * Constructor.
	 * @param object $sphinxClient A Sphinx instance.
	 * @param string $modelClass the model class. This will be assigned to the {@link modelClass} property.
	 * @param array configuration (name=>value) to be applied to this data provider.
	 * Any public properties of the data provider can be configured via this parameter
	 */
	public function __construct($sphinxClient, $modelClass=null, $config=[])
	{
		$this->modelClass = $modelClass;
		$this->sphinxClient = $sphinxClient;
		$this->setId('SphinxData' . $this->modelClass);
		foreach ($config as $key => $value) {
			$this->{$key} = $value;
		}
	}

	/**
	 * @return CDbCriteria the query criteria
	 */
	public function getCriteria()
	{
		if ($this->criteria === null) {
			$this->criteria = new CDbCriteria;
		}
		return $this->criteria;
	}

	/**
	 * @param mixed the query criteria. This can be either a CDbCriteria object or an array
	 *        representing the query criteria.
	 */
	public function setCriteria($value)
	{
		$this->criteria = $value instanceof CDbCriteria ? $value : new CDbCriteria($value);
		$this->result = null;
	}

	/**
	 * Returns the number of data items in the current page.
	 * This is equivalent to <code>count($provider->getData())</code>.
	 * When {@link pagination} is set false, this returns the same value as {@link totalItemCount}.
	 *
	 * @param bool whether the number of data items should be re-calculated.
	 * @return int the number of data items in the current page.
	 */
	public function getItemCount($refresh=false)
	{
		if ($refresh || null === $this->result) {
			$this->fetchData();
		}
		if ($this->sphinxClient->result['total_found'] == 0 || empty($this->sphinxClient->result['matches'])) {
			return 0;
		}
		return count($this->sphinxClient->result['matches']);
	}

	/**
	 * Returns the total number of data items (not only on this page).
	 *
	 * @return int the total number of data items.
	 */
	public function getTotalItemCount($refresh=false)
	{
		if ($refresh || null === $this->result) {
			$this->fetchData();
		}
		return (int) $this->sphinxClient->result['total_found'];
	}

	/**
	 * Fetches the data using the Sphinx client.
	 * @return array list of data items
	 */
	protected function fetchData($cache=true)
	{
		if ($cache && $this->result) {
			return $this->result;
		}

		$pagination = $this->getPagination();
		// Warning: in Yii 1.1.1 CPagination::getPageCount() needs to know the item count
		if ($pagination !== false) {
			$pagination->setItemCount(1000000); // The right value will be set later
			if (property_exists($pagination, 'pageVar')) {
				$pagination->pageVar = 'page';
			}
		}
		$this->sphinxClient->applyPagination($pagination);

		$sort = $this->getSort();
		if ($sort->sortVar !== $this->getId() . '_sort') {
			$this->sphinxClient->applySort($this->getSort());
		}
		$this->sphinxClient->fetchData();

		if ($pagination !== false) {
			$pagination->setItemCount((int) $this->sphinxClient->result['total_found']);
		}
		if ($this->sphinxClient->result['total_found'] == 0
			|| !isset($this->sphinxClient->result['matches'])) {
			$this->result = [];
		} elseif (isset($this->modelClass)) {
			$this->result = $this->_sphinxToAr($this->sphinxClient->result['matches']);
		} else {
			$this->result = $this->sphinxClient->result['matches'];
			foreach (array_keys($this->result) as $id) {
				$this->result[$id]['attrs']['id'] = $id;
			}
		}
		return array_values($this->result);
	}

	/**
	 * Fetches the data item keys from the persistent data storage.
	 * @return array list of data item keys.
	 */
	protected function fetchKeys()
	{
		$keys = [];
		foreach (array_keys($this->getData()) as $i) {
			$keys[] = $i;
		}
		return $keys;
	}

	/**
	 * Calculates the total number of data items.
	 * @return int the total number of data items.
	 */
	protected function calculateTotalItemCount()
	{
		return (int) $this->sphinxClient->result['total_found'];
	}

	/**
	 * Fetch the DB records from a Sphinx result.
	 *
	 * @param array $matches Part of the Sphinx result.
	 * @return array List of CActiveRecord instances.
	 */
	private function _sphinxToAr($matches)
	{
		if (empty($matches)) {
			return [];
		}
		$records = [];
		if ($this->mapAttributes) {
			$mapAttributes = $this->mapAttributes;
		} else {
			$modelAttributes = (new $this->modelClass)->attributeNames();
			$mapAttributes = array_combine($modelAttributes, $modelAttributes);
		}
		foreach ($matches as $id => $match) {
			$attributes = [];
			foreach ($mapAttributes as $attrName => $sphxName) {
				if ($attrName === 'id') {
					$attributes['id'] = $id;
				} elseif (isset($match['attrs'][$sphxName])) {
					$attributes[$attrName] = $match['attrs'][$sphxName];
				}
			}
			$record = new $this->modelClass;
			$record->setAttributes($attributes, false);
			$record->metadata = $match;
			$records[] = $record;
		}

		return $records;
	}
}
