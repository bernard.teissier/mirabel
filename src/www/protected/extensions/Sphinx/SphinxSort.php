<?php
/**
 * SphinxSort class file.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */

/**
 * SphinxSort extends CSort for Sphinx.
 *
 * CSort is in fact dependent on a AR model (CActiveSort?).
 * This class replaces it with a mapping of attributes to Sphinx.
 */
class SphinxSort extends CSort
{
	public $separators = ['-', ':'];

	/**
	 * Constructor.
	 *
	 * Only columns whose value is in the attributes' keys will be sortable.
	 *
	 * @param array $attributes Mapping of sorting attributes: AR name => Sphinx name.
	 */
	public function __construct($attributes=[])
	{
		$this->attributes = $attributes;
		$this->attributes['@rank'] = '@rank';
	}

	public function getDirections()
	{
		$directions = parent::getDirections();
		if (empty($directions) && is_array($this->defaultOrder)) {
			$directions = $this->defaultOrder;
		}
		return $directions;
	}

	/**
	 * Returns an order for a sort type of SPH_SORT_EXTENDED.
	 *
	 * @return string the order-by columns represented by this sort object.
	 */
	public function getOrderBy($criteria = null)
	{
		$directions = $this->getDirections();
		if (empty($directions)) {
			return (is_string($this->defaultOrder) ? $this->defaultOrder : '');
		}
		$orders = [];
		foreach ($directions as $attribute => $descending) {
			$attr = $this->resolveAttribute($attribute);
			if (!$attribute) {
				Yii::log("Sphinx ERROR: unknown attribute '$attr'", 'error', 'sphinx');
				continue;
			}
			if (is_array($attr)) {
				$orders[] = $attr[$descending ? 'desc' : 'asc'];
			} else {
				$orders[] = $attr . ($descending ? ' DESC' : ' ASC');
			}
		}
		return implode(', ', $orders);
	}

	/**
	 * Returns the real definition of an attribute given its name.
	 * @param string the attribute name that the user requests to sort on
	 * @return mixed the attribute name or the virtual attribute definition. False if the attribute cannot be sorted.
	 */
	public function resolveAttribute($attribute)
	{
		if (isset($this->attributes[$attribute])) {
			return $this->attributes[$attribute];
		}
		return false;
	}

	/**
	 * Returns the link that will enable a sort by an attribute.
	 *
	 * @param string $attribute
	 * @param string $label
	 * @param array $htmlOptions
	 * @return string HTML link.
	 */
	public function link($attribute, $label=null, $htmlOptions=[])
	{
		if ($label === null) {
			$label = $this->resolveLabel($attribute);
		}
		if (($definition = $this->resolveAttribute($attribute)) === false) {
			return $label;
		}
		$directions = $this->getDirections();
		if (is_array($directions) && !empty($directions)) {
			$dirAttr = array_key_first($directions);
			$dirAsc = $directions[$dirAttr]; // only for the first direction
		}
		if (isset($dirAttr) && $dirAttr == $attribute) {
			$class = $dirAsc ? 'desc' : 'asc';
			if (isset($htmlOptions['class'])) {
				$htmlOptions['class'].=' ' . $class;
			} else {
				$htmlOptions['class'] = $class;
			}
			$descending = !$dirAsc;
			unset($directions[$attribute]);
		} elseif (is_array($definition) && isset($definition['default'])) {
			$descending = $definition['default'] === 'desc';
		} else {
			$descending = false;
		}

		if ($this->multiSort) {
			$directions = array_merge([$attribute => $descending], $directions);
		} else {
			$directions = [$attribute => $descending];
		}

		$url = $this->createUrl(Yii::app()->getController(), $directions);

		return $this->createLink($attribute, $label, $url, $htmlOptions);
	}
}
