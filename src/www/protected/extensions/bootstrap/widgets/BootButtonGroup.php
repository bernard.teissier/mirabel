<?php
/**
 * BootButtonGroup class file.
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright Copyright &copy; Christoffer Niska 2011-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package bootstrap.widgets
 * @since 0.9.10
 */

Yii::import('bootstrap.widgets.BootButton');

/**
 * Bootstrap button group widget.
 */
class BootButtonGroup extends CWidget
{
	// Toggle options.
	public const TOGGLE_CHECKBOX = 'checkbox';

	public const TOGGLE_RADIO = 'radio';

	/**
	 * @var string the button callback type.
	 * @see BootButton::$buttonType
	 */
	public $buttonType = BootButton::BUTTON_LINK;

	/**
	 * @var string the button type.
	 * @see BootButton::$type
	 */
	public $type = BootButton::TYPE_NORMAL;

	/**
	 * @var string the button size.
	 * @see BootButton::$size
	 */
	public $size = BootButton::SIZE_NORMAL;

	/**
	 * @var bool indicates whether to encode the button labels.
	 */
	public $encodeLabel = true;

	/**
	 * @var array the HTML attributes for the widget container.
	 */
	public $htmlOptions = [];

	/**
	 * @var array the button configuration.
	 */
	public $buttons = [];

	/**
	 * @var bool indicates whether to enable button toggling.
	 */
	public $toggle;

	/**
	 * Initializes the widget.
	 */
	public function init()
	{
		$classes = 'btn-group';
		if (isset($this->htmlOptions['class'])) {
			$this->htmlOptions['class'] .= ' ' . $classes;
		} else {
			$this->htmlOptions['class'] = $classes;
		}

		$validToggles = [self::TOGGLE_CHECKBOX, self::TOGGLE_RADIO];

		if (isset($this->toggle) && in_array($this->toggle, $validToggles)) {
			$this->htmlOptions['data-toggle'] = 'buttons-' . $this->toggle;
		}
	}

	/**
	 * Runs the widget.
	 */
	public function run()
	{
		echo CHtml::openTag('div', $this->htmlOptions);

		foreach ($this->buttons as $button) {
			$this->controller->widget('bootstrap.widgets.BootButton', [
				'buttonType'=>$button['buttonType'] ?? $this->buttonType,
				'type'=>$button['type'] ?? $this->type,
				'size'=>$this->size,
				'icon'=>$button['icon'] ?? null,
				'label'=>$button['label'] ?? null,
				'url'=>$button['url'] ?? null,
				'active'=>$button['active'] ?? false,
				'items'=>$button['items'] ?? [],
				'ajaxOptions'=>$button['ajaxOptions'] ?? [],
				'htmlOptions'=>$button['htmlOptions'] ?? [],
				'encodeLabel'=>$button['encodeLabel'] ?? $this->encodeLabel,
			]);
		}

		echo '</div>';
	}
}
