<?php
/**
 * BootButtonColumn class file.
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright  Copyright &copy; Christoffer Niska 2011-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package bootstrap.widgets
 * @since 0.9.8
 */

Yii::import('zii.widgets.grid.CButtonColumn');

/**
 * Bootstrap button column widget.
 * Used to set buttons to use Glyphicons instead of the defaults images.
 */
class BootButtonColumn extends CButtonColumn
{
	/**
	 * @var string the view button icon (defaults to 'eye-open').
	 */
	public $viewButtonIcon = 'eye-open';

	/**
	 * @var string the update button icon (defaults to 'pencil').
	 */
	public $updateButtonIcon = 'pencil';

	/**
	 * @var string the delete button icon (defaults to 'trash').
	 */
	public $deleteButtonIcon = 'trash';

	/**
	 * Initializes the default buttons (view, update and delete).
	 */
	protected function initDefaultButtons()
	{
		parent::initDefaultButtons();

		if ($this->viewButtonIcon !== false && !isset($this->buttons['view']['icon'])) {
			$this->buttons['view']['icon'] = $this->viewButtonIcon;
		}
		if ($this->updateButtonIcon !== false && !isset($this->buttons['update']['icon'])) {
			$this->buttons['update']['icon'] = $this->updateButtonIcon;
		}
		if ($this->deleteButtonIcon !== false && !isset($this->buttons['delete']['icon'])) {
			$this->buttons['delete']['icon'] = $this->deleteButtonIcon;
		}

		if (isset($this->buttons['delete'])) {
			$this->buttons['delete']['method'] = 'post';
		}

		if (isset($this->buttons['delete']['click'])) {
			unset($this->buttons['delete']['click']);
		}
	}

	/**
	 * Registers the client scripts for the button column.
	 */
	protected function registerClientScript()
	{
		$js=[];
		foreach ($this->buttons as $button) {
			if (isset($button['click'])) {
				$function=CJavaScript::encode($button['click']);
				$class=preg_replace('/\s+/', '.', $button['options']['class']);
				$js[]="jQuery(document).on('click','#{$this->grid->id} td .{$class}',$function);";
			}
		}

		if (is_string($this->deleteConfirmation)) {
			$js[] = "jQuery('#{$this->grid->id}').on('click', '.delete', function(e) { return confirm(" . CJavaScript::encode($this->deleteConfirmation) . "); });";
		}

		if ($js!==[]) {
			Yii::app()->getClientScript()->registerScript(__CLASS__ . '#' . $this->id, implode("\n", $js));
		}
	}

	/**
	 * Renders a link button.
	 * @param string $id the ID of the button
	 * @param array $button the button configuration which may contain 'label', 'url', 'imageUrl' and 'options' elements.
	 * @param int $row the row number (zero-based)
	 * @param mixed $data the data object associated with the row
	 */
	protected function renderButton($id, $button, $row, $data)
	{
		if (isset($button['visible']) && !$this->evaluateExpression($button['visible'], ['row'=>$row, 'data'=>$data])) {
			return;
		}
		$label = $button['label'] ?? $id;
		$url = isset($button['url']) ? $this->evaluateExpression($button['url'], ['data'=>$data, 'row'=>$row]) : '#';
		$options = $button['options'] ?? [];

		if (!isset($options['title'])) {
			$options['title'] = $label;
		}

		if (!isset($options['rel'])) {
			$options['rel'] = 'tooltip';
		}

		if (isset($options['class'])) {
			$options['class'] .= " " . $id;
		} else {
			$options['class'] = $id;
		}

		if (isset($button['method']) && strcasecmp($button['method'], 'get') !== 0) {
			$options['class'] .= " btn btn-link";
			$options['type'] = "submit";
			echo "<form></form>" . CHtml::form($url, $button['method'], ['style' => 'display:inline;']);
			if (isset($button['icon'])) {
				if (strpos($button['icon'], 'icon') === false) {
					$button['icon'] = 'icon-' . implode(' icon-', explode(' ', $button['icon']));
				}
				echo CHtml::htmlButton('<i class="' . $button['icon'] . '"></i>', $options);
			} elseif (isset($button['imageUrl']) && is_string($button['imageUrl'])) {
				echo CHtml::htmlButton(CHtml::image($button['imageUrl'], $label), $options);
			} else {
				echo CHtml::htmlButton($label, $options);
			}
			echo CHtml::endForm();
		} else {
			if (isset($button['icon'])) {
				if (strpos($button['icon'], 'icon') === false) {
					$button['icon'] = 'icon-' . implode(' icon-', explode(' ', $button['icon']));
				}
				echo CHtml::link('<i class="' . $button['icon'] . '"></i>', $url, $options);
			} elseif (isset($button['imageUrl']) && is_string($button['imageUrl'])) {
				echo CHtml::link(CHtml::image($button['imageUrl'], $label), $url, $options);
			} else {
				echo CHtml::link($label, $url, $options);
			}
		}
	}
}
