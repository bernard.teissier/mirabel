<?php
/**
 * BootAlert class file.
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright  Copyright &copy; Christoffer Niska 2011-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package bootstrap.widgets
 */

/**
 * Bootstrap alert widget.
 */
class BootAlert extends CWidget
{
	/**
	 * @var array the keys for which to get flash messages.
	 */
	public $keys = ['success', 'info', 'warning', 'error', /* or */'danger'];

	/**
	 * @var string the template to use for displaying flash messages.
	 */
	public $template = '<div class="alert alert-block alert-{key}{class}"><a class="close" data-dismiss="alert">&times;</a>{message}</div>';

	/**
	 * @var string[] the JavaScript event handlers.
	 */
	public $events = [];

	/**
	 * @var array the HTML attributes for the widget container.
	 */
	public $htmlOptions = [];

	/**
	 * Initializes the widget.
	 */
	public function init()
	{
		parent::init();

		if (!isset($this->htmlOptions['id'])) {
			$this->htmlOptions['id'] = $this->getId();
		}
	}

	/**
	 * Runs the widget.
	 */
	public function run()
	{
		$id = $this->id;

		if (is_string($this->keys)) {
			$this->keys = [$this->keys];
		}

		echo CHtml::openTag('div', $this->htmlOptions);

		foreach (Yii::app()->user->getFlashes() as $key => $message) {
			if (in_array(strtok($key, " "), $this->keys)) {
				echo strtr($this->template, [
					'{class}'=>' fade in',
					'{key}'=>$key,
					'{message}'=>$message,
				]);
			}
		}

		echo '</div>';

		$selector = "#{$id} .alert";
		$id .= '_' . uniqid(true, true);

		/** @var CClientScript $cs */
		$cs = Yii::app()->getClientScript();
		$cs->registerScript(__CLASS__ . '#' . $id, "jQuery('{$selector}').alert();");

		foreach ($this->events as $name => $handler) {
			$handler = CJavaScript::encode($handler);
			$cs->registerScript(__CLASS__ . '#' . $id . '_' . $name, "jQuery('{$selector}').on('" . $name . "', {$handler});");
		}
	}
}
