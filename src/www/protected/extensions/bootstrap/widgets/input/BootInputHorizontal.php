<?php
/**
 * BootInputHorizontal class file.
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright Copyright &copy; Christoffer Niska 2011-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package bootstrap.widgets.input
 */

Yii::import('bootstrap.widgets.input.BootInput');

/**
 * Bootstrap horizontal form input widget.
 * @since 0.9.8
 */
class BootInputHorizontal extends BootInput
{
	protected $hint = '';

	/**
	 * Runs the widget.
	 */
	public function run()
	{
		$this->hint = $this->getHint();
		echo CHtml::openTag('div', ['class'=>'control-group ' . $this->getContainerCssClass()]);
		parent::run();
		echo '</div>';
	}

	/**
	 * Returns the label for this block.
	 * @param array $htmlOptions additional HTML attributes
	 * @return string the label
	 */
	protected function getLabel($htmlOptions = [])
	{
		if (!isset($this->htmlOptions['labelOptions'])) {
			$this->htmlOptions['labelOptions'] = [];
		}

		if (isset($this->htmlOptions['labelOptions']['class'])) {
			$this->htmlOptions['labelOptions']['class'] .= ' control-label';
		} else {
			$this->htmlOptions['labelOptions']['class'] = 'control-label';
		}

		return parent::getLabel();
	}

	/**
	 * Renders a checkbox.
	 * @return string the rendered content
	 */
	protected function checkBox()
	{
		$attribute = $this->attribute;
		$htmlOptions = $this->htmlOptions;
		if (isset($this->htmlOptions['label'])) {
			$label = $this->htmlOptions['label'];
			unset($this->htmlOptions['label']);
		} else {
			$label = $this->model->getAttributeLabel($attribute);
		}

		CHtml::resolveNameID($this->model, $attribute, $htmlOptions);
		if (isset($htmlOptions['label-position']) && $htmlOptions['label-position'] == 'left') {
			unset($htmlOptions['label-position']);
			echo '<label class="control-label" for="' . $this->getAttributeId($this->attribute) . '">';
			echo $label;
			echo "</label>";
			echo '<div class="controls">';
			echo $this->form->checkBox($this->model, $attribute, $htmlOptions) . PHP_EOL;
		} else {
			echo '<div class="controls">';
			echo '<label class="checkbox" for="' . $this->getAttributeId($this->attribute, $this->htmlOptions) . '">';
			echo $this->form->checkBox($this->model, $this->attribute, $htmlOptions) . PHP_EOL;
			echo $label;
		}
		echo $this->getError() . $this->hint;
		echo '</div>';
	}

	/**
	 * Renders a list of checkboxes.
	 * @return string the rendered content
	 */
	protected function checkBoxList()
	{
		echo $this->getLabel();
		echo '<div class="controls">';
		echo $this->form->checkBoxList($this->model, $this->attribute, $this->data, $this->htmlOptions);
		echo $this->getError() . $this->hint;
		echo '</div>';
	}

	/**
	 * Renders a list of inline checkboxes.
	 * @return string the rendered content
	 */
	protected function checkBoxListInline()
	{
		$this->htmlOptions['inline'] = true;
		$this->checkBoxList();
	}

	/**
	 * Renders a drop down list (select).
	 * @return string the rendered content
	 */
	protected function dropDownList()
	{
		echo $this->getLabel();
		echo '<div class="controls">';
		echo $this->form->dropDownList($this->model, $this->attribute, $this->data, $this->htmlOptions);
		echo $this->getError() . $this->hint;
		echo '</div>';
	}

	/**
	 * Renders a file field.
	 * @return string the rendered content
	 */
	protected function fileField()
	{
		echo $this->getLabel();
		echo '<div class="controls">';
		$error = $this->getError();
		echo $this->form->fileField($this->model, $this->attribute, $this->htmlOptions);
		echo $error . $this->hint;
		echo '</div>';
	}

	/**
	 * Renders a password field.
	 * @return string the rendered content
	 */
	protected function passwordField()
	{
		echo $this->getLabel();
		echo '<div class="controls">';
		echo $this->getPrepend();
		$append = $this->getAppend();
		echo $this->form->passwordField($this->model, $this->attribute, $this->htmlOptions);
		echo $append;
		echo $this->getError() . $this->hint;
		echo '</div>';
	}

	/**
	 * Renders a radio button.
	 * @return string the rendered content
	 */
	protected function radioButton()
	{
		$attribute = $this->attribute;
		echo '<div class="controls">';
		echo '<label class="radio" for="' . $this->getAttributeId($attribute) . '">';
		echo $this->form->radioButton($this->model, $attribute, $this->htmlOptions) . PHP_EOL;
		echo $this->model->getAttributeLabel($attribute);
		echo $this->getError() . $this->hint;
		echo '</label></div>';
	}

	/**
	 * Renders a list of radio buttons.
	 * @return string the rendered content
	 */
	protected function radioButtonList()
	{
		echo $this->getLabel();
		echo '<div class="controls">';
		echo $this->form->radioButtonList($this->model, $this->attribute, $this->data, $this->htmlOptions);
		echo $this->getError() . $this->hint;
		echo '</div>';
	}

	/**
	 * Renders a list of inline radio buttons.
	 * @return string the rendered content
	 */
	protected function radioButtonListInline()
	{
		$this->htmlOptions['inline'] = true;
		$this->radioButtonList();
	}

	/**
	 * Renders a textarea.
	 * @return string the rendered content
	 */
	protected function textArea()
	{
		echo $this->getLabel();
		echo '<div class="controls">';
		echo $this->form->textArea($this->model, $this->attribute, $this->htmlOptions);
		echo $this->getError() . $this->hint;
		echo '</div>';
	}

	/**
	 * Renders a text field.
	 * @return string the rendered content
	 */
	protected function textField()
	{
		echo $this->getLabel();
		echo '<div class="controls">';
		echo $this->getPrepend();
		$append = $this->getAppend();
		echo $this->form->textField($this->model, $this->attribute, $this->htmlOptions);
		echo $append;
		echo $this->getError() . $this->hint;
		echo '</div>';
	}

	/**
	 * Renders a CAPTCHA.
	 * @return string the rendered content
	 */
	protected function captcha()
	{
		echo $this->getLabel();
		echo '<div class="controls"><div class="captcha">';
		echo '<div class="widget">' . $this->widget('CCaptcha', $this->getCaptchaOptions(), true) . '</div>';
		echo $this->form->textField($this->model, $this->attribute, $this->htmlOptions);
		echo $this->getError() . $this->hint;
		echo '</div></div>';
	}

	/**
	 * Renders an uneditable field.
	 * @return string the rendered content
	 */
	protected function uneditableField()
	{
		echo $this->getLabel();
		echo '<div class="controls">';
		echo CHtml::tag('span', $this->htmlOptions, $this->model->{$this->attribute});
		echo $this->getError() . $this->hint;
		echo '</div>';
	}

	/**
	 * Renders a CJuiAutoComplete field.
	 * @return string the rendered content
	 */
	protected function autoCompleteField()
	{
		echo $this->getLabel();
		echo '<div class="controls">';
		echo $this->getPrepend();
		$append = $this->getAppend();
		$source = $this->data['source'];
		unset($this->data['source']);
		Yii::app()->getController()->widget(
			'zii.widgets.jui.CJuiAutoComplete',
			[
				'name' => Chtml::resolveName($this->model, $this->attribute),
				'value' => $this->model->{$this->attribute},
				'source' => $source,
				// cf http://api.jqueryui.com/autocomplete/
				'options' => $this->data,
			]
		);
		echo $append;
		echo $this->getError() . $this->hint;
		echo '</div>';
	}

	/**
	 * Renders a free field.
	 * @return string the rendered content
	 */
	protected function freeField()
	{
		echo $this->getLabel();
		echo '<div class="controls">';
		echo CHtml::tag('span', $this->htmlOptions, $this->value);
		echo $this->getError() . $this->hint;
		echo '</div>';
	}
}
