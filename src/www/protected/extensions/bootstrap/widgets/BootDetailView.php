<?php

/**
 * BootDetailView class file.
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright Copyright &copy; Christoffer Niska 2011-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package bootstrap.widgets
 */
Yii::import('zii.widgets.CDetailView');

/**
 * Bootstrap detail view widget.
 * Used for setting default HTML classes and disabling the default CSS.
 */
class BootDetailView extends CDetailView
{
	// Table types.
	public const TYPE_PLAIN = '';

	public const TYPE_STRIPED = 'striped';

	public const TYPE_BORDERED = 'bordered';

	public const TYPE_CONDENSED = 'condensed';

	/**
	 * @var string|array the table type.
	 * Valid values are '', 'striped', 'bordered' and/or 'condensed'.
	 */
	public $type = [self::TYPE_STRIPED, self::TYPE_CONDENSED];

	/**
	 * @var string the URL of the CSS file used by this detail view.
	 * Defaults to false, meaning that no CSS will be included.
	 */
	public $cssFile = false;

	/**
	 * @var bool Line where the value is empy won't be displayed.
	 */
	public $hideEmptyLines = false;

	/**
	 * Initializes the widget.
	 */
	public function init()
	{
		parent::init();

		$classes = ['table'];

		if (is_string($this->type)) {
			$this->type = explode(' ', $this->type);
		}

		$validTypes = [self::TYPE_STRIPED, self::TYPE_BORDERED, self::TYPE_CONDENSED];

		foreach ($this->type as $type) {
			if (in_array($type, $validTypes)) {
				$classes[] = 'table-' . $type;
			}
		}

		$classesStr = implode(' ', $classes);
		if (isset($this->htmlOptions['class'])) {
			$this->htmlOptions['class'] .= ' ' . $classesStr;
		} else {
			$this->htmlOptions['class'] = $classesStr;
		}
	}

	/**
	 * Renders the detail view.
	 * This is the main entry of the whole detail view rendering.
	 */
	public function run()
	{
		foreach ($this->attributes as $k => $attribute) {
			if (is_array($attribute) && isset($attribute['value']) && is_object($attribute['value']) && ($attribute['value'] instanceof \Closure)
			) {
				$this->attributes[$k]['value'] = call_user_func_array($attribute['value'], [$this->data]);
			}
		}
		if ($this->hideEmptyLines) {
			$matches = [];
			foreach ($this->attributes as $k => $attribute) {
				if (is_string($attribute)) {
					if (!preg_match('/^([\w.]+)(:(\w*))?(:(.*))?$/', $attribute, $matches)) {
						throw new CException(Yii::t('zii', 'The attribute must be specified in the format of "Name:Type:Label", where "Type" and "Label" are optional.'));
					}
					$this->attributes[$k] = [
						'name' => $matches[1],
						'type' => $matches[3] ?? 'text',
					];
					if (isset($matches[5])) {
						$this->attributes[$k]['label'] = $matches[5];
					}
				}
				if (isset($this->attributes[$k]['visible'])) {
					continue;
				}
				if (isset($this->attributes[$k]['value'])) {
					$value = $this->attributes[$k]['value'];
				} elseif (isset($this->attributes[$k]['name'])) {
					$value = (string) CHtml::value($this->data, $this->attributes[$k]['name']);
				} else {
					$value = null;
				}
				$this->attributes[$k]['visible'] = !empty($value);
			}
		}
		parent::run();
	}
}
