<?php

/**
 * In rules():
 * ['dateStart, dateEnd', 'ext.validators.DateVersatileValidator'],
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class DateVersatileValidator extends CValidator
{
	public $onlyFullDate = false;

	public $nothingAfter = false;

	public $padding = false;

	public $modernDate = 1650;

	public $allowempty = true;

	/**
	 * Validates the attribute of the object.
	 *
	 * If there is any error, the error message is added to the object.
	 *
	 * @param CModel $object the object being validated.
	 * @param string $attr the attribute being validated.
	 */
	protected function validateAttribute($object, $attr)
	{
		if ($object->{$attr} === null || trim($object->{$attr}) === '' || $object->{$attr} === '000-00-00') {
			if (!$this->allowempty) {
				$object->addError($attr, "Cette date ne peut être vide.");
			}
			if ($object->{$attr} !== null) {
				$object->{$attr} = '';
			}
			return;
		}
		$value = str_replace(
			['/', "\xFE\xFF", "\xEF\xBB\xBF"],
			['-', '', ''],
			trim($object->{$attr}) . ' '
		);
		if (preg_match('/^(\d{4})(-\d\d)?(-\d\d)?\s+(.*?)$/', $value, $match)) {
			// ISO
			list(, $year, $month, $day, $comment) = $match;
		} elseif (preg_match('/^(\d\d-)?(\d\d-)?(\d{4})\s+(.*?)$/', $value, $match)) {
			// European
			list(, $day, $month, $year, $comment) = $match;
			if (empty($month)) {
				$month = $day;
				$day = '';
			}
		} else {
			$object->addError($attr, "Format de date non valide (YYYY-mm-dd ou dd/mm/YYYY) dans '{$value}'.");
			return;
		}
		$month = trim($month, '-');
		$day = trim($day, '-');
		if (!checkdate($month ?: 1, $day ?: 1, $year)) {
			$object->addError($attr, "Date non valide mais de format correct (il n'y a pas de 35 mars, etc).");
			return;
		}
		if ($this->onlyFullDate and (!$day or !$month)) {
			$object->addError($attr, 'La date est incomplète (format attendu, YYYY-mm-dd ou dd/mm/YYYY).');
			return;
		}
		if ($this->modernDate && $year < $this->modernDate) {
			$object->addError($attr, "La date est trop ancienne, elle doit être postérieure à {$this->modernDate}.");
			return;
		}
		if ($this->nothingAfter and $comment) {
			$object->addError($attr, 'La date est suivie de texte excédentaire.');
			return;
		}
		if ($this->padding) {
			$object->{$attr} = trim(sprintf('%4d-%02d-%02d %s', $year, $month, $day, $comment));
		} else {
			if (empty($month)) {
				$object->{$attr} = trim(sprintf('%4d %s', $year, $comment));
			} elseif (empty($day)) {
				$object->{$attr} = trim(sprintf('%4d-%02d %s', $year, $month, $comment));
			} else {
				$object->{$attr} = trim(sprintf('%4d-%02d-%02d %s', $year, $month, $day, $comment));
			}
		}
	}
}
