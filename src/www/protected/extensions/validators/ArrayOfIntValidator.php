<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class ArrayOfIntValidator extends CValidator
{
	/**
	 * @var bool whether the attribute value can be null or empty. Defaults to true,
	 * meaning that if the attribute is empty, it is considered valid.
	 */
	public $allowEmpty = true;

	/**
	 * @var bool
	 */
	public $allowNegative = false;

	/**
	 * Validates the attribute of the object.
	 *
	 * If there is any error, the error message is added to the object.
	 *
	 * @param CModel $object the object being validated.
	 * @param string $attr the attribute being validated.
	 */
	protected function validateAttribute($object, $attr)
	{
		if ($this->allowEmpty && $this->isEmpty($object->{$attr})) {
			if (null !== $object->{$attr}) {
				$object->{$attr} = [];
			}
			return;
		}
		if (!is_array($object->{$attr})) {
			if ($object->{$attr} === '') {
				$object->{$attr} = [];
				return;
			}
			if (is_string($object->{$attr})) {
				$object->{$attr} = explode(',', $object->{$attr});
			} else {
				$object->addError($attr, "Should be an array.");
				return;
			}
		}
		$object->{$attr} = array_filter(
			$object->{$attr},
			function ($x) {
				return $x !== '';
			}
		);
		foreach ($object->{$attr} as $k => $v) {
			if (is_int($v) || ctype_digit($v) || ($this->allowNegative && preg_match('/^-?\d+\s*$/', $v))) {
				$object->{$attr}[$k] = (int) $v;
			} else {
				$object->addError($attr, "Ce champ contient une valeur non-valide.");
				return;
			}
		}
	}
}
