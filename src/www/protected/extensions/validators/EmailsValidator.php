<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class EmailsValidator extends CValidator
{
	/**
	 * @var bool whether the attribute value can be null or empty. Defaults to true,
	 * meaning that if the attribute is empty, it is considered valid.
	 */
	public $allowEmpty = true;

	/**
	 * Validates the attribute of the object.
	 *
	 * If there is any error, the error message is added to the object.
	 *
	 * @param CModel $object the object being validated.
	 * @param string $attr the attribute being validated.
	 */
	protected function validateAttribute($object, $attr)
	{
		if ($this->allowEmpty && $this->isEmpty($object->{$attr})) {
			if (null !== $object->{$attr}) {
				$object->{$attr} = "";
			}
			return;
		}
		$emailValidator = new CEmailValidator();
		$emails = array_filter(array_map('trim', explode("\n", $object->{$attr})));
		foreach ($emails as $e) {
			if (!$emailValidator->validateValue($e)) {
				$object->addError($attr, "L'email $e n'est pas valide.");
			}
		}
	}
}
