<?php

/**
 * Validates an URL by fetching its content.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class UrlFetchableValidator extends CValidator
{
	public const DISABLED_IF_REQUEST_CONTAINS = 'nourlcheck';

	/**
	 * @var bool whether the attribute value can be null or empty. Defaults to true,
	 * meaning that if the attribute is empty, it is considered valid.
	 */
	public $allowEmpty = true;

	/**
	 * @var bool disable this validator during unit tests
	 */
	public $disabledInUnitTests = true;

	public $lastError = '';

	public function checkLink($url)
	{
		if (YII_ENV === 'test') {
			return true;
		}
		if (PHP_SAPI !== 'cli' && !Yii::app()->user->isGuest && !empty($_REQUEST[self::DISABLED_IF_REQUEST_CONTAINS])) {
			return true;
		}

		$allowedResults = [
			LinkChecker::RESULT_SUCCESS, LinkChecker::RESULT_IGNORED, LinkChecker::RESULT_EMPTY,
		];
		$checker = new LinkChecker();
		$checker->setTimeout(30);
		list($success, $this->lastError) = $checker->checkLink($url);
		return in_array($success, $allowedResults, true);
	}

	/**
	 * @param CModel $object the object being validated.
	 * @param bool $force (opt, false)
	 * @return string HTML
	 */
	public static function toggleUrlChecker($object, $force = false)
	{
		$html = '<div class="alert alert-block alert-warning">'
			. 'Vous pouvez désactiver temporairement la vérification des URL :<div><label>'
			. '<input type="checkbox" name="' . self::DISABLED_IF_REQUEST_CONTAINS . '" value="1" %s />'
			. ' Accepter les URL sans les vérifier</label></div></div>';
		if (!empty($_REQUEST[self::DISABLED_IF_REQUEST_CONTAINS])) {
			return sprintf($html, 'checked="checked"');
		}
		if ($force) {
			return sprintf($html, '');
		}
		foreach ($object->rules() as $rule) {
			if (substr_compare($rule[1], 'UrlFetchableValidator', -21) === 0) {
				foreach (preg_split('/,\s*/', $rule[0]) as $target) {
					if ($object->getError($target)) {
						return sprintf($html, '');
					}
				}
			}
		}
		return '';
	}

	/**
	 * Validates the attribute of the object.
	 *
	 * If there is any error, the error message is added to the object.
	 *
	 * @param CModel $object the object being validated.
	 * @param string $attr the attribute being validated.
	 */
	protected function validateAttribute($object, $attr)
	{
		if ((defined('TEST_BASE_URL') || (defined('YII_TEST') && YII_TEST)) && $this->disabledInUnitTests) {
			return;
		}
		if ($this->allowEmpty && $this->isEmpty($object->{$attr})) {
			return;
		}
		if ($object->hasErrors($attr)) {
			return;
		}
		if (!$this->checkLink($object->{$attr})) {
			$object->addError($attr, "Cette URL n'a pu être téléchargée : " . $this->lastError);
		}
	}
}
