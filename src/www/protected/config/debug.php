<?php

/*
 * Modify this file if you want to change the debugging behaviour,
 * when APP_PRODUCTION_MODE is true or false.
 */


// Set the production mode according to the various parameters
if (!defined('APP_PRODUCTION_MODE')) {
	define('APP_PRODUCTION_MODE', true);
}

// How to behave in production mode
if (APP_PRODUCTION_MODE) {
	// in production mode, no debugging
	defined('YII_DEBUG') or define('YII_DEBUG', false);
	defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 0);
	ini_set('error_display', 0);
} else {
	defined('YII_DEBUG') or define('YII_DEBUG', true);
	defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
	error_reporting(defined('E_STRICT') ? (E_ALL | E_STRICT) : E_ALL);

	// display logs: warnings and errors at the bottom of the page
	$localConfig['components']['log']['routes'][] = [
		'class' => 'CWebLogRoute',
		'levels' => 'error, warning',
		'showInFireBug' => false,
	];
}
