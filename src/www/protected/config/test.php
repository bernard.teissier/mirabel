<?php

// This is the configuration for the tests.

// It is loaded after "main.php" and "local.php"

/**
 * For automated tests,
 * change the following URL based on your server configuration
 * Make sure the URL ends with a slash so that we can use relative URLs in test cases
 */
defined('TEST_BASE_URL')
	or define('TEST_BASE_URL', 'http://localhost/testdrive/index-test.php/');


return [
	'components' => [
		'db' => [
			'connectionString' => 'mysql:host=' . (getenv('MIRABEL_MYSQL_HOST') ?: 'localhost') . ';dbname=mirabel2_tests',
			'username' => 'mirabel',
			'password' => 'mirabel',
			'charset' => 'utf8mb4',
			'enableProfiling' => 0,
			'enableParamLogging' => 0,
			'schemaCachingDuration' => 3600,
		],
		'cache' => [
			'class' => 'CFileCache',
		],
		'matomo' => [
			'siteId' => 0, // 0 => disabled
			'tokenAuth' => '', // Disable the tracking within the API.
		],
		'request' => [
			'class' => 'CodeceptionHttpRequest',
		],
	],
	'params' => [
		'matomo' => [
			'siteId' => 0,
			'rootUrl' => '',
			'tokenAuth' => '',
		],
		'sphinx' => [
			'host' => getenv('MIRABEL_SPHINX_HOST') ?: 'localhost',
			'port' => getenv('MIRABEL_SPHINX_PORT') ?: 9313,
			'prefix' => 'test_',
		],
	],
];
