<?php

/**
 * Do not modify this file unless you're a developper.
 * For configuring the application, use "local.php" in the same directory.
 */

setlocale(LC_ALL, 'fr_FR.utf8');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return [
	'basePath' => dirname(__DIR__),
	'language' => 'fr',
	'charset' => 'UTF-8',

	// preloading components
	'preload' => ['bootstrap', 'log'],

	// autoloading model and component classes
	'import' => [
		'application.models.*',
		'application.models.abstract.*',
		'application.models.behaviors.*',
		'application.models.decorators.*',
		'application.models.forms.*',
		'application.models.import.*',
		'application.models.interfaces.*',
		'application.models.searches.*',
		'application.models.services.*',
		'application.models.serialized.*',
		'application.models.traits.*',
		'application.components.*',
		'ext.bootstrap.widgets.*',
		'ext.validators.*',
		'ext.ExtJsTree.*',
	],

	'modules' => [
		'api',
	],

	// application components
	'components' => [
		'authManager' => [
			'enabled' => false,
		],
		'bootstrap' => [
			'class' => 'ext.bootstrap.components.Bootstrap',
			'responsiveCss' => true,
			'plugins' => [
				'popover' => [
					'options' => [
						'container' => 'body',
					],
					'suffix' => '.click(function(e) { e.preventDefault(); return false; })',
				],
				'tooltip' => [
					'selector' => 'a.tooltip', // bind the plugin tooltip to anchor tags with the 'tooltip' class
					'options' => [
						'placement' => 'bottom', // place the tooltips below instead
					],
				],
				// To prevent a plugin from being loaded set it to false as demonstrated below
				'transition' => false, // disable CSS transitions
				// If you need help with configuring the plugins, please refer to Bootstrap's own documentation:
				// https://getbootstrap.com/2.3.2/
			],
		],
		'cache' => [
			'class' => 'CFileCache',
		],
		'coreMessages' => [
			'basePath' => null,
		],
		'errorHandler' => [
			// use 'site/error' action to display errors
			'errorAction' => 'site/error',
		],
		'format' => [
			'class' => '\extensions\format\ExtFormatter',
			'dateFormat' => 'd/m/Y',
			'datetimeFormat' => 'd/m/Y H:i',
			'timeFormat' => 'H:i',
			'booleanFormat' => ['Non', 'Oui'],
			'numberFormat' => ['decimals' => 2, 'decimalSeparator' => ',', 'thousandSeparator' => ' '],
		],
		'log' => [
			'class' => 'CLogRouter',
			'routes' => [
				[
					'class' => 'ext.ExtDbLogRoute.ExtDbLogRoute',
					'levels' => 'error',
					'connectionID' => 'db',
					'except' => 'exception.CHttpException.404',
				],
				[
					'class' => 'CFileLogRoute',
					'levels' => 'error',
					'logFile' => 'errors.log',
					'except' => 'exception.CHttpException.404',
				],
				[
					'class' => 'CFileLogRoute',
					'levels' => 'warning',
					'logFile' => 'warnings.log',
					'except' => 'exception.CHttpException.404',
				],
			],
		],
		'markdown' => function () {
			$environment = \League\CommonMark\Environment::createCommonMarkEnvironment();
			$environment->addExtension(new \League\CommonMark\Extension\DisallowedRawHtml\DisallowedRawHtmlExtension());
			$environment->addExtension(new \League\CommonMark\Extension\HeadingPermalink\HeadingPermalinkExtension());
			$environment->setConfig(['heading_permalink' => ['insert' => 'after']]);
			$environment->addExtension(new \League\CommonMark\Extension\Strikethrough\StrikethroughExtension());
			$environment->addExtension(new \League\CommonMark\Extension\Table\TableExtension());
			$environment->addExtension(new \League\CommonMark\Extension\TaskList\TaskListExtension());
			return new \League\CommonMark\CommonMarkConverter([], $environment);
		},
		'session' => [
			'class' => CHttpSession::class,
			'gcProbability' => 1, // 1% chance of removing old sessions
			'savePath' => dirname(__DIR__) . '/runtime/sessions', // \Yii::getPathOfAlias('application.runtime.sessions'),
			'timeout' => 1800, // seconds
		],
		'sphinx' => function() {
			$c = new \CDbConnection('mysql:unix_socket=/tmp/sphinx-mirabel-mysql.socket');
			$c->autoConnect = false;
			$c->enableProfiling = false;
			$c->enableParamLogging = false;
			$c->schemaCachingDuration = (APP_PRODUCTION_MODE ? 3600 : 0);
			$c->tablePrefix = \Yii::app()->params['sphinx']['prefix'];
			$c->driverMap['mysql'] = '\components\sphinx\Schema';
			return $c;
		},
		'urlManager' => [
			// uncomment the following to enable URLs in path-format
			'urlFormat' => 'path',
			'appendParams' => false,
			'showScriptName' => false,
			'rules' => [
				'site/page/<p:.+>' => 'site/page',
				'revue/<by:titre|titre-id|issn|sudoc|worldcat>/<id:.+>' => 'revue/viewBy',
				'upload/view/<file:.+>' => 'upload/view',
				'editeur/idref/<idref:\d+.>' => 'editeur/idref',
				'editeur/pays/<paysId:[A-Z-]{2,5}>/<paysNom:.+>' => 'editeur/pays',
				'editeur/pays/<paysId:.+>' => 'editeur/pays',
				'theme/<id:\d+>/<name:.+>' => 'categorie/revues',
				'doc/<view:.+>.md' => 'doc/view',
				// API
				['api/<controller>/index', 'pattern' => 'api/mes/<controller:acces|titres>', 'defaultParams' => ['partenaire' => -1]],
				['api/<controller>/<action>', 'pattern' => 'api/mes/<controller:acces>/<action:changes>', 'defaultParams' => ['partenaire' => -1]],
				'api/<controller:\w+>/<id:\d+>' => 'api/<controller>/view',
				// Generic rules for pretty URLs
				'<controller:\w+>/<id:\d+>/<nom:[^\/]+>' => '<controller>/view',
				'<controller:\w+>/<id:\d+>' => '<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
				'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
			],
		],
		'user' => [
			'class' => 'WebUser',
			'autoUpdateFlash' => false,
		],
		'viewRenderer' => [
			'class' => 'MdViewRenderer',
		],
	],
	'params' => [
		'version' => 'v2.5',
		// comment on the version
		'versionNotes' => '2017-07-25',
		// Mantis Project ID
		'mantisId' => 28,
	],
];
