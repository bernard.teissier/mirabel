<?php

/**
 * This file completes and overwrites the configuration set in "main.php".
 */

/**
 * If true, then the debug mode will be disabled (see "debug.php").
 * The configuration below also uses this setting
 */
defined('APP_PRODUCTION_MODE')
	or define('APP_PRODUCTION_MODE', true);

/**
 * Read and update each setting to your instance
 */
return [
	// defautt: "Mir@bel"
	'name' => 'Mir@bel local',

	'components' => [
		// rw access to MariaDB/MySQL
		'db' => [
			'connectionString' => 'mysql:host=localhost;dbname=mirabel',
			'username' => 'mirabeluser',
			'password' => 'mirabelpass',
			'charset' => 'utf8mb4',
			'enableProfiling' => !APP_PRODUCTION_MODE,
			'enableParamLogging' => !APP_PRODUCTION_MODE,
			'schemaCachingDuration' => (APP_PRODUCTION_MODE ? 3600 : 0),
		],
	],

	// application-level parameters that can be accessed
	// with the syntax Yii::app()->params['paramName']
	'params' => [
		// Main emails. Other settings are on the configuration web page.
		'adminEmail' => 'contact@example.org',
		'emailAlerteEditeurs' => 'alerte@example.org',

		// required for building URLs in console commands
		'baseUrl' => 'http://mirabel.localhost',

		// do not change manually, should be filled up from the VCS when deployed
		'vcsVersion' => '',

		// Sphinx search
		'sphinx' => [
			'host' => 'localhost',
			'port' => 9313,
			'prefix' => 'devel_',
		],

		// arbre thématique en page d'accueil
		'displayThemesOnHomePage' => true,

		// user tracking
		'matomo' => [
			'siteId' => 0, // 0 => disabled
			'rootUrl' => 'https://matomo.localhost/', // trailing slash
			'tokenAuth' => '', // For tracking API. Generated with Matomo, Personal, Security, Auth tokens
		],

		// To access Sherpa, a key is needed.
		// See https://v2.sherpa.ac.uk/api/
		'sherpa' => [
			'api-key' => '',
		],

		/*
		 * If set, the API will use a distinct connection to the same DB
		 */
		'api.db' => [
			/*
			'connectionString' => 'mysql:host=localhost;dbname=mirabel',
			'username' => 'mirabelro',
			'password' => 'mir-read-only',
			'enableProfiling' => false,
			'enableParamLogging' => false,
			 */
		],

		/*
		// Used by the CLI script that uploads KBART onto a Bacon server.
		'bacon' => [
			'url' => 'https://cloud.abes.fr/...',
			'username' => '',
			'password' => '',
		],
		 */
	],
];
