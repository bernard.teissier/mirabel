<?php

require_once dirname(__DIR__, 3) . '/vendor/autoload.php';
spl_autoload_register(['YiiBase', 'autoload']);

$config = CMap::mergeArray(
	CMap::mergeArray(require __DIR__ . '/config/main.php', require __DIR__ . '/config/local.php'),
	CMap::mergeArray(require __DIR__ . '/config/console.php', require __DIR__ . '/config/test.php')
);

require_once YII_PATH . '/yiic.php';
