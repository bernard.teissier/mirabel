<?php
return [
	'template' => 'silecs',
	'tablePrefix' => '',
	'modelPath' => 'application.models',
	'baseClass' => 'CActiveRecord',
	'buildRelations' => '1',
];
