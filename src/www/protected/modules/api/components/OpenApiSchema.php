<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class OpenApiSchema
{
	public $ref;

	public $format;

	public $title;

	public $description;

	public $default;

	public $multipleOf;

	public $maximum;

	public $exclusiveMaximum;

	public $minimum;

	public $exclusiveMinimum;

	public $maxLength;

	public $minLength;

	public $pattern;

	public $maxItems;

	public $minItems;

	public $uniqueItems;

	public $maxProperties;

	public $minProperties;

	public $required;

	public $enum;

	public $type;

	public $items;

	public $properties = [];

	public function __construct($values)
	{
		foreach ((object) $values as $k => $v) {
			$k = str_replace('$', '', $k);
			if (property_exists($this, $k)) {
				$this->{$k} = $v;
			}
		}
	}

	public function __toString(): string
	{
		return $this->toHtml();
	}

	public function toHtml(): string
	{
		$value = '';
		if ($this->title) {
			$value .= '<span class="title">' . htmlspecialchars($this->title) . '</span> ';
		}
		if (isset($this->type)) {
			if ($this->type === 'array') {
				$sub = new OpenApiSchema($this->items);
				$value .= "[ " . $sub->toHtml() . " ]";
			} elseif ($this->type === 'object') {
				$values = [];
				if (isset($this->ref)) {
					$class = htmlspecialchars(basename($this->ref));
					$value .= '→ <a href="#definition-' . $class . '">' . $class . "</a>";
				} elseif ($this->properties) {
					foreach ($this->properties as $p) {
						$sub = new OpenApiSchema($p);
						$values[] = $sub->toHtml();
						$value .= '{ ' . join(', ', $values) . ' }';
					}
				} else {
					$value = "?";
				}
			} else {
				$value .= htmlspecialchars($this->type);
				if (isset($this->format)) {
					$value .= ', ' . htmlspecialchars($this->format);
				}
			}
		} elseif (isset($this->ref)) {
			$class = htmlspecialchars(basename($this->ref));
			$value .= '→ <a href="#definition-' . $class . '">' . $class . "</a>";
		}
		if ($this->description) {
			$value .= '<div class="description">' . htmlspecialchars($this->description) . '</div> ';
		}
		return $value;
	}
}
