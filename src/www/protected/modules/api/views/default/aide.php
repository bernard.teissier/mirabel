<?php

/* @var $aide OpenApi */

$this->pageTitle = $api->info->title;

$markdown = new CMarkdownParser();
?>

<div id="api-yaml">
    Télécharger la description au format
    <?= CHtml::link("OpenApi JSON", ['/api/default/openapi']) ?> (Swagger)
</div>

<h1><?= CHtml::encode($this->pageTitle) ?></h1>

<section id="api-info">
	<div class="api-description">
		<?= $api->info->description ?>
	</div>
	<div>
		<strong>Version</strong>
		<?= CHtml::encode($api->info->version) ?>
	</div>
	<?php
	if (isset($api->info->termsOfService)) {
		?>
        <div class="api-terms">
            <?= $markdown->safeTransform($api->info->termsOfService) ?>
        </div>
        <?php
	}
	?>
    <?php
	if (isset($api->info->license)) {
		?>
        <div class="api-license">
            <h2>Licence des données</h2>
            <?= CHtml::encode($api->info->license->name) ?> :
            <?= CHtml::link(CHtml::encode($api->info->license->url), $api->info->license->url) ?>
        </div>
        <?php
	}
	?>
    <?php
	if (isset($api->info->contact)) {
		?>
        <div class="api-contact">
            <h2>Contact</h2>
            <dl>
                <dt>Nom</dt><dd><?= CHtml::encode($api->info->contact->name) ?></dd>
                <?php if (isset($api->info->contact->url)) {
			echo "<dt>URL</dt>"
						. "<dd>" . CHtml::link(CHtml::encode($api->info->contact->url), $api->info->contact->url) . "</dd>";
		} ?>
                <?php if (isset($api->info->contact->email)) {
			echo "<dt>Adresse électronique</dt>"
						. "<dd>" . CHtml::encode($api->info->contact->email) . "</dd>";
		} ?>
            </dl>
            <?= CHtml::encode($api->info->contact->name) ?> :
            <?= CHtml::link(CHtml::encode($api->info->contact->url), $api->info->contact->url) ?>
        </div>
        <?php
	}
	?>
    <div class="api-format">
        <h2>Format des réponses</h2>
        Les réponses de l'API sont au format JSON.
        Le format XML pourrait être ajouté, à la demande.
    </div>

</section>

<section class="api" id="api-paths">
    <h2>Liste des chemins</h2>

    <?php
	if ($api->basePath) {
		echo "<p>Tous les chemins doivent être préfixés par <code>" . CHtml::encode($api->basePath) . "</code>.</p>";
	}
	?>
    <ul>
        <?php
		foreach ($api->paths as $path => $methods) {
			foreach ($methods as $method => $details) {
				$htmlMethod =  CHtml::tag('span', ['class' => 'api-http-method'], strtoupper($method));
				echo CHtml::tag(
					'li',
					[],
					CHtml::link(
						CHtml::encode($path) . " " . $htmlMethod,
						'#path-' . $method . str_replace('/', '-', $path)
					)
					. " "
					. CHtml::tag('span', ['class' => 'api-summary'], $details->summary)
				);
			}
		}
		?>
    </ul>
</section>

<section class="api" id="api-details">
    <h2>Détail des actions</h2>

    <?php
	$baseUrl = $api->getBaseUrl();
	foreach ($api->paths as $path => $methods) {
		echo '<h3 class="api-path">' . CHtml::encode($path) . '</h3>';
		foreach ($methods as $method => $details) {
			$pathUrl = $baseUrl . $path; ?>
            <article id="path-<?= CHtml::encode($method . str_replace('/', '-', $path)) ?>">
                <h4><?= CHtml::encode(strtoupper($method) . " " . $path) ?></h4>
                <dl>
                    <dt>Titre</dt>
                    <dd><?= $details->summary ?></dd>
                    <?php if (!empty($details->description)) { ?>
                        <dt>Description</dt>
                        <dd><?= $markdown->safeTransform($details->description) ?></dd>
                    <?php } ?>
                    <dt>URL</dt>
                    <dd><?= CHtml::encode(strtoupper($method) . " " . $pathUrl) ?></dd>
                    <?php if (!empty($details->parameters)) { ?>
                        <dt>Paramètres</dt>
                        <dd class="api-parameters">
                            <table class="table table-compact">
                                <thead>
                                    <tr>
                                        <th>Nom</th>
                                        <th>Description</th>
                                        <th>Lieu</th>
                                        <th>Requis ?</th>
                                        <th>Type, format</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
									foreach ($details->parameters as $parameter) {
										echo "<tr>"
											. CHtml::tag('td', [], CHtml::encode($parameter->name))
											. CHtml::tag('td', [], $markdown->safeTransform($parameter->description))
											. CHtml::tag('td', [], CHtml::encode($parameter->in))
											. CHtml::tag('td', [], empty($parameter->required) ? "Non" : "Oui")
											. CHtml::tag('td', [], CHtml::encode($parameter->type) . (isset($parameter->format) ? ', ' . CHtml::encode($parameter->format) : ''))
											. "</tr>\n";
									}
									?>
                                </tbody>
                            </table>
                        </dd>
                    <?php } ?>
                    <?php if (!empty($details->responses)) { ?>
                        <dt>Réponses</dt>
                        <dd class="api-responses">
                            <table class="table table-compact">
                                <thead>
                                    <tr>
                                        <th>Code HTTP</th>
                                        <th>Description</th>
                                        <th>Schema</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
									foreach ($details->responses as $code => $response) {
										echo "<tr>"
											. CHtml::tag('td', [], CHtml::encode($code))
											. CHtml::tag('td', [], $markdown->safeTransform($response->description))
											. CHtml::tag('td', [], new OpenApiSchema($response->schema))
											. "</tr>\n";
									}
									?>
                                </tbody>
                            </table>
                        </dd>
                    <?php } ?>
                </dl>
                <?php
				if (empty($details->parameters)) {
					echo '<p> '
						. CHtml::link("Essayer cette action", $pathUrl, ['class' => 'btn btn-default', 'target' => '_blank'])
						. '</p>';
				} ?>
            </article>
            <?php
		}
	}
	?>
</section>


<section class="api" id="api-definitions">
    <h2>Modèles de données</h2>

    <?php
	foreach ($api->definitions as $name => $definition) {
		echo '<article id="definition-' . CHtml::encode($name) . '">';
		echo '<h3 class="api-definition">' . CHtml::encode($name) . '</h3>';
		if ($definition->type === 'object') {
			if (isset($definition->description)) {
				echo CHtml::tag("div", [], $markdown->safeTransform($definition->description));
			} ?>
            <table class="table table-compact">
                <tbody>
                    <?php
					foreach ($definition->properties as $propName => $propDetails) {
						echo "<tr>"
							. CHtml::tag('td', [], CHtml::encode($propName))
							. CHtml::tag('td', [], new OpenApiSchema($propDetails))
							. "</tr>\n"
							;
					} ?>
                </tbody>
            </table>
            <?php
		}
		echo "</article>\n";
	}
	?>
</section>
