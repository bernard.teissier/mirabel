<?php

/**
 * En cas de modification de la structure, tenir à jour la codumentation de l'API dans data/openapi.json.
 * Éventuellement, utiliser editor.swagger.io pour rédiger.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class ApiAccesPossessions extends ApiAcces
{
	use FailOnUnknownParameter;

	public $abonnement = false;

	public $possession = false;

	public function afterConstruct()
	{
		$this->setScenario('search');
	}

	public function attributeNames()
	{
		return ['partenaire', 'possession', 'abonnement'];
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['partenaire', 'required', "message" => "Le paramètre 'partenaire' est requis, avec pour valeur l'identifiant secret."],
			['partenaire', 'validatePartenaire'],
			[['abonnement', 'possession'], 'boolean'],
		];
	}

	public function afterValidate()
	{
		if (!$this->abonnement && !$this->possession) {
			$this->addError('abonnement', "Aucun critère. Les filtres de possession et d'abonnement ne peuvent être tout deux désactivés.");
		}
		return parent::afterValidate();
	}

	public function apply(): array
	{
		$this->titreSql = $this->getSearchTitresSql();
		return parent::apply();
	}

	protected function getSearchTitresSql()
	{
		$search = new ApiTitres();
		$search->setPartenaire($this->part);
		$search->abonnement = $this->abonnement;
		$search->possession = $this->possession;
		$cmd = $search->search();
		$cmd->select = "t.id, abo.diffusion, abo.proxy, abo.proxyUrl";
		return $cmd->getText();
	}
}
