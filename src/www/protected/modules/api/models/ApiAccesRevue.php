<?php

use models\validators\IssnValidator;

/**
 * En cas de modification de la structure, tenir à jour la codumentation de l'API dans data/openapi.json.
 * Éventuellement, utiliser editor.swagger.io pour rédiger.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class ApiAccesRevue extends ApiAccesTitre
{
	public $revueid;

	public $issn;

	public $worldcat;

	public $sudoc;

	public function attributeNames()
	{
		return ['revueid', 'issn', 'partenaire', 'worldcat', 'sudoc'];
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['partenaire', 'validatePartenaire'],
			['revueid, worldcat', 'numerical', 'integerOnly' => true],
			['sudoc', 'safe'],
			['issn', '\models\validators\IssnValidator', 'allowMissingCaret' => true, 'allowEmptyChecksum' => true],
		];
	}

	public function beforeValidate()
	{
		if (!array_filter($this->attributes)) {
			$this->addError('revueid', "Au moins un critère doit être non-vide. Voir l'aide HTML ou les spécifications OpenAPI pour la liste des paramètres.");
		}
		return parent::beforeValidate();
	}

	public function apply(): array
	{
		$titreIds = $this->searchTitres();
		if ($titreIds) {
			return $this->searchServices($titreIds);
		}
		return [
			'code' => 404,
			'message' => "Aucun titre ne correspond.",
		];
	}

	/**
	 * @return int[] Array of titreId
	 */
	protected function searchTitres(): array
	{
		if (empty($this->revueid)) {
			$command = Yii::app()->db->createCommand()
				->select('t.revueId')
				->from('Titre t')
				->leftJoin("Issn i", "i.titreId = t.id");

			if ($this->worldcat) {
				$command->andWhere('i.worldcatOcn = :worldcat', [':worldcat' => $this->worldcat]);
			}
			if ($this->sudoc) {
				$command->andWhere('i.sudocPpn = :sudoc', [':sudoc' => $this->sudoc]);
			}
			if ($this->issn) {
				$v = new IssnValidator();
				$v->allowEmpty = false;
				$v->allowEmptyChecksum = true;
				$v->allowMissingCaret = true;
				$v->specialValues = [];
				$v->validateString($this->issn);
				$issn = $v->lastIssn;
				$command->andWhere("i.issn = :issn", [':issn' => $issn]);
			}
			$revueId = (int) $command->queryScalar();
			if (!$revueId) {
				return [];
			}
		} else {
			$revueId = (int) $this->revueid;
		}

		return Yii::app()->db->createCommand("SELECT id FROM Titre WHERE revueId = $revueId")->queryColumn();
	}
}
