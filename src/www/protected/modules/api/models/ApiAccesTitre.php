<?php

/**
 * En cas de modification de la structure, tenir à jour la codumentation de l'API dans data/openapi.json.
 * Éventuellement, utiliser editor.swagger.io pour rédiger.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class ApiAccesTitre extends ApiTitres
{
	public function attributeNames()
	{
		return array_merge(parent::attributeNames(), ['partenaire']);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array_merge(
			parent::rules(),
			[
				['partenaire', 'validatePartenaire'],
			]
		);
	}

	public function apply(): array
	{
		$titreIds = $this->searchTitresIds();
		if ($titreIds) {
			return $this->searchServices($titreIds);
		}
		return [
			'code' => 404,
			'message' => "Aucun accès/titre ne correspond.",
		];
	}

	/**
	 * @param array $titreIds
	 * @return Service[]
	 */
	protected function searchServices($titreIds): array
	{
		$apiAcces = new ApiAcces();
		$apiAcces->titreIds = $titreIds;
		if ($this->issn) {
			$apiAcces->displayIssns = true;
		}
		if ($this->part) {
			$apiAcces->setPartenaire($this->part);
		}
		return $apiAcces->apply();
	}

	protected function searchTitresIds(): array
	{
		$command = $this->search();
		$command->select = "t.id";
		return $command->queryColumn();
	}
}
