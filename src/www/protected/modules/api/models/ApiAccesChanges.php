<?php

/**
 * En cas de modification de la structure, tenir à jour la codumentation de l'API dans data/openapi.json.
 * Éventuellement, utiliser editor.swagger.io pour rédiger.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class ApiAccesChanges extends ApiAccesPossessions
{
	public $depuis;

	public function attributeNames()
	{
		return ['partenaire', 'depuis'];
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['partenaire, depuis', 'required'],
			['partenaire', 'validatePartenaire'],
			['depuis', 'numerical', 'integerOnly' => true, 'min' => 10000],
		];
	}

	public function beforeValidate()
	{
		// allow &depuis=2019-01-22+12:30:00
		if (!ctype_digit($this->depuis)) {
			$this->depuis = strtotime($this->depuis);
		}
		return parent::beforeValidate();
	}

	public function afterValidate()
	{
		return true;
	}

	public function apply(): array
	{
		return [
			'ajouts' => $this->searchAdditions(),
			'modifications' => $this->searchUpdates(),
			'suppressions' => $this->searchDeletions(),
		];
	}

	protected function searchAdditions()
	{
		$command = parent::search();
		$command->andWhere("hdateCreation > " . (int) $this->depuis);
		return $command->queryAll();
	}

	protected function searchUpdates()
	{
		$command = parent::search();
		$command->andWhere("hdateCreation < " . (int) $this->depuis . " AND hdateModif > " . (int) $this->depuis);
		return $command->queryAll();
	}

	protected function searchDeletions()
	{
		$sql = "SELECT * FROM Intervention "
			. "WHERE hdateVal > " . (int) $this->depuis
			. " AND action = 'service-D' AND titreId IS NOT NULL";
		$interventions = Intervention::model()->findAllBySql($sql);
		$list = [];
		foreach ($interventions as $i) {
			/* @var $i Intervention */
			$content = $i->contenuJson->toArray();
			foreach ($content as $c) {
				if ($c['model'] === 'Service' && $c['operation'] === 'delete') {
					$list[] = (int) $c['before']['id'];
				}
			}
		}
		return $list;
	}
}
