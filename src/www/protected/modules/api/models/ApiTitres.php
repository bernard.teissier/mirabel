<?php

/**
 * En cas de modification de la structure, tenir à jour la documentation de l'API dans data/openapi.json.
 * Éventuellement, utiliser editor.swagger.io pour rédiger.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class ApiTitres extends CModel implements ApiInterface
{
	use FailOnUnknownParameter;
	use WithPartenaireAttribute;
	use ValidatorsApi;

	/**
	 * @var int|array
	 */
	public $id;

	/**
	 * @var int|array
	 */
	public $revueid;

	/**
	 * @var int|array
	 */
	public $worldcat;

	public $issn;

	public $sudoc;

	public $titre;

	/**
	 * @var bool
	 */
	public $actif;

	/**
	 * @var bool
	 */
	public $abonnement;

	/**
	 * @var bool
	 */
	public $possession;

	private $issns = [];

	public function afterConstruct()
	{
		$this->setScenario('search');
	}

	public function attributeNames()
	{
		return [
			'id', 'revueid', 'issn', 'worldcat', 'sudoc', 'titre', 'actif',
			'partenaire', 'abonnement', 'possession',
		];
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['id, revueid, worldcat', 'validateSequence'],
			['sudoc, titre', 'safe'],
			['issn', 'validateIssns'],
			['partenaire', 'validatePartenaire'],
			['actif, abonnement, possession', 'boolean'],
		];
	}

	public function afterValidate()
	{
		if (($this->abonnement || $this->possession) && !$this->partenaireId) {
			$this->addError('partenaire', "Les filtres de possession et d'abonnement ne sont accessibles qu'avec le critère \"partenaire\".");
		}
		if ($this->titre) {
			$crit = str_replace(['%', '_'], ['', ''], $this->titre);
			if (strlen($crit) < 3) {
				$this->addError('titre', "Le filtre par titre doit porter sur au moins 3 caractères.");
			}
		}
		return parent::afterValidate();
	}

	public function apply(): array
	{
		$titresQuery = $this->search();
		$titres = $titresQuery->queryAll();
		if ($titres) {
			// add ISSN data, with a sub-query if many journals were found
			if (count($titres) < 50) {
				$this->loadIssns($titres);
			} else {
				$this->loadIssns($titresQuery);
			}
			return array_map([$this, "formatResult"], $titres);
		}
		return [];
	}

	public function search(): \CDbCommand
	{
		$command = Yii::app()->db->createCommand()
			->select("t.*, GROUP_CONCAT(DISTINCT Editeur.nom SEPARATOR '\t') AS editeurs, (1) AS noquotes")
			->from('Titre t')
			->leftJoin('Titre_Editeur te', "te.titreId = t.id")
			->leftJoin('Editeur', "te.editeurId = Editeur.id")
			->group("t.id")
			->order("t.titre ASC, t.revueId");

		if ($this->partenaireId) {
			$partenaireId = (int) $this->partenaireId;
			$command->setSelect($command->select . ", pt.identifiantLocal, pt.bouquet");
			$extraUnion = "";
			if ($this->possession) {
				if ($this->abonnement) {
					$command->leftJoin('Partenaire_Titre pt', "pt.titreId = t.id AND pt.partenaireId = $partenaireId");
					$extraUnion = "UNION SELECT titreId, 'possession' AS diffusion, 1 AS proxy, NULL AS proxyURL FROM Partenaire_Titre WHERE partenaireId = $partenaireId";
				} else {
					$command->join('Partenaire_Titre pt', "pt.titreId = t.id AND pt.partenaireId = $partenaireId");
					$command->join(
						"(SELECT titreId, 'possession' AS diffusion, 1 AS proxy, NULL AS proxyUrl FROM Partenaire_Titre WHERE partenaireId = $partenaireId) AS abo",
						"abo.titreId = t.id"
					);
				}
			} else {
				// LEFT JOIN just for local identifiers (ID of the title local to the partner)
				$command->leftJoin('Partenaire_Titre pt', "pt.titreId = t.id AND pt.partenaireId = $partenaireId");
			}
			if ($this->abonnement) {
				$aboId = Abonnement::ABONNE;
				$command
					->join(
						<<<EOSQL
							(
							  SELECT s.titreId, 'abonné' AS diffusion, a.proxy, a.proxyUrl
							  FROM Abonnement a
							    JOIN Service s ON s.ressourceId = a.ressourceId
							  WHERE a.partenaireId = $partenaireId AND a.mask = $aboId AND a.collectionId IS NULL
							UNION
							  SELECT s.titreId, 'abonné' AS diffusion, a.proxy, a.proxyUrl
							  FROM Abonnement a
							    JOIN Service_Collection sc ON sc.collectionId = a.collectionId
							    JOIN Service s ON sc.serviceId = s.id
							  WHERE a.partenaireId = $partenaireId AND a.mask = $aboId
							$extraUnion
							) AS abo
							EOSQL,
						"t.id = abo.titreId"
					);
			}
		}
		if ($this->actif) {
			$command->andWhere("t.obsoletePar IS NULL");
		}
		if ($this->id) {
			if (is_array($this->id)) {
				$command->andWhere('t.id IN(' . join(',', array_map('intval', $this->id)) . ")");
			} else {
				$command->andWhere('t.id = ' . (int) $this->id);
			}
		}
		if ($this->revueid) {
			if (is_array($this->revueid)) {
				$command->andWhere('t.revueId IN(' . join(',', array_map('intval', $this->revueid)) . ")");
			} else {
				$command->andWhere('t.revueId = ' . (int) $this->revueid);
			}
			$command->order("t.revueId, (t.obsoletePar IS NULL) DESC, t.dateDebut DESC");
		}
		if ($this->titre) {
			if (strpos($this->titre, "%") === false) {
				$command->andWhere(
					't.titre IN (:titre, :nopref) OR t.sigle = :sigle',
					[':titre' => $this->titre, ':nopref' => self::removePrefix($this->titre), ':sigle' => $this->titre]
				);
			} else {
				$title = str_replace(
					['%', '_'],
					['\%', '\_'],
					self::removePrefix(trim($this->titre, '%'))
				);
				$command->andWhere(
					't.titre LIKE :titre',
					[':titre' => preg_match('/^%.+%/', $this->titre) ? "%$title%" : "$title%"]
				);
			}
		}

		if ($this->issn || $this->sudoc || $this->worldcat) {
			$command->leftJoin("Issn i", "i.titreId = t.id")
				->group("t.id");
			if ($this->worldcat) {
				if (is_array($this->worldcat)) {
					$command->andWhere('i.worldcatOcn IN(' . join(',', array_map('intval', $this->worldcat)) . ")");
				} else {
					$command->andWhere('i.worldcatOcn = :worldcat', [':worldcat' => (int) $this->worldcat]);
				}
			}
			if ($this->sudoc) {
				$command->andWhere('i.sudocPpn = :sudoc', [':sudoc' => $this->sudoc]);
			}
			if ($this->issn) {
				if (is_array($this->issn)) {
					$in = join(
						"','",
						array_filter(
							$this->issn,
							function ($x) {
								return preg_match('/^[\dX-]+$/', $x);
							}
						)
					);
					$command->andWhere("i.issn IN('$in') OR i.issnl IN('$in')");
				} else {
					$command->andWhere(
						"i.issn = :i1 OR i.issnl = :i2",
						[':i1' => $this->issn, ':i2' => $this->issn]
					);
				}
			}
		}

		return $command;
	}

	/**
	 * @param array|CDbCommand $titres
	 */
	public function loadIssns($titres): void
	{
		$this->issns = [];
		if (is_array($titres)) {
			if (!$titres) {
				return;
			}
			$ids = array_map(function ($t) {
				return (int) $t['id'];
			}, $titres);
			$joinedIds = join(',', $ids);
			$params = [];
		} else {
			assert($titres instanceof \CDbCommand);
			$titres->setText("");
			$titres->select("t.id");
			$titres->order = "";
			$joinedIds = $titres->getText();
			$params = $titres->params;
		}
		$sql = <<<EOSQL
			SELECT
			    titreId
			    , bnfArk
			    , issn
			    , issnl
			    , CASE statut WHEN 0 THEN 'valide' WHEN 2 then 'en cours' WHEN 4 THEN 'sans' END AS statut
			    , sudocNoHolding
			    , sudocPpn
			    , support
			    , worldcatOcn
			FROM Issn
			WHERE titreId IN ($joinedIds) AND statut IN (0, 2, 4)
			ORDER BY titreId ASC, id ASC
			EOSQL;
		$issns = Yii::app()->db
			->createCommand($sql)
			->queryAll(true, $params);
		foreach ($issns as $issn) {
			$tid = (int) $issn['titreId'];
			if (isset($this->issns[$tid])) {
				$this->issns[$tid][] = self::formatIssn($issn);
			} else {
				$this->issns[$tid] = [self::formatIssn($issn)];
			}
		}
	}

	public static function formatIssn($issn)
	{
		$i = (object) $issn;
		return (object) array_filter(
			[
				"bnfark" => $i->bnfArk ?: null,
				"issn" => $i->issn ?: null,
				"issnl" => $i->issnl ?: null,
				"statut" => $i->statut,
				"sudocnoholding" => $i->sudocPpn ? (bool) $i->sudocNoHolding : null,
				"sudocppn" => $i->sudocPpn ?: null,
				"support" => $i->support,
				"worldcatocn" => $i->worldcatOcn ? (int) $i->worldcatOcn : null,
			],
			function ($x) {
				return $x !== null;
			}
		);
	}

	public function formatResult($attributes): array
	{
		$result = [];
		if (is_array($attributes)) {
			$attributes = (object) $attributes;
		}

		$integers = [
			// new-name => DB-name
			'id' => 'id',
			'revueid' => 'revueId',
			'obsoletepar' => 'obsoletePar',
		];
		foreach ($integers as $new => $old) {
			if (isset($attributes->{$old})) {
				$result[$new] = (int) $attributes->{$old};
			} else {
				$result[$new] = null;
			}
		}

		$strings = [
			'titre' => 'titre',
			'prefixe' => 'prefixe',
			'datedebut' => 'dateDebut',
			'datefin' => 'dateFin',
			'url' => 'url',
			'periodicite' => 'periodicite',
			'identifiantpartenaire' => 'identifiantLocal',
			'bouquetpartenaire' => 'bouquet',
		];
		foreach ($strings as $new => $old) {
			if (!empty($attributes->{$old})) {
				$result[$new] = $attributes->{$old};
			} else {
				$result[$new] = null;
			}
		}
		$result['editeurs'] = $attributes->editeurs ? explode("\t", $attributes->editeurs) : [];

		if (!empty($attributes->langues)) {
			$result['langues'] = explode(', ', $attributes->langues);
		} else {
			$result['langues'] = [];
		}
		$result['liens'] = (
			empty($attributes->liensJson)
				? []
				: array_map(
					function ($l) {
						return substr($l['url'], 0, 1) === '/' ? Yii::app()->params->itemAt('baseUrl') . $l['url'] : $l['url'];
					},
					json_decode($attributes->liensJson, \JSON_OBJECT_AS_ARRAY)
				)
		);

		$result['url_revue_mirabel'] = Yii::app()->controller->createAbsoluteUrl('/revue/view', ['id' => (int) $attributes->revueId]);

		if (isset($this->issns[$result['id']])) {
			$result['issns'] = $this->issns[$result['id']];
		}

		return $result;
	}

	protected static function removePrefix(string $title): string
	{
		$prefixes = array_filter(explode('/', \Config::read('import.prefixes')));
		if (!$prefixes) {
			return $title;
		}
		foreach ($prefixes as $pref) {
			if (strncasecmp($title, $pref, strlen($pref)) === 0) {
				return substr($title, strlen($pref));
			}
		}
		return $title;
	}
}
