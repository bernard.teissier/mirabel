<?php

/**
 * En cas de modification de la structure, tenir à jour la codumentation de l'API dans data/openapi.json.
 * Éventuellement, utiliser editor.swagger.io pour rédiger.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class ApiRevues extends CModel implements ApiInterface
{
	use FailOnUnknownParameter;

	public $ressourceid;

	public $possession;

	public $partenaire;

	private $partenaireId;

	public function afterConstruct()
	{
		$this->setScenario('search');
	}

	public function attributeNames()
	{
		return ['ressourceid', 'possession', 'partenaire'];
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['partenaire', 'required'],
			['ressourceid', 'numerical', 'integerOnly' => true],
			['possession', 'boolean'],
			['partenaire', 'validatePartenaire'],
		];
	}

	public function validatePartenaire($attrName)
	{
		if (!empty($this->{$attrName})) {
			$partenaire = Partenaire::model()->findByAttributes(['hash' => $this->{$attrName}]);
			if ($partenaire) {
				$this->partenaireId = $partenaire->id;
			} else {
				$this->addError($attrName, "Aucun partenaire n'a cet identifiant.");
			}
		}
	}

	public function apply(): array
	{
		$titres = $this->search();
		if ($titres) {
			return ['revues' => self::formatResult($titres)];
		}
		return [];
	}

	protected function search(): array
	{
		$select = 't.*, i.issne';
		$command = Yii::app()->db->createCommand()
			->from('Titre t')
			->leftJoin("Identification i", "i.titreId = t.id")
			->where("t.obsoletePar IS NULL");

		if ($this->ressourceid) {
			$command
				->join("Service s", "s.titreId = t.id")
				->andWhere('s.ressourceId = :rid', [':rid' => $this->ressourceid]);
		}
		if ($this->possession && $this->partenaireId) {
			$command
				->join("Partenaire_Titre pt", "pt.titreId = t.id AND pt.partenaireId = " . $this->partenaireId);
			$select .= ", pt.identifiantLocal AS identifiantlocal";
		}

		$command->select($select);
		return $command->queryAll();
	}

	protected static function formatResult($result): array
	{
		$apititres = new ApiTitres();
		$apititres->loadIssns($result);
		$r = [];
		foreach ($result as $k => $v) {
			$r[$k] = $apititres->formatResult((object) $v);
		}
		return $r;
	}
}
