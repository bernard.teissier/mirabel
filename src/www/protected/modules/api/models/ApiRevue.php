<?php

/**
 * En cas de modification de la structure, tenir à jour la codumentation de l'API dans data/openapi.json.
 * Éventuellement, utiliser editor.swagger.io pour rédiger.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class ApiRevue extends CModel implements ApiInterface
{
	use FailOnUnknownParameter;

	public $id;

	public function attributeNames()
	{
		return ['id'];
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['id', 'numerical', 'integerOnly' => true, 'min' => 1],
		];
	}

	public function apply(): array
	{
		$revue = Yii::app()->db->createCommand("SELECT * FROM Revue WHERE statut = 'normal' AND id = " . (int) $this->id)
			->queryRow();
		if (!$revue) {
			return [
				'code' => 404,
				'message' => "Aucune revue ne correspond.",
			];
		}
		$revue['dermodif'] = Yii::app()->db->createCommand(
			"SELECT hdateVal FROM Intervention WHERE revueId = {$revue['id']} ORDER BY id DESC LIMIT 1"
		)->queryScalar();
		$apiTitre = new ApiTitres();
		$apiTitre->revueid = $revue['id'];
		$titres = $apiTitre->search()
			->order("(t.obsoletePar IS NULL) DESC, t.dateDebut DESC")
			->queryAll();
		if (empty($titres)) {
			return [];
		}
		return self::formatResult($revue, $titres);
	}

	public static function formatResult($attributes, $titres): array
	{
		$result = [];
		if (is_array($attributes)) {
			$attributes = (object) $attributes;
		}

		$integers = [
			// new-name => DB-name
			'id' => 'id',
			'dermodif' => 'dermodif',
			'derverif' => 'hdateVerif',
		];
		foreach ($integers as $new => $old) {
			if (isset($attributes->{$old})) {
				$result[$new] = (int) $attributes->{$old};
			} else {
				$result[$new] = null;
			}
		}

		$apititres = new ApiTitres();
		$apititres->loadIssns($titres);
		$result['titres'] = array_map([$apititres, 'formatResult'], $titres);

		return $result;
	}
}
