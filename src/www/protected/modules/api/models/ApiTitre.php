<?php

/**
 * En cas de modification de la structure, tenir à jour la codumentation de l'API dans data/openapi.json.
 * Éventuellement, utiliser editor.swagger.io pour rédiger.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class ApiTitre extends ApiTitres implements ApiInterface
{
	public function attributeNames()
	{
		return ['id'];
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['id', 'numerical', 'integerOnly' => true, 'min' => 1],
		];
	}

	public function apply(): array
	{
		$titres = $this->search()->queryAll();
		if ($titres) {
			$this->loadIssns($titres);
			return $this->formatResult($titres[0]);
		}
		return [
			'code' => 404,
			'message' => "Aucun titre ne correspond.",
		];
	}
}
