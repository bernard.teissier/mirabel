<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
interface ApiInterface
{
	public function getAttributes();

	public function setAttributes($attributes);

	public function validate();

	public function getErrors();

	public function apply(): array;
}
