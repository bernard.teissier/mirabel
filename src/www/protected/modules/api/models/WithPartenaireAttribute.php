<?php

trait WithPartenaireAttribute
{
	/**
	 * @var string
	 */
	public $partenaire;

	/**
	 * @var Partenaire
	 */
	public $part;

	/**
	 * @var int Partenaire.id
	 */
	protected $partenaireId;

	public function validatePartenaire(?string $attrName): void
	{
		if (!empty($this->{$attrName})) {
			$partenaire = Partenaire::model()->findByAttributes(['hash' => $this->{$attrName}]);
			if ($partenaire) {
				$this->partenaireId = $partenaire->id;
				$this->part = $partenaire;
			} else {
				throw new CHttpException(403, "Le paramètre 'partenaire' est requis, avec pour valeur l'identifiant secret du partenaire dans Mir@bel.");
			}
		}
	}

	public function setPartenaire(Partenaire $partenaire): void
	{
		$this->part = $partenaire;
		$this->partenaireId = (int) $partenaire->id;
		$this->partenaire = $partenaire->hash;
	}

	public function setPartenaireId(int $id): void
	{
		$this->partenaireId = $id;
		if (empty($this->part) || $this->part->id != $this->partenaireId) {
			$this->part = Partenaire::model()->find($this->partenaireId);
		}
	}
}
