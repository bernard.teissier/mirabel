<?php

/**
 * Applies an ApiAction and outputs the result.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class Api
{
	/**
	 * @var ApiInterface
	 */
	private $action;

	/**
	 * @var string
	 */
	private $format = 'json';

	private static $formats = [
		'json' => 'Content-Type: application/json; charset="UTF-8"',
		'xml' => 'Content-Type: text/xml; charset="UTF-8"',
		'csv' => 'Content-Type: text/csv; charset="UTF-8"; header=present',
	];

	public function __construct(ApiInterface $action)
	{
		$this->action = $action;
		/**
		 * @todo Detect if XML is requested in the HTTP header
		 */
	}

	/**
	 * @param string $format json, xml, csv
	 * @throws Exception
	 */
	public function setFormat(string $format): void
	{
		if (!isset(self::$formats)) {
			throw new Exception("Invalid format");
		}
		$this->format = $format;
	}

	/**
	 * @param int $code HTTP code
	 */
	public function header(int $code)
	{
		if (headers_sent()) {
			throw new CHttpException(500, "Headers sent");
		}
		http_response_code($code);
		header(self::$formats[$this->format]);
		header('Access-Control-Allow-Origin: *');
	}

	public function getResponse(): array
	{
		//$criteria = array_filter($this->action->getAttributes(), function($x) { return $x !== null; });
		if (!$this->action->validate()) {
			$response = [
				'code' => 500,
				'message' => "Arguments non-valides",
				'fields' => join(", ", array_keys($this->action->getErrors())),
				'details' => $this->action->getErrors(),
			];
		} else {
			$data = $this->action->apply();
			$errors = $this->action->getErrors();
			if ($errors) {
				$response = [
					'code' => 500,
					'message' => "Erreur de traitement",
					'fields' => join(", ", array_keys($errors)),
					'details' => $errors,
				];
			} else {
				$response = $data;
			}
		}
		return $response;
	}

	/**
	 * Prints the full response, HTTP header included.
	 */
	public function printResponse(): void
	{
		try {
			$response = $this->getResponse();
			$this->header($response['code'] ?? 200);
			echo $this->encode($response);
		} catch (CHttpException $e) {
			http_response_code($e->statusCode);
			echo $this->encode(
				[
					'code' => (int) $e->statusCode,
					'message' => $e->getMessage(),
				]
			);
		} catch (Exception $e) {
			echo $this->encode(
				[
					'code' => 500,
					'message' => "Erreur de traitement : " . $e->getMessage(),
				]
			);
		}
	}

	public static function processDefault(ApiInterface $engine): void
	{
		$parameters = Api::readParameters(Yii::app()->request);
		$engine->setAttributes($parameters);
		$api = new Api($engine);
		$api->printResponse();
	}

	/**
	 * @param CHttpRequest $request
	 * @param string $rootPath Which path to remove from the beginning of the URL (e.g. "/api"
	 * @return array Body JSON parameters, or ?query=JSON, or URL .../k/v, or GET parameters
	 */
	public static function readParameters(CHttpRequest $request, $rootPath = ""): array
	{
		$data = [];
		$restParams = $request->getRestParams();
		if ($restParams) {
			$data[] = $restParams;
		}
		$rawQuery = $request->getParam('query', null);
		if ($rawQuery !== null) {
			$data[] = json_decode($rawQuery);
		}
		$data[] = (empty($_GET) ? $_POST : $_GET);
		// merge these sources, with precedence at the end.
		return call_user_func_array('array_merge', $data);
	}

	/**
	 *
	 * @param mixed $response
	 * @throws Exception
	 * @return string
	 */
	private function encode($response): string
	{
		if ($this->format === 'json') {
			return json_encode($response, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
		}
		if ($this->format === 'xml') {
			/**
			 * @todo convert blindly a data structure to XML
			 */
			return "<error>Not yet implemented</error>";
		}
		if ($this->format === 'csv') {
			/**
			 * @todo convert blindly a data structure to CSV
			 */
			return "Not yet implemented";
		}
		throw new Exception("I don't know how to encode into this format.");
	}
}
