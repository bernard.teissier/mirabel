<?php

/**
 * Description of FailOnUnknwonParameter.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
trait FailOnUnknownParameter
{
	public function onUnsafeAttribute($name, $value)
	{
		$this->addError('criteres', "Paramètre '$name' inconnu. Voir l'aide HTML ou les spécifications OpenAPI pour la liste des paramètres.");
	}

	public function clearErrors($attribute = null)
	{
		if (!$attribute) {
			$criteria = $this->getError('criteres');
		}
		parent::clearErrors($attribute);
		if (!empty($criteria)) {
			$this->addError('criteres', $criteria);
		}
	}
}
