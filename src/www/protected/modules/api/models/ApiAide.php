<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class ApiAide extends CModel implements ApiInterface
{
	public $action;

	public function attributeNames()
	{
		return ['action'];
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['action', 'required', "message" => "Paramètre 'action' requis, valant 'titre', 'revue' ou 'revues'."],
			['action', 'validateClassName'],
		];
	}

	public function validateClassName($attrName)
	{
		$action = strtolower($this->action);
		$fileName = __DIR__ . "/Api" . ucfirst($action) . ".php";
		if (!file_exists($fileName)) {
			$this->addError($attrName, "Cette action est inconnue.");
		}
	}

	public function apply(): array
	{
		$action = strtolower($this->action);
		$class = "Api" . ucfirst($action);
		try {
			$engine = new $class;
			return ['criteres' => $engine->attributeNames()];
		} catch (Exception $_) {
			return [];
		}
	}
}
