<?php

class TitresController extends CController
{
	public function actionIndex()
	{
		$engine = new ApiTitres();
		$parameters = Api::readParameters(Yii::app()->request);
		$engine->setAttributes($parameters);
		$api = new Api($engine);
		$api->printResponse();
	}

	public function actionView()
	{
		$engine = new ApiTitre();
		$parameters = Api::readParameters(Yii::app()->request);
		$engine->setAttributes($parameters);
		$api = new Api($engine);
		$api->printResponse();
	}
}
