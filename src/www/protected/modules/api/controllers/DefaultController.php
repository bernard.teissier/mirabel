<?php

class DefaultController extends Controller
{
	/**
	 * Web service help page
	 */
	public function actionIndex()
	{
		$decQuery = Api::readParameters(Yii::app()->request, 'api');
		$result = [];
		if (empty($decQuery->action)) {
			return $this->displayHelp();
		}
		if ($decQuery->action === 'find-one' && $decQuery->model === 'Titre' && !empty($decQuery->parameters)) {
			if (strncmp($_SERVER['REMOTE_ADDR'], '193.49.39.', 10) !== 0 && strncmp($_SERVER['REMOTE_ADDR'], '127.0.0.1', 9) !== 0) {
				throw new CHttpException(403, "Accès restreint par IP.");
			}
			$titre = Titre::model()->findByAttributes((array) $decQuery->parameters);
			if ($titre) {
				$result["response"] = $titre->attributes;
			} else {
				$result["response"] = null;
				$result["error"] = "No model matches these parameters";
			}
		}

		$this->header('json');
		echo json_encode($result);
	}

	public function actionAide()
	{
		$engine = new ApiAide();
		$parameters = Api::readParameters(Yii::app()->request, 'api/revues/' . $this->action->id);
		$engine->setAttributes($parameters);
		$api = new Api($engine);
		$api->printResponse();
	}

	public function actionOpenapi()
	{
		$file = Yii::getPathOfAlias('application') . '/data/openapi.json';
		Yii::app()->getRequest()->sendFile("Mirabel-openAPI.json", file_get_contents($file));
	}

	private function displayHelp()
	{
		$file = Yii::getPathOfAlias('application') . '/data/openapi.json';
		if (!file_exists($file)) {
			throw new CHttpException(404, "La documentation de l'API est absente.");
		}
		if (!is_readable($file)) {
			throw new CHttpException(404, "La documentation de l'API n'est pas accessible.");
		}
		$api = new OpenApi(json_decode(file_get_contents($file)));

		return $this->render('aide', ['api' => $api]);
	}
}
