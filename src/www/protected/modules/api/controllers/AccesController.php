<?php

class AccesController extends CController
{
	public function actionChanges()
	{
		$engine = new ApiAccesChanges();
		$parameters = Api::readParameters(Yii::app()->request);
		$engine->setAttributes($parameters);
		$api = new Api($engine);
		$api->printResponse();
	}

	public function actionIndex()
	{
		$engine = new ApiAccesPossessions();
		$parameters = Api::readParameters(Yii::app()->request);
		$engine->setAttributes($parameters);
		$api = new Api($engine);
		$api->printResponse();
	}

	public function actionRevue()
	{
		$engine = new ApiAccesRevue();
		$parameters = Api::readParameters(Yii::app()->request);
		$engine->setAttributes($parameters);
		$api = new Api($engine);
		$api->printResponse();
	}

	public function actionTitres()
	{
		$engine = new ApiAccesTitre();
		$parameters = Api::readParameters(Yii::app()->request);
		$engine->setAttributes($parameters);
		$api = new Api($engine);
		$api->printResponse();
	}
}
