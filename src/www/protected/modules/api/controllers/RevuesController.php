<?php

class RevuesController extends CController
{
	public function actionIndex()
	{
		Api::processDefault(new ApiRevues());
	}

	public function actionView()
	{
		Api::processDefault(new ApiRevue());
	}
}
