<?php

class ApiModule extends CWebModule
{
	/**
	 * This method is called when the module is being created.
	 */
	public function init()
	{
		// use an distinct MySQL account if it exists
		$dbconfig = Yii::app()->params['api.db'];
		if ($dbconfig) {
			// merge the API specific config onto the default one
			Yii::app()->setComponent('db', $dbconfig, true);
			if (Yii::app()->db->isInitialized) {
				Yii::app()->setComponent('db', null);
			}
		}
		// import the module-level models and components
		$this->setImport(
			[
				'api.models.*',
				'api.components.*',
			]
		);
	}

	/**
	 * This method is called before any module controller action is performed.
	 *
	 * @param CController $controller
	 * @param CAction $action
	 * @return bool
	 */
	public function afterControllerAction($controller, $action)
	{
		// write an event in Matomo/Matomo, except for the HTML main page
		if ($controller->id !== 'default' && $action->id !== 'index') {
			$matomoConfig = Yii::app()->params['matomo'];
			if ($matomoConfig && !empty($matomoConfig['tokenAuth'])) {
				// after the response is ready, send it then record in Matomo
				Yii::app()->onEndRequest = function(CEvent $event) {
					flush();
					fastcgi_finish_request();
					$partenaire = isset($_GET['partenaire']) ?
						Partenaire::model()->findByAttributes(['hash' => $_GET['partenaire']])
						: null;
					ApiModule::recordInMatomo($event->sender->params['matomo'], $partenaire);
				};
			}
		}

		return parent::afterControllerAction($controller, $action);
	}

	/**
	 * This method is called before any module controller action is performed.
	 *
	 * @param CController $controller
	 * @param CAction $action
	 * @return bool
	 */
	public function beforeControllerAction($controller, $action)
	{
		if ($controller->id !== 'default' && $action->id !== 'index') {
			// Allow JS calls from external sites, except for the HTML main page
			header("Access-Control-Allow-Origin: *");
			// Do not compress the response, which would wait for the process end
			header("Content-Encoding: none");
		}
		return parent::beforeControllerAction($controller, $action);
	}

	/**
	 * Send a HTTP request to the Matomo server that records this event.
	 */
	public static function recordInMatomo(array $config, ?\Partenaire $partenaire): void
	{
		MatomoTracker::$URL = $config['rootUrl'];
		$matomoTracker = new MatomoTracker($config['siteId']);
		$matomoTracker->setTokenAuth($config['tokenAuth']);
		/*
		// force some settings if Matomo's guess is not right
		$matomoTracker->setIp($_SERVER['REMOTE_ADDR']);
		$matomoTracker->setUserAgent($_SERVER['HTTP_USER_AGENT']);
		$matomoTracker->setBrowserLanguage($_SERVER['HTTP_ACCEPT_LANGUAGE']);
		$requestUrl = "https://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']
			. (empty($_GET) ? "" : "?" . http_build_query($_GET));
		$matomoTracker->setUrl($requestUrl);
		 */
		$urlAction = preg_replace(
			'#^/api#',
			'',
			preg_replace('#\?.*$#', '', $_SERVER['REQUEST_URI'])
		);
		$action = $urlAction ?: "aide";
		$target = $partenaire ? $partenaire->getShortName() : false;
		try {
			$matomoTracker->doTrackEvent("API", $action, $target);
		} catch (\Exception $e) {
			Yii::log("API Matomo error ($action / $target): " . $e->getMessage(), CLogger::LEVEL_WARNING);
		}
	}
}
