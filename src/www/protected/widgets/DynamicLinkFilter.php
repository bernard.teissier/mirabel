<?php

namespace widgets;

class DynamicLinkFilter
{
	/**
	 * @var bool
	 */
	public $debug;

	/**
	 * @var array [{id: Sourcelien.id, nom: Sourcelien.nom}]
	 */
	private $sources = [];

	public function __construct()
	{
		$this->debug = defined('YII_DEBUG') && YII_DEBUG;
		$this->sources = $this->fetchSources();
	}

	/**
	 * @param array $ids List of Sourcelien.id (negative if excluded)
	 * @return string HTML
	 */
	public function run(array $ids): string
	{
		$this->loadJavascriptAssets();

		$data = [
			'sources' => $this->sources,
			'ids' => $this->filterIds($ids),
			'hint' => \Hint::model()->getMessage('SearchTitre', 'lien'),
		];
		$encData = json_encode($data);
		$id = "DynamicLinkFilter-" . md5($encData);
		// Call the JS init function
		\Yii::app()->clientScript->registerScript($id, "dynamicLinkFilter.init('$id', $encData);");

		// The HTML block that will be dynamically filled.
		return '<div id="' . $id . '"></div>';
	}

	private function fetchSources(): array
	{
		return array_map(
			function (array $x): array {
				return [
					'id' => (int) $x[0],
					'nom' => $x[1],
				];
			},
			\Yii::app()->db->createCommand("SELECT id, nom FROM Sourcelien ORDER BY nom")->queryAll(false)
		);

	}

	private function filterIds(array $ids): array
	{
		$result = [];
		foreach ($ids as $id) {
			if (isset($this->sources[abs($id)])) {
				$result[] = (int) $id;
			}
		}
		return $result;
	}

	private function loadJavascriptAssets()
	{
		$assets = [
			$this->debug ? 'mithril.js' : 'mithril.min.js',
			// This JS will define dynamicLinkFilter.init(htmlID, config)
			'dynamic-link-filter.js',
		];

		$am = \Yii::app()->assetManager;
		$cs = \Yii::app()->clientScript;
		foreach ($assets as $file) {
			$cs->registerScriptFile(
				$am->publish(__DIR__ . "/assets/$file")
			);
		}
	}
}
