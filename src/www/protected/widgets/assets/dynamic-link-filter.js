let dynamicLinkFilter = (function() {
	let hint = "";
	let sources = new Map();
	const state = {
		ids: new Map() // sourceId => true/include | false/exclude
	};

	const Hint = {
		oncreate: function (vnode) {
			$(vnode.dom).popover();
		},
		view: function (vnode) {
			if (!vnode.attrs.content) {
				return null;
			}
			return m(
				"a",
				{
					href: "#",
					'data-title': vnode.attrs.title,
					'data-content': vnode.attrs.content,
					'data-html': "true",
					'data-trigger': "hover",
					'data-container': 'body',
					rel: "popover"
				},
				"?"
			);
		}
	};

	const Radio = {
		view: function(vnode) {
			const value = vnode.attrs.value;
			const checked = vnode.attrs.checked;
			return m("span",
				{
					style: "padding: 3px 4px; background-color: " + (value > 0 ? "#04B45F" : "#B40404"),
					title: (value > 0 ? "Exiger" : "Exclure") + " la présence de ce lien"
				},
				m("input.radio", {
					type: "radio",
					style: "display: inline; float: none;",
					name: "SearchTitre[lien][" + (value > 0 ? value : -value)  + "]",
					value: value,
					checked: checked,
					onclick: vnode.attrs.onclick
				})
			);
		}
	};

	const Source = {
		view: function (vnode) {
			const sourceId = vnode.attrs.sourceId;
			const include = vnode.attrs.include;
			const source = sources.get(sourceId);
			if (typeof source === 'undefined') {
				return m("div.alert.alert-error", "Cet ID est inconnu : " + sourceId);
			}
			return m("div",
				m(Radio, {value: sourceId, checked: include}),
				m(Radio, {value: -sourceId, checked: !include}),
				m("label", {style: "display: inline-block; margin: 0 1ex"}, source.nom),
				m("a",
					{
						href: "#",
						onclick: function() {
							state.ids.delete(sourceId);
							return false;
						},
						title: "Retirer ce filtre sur " + source.nom
					},
					m("span.icon.icon-trash")
				)
			);
		}
	};

	const SourceDropdown = {
		view: function(vnode) {
			let options = [m("option", {value: 99999}, "...")];
			sources.forEach(function(source) {
				if (!state.ids.has(source.id)) {
					options.push(m("option", {value: source.id}, source.nom));
				}
			});
			return m("select",
				{
					id: "source-dropdown",
					style: "display: inline-block; margin: 0 1ex",
					onchange: vnode.attrs.onchange
				},
				options
			);
		}
	};

	function SourceAdd() {
		let include = null;
		let sourceId = 99999;

		function isComplete() {
			return !(sourceId === 99999 || include === null);
		}

		return {
			view: function (vnode) {
				return m("div.controls",
					m(Radio, {
						value: sourceId,
						checked: include,
						onclick: function() {
							include = true;
						}
					}),
					m(Radio, {
						value: -sourceId,
						checked: include === false,
						onclick: function() {
							include = false;
						}
					}),
					m(SourceDropdown, {
						onchange: function() {
							sourceId = parseInt(this.value);
						}
					}),
					m("button.btn",
						{
							type: "button",
							disabled: !isComplete(),
							onclick: function() {
								state.ids.set(sourceId, include);
								include = null;
								sourceId = 99999;
								document.querySelector('#source-dropdown').selectedIndex = 0;
							},
							title: (isComplete()
								? "Ajouter ce filtre aux critères de recherche"
								: "Il faut compléter le filtre avant de l'activer")
						},
						m("span.icon.icon-plus"),
					),
					m(Hint, {title: "Liens", content: hint})
				);
			}
		};
	};

	const SourceList = {
		view: function() {
			let result = [];
			state.ids.forEach(function(v, k) {
				result.push(m(Source, {sourceId: k, include: v}));
			});
			return m("div.control-group",
				m("label.control-label", {"for" : "SearchTitre_lien"}, "Autres liens"),
				m("div.controls", result),
				m(SourceAdd)
			);
		}
	};

	return {
		init: function(htmlId, data) {
			// find the root HTML element
			const root = document.getElementById(htmlId);
			if (!root) {
				alert("JS bug, root element #" + htmlId + " was not found.");
				return;
			}

			// load the data
			for (let s of data.sources) {
				sources.set(s.id, s);
			}
			for (let id of data.ids) {
				if (id > 0) {
					state.ids.set(id, true);
				} else {
					state.ids.set(-id, false);
				}
			}
			if (data.hint) {
				hint = data.hint;
			}

			m.mount(root, SourceList);
		}
	};
})();
