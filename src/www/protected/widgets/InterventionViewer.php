<?php

namespace widgets;

class InterventionViewer
{
	public static function expandKeys(array $assoc, \CActiveRecord $model)
	{
		$result = [];
		foreach ($assoc as $k => $v) {
			$m = [];
			$final = null;
			if (is_scalar($v)) {
				$record = null;
				if ($v > 0 && $k === "paysId") {
					$v = \Pays::model()->findByPk($v)->nom;
				} elseif ($v > 0 && ($k === "obsoletePar" || $k === "titreId")) {
					$record = \Titre::model()->findByPk((int) $v);
					if ($record) {
						$final = "$v ["
							. \CHtml::link($record->getFullTitle(), ['/titre/view', 'id' => $record->id])
							. "]";
					} else {
						$final = "$v [titre supprimé]";
					}
				} elseif ($v > 0 && $k === "modifPar") {
					$record = \Utilisateur::model()->findByPk((int) $v);
				} elseif ($v > 0 && preg_match('/^(\w+)Id/', $k, $m)) {
					$class = '\\' . ucfirst($m[1]);
					$record = $class::model()->findByPk((int) $v);
				} else {
					$className = '\\' . get_class($model);
					$enumName = "enum" . ucfirst($k);
					if (property_exists($className, $enumName)) {
						$enum = $className::${$enumName};
						if (is_array($enum) && isset($enum[$v])) {
							$v = $enum[$v];
						}
					}
				}
				if (substr($k, -2) === 'Id' && ($v === 0 || $v === "0")) {
					// FK auto-filled from a just created PK
					$v = '*ID créé*';
				}
				if (!isset($final)) {
					if ($record) {
						if (method_exists($record, 'getSelfLink')) {
							$final = "$v [" . $record->getSelfLink() . "]";
						} elseif (method_exists($record, 'getFullName')) {
							$final = "$v [" . $record->getFullName() . "]";
						}
					} else {
						$final = \CHtml::encode($v);
					}
				}
			} else {
				$final = $v;
			}
			$result[] = [$model->getAttributeLabel($k), $k => $final];
		}
		return $result;
	}

	public static function prettifyModelName($name)
	{
		if ($name === 'Service') {
			return "Accès en ligne";
		}
		return $name;
	}
}
