<?php

require __DIR__ . '/../../yiipath.php';
require_once YII_PATH . '/yiit.php';

$mainConfig = require dirname(__DIR__) . '/config/main.php';
$testConfig = require dirname(__DIR__) . '/config/test.php';
$config = CMap::mergeArray(
	$mainConfig,
	$testConfig
);
unset($mainConfig, $testConfig);

require_once(__DIR__ . '/WebTestCase.php');

Yii::createWebApplication($config);
