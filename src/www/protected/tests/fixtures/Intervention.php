<?php

return [
	'intervention1' => [
		'ressourceId' => null,
		'revueId' => 2,
		'titreId' => 1,
		'editeurId' => null,
		'statut' => 'attente',
		'contenuJson' => '{
"class": "InterventionDetail",
"content": [
	{
		"model": "Titre",
		"operation": "update",
		"id": 1,
		"before": {
			"titre": "Balai",
			"sigle": "Sigle1"
		},
		"after": {
			"titre": "Titre nouveau",
			"sigle": "Sigle nouveau"
		}
	},
	{
		"model": "TitreEditeur",
		"operation": "create",
		"after": {
			"titreId": 1,
			"editeurId": 3
		}
	}
]}',
		'hdateProp' => strtotime('2010-06-01 10:05:00'),
		'hdateVal' => null,
		'utilisateurIdProp' => null,
		'import' => 0,
		'ip' => '192.168.0.10',
		'email' => 'youp@la.boum',
		'commentaire' => 'Commentaire de proposition',
		'utilisateurIdVal' => null,
	],
];
