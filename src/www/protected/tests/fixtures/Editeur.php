<?php
return [
	[
		'id' => '1',
		'nom' => 'Nom1',
		'prefixe' => 'Préfixe1',
		'description' => 'Description1',
		'url' => 'http://la.bas.org/editeur1',
		'specialite' => 'Spécialité1',
		'geo' => 'Repère géo',
		'statut' => 'normal',
		'hdateVerif' => '1334080295',
	],
	[
		'id' => '2',
		'nom' => 'Nom2',
		'prefixe' => 'Préfixe2',
		'description' => 'Description2',
		'url' => 'http://la.bas.org/editeur2',
		'specialite' => 'Spécialité2',
		'geo' => '',
		'statut' => 'normal',
		'hdateVerif' => '1334600295',
	],
	[
		'id' => '3',
		'nom' => 'Nom3',
		'prefixe' => 'Préfixe3',
		'description' => 'Description3',
		'url' => 'http://la.bas.org/editeur3',
		'specialite' => 'Spécialité3',
		'statut' => 'normal',
		'hdateVerif' => '1334600895',
	],
	[
		'nom' => 'Antipodes',
		'prefixe' => '',
		'description' => '',
		'url' => '',
		'statut' => 'normal',
		'hdateVerif' => '1334600895',
	],
];
