<?php

return [
	[
		'id' => 1,
		'statut' => 'normal',
		'hdateVerif' => null,
	],
	[
		'id' => 2,
		'statut' => 'normal',
		'hdateVerif' => 123456,
	],
	[
		'id' => 3,
		'statut' => 'normal',
		'hdateVerif' => 1234567,
	],
];
