<?php

/**
 * Tests the AR class "Identification".
 */
class IdentificationTest extends CDbTestCase
{
	/** @var array FixtureName => ModelName */
	public $fixtures = [
		'revues' => 'Revue',
		'titres' => 'Titre',
		'ressources' => 'Ressource',
	];

	public function testCreateOk()
	{
		$i = new Identification();
		$i->attributes = [
			'titreId' => 1,
			'ressourceId' => 1,
			'bimpe' => '12345',
			'issne' => '0395-2649',
			'idInterne' => 'FNARS',
		];
		$this->assertTrue($i->validate(), print_r($i->errors, true));
		$this->assertTrue($i->save());
		$this->assertGreaterThan(0, $i->id);
		$this->assertTrue($i->delete());
	}

	public function testCreateIssneEqualsIssn()
	{
		$i = new Identification();
		$i->attributes = [
			'titreId' => 1,
			'ressourceId' => 1,
			'bimpe' => '12345',
			'issne' => '2112-2679',
			'idInterne' => 'FNARS',
		];
		$this->assertFalse($i->validate());
	}

	public function testCreateConflictIssn()
	{
		$i = new Identification();
		$i->attributes = [
			'titreId' => 1,
			'ressourceId' => 1,
			'bimpe' => '',
			'issne' => '1660-7880',
			'idInterne' => 'FNARS',
		];
		$this->assertFalse($i->validate(), print_r($i->errors, true));
		$this->assertEquals(['issne'], array_keys($i->getErrors()));
	}

	public function testCreateConflictIssne()
	{
		$i = new Identification();
		$i->attributes = [
			'titreId' => 1,
			'ressourceId' => 1,
			'issne' => '0395-2649',
		];
		$this->assertTrue($i->save());

		$i = new Identification();
		$i->attributes = [
			'titreId' => 2,
			'ressourceId' => 1,
			'issne' => '0395-2649',
		];
		$this->assertTrue($i->validate(), "Same journal, so identical ISSN-E are accepted");

		$i = new Identification();
		$i->attributes = [
			'titreId' => 3,
			'ressourceId' => 1,
			'issne' => '0395-2649',
		];
		$this->assertFalse($i->validate(), "Other journal, so identical ISSN-E are rejected");
		$this->assertEquals(['issne'], array_keys($i->getErrors()));
	}
}
