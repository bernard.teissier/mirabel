<?php

Yii::import('application.models.import.ImportCairn');

/**
 * Tests the AR class "ImportCairn".
 */
class ImportCairnTest extends CDbTestCase
{
	/** @var array FixtureName => ModelName */
	public $fixtures = [
		'ressources' => 'Ressource',
		'revues' => 'Revue',
		'titres' => 'Titre',
		'editeurs' => 'Editeur',
		'titresEditeurs' => ':Titre_Editeur',
		'partenaires' => 'Partenaire',
		'collections' => 'Collection',
		'identifications' => 'Identification',
	];

	/**
	 * @dataProvider provideCsvLines
	 */
	public function testExtractTitleData($expected, $input)
	{
		$import = new ImportCairn();
		$import->time = mktime(0, 0, 0, 7, 15, 2012);
		$this->assertEquals($expected['titleData'], $import->extractTitleData($input));
	}

	/**
	 * @dataProvider provideCsvLines
	 */
	public function testExtractCollections($expected, $input)
	{
		$import = new ImportCairn();
		$import->time = mktime(0, 0, 0, 7, 15, 2012);
		$this->assertEquals($expected['collections'], $import->extractCollections($input));
	}

	/**
	 * @dataProvider provideCsvLines
	 */
	public function testExtractServicesData($expected, $input)
	{
		$import = new ImportCairn();
		$import->time = mktime(0, 0, 0, 7, 15, 2012);
		$this->assertEquals($expected['services'], $import->extractServicesData($input));
	}

	public function provideCsvLines()
	{
		return [
			[ // parameters of the first call
				[
					'titleData' => [
						'titre' => "A contrario",
						'editeur' => "Antipodes",
						'issn' => "1660-7880",
						'idInterne' => 'revue-a-contrario',
						'urlLocal' => 'http://www.cairn.info/revue-a-contrario.htm',
					],
					'services' => [
						[
							'type' => 'Intégral',
							'acces' => 'Libre',
							'lacunaire' => 0,
							'selection' => 0,
							'derNumUrl' => '',
							'url' => "http://www.cairn.info/revue-a-contrario.htm",
							'volDebut' => 1,
							'noDebut' => null,
							'numeroDebut' => 'Vol. 1',
							'volFin' => null,
							'noFin' => null,
							'numeroFin' => '',
							'dateBarrDebut' => '2003',
							'dateBarrFin' => '2007',
							'dateBarrInfo' => 'Barrière mobile de 4 ans',
						],
						[
							'type' => 'Intégral',
							'acces' => 'Restreint',
							'lacunaire' => 0,
							'selection' => 0,
							'derNumUrl' => '',
							'url' => "http://www.cairn.info/revue-a-contrario.htm",
							'volDebut' => null,
							'noDebut' => null,
							'numeroDebut' => '',
							'volFin' => 244,
							'noFin' => null,
							'numeroFin' => 'Vol. CCXLIV',
							'dateBarrDebut' => '2008',
							'dateBarrFin' => '2009',
							'dateBarrInfo' => 'Barrière mobile de 4 ans',
						],
					],
					'collections' => [1, 3],
				],
				[
					"A contrario", "Antipodes", "1660-7880", "", "2 nos par an",
					"http://www.cairn.info/revue-a-contrario.htm",
					"2003/1, Vol. 1", "2009/2, Vol. CCXLIV", "4 ans", "X", "", "X", "", "", "", "", "", "", "",
				],
			],
			[
				[
					'titleData' => [
						'titre' => "Politix",
						'editeur' => "De Boeck Université",
						'issn' => "0295-2319",
						'idInterne' => 'revue-politix',
						'urlLocal' => 'http://www.cairn.info/revue-politix.htm',
					],
					'services' => [
						[
							'type' => 'Intégral',
							'acces' => 'Libre',
							'lacunaire' => 0,
							'selection' => 0,
							'derNumUrl' => '',
							'url' => "http://www.cairn.info/revue-politix.htm",
							'volDebut' => null,
							'noDebut' => 69,
							'numeroDebut' => 'no 69',
							'volFin' => null,
							'noFin' => 85,
							'numeroFin' => 'no 85',
							'dateBarrDebut' => '2005',
							'dateBarrFin' => '2009',
							'dateBarrInfo' => '',
						],
					],
					'collections' => [3],
				],
				[
					"Politix", "De Boeck Université", "0295-2319", "", "4 nos :an",
					"http://www.cairn.info/revue-politix.htm",
					"2005/1, n° 69", "2009/1, n° 85", "accès libre intégral", "", "", "X", "", "", "", "", "", "", "",
				],
			],
			[
				[
					'titleData' => [
						'titre' => "Agora débats/jeunesses",
						'editeur' => "Presses de Sciences Po",
						'issn' => "1268-5666",
						'idInterne' => 'revue-agora-debats-jeunesses',
						'urlLocal' => 'http://www.cairn.info/revue-agora-debats-jeunesses.htm',
					],
					'services' => [
						[
							'type' => 'Intégral',
							'acces' => 'Restreint',
							'lacunaire' => 0,
							'selection' => 0,
							'derNumUrl' => '',
							'url' => "http://www.cairn.info/revue-agora-debats-jeunesses.htm",
							'volDebut' => null,
							'noDebut' => null,
							'numeroDebut' => '',
							'volFin' => 4,
							'noFin' => 61,
							'numeroFin' => 'Tome 4, no 61',
							'dateBarrDebut' => '2009',
							'dateBarrFin' => '2012',
							'dateBarrInfo' => 'Barrière mobile de 3 ans',
						],
					],
					'collections' => [1, 3, 5],
				],
				explode(';', 'Agora débats/jeunesses;Presses de Sciences Po;1268-5666;;3 nos par an;http://www.cairn.info/revue-agora-debats-jeunesses.htm;2009/1, N° 51;2012/2, Tome 4, N° 61;3 ans;X;;X;;X;;;;;'),
			],
		];
	}

	/**
	 * @dataProvider provideTitles
	 */
	public function testIdentifyTitle($expectedType, $attrs)
	{
		$import = new ImportCairn();
		$titleIdentifier = new TitleIdentifier($import->log);
		$titleIdentifier->setRessource($import->getRessource());
		$title = $titleIdentifier->identify($attrs);
		$this->assertInternalType($expectedType, $title);
	}

	public function provideTitles()
	{
		return [
			[
				'null',
				[
					'titre' => "A contrario",
					'editeur' => "Antipodes",
				],
			],
			[
				'object',
				[
					'titre' => "A contrario. Revue interdisciplinaire de sciences sociales",
					'editeur' => "Antipodes",
				],
			],
			[
				'object',
				[
					'titre' => "A contrario",
					'idInterne' => 'ACON',
				],
			],
			[
				'object',
				[
					'titre' => "A contrario",
					'issne' => '0395-2037',
				],
			],
			[
				'object',
				['issn' => "1660-7880"],
			],
			[
				'null',
				[
					'titre' => "plouf",
					'editeur' => "Antipodes",
				],
			],
		];
	}

	public function testImportAllSuccess()
	{
		Yii::app()->db->createCommand("DELETE FROM Titre_Collection")->execute();
		Yii::app()->db->createCommand("DELETE FROM Identification WHERE id > 1")->execute();
		$import = new ImportCairn(['url' => dirname(__DIR__) . '/data/import_cairn.csv']);
		$this->assertEquals('info', $import->importAll(), "LOG: " . $import->log->format('string'));
		$logs = $import->log->format('array');
		$this->assertCount(2, $logs, print_r($logs, true));
		$this->assertEquals('info', $logs[0]['level'], print_r($logs, true));
		$interventions = $import->getInterventions();
		$this->assertCount(3, $interventions);
		$this->assertEquals(
			"Création d'accès en ligne, revue ",
			substr($interventions[0]->description, 0, 35)
		);
		$this->assertEquals(
			'Création de la revue « Approche Centrée sur la Personne. Pratique et recherche »',
			$interventions[1]->description
		);
		$this->assertEquals(
			"Création de l'éditeur ",
			substr($interventions[2]->description, 0, 24)
		);
		// collections
		$countSql = "SELECT count(*) FROM Titre_Collection WHERE titreId = 4";
		$this->assertEquals('2', Yii::app()->db->createCommand($countSql)->queryScalar());
		// éditeurs
		$i = $interventions[2]->contenuJson->toArray();
		$this->assertEquals('Editeur', $i[0]['model'], "New editor expected " . print_r($i, true));
	}

	public function testImportAllFileNotFound()
	{
		Yii::app()->db->createCommand("DELETE FROM Titre_Collection")->execute();
		try {
			$import = new ImportCairn(['url' =>dirname(__DIR__) . '/data/import_nothing.csv']);
		} catch (Exception $e) {
			return;
		}
		$this->fail('An expected exception has not been raised.');
	}

	public function testExtractServicesDataDates()
	{
		$import = new ImportCairn();
		$import->time = mktime(0, 0, 0, 7, 15, 2012);
		$services = $import->extractServicesData(
			explode(';', 'Chimères;ERES;0986-6035;;2 nos par an;http://www.cairn.info/revue-chimeres.htm;2012/1, N° 76;2012/1, N° 76;4 ans;X;X;;;;X;;;')
		);
		$this->assertCount(1, $services);
		$this->assertEquals('Restreint', $services[0]['acces']);
		$this->assertEquals('2012', $services[0]['dateBarrDebut']);
		$this->assertEquals('2012', $services[0]['dateBarrFin']);
	}
}
