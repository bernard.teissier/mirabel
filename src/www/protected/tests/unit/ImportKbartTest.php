<?php

Yii::import('application.models.import.ImportKbart');

/**
 * Tests the AR class "ImportKbart".
 */
class ImportKbartTest extends CDbTestCase
{
	/** @var array FixtureName => ModelName */
	public $fixtures = [
		'ressources' => 'Ressource',
		'revues' => 'Revue',
		'titres' => 'Titre',
		'editeurs' => 'Editeur',
		'titresEditeurs' => ':Titre_Editeur',
		'partenaires' => 'Partenaire',
		'identifications' => 'Identification',
		'services' => 'Service',
		'collections' => 'Collection',
	];

	protected $importParams = [
		'ressourceId' => 1,
		'name' => 'Cairn',
		'type' => 'Intégral',
		'lacunaire' => 1,
		'selection' => 0,
		'url' => '',
	];

	public function testImportAllSuccess()
	{
		$params = $this->importParams;
		$params['url'] = dirname(__DIR__) . '/data/import_KBART-1.txt';
		$import = new ImportKbart($params);
		$import->time = mktime(0, 0, 0, 7, 15, 2013);

		$this->assertEquals('info', $import->importAll(), $import->log->format('string'));
		$interventions = $import->getInterventions();
		$this->assertCount(3, $interventions, $interventions[0]->description . "\n");

		// logs
		$logs = $import->log->format('array');
		$this->assertCount(3, $logs, $import->log->format('string'));
		$this->assertEquals('Abstracta Iranica', $logs[0]['title'], print_r($logs[1], true));
		$this->assertEquals('procès', $logs[1]['title'], print_r($logs[0], true));
		$dom = new DOMDocument();
		$html = '<div>' . $import->log->format('html') . '</div>';
		$this->assertTrue($dom->loadXml($html), $html, "HTML log are not valid HTML.");
		unset($dom);
		$this->assertEquals('info', $logs[0]['level'], print_r($logs[0], true));
		$this->assertEquals('warning', $logs[1]['level'], print_r($logs[1], true));

		// revues + services
		/*
		echo "\n**********************************************\n";
		foreach ($interventions as $i) {
			print_r($i->contenuJson->toArray());
			echo "\n*****\n";
		}
		 */
		$i = $interventions[0]->contenuJson->toArray();
		$this->assertCount(1, $i, "1 new service expected: " . print_r($i, true));
		$this->assertEquals('Service', $i[0]['model'], "New Service expected: " . print_r($i, true));
		$this->assertEquals('2012', $i[0]['after']['dateBarrFin']);
		$i = $interventions[1]->contenuJson->toArray();
		$this->assertEquals('Revue', $i[0]['model'], "New Revue expected: " . print_r($i, true));
		$this->assertEquals('procès', $i[1]['after']['titre'], "Wrong title: " . print_r($i, true));
		$this->assertCount(4, $i, "Titre+Revue+Identification+TitreEditeur expected, got: " . print_r($i, true));
		$i = $interventions[2]->contenuJson->toArray();
		$this->assertCount(1, $i, "1 new service expected: " . print_r($i, true));
		$this->assertEquals('Service', $i[0]['model'], "New Service expected: " . print_r($i, true));
		$this->assertEquals('2011-10-01', $i[0]['after']['dateBarrFin']);

		unset($import);
	}

	public function testImportCompositeEmbargo()
	{
		$params = $this->importParams;
		$params['url'] = dirname(__DIR__) . '/data/import_KBART-2.txt';
		$import = new ImportKbart($params);
		$import->time = mktime(0, 0, 0, 7, 15, 2013);

		$this->assertEquals('info', $import->importAll(), $import->log->format('string'));
		$interventions = $import->getInterventions();
		$this->assertCount(2, $interventions, $interventions[0]->description . "\n");

		// logs
		$logs = $import->log->format('array');
		$this->assertCount(2, $logs, $import->log->format('string'));
		$this->assertContains("La date de fin d'accès 2013-05-20 est postérieure à la date barrière", $logs[1]['msg']);

		$i = $interventions[0]->contenuJson->toArray();
		$this->assertCount(1, $i, "1 new service expected: " . print_r($i, true));
		$this->assertEquals('Service', $i[0]['model'], "New Service expected: " . print_r($i, true));
		$this->assertEquals('2004', $i[0]['after']['dateBarrDebut']);
		$this->assertEquals('2013-06-15', $i[0]['after']['dateBarrFin']);
		$i = $interventions[1]->contenuJson->toArray();
		$this->assertCount(1, $i, "1 new service expected: " . print_r($i, true));
		$this->assertEquals('Service', $i[0]['model'], "New Service expected: " . print_r($i, true));
		$this->assertEquals('1973-02-01', $i[0]['after']['dateBarrDebut']);
		$this->assertEquals('2012', $i[0]['after']['dateBarrFin']);

		unset($import);
	}
}
