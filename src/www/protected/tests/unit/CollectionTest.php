<?php

class CollectionTest extends CDbTestCase
{
	/** @var array FixtureName => ModelName */
	public $fixtures = [
		'ressources' => 'Ressource',
		'collections' => 'Collection',
	];

	public function testCreateOk()
	{
		$c = new Collection();
		$c->attributes = [
			'ressourceId' => '1',
			'nom' => 'Timbres',
		];
		$this->assertTrue($c->validate(), "Validation failed: " . print_r($c->errors, true));
		$this->assertTrue($c->save(false), "Saving failed.");
		$this->assertGreaterThan(0, $c->id);
	}

	public function testGetFullName()
	{
		$this->assertEquals('Cairn — Bouquet Général', $this->collections(0)->getFullName());
		$this->assertEquals('Cairn — Bouquet Psychologie', $this->collections(1)->getFullName());
	}

	public function testSearchByName()
	{
		$c = new Collection('search');
		$c->attributes = [
			'nom' => 'social',
		];
		$this->assertCount(3, $c->search()->getData());
	}

	public function testSearchByRessource()
	{
		$c = new Collection('search');
		$c->attributes = [
			'ressourceId' => '1',
		];
		$this->assertCount(2, $c->search()->getData());
		$c->attributes = [
			'ressourceId' => '2',
		];
		$this->assertCount(11, $c->search()->getData());
		$c->attributes = [
			'ressourceId' => '3',
		];
		$this->assertCount(0, $c->search()->getData());
	}
}
