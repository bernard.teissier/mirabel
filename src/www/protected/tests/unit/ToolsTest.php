<?php

/**
 * Tests the static methods of the class "Tools".
 */
class ToolsTest extends CTestCase
{
	/** @var array FixtureName => ModelName */
	public $fixtures = [
		'titres' => 'Titre',
	];

	public function testSqlToPairs()
	{
		$this->assertEquals(
			['3' => 'Alphabet', '1' => 'Balai', '2' => 'Virgule'],
			Tools::sqlToPairs("SELECT id, titre FROM Titre WHERE id < 4 ORDER BY titre")
		);
	}

	/**
	 * @dataProvider providesIpAndMasks
	 */
	public function testMatchIpMask($ip, $maskIp, $maskBits, $expected)
	{
		$this->assertEquals($expected, Tools::matchIpMask($ip, $maskIp, $maskBits));
	}

	public function providesIpAndMasks()
	{
		return [
			['127.0.0.1', '127.0.0.1', 32, true],
			['127.0.0.1', '127.0.0.10', 24, true],
			['192.168.0.10', '127.0.0.1', 32, false],
			['192.168.0.3', '192.168.0.1', 32, false],
			['192.168.0.3', 3232235524, 32, false],
			['192.168.0.2', '192.168.0.3', 31, true],
			['192.168.0.2', '3232235523', 31, true],
		];
	}

	/**
	 * @dataProvider providesIpAndFilters
	 */
	public function testMatchIpFilter($ip, $filter, $expected)
	{
		$this->assertEquals($expected, Tools::matchIpFilter($ip, $filter));
	}

	public function providesIpAndFilters()
	{
		return [
			['127.0.0.1', '127.0.0.1', true],
			['127.0.0.10', '127.0.0.1', false],
			['127.0.0.10', '127.0.0.*', true],
			['127.0.0.10', '127.0.0.', true],
			['127.0.0.1', '192.168.0.10', false],
			['192.168.0.1', '192.168.0.3', false],
			['192.168.0.3', '127.0.0.0/24, 192.168.0.0/30', true],
			['192.168.0.5', '192.168.0.1-60', true],
			['192.168.0.65', '192.168.0.1-60', false],
			['192.168.6.1', '192.168.5-10.', true],
			['192.168.10.250', '192.168.5-10.', true],
			['192.168.1.10', '192.168.5-10.', false],
		];
	}
}
