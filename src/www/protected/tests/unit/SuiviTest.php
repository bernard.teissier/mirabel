<?php

/**
 * Tests the AR class "Suivi".
 */
class SuiviTest extends CDbTestCase
{
	/** @var array FixtureName => ModelName */
	public $fixtures = [
		'suivis' => 'Suivi',
		'titres' => 'Titre',
		'service' => 'Service',
	];

	public function testFind()
	{
		$suivi = Suivi::model()->findByPk(1);
		$this->assertTrue(is_object($suivi));
		$this->assertEquals("Suivi", get_class($suivi));
		$this->assertEquals("Revue", $suivi->cible);
		$this->assertEquals(1, $suivi->cibleId);
		$this->assertEquals(1, $suivi->partenaireId);
	}

	public function testCreateOk()
	{
		$suivi = new Suivi();
		$suivi->attributes = [
			'cible' => 'Revue',
			'cibleId' => 2,
			'partenaireId' => 1,
		];
		$this->assertTrue($suivi->validate());
		$this->assertTrue($suivi->save(false));
	}

	public function testCreateError()
	{
		$suivi = new Suivi();
		$suivi = new Suivi();
		$suivi->attributes = [
			'cible' => 'Machin',
			'cibleId' => "deux",
		];
		$this->assertFalse($suivi->validate());
		$this->assertEquals(3, count($suivi->getErrors()), print_r($suivi->getErrors(), true));
	}

	public function testCheckDirectAccessOwn()
	{
		$userSuivi = [
			'Revue' => [1],
		];
		$this->assertTrue(Suivi::checkDirectAccess($this->titres(0), $userSuivi));
	}

	public function testCheckDirectAccessOther()
	{
		$userSuivi = [
			'Revue' => [2],
		];
		$this->assertFalse(Suivi::checkDirectAccess($this->titres(0), $userSuivi));
	}

	public function testCheckDirectAccessNobody()
	{
		$userSuivi = [
			'Revue' => [1],
		];
		$this->assertTrue(Suivi::checkDirectAccess($this->titres(3), $userSuivi));
	}
}
