<?php

class LinkCheckerTest extends CTestCase
{
	/**
	 * @dataProvider provideUrl
	 */
	public function testCheck($urls, $expected)
	{
		$checks = LinkChecker::check($urls);
		foreach ($expected as $url => $status) {
			$this->assertArrayHasKey($url, $checks, "URL $url is not in the results.");
			$this->assertEquals($status, $checks[$url], "URL $url does not has the expected status.");
		}
	}

	public function provideUrl()
	{
		return [
			[
				['http://www.google.fr'],
				['http://www.google.fr' => true],
			],
			[
				['http://www.google.fr', 'http://free.fr/xyz'],
				['http://www.google.fr' => true, 'http://free.fr/xyz' => false],
			],
			[
				['http://telemme.mmsh.univ-aix.fr/publications/revues.aspx#Rives'],
				['http://telemme.mmsh.univ-aix.fr/publications/revues.aspx#Rives' => true],
			],
			[
				['http://adosen-sante.com/spip.php?rubrique14#x', 'http://adosen-sante.com/spip.php?rubrique14'],
				['http://adosen-sante.com/spip.php?rubrique14#x' => true, 'http://adosen-sante.com/spip.php?rubrique14' => true],
			],
		];
	}
}
