<?php

Yii::import('application.models.import.ImportPersee');

/**
 * Tests the AR class "ImportPersee".
 */
class ImportPerseeTest extends CDbTestCase
{
	/** @var array FixtureName => ModelName */
	public $fixtures = [
		'ressources' => 'Ressource',
		'revues' => 'Revue',
		'titres' => 'Titre',
		'editeurs' => 'Editeur',
		'titresEditeurs' => ':Titre_Editeur',
		'partenaires' => 'Partenaire',
		'collections' => 'Collection',
		'identifications' => 'Identification',
	];

	/**
	 * @dataProvider provideCsvLines
	 */
	public function testExtractTitleData($expected, $input)
	{
		$import = new ImportPersee(['url' => dirname(__DIR__) . '/data/import_persee.csv']);
		$this->assertEquals($expected['titleData'], $import->extractTitleData($input));
	}

	/**
	 * @dataProvider provideCsvLines
	 */
	public function testExtractServicesData($expected, $input)
	{
		$import = new ImportPersee(['url' => dirname(__DIR__) . '/data/import_persee.csv']);
		$this->assertEquals($expected['services'], $import->extractServicesData($input));
	}

	public function provideCsvLines()
	{
		return [
			[ // parameters of the first call
				[
					'titleData' => [
						'titre' => "Actes de la Recherche en Sciences Sociales",
						'editeur' => "Maison des sciences de l'homme",
						'issn' => "0335-5322",
						'issne' => '1955-2564',
						'idInterne' => 'arss',
					],
					'services' => [
						[
							'type' => 'Intégral',
							'acces' => 'Libre',
							'lacunaire' => 0,
							'selection' => 0,
							'url' => "http://www.persee.fr/web/revues/home/prescript/revue/arss",
							'alerteRssUrl' => "http://www.persee.fr/c/ext/prescript/rss?revueKey=arss",
							'numeroDebut' => 'Vol. 1, n° 1',
							'numeroFin' => 'Vol. 150',
							'dateBarrDebut' => '1975-01',
							'dateBarrFin' => '2003-12',
							'dateBarrInfo' => '',
						],
					],
				],
				[
					"Actes de la Recherche en Sciences Sociales", "arss", "Maison des sciences de l'homme",
					"0335-5322 : Actes de la recherche en sciences sociales", "1955-2564",
					"http://www.persee.fr/web/revues/home/prescript/revue/arss", "1975-2003",
					"Actes de la recherche en sciences sociales. Vol. 1, n°1, janvier 1975. Hiérarchie sociale des objets.",
					"Actes de la recherche en sciences sociales. Vol. 150, décembre 2003. Regards croisés sur l'anthropologie de Pierre Bourdieu.",
					"http://www.persee.fr/c/ext/prescript/rss?revueKey=arss",
					"Résumés eng : 612 (1975-2003)|Résumés fre : 600 (1976-2003)|Résumés ger : 592 (1975-2003)|Résumés spa : 248 (1987-2003)",
					"",
				],
			],
			[ // parameters of the first call
				[
					'titleData' => [
						'titre' => "Mots. Les langages du politique",
						'editeur' => "ENS Editions",
						'issn' => "0243-6450",
						'issne' => '1960-6001',
						'idInterne' => 'mots',
					],
					'services' => [
						[
							'type' => 'Intégral',
							'acces' => 'Libre',
							'lacunaire' => 0,
							'selection' => 0,
							'url' => "http://www.persee.fr/web/revues/home/prescript/revue/mots",
							'alerteRssUrl' => "http://www.persee.fr/c/ext/prescript/rss?revueKey=mots",
							'numeroDebut' => 'n° 1',
							'numeroFin' => '',
							'dateBarrDebut' => '1980-10',
							'dateBarrFin' => '2002',
							'dateBarrInfo' => '',
						],
					],
				],
				explode(
					';',
					'Mots. Les langages du politique;mots;ENS Editions;0243-6450 : Mots|0294-796X :'
				. ' Travaux de lexicométrie et lexicologie politique;1960-6001;http://www.persee.fr/web/'
				. 'revues/home/prescript/revue/mots;1980-2002;Mots, octobre 1980, N°1. Saussure, Zipf, Lagado,'
				. ' des méthodes, des calculs, des doutes et le vocabulaire de quelques textes politiques.;Mots,'
				. 'Index général : janvier 1980 - décembre 2001.;http://www.persee.fr/c/ext/prescript/rss?'
				. 'revueKey=mots;Résumés eng : 382 (1980-2001)|Résumés fre : 387 (1980-2001)|Résumés spa : 256 (1989-2001);'
				),
			],
		];
	}

	public function testImportAllSuccess()
	{
		$import = new ImportPersee(['url' => dirname(__DIR__) . '/data/import_persee.csv']);
		$this->assertEquals('info', $import->importAll(), $import->log->format('string'));
		$logs = $import->log->format('array');
		$this->assertCount(2, $logs);
		$interventions = $import->getInterventions();
		$this->assertCount(4, $interventions);
		// revues
		$this->assertEquals('warning', $logs[0]['level'], print_r($logs, true));
		$this->assertEquals('warning', $logs[1]['level']);
		$i = $interventions[0]->contenuJson->toArray();
		$this->assertEquals('Revue', $i[0]['model'], "New Revue expected " . print_r($i, true));
		$i = $interventions[2]->contenuJson->toArray();
		$this->assertEquals('Revue', $i[0]['model'], "New Revue expected " . print_r($i, true));
		// éditeurs
		$i = $interventions[1]->contenuJson->toArray();
		$this->assertEquals('Editeur', $i[0]['model'], "New editor expected " . print_r($i, true));
		$i = $interventions[3]->contenuJson->toArray();
		$this->assertEquals('Editeur', $i[0]['model'], "New editor expected " . print_r($i, true));
	}
}
