<?php

Yii::import('application.models.ExportServices');

/**
 * Tests the AR class "ExportServices".
 */
class ExportServicesTest extends CDbTestCase
{
	/** @var array FixtureName => ModelName */
	public $fixtures = [
		'ressources' => 'Ressource',
		'revues' => 'Revue',
		'titres' => 'Titre',
		'editeurs' => 'Editeur',
		'titresEditeurs' => ':Titre_Editeur',
		'partenaires' => 'Partenaire',
		'identifications' => 'Identification',
		'services' => 'Service',
		'collections' => 'Collection',
	];

	public function testExportToKbartSimple()
	{
		$serviceFree = new Service();
		$serviceFree->attributes = [
			'ressourceId' => 2, // Cairn
			'titreId' => 3, // L'Alphabet
			'type' => 'Intégral',
			'accès' => 'Libre',
			'url' => 'http://nulle.part.com/',
			'alerteRssUrl' => '',
			'alerteMailUrl' => '',
			'derNumUrl' => '',
			'langues' => 'Fre',
			'numeroDebut' => 'vol.1 no.1',
			'noDebut' => '1',
			'volDebut' => '1',
			'numeroFin' => '',
			'dateBarrDebut' => '1990-03',
			'dateBarrFin' => '',
			'dateBarrInfo' => '',
			'hdateModif' => '3721642',
			'statut' => 'normal',
		];
		$serviceFree->save(false);

		$filter = new Service('filter');
		$revuesIds = [2];
		$export = new ExportServices($revuesIds, $filter);
		$kbart = $export->exportToKbart();
		$this->assertNotEmpty($kbart);
		$lines = explode("\n", trim($kbart));
		$this->assertCount(2, $lines);
		$this->assertEquals(
			[
				'Alphabet', '', '', '1990-03', '1', '1', '', '', '', 'http://nulle.part.com/', '', '',
				'', 'fulltext', '', '',
				'', 'Cairn', '', 'Libre', '', '3',
			],
			str_getcsv($lines[1], "\t")
		);
	}

	public function testExportToKbartWithComputedEmbargo()
	{
		$serviceFree = new Service();
		$serviceFree->attributes = [
			'ressourceId' => 2, // Cairn
			'titreId' => 3, // L'Alphabet
			'type' => 'Intégral',
			'accès' => 'Libre',
			'url' => 'http://nulle.part.com/',
			'alerteRssUrl' => '',
			'alerteMailUrl' => '',
			'derNumUrl' => '',
			'langues' => 'Fre',
			'numeroDebut' => 'vol.1 no.1',
			'noDebut' => '1',
			'volDebut' => '1',
			'numeroFin' => 'vol.13 no.130',
			'noFin' => '130',
			'volFin' => '13',
			'dateBarrDebut' => '1990-03',
			'dateBarrFin' => '2010',
			'dateBarrInfo' => '',
			'hdateModif' => '3721642',
			'statut' => 'normal',
		];
		$serviceFree->save(false);
		$serviceRes = new Service();
		$serviceRes->attributes = $serviceFree->attributes;
		$serviceRes->setAttribute('accès', 'Restreint');
		$serviceRes->setAttribute('numeroDebut', 'vol.11 no.101');
		$serviceRes->setAttribute('numeroFin', 'vol.13 no.130');
		$serviceRes->setAttribute('dateBarrDebut', '2011-01');
		$serviceRes->setAttribute('dateBarrFin', '2013-06');
		$serviceRes->save(false);

		$filter = new Service('filter');
		$filter->acces = 'Libre';
		$revuesIds = [2];
		$export = new ExportServices($revuesIds, $filter);
		$export->time = mktime(12, 0, 0, 07, 01, 2013);
		$kbart = $export->exportToKbart();
		$this->assertNotEmpty($kbart);
		$lines = explode("\n", trim($kbart));
		$this->assertCount(2, $lines);
		$this->assertEquals(
			[
				"Alphabet", '', '', '1990-03', '1', '1', '2013-06', '13', '130', 'http://nulle.part.com/', '', '',
				'P3Y', 'fulltext', '', '',
				'', 'Cairn', '', 'Mixte', '', '3',
			],
			str_getcsv($lines[1], "\t")
		);
	}

	public function testExportToKbart()
	{
		$filter = new Service('filter');
		$revuesIds = [1];
		$export = new ExportServices($revuesIds, $filter);
		$kbart = $export->exportToKbart();
		$this->assertNotEmpty($kbart);
		$lines = explode("\n", trim($kbart));
		$this->assertCount(2, $lines);
		$this->assertEquals(
			[
				'Virgule', '', '', '1998-03', '1', '3', '', '6', '8', 'http://la.bas.com/', '', '',
				'', 'abstracts', 'Sommaire', 'Préfixe1Nom1',
				'', 'Les Rêveurs de rune', 'Bouquet Psychologie', 'Mixte', '', '2',
			],
			str_getcsv($lines[1], "\t")
		);
	}

	public function testExportToCsv()
	{
		$filter = new Service('filter');
		$revuesIds = [1];
		$export = new ExportServices($revuesIds, $filter);
		$csv = $export->exportToCsv();
		$this->assertNotEmpty($csv);
		$lines = explode("\n", trim($csv));
		$this->assertCount(3, $lines);
		//	'titre', 'ISSN', 'URL Mir@bel', 'id de la revue', 'URL du service', 'ressource', 'type de service', "type d'accès",
		//	'date barrière de début', 'début', 'début (vol)', 'début (numéro)',
		//	'date barrière de fin', 'fin', 'fin (vol)', 'fin (numéro)',
		//	'id', 'dernière modification', 'description'
		$this->assertEquals(
			[
				'Virgule', '', '', '1', 'http://la.bas.com/', 'Les Rêveurs de rune', 'Sommaire', 'Libre',
				'1998-03', 'vol.1 no.3', '1', '3', '2010-01', 'vol.6 no.8', '6', '8',
				'1', '1970-02-13', '',
			],
			str_getcsv($lines[1], ";")
		);
	}
}
