<?php

/**
 * Tests the AR class "Editeur".
 */
class EditeurTest extends CDbTestCase
{
	/** @var array FixtureName => ModelName */
	public $fixtures = [
		'editeurs' => 'Editeur',
		'titres' => 'Titre',
		'titresEditeurs' => ':Titre_Editeur', // table sans modèle
	];

	public function testFind()
	{
		$editeur = Editeur::model()->findByPk(1);
		$this->assertTrue(is_object($editeur));
		$this->assertEquals("Editeur", get_class($editeur));
		$this->assertEquals("normal", $editeur->statut);
		$this->assertEquals("Préfixe1Nom1", $editeur->fullName);
	}

	public function testCreateOk()
	{
		$editeur = new Editeur();
		$editeur->attributes = [
			'nom' => 'Alembert',
			'prefixe' => "D' ",
			'statut' => 'normal',
		];
		$editeur->confirm = true;
		$this->assertTrue($editeur->validate());
		$this->assertTrue($editeur->save(false));
		$this->assertEquals($_SERVER['REQUEST_TIME'], $editeur->hdateModif);
		$this->assertGreaterThan(0, $editeur->id);
		$this->assertEquals("D'Alembert", $editeur->getFullName());
	}

	public function testCreateError()
	{
		$editeur = new Editeur();
		$editeur->attributes = [
			'nom' => 'nom1',
			'statut' => 'bizarre',
			'url' => 'zou',
			'prefixe' => '1234567890 1234567890 1234567890',
		];
		$editeur->confirm = true;
		$this->assertFalse($editeur->save());
		$this->assertEquals(3, count($editeur->getErrors()));
		return $editeur;
	}

	public function testDelete()
	{
		$this->assertFalse($this->editeurs(0)->delete());
		$this->assertTrue($this->editeurs(2)->delete());
		$editeur = new Editeur();
		$this->setExpectedException('CDbException');
		$editeur->delete();
	}

	public function testcompleteTerm()
	{
		$this->markTestSkipped("Sphinx server required");
		$this->assertEquals(
			[['id' => 1, 'value' => 'Nom1', 'label' => 'Préfixe1Nom1']],
			Editeur::completeTerm('no', [2, 3])
		);
		$this->assertEquals(
			[],
			Editeur::completeTerm('%n')
		);
	}

	public function testCountRevues()
	{
		$this->assertEquals(1, $this->editeurs(0)->countRevues());
		$this->assertEquals(1, $this->editeurs(1)->countRevues());
		$this->assertEquals(0, $this->editeurs(2)->countRevues());
	}

	public function testScopeOrder()
	{
		$editors = Editeur::model()->findAll();
		$editorsSorted = Editeur::model()->sorted()->findAll();
		$this->assertEquals(count($editors), count($editorsSorted));
		$this->assertEquals($editors[3], $editorsSorted[0]);
	}
}
