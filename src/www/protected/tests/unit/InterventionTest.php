<?php

class CliUser extends CApplicationComponent
{
	public $id;
}

/**
 * Tests the AR class "Intervention".
 */
class InterventionTest extends CDbTestCase
{
	/** @var array FixtureName => ModelName*/
	public $fixtures = [
		'interventions' => 'Intervention',
		'titres' => 'Titre',
		'editeurs' => 'Editeur',
		'titresediteurs' => 'TitreEditeur',
	];

	public function setUp()
	{
		// initialize a user (defaults to null in tests)
		$u = new CliUser;
		$u->id = 1;
		Yii::app()->setComponent('user', $u);
		parent::setUp();
	}

	public function testFind()
	{
		$intervention = Intervention::model()->findByPk(1);
		$this->assertTrue(is_object($intervention));
		$this->assertEquals("Intervention", get_class($intervention));
		$this->assertEquals("attente", $intervention->statut);
		$this->assertTrue(is_object($intervention->contenuJson));
		$this->assertEquals('InterventionDetail', get_class($intervention->contenuJson));
		$d = $intervention->contenuJson->toArray();
		$this->assertCount(2, $d);
		$this->assertEquals('Titre', $d[0]['model']);
	}

	public function testCreate()
	{
		$intervention = new Intervention();
		$intervention->setAttributes(
			[
				'contenuJson' => '[{"model": "Titre", "operation": "delete"}]',
				'statut' => 'attente',
				'commentaire' => '<p>Bonjour</p>',
			],
			false
		);
		$this->assertTrue($intervention->validate(), print_r($intervention->errors, true));
		$this->assertTrue($intervention->save(false), print_r($intervention->errors, true));
		$this->assertEquals($_SERVER['REQUEST_TIME'], $intervention->hdateProp);
		$this->assertEquals('&lt;p&gt;Bonjour&lt;/p&gt;', Intervention::model()->findByPk($intervention->id)->commentaire);
	}

	public function testCreateImport()
	{
		$intervention = new Intervention('import');
		$intervention->setAttributes(
			[
				'contenuJson' => '[{"model": "Titre", "operation": "delete"}]',
				'statut' => 'attente',
				'commentaire' => '<p>Bonjour</p>',
			],
			false
		);
		$this->assertTrue($intervention->validate(), print_r($intervention->errors, true));
		$this->assertTrue($intervention->save(false), print_r($intervention->errors, true));
		$this->assertEquals($_SERVER['REQUEST_TIME'], $intervention->hdateProp);
		$this->assertEquals('<p>Bonjour</p>', Intervention::model()->findByPk($intervention->id)->commentaire);
	}

	public function testCreateEmptySucceeds()
	{
		$intervention = new Intervention();
		$intervention->statut = 'attente';
		$intervention->attributes = [
			'commentaire' => "c'est moi !",
		];
		$intervention->contenuJson = [
			['model' => 'Revue', 'operation' => 'update', 'id' => 1, 'before' => [], 'after' => []],
		];
		$this->assertTrue($intervention->validate(), print_r($intervention->errors, true));
	}

	public function testCreateEmptyFails()
	{
		$intervention = new Intervention();
		$intervention->statut = 'attente';
		$intervention->contenuJson = [
			['model' => 'Revue', 'operation' => 'update', 'id' => 1, 'before' => [], 'after' => []],
		];
		$this->assertFalse($intervention->validate(), print_r($intervention->errors, true));

		$intervention->contenuJson = new InterventionDetail([
			['model' => 'Revue', 'operation' => 'update', 'id' => 1, 'before' => [], 'after' => []],
		]);
		$this->assertFalse($intervention->validate(), print_r($intervention->errors, true));
	}

	public function testSearch()
	{
		$intervention = new Intervention('search');
		$intervention->unsetAttributes();
		$intervention->suivi = false; // or set Yii::app()->user->suivi, etc
		$this->assertCount(1, $intervention->search()->data);
		$intervention->attributes = [
			'titre_titre' => 'Alphabet',
		];
		$this->assertCount(1, $intervention->search()->data);
		$intervention->attributes = [
			'titre_titre' => 'yop',
		];
		$this->assertCount(0, $intervention->search()->data);
	}

	public function testSearchByHdate()
	{
		$intervention = new Intervention('search');
		$intervention->unsetAttributes();
		$intervention->suivi = false; // or set Yii::app()->user->suivi, etc
		$intervention->hdateProp = '2010';
		$this->assertCount(1, $intervention->search()->getData(true));
		$intervention->hdateProp = '<2010';
		$this->assertCount(0, $intervention->search()->getData(true));
		$intervention->hdateProp = '> 2010';
		$this->assertCount(0, $intervention->search()->getData(true));
		$intervention->hdateProp = '>= 2010';
		$this->assertCount(1, $intervention->search()->getData(true));
		$intervention->hdateProp = '> 2009';
		$this->assertCount(1, $intervention->search()->getData(true));
	}

	public function testValidate()
	{
		$intervention = Intervention::model()->findByPk(1);
		$intervention->validate();
		$this->assertInstanceOf('InterventionDetail', $intervention->contenuJson);
	}

	public function testAcceptSucceeds()
	{
		$old = $this->titres(0);
		$this->assertCount(2, $old->editeurs);
		$intervention = Intervention::model()->findByPk(1);
		$this->assertTrue($intervention->accept(false), print_r($intervention->getErrors(), true));
		$titre = Titre::model()->findByPk(1);
		$this->assertEquals('Titre nouveau', $titre->titre);
		$this->assertCount(3, $titre->editeurs);
		$this->assertGreaterThan($old->hdateModif, $titre->hdateModif);
		$this->assertEquals($_SERVER['REQUEST_TIME'], $intervention->hdateVal);
		$this->assertEquals('accepté', $intervention->statut);
		return $intervention;
	}

	public function testAcceptAddsId()
	{
		$editeur = new Editeur();
		$intervention = $editeur->buildIntervention(true);
		$editeur->nom = 'mon editeur';
		$intervention->contenuJson->create($editeur);
		$intervention->accept();
		$details = $intervention->contenuJson->toArray();
		$this->assertArrayHasKey('id', $details[0], print_r($details, true));
		$this->assertGreaterThan(0, $details[0]['id']);

		$intervention = Intervention::model()->findByPk(1);
		$intervention->accept();
		$details = $intervention->contenuJson->toArray();
		$this->assertFalse(isset($details[1]['id']), print_r($details[1], true));
	}

	public function testAcceptFails()
	{
		$te = new TitreEditeur;
		$te->attributes = ['titreId' => 1, 'editeurId' => 3];
		$te->save();

		$intervention = Intervention::model()->findByPk(1);
		$this->assertFalse($intervention->accept());
		/** @todo verify nothing has changed */
		$this->assertNull($intervention->hdateVal);
	}

	public function testAcceptFailsValidation()
	{
		$intervention = Intervention::model()->findByPk(1);
		$intervention->email = 'hop';
		$this->assertFalse($intervention->accept());
		$this->assertNull($intervention->hdateVal);
	}

	/**
	 * @depends testAcceptSucceeds
	 * @param Intervention $i
	 */
	public function testAcceptWrong($i)
	{
		try {
			$i->accept();
		} catch (Exception $e) {
			return;
		}
		$this->fail('An expected exception has not been raised.');
	}

	public function testRejectSucceeds()
	{
		$intervention = Intervention::model()->findByPk(1);
		$this->assertTrue($intervention->reject(), print_r($intervention->getErrors(), true));
		$this->assertEquals($_SERVER['REQUEST_TIME'], $intervention->hdateVal);
		$this->assertEquals('refusé', $intervention->statut);
	}

	/**
	 * @depends testAcceptSucceeds
	 * @param Intervention $i
	 */
	public function testRejectWrong($i)
	{
		try {
			$i->reject();
		} catch (Exception $e) {
			return $i;
		}
		$this->fail('An expected exception has not been raised.');
	}

	public function testRevert()
	{
		$old = Titre::model()->findByPk(3);
		$new = clone ($old);
		$new->titre = "Voilà du nouveau";
		$i = new Intervention();
		$i->statut = 'attente';
		$i->contenuJson = new InterventionDetail();
		$i->contenuJson->update($old, $new);
		$this->assertTrue($i->accept(true));
		$this->assertEquals('accepté', $i->statut);
		$this->assertTrue($i->revert(), print_r($i->errors, true));
		$titre = Titre::model()->findByPk(1);
		$this->assertCount(2, $titre->editeurs);
		$this->assertEquals('Balai', $titre->titre);
	}

	public function testRevertFails()
	{
		$intervention = Intervention::model()->findByPk(1);
		try {
			$intervention->revert();
		} catch (Exception $e) {
			$this->assertEquals("Impossible d'annuler une intervention non acceptée.", $e->getMessage());
			return;
		}
		$this->fail('An expected exception has not been raised.');
	}

	/**
	 * @dataProvider providesClasses
	 */
	public function testAttributesLabels($className)
	{
		$model = call_user_func([$className, 'model']);
		$this->assertInstanceOf('Intervention', $model->buildIntervention(true));
		$model->id = 1;
		$this->assertInstanceOf('Intervention', $model->buildIntervention(false));
	}

	public function providesClasses()
	{
		return [
			['Editeur'],
			['Ressource'],
			['Service'],
			['Titre'],
		];
	}
}
