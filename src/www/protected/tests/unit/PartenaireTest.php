<?php

/**
 * Tests the AR class "Partenaire".
 */
class PartenaireTest extends CDbTestCase
{
	/** @var array FixtureName => ModelName */
	public $fixtures = [
		'partenaires' => 'Partenaire',
		'suivis' => ':Suivi', // table sans modèle
	];

	public function testFind()
	{
		$partenaire = Partenaire::model()->findByPk(1);
		$this->assertTrue(is_object($partenaire));
		$this->assertEquals("Partenaire", get_class($partenaire));
		$this->assertEquals("actif", $partenaire->statut);
	}

	public function testCreateOk()
	{
		$partenaire = new Partenaire();
		$partenaire->attributes = [
			'nom' => 'Alembert',
			'prefixe' => "D' ",
			'type' => 'normal',
			'statut' => 'actif',
		];
		$this->assertTrue($partenaire->validate(), print_r($partenaire->errors, true));
		$this->assertTrue($partenaire->save(false));
		$this->assertEquals($_SERVER['REQUEST_TIME'], $partenaire->hdateCreation);
		$this->assertEquals($_SERVER['REQUEST_TIME'], $partenaire->hdateModif);
		$this->assertGreaterThan(0, $partenaire->id);
		$this->assertEquals("D'Alembert", $partenaire->getFullName());
	}

	public function testCreateError()
	{
		$partenaire = new Partenaire();
		$partenaire->attributes = [
			'nom' => 'nom1',
			'type' => 'bizarre',
			'url' => 'zou',
			'email' => 'invalid@email',
			'statut' => 'anormal',
		];
		$this->assertFalse($partenaire->save());
		$errors = $partenaire->getErrors();
		$this->assertCount(4, $errors);
		$errorNames = array_keys($errors);
		sort($errorNames, SORT_STRING);
		$this->assertEquals(['email', 'statut', 'type', 'url'], $errorNames);
	}

	public function testGetRightsSuivi()
	{
		$this->assertEquals(
			['Revue' => [1], 'Ressource' => [1]],
			$this->partenaires(0)->getRightsSuivi()
		);
		$this->assertEquals(
			[],
			$this->partenaires(2)->getRightsSuivi()
		);
	}

	public function testGetListByType()
	{
		$list = Partenaire::getListByType();
		$this->assertEquals(['normal', 'import', 'editeur'], array_keys($list));
		$this->assertCount(1, $list['normal'], print_r($list, true));
		$this->assertCount(1, $list['editeur'], print_r($list, true));
	}

	public function testFindInstituteByIp()
	{
		$this->assertEquals(0, Partenaire::findInstituteByIp('99.13.1.45'));
		$this->assertEquals(0, Partenaire::findInstituteByIp('88.68.0.45'));
		$this->assertEquals(4, Partenaire::findInstituteByIp('88.69.1.45'));
		$this->assertEquals(3, Partenaire::findInstituteByIp('192.168.10.20'));
		$this->assertEquals(2, Partenaire::findInstituteByIp('::1'));
	}
}
