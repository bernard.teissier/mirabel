<?php

/**
 * Tests the AR class "Service".
 */
class ServiceTest extends CDbTestCase
{
	/** @var array FixtureName => ModelName */
	public $fixtures = [
		'services' => 'Service',
		'titres' => 'Titre',
		'ressources' => 'Ressource',
	];

	public function testFind()
	{
		$service = Service::model()->findByPk(1);
		$this->assertTrue(is_object($service));
		$this->assertEquals("Service", get_class($service));
		$this->assertEquals("normal", $service->statut);
		$this->assertEquals("Libre", $service->acces);
		$this->assertTrue(strpos($service->links, "> <") > 10);
	}

	public function testCreateOk()
	{
		$timeStart = time() - 3600;
		$service = new Service('reprise');
		$service->attributes = [
			'ressourceId' => 1,
			'titreId' => '1',
			'type' => 'Résumé',
			'acces' => 'Restreint',
			'url' => 'http://la.basse.com/',
			'dateBarrDebut' => '1981-05',
			'langues' => 'English, French, Italian, German, Spanish',
		];
		$this->assertTrue($service->validate(), print_r($service->errors, true));
		$this->assertTrue($service->save(false));
		$this->assertGreaterThan($timeStart, $service->hdateModif);
		$this->assertGreaterThan(0, $service->id);
		$this->assertEquals('eng, fre, ita, ger, spa', $service->langues);
		return $service;
	}

	public function testCreateError()
	{
		$service = new Service();
		$service->attributes = [
			'ressourceId' => 'a',
			'titreId' => '',
			'type' => 'Reprise',
			'url' => 'bof',
		];
		$this->assertFalse($service->validate());
		$this->assertEquals(6, count($service->getErrors()), print_r($service->getErrors(), true));
	}

	public function testCreateErrorDates()
	{
		$service = new Service();
		$service->attributes = [
			'ressourceId' => 1,
			'titreId' => 1,
			'type' => 'Résumé',
			'acces' => 'Restreint',
			'url' => 'http://la.basse.com/',
			'dateBarrDebut' => '1981-05',
			'dateBarrFin' => '1981',
		];
		$this->assertFalse($service->validate());
		$this->assertEquals(1, count($service->getErrors()), print_r($service->getErrors(), true));
	}

	/**
	 * @depends testCreateOk
	 */
	public function testGetPeriode($s)
	{
		$this->assertEquals("mars 1998 (vol.1 no.3) — janvier 2010 (vol.6 no.8)", $this->services(0)->periode);
		$this->assertEquals("mai 1981 — …", $s->getPeriode());
		$s->dateBarrDebut = '';
		$s->dateBarrFin = '1981-05';
		$this->assertEquals("… — mai 1981", $s->getPeriode());
	}
}
