<?php

/**
 * Tests the AR class "Revue".
 */
class RevueTest extends CDbTestCase
{
	/** @var array FixtureName => ModelName */
	public $fixtures = [
		'revues' => 'Revue',
		'titres' => 'Titre',
		'ressources' => 'Ressource',
		'collections' => 'Collection',
		'titresCollections' => ':Titre_Collection',
	];

	public function testFind()
	{
		$revue = Revue::model()->findByPk(1);
		$this->assertTrue(is_object($revue));
		$this->assertEquals("Revue", get_class($revue));
		$this->assertEquals("normal", $revue->statut);
		$this->assertEquals(null, $revue->hdateVerif);
	}

	public function testCreateOk()
	{
		$revue = new Revue();
		$revue->attributes = [
			'statut' => 'normal',
		];
		$this->assertTrue($revue->validate(), "Validation failed: " . print_r($revue->errors, true));
		$this->assertTrue($revue->save(false), "Saving failed.");
		$this->assertGreaterThan(0, $revue->id);

		$revue->attributes = [
			'statut' => 'attente',
			'hdateVerif' => 10000,
		];
		$this->assertTrue($revue->validate(), "This object should pass validation.");
		$this->assertTrue($revue->save(false));
		$this->assertGreaterThan(0, $revue->id);
	}

	public function testGetTitres()
	{
		$this->assertEquals([2, 1], array_keys($this->revues(0)->getTitres()));
		$this->assertEquals([3], array_keys($this->revues(1)->getTitres()));
		$this->assertEquals([0, 1], array_keys($this->revues(0)->getTitres(false)));
		$revue = new Revue();
		$this->assertEquals([], $revue->getTitres());
	}

	public function testGetFullTitle()
	{
		$this->assertEquals('La Virgule — Sigle2', $this->revues(0)->getFullTitle());
		$this->assertEquals("L'Alphabet — Sigle3", $this->revues(1)->getFullTitle());
		$this->assertEquals(
			'A contrario. Revue interdisciplinaire de sciences sociales — Sigle4',
			$this->revues(2)->getFullTitle()
		);
	}

	public function testGetName()
	{
		$this->assertEquals('Virgule', $this->revues(0)->nom);
		$this->assertEquals('Alphabet', $this->revues(1)->nom);
		$this->assertEquals(
			'A contrario. Revue interdisciplinaire de sciences sociales',
			$this->revues(2)->nom
		);
	}

	public function testGetCollections()
	{
		$collections = $this->revues(0)->getCollections();
		$this->assertCount(2, $collections);
		$this->assertEquals(['collection', 'ressourceNom', 'titreTitre'], array_keys($collections[0]));
		$this->assertEquals('Cairn', $collections[0]['ressourceNom']);
		$this->assertEquals('Bouquet Général', $collections[0]['collection']->nom);

		$revue = new Revue();
		$this->assertEquals([], $revue->getCollections());
	}
}
