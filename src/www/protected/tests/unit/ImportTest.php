<?php

Yii::import('application.models.import.ImportKbart');

/**
 * Tests the AR class "Import" (through non-abstract child ImportKbart).
 */
class ImportTest extends CDbTestCase
{
	protected $importParams = [
		'ressourceId' => 1,
		'separator' => ',',
		'name' => 'Cairn',
		'type' => 'Intégral',
		'lacunaire' => 1,
		'selection' => 0,
		'url' => '',
	];

	/**
	 * @dataProvider provideKbartInput
	 */
	public function testConvKbartToDate($expected, $input)
	{
		$import = new ImportKbart($this->importParams);
		$time = mktime(12, 0, 0, 7, 2, 2012);
		$date = $import->normalize->convKbartToDate($input, $time);
		unset($date['fulldate']);
		$this->assertEquals($expected, $date);
	}

	public function provideKbartInput()
	{
		// Format : résultat attendu (tableau), puis entrée (texte)
		// Les dates sont calculées relativement au 2012-07-02.
		return [
			[
				[
					'date' => '2011',
					'type' => 'P',
					'info' => "Accès avant une date barrière mobile de 1 année calendaire",
				],
				"P1Y",
			],
			[
				[
					'date' => '2011-07-02',
					'type' => 'P',
					'info' => "Accès avant une date barrière mobile de 1 année glissante",
				],
				"P365D",
			],
			[
				[
					'date' => '2010',
					'type' => 'P',
					'info' => "Accès avant une date barrière mobile de 2 années calendaires",
				],
				"P2Y",
			],
			[
				[
					'date' => '2011',
					'type' => 'R',
					'info' => "Accès après une date barrière mobile de 2 années calendaires",
				],
				"R2Y",
			],
			[
				[
					'date' => '2010-07-02',
					'type' => 'P',
					'info' => "Accès avant une date barrière mobile de 2 années glissantes",
				],
				"P730D",
			],
			[
				[
					'date' => '2012-05',
					'type' => 'P',
					'info' => "Accès avant une date barrière mobile de 2 mois calendaires",
				],
				"P2M",
			],
			[
				[
					'date' => '2012-05-02',
					'type' => 'P',
					'info' => "Accès avant une date barrière mobile de 2 mois glissants",
				],
				"P60D",
			],
			[
				[
					'date' => '2012-03-24',
					'type' => 'R',
					'info' => "Accès après une date barrière mobile de 100 jours",
				],
				"R100D",
			],
			// quelques entrées erronées
			[['date' => '', 'info' => '', 'type' => ''],	"100D", ],
			[['date' => '', 'info' => '', 'type' => ''],	"M100D"],
			[['date' => '', 'info' => '', 'type' => ''],	"P100m"],
		];
	}
}
