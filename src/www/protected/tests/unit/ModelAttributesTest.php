<?php

/**
 * Tests the attributes of several models.
 */
class ModelAttributesTest extends CDbTestCase
{
	/**
	 * @dataProvider providesClasses
	 */
	public function testAttributesLabels($className)
	{
		$model = call_user_func([$className, 'model']);
		$this->assertEquals(
			[],
			array_diff(
				array_keys($model->metadata->columns),
				array_keys($model->attributeLabels())
			)
		);
	}

	public function providesClasses()
	{
		return [
			['Collection'],
			['Editeur'],
			['Hint'],
			['Identification'],
			['Intervention'],
			['LogExt'],
			['Partenaire'],
			['Revue'],
			['Ressource'],
			['Service'],
			['Titre'],
			['Utilisateur'],
		];
	}
}
