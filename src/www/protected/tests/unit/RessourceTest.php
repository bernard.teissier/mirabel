<?php

/**
 * Tests the AR class "Ressource".
 */
class RessourceTest extends CDbTestCase
{
	/** @var array FixtureName => ModelName */
	public $fixtures = [
		'ressources' => 'Ressource',
		'titres' => 'Titre',
		'services' => 'Service',
	];

	public function testFind()
	{
		$ressource = Ressource::model()->findByPk(1);
		$this->assertTrue(is_object($ressource));
		$this->assertEquals("Ressource", get_class($ressource));
		$this->assertEquals("Bouquet", $ressource->type);
		$this->assertEquals("Libre", $ressource->acces);
		$this->assertEquals("Les Rêveurs de rune", $ressource->getFullName());
	}

	public function testCreateOk()
	{
		$ressource = new Ressource();
		$ressource->attributes = [
			'nom' => 'Cairn',
			'type' => 'Archive',
			'acces' => 'Restreint',
			'url' => 'http://cairn.org',
		];
		$this->assertTrue($ressource->validate(), print_r($ressource->errors, true));
		$this->assertTrue($ressource->save(false));
		$this->assertEquals($_SERVER['REQUEST_TIME'], $ressource->hdateModif);
		$this->assertGreaterThan(0, $ressource->id, print_r($ressource->attributes, true));
		return $ressource;
	}

	public function testCreateError()
	{
		$ressource = new Ressource();
		$ressource->attributes = [
			'nom' => 'Machin',
			'type' => 'Archiveur',
			'acces' => 'R',
			'url' => 'cairn.org',
		];
		$this->assertFalse($ressource->validate());
		$this->assertEquals(3, count($ressource->getErrors()), print_r($ressource->getErrors(), true));
	}

	/**
	 * @depends testCreateOk
	 */
	public function testCountRevues($r)
	{
		$this->assertEquals(0, $r->countRevues());
		$this->assertEquals(1, $this->ressources(0)->countRevues());
	}

	public function testCompleteTerm()
	{
		$this->markTestSkipped("Sphinx server required");
		$this->assertEquals(
			[['id' => 1, 'value' => 'Rêveurs de rune', 'label' => 'Les Rêveurs de rune']],
			Ressource::completeTerm("reveur")
		);
		$this->assertEquals(
			[],
			Ressource::completeTerm("rune")
		);
	}
}
