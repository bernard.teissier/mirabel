<?php

/**
 * Tests the AR class "Utilisateur".
 */
class UtilisateurTest extends CDbTestCase
{
	/** @var array FixtureName => ModelName */
	public $fixtures = [
		'utilisateurs' => 'Utilisateur',
		'partenaires' => 'Partenaire',
	];

	public function testFind()
	{
		$utilisateur = Utilisateur::model()->findByPk(1);
		$this->assertTrue(is_object($utilisateur));
		$this->assertEquals("Utilisateur", get_class($utilisateur));
		$this->assertEquals(1, $utilisateur->actif);
		$this->assertEquals("monsieur", $utilisateur->login);
	}

	public function testCreateOk()
	{
		$utilisateur = new Utilisateur();
		$utilisateur->attributes = [
			'login' => 'madame',
			'partenaireId' => 2,
		];
		$this->assertTrue($utilisateur->validate());
		$this->assertTrue($utilisateur->save(false));
		$this->assertEquals($_SERVER['REQUEST_TIME'], $utilisateur->hdateModif);
		$this->assertGreaterThan(0, $utilisateur->id);
	}

	public function testCreateError()
	{
		$utilisateur = new Utilisateur();
		$utilisateur->attributes = [
			'login' => 'nom1 !',
		];
		$this->assertFalse($utilisateur->validate());
		$this->assertEquals(2, count($utilisateur->getErrors()));
	}

	public function testSearch()
	{
		$utilisateur = new UtilisateurSearch();
		$utilisateur->attributes = [
			'hdateCreation' => '>2012-04',
			'permAdmin' => '',
			'permImport' => '',
			'permPartenaire' => '',
			'permIndexation' => '',
			'suiviEditeurs' => '',
			'suiviNonSuivi' => '',
		];
		$prov = $utilisateur->search();
		$this->assertInstanceOf('CDataProvider', $prov);
		$this->assertCount(1, $prov->getData());

		$utilisateur->hdateCreation = '<=2012-04';
		$prov = $utilisateur->search();
		$this->assertInstanceOf('CDataProvider', $prov);
		$this->assertCount(0, $prov->getData());
	}

	public function testValidateLocalPassword()
	{
		$u = $this->utilisateurs(0);
		$this->assertFalse($u->validatePassword('rueisnom'));
		$u->motdepasse = 'azertyuiop';
		$this->assertFalse($u->validatePassword('rueisnom'));
		$this->assertFalse($u->validatePassword('azertyuiop'));
		$u->actif = 0;
		$this->assertFalse($u->validatePassword('azertyuiop'));
		$u->actif = 1;
		$u->partenaire->statut = 'inactif';
		$this->assertFalse($u->validatePassword('azertyuiop'));
	}

	public function testValidatePasswordDebug()
	{
		$u = $this->utilisateurs(0);
		$this->assertFalse($u->validatePassword('azertyuiop'));
		$this->assertFalse($u->validatePassword('rueisnom'));
	}
}
