<?php

/**
 * Tests the AR class "Titre".
 */
class TitreTest extends CDbTestCase
{
	/** @var array FixtureName => ModelName */
	public $fixtures = [
		'revues' => 'Revue',
		'titres' => 'Titre',
		'editeurs' => 'Editeur',
		'titresEditeurs' => ':Titre_Editeur', // table sans modèle
	];

	public function testFind()
	{
		$titre = Titre::model()->findByPk(1);
		$this->assertTrue(is_object($titre));
		$this->assertEquals("Titre", get_class($titre));
		$this->assertEquals("normal", $titre->statut);
		$this->assertEquals("Du Balai — Sigle1", $titre->FullTitle);
		$this->assertNull(Titre::model()->findByPk(2)->obsoletePar);
	}

	public function testCreateOk()
	{
		$titre = new Titre();
		$titre->attributes = [
			'revueId' => 1,
			'titre' => 'Nouveau titre',
			'prefixe' => 'Le ',
			'dateDebut' => '2000-05',
			'dateFin' => '2010',
			'issn' => '0183-875X',
			'issnl' => '',
			'electronique' => '1',
			'langues' => 'English, French, Italian, German, Spanish',
		];
		$this->assertTrue($titre->validate(), print_r($titre->errors, true));
		$this->assertTrue($titre->save());
		$this->assertEquals($_SERVER['REQUEST_TIME'], $titre->hdateModif);
		$this->assertGreaterThan(0, $titre->id);
		$this->assertNull($titre->obsoletePar);
		$this->assertEquals('1', $titre->electronique);
		$this->assertEquals('Le ', $titre->prefixe);
		$this->assertEquals(
			$titre->id,
			Titre::model()->findByPk(2)->obsoletePar,
			"Les anciens titres doivent être obsolètes après l'insertion d'un titre sans suivant."
		);
		$this->assertEquals('eng, fre, ita, ger, spa', $titre->langues);
	}

	public function testCreateWithLinks()
	{
		$titre = new Titre();
		$titre->attributes = [
			'revueId' => 1,
			'titre' => 'Nouveau titre',
			'liensJson' => [['src' => 'first', 'url' => 'http://nginx.org/']],
		];
		$this->assertTrue($titre->validate());
		$titre->liensJson = [
			['src' => 'second', 'url' => 'http://nowhere.org/'],
		];
		$this->assertFalse($titre->validate());
	}

	public function testCreateError()
	{
		$titre = new Titre();
		$titre->attributes = [
			'titre' => 'nom1',
			'statut' => 'bizarre',
			'url' => 'zou',
			'prefixe' => '1234567890 1234567890 1234567890',
			'dateDebut' => 'bientôt',
		];
		$this->assertFalse($titre->validate());
		$errors = $titre->getErrors();
		$this->assertCount(4, $errors, print_r($errors, true));
		$errorNames = array_keys($errors);
		sort($errorNames, SORT_STRING);
		$this->assertEquals(['dateDebut', 'prefixe', 'statut', 'url'], $errorNames);
	}

	public function testCreateErrorOnDates()
	{
		$titre = new Titre();
		$titre->attributes = [
			'titre' => 'nom1',
			'revueId' => 1,
			'dateDebut' => '2010-01',
			'dateFin' => '02/2009',
		];
		$this->assertFalse($titre->validate());
		$errors = $titre->getErrors();
		$this->assertCount(1, $errors);
		$errorNames = array_keys($errors);
		$this->assertEquals(['dateDebut'], $errorNames);
	}

	public function testCreateDuplicate()
	{
		$this->markTestSkipped("Sphinx server required");
		$titre = new Titre();
		$titre->attributes = [
			'titre' => 'Balai',
			'revueId' => 1,
		];
		$this->assertFalse($titre->validate());
		$errors = $titre->getErrors();
		$this->assertCount(1, $errors, print_r($errors, true));
		$this->assertEquals(['titre'], array_keys($errors));
	}

	public function testUpdateError()
	{
		$titre = $this->titres(1);
		$titre['obsoletePar'] = '2';
		$this->assertFalse($titre->validate());
		$errors = $titre->getErrors();
		$this->assertCount(1, $errors, print_r($errors, true));
		$errorNames = array_keys($errors);
		$this->assertEquals(['obsoletePar'], $errorNames);
	}

	public function testGetOtherTitles()
	{
		$titre = $this->titres(0);
		$others = $titre->getOtherTitles();
		$this->assertCount(1, $others);
		$this->assertEquals(2, $others[0]->id);

		$others = $titre->getOtherTitles(true);
		$this->assertCount(1, $others);
		$this->assertTrue(isset($others[2]));

		$this->assertEquals([], $this->titres(2)->getOtherTitles());
		$this->assertEquals([], Titre::model()->getOtherTitles());
	}

	public function testGetOtherTitlesNew()
	{
		$titre = new Titre();
		$titre->attributes = [
			'revueId' => 1,
			'titre' => 'Nouveau titre',
			'dateDebut' => '2000-05',
			'dateFin' => '2010',
		];
		$this->assertCount(2, $titre->getOtherTitles());
	}

	public function testGetRelationsEditeurs()
	{
		$t = new Titre();
		$this->assertEquals([], $t->getRelationsEditeurs());

		$titre = $this->titres(0);
		$relations = $titre->getRelationsEditeurs();
		$this->assertCount(2, $relations, print_r($relations, true));
		$this->assertEquals("Editeur", get_class($relations[1]));
	}

	public function testAddRelationsEditeur()
	{
		$titre = $this->titres(1);
		$this->assertCount(1, $titre->getRelationsEditeurs());
		$this->assertEquals(2, $titre->addRelationsEditeur([2, 3]));
		$this->assertCount(3, $titre->getRelationsEditeurs());
		$this->assertEquals(0, $titre->addRelationsEditeur([0, 1, 2, 5]));
		$this->assertCount(3, $titre->getRelationsEditeurs());
	}

	public function testDeleteRelationEditeur()
	{
		$titre = $this->titres(0);
		$this->assertTrue($titre->deleteRelationEditeur(1));
		$this->assertCount(1, $titre->getRelationsEditeurs());
		$this->assertFalse($titre->deleteRelationEditeur(3));
	}

	public function testGetFullTitle()
	{
		$this->assertEquals('Du Balai — Sigle1', $this->titres(0)->getFullTitle());
		$this->assertEquals('La Virgule — Sigle2', $this->titres(1)->getFullTitle());
		$this->assertEquals("L'Alphabet — Sigle3", $this->titres(2)->getFullTitle());
	}

	public function testGetPrefixedTitle()
	{
		$this->assertEquals('Du Balai', $this->titres(0)->getPrefixedTitle());
		$this->assertEquals('La Virgule', $this->titres(1)->getPrefixedTitle());
		$this->assertEquals("L'Alphabet", $this->titres(2)->getPrefixedTitle());
	}

	public function testCreateWithPrefix()
	{
		$titre = new Titre();
		$titre->attributes = [
			'revueId' => 1,
			'titre' => 'Absent',
			'prefixe' => "L´ ",
		];
		$this->assertTrue($titre->validate(), print_r($titre->errors, true));
		$this->assertEquals("L'", $titre->prefixe);
		$this->assertEquals("L'Absent", $titre->getFullTitle());

		$titre = new Titre();
		$titre->attributes = [
			'revueId' => 2,
			'titre' => 'Montre',
			'prefixe' => "La",
		];
		$this->assertTrue($titre->validate());
		$this->assertEquals("La ", $titre->prefixe);
		$this->assertEquals("La Montre", $titre->getFullTitle());
	}

	public function testDelete()
	{
		$this->assertTrue($this->titres(0)->delete());
		$this->assertFalse($this->titres(1)->delete());
	}

	public function testCompleteTerm()
	{
		$this->markTestSkipped("Sphinx server required");
		$this->assertEquals(
			[['id' => 1, 'value' => 'Balai', 'label' => 'Du Balai']],
			Titre::completeTerm("bal")
		);
		$this->assertEquals(
			[],
			Titre::completeTerm("Les possédés")
		);
	}

	/**
	 * @dataProvider provideDates
	 */
	public function testGetPeriode($expected, $start, $end)
	{
		$titre = new Titre();
		$titre->dateDebut = $start;
		$titre->dateFin = $end;
		$this->assertEquals($expected, $titre->getPeriode());
	}

	public function provideDates()
	{
		return [
			['', '', ''],
			['2005 – mai 2010', '2005', '2010-05'],
			['2005 – …', '2005', ''],
			['… – 10 janvier 2001', '', '2001-01-10'],
		];
	}
}
