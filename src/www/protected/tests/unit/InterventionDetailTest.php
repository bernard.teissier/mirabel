<?php

/**
 * Tests the AR class "InterventionDetail".
 */
class InterventionDetailTest extends CDbTestCase
{
	/** @var array FixtureName => ModelName */
	public $fixtures = [
		'revues' => 'Revue',
		'titres' => 'Titre',
	];

	public $details = [
		'create' => [
			'model' => 'Titre',
			'operation' => 'create',
			'after' => [
				'titre' => 'ajout de test',
				'revueId' => '1',
				'obsoletePar' => '1',
				'dateDebut' => '2001-02',
				'dateFin' => '2010-12',
				'liensJson' => '',
				'statut' => 'normal',
			],
			'msg' => 'hop',
		],
		'delete' => [
			'model' => 'Titre',
			'operation' => 'delete',
			'id' => '1',
			'before' => [
				'id' => '1',
				'revueId' => '1',
				'titre' => 'Balai',
				'prefixe' => 'Du ',
				'sigle' => 'Sigle1',
				'obsoletePar' => 2,
				'dateDebut' => '2000-01-01',
				'dateFin' => '2011-01-01',
				'sudoc' => '',
				'worldcat' => '',
				'issn' => '2112-2679',
				'issnl' => '',
				'url' => '',
				'liensJson' => '',
				'periodicite' => '',
				'electronique' => '0',
				'langues' => 'fre',
				'statut' => 'normal',
				'hdateModif' => '1334813664',
			],
			'msg' => 'hop',
		],
		'update' => [
			'model' => 'Titre',
			'operation' => 'update',
			'id' => 1,
			'before' => [
				'dateDebut' => '2000-01-01',
				'dateFin' => '2011-01-01',
			],
			'after' => [
				'dateDebut' => '2001-02',
				'dateFin' => '2010-12',
			],
			'msg' => 'hop',
		],
	];

	public function testSerialize()
	{
		$detail = new InterventionDetail([$this->details['create']]);
		$json = $detail->serialize();
		$decoded = json_decode($json, true);
		$this->assertEquals($this->details['create'], $decoded['content'][0]);
	}

	public function testCreate()
	{
		$titre = new Titre();
		$titre->attributes = [
			'titre' => 'ajout de test',
			'revueId' => '1',
			'obsoletePar' => '1',
			'dateDebut' => '2001-02',
			'dateFin' => '2010-12',
			'statut' => 'normal',
			'liensJson' => '',
		];
		$this->assertTrue($titre->validate());
		$detail = new InterventionDetail;
		$this->assertTrue($detail->create($titre, 'hop'));
		$expected = [$this->details['create']];
		$this->assertEquals($expected, $detail->toArray());
		return $detail;
	}

	public function testDelete()
	{
		$titre = $this->titres(0);
		$detail = new InterventionDetail;
		$this->assertTrue($detail->delete($titre, 'hop'));
		$expected = [$this->details['delete']];
		$this->assertEquals($expected, $detail->toArray());
	}

	public function testUpdate()
	{
		$titre = $this->titres(0);
		$titreUpdated = clone $titre;
		$attributes = [
			'dateDebut' => '2001-02',
			'dateFin' => '2010-12',
			'statut' => 'normal',
		];
		$titreUpdated->attributes = $attributes;
		$detail = new InterventionDetail;
		$this->assertTrue($detail->update($titre, $titreUpdated, 'hop'));
		$expected = [$this->details['update']];
		$this->assertEquals($expected, $detail->toArray());
		return $detail;
	}

	/**
	 * @depends testUpdate
	 * @param InterventionDetail $d
	 */
	public function testArrayNotation($detail)
	{
		$expected = [$this->details['update']];
		$this->assertTrue(isset($detail[0]));
		$this->assertEquals($expected[0], $detail[0]);
	}

	public function testUpdateEmpty()
	{
		$titre = $this->titres(0);
		$detail = new InterventionDetail;
		$this->assertFalse($detail->update($titre, $titre));
		$this->assertCount(1, $detail->toArray());
		$first = $detail->toArray();
		$first = $first[0];
		$this->assertEquals('update', $first['operation']);
		$this->assertEquals([], $first['after']);
	}

	public function testUpdateEmptyRejected()
	{
		$titre = $this->titres(0);
		$detail = new InterventionDetail;
		$this->assertFalse($detail->update($titre, $titre, '', false));
		$this->assertCount(0, $detail->toArray());
	}

	public function testUpdateFails()
	{
		$old = $this->titres(0);
		$new = new Titre();
		$new->attributes = ['statut' => 'normal'];
		$detail = new InterventionDetail;
		$this->assertFalse($detail->update($old, $new));
	}

	public function testCreateFails()
	{
		$titre = $this->titres(0);
		$titre->id = null;
		$detail = new InterventionDetail;
		$this->assertFalse($detail->create($titre));
	}

	public function testDeleteFails()
	{
		$titre = new Titre;
		$detail = new InterventionDetail;
		$this->assertFalse($detail->delete($titre));
	}

	/**
	 * @depends testCreate
	 * @param object $details result of testCreate
	 */
	public function testMixCreateDelete($detail)
	{
		$titre = $this->titres(0);
		$this->assertTrue($detail->delete($titre, 'hop'));
		$expected = [$this->details['create'], $this->details['delete']];
		$this->assertEquals($expected, $detail->toArray());
	}

	public function testApplyCreate()
	{
		$db = Yii::app()->db;
		$sqlMax = "SELECT id FROM Titre ORDER BY id DESC LIMIT 1";
		$maxId = (int) $db->createCommand($sqlMax)->queryScalar();
		$detail = new InterventionDetail([$this->details['create']]);
		$this->assertTrue($detail->apply(), print_r($detail->getErrors(), true));
		$this->assertEquals($maxId + 1, $db->createCommand($sqlMax)->queryScalar());
		$titre = Titre::model()->findByPk($maxId + 1);
		$this->assertEquals('2001-02', $titre->dateDebut);
		$this->assertEquals('2010-12', $titre->dateFin);
		$this->assertEquals('normal', $titre->statut);
		$array = $detail->toArray();
		$this->assertArrayHasKey('id', $array[0], print_r($array, true));
		$this->assertEquals($maxId + 1, $array[0]['id'], print_r($array, true));
		return $detail;
	}

	// Fails because the object does not exist
	public function testApplyDeleteFails()
	{
		$before = Yii::app()->db->createCommand("SELECT id FROM Titre")->queryColumn();
		$a = $this->details['delete'];
		$a['id'] = '7';
		$a['before']['id'] = '7';
		$detail = new InterventionDetail([$a]);
		$this->assertFalse($detail->apply());
		$this->assertEquals($before, Yii::app()->db->createCommand("SELECT id FROM Titre")->queryColumn());
	}

	public function testApplyUpdateSucceeds()
	{
		$detail = new InterventionDetail([$this->details['update']]);
		$this->assertTrue($detail->apply());
		$titre = $this->titres(0);
		$this->assertEquals('2001-02', $titre->dateDebut);
		$this->assertEquals('2010-12', $titre->dateFin);
		$this->assertEquals('normal', $titre->statut);
		return $detail;
	}

	public function testApplyUpdateFails()
	{
		$details = $this->details['update'];
		$details['id'] = 8;
		$detail = new InterventionDetail([$details]);
		$this->assertFalse($detail->apply());
		$errors = $detail->getErrors();
		$this->assertArrayHasKey('Titre', $errors, print_r($errors, true));
		$this->assertCount(2, $errors);

		$detail = new InterventionDetail();
		$this->assertFalse($detail->apply());
	}

	public function testApplyUpdateFailsAtValidation()
	{
		$details = $this->details['update'];
		$details['after']['dateDebut'] = 'demain';
		$detail = new InterventionDetail([$details]);
		$this->assertFalse($detail->apply());
		$errors = $detail->getErrors();
		$this->assertArrayHasKey('Titre.dateDebut', $errors);
		$this->assertEquals(['Format de date non valide (YYYY-mm-dd ou dd/mm/YYYY).'], $errors['Titre.dateDebut']);
		$this->assertCount(2, $errors);
	}

	public function testIsEmpty()
	{
		$detail1 = new InterventionDetail();
		$this->assertTrue($detail1->isEmpty());

		$detail2 = new InterventionDetail([$this->details['create']]);
		$this->assertFalse($detail2->isEmpty());

		$titre = $this->titres(0);
		$detail3 = new InterventionDetail;
		$this->assertFalse($detail3->update($titre, $titre));
		$this->assertTrue($detail3->isEmpty());
	}

	public function testRevertCreate()
	{
		Yii::app()->db->createCommand("DELETE FROM Service")->execute();
		$before = Yii::app()->db->createCommand("SELECT id FROM Titre ORDER BY id")->queryColumn();
		$detail = new InterventionDetail([$this->details['create']]);
		$this->assertTrue($detail->apply());
		$this->assertEquals(count($before) + 1, Yii::app()->db->createCommand("SELECT count(*) FROM Titre")->queryScalar());
		$this->assertTrue($detail->revert(), "ERRORS: " . print_r($detail->getErrors(), true));
		$this->assertEquals($before, Yii::app()->db->createCommand("SELECT id FROM Titre ORDER BY id")->queryColumn());
		$this->assertFalse($detail->revert());
	}

	public function testRevertUpdate()
	{
		$detail = new InterventionDetail([$this->details['update']]);
		$detail->apply();
		$this->assertTrue($detail->revert());
		$titre = Titre::model()->findByPk(1);
		$this->assertEquals('2000-01-01', $titre->dateDebut);
		$this->assertEquals('2011-01-01', $titre->dateFin);
		$this->assertFalse($detail->revert());
	}

	public function testRevertUpdateFails()
	{
		$detail = new InterventionDetail([$this->details['update']]);
		$detail->apply();
		Yii::app()->db->createCommand("UPDATE Titre SET dateDebut = '2002-02-02' WHERE id=1")->execute();
		$this->assertFalse($detail->revert());
		$this->assertEquals(['confirm', 0], array_keys($detail->getErrors()));
	}
}
