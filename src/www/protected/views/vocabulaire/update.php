<?php

/** @var Controller $this */
/** @var Vocabulaire $model */
/** @var array $categoriesEtAlias */
assert($this instanceof Controller);

$this->pageTitle = "Vocabulaire - Modification";
$this->breadcrumbs = [
	'Thématique' => ['/categorie/index'],
	'Vocabulaire' => ['index'],
	$model->titre,
];

$this->menu[] = ['label' => 'Modifier ce vocabulaire', 'url' => ['update', 'id' => $model->id]];
?>

<h1>Modifier le vocabulaire <em><?php echo CHtml::encode($model->titre); ?></em></h1>

<?php
echo $this->renderPartial('_form', ['model' => $model]);
?>

<h2>Alias associés à ce vocabulaire</h2>

<table id="vocabulaire-alias" class="table table-striped table-bordered table-condensed">
	<thead>
		<tr>
			<th>Thème Mir@bel</th>
			<th>Alias pour ce vocabulaire</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($categoriesEtAlias as $x) {
			echo "<tr>
			<td>" . CHtml::encode($x[0]) . "</td>
			<td>" . CHtml::encode($x[1]) . "</td>
			<td>" . HtmlHelper::postButton("Suppr", ['categorieAlias/delete', 'id' => $x[2]]) . "</td>
		</tr>";
		}
		?>
	</tbody>
</table>
