<?php

/** @var Controller $this */
/** @var CategorieAliasImport $importAlias */
assert($this instanceof Controller);

$this->pageTitle = "Thématique - Import Vocab";
$this->breadcrumbs = [
	'Thématique' => ['/categorie/index'],
	'Vocabulaires' => ['index'],
	'Import, étape 2/2',
];

$this->menu[] = ['label' => "Retour à l'envoi CSV", 'url' => ['prepareImport']];
?>

<h1>Importer des vocabulaires thématiques 2/2</h1>

<?php
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'import-vocab-form',
		'enableAjaxValidation' => false,
		'type' => BootActiveForm::TYPE_HORIZONTAL,
	]
);
/** @var BootActiveForm $form */
?>

<?php
echo $form->errorSummary($importAlias);
?>

<fieldset>
	<legend>Vocabulaires à créer</legend>
	<?php
	if ($importAlias->getVocabsCreateCandidates()) {
		echo $form->checkBoxListRow(
			$importAlias,
			'vocabsCreate',
			array_combine($importAlias->getVocabsCreateCandidates(), $importAlias->getVocabsCreateCandidates())
		);
	} else {
		echo "<p>Aucun nouveau vocabulaire dans l'en-tête du CSV.</p>";
	}
?>
</fieldset>
<fieldset>
	<legend>Vocabulaires à compléter</legend>
	<?php
	if ($importAlias->getVocabsUpdateCandidates()) {
		echo $form->checkBoxListRow(
			$importAlias,
			'vocabsUpdate',
			array_combine($importAlias->getVocabsUpdateCandidates(), $importAlias->getVocabsUpdateCandidates())
		);
	} else {
		echo "<p>Tous les vocabulaires de l'en-tête du CSV sont inconnus de Mir@bel.</p>";
	}
?>
</fieldset>

<div class="form-actions">
	<?php
$this->widget(
	'bootstrap.widgets.BootButton',
	[
		'buttonType' => 'submit',
		'type' => 'primary',
		'label' => 'Importer ces vocabulaires',
	]
);
?>
</div>

<?php
$this->endWidget();
?>
