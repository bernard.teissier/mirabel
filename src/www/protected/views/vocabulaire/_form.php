<?php

/** @var Controller $this */
/** @var Vocabulaire $model */
assert($this instanceof Controller);

$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'vocabulaire-form',
		'enableAjaxValidation' => false,
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'hints' => Hint::model()->getMessages('Vocabulaire'),
	]
);
/** @var BootActiveForm $form */
?>

<?php
echo $form->errorSummary($model);

echo $form->textFieldRow($model, 'titre');
echo $form->textAreaRow($model, 'commentaire', ['cols' => 74, 'rows' => 10, 'class' => 'span6']);
?>
<div class="form-actions">
	<?php
$this->widget(
	'bootstrap.widgets.BootButton',
	[
		'buttonType' => 'submit',
		'type' => 'primary',
		'label' => $model->isNewRecord ? 'Créer ce vocabulaire' : 'Enregistrer',
	]
);
?>
</div>

<?php
$this->endWidget();
?>
