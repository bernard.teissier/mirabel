<?php

/** @var Controller $this */
/** @var Utilisateur $model */
assert($this instanceof Controller);

$this->breadcrumbs[] = 'Nouveau';
?>

<h1>Nouvel utilisateur</h1>

<?php
echo $this->renderPartial('_form', ['model' => $model]);
?>
