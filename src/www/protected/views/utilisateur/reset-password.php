<?php

/** @var Controller $this */
/** @var Login $model */
/** @var bool $misconfigured */
/** @var bool $notfound */

use models\forms\Login;

assert($this instanceof Controller);

$this->pageTitle = Yii::app()->name . ' - Identifiant perdu';
$this->breadcrumbs = [
	'Identifiant perdu',
];
?>

<h1>Demander un nouveau mot de passe</h1>

<p>
	Si vous avez perdu votre mot de passe, indiquez votre adresse électronique ci-dessous
	et un nouveau mot de passe vous sera envoyé par courriel.
</p>

<?php
if ($notfound) {
	echo "<p class=\"alert alert-warning\">Aucun utilisateur non-LDAP ne correspond à cet identifiant.</p>";
}
?>

<div class="form">
<?php
	$form = $this->beginWidget(
	'BootActiveForm',
	[
		'id' => 'login-form',
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'enableClientValidation' => false,
		'hints' => ['username' => "Indiquez ici votre login ou votre adresse électronique."],
		'action' => ['utilisateur/resetPassword'],
	]
);
	/** @var BootActiveForm $form */
	echo $form->errorSummary($model);
	echo $form->textFieldRow($model, 'username');

	if ($misconfigured) {
		echo "<p class=\"alert alert-error\">L'envoi de message n'est actuellement pas actif sur la plate-forme. Nous le réactiverons au plus vite.</p>";
	}
	?>

	<div class="form-actions">
		<?php
		$options = ['class' => 'btn', 'type' => 'submit'];
		if ($misconfigured) {
			$options['disabled'] = "disabled";
		}
		echo CHtml::htmlButton('<i class="icon-ok"></i> Nouveau mot de passe', $options);
		?>
	</div>

<?php $this->endWidget(); ?>
</div>
