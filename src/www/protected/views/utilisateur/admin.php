<?php

/** @var Controller $this */
/** @var UtilisateurSearch $model */
assert($this instanceof UtilisateurController);

$this->breadcrumbs = [
	"admin" => '/admin',
	"utilisateurs",
]
?>

<h1>Administration des utilisateurs</h1>
<p>
	Vous pouvez saisir un opérateur de comparaison
	(<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
	or <b>=</b>) au début de chaque zone de recherche afin de désigner le mode de comparaison.
</p>

<?php
$columns = [
	[
		'name' => 'login',
		'type' => 'raw',
		'value' => function (Utilisateur $data) {
			return CHtml::link($data->login, ['view', "id" => $data->id]);
		},
	],
	[
		'name' => 'partenaireId',
		'value' => function (Utilisateur $data) {
			return CHtml::encode($data->partenaire->getFullName());
		},
		'filter' => CHtml::listData(Partenaire::model()->ordered()->findAll(), 'id', 'nom'),
	],
	[
		'name' => 'actif',
		'type' => 'boolean',
		'filter' => ['Non', 'Oui'],
	],
	'derConnexion:datetime',
	'hdateModif:datetime',
	[
		'name' => 'special',
		'value' => function (Utilisateur $data) {
			return $data->getSpecialities();
		},
		'type' => 'raw',
		'filter' => UtilisateurSearch::$specialValues,
	],
	[
		'name' => 'partenaireType',
		'type' => 'text',
		'filter' => ["demo" => "démo", "editeur" => "éditeur", "normal" => "normal", "provisoire" => "provisoire"],
	],
	[
		'class' => 'BootButtonColumn', // CButtonColumn
		'deleteButtonOptions' => ['title' => "Désactiver cet utilisateur"],
		'deleteConfirmation' => "Voulez-vous vraiment désactiver cet utilisateur ?",
		'updateButtonOptions' => ['title' => "Modifier ce compte"],
	],
];

$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'utilisateur-grid',
		'dataProvider' => $model->search(),
		'filter' => $model, // null to disable
		'columns' => $columns,
		'ajaxUpdate' => false,
	]
);
?>
