<?php

/** @var Controller $this */
/** @var Utilisateur $model */
assert($this instanceof Controller);

$this->pageTitle = "Utilisateur " . CHtml::encode($model->login);
?>

<h1>Utilisateur <em><?php echo CHtml::encode("{$model->nomComplet} ({$model->login})"); ?></em></h1>

<?php
$attributes = [
	'id',
	[
		'label' => $model->getAttributeLabel('partenaireId'),
		'type' => 'raw',
		'value' => $model->partenaire->getSelfLink(),
	],
	'login',
	[
		'name' => 'actif',
		'type' => 'boolean',
		'cssClass' => ($model->actif ? '' : 'warning'),
	],
	'nomComplet',
	'nom',
	'prenom',
	'email' => 'email:email',
	'authLdap:boolean',
	'derConnexion:datetime',
	'hdateCreation:datetime',
	'hdateModif:datetime',
	'permAdmin:boolean',
	'permImport:boolean',
	'permPartenaire:boolean',
	'permIndexation:boolean',
	'permRedaction:boolean',
	'suiviEditeurs:boolean',
	'suiviNonSuivi:boolean',
	'suiviPartenairesEditeurs:boolean',
	[
		'name' => 'listeDiffusion',
		'type' => 'boolean',
		'visible' => Yii::app()->user->checkAccess('utilisateur/liste-diffusion', ['Utilisateur' => $model]),
	],
];
if (Yii::app()->user->id == $model->id) {
	$attributes['email'] = [
		'name' => 'email',
		'type' => 'raw',
		'value' => CHtml::encode($model->email)
			. " " . CHtml::link("Modifier…", ['/utilisateur/update', 'id' => $model->id], ['class' => 'btn']),
	];
}
$this->widget(
	'bootstrap.widgets.BootDetailView',
	[
		'data' => $model,
		'attributes' => $attributes,
	]
);
