<?php

/** @var Controller $this */
/** @var Utilisateur $model */
assert($this instanceof Controller);

/** @var BootActiveForm $form */
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'utilisateur-form',
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'enableAjaxValidation' => false,
		'hints' => Hint::model()->getMessages('Utilisateur'),
		'htmlOptions' => ['class' => 'well'],
	]
);
?>

<?php
echo $form->errorSummary($model);

if (Yii::app()->user->checkAccess('utilisateur/admin')) {
	echo '<p class="help-block">Les champs avec <span class="required">*</span> sont obligatoires.</p>';
	echo $form->checkBoxRow($model, 'actif', ['label-position' => 'left']);
	if (empty($model->partenaireId) || empty($model->partenaire->editeurId)) {
		echo $form->dropDownListRow(
			$model,
			'partenaireId',
			Tools::sqlToPairs("SELECT id, nom FROM Partenaire WHERE type='normal' ORDER BY nom"),
			['class' => 'span6', 'empty' => '']
		);
	}
}
?>
<fieldset class="control-group">
	<legend>Profil</legend>
	<?php
	if (Yii::app()->user->checkAccess('utilisateur/admin')) {
		if (empty($model->partenaireId) || empty($model->partenaire->editeurId)) {
			?>
			<p>
				Si l'authentification (login + mot de passe) passe par LDAP,
				adresse électronique, nom et prénom seront réinitialisés à la prochaine connexion.
			</p>
		<?php
		}
		echo $form->textFieldRow($model, 'nom');
		echo $form->textFieldRow($model, 'prenom');
		echo $form->textFieldRow($model, 'nomComplet', ['class' => 'span6']);
		echo $form->textFieldRow($model, 'login', ['class' => 'span6']);
		Yii::app()->clientScript->registerCoreScript('jquery');
		if ($model->isNewRecord) {
			Yii::app()->clientScript->registerScript(
				'auto-login',
				'var autoLoginUrl = ' . CJSON::encode($this->createAbsoluteUrl('/utilisateur/suggestLogin')) . ";"
			);
		}
		Yii::app()->clientScript->registerScript('nomcomplet', '
$("#Utilisateur_nom, #Utilisateur_prenom").on("keyup", function() {
    $("#Utilisateur_nomComplet").val($("#Utilisateur_prenom").val() + " " + $("#Utilisateur_nom").val());
	if (typeof autoLoginUrl !== "undefined") {
		$.ajax({
			url: autoLoginUrl,
			data: { nom: $("#Utilisateur_nom").val(), prenom: $("#Utilisateur_prenom").val() }
		}).done(function(suggests) {
			if (suggests) {
				$("#Utilisateur_login").val(suggests);
			}
		});
	}
});
');
	}
	echo $form->textFieldRow($model, 'email', ['class' => 'span6']);
	?>
</fieldset>

<fieldset class="control-group">
	<legend>Authentification</legend>
	<p>
		Nouveau mot de passe à enregistrer, à saisir deux fois.
		<?php if (!$model->isNewRecord) {
		echo "Laisser vide pour conserver le mot de passe actuel.";
	} ?>
	</p>
	<?php
	if (Yii::app()->user->checkAccess('utilisateur/admin') && (empty($model->partenaireId) || $model->partenaire->type === 'normal')) {
		echo $form->checkBoxRow($model, 'authLdap', ['label-position' => 'left']);
	}
	unset($model->motdepasse);
	echo $form->passwordFieldRow($model, 'motdepasse', ['class' => 'span6']);
	$model->passwordDuplicate = '';
	echo $form->passwordFieldRow($model, 'passwordDuplicate', ['class' => 'span6']);
	?>
</fieldset>

<?php if (Yii::app()->user->checkAccess('utilisateur/admin')) { ?>
<fieldset class="control-group">
	<legend>Permissions</legend>
	<?php
	echo $form->checkBoxRow($model, 'permAdmin');
	echo $form->checkBoxRow($model, 'permImport');
	echo $form->checkBoxRow($model, 'permPartenaire');
	echo $form->checkBoxRow($model, 'permIndexation');
	echo $form->checkBoxRow($model, 'permRedaction');
	?>
</fieldset>
<fieldset class="control-group">
	<legend>Suivi (droits individuels)</legend>
	<?php
	echo $form->checkBoxRow($model, 'suiviEditeurs');
	echo $form->checkBoxRow($model, 'suiviNonSuivi');
	echo $form->checkBoxRow($model, 'suiviPartenairesEditeurs');
	?>
</fieldset>
<?php } ?>

<?php
if (
	Yii::app()->user->checkAccess('utilisateur/liste-diffusion', ['Utilisateur' => $model])
	&& (empty($model->partenaireId) || $model->partenaire->type === 'normal')
) {
	?>
	<fieldset class="control-group">
		<legend>Listes de diffusion</legend>
		<?= $form->checkBoxRow($model, 'listeDiffusion', ['label' => "Abonné à la liste [mirabel_contenu]"]); ?>
	</fieldset>
	<?php
}
?>

<div class="form-actions">
	<?php
$this->widget(
		'bootstrap.widgets.BootButton',
		[
			'buttonType' => 'submit',
			'type' => 'primary',
			'label' => $model->isNewRecord ? 'Créer' : 'Enregistrer',
		]
	);
?>
</div>

<?php
$this->endWidget();
?>
