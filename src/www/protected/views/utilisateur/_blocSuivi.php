<?php

/** @var Controller $this */
/** @var array $intvByCategory Cf Utilisateur::listInterventionsTrackedByUser() */
/** @var int|null $partenaireId */
assert($this instanceof Controller);

?>
<h3>
	Modifications suivies
	<?php
	if (!empty($partenaireId)) {
		echo "  "
			. CHtml::link(
				'<i class="icon icon-th-list"></i>',
				['/partenaire/tableauDeBord', 'id' => $partenaireId],
				['title' => "Tableau de bord de suivi pour la veille"]
			);
	}
	?>
</h3>
<div class="modifs-suivies">
<?php
$tabsTitles = [
	'Éditeurs A' => ["Interventions sur les éditeurs, en attente de validation"],
	'Éditeurs V' => ["Interventions sur les éditeurs, récemment validées"],
	'En attente' => [
		"Interventions suivies, en attente de validation",
		"Seules les interventions de moins de 3 mois sont répertoriées, pour l'historique consultez la "
			. CHtml::link("page des interventions", ['/intervention/admin', 'q' => ['suivi' => 1, 'statut' => 'attente']]),
	],
	'Validées' => [
		"Interventions suivies, récemment validées",
		"Seules les  interventions de moins de 3 mois sont répertoriées, pour l'historique consultez la "
			. CHtml::link("page des interventions", ['/intervention/admin', 'q' => ['suivi' => 1, 'statut' => 'accepté']]),
	],
	'Non-suivi A' => [
		"Interventions non-suivies, en attente de validation",
		"Seules les interventions de moins d'un an sont répertoriées, pour l'historique consultez la "
			. CHtml::link("page des interventions", ['/intervention/admin', 'q' => ['statut' => 'attente']]),
	],
	'Non-suivi V' => [
		"Interventions non-suivies, récemment validées",
		"Seules les interventions de moins de 3 mois sont répertoriées, pour l'historique consultez la "
			. CHtml::link("page des interventions", ['/intervention/admin', 'q' => ['statut' => 'accepté']]),
	],
	'Partenaires-Éditeurs A' => ["Interventions de Partenaires-Éditeurs, en attente de validation"],
	'Partenaires-Éditeurs V' => ["Interventions de Partenaires-Éditeurs, récemment validées"],
];
$tabs = [];
foreach ($intvByCategory as $category => $intvs) {
	$tabs[] = [
		'label' => $category . ' (' . count($intvs) . ')',
		'content' => $this->renderPartial(
			'/intervention/_list',
			['interventions' => $intvs, 'descriptions' => $tabsTitles[$category]],
			true
		),
		'itemOptions' => [
			'title' => $tabsTitles[$category][0] ?? '',
		],
	];
}
$tabs[0]['active'] = true;
$this->widget(
	'bootstrap.widgets.BootTabbable',
	[
		'type' => 'tabs',
		'placement' => 'above', // 'above', 'right', 'below' or 'left'
		'tabs' => $tabs,
	]
);
unset($tabs);
?>
</div>