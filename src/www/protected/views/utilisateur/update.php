<?php

/** @var Controller $this */
/** @var Utilisateur $model */
assert($this instanceof Controller);

$this->pageTitle = "Utilisateur - modifier";
$this->breadcrumbs[] = 'Modifier';
?>

<h1>Modifier l'utilisateur <em><?= CHtml::encode($model->login); ?></em></h1>

<?php
if ($model->authLdap && !Yii::app()->user->checkAccess('utilisateur/admin')) {
	?>
	<p class="info">
		Vous êtes authentifié par LDAP.
		Par conséquent, votre profil est stocké dans LDAP et ne peut être modifié par Mir@bel.
	</p>
	<?php
} else {
	echo $this->renderPartial('_form', ['model' => $model]);
}
?>
