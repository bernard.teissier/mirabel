<?php

/** @var Controller $this */
/** @var Titre $model */
/** @var Ressource[] $ressources id => Ressource */
/** @var Identification[] $identifications */
/** @var Identification $creation */
assert($this instanceof Controller);

$revueTitles = $model->revue->titres;
$directAccess = !Yii::app()->user->isGuest && Yii::app()->user->checkAccess('suivi', $model);

$this->pageTitle = "Identification / " . $model->titre;
$this->breadcrumbs = [
	'Revues' => ['/revue/index'],
	reset($revueTitles)->getFullTitle() => ['/revue/view', 'id' => $model->revueId],
	"[titre] " . $model->getFullTitle() => ['view', 'id' => $model->id],
	"id internes",
];

$this->menu = [
	['label' => 'Voir ce titre', 'url' => ['view', 'id' => $model->id]],
];
?>

<h1>ID internes pour <em><?= CHtml::encode($model->getFullTitle()) ?></em></h1>

<p>
	Sur cette page, vous pouvez modifier les identifiants locaux à une ressource
	pour le titre <em><?= CHtml::encode($model->getFullTitle()) ?></em>.
<p>
<dl>
	<dt>
		Si la ressource est liée par un accès en ligne,
	</dt>
	<dd>
		elle apparaît en début de liste, avec un formulaire par identification existante
		(donc aucun formulaire s'il y a un accès sans identification).
	</dd>
	<dt>
		Si la ressource déclare déjà une identification,
	</dt>
	<dd>
		mais sans accès en ligne, elle apparaît plus loin dans la liste,
		avec un formulaire par identification existante.
	</dd>
	<dt>
		Si la ressource n'est pas liée à ce titre,
	</dt>
	<dd>
		le formulaire en pied de page permet de la sélectionner (par complétion sur son nom)
		et d'ajouter une identification.
	</dd>
</dl>

<?php
/** @var BootActiveForm $form */
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'identification-form',
		'enableAjaxValidation' => false,
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'hints' => Hint::model()->getMessages('Identification'),
		'htmlOptions' => ['class' => 'well'],
	]
);
$options = ['class' => 'span5'];
foreach ($identifications as $rid => $idents) {
	?>
	<fieldset>
		<legend>
			Ressource <em><?= CHtml::encode($ressources[$rid]->getFullName()) ?></em>
		</legend>
		<?php
		foreach ($idents as $num => $identification) {
			if ($num > 0) {
				// separator
				echo "<hr />";
			}
			echo $form->errorSummary($identification);

			echo $form->hiddenField($identification, "[$rid][$num]ressourceId");
			echo $form->textFieldRow($identification, "[$rid][$num]idInterne", $options);
		}
		?>
	</fieldset>
<?php
}
?>

<fieldset>
	<legend>
		Nouvelle association
	</legend>
	<?php
	echo $form->errorSummary($creation);

	$this->renderPartial(
		'/global/_autocompleteField',
		[
			'model' => $creation, 'attribute' => '[0]ressourceId',
			'url' => '/ressource/ajaxComplete',
			'form' => $form, 'value' => '',
		]
	);
	echo $form->textFieldRow($creation, "[0]idInterne", $options);
	?>
</fieldset>

<div class="form-actions">
	<?php
	$this->widget(
		'bootstrap.widgets.BootButton',
		[
			'buttonType' => 'submit',
			'type' => 'primary',
			'label' => 'Enregistrer',
		]
	);
	?>
</div>

<?php
$this->endWidget();
?>
