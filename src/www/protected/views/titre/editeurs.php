<?php

/** @var Controller $this */
/** @var models\views\titre\Editeurs $model */

assert($this instanceof TitreController);

$this->pageTitle = "Rôles des éditeurs pour " . $model->titre->titre;
$this->breadcrumbs = [
	'Revues' => ['/revue/index'],
	$model->titre->revue->getFullTitle() => ['/revue/view', 'id' => $model->titre->revueId],
	"Rôles des éditeurs",
];

Yii::app()->getClientScript()->registerCssFile('/css/glyphicons.css');
?>
<h1>
	Rôles des éditeurs pour <em><?= CHtml::encode($model->titre->titre) ?></em>
</h1>

<?php
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'titre-editeurs-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => false,
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'hints' => Hint::model()->getMessages('TitreEditeur'),
		'htmlOptions' => ['class' => 'well'],
	]
);
/** @var BootActiveForm $form */
?>

<?php
$roleValues = array_merge(['' => "indéterminé"], TitreEditeur::getPossibleRoles());
$multiple = (count($model->editeurs) > 1 ? 1 : 0);
foreach ($model->editeurs as $editeur) {
	$te = $model->roles[$editeur->id];
	echo '<fieldset class="' . ($te->isComplete() ? "complete" : "incomplete") . '">';
	echo '<legend style="display: flex">';
	echo '<div style="flex: 1">'
		. CHtml::encode($editeur->getFullName())
		. ($multiple ? '  <button type="button" class="toggle-block btn"><span class="glyphicon glyphicon-' . ($te->isComplete() ? "plus" : "minus") . '" aria-hidden="true"></span></button>' : "")
		. "</div>";
	echo "</legend>";
	echo '<div class="fields">';
	echo $form->radioButtonListRow($te, "[{$editeur->id}]ancien", ['' => "<b>indéterminé</b>", '1' => "ancien éditeur", '0' => "éditeur en cours"], ['uncheckValue' => null]);
	echo $form->radioButtonListRow($te, "[{$editeur->id}]commercial", ['' => "<b>indéterminé</b>", '1' => "rôle commercial", '0' => "non"], ['uncheckValue' => null]);
	echo $form->radioButtonListRow($te, "[{$editeur->id}]intellectuel", ['' => "<b>indéterminé</b>", '1' => "rôle intellectuel", '0' => "non"], ['uncheckValue' => null]);
	echo $form->dropDownListRow($te, "[{$editeur->id}]role", $roleValues);
	echo '</div>';
	echo "</fieldset>";
}
?>

<div class="form-actions">
	<button type="submit" class="btn btn-primary">Enregistrer les changements</button>
</div>

<?php
$this->endWidget();

Yii::app()->getClientScript()->registerScript(
	'titre-editeurs',
	<<<EOJS
	if ($multiple) {
		$("fieldset.complete").each(function() {
			$(".fields", this).hide(0);
		});
	}
	$("form").on('click', 'button.toggle-block', function() {
		$(this).closest('fieldset').find(".fields").toggle();
		var btnSpan = $('span', this);
		if (btnSpan.hasClass('glyphicon-minus')) {
			btnSpan.removeClass('glyphicon-minus').addClass('glyphicon-plus');
		} else {
			btnSpan.removeClass('glyphicon-plus').addClass('glyphicon-minus');
		}
	})
	EOJS
);
