<?php

/** @var Controller $this */
/** @var Titre $model */
/** @var array $editeurNew of Editeur */
/** @var bool $direct */
/** @var Intervention $intervention */
/** @var bool $forceProposition */
/** @var array $issns */
assert($this instanceof Controller);

$this->pageTitle = 'Modifier le titre ' . $model->titre;
$revueTitles = $model->revue->titres;
$this->breadcrumbs = [
	'Revues' => ['/revue/index'],
	reset($revueTitles)->getFullTitle() => ['/revue/view', 'id' => $model->revueId],
];
if (Yii::app()->user->isGuest) {
	$this->breadcrumbs[] = "[titre] " . $model->getFullTitle();
} else {
	$this->breadcrumbs["[titre] " . $model->getFullTitle()] = ['view', 'id' => $model->id];
	$this->breadcrumbs[] = 'Modifier';
}

if (!Yii::app()->user->isGuest) {
	$orphan = '';
	foreach ($model->editeurs as $e) {
		$count = TitreEditeur::model()->countByAttributes(['editeurId' => $e->id]);
		if ($count < 2) {
			$orphan .= "\nSupprimer ce titre rendra orphelin l'éditeur : " . $e->nom;
		}
	}
	$this->menu = [
		['label' => 'Voir ce titre', 'url' => ['view', 'id' => $model->id]],
		[
			'label' => 'Changer sa revue',
			'url' => ['changeJournal', 'id' => $model->id],
			'visible' => Yii::app()->user->checkAccess('titre/admin'),
		],
		['label' => 'Supprimer ce titre', 'url' => '#',
			'linkOptions' => [
				'submit' => ['delete', 'id' => $model->id],
				'confirm' => "Êtes-vous certain de vouloir supprimer ce titre ?\n" . $orphan,
			],
		],
		['label' => ' ', 'itemOptions' => ['class' => 'nav-header']],
		['label' => 'Créer un titre (même revue)', 'url' => ['createByIssn', 'revueId' => $model->revueId]],
		['label' => 'Titre — Ressource', 'itemOptions' => ['class' => 'nav-header']],
		[
			'label' => 'Ajouter un accès en ligne',
			'url' => ['/service/create', 'titreId' => $model->id, 'revueId' => $model->revueId],
		],
		[
			'label' => 'Éditer les id internes',
			'url' => ['/titre/identification', 'id' => $model->id],
		],
	];
}
?>

<h1>Modifier le titre <em><?php echo CHtml::encode($model->getFullTitle()); ?></em></h1>

<?php
echo $this->renderPartial(
	'_form',
	[
		'model' => $model,
		'editeurNew' => $editeurNew,
		'revue' => false,
		'direct' => $direct,
		'intervention' => $intervention,
		'forceProposition' => $forceProposition,
		'issns' => $issns,
	]
);
?>
