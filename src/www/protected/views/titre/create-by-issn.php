<?php

/** @var TitreController $this */
/** @var IssnForm $model */
/** @var Revue|null $revue */
assert($this instanceof Controller);

$this->pageTitle = "Nouveau titre";
$this->breadcrumbs = ['Revues' => ['/revue/index']];
if ($revue) {
	$revueTitle = $revue->getFullTitle();
	$this->pageTitle .= " pour " . $revueTitle;
	$this->breadcrumbs[$revueTitle] = ['/revue/view', 'id' => $revue->id];
}
$this->breadcrumbs[] = "Créer un titre par ISSN";
?>

<h1><?= CHtml::encode($this->pageTitle) ?></h1>

<?php
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'issn-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => false,
		'type' => BootActiveForm::TYPE_HORIZONTAL,
	]
);
/** @var BootActiveForm $form */
?>

<div class="alert alert-info">
	Saisissez un ou plusieurs ISSN pour préremplir les données du titre grâce au service Sudoc français,
	ou passez à l'étape suivante si ce titre n'a pas d'ISSN.
	<?php if (!Yii::app()->user->isGuest) { ?>
	Pour mémoire vous pouvez consulter une page récapitulative sur la
	<?= CHtml::link("création de revue", ['/site/page', 'id' => 'Création de revue']) ?>
	dans Mir@bel.
	<?php } ?>
</div>

<?= preg_replace_callback(
	'/\b(\d{4}-\d{3}[\dX])\b/',
	function ($matches) {
		return CHtml::Link($matches[0], ['/revue/viewBy', 'by' => 'issn', 'id' => $matches[1]]);
	},
	$form->errorSummary($model)
) ?>

<div id="titre-issns" class="issn-block">
	<?= $form->hiddenField($model, 'revueId') ?>
	<?= $form->textFieldRow($model, 'issn', ['class' => 'issn-number input-xlarge']) ?>
	<div class="alert alert-error hidden"></div>
</div>

<div class="form-actions">
	<button type="submit" class="btn btn-success" id="create-with-issn" name="ok" value="1">
		Créer un titre avec ces ISSN
	</button>
	<span class="icon-loading masked" style="margin-right: 1ex">
		<?= CHtml::image(Yii::app()->getBaseUrl() . '/images/hourglass.png', 'loading...') ?>
	</span>
	<?= CHtml::link(
	"Créer un titre <strong>sans ISSN</strong>",
	['/titre/create', 'revueId' => $revue ? $revue->id : null],
	['class' => 'btn btn-danger', 'id' => 'create-without-issn']
) ?>
</div>

<div id="sudoc-info">
</div>
<?php
$this->endWidget();

$cs = Yii::app()->getClientScript();
/** @var CClientScript $cs */
$cs->registerScript('create-by-issn', file_get_contents(__DIR__ . '/js/_create-by-issn.js'));
