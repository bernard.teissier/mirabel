<?php

/** @var Controller $this */
/** @var BootActiveForm $form */
/** @var string $position */
/** @var Issn $issn */
assert($this instanceof Controller);

$loadingImg = '<span class="sudoc-by-issn" title="Interroger le SUDOC par ISSN puis PPN">'
	. '→SUDOC <span class="icon-loading hide">'
	. CHtml::image(Yii::app()->getBaseUrl() . '/images/hourglass.png', 'loading...')
	. '</span></span>';
$supportChoices = [
	Issn::SUPPORT_PAPIER => "Papier (ISSN)",
	Issn::SUPPORT_ELECTRONIQUE => "Électronique (ISSN-E)",
	Issn::SUPPORT_INCONNU => '<b style="color:red">&#9888;</b> Indéterminé <b style="color:red">&#9888;</b>',
];
?>
<div class="issn-block well form-compact" data-position="<?= $position ?>">
	<button type="button" class="issn-remove btn btn-danger pull-right" data-name="Issn[<?= $position ?>][_delete]">
		Marquer pour suppression
	</button>
	<?= $form->hiddenField($issn, "[$position]id") ?>
	<?= $form->hiddenField($issn, "[$position]titreId", ['value' => $issn->titreId ?: 0]) ?>
	<?= $form->textFieldRow($issn, "[$position]issn", ['append' => $loadingImg, 'class' => 'issn-number']) ?>
	<?= $form->dropDownListRow($issn, "[$position]statut", Issn::$enumStatut) ?>
	<?= $form->dropDownListRow($issn, "[$position]support", $supportChoices, ['class' => 'issn-support', 'encode' => false]) ?>
	<?php
	// champs plus secondaires
	echo $form->textFieldRow($issn, "[$position]issnl", ['class' => 'issnl']);
	echo $form->textFieldRow($issn, "[$position]dateDebut", ['class' => 'date-debut']);
	echo $form->textFieldRow($issn, "[$position]dateFin", ['class' => 'date-fin']);
	echo $form->textFieldRow($issn, "[$position]titreReference", ['class' => 'titre-reference input-xxlarge']);
	$countries = Tools::sqlToPairs("SELECT code2, nom FROM Pays");
	$countries[''] = '';
	$countries['ZZ'] = "Plusieurs pays";
	if (!isset($countries[$issn->pays])) {
		$issn->pays = '';
	}
	echo $form->dropDownListRow($issn, "[$position]pays", $countries, ['readonly' => 'readonly', 'class' => 'pays']);

	// hors du registre ISSN
	$sudocClass = ['class' => 'sudoc-ppn'];
	if (!empty($issn->sudocNoHolding)) {
		$sudocClass['append'] = "Pas de notice publique dans le Sudoc (no holding)";
	}
	echo $form->textFieldRow($issn, "[$position]sudocPpn", $sudocClass);
	$x = "[$position]sudocNoHolding";
	$noholdingId = CHtml::getIdByName(CHtml::resolveName($issn, $x));
	?>
	<input type="hidden" name="Issn[<?= $issn->id ?>][sudocNoHolding]" value="0" />
	<?php
	$noholdingOptions = [
		'class' => 'sudoc-noholding',
		'hint' => $form->hints['Issn']['sudocNoHolding'] ?? '',
		'label' => "Sans notice SUDOC",
		'uncheckValue' => null,
	];
	if (Yii::app()->user->isGuest) {
		$noholdingOptions['disabled'] = true;
		echo $form->hiddenField($issn, "[$position]sudocNoHolding");
	}
	echo $form->checkBoxRow($issn, "[$position]sudocNoHolding", $noholdingOptions);
	?>

	<?= $form->textFieldRow($issn, "[$position]bnfArk", ['class' => 'bnf-ark']); ?>
	<?= $form->textFieldRow($issn, "[$position]worldcatOcn", ['class' => 'worldcat-ocn']); ?>
</div>
