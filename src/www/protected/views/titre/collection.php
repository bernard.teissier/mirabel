<?php

/** @var Controller $this */
/** @var Titre $model */
/** @var array $collections */
/** @var array $collectionsSansAcces */
assert($this instanceof Controller);

$this->pageTitle = "Collections / " . $model->titre;
$this->breadcrumbs = [
	'Revues' => ['/revue/index'],
	$model->getFullTitle() => ['/revue/view', 'id' => $model->revueId],
	"[titre] " . $model->getFullTitle() => ['view', 'id' => $model->id],
	"Collections",
];

$this->menu = [
	['label' => 'Voir ce titre', 'url' => ['view', 'id' => $model->id]],
];
?>

<h1>Gestion des collections liées à <em><?php echo CHtml::encode($model->getFullTitle()); ?></em></h1>

<?php
if ($collectionsSansAcces) {
	?>
	<h2>Collections liées au titre mais sans accès</h2>
	<p class="alert alert-error">
		Ces collections sont liées au titre, mais leur ressource ne propose pas d'accès sur ce titre.
	</p>
<?php
	echo $this->renderPartial('_collections_table', ['collections' => $collectionsSansAcces]);
}
?>

<h2>Collections avec accès</h2>
<?php
if ($collections) {
	?>
	<p class="alert">
		Cette liste présente toutes les collections des ressources déclarant des accès sur ce titre.
	</p>
	<?php
	echo $this->renderPartial('_collections_table', ['collections' => $collections]);
} else {
	?>
	<p class="alert">
		Aucune collection ne peut être rattachée à ce titre,
		car aucune ressource ne déclare simultanément des accès en ligne sur ce titre et des collections.
	</p>
	<?php
}
