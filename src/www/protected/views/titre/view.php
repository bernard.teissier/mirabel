<?php

/** @var Controller $this */
/** @var Titre $model */
/** @var Intervention $lastUpdate */
/** @var array $autresTitres */
assert($this instanceof Controller);

$revueTitles = $model->revue->titres;
$directAccess = !Yii::app()->user->isGuest && Yii::app()->user->checkAccess('suivi', $model);
$collections = $model->collections;

$this->pageTitle = 'Détails du titre ' . $model->titre;
$this->breadcrumbs = [
	'Revues' => ['/revue/index'],
	reset($revueTitles)->getFullTitle() => ['/revue/view', 'id' => $model->revueId],
	"[titre] " . $model->getFullTitle(),
];

if (!Yii::app()->user->isGuest) {
	$orphan = '';
	foreach ($model->editeurs as $e) {
		$count = TitreEditeur::model()->countByAttributes(['editeurId' => $e->id]);
		if ($count < 2) {
			$orphan .= "\nSupprimer ce titre rendra orphelin l'éditeur : " . $e->nom;
		}
	}
	$this->menu = [
		['label' => 'Modifier ce titre', 'url' => ['update', 'id' => $model->id]],
		[
			'label' => 'Changer sa revue',
			'url' => ['changeJournal', 'id' => $model->id],
			'visible' => Yii::app()->user->checkAccess('titre/admin'),
		],
		['label' => 'Supprimer ce titre', 'url' => '#',
			'linkOptions' => [
				'submit' => ['delete', 'id' => $model->id],
				'confirm' => "Êtes-vous certain de vouloir supprimer ce titre ?\n" . $orphan,
			],
		],
		['label' => ' ', 'itemOptions' => ['class' => 'nav-header']],
		['label' => 'Créer un titre (même revue)', 'url' => ['createByIssn', 'revueId' => $model->revueId]],
		['label' => 'Titre — Ressource', 'itemOptions' => ['class' => 'nav-header']],
		[
			'label' => 'Ajouter un accès en ligne',
			'url' => ['/service/create', 'titreId' => $model->id, 'revueId' => $model->revueId],
		],
		[
			'label' => 'Éditer les id internes',
			'url' => ['/titre/identification', 'id' => $model->id],
		],
		[
			'label' => 'Gérer les collections liées',
			'url' => ['/titre/collection', 'id' => $model->id],
			'visible' => $model->hasPotentialCollections(),
		],
	];
}

?>

<?= $this->renderPartial('/global/_search-navigation', ['searchNavigation' => $this->loadSearchNavigation(), 'currentId' => $model->id, 'attr' => 'titre']); ?>

<h1>Détail du Titre <em><?php echo CHtml::encode($model->getFullTitle()); ?></em></h1>

<?php
$editeurs = [];
$roles = TitreEditeur::getPossibleRoles();
foreach ($model->titreEditeurs as $te) {
	$editeurs[] = '<div>' . $te->editeur->getSelfLink()
		. ($te->ancien === null ? " [éditeur précédent: indéterminé]" : ($te->ancien ? " [éditeur précédent]" : ""))
		. ($te->commercial ? " [commercial]" : "")
		. ($te->commercial ? " [intellectuel]" : "")
		. ($te->role === null ? " [fonction éditoriale indéterminée]" : " [fonction éditoriale: " . $roles[$te->role] . "]")
		. '</div>';
}
$this->widget(
	'bootstrap.widgets.BootDetailView',
	[
		'data' => $model,
		'attributes' => [
			'id',
			'revueId',
			'titre',
			'prefixe',
			'sigle',
			[
				'name' => 'obsoletePar',
				'type' => 'raw',
				'value' => $model->obsoletePar ? $model->obsoleteParTitre->getSelfLink() : '',
			],
			'dateDebut',
			'dateFin',
			[
				'label' => "ISSN etc",
				'type' => 'raw',
				'value' => $this->renderPartial(
					'_issn-table',
					['issns' => $model->issns, 'highlightIssn' => $model->getPreferedIssn()],
					true
				),
			],
			'url:url',
			'urlCouverture:url',
			[
				'name' => 'liensJson',
				'type' => 'raw',
				'value' => $model->getDetailedLinksOther(),
			],
			'periodicite',
			'electronique:boolean',
			[
				'name' => 'langues',
				'value' => \models\lang\Convert::codesToFullNames($model->langues),
			],
			[
				'name' => 'Éditeurs',
				'type' => 'raw',
				'value' => join("\n", $editeurs),
			],
			//'statut',
			[
				'label' => 'Dernière modification',
				'type' => 'raw',
				'value' => $lastUpdate ?
					(Yii::app()->user->isGuest ? Yii::app()->format->formatDatetime($lastUpdate->hdateVal)
						:Chtml::link(Yii::app()->format->formatDatetime($lastUpdate->hdateVal), ['/intervention/view', 'id' => $lastUpdate->id]))
					: '-',
			],
		],
	]
);
?>

<?php if (count($autresTitres) > 1) { ?>
<section id="titre-historique">
	<h2>Historique des titres</h2>
	<ol>
	<?php
	foreach ($autresTitres as $autre) {
		/** @var Titre $autre */
		echo "<li>";
		if ($autre->id == $model->id) {
			echo CHtml::encode($model->getFullTitleWithPerio());
		} else {
			echo CHtml::link(CHtml::encode($autre->getFullTitleWithPerio()), ['titre/view', 'id' => $autre->id]);
		}
		echo "</li>";
	}
	?>
	</ol>
</section>
<?php } ?>

<h2>Accès en ligne</h2>
<?php
$display = new DisplayServices($model->services, Yii::app()->user->getInstitute());
$display->displayAll = true;
$this->renderPartial(
	'/service/_summaryTable',
	[
		'display' => $display,
		'directAccess' => $directAccess,
		'partenaireId' => Yii::app()->user->getInstitute(),
		'publicView' => true,
		'publicViewUrl' => '',
	]
);

if ($collections) {
	echo '<h3 class="collapse-toggle">Collections</h3>'
		. '<div class="collapse-target">'
		. '<table class="table table-striped table-bordered table-condensed">'
		. '<thead><th>Ressource</th><th>Collection</th></thead>'
		. '<tbody>';
	foreach ($collections as $c) {
		echo '<tr>'
			. '<td>' . CHtml::encode($c->ressource->nom) . '</td>'
			. '<td>' . CHtml::encode($c->nom) . '</td>'
			. "</tr>\n";
	}
	echo '</tbody></table>';
	echo "</div>\n";
}
?>

<h2>Suivi</h2>
<?php
$partenaires = $model->getPartenairesSuivant();
if ($partenaires) {
	echo '<ul>';
	foreach ($partenaires as $partenaire) {
		echo '<li>' . $partenaire->getSelfLink() . ' suit la revue de ce titre, dans Mir@bel.</li>';
	}
	echo "</ul>\n";
} else {
	echo "<p>Ce titre n'est pas suivi dans Mir@bel.</p>";
}
?>

<h2>Possession</h2>
<?php
$owners = $model->getOwners();
if (empty($owners)) {
	echo "<p>Aucun institut ne possède ce titre.</p>";
} else {
	echo "<ul>";
	/** @var Partenaire $owner */
	foreach ($owners as $owner) {
		$url = $owner->buildPossessionUrl($model);
		echo "<li>"
			. ($url
				? CHtml::encode($owner->getFullName()) . " " . CHtml::link("[Notice]", $url)
				: CHtml::encode($owner->getFullName()))
			. "</li>";
	}
	echo "</ul>";
}
?>

<?php
$this->renderPartial('/global/_modif-verif', ['target' => $model, 'rssTarget' => $model->revue]);
