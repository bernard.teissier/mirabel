function validateIssn(issn) {
	return issn.match(/^\d{4}-?\d{3}[\dX]$/);
}

function makePpnInfoPrinter(block, controls) {
	return function (info) {
		block.find('.sudoc-info').remove();
		var msgHtml =
			'<div class="sudoc-info">'
			+ '<p>Infos associées au PPN <a target="_blank" href="https://www.sudoc.fr/' + info.ppn + '">' + info.ppn + '</a>'
			+ ' ('+ (info.support === 'inconnu' ? "<b>support indéterminé</b>" : info.support) + ')</p>'
			+ '<b>Titre réf.</b> : ' + htmlEscape(info.titreIssn)
			//+ '<br><b>Dates</b> : ' + (info.dateDebut || '') + " — " + (info.dateFin || '')
			+ (info.worldcat ? '<br><b>Worldcat</b> : ' +  info.worldcat : '')
			//+ '<br><b>Langue</b> : <span class="lang">' + (info.titre.langues || '') + "</span>"
			+ "</div>";
		var msg = $(msgHtml);
		msg.appendTo(controls);
		if ($('.titre-reference', block).length > 0) {
			setIssnData({container: block, notice: info});
		}
	};
}

function setIssnData(data) {
	var info = data.notice;
	var container = data.container;
	container.find('.previous-value').remove();
	if (info.support && info.support !== 'inconnu') {
		changeField($('.issn-support', container), info.support);
	}
	changeField($('.sudoc-ppn', container), info.ppn);
	changeField($('.sudoc-noholding', container), info.sudocNoHolding);
	changeField($('.bnf-ark', container), info.bnfArk || null);
	changeField($('.worldcat-ocn', container), info.worldcat || null);
	changeField($('.issn-number', container), info.issn || null);
	changeField($('.issnl', container), info.issnl || null);
	changeField($('.pays', container), info.pays ? (info.pays).toUpperCase() : null);
	changeField($('.titre-reference', container), info.titreIssn || null);
	if (info.dateDebut) {
		changeField($('.date-debut', container), info.dateDebut || '');
	}
	if (info.dateFin) {
		changeField($('.date-fin', container), info.dateFin || '');
	}

	markTitleFields();

	function changeField(field, value) {
		if (value === null || typeof value === 'undefined') {
			return;
		}
		if (field.attr('type') === 'checkbox') {
			var wasChecked = field.prop('checked');
			var toCheck = Boolean(value);
			if (wasChecked !== toCheck) {
				field.prop('checked', toCheck);
				markChange();
			}
		} else {
			var previousValue = field.val();
			if (previousValue !== value) {
				field.val(value);
				markChange(previousValue);
			}
		}

		function markChange(previousValue) {
			field.closest('.control-group').addClass('success');
			if (previousValue) {
				field.closest('.controls').append(' <span class="previous-value">[auparavant : <em>' + htmlEscape(previousValue) + '</em>]</span>');
			}
			const wrapper = document.querySelector('#issn-changed-by-sudoc');
			if (wrapper) {
				wrapper.classList.remove('hidden');
				document.querySelector('#change-issns').click();
			}
		}
	}
}

function markTitleFields() {
	markStartDate();
	markEndDate();
	markLang();
	function markStartDate() {
		var dates = $('#titre-issns .date-debut')
			.map(function(index, e) { return $(e).val().trim(); })
			.toArray()
			.filter(function(v) { return v !== ''; });
		if (dates.length) {
			var dateDebutMin = dates.reduce(function(a, b) {
				return (a > b ? b : a);
			});
			if (dateDebutMin > 0) {
				appendAddOn('#Titre_dateDebut', dateDebutMin);
			} else if (dateDebutMin < 0) {
				appendAddOnInfo('#Titre_dateDebut', "date imprécise");
			}
		}
	}
	function markEndDate() {
		var dateFinMax = null;
		var issnBlocks = document.querySelectorAll('#titre-issns .issn-block');
		for (var i = 0; i < issnBlocks.length; ++i) {
			var dateDebut = issnBlocks.item(i).querySelector('.date-debut').value.trim();
			if (dateDebut !== '') {
				var dateFin = issnBlocks.item(i).querySelector('.date-fin').value.trim();
				if (dateFin.match(/X/)) {
					appendAddOnInfo('#Titre_dateFin', "date imprécise");
					return;
				}
				if (dateFin === '') {
					dateFinMax = dateFin;
					break;
				}
				if (dateFin > dateFinMax) {
					dateFinMax = dateFin;
				}
			}
		}
		if (dateFinMax !== null) {
			appendAddOn('#Titre_dateFin', dateFinMax);
		}
	}
	function markLang() {
		var lang = $('.sudoc-info .lang')
			.map(function(index, e) { return $(e).text().trim(); })
			.toArray()
			.filter(function(v) { return v !== ''; });
		if (lang.length > 0) {
			appendAddOn('#Titre_langues', lang[0]);
		}
	}
	function appendAddOn(selector, value) {
		var button = '';
		if (value !== $(selector).val()) {
			button = ' <button class="btn btn-mirabel sudoc-apply" type="button">Appliquer la valeur Sudoc</button>';
		}
		if (!$(selector).parent().hasClass('input-append')) {
			$(selector).wrap('<div class="input-append"></div>');
		} else {
			$(selector).parent().find('span.add-on, button').remove();
		}
		$(selector)
			.after('<span class="add-on sudoc-value" data-value="' + value + '" title="Informations déduites de l´interrogation SUDOC, à partir des ISSN renseignés">Sudoc "' + value + '"</span>' + button);
	}
	function appendAddOnInfo(selector, textInfo) {
		if (!$(selector).parent().hasClass('input-append')) {
			$(selector).wrap('<div class="input-append"></div>');
		} else {
			$(selector).parent().find('span.add-on, button').remove();
		}
		$(selector)
			.after('<span class="add-on sudoc-value" title="Informations déduites de l´interrogation SUDOC, à partir des ISSN ou PPN renseignés">Sudoc "' + textInfo + '"</span>');
	}
}

function findHoldingNotice(notices) {
	for (let notice of notices) {
		if (!notice.sudocNoHolding) {
			return notice;
		}
	}
	return null;
}

// *** for event handlers ***

function updateThroughSudoc(criteria, block, controls, inputField) {
	console.info("sudocByIssn() : ", criteria, " & inputField ", inputField);
	controls.find("span.help-inline.error").remove();
	controls.closest(".control-group").find(".alert").remove();
	block.find(".sudoc-info").remove();
	if (criteria.issn && !validateIssn(criteria.issn)) {
		$('<span class="help-inline error">Cet ISSN n´est pas valide (format 1234-123X attendu).</span>').appendTo(controls);
		return;
	} else if (criteria.ppn && !criteria.ppn.match(/^\d+[\dX]$/)) {
		$('<span class="help-inline error">ISSN vide et PPN non-valide. L´interrogation du Sudoc est impossible.</span>').appendTo(controls);
		return;
	}
	var icon = controls.find('.icon-loading');
	icon.show(0);

	$.ajax({
		type: 'GET',
		url: '/titre/ajaxSudoc',
		data: criteria
	}).done(function (data) {
		var msg;
		var callback = makePpnInfoPrinter(block, controls);
		if (!data.notices || data.notices.length === 0) {
			callback = null;
			msg = $('<div class="alert alert-warning">'
				+ "Réponse sans erreur mais sans PPN donc sans notice du Sudoc. Si vous pensez que c'est une erreur, contactez un responsable de Mir@bel."
				+ '</div>'
			);
		} else if (data.notices.length > 1) {
			let selectedNotice = findHoldingNotice(data.notices);
			if (selectedNotice !== null) {
				data.notices = [selectedNotice];
			}
			msg = $(
				'<div class="alert alert-warning">'
				+ "Plusieurs notices pour cet ISSN, "
				+ (selectedNotice === null ? "toutes non-localisées. La première listée est utilisée." : "la première localisée a été choisie.")
				+ '</div>'
			);
		} else if (!data.notices[0].ppn) {
			msg = $('<em style="margin-left: 2ex">Aucun PPN associé selon Sudoc.</em>');
		}
		// only the first notice
		if (callback !== null) {
			callback.call(this, data.notices[0]);
		}

		if (msg) {
			msg.appendTo(controls);
			msg.fadeOut(6000, function() { $(this).remove(); });
		}
	}).fail(function (jqXHR, textStatus, errorThrown) {
		var msg = '<div class="alert alert-warning">';
		if (!jqXHR.status || jqXHR.status >= 500) {
			msg += "Le web-service proxy de Mirabel sur le Sudoc a rencontré une erreur : <strong>"
				+ htmlEscape(jqXHR.responseText)
				+ "</strong></div>";
		} else if (jqXHR.status >= 400 && jqXHR.status < 500) {
			msg += "<p>Cet ISSN est introuvable dans le Sudoc. Vérifiez que votre ISSN est correctement saisi.</p>";
			if (document.querySelector('#create-without-issn')) {
				msg += "<p>Si vous préférez créer le titre sans passer par l'import Sudoc, utilisez le bouton « Créer un titre sans ISSN »</p>";
			}
		} else {
			msg += "Erreur en interrogeant le Sudoc (" + jqXHR.status + " — " + errorThrown + ")."
				+ " Vérifiez que votre ISSN est correctement saisi.";
		}
		msg += '</div>';
		$(msg).insertAfter(controls);
		//msg.fadeOut(6000, function() { $(this).remove(); });
	}).always(function() {
		icon.hide(0);
	});
}


// *** event handlers ***

/**
 * clickSudocByIssn is an event handler that will update the HTML of the ISSN block
 * (containing the event target) with the data returned by /titre/ajaxSudoc.
 */
function clickSudocByIssn() {
	var inputField = $(this).closest('.controls').find('.issn-number').first();
	var block = inputField.closest('.issn-block');
	var controls = inputField.closest('.controls');
	var issn = inputField.val().trim().toUpperCase();
	if (issn === '') {
		var ppn = inputField.closest('.issn-block').find('.sudoc-ppn').first().val();
		if (ppn) {
			updateThroughSudoc({ ppn: ppn }, block, controls, inputField);
		}
	} else {
		updateThroughSudoc({ issn: issn }, block, controls, inputField);
	}
}

/**
 * onIssnInput is an event handler that will update the HTML of the ISSN block
 * (if the value of the event target is a valid ISSN and has changed)
 * with the data returned by /titre/ajaxSudoc.
 */
function onIssnInput() {
	var inputField = $(this);
	var issn = inputField.val().trim().toUpperCase();
	if (inputField.val() !== issn) {
		inputField.val(issn); // cleanup value
	}
	if (!validateIssn(issn)) {
		return;
	}
	if (issn === inputField.data('lastIssn')) {
		return;
	}
	inputField.data('lastIssn', issn);

	var block = inputField.closest('.issn-block');
	var controls = inputField.closest('.controls');
	updateThroughSudoc({ issn: issn }, block, controls, inputField);
}


// *** events ***

$('#more-issn').on('click', function() {
	var blocks = document.querySelectorAll('.issn-block');
	var titreId = document.querySelector('#titre-issns').getAttribute('data-titreId');
	var position = blocks.item(blocks.length - 1).getAttribute('data-position');
	var num = position.match(/^new(\d+)/);
	if (num) {
		position = "new" + (parseInt(num[1]) + 1);
	} else {
		position = "new0";
	}
	$.ajax({
		type: 'GET',
		url: '/titre/ajaxIssn',
		data: { position: position, titreId: titreId },
		success: function(data) {
			$('#titre-issns > .issn-block').last().after(data);
		}
	});
});

$('#change-issns').on('click', function() {
	$('#titre-issns.hidden').removeClass('hidden');
	$('#change-issns').hide();
});

$('#titre-issns').on('input', 'input.issn-number', onIssnInput);
$('#titre-issns').on('click', '.sudoc-by-issn', clickSudocByIssn);
$('#titre-issns').on('click', '.toggle-details', function() {
	$(this).closest('.issn-block').find('.issn-advanced').show();
	$(this).hide();
});
$('#titre-issns').on('click', '.issn-remove', function() {
	var block = $(this).closest('.issn-block');
	var name = $(this).attr('data-name');
	var issn = block.find('.issn-number').first().val();
	block.attr('data-delete', issn)
		.hide()
		.after('<p class="alert alert-danger">' +
			'<input name="' + name + '" value="1" type="hidden" />' +
			'Cet ISSN ' + issn + ' sera supprimé lorsque le formulaire sera envoyé et appliqué. ' +
			'<button type="button" class="issn-restore btn btn-default" data-issn="' + issn + '">Restaurer</button>' +
			'</p>'
		);
});
$('#titre-issns').on('click', '.issn-restore', function() {
	var par = $(this).closest('.alert');
	var issn = $(this).attr('data-issn');
	var block = par.siblings('.issn-block[data-delete="' + issn + '"]').first();
	block.show().removeAttr('data-delete');
	par.remove();
});

$('#Titre_sudoc').on('input', function() {
	$('#sudoc-info').remove();
});

$('#titre-form').on('click', 'button.sudoc-apply', function() {
	var v = $(this).parent().find('.sudoc-value').attr('data-value');
	$(this).parent().find('input').val(v);
});
