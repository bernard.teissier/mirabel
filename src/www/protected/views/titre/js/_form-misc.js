$('#Titre_titre').on("input", function() {
	var content = $('#Titre_titre').val();
	if (content.match(/^(Les? |La |L'|L’|De |The |Der |Die |El )/)) {
		if ($("#Titre_titre_hint").length === 0) {
			$("#Titre_titre").closest('.control-group').after('<div class="js-hint alert alert-warning" id="Titre_titre_hint"></div>');
		}
		$('#Titre_titre_hint').text("Ce titre semble utiliser un préfixe, ce qui nuira au tri des titres. Utiliser le champ Préfixe si possible.");
		window.setTimeout(function(){ $('#Titre_titre_hint').remove(); }, 5000);
	}
});

$('#Titre_urlCouverture').on("focus", function() {
	$('#update-image').prop("checked", "checked");
});
