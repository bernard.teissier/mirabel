$('#submit-editeur').click(function() {
	$('#editeur-form').submit();
});

var lastEditeurHash = "";
$('#modal-editeur-create').on('submit', '#editeur-form', function() {
	if ($("#editeur-duplicate-id").val() === lastEditeurHash) {
		console.log("Demande déjà envoyée, ignore la soumission");
		return;
	}
	lastEditeurHash = $("#editeur-duplicate-id").val();
	$.ajax({
		type: 'POST',
		url: $(this).attr("action"),
		data: $(this).serialize(),
		success: function (data) {
			if (data.result === 'success') {
				$('#modal-editeur-create').modal('hide');
				addNewEditeur(data.id, data.label);
				$("#modal-editeur-create-link").closest('.controls').append(
					'<div class="alert alert-success">' + data.message + '</div>'
				);
			} else {
				lastEditeurHash = "";
				$('#modal-editeur-create .modal-body').html(data.html);
			}
		},
		dataType: 'json'
	});
	return false; // prevent normal submit
});

function addNewEditeur(id, label) {
	$('#TitreEditeurNew').clone()
		.find('label.editeur-nom').append(label).end()
		.find('label.editeur-nom input').val(id).attr('checked', 'checked').end()
		.find('a[rel="popover"]').addClass("titre-editeurs-hint").end()
		.html(function(index, oldhtml) {
			return oldhtml.replaceAll('TitreEditeurNew[0]', 'TitreEditeurNew[' + id + ']');
		})
		.insertBefore('#TitreEditeurComplete').removeClass('hidden');
	$('a.titre-editeurs-hint').filter(function(index) { return index > 0; }).remove();
	$('a.titre-editeurs-hint').popover();
	return true;
}
