<?php

/** @var TitreController $this */
/** @var \models\sudoc\Notice $notice */
/** @var Issn[] $issns */
/** @var Titre $titre */
assert($this instanceof Controller);

$titreInfo = [];
$labelsT = Titre::model()->attributeLabels();
foreach ($titre->getAttributes() as $k => $v) {
	if ($v !== '' && $v !== null) {
		$titreInfo[$titre->getAttributeLabel($k)] = $v;
	}
}

?>
<div>
	<div style="text-align: center">
		<strong>Données que Mir@bel proposera à l'insertion</strong>
	</div>
	<div class="row-fluid">
		<div class="span6">
			<div>
				<strong class="label">Titre</strong>
				<?= HtmlTable::buildFromAssoc($titreInfo)->toHtml() ?>
			</div>
			<?php
			if ($notice->url) {
				?>
			<div>
				<strong class="label">infos non reprises</strong>
				<table class="table table-bordered table-condensed">
					<tbody>
						<tr>
							<th>URL selon le Sudoc</th>
							<td><?= CHtml::link(CHtml::encode($notice->url), $notice->url, ['target' => '_blank']) ?> [nouvel onglet]</td>
						</tr>
					</tbody>
				</table>
			</div>
				<?php
			}
			?>
		</div>
		<div class="span6">
			<?php
			foreach ($issns as $issn) {
				?>
			<div>
				<strong class="label">Issn</strong>
				<?php
				$this->widget(
					'bootstrap.widgets.BootDetailView',
					[
						'data' => $issn,
						'attributes' => [
							'issn',
							[
								'name' => 'statut',
								'value' => Issn::$enumStatut[$issn->statut],
							],
							[
								'name' => 'support',
								'value' => Issn::$enumSupport[$issn->support],
							],
							'issnl',
							'dateDebut',
							'dateFin',
							[
								'name' => 'country',
								'value' => $issn->getCountry(),
							],
							'titreReference',
							[
								'name' => 'sudocPpn',
								'label' => 'PPN',
								'type' => 'raw',
								'value' => $issn->getSudocLink(false),
							],
							'sudocNoHolding:boolean',
							'bnfLink:raw:BNF Ark',
							'worldcatLink:raw:Worldcat',
						],
						'htmlOptions' => [
							'class' => "table table-striped table-bordered table-condensed",
						],
						'hideEmptyLines' => true,
					]
				); ?>
			</div>
				<?php
			}
			?>
		</div>
	</div>
</div>
