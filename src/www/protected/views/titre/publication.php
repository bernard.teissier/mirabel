<?php

use models\sherpa\Publication;

/** @var Controller $this */
/** @var string $doajUrl */
/** @var \models\sherpa\Publication $publication */
/** @var Titre $titre */

assert($this instanceof TitreController);

$this->pageTitle = 'Politique de publication pour ' . $titre->titre;
$this->breadcrumbs = [
	'Revues' => ['/revue/index'],
	$titre->revue->getFullTitle() => ['/revue/view', 'id' => $titre->revueId],
	"Politique de publication",
];

Yii::app()->getClientScript()->registerCssFile('/css/glyphicons.css');

$intro = preg_replace(
	'/{{SHERPA_LINK (.+?)}}/',
	"<a href=\"$publication->romeoUrl\">$1</a>",
	str_replace(
		['{{SHERPA_URL}}'],
		[$publication->romeoUrl],
		Config::read("sherpa.intro")
	)
);
$mirabelRevueUrl = $this->createUrl('/revue/view', ['id' => $titre->revueId, 'nom' => Norm::urlParam($titre->getFullTitle())]);
?>
<h1>
	<?= CHtml::encode($titre->getFullTitle()) ?>
	<a href="<?= CHtml::encode($mirabelRevueUrl) ?>" title="Voir la page principale de cette revue dans Mir@bel">
		<?= CHtml::image(Yii::app()->getBaseUrl() . '/images/logo-mirabel-carre.png', "Mir@bel", ['width' => 24, 'height' => 24]) ?>
	</a>
	<div><small>Politique en matière de droits, de dépôt et d'auto-archivage</small></div>
</h1>

<?php if ($doajUrl) { ?>
<p>
	Revue en accès ouvert, voir <a href="<?= CHtml::encode($doajUrl) ?>" title="Directory of Open Access Journals">DOAJ</a>.
</p>
<?php } ?>

<div class="alert alert-info">
	<?= $intro ?>
</div>

<div class="sherpa-oa">
	<?php
	Yii::app()->getClientScript()->registerScript(
		'popover', // https://getbootstrap.com/2.3.2/javascript.html#popovers
		<<<EOJS
		$('.help-popover').popover({
			container: 'body',
			trigger: 'click',
			html: true,
		}).on('click', function(event) {
			var target = $(this);
			$("body").one("click", function(e) {
				target.popover('hide');
			});
			event.preventDefault();
			return false;
		});
		EOJS
	);
	$popoverOptions = ['class' => 'help-popover'];

	$helpEmbargo = \Config::read('sherpa.aide.embargo');
	if ($helpEmbargo) {
		$helpEmbargoLink = ' '
			. CHtml::link("(aide)", "#", array_merge($popoverOptions, ['data-title' => "Explication des embargos", 'data-content' => $helpEmbargo]));
	} else {
		$helpEmbargoLink = "";
	}
	unset($helpEmbargo);

	$helpFee = \Config::read('sherpa.aide.frais');
	if ($helpFee) {
		$helpFeeLink = CHtml::link(
			"(aide)",
			"#",
			array_merge($popoverOptions, ['data-title' => "Explication des frais additionnels", 'data-content' => $helpFee])
		);
	} else {
		$helpFeeLink = "";
	}
	unset($helpFee);

	$helpLicense = \Config::read('sherpa.aide.licence');
	if ($helpLicense) {
		$helpLicenseLink = ' '
			. CHtml::link("(aide)", "#", array_merge($popoverOptions, ['data-title' => "Pour en savoir plus sur les licences…", 'data-content' => $helpLicense]));
	} else {
		$helpLicenseLink = "";
	}
	unset($helpLicense);

	$versionsTitles = [
		'published' => "Version publiée",
		'accepted' => "Version acceptée",
		'submitted' => "Version soumise",
	];
	if (!$publication->policy) {
		echo "<p class=\"alert alert-error\">Cette revue n'a pas de politique connue pour son mode de publication.</p>";
	}
	foreach ($versionsTitles as $version => $versionFr) {
		$oas = $publication->getOaByVersion($version);
		echo '<section>';
		$help = \Config::read("sherpa.aide.version.$version");
		if ($help) {
			$helpLink = CHtml::link("(aide)", "#", array_merge($popoverOptions, ['data-title' => "$versionFr", 'data-content' => $help]));
		} else {
			$helpLink = "";
		}
		if (count($oas) === 0) {
			$summary = '<span class="oa-summary"><span class="glyphicon glyphicon-remove" title="Non autorisée"></span></span>';
			echo CHtml::tag(
				'h2',
				['class' => 'collapse-toggle', 'data-collapse-target' => "#version-$version"],
				"$versionFr $summary $helpLink"
			);
			echo '<div class="collapse-target" id="version-' . $version . '">';
		} elseif (count($oas) === 1) {
			echo CHtml::tag(
				'h2',
				['class' => 'collapse-toggle', 'data-collapse-target' => "#version-$version"],
				"$versionFr {$oas[0]->getSummaryIcons()} $helpLink"
			);
			echo '<div class="collapse-target" id="version-' . $version . '">';
		} else {
			echo CHtml::tag(
				'h2',
				['class' => 'collapse-toggle', 'data-collapse-target' => ".version-$version"],
				"$versionFr $helpLink");
			echo '<div class="oa-options" id="version-' . $version . '">';
		}
		unset($help);

		if (!$oas) {
			?>
			<table class="table-condensed table-hover <?= (count($oas) > 1 ? "collapse-target version-$version" : '') ?>">
				<tbody>
					<tr>
						<td>
							<span class="glyphicon glyphicon-remove" title="Non autorisée"></span>
							<strong>Diffusion non autorisée</strong>
						</td>
					</tr>
				</tbody>
			</table>

			<?php
		}
		foreach ($oas as $num => $oa) {
			/** @var \models\sherpa\Oa $oa */
			if (count($oas) > 1) {
				echo "<div>";
				echo CHtml::tag(
					'h3',
					['class' => 'collapse-toggle-show', 'data-collapse-target' => ".version-$version"],
					"Option " . ($num + 1) . $oa->getSummaryIcons()
				);
			} ?>
			<table class="table-condensed table-hover <?= (count($oas) > 1 ? "collapse-target version-$version" : '') ?>">
				<tbody>
					<?php if (in_array('this_journal', $oa->locations) || in_array('any_website', $oa->locations)) { ?>
					<tr>
						<td>
							<span class="glyphicon glyphicon-eye-open"></span>
							<?php
							if (in_array('this_journal', $oa->locations) && in_array('any_website', $oa->locations)) {
								echo "Publication en accès ouvert sur le site de l'éditeur et autorisée sur tout site web";
							} elseif (in_array('this_journal', $oa->locations)) {
								echo "Publication en accès ouvert sur le site de l'éditeur";
							} else {
								echo "Publication autorisée sur tout site web";
							}
							?>
							<a href="<?= CHtml::encode($titre->url) ?>"><span class="glyphicon glyphicon-link"></span></a>
						</td>
					</tr>
					<?php } ?>
					<?php if ($oa->additionalOaFee || $version === 'published') { ?>
					<tr>
						<td>
							<span class="glyphicon glyphicon-euro<?= ($oa->additionalOaFee ? "" : " glyphicon-slashed") ?>"></span>
							<strong><?= ($oa->additionalOaFee ? "Frais additionnels de publication" : "Pas de frais additionnels de publication") ?></strong>
							<?= $helpFeeLink ?>
						</td>
					</tr>
					<?php } ?>
					<tr>
						<td>
							<span class="glyphicon glyphicon-hourglass<?= (empty($oa->embargo) ? " glyphicon-slashed" : "") ?>"></span>
							<strong><?= (empty($oa->embargo) ? "Pas d'embargo" : "Embargo de {$oa->getPrintableEmbargo()}") ?></strong>
							<?= $helpEmbargoLink ?>
						</td>
					</tr>
					<?php if ($oa->licenses) { ?>
					<tr>
						<td>
							<span class="cc-icon"><img src="/images/icons/cc.svg" width=14 height=14 /></span>
							Licence Creative Commons <strong><?= join("</strong> ou <strong>", $oa->licenses) ?></strong>
							<?= $helpLicenseLink ?>
						</td>
					</tr>
					<?php } ?>
					<?php if ($oa->copyrightOwner) { ?>
					<tr>
						<td>
							<span class="glyphicon glyphicon-copyright-mark"></span>
							Détenteur des droits :
							<strong><?= $oa->getPrintableCopyrightOwner() ?></strong>
						</td>
					</tr>
					<?php } ?>
					<?php
					$locations = $oa->getPrintableLocations('');
					if ($locations) { ?>
					<tr>
						<td>
							<span class="glyphicon glyphicon-education"></span>
							Emplacements autorisés :
							<div class="block-value"><?= join("<br>", $locations) ?></div>
						</td>
					</tr>
					<?php } ?>
					<?php if ($oa->conditions) { ?>
					<tr>
						<td>
							<span class="glyphicon glyphicon-list"></span>
							Conditions :
							<div class="block-value"><?= join("<br>", $oa->getPrintableConditions()) ?></div>
						</td>
					</tr>
					<?php } ?>
					<?php if ($oa->prerequisites) { ?>
					<tr>
						<td>
							<span class="glyphicon glyphicon-check"></span>
							Il existe des prérequis pour cette option, voir <?= CHtml::link("Sherpa Romeo", $publication->romeoUrl) ?>.
						</td>
					</tr>
					<?php } ?>
					<?php if ($oa->publicNotes) { ?>
					<tr>
						<td>
							<span class="glyphicon glyphicon-edit"></span>
							Des remarques sont présentes pour cette option, voir <?= CHtml::link("Sherpa Romeo", $publication->romeoUrl) ?>.
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
			<?php
			if (count($oas) > 1) {
				echo "</div>"; // option
			}
		}
		echo '</div>'; // .oa-options
		echo '</section>'; // version
	}
	?>
</div>

<div class="sherpa-general">
	<h2>Informations sur la politique de publication</h2>
	<?php
	$openAccess = $publication->getOpenAccess();
	if ($openAccess === Publication::ACCESS_CLOSED) {
		echo "<div>L'éditeur refuse l'accès ouvert.</div>";
	} elseif ($openAccess === Publication::ACCESS_MIXED) {
		echo "<div>L'éditeur peut refuser l'accès ouvert.</div>";
	}
	?>
	<div>
		<ul>
			<?php
			foreach ($publication->getResourceUrls() as $name => $url) {
				echo CHtml::tag('li', [], CHtml::link($name, $url));
			}
			?>
		</ul>
	</div>
</div>

<?php
if (
	(count($publication->preferredTitle) > 1)
	|| (count($publication->preferredTitle) > 0 && $publication->preferredTitle !== $titre->titre)
	) {
	?>
<div class="sherpa-titles" style="margin-top: 2ex">
	<h2>Titre et éditeur dans Sherpa Romeo</h2>
	<ul>
	<?php
	foreach ($publication->preferredTitle as $title) {
		echo '<li>' .  CHtml::encode($title) . '</li>';
	}
	?>
	</ul>
	<?php
	if ($publication->publishers) {
		echo "<p>Publié par :";
		echo '<ul class="editeurs">';
		foreach ($publication->publishers as $publisher) {
			echo '<li>';
			echo CHtml::encode($publisher->name[0]);
			if ($publisher->relationship) {
				echo ", ", $publisher->getPrintableRelationship();
			}
			echo '</li>';
		}
		echo '</ul>';
	}
	?>
	<div>
		<?php
		if ($publication->dateModified) {
			echo "Dernière modification : {$publication->dateModified}";
		}
		?>
	</div>
	<div>
		Voir la page <?= CHtml::link("Sherpa Romeo", $publication->romeoUrl) ?> pour cette revue.
	</div>
</div>
<?php
}
?>
<p>
	<a href="<?= CHtml::encode($mirabelRevueUrl) ?>" title="Voir cette revue dans Mir@bel">
		<?= CHtml::image(Yii::app()->getBaseUrl() . '/images/logo-mirabel-carre.png', "Mir@bel", ['width' => 16, 'height' => 16]) ?>
	</a>
	Voir la <a href="<?= CHtml::encode($mirabelRevueUrl) ?>" title="Revue <?= CHtml::encode($titre->getFullTitle()) ?>">page principale</a> pour cette revue.
</p>
