<?php

/** @var Controller $this */
/** @var array $collections */
assert($this instanceof Controller);

?>
<table class="table table-bordered">
	<thead>
		<tr>
			<th>Ressource</th>
			<th>Nom</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($collections as $c) {
			/** @var CollectionListItem $c */
			echo '<tr' . ($c->linked ? ' class="success"' : '')
				. '><td>' . CHtml::encode($c->ressourceNom)
				. '</td><td>' . CHtml::encode($c->collection->nom)
				. '</td><td>';
			echo '<form action="" method="post">';
			if ($c->linked) {
				echo CHtml::hiddenField('delete', $c->collection->id);
				echo '<button type="submit">Enlever</button>';
			} else {
				echo CHtml::hiddenField('add', $c->collection->id);
				echo '<button type="submit">Ajouter à cette collection</button>';
			}
			echo '</form>';
			echo '</td></tr>';
		}
		?>
	</tbody>
</table>
