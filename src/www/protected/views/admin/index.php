<?php

/** @var Controller $this */

assert($this instanceof Controller);

$this->pageTitle = "Administration";
$this->breadcrumbs = [
	'Administration de Mir@bel',
];
?>

<h1>
	Administration de Mir@bel
	<small>(permission <em>admin</em> requise)</small>
</h1>

<div class="span5 well">
	<h2>Paramètres</h2>
	<ul>
		<li><?= CHtml::link("Messages de suivi", ['/config', 'Config' => ['category' => 'alerte']]) ?></li>
		<li><?= CHtml::link("Serveur e-mail", ['/config', 'Config' => ['category' => 'email']]) ?> (SMTP…)</li>
		<li><?= CHtml::link("Sherpa", ['/config', 'Config' => ['category' => 'sherpa']]) ?></li>
		<li><?= CHtml::link("Autres réglages", ['/config', 'Config' => ['category' => 'autres']]) ?> (contact, RSS…)</li>
		<li><?= CHtml::link("Paramètres fixés par le fichier <code>config/local.php</code>", ['/config/index']) ?></li>
	</ul>
</div>

<div class="span5 well">
	<h2>Contenus</h2>
	<ul>
		<li><?= CHtml::link("Sources de liens", ['/sourcelien']) ?></li>
		<li><?= CHtml::link("Thématique", ['/categorie/admin']) ?></li>
	</ul>
</div>

<div class="span5 well">
	<h2>Gestion des partenaires</h2>
	<ul>
		<li><?= CHtml::link("Partenaires", ['/partenaire/admin']) ?></li>
		<li><?= CHtml::link("Utilisateurs", ['/utilisateur/admin']) ?></li>
		<li><?= CHtml::link("Indicateurs de suivi", ['/stats/suivi']) ?></li>
	</ul>
</div>

<div class="span5 well">
	<h2>Maintenance</h2>
	<ul>
		<li><?= CHtml::link("Logs (erreurs et avertissements)", ['/logExt']) ?></li>
		<li>
			<?= CHtml::link(
				"Mode de maintenance",
				['/config', 'Config[category]' => 'maintenance']
			) ?>
		</li>
		<li>
			<?php
			if (\Config::read('maintenance.authentification.inactive')) {
				echo "<p>Les connexions sont actuellement restreintes aux administrateurs.</p>";
				echo HtmlHelper::postButton(
					"Autoriser les connexions",
					['admin/finMaintenance'],
					[],
					['class' => 'btn btn-success', 'confirm' => 'Voulez-vous ré-autoriser les connexions ?']
				);
			} else {
				echo "<p>Déconnecte les autres utilisateurs
					et bloque toute authentification non-admin jusqu'à nouvel ordre.</p>";
				echo HtmlHelper::postButton(
					"Déconnexion générale",
					['admin/deconnexion'],
					[],
					['class' => 'btn btn-danger', 'confirm' => 'Voulez-vous déconnecter les autres utilisateurs ?']
				);
			}
			?>
		</li>
	</ul>
</div>
