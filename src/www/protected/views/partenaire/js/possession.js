$('#Possession_titreIdComplete').on('autocompleteselect', function(event, ui) {
	var row = $('#titre' + ui.item.id).first();
	if (row.length) {
		var idvalue = $("td.id", row).first().text();
		$("#Possession_identifiantLocal").val(idvalue);
		$("#Possession_bouquet").val( $("td.bouquet", row).first().text() );
		$("#overwrite").show();
	} else {
		$("#Possession_identifiantLocal").val('');
		$("#Possession_bouquet").val('');
		$("#overwrite").hide();
	}
	return true;
});

$('#possessions-checkall').on('click', function(){
	$('#possessions-table :checkbox').prop('checked', this.checked);
});

$('#possessions-table').on('submit', function(){
	var suppr = $('#possessions-table input:checkbox(:checked)').length;
	if ($('#possessions-checkall(:checked)').length > 0) {
		suppr--;
	}
	if (suppr === 0) {
		alert("Aucun titre n'a été sélectionné, donc aucune opération de suppression d'une possession n'est à appliquer.");
		return false;
	} else {
		return confirm("Vous allez supprimer de vos possessions " + suppr + " titres. Confirmez-vous cette opération non-réversible ?");
	}
});
