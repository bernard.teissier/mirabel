<?php

/** @var Controller $this */
/** @var Partenaire $model */
/** @var array $existing */
/** @var Possession $possession */
/** @var array $bouquets */
/** @var SearchNavigation $searchNavRevues */
/** @var SearchNavigation $searchNavTitres */
assert($this instanceof Controller);

$this->breadcrumbs = [
	'Partenaires' => ['index'],
	$model->nom => ['view', 'id' => $model->id],
	"Possessions",
];

if (!Yii::app()->user->isGuest) {
	$this->menu = [
		['label' => 'Retour à ce partenaire', 'url' => ['view', 'id' => $model->id]],
		['label' => ''],
		['label' => 'Liste des partenaires', 'url' => ['index']],
		['label' => 'Administration des partenaires', 'url' => ['admin']],
		['label' => ''],
		['label' => 'Possessions', 'itemOptions' => ['class' => 'nav-header']],
		['label' => 'Import CSV', 'url' => ['/possession/import', 'partenaireId' => $model->id]],
		['label' => 'Export CSV', 'url' => ['/possession/export', 'partenaireId' => $model->id]],
		['label' => "Export CSV détaillé", 'url' => ['/possession/exportMore', 'partenaireId' => $model->id]],
	];
}
?>

<h1>Possessions du partenaire <em><?= CHtml::encode($model->nom) ?></em></h1>


<h2>Nouvelle possession de titre</h2>
<?php
if (empty($_POST['noAct'])) {
	?>
	<p>
		Utilisez la complétion pour ajouter de nouveaux éléments.
	</p>
	<?php
} else {
		?>
	<p class="alert alert-info">
		<strong>Attention !</strong>
		Pour enregistrer la possession, il faut valider ce formulaire,
		après avoir complété ses deux champs optionnels.
	</p>
	<?php
	}
/** @var BootActiveForm $form */
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'possession-form',
		'action' => ['partenaire/possession', 'id' => $model->id],
		'enableAjaxValidation' => false,
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'hints' => Hint::model()->getMessages('Possession'),
		'htmlOptions' => ['class' => 'well'],
	]
);
$this->renderPartial(
	'/global/_autocompleteField',
	[
		'model' => $possession, 'attribute' => 'titreId',
		'url' => '/titre/ajaxComplete', 'form' => $form,
		'value' => (empty($possession->titreId) ? '' : $possession->titre->titre),
	]
);
echo $form->textFieldRow($possession, 'identifiantLocal');
?>
<div class="control-group">
	<label class="control-label required">
		<?php echo $possession->getAttributeLabel('bouquet'); ?>
	</label>
	<div class="controls">
		<?php
		$params = [
			'name' => "Possession[bouquet]",
			'source' => $bouquets,
			'options' => [
				// min letters typed before we try to complete
				'minLength' => '2',
			],
		];
		$this->widget('zii.widgets.jui.CJuiAutoComplete', $params);
		echo $form->error($possession, 'bouquet');
		if (!empty($form->hints['bouquet'])) {
			echo CHtml::tag(
				'a',
				[
					'href' => '#',
					'data-title' => $model->getAttributeLabel('bouquet'),
					'data-content' => $form->hints['bouquet'],
					'data-html' => true,
					'data-trigger' => 'hover',
					'rel' => 'popover',
				],
				'?'
			);
		}
		?>
	</div>
</div>
<?php
echo '<div id="overwrite" class="hide">' . $form->checkBoxRow($possession, 'overwrite') . '</div>';
echo CHtml::submitButton("Ajouter ce titre", ['class' => 'btn']);
$this->endWidget();
?>

<h2>Possessions déjà déclarées <?= empty($existing) ? "" : " (" . count($existing) . ")" ?></h2>
<p>
	En cochant des cases, puis en enregistrant les modifications en bas de page,
    les titres sélectionnés seront supprimés de vos possessions.
</p>
<?php
if (empty($existing)) {
	echo "<p>Aucun titre.</p>";
} else {
	echo CHtml::form('', 'post', ['class' => 'form-horizontal', 'id' => 'possessions-table'])
		. CHtml::hiddenField('id', $model->id); ?>
	<table class="table table-striped table-bordered table-condensed">
		<thead>
			<th>
				<input type="checkbox" name="checkall" id="possessions-checkall" />
			</th>
			<th>
				<?php
				echo CHtml::link("Bouquet", ['possession', 'id' => $model->id, 'tri' => 'bouquet']); ?>
			</th>
			<th>
				<?php
				echo CHtml::link("Identifiant", ['possession', 'id' => $model->id, 'tri' => 'identifiant'])
					. " " . CHtml::link("(num)", ['possession', 'id' => $model->id, 'tri' => 'idnum']); ?>
			</th>
			<th>
				<?php
				echo CHtml::link("Titre", ['possession', 'id' => $model->id, 'tri' => '']); ?>
			</th>
		</thead>
	<tbody>
	<?php
	$pos = 0;
	foreach ($existing as $p) {
		/** @var Possession $p */
		echo "<tr id=\"titre{$p->titreId}\">"
			. "<td>" . CHtml::checkBox("existing[t{$p->titreId}]", false, ['uncheckValue' => null]) . "</td>"
			. '<td class="bouquet">' . CHtml::encode($p->bouquet) . "</td>"
			. '<td class="id">'
			. CHtml::link(CHtml::encode($p->identifiantLocal), $model->buildPossessionUrl($p->titre, $p->identifiantLocal))
			. "</td>";
		echo '<td>'
			. CHtml::link("revue", ['revue/view', 'id' => $p->titre->revueId, 's' => $searchNavRevues->getHash($pos)], ['class' => 'label pull-right'])
			. CHtml::link("titre", ['titre/view', 'id' => $p->titreId, 's' => $searchNavTitres->getHash()], ['class' => 'label pull-right', 'style' => 'margin-right: 1ex;'])
			. '<label for="existing_' . $p->titreId . '">'
			. CHtml::encode($p->titre->getFullTitle())
			. "</label>"
			. "</td>";
		echo "</tr>";
		$pos++;
	} ?>
	</tbody>
	</table>
	<button type="submit" class="btn">Enlever les titres sélectionnés de mes possessions</button>
	<?php
	echo CHtml::endForm();
}

$this->loadViewJs(__FILE__);
?>
