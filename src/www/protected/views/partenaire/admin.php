<?php

/** @var Controller $this */
/** @var Partenaire $model */
assert($this instanceof Controller);

$this->breadcrumbs = [
	'Partenaires' => ['index'],
	'Administration',
];

$this->menu = [
	['label' => 'Liste', 'url' => ['index']],
	['label' => 'Créer', 'url' => ['create']],
];
?>

<h1>Administration des partenaires</h1>

<p>
	Vous pouvez saisir un opérateur de comparaison
	(<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
	ou <b>=</b>) au début de chaque zone de recherche afin de désigner le mode de comparaison.
</p>

<form action="">
	<button type="submit">Filtrer</button>
<?php
$columns = [
	'nom',
	[
		'name' => 'type',
		'filter' => Partenaire::$enumType,
	],
	'description',
	[
		'name' => 'statut',
		'filter' => Partenaire::$enumStatut,
	],
	/*
		'id',
		'url:url',
		'email:email',
		'logoUrl:image',
		'hash',
		'ipAutorisees',
		'hdateCreation:datetime',
		*/
	'hdateModif:datetime',
	[
		'class' => 'BootButtonColumn', // CButtonColumn
	],
];
$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	//'zii.widgets.grid.CGridView',
	[
		'id' => 'partenaire-grid',
		'dataProvider' => $model->search(),
		'filter' => $model, // null to disable
		'columns' => $columns,
		'ajaxUpdate' => false,
	]
);
?>
</form>
