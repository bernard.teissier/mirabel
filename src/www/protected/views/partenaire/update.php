<?php

/** @var Controller $this */
/** @var Partenaire $model */
assert($this instanceof Controller);

$this->breadcrumbs = [
	'Partenaires' => ['index'],
	$model->getFullName() => ['view', 'id' => $model->id],
	'Modifier',
];

$this->menu = [
	['label' => 'Liste', 'url' => ['index']],
	['label' => 'Administration', 'url' => ['admin']],
];
?>

<h1>Modifier le partenaire <em><?php echo CHtml::encode($model->getFullName()); ?></em></h1>

<?php
echo $this->renderPartial('_form', ['model' => $model]);
?>
