<?php

/** @var Controller $this */
/** @var Partenaire $model */
assert($this instanceof Controller);

$this->breadcrumbs = [
	'Partenaires' => ['index'],
	'Créer',
];

$this->menu = [
	['label' => 'Liste', 'url' => ['index']],
	['label' => 'Administration', 'url' => ['admin']],
];
?>

<h1>Nouveau partenaire</h1>

<?php
echo $this->renderPartial('_form', ['model' => $model]);
?>
