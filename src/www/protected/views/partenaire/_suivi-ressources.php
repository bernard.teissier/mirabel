<?php

/** @var Controller $this */
/** @var Partenaire $partenaire */
/** @var RessourceSearch $suiviRessources */
/** @var array $shares */
assert($this instanceof Controller);

$dataProvider = $suiviRessources->search();
if ($dataProvider->itemCount === 0) {
	return;
}

$alertBefore = (int) strtotime("-6 month");
?>

<h3>Ressources suivies</h3>
<?php
$ressColumns = [
	[
		'name' => 'nom',
		'header' => 'Ressource',
		'type' => 'html',
		'value' => function (Ressource $r) {
			return CHtml::link(CHtml::encode($r->nom), ['/ressource/view', 'id' => $r->id]);
		},
	],
	[
		'visible' => in_array('Ressource', $shares),
		'header' => '<abbr title="Lorsque le suivi est partagé par plusieurs partenaires">Autre suivi</abbr>',
		'type' => 'html',
		'value' => function (Ressource $r) use ($partenaire) {
			$suivi = explode(",", $r->confirm);
			if (count($suivi) === 1) {
				return "";
			}
			$names = [];
			foreach ($suivi as $pid) {
				if ($pid != $partenaire->id) {
					$names[] = CHtml::link(
						CHtml::encode(Partenaire::model()->findByPk($pid)->nom),
						['/partenaire/view', 'id' => $pid]
					);
				}
			}
			return join(" ; ", $names);
		},
	],
	[
		'name' => 'hdateModif',
		'type' => 'date',
		'header' => 'Dernière modif.',
		'filter' => false,
	],
	[
		'name' => 'hdateVerif',
		'type' => 'html',
		'header' => 'Dernière vérif.',
		'value' => function (Ressource $r) use ($alertBefore) {
			$text = $r->hdateVerif ? Yii::app()->format->formatDate($r->hdateVerif) : "";
			if (!$r->hdateVerif || $r->hdateVerif < $alertBefore) {
				$text .= ' <i class="icon icon-warning-sign" title="Attention, votre dernière vérification remonte à plus de 6 mois"></i>';
			}
			return $text;
		},
		'filter' => false,
	],
];
$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'partenaire-suivi-ressources-grid',
		'dataProvider' => $dataProvider,
		'filter' => $suiviRessources,
		'columns' => $ressColumns,
		'ajaxUpdate' => false,
		'summaryText' => CHtml::link('{count} ressources suivies', ['/ressource/index', 'suivi' => $partenaire->id]),
	]
);
?>
