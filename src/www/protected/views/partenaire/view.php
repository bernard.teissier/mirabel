<?php

/** @var Controller $this */
/** @var Partenaire $model */
/** @var string $logoImg */
/** @var AbonnementInfo $abonnementInfo */
/** @var array uploadedFiles */

assert($this instanceof Controller);

$this->pageTitle = 'Partenaire ' . $model->nom;

$this->breadcrumbs = [
	'Partenaires' => ['index'],
	$model->nom,
];

$encodedCanonicalUrl = CHtml::encode(
	$this->createAbsoluteUrl('/partenaire/view', ['id' => $model->id])
);
$encodedName = CHtml::encode($model->nom);
$encodedTitle = CHtml::encode($this->pageTitle);
$encodedLogoUrl = CHtml::encode(
	$model->getLogoUrl(false)
	?: Yii::app()->getBaseUrl(true) . '/images/logo-mirabel-carre.png'
);
$this->appendToHtmlHead(
	<<<EOL
		<link rel="canonical" href="$encodedCanonicalUrl" />

		<meta name="description" content="accès en ligne aux contenus des revues de Mirabel pour le partenaire $encodedName" lang="fr" />
		<meta name="keywords" content="revue, accès en ligne, texte intégral, sommaire, périodique, $encodedName" lang="fr" />
		<link rel="schema.dcterms" href="http://purl.org/dc/terms/" />
		<meta name="dcterms.title" content="Mirabel : partenaire $encodedName" />
		<meta name="dcterms.subject" content="revue, accès en ligne, texte intégral, sommaire, périodique, $encodedName" />
		<meta name="dcterms.language" content="fr" />
		<meta name="dcterms.creator" content="Mirabel" />
		<meta name="dcterms.publisher" content="Sciences Po Lyon" />

		<meta name="twitter:card" content="summary" />
		<meta name="twitter:site" content="@mirabel_revues" />
		<meta name="twitter:title" content="Mir@bel - {$encodedTitle}" />
		<meta name="twitter:description" content="accès en ligne aux contenus des revues de Mirabel pour le partenaire $encodedName" />
		<meta name="twitter:image" content="$encodedLogoUrl" />
		<meta property="og:image" content="$encodedLogoUrl" />
		EOL
);

if (Yii::app()->user->isGuest) {
	$permModif = false;
	$permAdmin = false;
	$permView = false;
} else {
	$permAdmin = Yii::app()->user->checkAccess('partenaire/admin');
	$permModif = Yii::app()->user->checkAccess('partenaire/update', ['partenaireId' => $model->id]);
	$permView = Yii::app()->user->checkAccess('partenaire/view', ['partenaireId' => $model->id]);
	$this->menu = [
		['label' => 'Liste des partenaires', 'url' => ['index']],
		['label' => 'Administration des partenaires', 'url' => ['admin'], 'visible' => $permAdmin],
		['label' => 'Créer un partenaire', 'url' => ['create'], 'visible' => $permAdmin],
		['label' => ''],
		['label' => 'Ce partenaire', 'itemOptions' => ['class' => 'nav-header']],
		['label' => "Stats d'activité", 'url' => ['/stats/activite', 'partenaireId' => $model->id], 'visible' => $permView],
		['label' => 'Modifier', 'url' => ['update', 'id' => $model->id], 'visible' => $permModif],
		[
			'label' => 'Déposer un logo',
			'url' => ($model->editeurId ? ['/editeur/logo', 'id' => $model->editeurId] : ['/upload', 'dest' => 'logos-p', 'partenaireId' => $model->id]),
			'visible' => $permModif,
		],
		[
			'label' => 'Ajouter/modifier le bloc éditorial',
			'url' => ['cms/create', 'partenaire' => $model->id],
			'visible' => $permModif && $model->type === 'normal',
		],
		[
			'label' => 'Supprimer', 'url' => '#',
			'linkOptions' => [
				'submit' => ['delete', 'id' => $model->id],
				'confirm' => "êtes-vous sûr de vouloir supprimer ce partenaire ?"
					. "\nToutes ses relations de possession et de suivi seront supprimées définitivement.",
			],
			'visible' => $permAdmin,
		],
		[
			'label' => 'Créer un utilisateur',
			'url' => ['/utilisateur/create', 'partenaireId' => $model->id],
			'visible' => $permAdmin,
		],
	];
	if ($model->type !== 'import') {
		array_push(
			$this->menu,
			['label' => ''],
			['label' => 'Suivi', 'itemOptions' => ['class' => 'nav-header']],
			['label' => "Tableau de bord", 'url' => ['tableauDeBord', 'id' => $model->id]],
			['label' => 'Revues', 'url' => ['suivi', 'id' => $model->id, 'target' => 'Revue']],
			['label' => 'Ressources', 'url' => ['suivi', 'id' => $model->id, 'target' => 'Ressource']],
			[
				'label' => 'Éditeurs',
				'url' => ['suivi', 'id' => $model->id, 'target' => 'Editeur'],
				'visible' => ($model->type === 'editeur'),
			]
		);
	}
	if ($model->canOwnTitles() && Yii::app()->user->checkAccess('possession/update', ['Partenaire' => $model])) {
		array_push(
			$this->menu,
			['label' => ''],
			['label' => 'Possessions', 'itemOptions' => ['class' => 'nav-header']],
			['label' => 'Gestion des titres', 'url' => ['possession', 'id' => $model->id]],
			['label' => 'Import CSV', 'url' => ['/possession/import', 'partenaireId' => $model->id]],
			['label' => 'Export CSV', 'url' => ['/possession/export', 'partenaireId' => $model->id]],
			['label' => "Export CSV détaillé", 'url' => ['/possession/exportMore', 'partenaireId' => $model->id]]
		);
	}
	if (Yii::app()->user->checkAccess('abonnement/update', $model)) {
		array_push(
			$this->menu,
			['label' => 'Abonnements', 'itemOptions' => ['class' => 'nav-header']],
			['label' => 'Mes collections', 'url' => ['collection/index', 'partenaireId' => $model->id], 'visible' => false], // "Mes collections" est désactivé
			['label' => 'Gestion des abonnements', 'url' =>  ['abonnements', 'id' => $model->id]],
			['label' => 'Nouvelle collection', 'url' => ['collection/create', 'partenaireId' => $model->id],
				'itemOptions' => ['title' => "Créer une collection-bibliothèque"],
				'visible' => false, // fonctionnalité temporairement désactivée
			]
		);
	}
}

if ($permModif && !$model->email) {
	?>
	<div class="alert alert-error">
		<strong>Attention : adresse de contact manquante. </strong>
		L'envoi des alertes de propositions de modifications ne pourra
		pas se faire pour ce partenaire tant que son adresse de contact ne sera pas remplie.
	</div>
	<?php
}
if ($permModif && !$logoImg) {
	?>
	<p class="alert alert-warning">
		Ce partenaire n'a pas de logo. Vous pouvez déposer une image en passant par le menu latéral.
	</p>
	<?php
}
?>

<h1>
	<?= $model->getLogoImg() ?>
	<em><?= CHtml::encode($model->nom) ?></em>
</h1>

<?php
if ($model->editeurId) {
	echo "<p>Ce compte de partenaire est associé à l'éditeur " . $model->editeur->getSelfLink() . ".</p>";
}

Cms::printBlock('partenaire', $model->id);

$attributes = [
	'nom',
	[
		'name' => 'sigle',
		'visible' => !Yii::app()->user->isGuest,
	],
	'description',
	'url:url',
];
if (!Yii::app()->user->isGuest) {
	$attributes = array_merge(
		$attributes,
		[
			'logoUrl:image',
			'phrasePerso',
			'email:emails',
			'importEmail:emails',
			'ipAutorisees',
			'possessionUrl',
			'proxyUrl',
			'hdateCreation:datetime',
			'hdateModif:datetime',
			[
				'name' => 'type',
				'type' => 'raw',
				'value' => Partenaire::$enumType[$model->type] . ($model->editeurId ? " — " . $model->editeur->getSelfLink() : ""),
			],
			['name' => 'statut', 'value' => Partenaire::$enumStatut[$model->statut]],
			[
				'label' => "Géolocalisation",
				'value' => trim(
					$model->geo
						. ($model->latitude && $model->longitude ? " ({$model->latitude}, {$model->longitude})" : "")
						. ($model->paysId ? ", " . $model->pays->nom : ""),
					", "
				),
			],
			[
				'label' => "Fusion des accès",
				'value' => ($model->fusionAcces ? "Activée" : "Désactivée"),
			],
			[
				'name' => 'hash',
				'label' => 'Clé <a href="/api">API</a>',
				'type' => 'raw',
				'value' => CHtml::tag(
					'span',
					['id' => 'partenaire-hash', 'title' => "Clé d'accès à l'API de Mir@bel"],
					$model->hash ? CHtml::encode($model->hash) : ""
				) . " "
					. ($permModif ?
						CHtml::tag('button', ['id' => "partenaire-hash-refresh", 'title' => "Générer une nouvelle clé d'accès à l'API"], '<i class="icon-refresh"></i>')
						: ""),
				'visible' => $permModif,
			],
		]
	);
	Yii::app()->clientScript->registerScript(
		'partenaire-hash-refresh',
		<<<EOJS
			$("#partenaire-hash-refresh").on('click', function() {
				var hash = document.getElementById('partenaire-hash').innerText;
				if (hash === '' || confirm("Attention, l'ancienne clé ne sera plus valable. Tous les accès à l'API devront utiliser la nouvelle clé. Confirmez-vous ?")) {
					$('#partenaire-hash').load('/partenaire/ajaxRefreshHash?id={$model->id}');
				}
			});
			EOJS
	);
} else {
	$attributes[] = [
		'label' => "Localisation",
		'value' => trim(
			$model->geo . ($model->paysId ? ", " . $model->pays->nom : ""),
			", "
		),
	];
}
$this->widget(
	'bootstrap.widgets.BootDetailView',
	[
		'data' => $model,
		'attributes' => $attributes,
		'hideEmptyLines' => true,
	]
);

if (!Yii::app()->user->isGuest) {
	echo "<h2>Utilisateurs associés</h2>\n";
	$users = $model->utilisateurs;
	if ($users) {
		$output = [];
		foreach ($users as $user) {
			if ($user->actif) {
				$name = $user->nomComplet ?: $user->login;
				$output[] = "<span>"
					. CHtml::link(
						CHtml::encode($name),
						['utilisateur/view', 'id' => $user->id],
						['title' => $user->nomComplet]
					) . "</span>";
			}
		}
		echo "<p>" . join(" / ", $output) . "</p>";
	} else {
		echo '<p>Aucun</p>'
			. ($permAdmin ? '<p class="alert alert-warning">Utilisez le menu latéral pour créer un compte utilisateur.</p>' : '');
	}
}
?>

<?= $this->renderPartial('_suivi', ['partenaire' => $model]) ?>

<?php
if (!Yii::app()->user->isGuest && $model->canOwnTitles()) {
	$numTitres = Yii::app()->db
		->createCommand("SELECT count(*) FROM Partenaire_Titre WHERE partenaireId = " . $model->id)
		->queryScalar(); ?>
	<h2>Possessions de titres de revues déclarées par ce partenaire dans Mir@bel</h2>
	<p>
		<?php
		if ($numTitres == 0) {
			echo "Aucun titre.";
		} else {
			echo '<ul><li>'
				. CHtml::link("$numTitres titres de revues.", ['/revue/search', 'q' => ['detenu' => $model->id]])
				. '</li></ul>';
		}
		?>
	</p>
	<?php if (Yii::app()->user->checkAccess('possession/update', ['Partenaire' => $model])) { ?>
	<p>
		<?= CHtml::link("Modifier manuellement les possessions", ['possession', 'id' => $model->id]) ?>
		 ou les 
		<?= CHtml::link("modifier via un import CSV", ['/possession/import', 'partenaireId' => $model->id]) ?>
	</p>
	<?php } ?>
<?php
}

if (!Yii::app()->user->isGuest) {
	?>
	<h2>Abonnements électroniques déclarés par ce partenaire dans Mir@bel</h2>
	<?php if ($abonnementInfo->count > 0) { ?>
	<ul>
		<li>
			<?= CHtml::link("{$abonnementInfo->count} abonnements électroniques", ['/partenaire/abonnements', 'id' => $model->id]) ?>
			(<?= $abonnementInfo->countRessources ?> ressources et <?= $abonnementInfo->countCollections ?> collections)
		</li>
		<li>
			donnant accès à <?= CHtml::link("{$abonnementInfo->countRevues} revues", ['/revue/search', 'q' => ['abonnement' => 1, 'pid' => $model->id]]) ?>.
		</li>
	</ul>
	<?php } else { ?>
	<p>
		Aucun abonnement déclaré pour l'instant.
	</p>
	<?php } ?>
	<?php if (Yii::app()->user->checkAccess('abonnement/update', $model)) { ?>
	<p>
		<?= CHtml::link("Modifier les abonnements", ['abonnements', 'id' => $model->id]) ?>
	</p>
	<?php } ?>
<?php
}

if (empty($model->editeurId) && Yii::app()->user->checkAccess('partenaire/update', ['Partenaire' => $model])) {
	?>
	<h2>Convention</h2>
	<?php
	if (!$uploadedFiles) {
		echo "<p class=\"alert alert-warning\">Aucune convention n'a encore été déposée sur le site.</p>";
	} else {
		echo "<ul>";
		foreach($uploadedFiles as $uploaded) {
			echo CHtml::tag(
				"li",
				[],
				CHtml::link($uploaded, $this->createUrl('/upload/view') . "/$uploaded")
			);
		}
		echo "</ul>";
	}
}

if (Yii::app()->user->checkAccess('partenaire/customize', ['partenaireId' => $model->id])) {
	$urlPerso1 = Yii::app()->createAbsoluteUrl('/', ['ppp' => $model->id]);
	$urlPerso2 = Yii::app()->createAbsoluteUrl('/revue/view', ['id' => 2513, 'nom' => 'Mondes_emergents', 'ppp' => $model->id]);
	$urlPerso3 = Yii::app()->createAbsoluteUrl('/site/search', ['global' => '"droits de l\'homme"', 'ppp' => $model->id]); ?>
	<h2>Accès personnalisé</h2>
	<p>
		Pour un accès personnalisé, avec votre logo et vos abonnements,
		vous pouvez utiliser le sélecteur en pied de page,
		ou le paramètre <code>?ppp=<?= $model->id ?></code> dans une URL.
		Le choix est conservé pour toute la session.
		Par exemple, un lien vers la page d'accueil <a href="<?= CHtml::encode($urlPerso1) ?>"><code><?= CHtml::encode($urlPerso1) ?></code></a>,
		<a href="<?= CHtml::encode($urlPerso2) ?>">une revue</a>
		ou <a href="<?= CHtml::encode($urlPerso3) ?>">une recherche</a>.
	</p>
	<?php
}
