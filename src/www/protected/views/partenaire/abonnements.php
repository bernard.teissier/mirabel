<?php

/** @var Controller $this */
/** @var Partenaire $partenaire */
/** @var AbonnementSearch $abonnement */
assert($this instanceof Controller);

$this->breadcrumbs = [
	'Partenaires' => ['index'],
	$partenaire->nom => ['view', 'id' => $partenaire->id],
	"Abonnements",
];

$this->menu = [
	['label' => 'Retour à ce partenaire', 'url' => ['view', 'id' => $partenaire->id]],
	['label' => ''],
	['label' => 'Liste des partenaires', 'url' => ['index']],
	['label' => 'Administration des partenaires', 'url' => ['admin'], 'visible' => Yii::app()->user->checkAccess('partenaire/admin')],
	['label' => ''],
	['label' => 'Possessions', 'itemOptions' => ['class' => 'nav-header']],
	['label' => 'Gestion des titres', 'url' => ['possession', 'id' => $partenaire->id]],
	['label' => 'Import CSV', 'url' => ['/possession/import', 'partenaireId' => $partenaire->id]],
	['label' => 'Export CSV', 'url' => ['/possession/export', 'partenaireId' => $partenaire->id]],
	['label' => 'Abonnements', 'itemOptions' => ['class' => 'nav-header']],
	['label' => 'Gestion des abonnements', 'url' =>  ['abonnements', 'id' => $partenaire->id]],
];
?>

<h1>Abonnements du partenaire <em><?= CHtml::encode($partenaire->nom) ?></em></h1>

<?php
if ($abonnement->countAll()) {
	$numResAb = $abonnement->countAbonnements();
	$numResM = $abonnement->countMasques();
	printf(
		"<p>Vous avez %d abonnement%s actif%s sur des ressources%s.</p>",
		$numResAb,
		($numResAb > 1 ? 's' : ''),
		($numResAb > 1 ? 's' : ''),
		($numResM > 0 ? sprintf(" et %d ressources masquées", $numResM) : '')
	);
} else {
	echo "<p>Vous n'avez aucun abonnement actif.</p>";
}

$columns = [
	[
		'name' => 'ressourceNom',
		'header' => 'Ressource',
		'type' => 'raw',
		'value' => function ($data) {
			return CHtml::link($data['ressourceNom'], ['/ressource/view', 'id' => $data['ressourceId']]);
		},
	],
	[
		'name' => 'collectionNom',
		'type' => 'raw',
		'header' => 'Collection',
		'value' => function ($data) {
			if ($data['collectionNom']) {
				return CHtml::encode($data['collectionNom'])
					. ($data['collectionType'] === Collection::TYPE_TEMPORAIRE ? ' <b>(temporaire)</b>' : '');
			}
			return '-';
		},
	],
	[
		'name' => 'mask',
		'type' => 'raw',
		'header' => "Type",
		'filter' => ['ni abonné, ni masqué', 'abonné', 'masqué'], // subset of Abonnement::$enumMask,
		'value' => function ($data) {
			$status = isset($data['mask']) ? Abonnement::$enumMask[$data['mask']] : '-';
			if ($data['mask'] == Abonnement::ABONNE && !empty($data['proxy'])) {
				if (!empty($data['proxyUrl'])) {
					$status .= ' <small title="utlise le proxy spécifique à cet abonnement (cf page de la ressource)">[P+]</small>';
				} else {
					$status .= ' <small title="utilise le proxy global (au niveau partenaire)">[P]</small>';
				}
			}
			return $status;
		},
	],
	[
		'type' => 'raw',
		'value' => function ($data) use ($partenaire, $abonnement) {
			$html = '';
			$params = [
				'partenaireId' => (int) $partenaire->id,
				'ressourceId' => (int) $data['ressourceId'],
				'return' => $this->createUrl(
					'/partenaire/abonnements',
					['id' => $partenaire->id, 'AbonnementSearch' => $abonnement->getFilters()]
				),
			];
			if ($data['collectionId']) {
				$params['collectionId'] = (int) $data['collectionId'];
			}
			if ($data['mask'] == Abonnement::ABONNE) {
				$html .= " " . CHtml::form(array_merge(['/abonnement/delete'], $params))
					. CHtml::submitButton("Se désabonner") . CHtml::endForm();
			} elseif ($data['mask'] == Abonnement::MASQUE) {
				$html .= " " . CHtml::form(array_merge(['/abonnement/delete'], $params))
					. CHtml::submitButton("Afficher") . CHtml::endForm();
			} else {
				if ($data['collectionType'] !== Collection::TYPE_TEMPORAIRE) {
					$html .= CHtml::form(array_merge(['/abonnement/subscribe'], $params))
						. CHtml::submitButton("S'abonner") . CHtml::endForm();
					$html .= " " . CHtml::form(array_merge(['/abonnement/hide'], $params))
						. CHtml::submitButton("Masquer") . CHtml::endForm();
				}
			}
			return trim($html);
		},
	],
];
$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'abonnements-grid',
		'dataProvider' => $abonnement->search(25),
		'filter' => $abonnement, // null to disable
		'columns' => $columns,
		'ajaxUpdate' => false,
		'summaryText' => '',
		'rowCssClassExpression' => function ($row, $data) {
			if ($data['collectionType'] === Collection::TYPE_TEMPORAIRE) {
				return 'alert alert-danger';
			}
		},
	]
);
?>
