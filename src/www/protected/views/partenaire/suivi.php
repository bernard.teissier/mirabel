<?php

/** @var Controller $this */
/** @var Partenaire $model */
/** @var string $target */
/** @var array $existing */
/** @var Suivi $suivi */
/** @var array $duplicates */
assert($this instanceof Controller);

$this->breadcrumbs = [
	'Partenaires' => ['index'],
	$model->nom => ['view', 'id' => $model->id],
	"Suivi ($target)",
];

if (!Yii::app()->user->isGuest) {
	$this->menu = [
		['label' => 'Liste des partenaires', 'url' => ['index']],
		['label' => 'Administration des partenaires', 'url' => ['admin']],
		['label' => ''],
		['label' => 'Créer', 'url' => ['create']],
		['label' => 'Modifier', 'url' => ['update', 'id' => $model->id]],
		['label' => 'Supprimer', 'url' => '#',
			'linkOptions' => [
				'submit' => ['delete', 'id' => $model->id],
				'confirm' => 'êtes-vous sûr de vouloir supprimer ce partenaire ?',
			],
		],
		['label' => ''],
		['label' => 'Suivi', 'itemOptions' => ['class' => 'nav-header']],
		['label' => "Tableau de bord", 'url' => ['tableauDeBord', 'id' => $model->id]],
		['label' => 'Revues', 'url' => ['suivi', 'id' => $model->id, 'target' => 'Revue'], 'visible' => ($target !== 'Revue')],
		['label' => 'Ressources', 'url' => ['suivi', 'id' => $model->id, 'target' => 'Ressource'], 'visible' => ($target !== 'Ressource')],
		[
			'label' => 'Éditeurs',
			'url' => ['suivi', 'id' => $model->id, 'target' => 'Editeur'],
			'visible' => ($model->type === 'editeur' && $target !== 'Editeur'),
		],
	];
}
?>

<h1>Suivi pour le partenaire <em><?= CHtml::encode($model->nom) ?></em></h1>

<h2>Nouveau suivi</h2>
<p>
	Utilisez la complétion pour ajouter de nouveaux éléments.
</p>
<?php
/** @var BootActiveForm $form */
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'partenaire-form',
		'enableAjaxValidation' => false,
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'hints' => Hint::model()->getMessages('Suivi'),
		'htmlOptions' => ['class' => 'well'],
	]
);
echo $form->hiddenField($suivi, 'partenaireId');
echo $form->hiddenField($suivi, 'cible');
$this->widget(
	'ext.MultiAutoComplete.MultiAutoComplete',
	[
		'model' => $suivi,
		'attribute' => 'cibleId',
		'foreignModelName' => $target,
		'foreignAttribute' => 'fullName',
		'ajaxUrl' => $this->createUrl('/' . strtolower($target) . '/ajaxComplete'),
		'options' => [
			// min letters typed before we try to complete
			'minLength' => '3',
		],
		'htmlOptions' => [
			'hint' => empty($form->hints['cible']) ? null : $form->hints['cible'],
		],
	]
);
echo CHtml::submitButton("Ajouter ces suivis", ['class' => 'btn']);
$this->endWidget();
?>

<h2><?= count($existing) ?> suivis déjà déclarés</h2>
<?php
if (empty($existing)) {
	echo "<p>Aucun objet de type '$target' n'est actuellement suivi.";
} else {
	echo CHtml::form('', 'post', ['class' => 'form-horizontal'])
		. CHtml::hiddenField('id', $model->id)
		. CHtml::hiddenField('target', $target);
	echo CHtml::submitButton("Enregistrer les modifications", ['class' => 'btn']);

	$columns = [
		[
			'header' => '',
			'value' => function ($object) {
				return CHtml::checkBox("existing[{$object->id}]", true, ['uncheckValue' => 0]);
			},
			'type' => 'raw',
		],
		[
			'header' => $target,
			'value' => function ($object) {
				return CHtml::tag("label", ['for' => "existing[{$object->id}]"], $object->getSelfLink());
			},
			'type' => 'raw',
		],
		[
			'header' => "Suivis en doublon",
			'value' => function ($object) use ($duplicates) {
				if (!isset($duplicates[$object->id])) {
					return '';
				}
				$html = [];
				foreach ($duplicates[$object->id] as $pData) {
					$p = new Partenaire();
					$p->setAttributes($pData, false);
					$html[] = $p->getSelfLink(true);
				}
				return join(", ", $html);
			},
			'type' => 'raw',
		],
	];
	$this->widget(
		'ext.bootstrap.widgets.BootGridView',
		[
			'id' => 'partenaire-suivi-grid',
			'htmlOptions' => ['class' => 'table table-striped table-condensed'],
			'dataProvider' => new CArrayDataProvider($existing, ['pagination' => false]),
			'filter' => false,
			'enablePagination' => false,
			'columns' => $columns,
			'ajaxUpdate' => false,
			'summaryText' => false,
		]
	);

	echo CHtml::submitButton("Enregistrer les modifications", ['class' => 'btn']);
	echo CHtml::endForm();
}
?>
