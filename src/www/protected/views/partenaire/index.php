<?php

/** @var Controller $this */
/** @var array $partenaires */
/** @var array $ressources */
assert($this instanceof Controller);

$this->pageTitle = "Partenaires";
$this->breadcrumbs = [
	'Partenaires',
];

if (!Yii::app()->user->isGuest) {
	$this->menu=[
		['label' => 'Créer', 'url' => ['create']],
		['label' => 'Administration', 'url' => ['admin']],
	];
	$partenaire = Partenaire::model()->findByPk(Yii::app()->user->partenaireId);
	if ($partenaire->canOwnTitles()) {
		$this->menu = array_merge(
			$this->menu,
			[
				['label' => ''],
				['label' => 'Possessions', 'itemOptions' => ['class' => 'nav-header']],
				['label' => 'Gestion des titres', 'url' => ['possession', 'id' => $partenaire->id]],
				['label' => 'Import CSV', 'url' => ['/possession/import', 'partenaireId' => $partenaire->id]],
				['label' => 'Export CSV', 'url' => ['/possession/export', 'partenaireId' => $partenaire->id]],
			]
		);
	}
}

$cs = Yii::app()->clientScript;
$cs->registerCoreScript('jquery.ui');
$cs->registerCssFile($cs->getCoreScriptUrl() . '/jui/css/base/jquery-ui.css');
$cs->registerScript('tabs', '
var activeIndex = 0;
var matches = window.location.hash.match(/^#tab-(\d)$/);
if (matches) {
	activeIndex = matches[1];
}
$(".tabs").tabs({
	active: activeIndex,
	activate: function(event, ui) {
		window.location.hash = "#tab-" + ui.newPanel.attr("data-index");
	}
});
');
?>

<h1>Les partenaires de Mir@bel</h1>

<div class="tabs">
	<ul>
		<li><a href="#partenaires-institutions">Membres</a></li>
		<?php if (!empty($partenaires['editeur'])) { ?>
		<li><a href="#partenaires-editeurs">Éditeurs</a></li>
		<?php } ?>
		<?php if (!empty($ressources)) { ?>
		<li><a href="#partenaires-ressources">Ressources</a></li>
		<?php } ?>
	</ul>

	<section id="partenaires-institutions" data-index="0">
		<h2>Les <?= count($partenaires['normal']) ?> membres du réseau Mir@bel</h2>
		<p><?= CHtml::link("Carte des institutions partenaires", ['/partenaire/carte']); ?></p>
		<?php
		echo $this->renderPartial('_list', ['partenaires' => $partenaires['normal']]);
		?>
	</section>

	<?php
	if (!empty($partenaires['editeur'])) {
		?>
		<section id="partenaires-editeurs" data-index="1">
			<h2>Les <?= count($partenaires['editeur']) ?> éditeurs partenaires de Mir@bel</h2>
			<?= $this->renderPartial('_list', ['partenaires' => $partenaires['editeur']]); ?>
		</section>
	<?php
	}
	?>

	<?php
	if (!empty($ressources)) {
		?>
		<section id="partenaires-ressources" data-index="2">
			<h2>Les <?= count($ressources) ?> ressources partenaires qui sont suivies dans Mir@bel</h2>
			<?= $this->renderPartial('/ressource/_list', ['ressources' => $ressources, 'withDescription' => false]); ?>
		</section>
	<?php
	}
	?>
</div>
