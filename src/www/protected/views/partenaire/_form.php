<?php

/** @var Controller $this */
/** @var Partenaire $model */
assert($this instanceof Controller);

/** @var BootActiveForm $form */
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'partenaire-form',
		'enableAjaxValidation' => false,
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'hints' => Hint::model()->getMessages('Partenaire'),
		'htmlOptions' => ['class' => 'well'],
	]
);
?>

<?php
echo $form->errorSummary($model);

echo $form->textFieldRow($model, 'nom', ['class' => 'span5']);
echo $form->textFieldRow($model, 'prefixe', ['class' => 'span1']);
echo $form->textFieldRow($model, 'sigle', ['class' => 'span5']);
echo $form->textAreaRow($model, 'email', ['class' => 'span5', 'rows' => 3]);

$types = Partenaire::$enumType;
if ($model->editeurId) {
	$types = ['editeur' => 'Éditeur'];
} else {
	unset($types['editeur']);
}
echo $form->dropDownListRow($model, 'type', $types);

echo $form->textAreaRow($model, 'description', ['rows' => 6, 'cols' => 50, 'class' => 'span8']);
echo $form->textFieldRow($model, 'phrasePerso', ['class' => 'span7']);
if (empty($model->editeurId)) {
	echo $form->textFieldRow($model, 'url', ['class' => 'span7']);
	echo $form->textFieldRow($model, 'ipAutorisees', ['class' => 'span7']);
	echo $form->textFieldRow($model, 'possessionUrl', ['class' => 'span7']);
	echo $form->textFieldRow($model, 'proxyUrl', ['class' => 'span7']);
	echo $form->checkBoxRow($model, 'fusionAcces');
	echo $form->textAreaRow($model, 'importEmail', ['class' => 'span5', 'rows' => 3]);
}
if (Yii::app()->user->checkAccess('partenaire/admin')) {
	echo $form->dropDownListRow($model, 'statut', Partenaire::$enumStatut);
}
?>
<fieldset>
	<legend>Géo-localisation</legend>
	<?php
	if (empty($model->editeurId)) {
		echo $form->dropDownListRow($model, 'paysId', CHtml::listData(Pays::model()->sorted()->findAll(), 'id', 'nom'), ['empty' => '—']);
		echo $form->textFieldRow($model, 'geo');
	}
	?>
	<button type="button" id="geolocate" style="display: none; float: right">Utiliser ma position actuelle</button>
	<?php
	echo $form->textFieldRow($model, 'latitude');
	echo $form->textFieldRow($model, 'longitude');

	Yii::app()->clientScript->registerScript("geolocate", '
		if ("geolocation" in navigator && navigator.geolocation) {
			$("#geolocate").show()
				.on("click", function(){
					navigator.geolocation.getCurrentPosition(
						function(position) {
							$("#Partenaire_latitude").val(position.coords.latitude);
							$("#Partenaire_longitude").val(position.coords.longitude);
							$("#geolocate-warning").show();
						},
						function() { alert("Erreur en lisant la position."); },
						{ enableHighAccuracy: true }
					);
				});
		}
		');
	?>
	<div id="geolocate-warning" style="display: none">
		<strong>Les données de géolocalisation ne sont pas toujours fiables.</strong>
		Après validation, vous pourrez le vérifier sur la <?= CHtml::link("carte des partenaires", ['/partenaire/carte'], ['target' => '_blank']) ?>.
		Si cela ne fonctionne pas, indiquez manuellement votre latitude &amp; longitude.
	</div>
</fieldset>

<div class="form-actions">
	<?php
$this->widget(
		'bootstrap.widgets.BootButton',
		[
			'buttonType' => 'submit',
			'type' => 'primary',
			'label' => $model->isNewRecord ? 'Créer' : 'Enregistrer',
		]
	);
?>
</div>

<?php
$this->endWidget();
?>
