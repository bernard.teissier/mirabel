<?php

/** @var PartenaireController $this */
/** @var array $mapConfig */
assert($this instanceof Controller);

$this->pageTitle = "Carte des membres de Mir@bel";

$this->breadcrumbs = [
	'Partenaires' => ['index'],
	"Carte",
];

$this->appendToHtmlHead('<link rel="stylesheet" href="/js/leaflet/leaflet.css"');
Yii::app()->clientScript->registerScriptFile("/js/leaflet/leaflet.js");
if ($stamen) {
	Yii::app()->clientScript->registerScriptFile("//maps.stamen.com/js/tile.stamen.js?v1.3.0");
}
Yii::app()->clientScript->registerScriptFile("/js/countries.js");
Yii::app()->clientScript->registerScriptFile('/js/world-map.js');

Yii::app()->clientScript->registerScript(
	'mapinit',
	'drawWorldMap(' . CJavaScript::encode($mapConfig) . ', ' . CJavaScript::encode($stamen) . ');'
);
?>
<h1>Localisation des membres du réseau Mir@bel</h1>

<div id="world-map" style="height: 600px;"></div>
