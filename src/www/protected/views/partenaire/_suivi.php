<?php

/** @var Controller $this */
/** @var Partenaire $partenaire */
assert($this instanceof Controller);
?>

<h2>Les données suivies par ce partenaire dans Mir@bel</h2>
<?php if (!Yii::app()->user->isGuest) { ?>
	<p>
		Consulter le <?= CHtml::link("tableau de bord", ['/partenaire/tableauDeBord', 'id' => $partenaire->id]) ?> détaillé.
	</p>
<?php } ?>
<?php
$viewEditeurs = !Yii::app()->user->isGuest
	&& Yii::app()->user->checkAccess('suiviPartenairesEditeurs');
$tabSuivi = $partenaire->getRightsSuivi($viewEditeurs);
if (count($tabSuivi)) {
	echo '<ul class="objets-suivis">';
	if (!empty($tabSuivi['Revue'])) {
		$nbRevues = count($tabSuivi['Revue']);
		echo '<li>'
			. CHtml::link(
				$nbRevues . ' revue' . ($nbRevues > 1 ? 's' : ''),
				['/revue/search', 'SearchTitre[suivi][]' => $partenaire->id]
			) . '</li>';
	}
	if (!empty($tabSuivi['Ressource'])) {
		$nbRessources = count($tabSuivi['Ressource']);
		echo '<li>'
			. CHtml::link(
				$nbRessources . ' ressource' . ($nbRessources == 1 ? '' : 's'),
				['/ressource/index', 'suivi' => $partenaire->id]
			) . '</li>';
	}
	if (!empty($tabSuivi['Editeur'])) {
		$nbEditeurs = count($tabSuivi['Editeur']);
		if ($nbEditeurs === 1) {
			echo '<li>' . CHtml::link("1 éditeur", ['/editeur/view', 'id' => $tabSuivi['Editeur'][0]]) . '</li>';
		} else {
			echo "<li>$nbEditeurs éditeurs</li>";
		}
	}
	echo '</ul>';
} else {
	echo '<p>Aucun objet actuellement suivi.</p>';
}
?>
