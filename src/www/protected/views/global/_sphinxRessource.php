<?php

/** @var Controller $this */
/** @var array $data */
/** @var null|string $hash */
assert($this instanceof Controller);

echo HtmlHelper::ressourceLinkItem($data['attrs'], $hash ?? null);
