<?php

/** @var Controller $this */
/** @var array $data */
/** @var null|string $hash */
assert($this instanceof Controller);

echo HtmlHelper::editeurLinkItem($data['attrs'], $hash ?? null);
