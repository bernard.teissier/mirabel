<?php

/** @var Controller $this */
/** @var ?SearchNavigation $searchNavigation */
/** @var int $currentId */
/** @var string $attr */
/** @var string $anchor */
assert($this instanceof Controller);

if ($searchNavigation) {
	if (empty($anchor)) {
		$anchor = '';
	} ?>
	<div id="search-navigation">
	<?php
	list($prev, $next) = $searchNavigation->getNeighbours($currentId);
	if ($prev) {
		echo CHtml::link(
			'<i class="icon-chevron-left" id="search-nav-prev"></i>',
			['view', 'id' => $prev['id'], 's' => $searchNavigation->getPreviousHash()],
			['title' => "Résultat précédent : " . $prev['attrs'][$attr]]
		);
	} else {
		$prevPage = $searchNavigation->getPreviousPage();
		if ($prevPage) {
			echo CHtml::link(
				'<i class="icon-chevron-left" id="search-nav-prev"></i>',
				$prevPage,
				['title' => "Page précédente des résultats"]
			);
		}
	}
	echo CHtml::link(
		' <i class="icon-list" id="search-nav-up"></i> ',
		$searchNavigation->getSearchUrl($anchor),
		['title' => "Retour aux résultats de la recherche"]
	);
	if ($next) {
		echo CHtml::link(
			' <i class="icon-chevron-right" id="search-nav-next"></i>',
			['view', 'id' => $next['id'], 's' => $searchNavigation->getNextHash()],
			['title' => "Résultat suivant : " . $next['attrs'][$attr]]
		);
	} else {
		$nextPage = $searchNavigation->getNextPage();
		if ($nextPage) {
			echo CHtml::link(
				' <i class="icon-chevron-right" id="search-nav-next"></i>',
				$nextPage,
				['title' => "Page suivante des résultats"]
			);
		}
	} ?>
	</div>
<?php
}
