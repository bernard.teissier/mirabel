<?php

/** @var Controller $this */
/** @var BootActiveForm $form */
/** @var Titre|Editeur $model */
assert($this instanceof Controller);

$modelName = CHtml::modelName($model);
$defaultSources = CJSON::encode(Sourcelien::listNames());

Yii::app()->clientScript->registerScript(
	'links-input',
	<<<EOJS
	var linksSources = $defaultSources;
	$(".saisie-liens input.source").autocomplete({
		minlength: 0,
		source: linksSources
	});
	var newLinkHtml = `<div class="controls">
		<label class="subelement">Source : </label>
		<input name="{$modelName}[liens][999][src]" type="text" class="source" />
		<span style="white-space: nowrap">
			<label class="subelement">Adresse : </label>
			<input name="{$modelName}[liens][999][url]" type="text" size="80" value="" class="url input-xlarge" autocomplete="off" />
		</span>
	</div>`;
	$(".saisie-liens .add-input").click(function() {
		var block = $(this).closest(".controls");
		var numElements = $('input.source[name*="[liens]"]').length;

		var newElement = $(newLinkHtml);

		var sourceName = newElement.find("input.source").attr("name")
			.replace(/\[liens\]\[\d+\]/, "[liens][" + numElements + "]");
		newElement.find("input.source")
			.attr("name", sourceName)
			.val("");

		var urlName = newElement.find("input.url").attr("name")
			.replace(/\[liens\]\[\d+\]/, "[liens][" + numElements + "]");
		newElement.find("input.url")
			.attr("name", urlName)
			.val("");

		newElement
			.insertBefore(block)
			.find("input.source").autocomplete({
				minlength: 0,
				source: linksSources
			});
	});
	EOJS
);
?>
<div class="control-group saisie-liens">
	<label class="control-label">Autres liens externes</label>
	<?php
	$i = 0;
	if ($model->getLiensExternes()) {
		foreach ($model->getLiensExternes() as $lien) {
			/** @var Lien $lien */ ?>
			<div class="controls">
				<?= $form->errorSummary($lien); ?>
				<label class="subelement">Source : </label>
				<?php
				echo $form->textField(
				$lien,
				'src',
				['name' => $modelName . "[liens][$i][src]", 'class' => 'source']
			); ?>
				<span style="white-space: nowrap">
					<label class="subelement">Adresse :</label>
					<input name="<?= $modelName ?>[liens][<?= $i ?>][url]" type="text" value="<?= CHtml::encode($lien->url) ?>"
						class="url input-xlarge" autocomplete="off" />
					<?= HtmlHelper::displayLinksHint($form, "Liens externes", 'liensExternes', 0, $i); ?>
				</span>
			</div>
			<?php
			$i++;
		}
	}
	$numExtFields = $i + 2;
	for (; $i < $numExtFields; $i++) {
		?>
		<div class="controls">
			<label class="subelement">Source : </label>
			<input name="<?= $modelName . "[liens][$i]" ?>[src]" type="text" class="source" />
			<span style="white-space: nowrap">
				<label class="subelement">Adresse : </label>
				<input name="<?= $modelName . "[liens][$i]" ?>[url]" type="text" size="80" value="" class="url input-xlarge" autocomplete="off" />
				<?= HtmlHelper::displayLinksHint($form, "Liens externes", 'liensExternes', 0, $i); ?>
			</span>
		</div>
		<?php
	}
	?>
	<div class="controls">
		<button type="button" class="btn btn-default add-input">+</button>
	</div>
</div>

<div class="control-group saisie-liens">
	<label class="control-label">Liens internes (Titres liés)</label>
	<?php
	$startRank = $i;
	if ($model->getLiensInternes()) {
		foreach ($model->getLiensInternes() as $lien) {
			/** @var Lien $lien */ ?>
			<div class="controls">
				<?php
				if ($lien->hasErrors()) {
					echo $form->errorSummary($lien);
				} ?>
				<label class="subelement">Source : </label>
				<?= CHtml::textField($modelName . "[liens][$i][src]", $lien->src, ['class' => 'source']); ?>
				<span style="white-space: nowrap">
					<label class="subelement">Adresse : </label>
					<input name="<?= $modelName ?>[liens][<?= $i ?>][url]" type="text"
						value="<?= CHtml::encode($lien->url) ?>" class="url input-xlarge" autocomplete="off" />
					<?= HtmlHelper::displayLinksHint($form, "Liens internes", 'liensInternes', $startRank, $i); ?>
				</span>
			</div>
			<?php
			$i++;
		}
	}
	$numIntFields = $i + 2;
	for (; $i < $numIntFields; $i++) {
		?>
		<div class="controls">
			<label class="subelement">Source : </label>
			<input type="text" name=<?= $modelName ?>[liens][<?= $i ?>][src]" class="source" />
			<span style="white-space: nowrap">
				<label class="subelement">Adresse : </label>
				<input name="<?= $modelName ?>[liens][<?= $i ?>][url]" type="text" size="80" value="" class="url input-xlarge" autocomplete="off" />
				<?= HtmlHelper::displayLinksHint($form, "Liens internes", 'liensInternes', $startRank, $i) ?>
			</span>
		</div>
		<?php
	}
	?>
	<div class="controls">
		<button type="button" class="btn btn-default add-input">+</button>
	</div>
</div>
