<?php

function printLocalIntervention(CActiveRecord $model)
{
	$fieldName = strtolower(get_class($model)) . 'Id';
	if (!Yii::app()->user->isGuest) {
		$interventions = Intervention::model()
			->findAllByAttributes(
				[$fieldName => (int) $model->getPrimaryKey(), 'statut' => 'attente'],
				['order' => 'id DESC']
			);
		if ($interventions) {
			echo '<div class="alert alert-block alert-info">'
				. '<a class="close" data-dismiss="alert">&times;</a>';
			echo "<h4>Interventions en attente de validation</h4><ul>";
			foreach ($interventions as $i) {
				echo "<li>"
					. CHtml::link(date('Y-m-d H:i', $i->hdateProp), ["/intervention/view", "id" => $i->id])
					. " " . $i->description
					. '</li>';
			}
			echo "</ul>";
			echo "</div>\n";
		}
	}
}
