<?php

/** @var Controller $this */
/** @var CActiveRecord $model */
/** @var string $attribute */
/** @var BootActiveForm $form */
assert($this instanceof Controller);

use models\validators\LangIso639Validator;

if (!isset($htmlOptions)) {
	$htmlOptions = ['class' => 'span6'];
}
CHtml::resolveNameID($model, $attribute, $htmlOptions);
$error = '';
if ($model->hasErrors($attribute)) {
	$htmlOptions['class'] .= ' error';
	$error = ' error';
}

Yii::app()->getClientScript()->registerScript(
	'autocomplete-lang',
	"
var availableTags = " . json_encode(LangIso639Validator::getAutocompleteListFr()) . ";
function split( val ) {
	return val.split(/,\s*/);
}
function extractLast( term ) {
	return split(term).pop();
}
",
	CClientScript::POS_READY
);
$params = [
	'model' => $model,
	'attribute' => $attribute,
	'source' => 'js:function(request, response) {
	// delegate back to autocomplete, but extract the last term
	response(
		$.ui.autocomplete.filter(availableTags, extractLast(request.term))
	);
}',
	'options' => [
		// min letters typed before we try to complete
		'minLength' => '0',
		'focus' => 'js:function() {
	// prevent value inserted on focus
	return false;
}',
		'select' => 'js:function(event, ui) {
	var terms = split( this.value );
	// remove the current input
	terms.pop();
	// add the selected item
	terms.push( ui.item.value );
	// add placeholder to get the comma-and-space at the end
	terms.push( "" );
	this.value = terms.join( ", " );
	return false;
}',
	],
	'htmlOptions' => [
		'class' => $htmlOptions['class'],
	],
];

?>
<div class="control-group<?php echo $error; ?>">
	<label class="control-label" for="<?php echo $htmlOptions['id']; ?>">
		<?php echo $model->getAttributeLabel($attribute); ?>
	</label>
	<div class="controls">
		<?php
		$this->widget('zii.widgets.jui.CJuiAutoComplete', $params);
		//echo $form->error($model, $attribute);
		if (!empty($form->hints[$attribute])) {
			echo CHtml::tag(
				'a',
				[
					'href' => '#',
					'data-title' => $model->getAttributeLabel($attribute),
					'data-content' => $form->hints[$attribute],
					'data-html' => true,
					'data-trigger' => 'hover',
					'rel' => 'popover',
				],
				'?'
			);
		}
		?>
	</div>
</div>

