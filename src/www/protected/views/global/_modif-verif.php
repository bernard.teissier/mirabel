<?php

/** @var Controller $this */
/** @var CActiveRecord $target */
/** @var CActiveRecord $rssTarget */
assert($this instanceof Controller);

if (!isset($rssTarget)) {
	$rssTarget = $target;
}
?>
<div id="modif-verif">
<?php
if (!empty($target->hdateVerif)) {
	echo '<p class="verif">Dernière vérification : '
		. Yii::app()->format->formatDatetime($target->hdateVerif)
		. ".</p>";
}
$field = strtolower(get_class($target)) . 'Id';
$lastModif = Intervention::model()->findBySql(
	"SELECT * FROM Intervention WHERE $field = {$target->id} AND statut = 'accepté' "
	. "ORDER BY hdateVal DESC LIMIT 1"
);
if (!empty($lastModif->hdateVal)) {
	echo '<p class="modif">Dernière modification : '
		. Yii::app()->format->formatDatetime($lastModif->hdateVal);
	$modifSummary = $lastModif->render()->summary();
	if ($modifSummary) {
		echo " (" . $modifSummary . ")";
	}
	$baseUrl = rtrim(Yii::app()->baseUrl, '/');
	echo '. ' . CHtml::link(
		CHtml::image("$baseUrl/images/flux.png", "flux RSS"),
		"$baseUrl/feeds.php?type=rss2&" . strtolower(get_class($rssTarget)) . "=" . $rssTarget->id
	);
	if (!Yii::app()->user->isGuest) {
		echo " ";
		echo CHtml::link(
			'<i class="icon-calendar"></i>',
			['/intervention/admin', 'q[' . strtolower($target->tableName()) . 'Id]' => $target->id],
			['title' => "historique des interventions"]
		);
		echo "</p>";
	}
}
?>
</div>
