<?php

/** @var Controller $this */
/** @var Editeur $model */
/** @var Upload $upload */
assert($this instanceof Controller);

$this->pageTitle = "Ressource {$model->nom} - Logo";

$this->breadcrumbs = [
	'Ressources' => ['index'],
	$model->getFullName() => ['view', 'id' => $model->id],
	'Logo',
];

$hints = Hint::model()->getMessages('Ressource');
?>

<h1>Logo de la ressource <em><?= CHtml::encode($model->getFullName()); ?></em></h1>

<p class="alert alert-info">
	Le logo sera redimensionné après téléchargement.
</p>

<div>
	<?php
	$img = $model->getLogoImg();
	if ($img) {
		echo "<div style=\"display: inline-block;\">Logo actuel : $img</div>";
		echo ' <div style="display: inline-block;">'
			. HtmlHelper::postButton("Supprimer", ['logo', 'id' => $model->id], ['delete' => 1])
			. '</div>';
	} else {
		echo "<strong>Pas encore de logo.</strong>";
	}
	?>
</div>

<section style="margin-top: 2ex">
	<h2>Utiliser un logo déjà en ligne</h2>
	<?php
	$form = $this->beginWidget(
		'bootstrap.widgets.BootActiveForm',
		[
			'id' => 'ressource-logo-url-form',
			'action' => ['logo', 'id' => $model->id],
			'enableAjaxValidation' => false,
			'type' => BootActiveForm::TYPE_HORIZONTAL,
			'hints' => $hints,
		]
	);
	/** @var BootActiveForm $form */
	echo $form->errorSummary($model);
	echo $form->textFieldRow($model, 'logoUrl', ['class' => 'span11']);
	?>
	<div class="form-actions">
		<button class="btn btn-primary" type="submit">Télécharger ce logo</button>
	</div>
	<?php $this->endWidget() ?>
</section>

<section>
	<h2>Envoyer un fichier image</h2>
	<?php
	$formUp = $this->beginWidget(
		'bootstrap.widgets.BootActiveForm',
		[
			'id' => 'ressource-logo-upload-form',
			'action' => ['logo', 'id' => $model->id],
			'enableAjaxValidation' => false,
			'type' => BootActiveForm::TYPE_HORIZONTAL,
			'hints' => $hints,
			'htmlOptions' => ['enctype' => 'multipart/form-data'],
		]
	);
	/** @var BootActiveForm $formUp */
	echo $formUp->errorSummary($upload);
	echo $formUp->fileFieldRow($upload, 'file', ['class' => 'span11']);
	?>
	<div class="form-actions">
		<button class="btn btn-primary" type="submit">Envoyer ce fichier</button>
	</div>
	<?php $this->endWidget() ?>
</section>
