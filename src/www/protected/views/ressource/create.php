<?php

/** @var Controller $this */
/** @var Ressource $model */
/** @var bool $direct */
/** @var Intervention $intervention */
assert($this instanceof Controller);

$this->pageTitle = 'Ressource - Créer';

$this->breadcrumbs = [
	'Ressources' => ['index'],
	'Créer',
];

$this->menu = [
	['label' => 'Liste', 'url' => ['index']],
];
?>

<h1>Nouvelle ressource</h1>

<?php
echo $this->renderPartial(
	'_form',
	['model' => $model, 'direct' => $direct, 'intervention' => $intervention]
);
?>
