<?php

/** @var Controller $this */
/** @var int $ressourceId */
assert($this instanceof Controller);

?><h2 class="collapse-toggle">Exporter les accès en ligne</h2>
<div class="collapse-target">
<?php

$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'exportServices-form',
		'action' => $this->createUrl('/service/export'),
		'method' => 'get',
		'enableAjaxValidation' => false,
		'htmlOptions' => ['class' => 'well'],
		'type' => BootActiveForm::TYPE_HORIZONTAL,
	]
);
/** @var BootActiveForm $form */
$model = new Service('filter');
?>
<fieldset>
	<legend>Exporter les accès en ligne</legend>
	<input type="hidden" name="q[ressourceId][]" value="<?= $ressourceId; ?>" />
	<input type="hidden" name="Service[ressourceId]" value="<?= $ressourceId; ?>" />
	<div class="control-group">
		<label class="control-label" for="format">Format</label>
		<div class="controls">
			<select id="format" name="format">
				<option value="kbart" selected="selected">KBART</option>
				<option value="csv">CSV</option>
			</select>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="Service_Acces">Type d'accès</label>
		<div class="controls">
			<label><input name="Service[acces]" value="" type="radio" /> Tous</label>
			<label><input name="Service[acces]" value="Libre" checked="checked" type="radio" /> Libres</label>
			<label><input name="Service[acces]" value="Restreint" type="radio" /> Restreints</label>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="Service_Type">Contenu</label>
		<div class="controls">
			<label><input name="Service[type][]" value="Intégral" type="checkbox" /> Limiter au texte intégral</label>
		</div>
	</div>
	<?php
	echo $form->textFieldRow(
	$model,
	'hdateModif',
	['size' => 10, 'class' => 'span2', 'labelOptions' => ['label' => 'Modifié après (AAAA-MM-DD)']]
);
	?>
	<div class="form-actions">
		<?php
		$this->widget(
		'bootstrap.widgets.BootButton',
		[
			'buttonType' => 'submit',
			'label' => "Exporter",
		]
	);
		?>
	</div>
</fieldset>

<?php
$this->endWidget();
?>
</div>
