<?php

/** @var Controller $this */
/** @var SphinxDataProvider $dataProvider */
/** @var int $suivi partenaireId */
/** @var string $import */
/** @var string $lettre */
/** @var string $searchHash */
/** @var bool $noweb */
assert($this instanceof Controller);

$this->pageTitle = 'Ressources';

$this->breadcrumbs=[
	'Ressources',
];

$this->menu=[
	['label' => 'Créer une ressource', 'url' => ['create']],
];
?>

<p>
	Dans Mir@bel, une ressource est un site proposant un ou des accès en ligne à des contenus de revues.
	Par exemple un bouquet de revues, un catalogue d'éditeur, une base de sommaires, une base de données bibliographiques ou le site web de la revue…
</p>

<div class="legende">
	<h3>Légende</h3>
	<ul>
		<?php
		if (Yii::app()->user->isGuest) {
			?>
		<li class="ressource suivi-other">Suivie par un partenaire</li>
		<?php
		} else {
			?>
		<li class="ressource suivi-self">Suivie par son propre partenaire</li>
		<li class="ressource suivi-other">Suivie par un autre partenaire</li>
		<?php
		}
		?>
	</ul>
</div>

<h1>Ressources <?php
if ($suivi > 0) {
			$partenaire = Partenaire::model()->findByPk($suivi);
			echo " suivies par le partenaire <em>" . CHtml::encode($partenaire->nom) . "</em>";
		} elseif ($suivi < 0) {
			echo " suivies par un partenaire";
		} elseif ($import === 'ressource') {
			echo " mises à jour automatiquement";
		} elseif ($import === 'collection') {
			echo " ayant des collections mises à jour automatiquement";
		} elseif (!empty($lettre)) {
			echo " — " . $lettre;
		}
?></h1>

<?php if ($suivi) { ?>
<p>
	<?= $dataProvider->sphinxClient->result['total_found'] ?> ressources suivies dans Mir@bel.
</p>
<?php } elseif ($import === 'ressource') { ?>
<p>
	<?= $dataProvider->sphinxClient->result['total_found'] ?> ressources mises à jour automatiquement dans Mir@bel.
</p>
<?php } elseif ($import === 'collection') { ?>
<p>
	<?= $dataProvider->sphinxClient->result['total_found'] ?> ressources pour lesquelles une partie des collections sont mises à jour
	automatiquement dans Mir@bel (mais pas toutes).
	Vous pouvez en parallèle consulter la liste des ressources dont toutes les collections sont
	<?= CHtml::link("mises à jour automatiquement", ['/ressource/index', 'import' => 'ressource']) ?>.
</p>
<?php } ?>

<?php
if (Yii::app()->user->isGuest && empty($suivi)) {
	echo CHtml::link(
		CHtml::image(Yii::app()->baseUrl . "/images/plus.png", "Ajout de ressource"),
		['create'],
		[
			'style' => 'float: right; margin-right: 40px;',
			'title' => 'Proposer de créer une nouvelle ressource',
		]
	);
}
$dataProvider->getData();

Tools::printSubPagination($dataProvider);
?>

<table class="items table list-results">
	<thead>
		<th>Nom</th>
		<th>Revues</th>
	</thead>
	<tbody>
		<?php
		foreach ($dataProvider->getData() as $result) {
			echo '<tr class="' . HtmlHelper::linkItemClass('ressource', $result['attrs']['suivi']) . '"><td>'
				. CHtml::link(
					CHtml::encode($result['attrs']['nomcomplet']),
					['view', 'id' => $result['attrs']['id'], 'nom' => Norm::urlParam($result['attrs']['nomcomplet']), 's' => $searchHash]
				)
				 . ($result['attrs']['web'] ? ' (site web)' : '')
				. '</td><td>'
				. CHtml::link(
					$result['attrs']['nbrevues'] . ' revue' . ($result['attrs']['nbrevues'] > 1 ? 's' : ''),
					['/revue/search', 'SearchTitre[ressourceId][]' => $result['attrs']['id']]
				) . '</td></tr>';
		}
		?>
	</tbody>
</table>

<?php Tools::printSubPagination($dataProvider); ?>

<div>
	<?php
	if (!empty($dataProvider->pagination)) {
		$url = ['index'];
		$letter = $dataProvider->pagination->getCurrentLetter();
		if (!ctype_digit($letter)) {
			$url['lettre'] = $letter;
		} else {
			$url['lettre'] = 9;
		}
		$url["noweb"] = !$noweb;
		if ($noweb) {
			echo CHtml::link("Afficher aussi le type <i>site web de la revue</i", $url, ['class' => 'btn']);
		} else {
			echo CHtml::link("Masquer le type <i>site web de la revue</i>", $url, ['class' => 'btn']);
		}
	}

	if (Yii::app()->user->isGuest) {
		echo " ", CHtml::link("Lister les ressources partenaires", ['partenaire/index', '#' => 'tab-2'], ['class' => 'btn', 'title' => "Ressources qui sont aussi des partenaires de Mir@bel"]);
	} else {
		echo '<div style="margin-top: .5ex">Listes restreintes aux ressources… ';
		echo CHtml::link("… partenaires", ['partenaire/index', '#' => 'tab-2'], ['class' => 'btn'])
			, " "
			, CHtml::link("… suivies", ['index', 'suivi' => -1], ['class' => 'btn', 'title' => "Ressources suivies dans Mir@bel par un partenaire"])
			, " "
			, CHtml::link("… mises à jour automatiquement", ['index', 'import' => 'ressource'], ['class' => 'btn', 'title' => "Ressources avec un import automatique, même partiel"])
			, " "
			, CHtml::link("… avec des collections mises à jour automatiquement", ['index', 'import' => 'collection'], ['class' => 'btn', 'title' => "Ressources ayant au moins une collection qui soit importée automatiquement"]);
		echo "</div>";
	}
	?>
</div>
