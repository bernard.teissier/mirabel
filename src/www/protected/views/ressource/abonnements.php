<?php

/** @var Controller $this */
/** @var Ressource $model */
/** @var CActiveRecord[] $cibles */

assert($this instanceof Controller);

$this->pageTitle = 'Abonnés à ' . $model->nom;

$this->breadcrumbs = [
	'Ressources' => ['ressource/index'],
	($model->sigle ?: $model->nom) => ['ressource/view', 'id' => $model->id],
	"Abonnés",
];

$this->menu = [
	['label' => 'Retour à la ressource', 'url' => ['ressource/view', 'id' => $model->id]],
];
?>

<h1>Abonnés à <em><?= CHtml::encode($model->nom) ?></em></h1>

<?php
foreach ($cibles as $cible) {
	/** @var Collection|Ressource $cible */
	echo "<h2>" . $cible->tableName() . " : " . CHtml::encode($cible->nom) . "</h2>\n";
	$abonnements = $cible->getAbonnements();
	if ($abonnements) {
		echo "<ul>";
		foreach ($abonnements as $ab) {
			/** @var Abonnement $ab */
			echo "<li>" . $ab->partenaire->getSelfLink() . "</li>\n";
		}
		echo "</ul>";
	} else {
		echo "<p>Aucun abonné.</p>";
	}
}
