<?php

/** @var Controller $this */
/** @var Collection|Ressource $model */
/** @var AbonnementSearch $abonnements */
assert($this instanceof Controller);

$context = ['partenaireId' => Yii::app()->user->getState('partenaireId')];

if ($model instanceof Collection) {
	$modelType = $model->type;
	$context['collectionId'] = $model->id;
} else {
	$modelType = "";
	$context['ressourceId'] = $model->id;
}

$aboDecorator = new AbonnementDecorator($abonnements->readStatus($model), $modelType);
echo $aboDecorator->getHtmlButton($context);

$abo = $abonnements->readAbonnement($model);
if ($abo && $abo->mask == Abonnement::ABONNE) {
	?>
	<div class="label label-<?= $abo->proxy ? 'success' : 'warning' ?>" title="<?= $abo->proxy ? 'proxy activé' : 'pas de proxy' ?>">
		<i class="icon-<?= $abo->proxy ? 'ok' : 'remove' ?>"></i>proxy
		<?php
		if ($abo->proxy) {
			echo CHtml::link(
				$abo->proxyUrl ? "URL" : "<s>URL</s>",
				'#',
				[
					'class' => "proxyurl",
					'data-attributes' => json_encode($abo->attributes),
					'title' => ($abo->proxyUrl ?: "") . "\nURL de proxy spécifique à cet abonnement ",
				]
			);
		} ?>
		<?= CHtml::form(['/abonnement/toggleProxy', 'id' => $abo->id]) ?>
			<button class="btn btn-mini" title="Changer l'activation du proxy"><i class="icon-refresh"></i></button>
		<?= CHtml::endForm() ?>
	</div>
	<?php
}

Yii::app()->clientScript->registerScript(
	'change-abonnement-proxyurl',
	<<<'EOL'
			$("#ressource-collections").on("click", "a.proxyurl", function(event) {
				event.stopPropagation();
				var abonnement = $(this).data("attributes");
				$('#modal-abonnement-proxyurl .abonnement-id').val(abonnement.id);
				$('#modal-abonnement-proxyurl .abonnement-proxyurl').val(abonnement.proxyUrl);
				$('#modal-abonnement-proxyurl').modal('show');
			});
		EOL
);
