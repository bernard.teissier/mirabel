<?php

/** @var Controller $this */
/** @var Ressource $model */
/** @var bool $direct */
/** @var Intervention $intervention */
assert($this instanceof Controller);

/** @var BootActiveForm $form */
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'ressource-form',
		'enableAjaxValidation' => false,
		'hints' => Hint::model()->getMessages('Ressource'),
		'htmlOptions' => ['class' => 'well'],
		'type' => BootActiveForm::TYPE_HORIZONTAL,
	]
);
?>

<div class="alert">
	<?php
	if ($direct) {
		echo "Les modifications seront appliquées immédiatement.";
	} else {
		echo "Les modifications que vous proposez devront être validées "
			,"par un membre partenaire de Mi@bel avant d'être appliquées.";
	}
	?>
</div>

<?php
echo HtmlHelper::errorSummary([$model, $intervention]);
if (!Yii::app()->user->isGuest) {
	echo UrlFetchableValidator::toggleUrlChecker($model);
}
$this->renderPartial(
	'/intervention/_errors',
	[
		'form' => $form,
		'intervention' => $intervention,
		'model' => $model,
	]
);

$yesno = ['' => 'Indéterminé', 'Non', 'Oui'];
echo $form->textFieldRow($model, 'nom', ['class' => 'span5']);
if (isset($model->confirm) and $model->confirm === false) {
	echo $form->checkBoxRow($model, 'confirm');
}
echo $form->textFieldRow($model, 'prefixe', ['class' => 'span2']);
echo $form->textFieldRow($model, 'sigle', ['class' => 'span4']);
echo $form->textFieldRow($model, 'url', ['class' => 'span8']);
echo $form->dropDownListRow($model, 'type', Ressource::$enumType);
echo $form->dropDownListRow($model, 'acces', Ressource::$enumAcces);
echo $form->textAreaRow($model, 'description', ['rows' => 6, 'cols' => 50, 'class' => 'span8']);
echo $form->textAreaRow($model, 'noteContenu', ['rows' => 6, 'cols' => 50, 'class' => 'span8']);
echo $form->textFieldRow($model, 'disciplines', ['class' => 'span5']);
echo $form->textFieldRow($model, 'diffuseur', ['class' => 'span5']);
echo $form->textFieldRow($model, 'partenaires', ['class' => 'span5']);
echo $form->textFieldRow($model, 'partenairesNb', ['class' => 'span1']);
echo $form->textFieldRow($model, 'revuesNb', ['class' => 'span5']);
echo $form->textFieldRow($model, 'articlesNb', ['class' => 'span5']);
echo $form->dropDownListRow($model, 'indexation', $yesno);
echo $form->dropDownListRow($model, 'alerteRss', $yesno);
echo $form->dropDownListRow($model, 'alerteMail', $yesno);
echo $form->dropDownListRow($model, 'exportPossible', $yesno);
$htmlOptions = [];
if (!Yii::app()->user->checkAccess('ressource/admin')) {
	$htmlOptions["disabled"] = "disabled";
}
echo $form->dropDownListRow(
	$model,
	'exhaustif',
	[
		Ressource::EXHAUSTIF_INDET => '?',
		Ressource::EXHAUSTIF_OUI => 'Oui, toutes les revues de cette ressource sont signalées dans Mir@bel',
		Ressource::EXHAUSTIF_NON => 'Non, une partie seulement des revues de cette ressource sont signalées dans Mir@bel',
	],
	$htmlOptions
);
echo $form->dropDownListRow($model, 'autoImport', ['Non', 'Oui'], $htmlOptions);
echo $form->textFieldRow($model, 'importIdentifications', ['class' => 'span8']);
echo $form->dropDownListRow($model, 'partenaire', ['Non', 'Oui'], $htmlOptions);

$this->renderPartial('/intervention/_contact', ['form' => $form, 'intervention' => $intervention, 'canForceValidating' => !$model->isNewRecord]);
?>
<div class="form-actions">
	<?php
	if ($direct) {
		$label = $model->isNewRecord ? 'Créer' : 'Enregistrer';
	} else {
		$label = $model->isNewRecord ? 'Proposer de créer cette ressource' : 'Proposer cette modification';
	}
	$this->widget(
		'bootstrap.widgets.BootButton',
		[
			'buttonType' => 'submit',
			'type' => 'primary',
			'label' => $label,
		]
	);
	?>
</div>

<?php
$this->endWidget();
?>
