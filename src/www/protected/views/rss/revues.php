<?php

/** @var Controller $this */
/** @var Titre $titre */
assert($this instanceof Controller);

$fullTitle = $titre->getFullTitle();
?>
<h2>Création de la revue <?php
	echo CHtml::link(htmlspecialchars($fullTitle), ['/revue/view', 'id' => $titre->revueId]);
	?></h2>

<table border="1">
	<thead>
		<tr>
			<th colspan="2"><?php echo htmlspecialchars($fullTitle); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php if ($titre->url) { ?>
		<tr>
			<td><b>URL</b></td>
			<td><?php echo CHtml::link(CHtml::encode($titre->url), $titre->url); ?></td>
		</tr>
		<?php } ?>
		<?php if ($titre->editeurs) { ?>
		<tr>
			<td><b>Éditeurs</b></td>
			<td><?php
			foreach ($titre->editeurs as $e) {
				echo CHtml::encode($e->nom);
			}
			?></td>
		</tr>
		<?php
		}
		$periode = $titre->getPeriode();
		if ($periode) {
			?>
		<tr>
			<td><b>Années</b></td>
			<td><?php echo CHtml::encode($periode); ?></td>
		</tr>
		<?php
		}
		if ($titre->periodicite) {
			?>
		<tr>
			<td><b>Périodicité</b></td>
			<td><?php echo CHtml::encode($titre->periodicite); ?></td>
		</tr>
		<?php
		} ?>
		<?php if ($titre->langues) { ?>
		<tr>
			<td><b>Langues</b></td>
			<td><?= \models\lang\Convert::codesToFullNames($titre->langues); ?></td>
		</tr>
		<?php } ?>
	</tbody>
</table>
