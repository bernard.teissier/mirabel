<?php

/** @var Controller $this */
/** @var int $siteId */
/** @var string $rootUrl */
assert($this instanceof Controller);

$user = Yii::app()->getUser();
?>
<script>
	var _paq = window._paq || [];
	<?php
	// tracker methods like "setCustomDimension" should be called before "trackPageView"

	$pId = empty($_COOKIE['institute']) ? null : (int) $_COOKIE['institute'];
	if ($pId) {
		$ipInstitute = Partenaire::model()->find($pId);
		printf(
			"_paq.push(['setCustomVariable', 1, 'Établissement /IP', %s, 'visit']);",
			json_encode($ipInstitute->getShortName())
		);
	}
	if ($this->institute) {
		printf(
			"_paq.push(['setCustomVariable', 2, 'Établissement', %s, 'page']);",
			json_encode($this->institute->getShortName())
		);
	}
	if (!$user->isGuest) {
		printf("_paq.push(['setUserId', %s]);", json_encode($user->login));
		$userPartenaire = Partenaire::model()
			->findBySql("SELECT * FROM Partenaire p JOIN Utilisateur u ON p.id = u.partenaireId WHERE u.id = " . (int) $user->id);
		printf(
			"_paq.push(['setCustomVariable', 3, 'Partenaire authentifié', %s, 'page']);",
			json_encode($userPartenaire->getShortName())
		);
	} elseif ($this->action->id === 'logout') {
		echo "_paq.push(['resetUserId']);";
	}
	?>
	_paq.push(['trackPageView']);
	_paq.push(['enableLinkTracking']);
	(function() {
		var u=<?= json_encode($rootUrl) ?>;
		_paq.push(['setTrackerUrl', u+'matomo.php']);
		_paq.push(['setSiteId', <?= json_encode($siteId) ?>]);
		var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
		g.type = 'text/javascript';
		g.async = true;
		g.src = u + 'matomo.js';
		s.parentNode.insertBefore(g, s);
	})();
</script>
<noscript>
  <img src="<?= $rootUrl ?>matomo.php?idsite=<?= (int) $siteId ?>&amp;rec=1" style="border:0;" alt="" />
</noscript>
