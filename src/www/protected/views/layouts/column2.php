<?php

/** @var Controller $this */
/** @var string $content */
assert($this instanceof Controller);

$this->beginContent('//layouts/main');
?>
<div class="row-fluid">
	<div class="span10">
		<div id="content">
			<?php echo $content; ?>
		</div><!-- content -->
	</div>
	<div class="span2" id="sidebar">
	<?php
	if (!Yii::app()->user->isGuest && !empty($this->menu)) {
		array_unshift(
			$this->menu,
			['label' => 'Opérations', 'itemOptions' => ['class' => 'nav-header']]
		);
		$this->beginWidget(
			'bootstrap.widgets.BootMenu',
			[
				'type' => 'list',
				'items' => $this->menu,
				'htmlOptions' => ['class' => 'well'],
			]
		);
		$this->endWidget();
	}
	if ($this->sidebarInsert) {
		echo $this->sidebarInsert;
	}
	if (!Yii::app()->user->isGuest && !empty($this->menuExt)) {
		$this->widget(
			'bootstrap.widgets.BootMenu',
			[
				'type' => 'list',
				'items' => $this->menuExt,
				'htmlOptions' => ['class' => 'alert', 'style' => 'margin-top: 2ex'],
			]
		);
	}
	?>
	</div><!-- sidebar -->
</div>
<?php $this->endContent(); ?>
