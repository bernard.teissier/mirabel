<?php

/** @var Controller $this */
assert($this instanceof Controller);

$user = Yii::app()->user;

$menu = [
	['label' => 'Le réseau', 'url' => ['/site/page', 'p' => 'presentation']],
	$user->isGuest
		? ['label' => 'Partenaires', 'url' => ['/partenaire']]
		: ['label' => 'Partenaires', 'items' => [
			['label' => 'Aide professionnelle et documentation', 'url' => ['/site/page', 'p' => 'aide']],
			['label' => "Indicateurs de contenu", 'url' => ['/stats']],
			['label' => "Indicateurs de suivi", 'url' => ['/stats/suivi'], 'visible' => $user->checkAccess('stats/suivi')],
			['label' => 'Liste des partenaires', 'url' => ['/partenaire']],
			['label' => 'Liste des personnes membres', 'url' => ['/site/page', 'p' => 'partenaires']],
			['label' => 'Listes de diffusion et adresses', 'url' => ['/site/page', 'p' => 'diffusion']],
			['label' => 'Outils de communication', 'url' => ['/site/page', 'p' => 'communication']],
			['label' => 'Pilotage du réseau', 'url' => ['/site/page', 'p' => 'pilotage']],
			['label' => "Statistiques de fréquentation", 'url' => ['/statsacces/index']],
		]],
	['label' => 'Admin', 'url' => '#', 'visible' => $user->checkAccess('menu-veilleur'), 'items' => [
		['label' => "Administration", 'url' => ['/admin/index'], 'visible' => $user->checkAccess('admin')],
		['label' => "Bulles d'aide", 'url' => ['/hint/admin'], 'visible' => $user->checkAccess('redaction/aide')],
		['label' => "Dépôt de fichiers", 'url' => ['/upload'], 'visible' => $user->checkAccess('redaction/editorial') || $user->checkAccess('partenaire/upload')],
		['label' => "Documentation technique", 'url' => ['/doc']],
		['label' => "Fonctions éditoriales", 'url' => ['/cms'], 'visible' => $user->checkAccess('redaction/editorial') || $user->checkAccess('partenaire/editorial')],
		[
			'label' => 'Imports', 'url' => ['/import/kbart'],
			'visible' => $user->checkAccess('import'),
		],
		['label' => 'Interventions', 'url' => ['/intervention/admin']],
		['label' => "Vérification des données", 'url' => ['/verification/index']],
	]],
	['label' => 'Listes', 'url' => '#', 'itemOptions' => ['id' => 'item-list'], 'items' => [
		['label' => 'Revues', 'url' => ['/revue/index']],
		['label' => 'Ressources', 'url' => ['/ressource/index']],
		['label' => 'Éditeurs', 'url' => ['/editeur/index']],
		['label' => "Thématique", 'url' => ['/categorie']],
	]],
];
$rightMenu = [];
if ($user->isGuest) {
	if ($this->institute) {
		$img = $this->institute->getLogoImg(true, ['height' => 40]);
		$itemOptions = ['title' => "Vous consultez Mir@bel avec la personnalisation " . $this->institute->nom];
		if ($img) {
			$itemOptions['class'] = 'etablissement';
		}
		$rightMenu[] = [
			'label' => ($img ?: "[" . ($this->institute->sigle ?: $this->institute->nom) . "]"),
			'encodeLabel' => !$img,
			'url' => ['/partenaire/view', 'id' => $this->institute->id],
			'itemOptions' => $itemOptions,
		];
	} else {
		$rightMenu[] = [
			'label' => "[personnalisation]",
			'url' => ['/site/page', 'p' => 'personnalisation'],
			'itemOptions' => ['title' => "Vous consultez la vue par défaut de Mir@bel ; sélectionnez un établissement pour bénéficier d'informations personnalisées"],
		];
	}
} else {
	array_push(
		$rightMenu,
		[
			'label' => '<i class="icon icon-th-list"></i>',
			'encodeLabel' => false,
			'url' => ['/partenaire/tableauDeBord', 'id' => $user->partenaireId],
			'itemOptions' => ['title' => "Tableau de bord de suivi pour la veille"],
		],
		[
			'label' => "[P]",
			'url' => ['/partenaire/view', 'id' => $user->partenaireId],
			'itemOptions' => ['title' => (empty($this->partenaire) ? "Mon profil de partenaire Mir@bel" : $this->partenaire->nom)],
		],
		[
			'label' => '<i class="icon-user"></i>',
			'encodeLabel' => false,
			'url' => ['/utilisateur/view', 'id' => $user->id],
			'itemOptions' => ['title' => "Consulter/modifier mon profil"],
		],
		[
			'label' => "Déconnexion<br />" . CHtml::encode($user->nomComplet),
			'encodeLabel' => false,
			'url' => ['/site/logout'],
			'itemOptions' => ['class' => 'double-line'],
		]
	);
}

$appName = Yii::app()->name . ' <em>' . Yii::app()->params->itemAt('version') . '</em>';
$navItems = [
	[
		'class' => 'bootstrap.widgets.BootMenu',
		'items' => $menu,
	],
];
if ($this->route !== 'site/index') {
	$navItems[] = $this->getSearchForm();
	$navItems[] = [
		'class' => 'bootstrap.widgets.BootMenu',
		'items' => [
			[
				'label' => 'Recherche<br />avancée',
				'url' => ['/revue/search'],
				'encodeLabel' => false,
				'itemOptions' => ['class' => 'double-line'],
			],
		],
	];
}
$navItems[] = [
	'class' => 'bootstrap.widgets.BootMenu',
	'items' => [
		[
			'label' => '?', 'url' => ['/site/page', 'p' => 'aide'],
			'itemOptions' => ['title' => 'Aide', 'id' => 'navlink-help'],
		],
	],
];
if ($rightMenu) {
	$navItems[] = [
		'class' => 'bootstrap.widgets.BootMenu',
		'htmlOptions' => ['class' => 'pull-right'],
		'items' => $rightMenu,
	];
}
$this->widget(
	'bootstrap.widgets.BootNavbar',
	[
		'fixed' => false,
		'fluid' => true,
		'collapse' => true, // requires bootstrap-responsive.css
		'brand' => (file_exists(Yii::app()->basePath . '/../images/logo-mirabel.png') ?
			CHtml::image(Yii::app()->baseUrl . '/images/logo-mirabel.png', strip_tags($appName), ['id' => 'logo', 'title' => "Le site qui facilite l'accès aux revues"])
			: $appName),
		'items' => $navItems,
	]
);
unset($menu, $rightMenu);
