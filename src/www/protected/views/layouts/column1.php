<?php

/** @var Controller $this */
/** @var string $content */
assert($this instanceof Controller);

?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="row-fluid">
	<div id="content">
		<?php echo $content; ?>
	</div><!-- content -->
</div>
<?php $this->endContent(); ?>
