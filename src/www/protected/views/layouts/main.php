<?php

/** @var Controller $this */
/** @var string $content */
assert($this instanceof Controller);

$user = Yii::app()->getUser();
$isMediaPrint = isset($_GET['media']) && $_GET['media'] === 'print';
if (strncmp($this->pageTitle, Yii::app()->name, 5)) {
	$this->pageTitle = Yii::app()->name . ' - ' . $this->pageTitle;
}

$cs = Yii::app()->getClientScript();
$cs->registerScriptFile('/js/main.js', CClientScript::POS_END);
$cs->registerCssFile('/css/main.css');
$cs->registerCssFile('/css/screen.css', 'screen');
$cs->registerCssFile('/css/screen-large.css', 'screen and (min-width: 1200px)');
$cs->registerCssFile('/css/screen-small.css', 'screen and (max-width: 990px)');
$cs->registerCssFile('/css/print.css', ($isMediaPrint ? '' : 'print'));
if (file_exists(dirname(Yii::app()->getBasePath()) . '/css/local.css')) {
	$cs->registerCssFile('/css/local.css');
}
if (file_exists(dirname(Yii::app()->getBasePath()) . '/images/non-officielle.png')) {
	$cs->registerCss(
		'non-officielle',
		<<<EOCSS
			@media screen {
				body {
					background-image: url(/images/non-officielle.png);
					background-repeat: no-repeat;
					background-position: left 200px;
					background-color:rgba(255,0,0,0.04);
				}
			}
			EOCSS
	);
}
?><!doctype html>
<html lang="fr">
<head>
	<meta charset="utf-8">

	<title><?= CHtml::encode($this->pageTitle) ?></title>

	<link rel="shortcut icon" href="<?= Yii::app()->getBaseUrl() ?>/images/favicon.ico" type="image/x-icon" />
	<link rel="alternate" type="application/atom+xml" href="<?= CHtml::encode($this->createAbsoluteUrl('/rss/breves')) ?>" title="Actualités du réseau Mir@bel" />
	<?= Controller::linkEmbeddedFeed('rss2') ?>
	<?= Controller::linkEmbeddedFeed('atom') ?>
	<?= $this->getHtmlHead() ?>
</head>

<body>
	<header id="header">
		<div id="navbar-top-separator"></div>
	<?php
	if (!$isMediaPrint) {
		$this->renderPartial('//layouts/_mainMenu', []);
		if ($this->institute && !empty($this->institute->phrasePerso)) {
			echo CHtml::tag(
				'div',
				['id' => "institute-warning"],
				CHtml::encode(str_replace('%NOM%', $this->institute->nom, $this->institute->phrasePerso))
			);
		}
	}
	?>
		<div class="navbar-bottom-separator"></div>
	</header>

<?php
if ($this->backgroundL) {
		echo '<div class="feuilles-g">';
	}
if ($this->backgroundR) {
	echo '<div class="feuilles-d">';
}
?>
<div class="container" id="page">
	<?php
	// Breadcrumbs
	if (isset($this->breadcrumbs)) {
		$this->widget(
			'bootstrap.widgets.BootBreadcrumbs',
			[
				'links' => $this->breadcrumbs,
			]
		);
	}

	// Flash messages: 'success', 'info', 'warning', 'error', 'danger'
	$this->widget('bootstrap.widgets.BootAlert');

	// The page content
	echo $content;
	?>
</div>
<?php
if ($this->backgroundR) {
		echo '</div>';
	}
if ($this->backgroundL) {
	echo '</div>';
}
?>

<footer>
	<div class="slogan">
		Mir@bel, le site qui facilite l'accès aux revues
		<?php
		if ($this->id === 'site' && $this->action->id === 'index') {
			// page d'accueil
			echo "— ISSN 2678-0038";
		}
		?>
	</div>
	<div id="footer-message">
		<div class="links">
			<a href="<?= $this->createUrl('/site/contact') ?>" title="Pour nous contacter">Contact</a>
			<a href="<?= $this->createUrl('/site/page/conditions-utilisation') ?>" title="Conditions d'utilisation de ce site web">Conditions d'utilisation</a>
			<?php if ($user->isGuest) { ?>
			<a href="<?= $this->createUrl('/site/login') ?>" title="Connexion réservée aux partenaires">Connexion</a>
			<?php } ?>
		</div>
		<div class="footer-main">
			<div class="footer-logo">
				<?php
				if ($this->institute) {
					$logo = $this->institute->getLogoImg();
					echo CHtml::link(
						($logo ?: CHtml::encode($this->institute->nom)),
						['/partenaire/view', 'id' => $this->institute->id],
						['title' => "Vous êtes un utilisateur de " . $this->institute->nom]
					);
				}
				?>
			</div>
			<div class="block">
				<?php
				$pilotesTxt = Config::read('partenaires.pilotes');
				if ($pilotesTxt) {
					echo "<div>Mir@bel est piloté par ";
					$pilotes = array_filter(array_map('trim', explode("\n", $pilotesTxt)));
					foreach ($pilotes as $rank => $line) {
						$parts = explode(" => ", $line);
						if (count($parts) > 2) {
							echo CHtml::link(CHtml::encode($parts[1]), ['/partenaire/view', 'id' => (int) $parts[0]], ['title' => $parts[2]]);
						} elseif (count($parts) === 2) {
							echo CHtml::link(CHtml::encode($parts[1]), ['/partenaire/view', 'id' => (int) $parts[0]]);
						} else {
							echo CHtml::tag('span', [], CHtml::encode($line));
						}
						if ($rank < count($pilotes) - 2) {
							echo ", ";
						} elseif ($rank == count($pilotes) - 2) {
							echo " et ";
						} else {
							echo ".";
						}
					}
					echo "</div>";
				}
				?>
				<div>
					L'interface est développée par SILECS et a été financée par la Région Auvergne Rhône-Alpes.
				</div>
				<form id="form-footer-institute" action="#" style="display:none;">
					<div>
					<?php
					echo CHtml::dropDownList(
					'sel-institute',
					empty($this->institute) ? '' : (string) $this->institute->id,
					Tools::sqlToPairs("SELECT id, IF(sigle <> '', sigle, nom) AS name FROM Partenaire WHERE type='normal' AND statut='actif' ORDER BY name"),
					[
						'empty' => '[partenaire]',
						'title' => "Sélectionnez votre établissement pour personnaliser votre navigation sur le site Mir@bel",
					]
				);
					?>
					</div>
				</form>
			</div>
			<div id="logo-rra" class="footer-logo">
				<img src="<?php echo Yii::app()->getBaseUrl(); ?>/images/logo-RRA.png" alt="Région Rhône-Alpes" />
			</div>
		</div>
	</div>
	<div id="footer-sep"></div>
	<?php
	if ((YII_DEBUG || !$user->isGuest) && Yii::app()->params->itemAt('vcsVersion')) {
		echo '<div class="debug-info">'
			. "<em>version" . (YII_DEBUG ? " de développement" : "") . "</em> "
			. Yii::app()->params->itemAt('vcsVersion')
			. "</div>";
		if (Yii::app()->params->itemAt('dataVersion')) {
			echo '<div class="debug-info">Données synchronisées le ' . Yii::app()->params->itemAt('dataVersion') . '</div>';
		}
	}

	$matomo = Yii::app()->params->itemAt('matomo');
	if ($matomo && !empty($matomo['siteId'])) {
		$this->renderPartial('//layouts/_matomo', $matomo);
	}
	?>
</footer>

</body>
</html>
