<?php

/** @var Controller $this */
/** @var SearchGlobal $model */
/** @var array $search */
/** @var int $totalCount */
assert($this instanceof Controller);

$this->pageTitle = 'Recherche : ' . $model->q;

$this->breadcrumbs = [
	'Recherche globale',
];
?>

<?= $this->renderPartial('/revue/_legend') ?>

<h1>
	Recherche :
	<em>
		<?php echo CHtml::link(CHtml::encode($model->q), ['/site/search', 'global' => $model->q]); ?>
	</em>
</h1>

<div id="global-search-results">
<?php
if (!$model->validate()) {
	return;
}

if ($totalCount === 0) {
	echo "<p>Aucun résultat trouvé.</p>";
	if (!Yii::app()->user->isGuest) {
		?>
		<ul>
			<li><?= CHtml::link("Créer une revue…", ['/titre/createByIssn'], ['class' => 'btn']) ?></li>
			<li><?= CHtml::link("Créer une ressource…", ['/ressource/create'], ['class' => 'btn']) ?></li>
			<li><?= CHtml::link("Créer un éditeur…", ['/editeur/create'], ['class' => 'btn']) ?></li>
		</ul>
		<?php
	}
	return;
}

$revuesCount = isset($search['titres']) ? (int) $search['titres']->itemCount : 0;
if ($revuesCount || !Yii::app()->user->isGuest) {
	?>
	<section class="well <?= $revuesCount ? '' : " empty" ?>" id="search-revues">
		<div>
			<?php
			if (!Yii::app()->user->isGuest) {
				?>
				<div class="create-button">
					<?= CHtml::link("Créer une revue…", ['/titre/createByIssn'], ['class' => 'btn']) ?>
				</div>
				<?php
			} ?>
			<h2>Revues</h2>
		</div>
		<?php
		if ($revuesCount) {
			$this->widget(
				'bootstrap.widgets.BootListView',
				[
					'dataProvider' => $search['titres'],
					'itemView' => '/global/_sphinxTitre',
					'ajaxUpdate' => false,
					'viewData' => ['hash' => $search['hashes']['titres']],
				]
			);
		} ?>
	</section>
	<?php
}

$ressourcesCount = isset($search['ressources']) ? (int) $search['ressources']->itemCount : 0;
if ($ressourcesCount || !Yii::app()->user->isGuest) {
	?>
	<section class="well" id="search-ressources">
		<div>
			<?php
			if (!Yii::app()->user->isGuest) {
				?>
				<div class="create-button">
					<?= CHtml::link("Créer une ressource…", ['/ressource/create'], ['class' => 'btn']) ?>
				</div>
				<?php
			} ?>
			<h2 title="Dans Mir@bel, une ressource est un site proposant un ou des accès en ligne à des contenus de revues.
Par exemple un bouquet de revues, un catalogue d\'éditeur, une base de sommaires,
une base de données bibliographiques ou le site web de la revue…">Ressources</h2>
		</div>
		<?php
		if ($ressourcesCount) {
			$this->widget(
				'bootstrap.widgets.BootListView',
				[
					'dataProvider' => $search['ressources'],
					'itemView' => '/global/_sphinxRessource',
					'ajaxUpdate' => false,
					'viewData' => ['hash' => $search['hashes']['ressources']],
				]
			);
		} ?>
	</section>
	<?php
}

$editeursCount = isset($search['editeurs']) ? (int) $search['editeurs']->itemCount : 0;
if ($editeursCount || !Yii::app()->user->isGuest) {
	?>
	<section class="well" id="search-editeurs">
		<div>
			<?php
			if (!Yii::app()->user->isGuest) {
				?>
				<div class="create-button">
					<?= CHtml::link("Créer un éditeur…", ['/editeur/create'], ['class' => 'btn']) ?>
				</div>
				<?php
			} ?>
			<h2>Éditeurs</h2>
		</div>
		<?php
		if ($editeursCount) {
			$this->widget(
				'bootstrap.widgets.BootListView',
				[
					'dataProvider' => $search['editeurs'],
					'itemView' => '/global/_sphinxEditeur',
					'ajaxUpdate' => false,
					'viewData' => ['hash' => $search['hashes']['editeurs']],
				]
			);
		} ?>
	</section>
	<?php
}
?>
</div>
