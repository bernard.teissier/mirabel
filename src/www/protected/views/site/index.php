<?php

/** @var Controller $this */
/** @var ?array $intvByCategory */
assert($this instanceof Controller);

?>

<div id="page_accueil">
	<div id="accueil-bandeau">
		<h1><img id="logo_grand" alt="Mir@bel : Le site web qui facilite l'accès aux revues - (Re)cueillir les savoirs" src="/images/logotype-tagline.png" /></h1>
	</div>
	<div id="accueil-colonnes">
		<div id="accueil-gauche">
			<div>
			<?php Cms::printBlock('accueil-intro'); ?>
			<?php $this->renderPartial('_categories'); ?>
			<?php Cms::printBlock('accueil-principal'); ?>
			</div>
		</div>
		<div id="accueil-droite">
			<div id="presentation-video">
				<?php Cms::printBlock('accueil-video'); ?>
			</div>
			<?php Cms::printBlock('accueil-actu'); ?>
		</div>
	</div>
</div>

<div style="clear: both"></div>
<?php
if (!Yii::app()->user->isGuest) {
	Cms::printBlock('accueil-connecté');
	if (isset($intvByCategory)) {
		$this->renderPartial(
			'/utilisateur/_blocSuivi',
			['intvByCategory' => $intvByCategory, 'partenaireId' => (int) Yii::app()->user->partenaireId]
		);
	}
}
