<?php

/** @var Controller $this */
/** @var Login $model */

use models\forms\Login;

assert($this instanceof Controller);

$this->pageTitle = Yii::app()->name . ' - Connexion';
$this->breadcrumbs = [
	'Connexion',
];
?>

<h1>Connexion</h1>

<?php
if (\Config::read('maintenance.authentification.inactive')) {
	?>
	<div class="alert alert-error" role="alert">
		Site en maintenance : la connexion est restreinte aux administrateurs.
	</div>
	<?php
}
?>

<p>
	Identifiez-vous en utilisant votre compte personnel :
</p>

<div class="form">
<?php
/** @var BootActiveForm $form */
$form = $this->beginWidget(
	'BootActiveForm',
	[
		'id' => 'login-form',
		'type' => 'horizontal',
		'enableClientValidation' => false,
	]
);
?>

	<?php
	echo $form->errorSummary($model);
	echo $form->textFieldRow($model, 'username');
	echo $form->passwordFieldRow(
		$model,
		'password',
		['hint' => ""]
	);
	?>

	<div class="form-actions">
		<?php
		echo CHtml::htmlButton('<i class="icon-ok"></i> Se connecter', ['class'=>'btn', 'type'=>'submit']);
		?>

		<div id="link-login-recover">
			Identifiants perdus ?
			<?php
			echo CHtml::link('Demander un nouveau mot de passe.', ['/utilisateur/resetPassword']);
			?>
		</div>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
