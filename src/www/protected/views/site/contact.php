<?php

/** @var Controller $this */
/** @var ContactForm $model */
/** @var bool $sent */
assert($this instanceof Controller);

if ($sent) {
	$this->pageTitle = Yii::app()->name . " - Merci de votre message";
	$this->breadcrumbs = [
		'Contact' => ['/site/contact'],
		"Merci de votre message",
	];
} else {
	$this->pageTitle = Yii::app()->name . ' - Contact';
	$this->breadcrumbs = ['Contact'];
}
?>

<h1>Nous contacter</h1>

<?php
Cms::printBlock('contact');
?>

<div class="form">

	<?php
	$form = $this->beginWidget(
	'BootActiveForm',
	[
		'id' => 'contact-form',
		'type' => 'horizontal',
		'enableClientValidation' => false,
		'htmlOptions' => ["autocomplete" => "off"],
	]
	);
	/** @var BootActiveForm $form */
	?>

	<?php
	echo $form->errorSummary($model);

	echo $form->textFieldRow($model, 'name', ['class' => 'span4']);
	echo CHtml::tag('div', ['class' => "contact-outside"], $form->textFieldRow($model, 'email', ['tabindex' => 10, 'autocomplete' => 'off'])); // fake
	echo $form->textFieldRow($model, 'courriel', ['class' => 'span4', 'type' => 'email']);
	echo CHtml::tag('div', ['class' => "contact-outside"], $form->textFieldRow($model, 'subject', ['tabindex' => 10, 'autocomplete' => 'off'])); // fake
	echo $form->textFieldRow($model, 'sujet', ['class' => 'span6', 'maxlength' => 128]);
	echo $form->textAreaRow($model, 'body', ['rows' => 6, 'class' => 'span6']);

	Yii::app()->clientScript->registerScript(
		'fakeSubject',
		'document.querySelector("#ContactForm_subject").setAttribute("value", "' . ContactForm::FAKE_DATA . '");'
	);
	?>

	<div class="group-control">
		<label class="control-label required"><?= $model->getAttributeLabel('verifyCode') ?></label>
		<div class="controls">
			<div>Indiquez ci-dessous le résultat du calcul <code><?= $model->getCaptchaText() ?></code></div>
			<?= CHtml::activetextField($model, 'verifyCode', ['class' => 'span3']) ?>
		</div>
		<?= $form->hiddenField($model, 'verifyCodeEnc') ?>
	</div>

	<div class="form-actions">
		<noscript>
		<p class="alert alert-danger">Ce formulaire a besoin de JavaScript. Cette contrainte est hélas nécessaire pour luttre contre le spam.</p>
		</noscript>
		<?php
		$this->widget(
			'bootstrap.widgets.BootButton',
			[
				'buttonType' => 'submit',
				'type' => 'primary',
				'label' => 'Envoyer',
			]
		);
		?>
	</div>

	<?php
	$this->endWidget();
	?>

</div><!-- form -->
