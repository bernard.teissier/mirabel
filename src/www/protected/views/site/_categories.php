<?php

/** @var Controller $this */
assert($this instanceof Controller);

if (isset(Yii::app()->params['displayThemesOnHomePage']) && Yii::app()->params['displayThemesOnHomePage'] === false) {
	return;
}

$discipline = Categorie::model()->findByAttributes(['categorie' => 'Discipline', 'profondeur' => 1]);
$themes = Categorie::model()->with(['categorieStats', 'children'])->findAllByAttributes(['parentId' => $discipline->id]);
?>
<section class="thematique">
	<h2>Thématiques des revues de Mir@bel</h2>
	<ul>
	<?php foreach ($themes as $theme) { ?>
		<li>
			<?= CHtml::link(
	CHtml::encode($theme->categorie),
	$theme->getSelfUrl(),
	[
		'title' => ($theme->categorieStats ? $theme->categorieStats->numRevuesIndRec : 0) . " revues.\n"
			. "Sous-thématiques :\n- "
			. join("\n- ", array_map(function ($x) {
				return $x->categorie;
			}, $theme->children)),
	]
) ?>
		</li>
	<?php } ?>
	</ul>
</section>
