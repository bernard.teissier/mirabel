
$("#SearchTitre_categorieComplete").autocomplete({
	"minLength": 2,
	"select": function(event, ui) {
		var name = ui.item.label;
		addCategory(ui.item.id, name, ui.item.profondeur);
		$("#SearchTitre_categorie").val(ui.item.id);
		$("#SearchTitre_categorieComplete").val('');
		return true;
	},
	'close': function(event, ui) {
		$('#SearchTitre_categorieComplete').val('');
	},
	"source": function(request, response) {
		$.ajax({
			url: "/categorie/complete",
			data: {"term":request.term, role: $("#SearchTitre_categorieComplete").attr("data-role")},
			success: function(data) { response(data); }
		});
	}
});

// event handler for removing categories
$('#categorie-liste').on('click', 'span.categorie', function(event) {
	if (event.target.tagName !== 'SPAN') {
		return true;
	}
	$(this).remove();
});
// event handler for toggling recursivity
$('#categorie-liste').on('click', 'i.categorie-recursive', function(event) {
	event.stopPropagation();
	var $this = $(this);
	$this.toggleClass("disabled");
	var cid = $this.siblings("input").first().val();
	if ($("input", $this).length > 0) {
		$("input", $this).remove();
	} else {
		$this.append('<input type="hidden" name="SearchTitre[cNonRec][]" value="' + cid + '" />');
	}
	return false;
});

function addCategory(catId, catName, level) {
	var recursive = '';
	if (level !== '3') {
		recursive = ' <i class="categorie-recursive" title="inclure les sous-thèmes">R</i>';
	}
	$("#categorie-liste").append('<span class="categorie">' + htmlEscape(catName) + '<input name="SearchTitre[categorie][]" type="hidden" value="' + catId + '" />' + recursive + '</span> ');
	$("#categories-combine").attr("class", ($("#categorie-liste span").length > 1 ? "" : "hidden"));
}
