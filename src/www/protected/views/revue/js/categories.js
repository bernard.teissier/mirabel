
jQuery(function($) {
	var revueId = $("#revueId").val();

	$("#revue-categories").on("submit", function(e) {
		var toRemove = $("#rc-modifications > .to-remove").length;
		if (toRemove > 0) {
			return confirm("Vous allez retirer " + toRemove + " thème(s) et en ajouter " + $("#rc-modifications > .to-add").length + ".\nConfirmez-vous cette opération ?");
		}
		return true;
	});

	$("#categorierevue-grid").on("click", "a.delete", function(e) {
		e.preventDefault();
		var categorie = $(this).data("categorie");
		console.log(categorie);
		$(this).after('<div style="position:absolute; width: 30ex; margin-left: -32ex; background-color: #f5f0f0; padding: 4px 6px;" class="tmp-to-remove">La suppression a été ajoutée aux opérations prévues.<br />Pour l\'appliquer, utiliser le bouton <b>Enregistrer ces modifications</b> ci-dessous.');
		window.setTimeout(function(){ $('.tmp-to-remove').remove(); }, 5000);
		removeCategory(categorie.id, categorie.categorie);
		return false;
	});

	$("#rc-modifications").on("click", ".action-drop", function(e) {
		$(this).closest('li').remove();
	});

	var localeCompare = new Intl.Collator("fr").compare;
	$.jstree.defaults.sort = function(a, b) {
		localeCompare(this.get_text(a), this.get_text(b));
	};

	$("#categorie-tree").on("select_node.jstree", function(e, data) {
		// data: node, selected, event
		console.log(data.node);
		if (data.node.parent !== '#') {
			var name = data.node.text;
			if (data.node.type !== "default") {
				name = "[" + data.node.type + "] " + name;
			}
			addCategory(data.node.id, name);
		}
		return false;
		/*
		$("#CategorieRevue_categorieId").val(data.node.id);
		$("#CategorieRevue_categorieIdComplete").val(data.node.text);
		$("#categorie-description").html(data.node.data.description);
		*/
	});

	$('#CategorieRevue_categorieIdComplete').autocomplete({
		'minLength': 2,
		'focus': function(event, ui) {
			if ("description" in ui.item) {
				$("#categorie-description").html(htmlEscape(ui.item.description).replace(/\n+/g, '<br />'));
			}
			if ("parent" in ui.item && ui.item.parent !== "") {
				$("#categorie-fullname").html(htmlEscape(ui.item.parent) + " / <strong>" + htmlEscape(ui.item.value) + "</strong>")
                    .css("background-color", "#fcf8e3");
			}
		},
		'select':function(event, ui) {
			//console.log(ui.item);
			var name = ui.item.label;
			addCategory(ui.item.id, name);
			$('#CategorieRevue_categorieId').val(ui.item.id);
			$('#CategorieRevue_categorieIdComplete').val('');
			return true;
		},
		'close': function(event, ui) {
			$("#categorie-description").text('');
			$('#CategorieRevue_categorieIdComplete').val('');
            $("#categorie-fullname").html('').css("background-color", "");
		},
		'source':function(request, response) {
			$.ajax({
				url: "/categorie/complete",
				data: {'term':request.term, role: $("#CategorieRevue_categorieIdComplete").attr('data-role')},
				success: function(data) { response(data); }
			});
		}
	});

	function removeCategory(catId, catName) {
		$("#rc-modifications > .nop").remove();
		var span = document.createElement("span");
		span.setAttribute('class', 'label');
		span.appendChild(document.createTextNode(catName));

		var drop = document.createElement("button");
		drop.setAttribute('type', 'button');
		drop.setAttribute('class', 'action-drop');
		drop.appendChild(document.createTextNode("Annuler"));

		var input = document.createElement("input");
		input.setAttribute('type', 'hidden');
		input.setAttribute('name', 'remove[]');
		input.setAttribute('value', catId);

		var li = document.createElement("li");
		li.setAttribute('class', 'to-remove');
		li.appendChild(document.createTextNode("Enlever "));
		li.appendChild(span);
		li.appendChild(input);
		li.appendChild(drop);

		$("#rc-modifications").append(li);
	}

	function addCategory(catId, catName) {
		$("#rc-modifications > .nop").remove();
		/* @todo Check unicity */
		var span = document.createElement("span");
		span.setAttribute('class', 'label');
		span.appendChild(document.createTextNode(catName));

		var drop = document.createElement("button");
		drop.setAttribute('type', 'button');
		drop.setAttribute('class', 'action-drop');
		drop.appendChild(document.createTextNode("Annuler"));

		var input = document.createElement("input");
		input.setAttribute('type', 'hidden');
		input.setAttribute('name', 'add[]');
		input.setAttribute('value', catId);

		var li = document.createElement("li");
		li.setAttribute('class', 'to-add');
		li.appendChild(document.createTextNode("Ajouter "));
		li.appendChild(span);
		li.appendChild(input);
		li.appendChild(drop);

		$("#rc-modifications").append(li);
	}
});
