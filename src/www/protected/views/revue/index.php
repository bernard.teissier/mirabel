<?php

/** @var Controller $this */
/** @var CDataProvider $dataProvider */
/** @var string $lettre */
/** @var string $searchHash */
/** @var int $totalRevues */
assert($this instanceof Controller);

$this->pageTitle = 'Revues - ' . $lettre;

$this->breadcrumbs = [
	'Revues',
];

$this->menu = [
	['label' => 'Créer une revue', 'url' => ['/titre/createByIssn', 'revueId' => 0]],
];

if (empty($searchHash)) {
	$searchHash = "";
}

$dataProvider->getData();

$this->renderPartial('_legend');
?>

<h1>Revues — <?= $lettre ?></h1>

<figure class="link-to-map">
	<?=
	CHtml::link(
		CHtml::image('/public/carte_revues_vignette.png', "Carte des $totalRevues revues de Mir@bel"),
		['/revue/carte']
	) ?>
	<figcaption>Carte des <?= $totalRevues ?> revues</figcaption>
</figure>
<div>
	<?= CHtml::link("Recherche avancée de revues", ['/revue/search']) ?>
</div>

<?php Tools::printSubPagination($dataProvider); ?>

<?php
if (Yii::app()->user->isGuest) {
	echo CHtml::link(
		CHtml::image(Yii::app()->baseUrl . "/images/plus.png", "Nouvelle revue"),
		['/titre/createByIssn', 'revueId' => 0],
		[
			'style' => 'float: right; margin-right: 40px;',
			'title' => 'Proposer de créer une nouvellle revue',
		]
	);
}
?>

<div class="list-results">
<?php
foreach ($dataProvider->getData() as $result) {
	echo HtmlHelper::titleLinkItem($result['attrs'], $searchHash);
}
?>
</div>

<?php
Tools::printSubPagination($dataProvider);
