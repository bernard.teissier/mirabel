<?php

/** @var Controller $this */
/** @var Revue $revue */
/** @var Titre[] $titres (titreId => Titre) */
/** @var bool $direct */
/** @var Titre $activeTitle */

assert($this instanceof Controller);

$revueIssnGroups = $revue->getIssnGroups();
$isGuest = Yii::app()->user->isGuest;

$imgUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets'))
	. '/gridview';

$tableContent = [];
$inPossessions = false;
foreach ($titres as $titre) {
	$row = (object) [
		'titre' => "",
		'issn' => "",
		'issne' => "",
		'local' => "",
		'dates' => "",
		'editeurs' => "",
	];

	$row->titre = '<span itemprop="name">' . CHtml::encode($titre->getPrefixedTitle()) . '</span>';
	if ($titre->sigle) {
		$row->titre .= '<span class="titre-sigle">' . CHtml::encode($titre->sigle) . '</span>';
	}

	if ($revueIssnGroups['issnp']) {
		if (isset($revueIssnGroups['issnp'][$titre->id])) {
			foreach ($revueIssnGroups['issnp'][$titre->id] as $issn) {
				/** @var Issn $issn */
				$printableIssn = $issn->issn ? $issn->getHtml(true) : "";
				$row->issn .= CHtml::tag(
					'div',
					[],
					($issn->sudocPpn ? $issn->getSudocLink() . " " : "")
					. $printableIssn
					. (
						$isGuest || $issn->statut != Issn::STATUT_VALIDE || $issn->support !== Issn::SUPPORT_INCONNU ?
						''
						: CHtml::tag('i', ['class' => 'icon-warning-sign', 'title' => "Support indéterminé : modifier l'ISSN pour préciser le support papier ou électronique"], '')
					)
				);
			}
		}
	}

	if ($revueIssnGroups['issne']) {
		if (isset($revueIssnGroups['issne'][$titre->id])) {
			foreach ($revueIssnGroups['issne'][$titre->id] as $issn) {
				/** @var Issn $issn */
				$printableIssn = $issn->issn ? $issn->getHtml(true) : "";
				$row->issne .= CHtml::tag(
					'div',
					[],
					($issn->sudocPpn ? $issn->getSudocLink() . " " : "") . $printableIssn
				);
			}
		}
	}

	// liens
	if ($this->institute && $titre->belongsTo($this->institute->id)) {
		$inPossessions = true;
		$possessionMsg = "Disponible dans votre bibliothèque";
		$url = $this->institute->buildPossessionUrl($titre);
		$divTitle = "Lien direct sur le catalogue de la bibliothèque de {$this->institute->nom}";
		if ($url) {
			$row->local .= " " . CHtml::link($possessionMsg, $url, ['title' => $divTitle]);
		} else {
			$row->local .= " <div>$possessionMsg</div>";
		}
	}


	if (isset($revueIssnGroups['dates'][$titre->id])) {
		$row->dates = CHtml::tag('abbr', ['title' => $revueIssnGroups['dates'][$titre->id]], $titre->getPeriode());
	} else {
		$row->dates = $titre->getPeriode();
	}

	$links = [];
	$editeursActifs = \Editeur::model()->findAllBySql(
		"SELECT e.* FROM Editeur e JOIN Titre_Editeur te ON te.editeurId = e.id AND IFNULL(te.ancien, 0) = 0 WHERE te.titreId = :tid",
		[':tid' => $titre->id]
	);
	foreach ($editeursActifs as $editeur) {
		$links[] = $editeur->getSelfLink(true);
	}
	$row->editeurs = join(', ', $links);
	unset($links);


	$row->actions = ($isGuest ? "" : CHtml::link(
		CHtml::image($imgUrl . '/view.png', 'Détails'),
		['/titre/view', 'id' => $titre->id],
		['title' => 'Détails de ce titre']
	))
		. CHtml::link(
			CHtml::image($imgUrl . '/update.png', 'Modifier'),
			['/titre/update', 'id' => $titre->id],
			['title' => $direct ? 'Modifier' : 'Proposer une modification']
		);

	$tableContent[] = $row;
}
?>
<table class="table table-striped table-bordered table-condensed">
	<thead>
		<tr>
			<th>Titre <?= CHtml::link(
				CHtml::image(Yii::app()->baseUrl . '/images/plus.png', "+titre"),
				['/titre/createByIssn', 'revueId' => $revue->id],
				[
					'title' => ($direct ? 'Ajouter' : 'Proposer') . " un nouveau titre",
					'class' => "pull-right",
				]
			) ?></th>
			<?= $revueIssnGroups['issnp'] ? '<th title="International Standard Serial Number">ISSN</th>' : "" ?>
			<?= $revueIssnGroups['issne'] ? '<th title="International Standard Serial Number - version électronique">ISSN-E</th>' : "" ?>
			<th>Années</th>
			<th>Dernier éditeur</th>
			<?php
			if ($inPossessions) {
				$iname = Yii::app()->user->getInstituteName();
				echo CHtml::tag('th', [], $iname ?: "Localisation");
			}
			?>
			<th><em>Action</em></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($tableContent as $row) { ?>
		<tr itemscope itemtype="http://schema.org/Periodical">
			<td><?= $row->titre ?></td>
			<?php
			if ($revueIssnGroups['issnp']) {
				echo CHtml::tag('td', ['class' => 'issn'], $row->issn);
			}
			if ($revueIssnGroups['issne']) {
				echo CHtml::tag('td', ['class' => 'issn'], $row->issne);
			}
			?>
			<td><?= $row->dates ?></td>
			<td><?= $row->editeurs ?></td>
			<?php if ($inPossessions) {
				echo CHtml::tag('td', [], $row->local);
			} ?>
			<td><?= $row->actions ?></td>
		</tr>
		<?php
		}
		?>
	</tbody>
</table>
