<?php

/** @var Controller $this */
/** @var Revue $revue */
assert($this instanceof Controller);

$tableMain = Wikidata::getRevueWdInfo((int) $revue->id);
if (!$tableMain) {
	return;
}
?>

<h2>Wikidata</h2>

<p>
	Ce bloc vous permet de repérer d'éventuelles données à ajouter ou corriger dans Mir@bel.
	Il affiche des données en provenance de la base de données libre et collaborative Wikidata,
	dont nous n'assurons ni la validité ni la mise à jour. C'est une simple aide à la veille.
</p>

<?php
$tableComp = \models\wikidata\Compare::getRevueComparison($revue->id);
if (empty($tableComp)) {
	echo "<p>Aucune donnée à comparer avec Wikidata.</p>";
} else {
	?>
<h3>Comparaison Wikidata / Mir@bel</h3>

<table class="table table-striped table-bordered table-condensed">
	<thead>
		<tr>
			<th>Lien</th>
			<th>Page Wikidata</th>
			<th>Valeur Wikidata</th>
			<th>Url Wikidata</th>
			<th>Url Mirabel</th>
			<th>Comparaison</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($tableComp as $row) {
			printf('<tr class="%s">', $row['compLevel']);
			echo "<td><b>" . CHtml::encode($row['text']) . "</b></td> ";
			echo "<td>" . $row['qidLink'] . "</td>"; // raw HTML
			echo "<td>" . CHtml::encode($row['wdVal']) . "</td>";
			echo "<td>" . CHtml::link(CHtml::encode($row['wdUrl']), $row['wdUrl']) . "</td>";
			echo "<td>" . CHtml::link(CHtml::encode($row['mUrl']), $row['mUrl']) . "</td>";
			echo "<td>" . $row['compMsg'] . "</td>";
			echo "</tr>\n";
		} ?>
	</tbody>
</table>
<?php
}
?>

<h3 class="collapse-toggle">Informations Wikidata</h3>
<div class="collapse-target">
	<table class="table table-striped table-bordered table-condensed">
		<tbody>
			<?php
			foreach ($tableMain as $row) {
				echo '<tr> <th>' . CHtml::encode($row['rowhead']) . '</th> ';
				echo '<td>' . $row['qidLink'] . '</td>'; // raw HTML
				echo "<td>";
				if (!empty($row['url'])) {
					echo CHtml::link(CHtml::encode($row['value']), $row['url']) . ' ';
				} else {
					echo CHtml::encode($row['value']) . ' ';
				}
				echo "</td> </tr>\n";
			}
			?>
		</tbody>
	</table>
</div>
