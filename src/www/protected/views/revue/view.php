<?php

/** @var Controller $this */
/** @var Revue $revue */
/** @var array $titres (titreId => Titre) */
/** @var array $services (Service) */
/** @var array $partenaires (Partenaire) */
/** @var bool $direct */
/** @var SearchNavigation $searchNavigation */
/** @var bool $forceRefresh */
/** @var ?Abonnement $abonnement */
/** @var bool $publicView */

assert($this instanceof RevueController);

// Pour afficher revue/view, il faut interroger 12 tables !

$activeTitle = reset($titres);
/** @var Titre $activeTitle */
$activeTitleDec = new TitreDecorator($activeTitle);

$imgUrl = Yii::app()->getAssetManager()
	->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview';

if (empty($titres)) {
	$currentTitle = "*aucun titre*";
} else {
	$currentTitle = $activeTitle->getFullTitle();
}
$this->pageTitle = $currentTitle;
$this->pageDescription = 'Accès en ligne aux contenus de la revue ' . $currentTitle;

$encodedTitle = CHtml::encode($currentTitle);
$encodedEditors = '';
foreach ($activeTitle->editeurs as $editeur) {
	$encodedEditors .= $editeur->prefixe . $editeur->nom . ($editeur->sigle ? " " . $editeur->sigle : "") . ", ";
}
$encodedEditors = CHtml::encode($encodedEditors);
$encodedPermanentUrl = CHtml::encode($this->createAbsoluteUrl('/revue/view', ['id' => $revue->id]));
$encodedCanonicalUrl = CHtml::encode($this->createAbsoluteUrl('/revue/view', ['id' => $revue->id, 'nom' => Norm::urlParam($titres[0]->getFullTitle())]));
$this->appendToHtmlHead(
	<<<EOL
	<link rel="canonical" href="$encodedCanonicalUrl" />

	<meta name="description" content="Accès en ligne aux contenus de la revue $encodedTitle" lang="fr" />
	<meta name="keywords" content="revue, accès en ligne, texte intégral, sommaire, périodique, $encodedEditors $encodedTitle" lang="fr" />
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/" />
	<meta name="dcterms.title" content="Mirabel : revue $encodedTitle" />
	<meta name="dcterms.subject" content="revue, accès en ligne, texte intégral, sommaire, périodique, $encodedEditors $encodedTitle" />
	<meta name="dcterms.language" content="fr" />
	<meta name="dcterms.creator" content="Mirabel" />
	<meta name="dcterms.publisher" content="Sciences Po Lyon" />

	<meta property="og:title" content="Mir@bel - $encodedTitle" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="$encodedPermanentUrl" />

	<meta name="twitter:card" content="summary" />
	<meta name="twitter:site" content="@mirabel_revues" />
	<meta name="twitter:title" content="Mir@bel - $encodedTitle" />
	<meta name="twitter:description" content="Accès en ligne aux contenus de la revue $encodedTitle" />
	EOL
);
$encodedLogoUrl = CHtml::encode(
	CHtml::encode($activeTitleDec->hasFront()
		? $activeTitleDec->getFrontUrl(true)
		: Yii::app()->getBaseUrl(true) . '/images/logo-mirabel-carre.png')
);
$this->appendToHtmlHead(
	<<<EOL
	<meta name="twitter:image" content="$encodedLogoUrl" />
	<meta property="og:image" content="$encodedLogoUrl" />
	EOL
);

$this->breadcrumbs = [
	'Revues' => ['index'],
	$activeTitle->getPrefixedTitle(),
];

if (!Yii::app()->user->isGuest) {
	$this->menu = [
		['label' => 'Liste des revues', 'url' => ['index']],
		['label' => 'Sur cette revue', 'itemOptions' => ['class' => 'nav-header']],
		['label' => 'Vérifier les liens' . (Wikidata::isRevuePresent($revue->id) ? ' et données Wikidata' : ''),
			'url' => ['checkLinks', 'id' => $revue->id],
		],
		['label' => 'Ajouter un titre à cette revue', 'url' => ['/titre/createByIssn', 'revueId' => $revue->id]],
		['label' => 'Ajouter un accès en ligne', 'url' => ['/service/create', 'revueId' => $revue->id]],
		['label' => 'Définir la thématique', 'url' => ['categories', 'id' => $revue->id]], // indexation-catégorisation
		[
			'label' => HtmlHelper::postButton(
				"Supprimer cette revue",
				['delete', 'id' => $revue->id],
				[],
				['class' => "btn btn-link", 'style' => "padding: 0; white-space: nowrap;", 'onclick' => 'return confirm("Êtes-vous certain de vouloir supprimer cette revue ?")']
			),
			'encodeLabel' => false,
			'visible' => $direct && (!$titres or count($titres) < 2),
		],
		['label' => 'Autre revue', 'itemOptions' => ['class' => 'nav-header']],
		['label' => 'Créer une revue', 'url' => ['/titre/createByIssn', 'revueId' => 0]],
		['label' => ''],
		['label' => 'Suivi', 'itemOptions' => ['class' => 'nav-header']],
		['label' => "Voir l'historique", 'url' => ['/intervention/admin', 'q[revueId]' => $revue->id]],
	];
	if (!Yii::app()->user->isMonitoring($revue)) {
		$label = CHtml::form(['/partenaire/suivi', 'id' => Yii::app()->user->partenaireId, 'target' => 'Revue'])
			. CHtml::hiddenField('Suivi[cibleId][]', (string) $revue->id)
			. CHtml::hiddenField('returnUrl', $this->createUrl('view', ['id' => $revue->id]), ['id' => 'dqoshfo'])
			. CHtml::htmlButton("Suivre cette revue", ['type' => 'submit', 'class' => "btn btn-primary btn-small"])
			. CHtml::endForm();
		$this->menu[] = ['encodeLabel' => false, 'label' => $label];
	}
	if (Yii::app()->user->checkAccess('verify', $revue)) {
		$this->menu = array_merge(
			$this->menu,
			[
				['label' => 'Vérification', 'itemOptions' => ['class' => 'nav-header']],
				[
					'encodeLabel' => false,
					'label' => $this->widget(
						'bootstrap.widgets.BootButton',
						[
							'label' => 'Indiquer que la revue a été vérifiée',
							'url' => ['verify', 'id' => $revue->id],
							'type' => 'primary', 'size' => 'small',
						],
						true
					),
					'visible' => $direct,
				],
			]
		);
	}

	if ($activeTitle && Yii::app()->user->checkAccess('possession/update')) {
		$owned = Yii::app()->db->createCommand("SELECT 1 FROM Partenaire_Titre WHERE partenaireId = :pid AND titreId = :tid")
			->queryScalar([':tid' => $activeTitle->id, ':pid' => Yii::app()->user->partenaireId]);
		if ($owned) {
			$addToPoss = "Le titre actif est dans mes possessions";
		} else {
			$addToPoss = CHtml::form(['/partenaire/possession', 'id' => Yii::app()->user->partenaireId])
				. CHtml::hiddenField('Possession[titreId]', (string) $activeTitle->id)
				. CHtml::hiddenField('noAct', "1")
				. CHtml::hiddenField('returnUrl', $this->createUrl('view', ['id' => $revue->id]))
				. CHtml::htmlButton('Ajouter à mes possessions', ['type' => 'submit', 'class' => "btn btn-small"])
				. CHtml::endForm();
		}
		array_push(
			$this->menu,
			['label' => 'Mes possessions', 'itemOptions' => ['class' => 'nav-header']],
			['label' => $addToPoss, 'encodeLabel' => false, 'title' => "Ajouter le dernier titre de cette revue à mes possessions en tant que Partenaire Mir@bel"]
		);
	}
}

/** @var Service $service */
/** @var Partenaire $partenaire */

$this->renderPartial('/global/_interventionLocal', ['model' => $revue]);
?>

<?= $this->renderPartial('/global/_search-navigation', ['searchNavigation' => $searchNavigation, 'currentId' => $revue->id, 'attr' => 'titrecomplet']); ?>

<h1>
	<?= CHtml::encode($activeTitle->getFullTitle()); ?>
	<?php
	if ($activeTitle->liens->containsSource(1)) { // "DOAJ"
		echo CHtml::image('/images/logo_libre_acces.png', "Libre accès / Open Access", ['class' => 'logo-open-access', 'width' => '64', 'height' => '32']);
	}
	?>
</h1>

<?php
$categories = $revue->getThemes();
if ($categories) {
	?>
<section id="revue-categories">
	<table class="detail-view table table-bordered table-condensed">
		<tbody>
			<tr>
				<th title="Thématique attribuée par le réseau Mir@bel à cette revue">Thématique</th>
				<td class="categories-labels">
					<?php
					foreach ($categories as $c) {
						/** @var Categorie $c */
						if ($c->role === "public" || Yii::app()->user->checkAccess('indexation')) {
							$class = ($c->role === "public" ? '' : ' class="candidate"');
							$catTitle = CHtml::link(CHtml::encode($c->categorie), $c->getSelfUrl());
							if ($c->description) {
								echo '<abbr title="' . CHtml::encode($c->description) . '"' . $class . '>' . $catTitle . "</abbr>  ";
							} else {
								echo "<span$class>" . $catTitle . "</span>  ";
							}
						}
					} ?>
				</td>
			</tr>
		</tbody>
	</table>
</section>
<?php
}
?>

<section id="revue-titres">
	<?php
	$this->renderPartial('_titres', ['revue' => $revue, 'titres' => $titres, 'direct' => $direct, 'activeTitle' => $activeTitle]);
	?>
</section>

<section id="revue-titre-actif">
	<?php
	$this->renderPartial(
		'_titre-actif',
		[
			'activeTitle' => $activeTitle,
			'activeTitleDec' => $activeTitleDec,
			'forceRefresh' => $forceRefresh,

		]
	);
	?>
</section>

<section id="revue-acces">
	<?php
	$display = new DisplayServices($services, Yii::app()->user->getInstitute());
	$display->setDisplayAll(empty($publicView) ? false : true);
	?>
	<div class="actions">
		<?php
		$canAdd = true;
		if ($publicView || $display->hasMerged()) {
			if ($publicView) {
				echo CHtml::link(
					"Regrouper les accès",
					['view', 'id' => $revue->id, 'public' => 0],
					[
						'title' => "Fusionner les accès similaires" . (Yii::app()->user->getInstitute() ? "" : " en fonction de mon établissement (abonnements)"),
						'class' => 'btn',
					]
				);
			} else {
				$canAdd = false;
				echo CHtml::link(
					"Afficher le détail",
					['view', 'id' => $revue->id, 'public' => 1],
					['title' => "Afficher chaque accès séparément, en séparant par collection", 'class' => 'btn']
				);
			}
		}
		if ($canAdd) {
			echo CHtml::link(
				CHtml::image(Yii::app()->baseUrl . "/images/plus.png", "Ajout d'un accès"),
				['service/create', 'revueId' => $revue->id],
				['title' => $direct ? 'Nouvel accès' : 'Proposer un nouvel accès']
			);
		}
		?>
	</div>
	<h2>Accès en ligne</h2>
	<?php
	$this->renderPartial(
		'/service/_summaryTable',
		[
			'display' => $display,
			'directAccess' => $direct,
			'partenaireId' => Yii::app()->user->getInstitute(),
			'publicView' => $publicView,
			'publicViewUrl' => ['view', 'id' => $revue->id, 'public' => 1],
		]
	);
	?>
</section>

<?php
if (!Yii::app()->user->isGuest) {
	$collections = $revue->getCollections(true, true);
	if ($collections) {
		?>
		<section id="revue-collections">
			<h2 class="collapse-toggle">Ressources et collections</h2>
			<div class="collapse-target">
				<table class="table table-striped table-bordered table-condensed">
					<thead>
						<th>Ressource — collection</th>
						<?php
						if ($abonnement) {
							echo '<th>Abonné ?</th>';
						}
						?>
					</thead>
					<tbody>
					<?php
					if (!$abonnement) {
						$abonnement = new AbonnementSearch();
						$abonnement->partenaireId = Yii::app()->user->partenaireId;
					}
					foreach ($collections as $row) {
					$nomColl = $row['collection']->nom;
					echo '<tr>'
						. '<td>'
						. CHtml::encode($row['ressourceNom'])
						. ($nomColl ? ' — ' . CHtml::encode($nomColl) : '')
						. '</td>';
					if ($abonnement) {
						$status = ($row['collection']->id
							? $abonnement->readStatus($row['collection'])
							: $abonnement->readStatusById('Ressource', $row['ressourceId']));
						echo "<td>"
							. ($status == Abonnement::ABONNE || $status == Abonnement::MASQUE ? Abonnement::$enumMask[$status] : '')
							. "</td>";
					}
					echo "</tr>\n";
				} ?>
					</tbody>
				</table>
			</div>
		</section>
		<?php
	}
}
?>

<section id="revue-suivi">
	<h2>Suivi</h2>
	<?php
	if ($partenaires) {
		echo '<ul class="objets-suivis">';
		foreach ($partenaires as $partenaire) {
			echo '<li>' . $partenaire->getSelfLink() . ' suit cette revue dans Mir@bel</li>';
		}
		echo "</ul>\n";
	} else {
		$collectionsImp = $revue->getCollectionsImport();
		if ($collectionsImp && $revue->hasRessourceImport()) {
			$ressources = $revue->getRessourcesNamesWithImport();
			echo "<p>Cette revue n'est pas suivie par un partenaire de Mir@bel "
				. "mais les informations d'accès en ligne fournies par "
				. (count($collectionsImp) > 1 ? "les collections " : "la collection ")
				. join(
					', ',
					array_map(
						function ($x) {
							return CHtml::tag('em', [], CHtml::encode($x));
						},
						$collectionsImp
					)
				)
				. " et "
				. (count($ressources) > 1 ? "les ressources " : "la ressource ")
				. join(
					', ',
					array_map(
						function ($x) {
							return CHtml::tag('em', [], CHtml::encode($x));
						},
						$ressources
					)
				)
				. " sont traitées automatiquement chaque jour. ";
		} elseif ($collectionsImp) {
			echo "<p>Cette revue n'est pas suivie par un partenaire de Mir@bel "
				. "mais les informations d'accès en ligne fournies par "
				. (count($collectionsImp) > 1 ? "les collections " : "la collection ")
				. join(
					', ',
					array_map(
						function ($x) {
							return CHtml::tag('em', [], CHtml::encode($x));
						},
						$collectionsImp
					)
				)
				. " sont traitées automatiquement chaque jour. ";
		} elseif ($revue->hasRessourceImport()) {
			$ressources = $revue->getRessourcesNamesWithImport();
			echo "<p>Cette revue n'est pas suivie par un partenaire de Mir@bel "
				. "mais les informations d'accès en ligne fournies par "
				. (count($ressources) > 1 ? "les ressources " : "la ressource ")
				. join(
					', ',
					array_map(
						function ($x) {
							return CHtml::tag('em', [], CHtml::encode($x));
						},
						$ressources
					)
				)
				. " sont traitées automatiquement chaque jour. ";
		} else {
			echo "<p>Cette revue est répertoriée par Mir@bel mais n'est pas encore suivie par un partenaire. "
				. "La mise à jour des informations n'est pas assurée. ";
		}
		echo "Les icônes " . CHtml::image(Yii::app()->baseUrl . "/images/plus.png", "Ajouter")
			. (Yii::app()->user->isGuest && $revue->hasRessourceImport() ? ', <i class="icon-envelope"></i>' : '')
			. " et " . CHtml::image($imgUrl . '/update.png', 'Modifier')
			. " vous permettent de proposer des modifications.</p>";
	}

	$this->renderPartial('/global/_modif-verif', ['target' => $revue]);
	?>
</section>
