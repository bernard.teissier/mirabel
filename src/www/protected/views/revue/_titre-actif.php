<?php
/** @var Titre $activeTitle */
/** @var TitreDecorator $activeTitleDec */

assert($this instanceof Controller);

$attributes = [
	'prefixedTitle',
	'url:url',
	'periodicite',
	[
		'name' => 'langues',
		'value' => \models\lang\Convert::codesToFullNames($activeTitle->langues),
	],
	[
		'name' => 'electronique',
		'type' => 'boolean',
		'visible' => (bool) $activeTitle->electronique,
	],
];

$editeursActifs = \Editeur::model()->findAllBySql(
	"SELECT e.* FROM Editeur e JOIN Titre_Editeur te ON te.editeurId = e.id AND IFNULL(te.ancien, 0) = 0 WHERE te.titreId = :tid",
	[':tid' => $activeTitle->id]
);
if ($editeursActifs) {
	$attributes[] = [
		'label' => count($editeursActifs) > 1 ? "Éditeurs" : "Éditeur",
		'type' => 'raw',
		'value' => join(', ', array_map(function($x) {return $x->getSelfLink(true); }, $editeursActifs)),
	];
}

$editeursAnciens = \Editeur::model()->findAllBySql(
	"SELECT e.* FROM Editeur e JOIN Titre_Editeur te ON te.editeurId = e.id AND te.ancien = 1 WHERE te.titreId = :tid",
	[':tid' => $activeTitle->id]
);
if ($editeursAnciens) {
	$attributes[] = [
		'label' => count($editeursAnciens) > 1 ? "Éditeurs précédents" : "Éditeur précédent",
		'type' => 'raw',
		'value' => join(', ', array_map(function($x) {return $x->getSelfLink(true); }, $editeursAnciens)),
	];
}

if ($activeTitle->liensJson) {
	$attributes[] = [
		'label' => "Politique de publication",
		'type' => 'raw',
		'value' => $activeTitle->getLinksEditorial(),
	];
}
$attributes[] = 'linksOther:raw:Autres liens';
if ($activeTitle->getLiensInternes()->getContent()) {
	$attributes[] = 'liensInternes:raw';
}

?>
<div class="active-title <?= ($activeTitleDec->hasFront() ? "with-front" : "no-front") ?>">
	<div class="title-front">
		<?= $activeTitleDec->displayFront($forceRefresh); ?>
	</div>
	<div class="title-attributes">
		<?php
		Yii::app()->getClientScript()->registerScript(
			'popover',
			<<<EOJS
				$(".popover-toggle").popover();
			EOJS
		);
		$this->widget(
			'ext.bootstrap.widgets.BootDetailView',
			[
				'data' => $activeTitle,
				'attributes' => $attributes,
				'hideEmptyLines' => true,
			]
		);
		?>
	</div>
</div>
