<?php

/** @var Controller $this */
/** @var SearchTitre $model */
/** @var ?SphinxDataProvider $results */
/** @var string $searchHash */
assert($this instanceof Controller);

$titleSuffix = $model->getTitleSuffix();
$this->pageTitle = 'Recherche de revues' . $titleSuffix;

if ($results !== null) {
	Yii::app()->clientScript->registerMetaTag('robots', 'noindex');
}

$this->breadcrumbs = [
	'Revues' => ['index'],
	'Recherche',
];

if (!Yii::app()->user->isGuest) {
	array_push(
		$this->menu,
		[
			'label' => 'Liste',
			'url' => ['/revue/index'],
		],
		[
			'label' => 'Carte',
			'url' => ['/revue/carte'],
		],
		[
			'label' => 'Nouvelle revue',
			'url' => ['/titre/create'],
		],
	);
}
if (isset($results)) {
	echo $this->renderPartial('_legend', null, true);

	$this->sidebarInsert .= $this->renderPartial('_export', ['search' => $model], true)
		. (Yii::app()->user->isGuest
			? ''
			: $this->renderPartial('_exportServices', ['search' => $model], true));

	$this->menu[] = [
		'label' => 'Nouvelle recherche ↓',
		'url' => '#new-search',
	];
}
?>

<h1>Recherche de revues<?php echo $titleSuffix; ?></h1>

<?php if ($results == null) { ?>
	<p>Il est aussi possible de <?= CHtml::link("chercher des éditeurs", ['/editeur/search']) ?>.</p>
<?php } else { ?>
	<div id="search-summary">
		<strong>Critères :</strong> <?= $model->htmlSummary() ?>
	</div>
<?php } ?>

<div id="extended-search-results">
<?php
if (isset($results)) {
	$this->widget(
		'bootstrap.widgets.BootListView',
		[
			'dataProvider' => $results,
			'itemView' => '/global/_sphinxTitre',
			'viewData' => ['hash' => $searchHash],
		]
	);
}
?>
</div>

<h2 id="new-search">Nouvelle recherche</h2>
<?php
$this->renderPartial(
	'_search',
	['model' => $model, 'partenaireId' => $this->institute ? $this->institute->id : null]
);
