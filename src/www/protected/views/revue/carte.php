<?php

/** @var Controller $this */
/** @var array $mapConfig */
/** @var bool $vivantes */
/** @var int $nbRevues */
/** @var string $stamen */
assert($this instanceof Controller);

$this->pageTitle = "Carte des revues";

$this->breadcrumbs = [
	'Revues' => ['index'],
	'Carte',
];

$this->appendToHtmlHead('<link rel="stylesheet" href="/js/leaflet/leaflet.css"');
Yii::app()->clientScript->registerScriptFile("/js/leaflet/leaflet.js");
if ($stamen) {
	Yii::app()->clientScript->registerScriptFile("//maps.stamen.com/js/tile.stamen.js?v1.3.0");
}
Yii::app()->clientScript->registerScriptFile("/js/countries.js");
Yii::app()->clientScript->registerScriptFile('/js/world-map.js');

Yii::app()->clientScript->registerScript(
	'mapinit',
	'drawWorldMap(' . CJavaScript::encode($mapConfig) . ', ' . CJavaScript::encode($stamen) . ');'
);
?>

<h1>Localisation des revues recensées dans Mir@bel</h1>

<figure id="carte-revues" class="carte">
	<div id="world-map" style="height: 600px;"></div>
	<figcaption>Carte des <?= $nbRevues ?> revues<?= $vivantes ? " vivantes" : "" ?>, selon le pays de leurs éditeurs</figcaption>
</figure>

<noscript>
	<p class="alert alert-warning">
		Cette carte ne s'affiche pas si JavaScript est désactivé dans votre navigateur.
	</p>
</noscript>

<p>
	Cliquer sur un pays vous permet d'afficher les détails
	puis de rebondir sur les listes de revues de ce pays.
</p>

<p>
	<?php
	if ($vivantes) {
		echo CHtml::link(
			"Afficher aussi les revues mortes",
			['/revue/carte', 'vivantes' => 0],
			['class' => 'btn btn-primary', 'title' => "Comptabiliser aussi les revues dont la parution a cessé."]
		);
	} else {
		echo CHtml::link(
			"N'afficher que les revues vivantes",
			['/revue/carte', 'vivantes' => 1],
			['class' => 'btn btn-primary', 'title' => "Ne pas comptabiliser les revues dont la parution a cessé."]
		);
	}
	?>
</p>

<p>
	Une revue qui a plusieurs éditeurs dans son historique de titres peut être comptabilisée dans plusieurs pays.
</p>
