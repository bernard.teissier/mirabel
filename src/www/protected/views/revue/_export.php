<?php

/** @var Controller $this */
/** @var SearchTitre $search */
assert($this instanceof Controller);

?>
<div class="well">
	<h4 title="Export au format tableur CSV de la liste des revues recherchées">Exporter les revues</h4>
	<form action="<?php echo $this->createUrl('/revue/export'); ?>" method="get" class="form-horizontal">
		<?php
		foreach ($search->attributes as $k => $value) {
			if ($value) {
				if (is_array($value)) {
					foreach ($value as $subvalue) {
						echo CHtml::hiddenField("q[$k][]", $subvalue);
					}
				} else {
					echo CHtml::hiddenField("q[$k]", $value);
				}
			}
		}
		if (!Yii::app()->user->isGuest) {
			echo CHtml::label(
				CHtml::checkBox('extended') . " Infos de possession",
				'extended',
				["title" => "Le CSV contiendra toutes les informations de possession de ces revues par le partenaire (identifiant local, etc)"]
			);
		}
		?>
		<button type="submit">Enregistrer<br /> les revues</button>
	</form>
</div>
