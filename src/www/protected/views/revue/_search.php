<?php

/** @var Controller $this */
/** @var SearchTitre $model */
/** @var int $partenaireId */
assert($this instanceof Controller);

/** @var BootActiveForm $form */
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'action' => Yii::app()->createUrl('/revue/search'),
		'method' => 'get',
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'hints' => Hint::model()->getMessages('SearchTitre'),
	]
);

echo $form->textFieldRow($model, 'titre', ['class' => 'span8']);
echo $form->textFieldRow($model, 'issn');
$accessTypes = [2 => 'Texte intégral', 3 => 'Résumé', 4 => 'Sommaire', 5 => 'Indexation'];
echo $form->checkBoxListInlineRow($model, 'acces', $accessTypes, ['uncheckValue' => '']);
echo $form->checkBoxRow($model, 'accesLibre', ['uncheckValue' => '']);
if (!Yii::app()->user->isGuest) {
	echo $form->checkBoxRow($model, 'sansAcces', ['uncheckValue' => '', 'class' => 'not-guest']);
}

$this->widget(
	'ext.MultiAutoComplete.MultiAutoComplete',
	[
		'model' => $model,
		'attribute' => 'editeurId',
		'foreignModelName' => 'Editeur',
		'foreignAttribute' => 'fullName',
		'ajaxUrl' => $this->createUrl('/editeur/ajaxComplete'),
		'options' => [
			// min letters typed before we try to complete
			'minLength' => '3',
		],
		'htmlOptions' => ['hint' => $form->getHint($model, 'editeurId')],
	]
);

echo $form->dropDownListRow(
	$model,
	'paysId',
	Tools::sqlToPairs("SELECT p.id, p.nom FROM Pays p JOIN Editeur e ON e.paysId = p.id GROUP BY p.id ORDER BY p.nom"),
	['empty' => '-']
);

$this->widget(
	'ext.MultiAutoComplete.MultiAutoComplete',
	[
		'model' => $model,
		'attribute' => 'ressourceId',
		'foreignModelName' => 'Ressource',
		'foreignAttribute' => 'fullName',
		'ajaxUrl' => $this->createUrl('/ressource/ajaxComplete'),
		'options' => [
			// min letters typed before we try to complete
			'minLength' => '3',
		],
		'htmlOptions' => ['hint' => $form->getHint($model, 'ressourceId')],
	]
);
echo $form->dropDownListRow(
	$model,
	'collectionId',
	CHtml::listData(Collection::model()->findAll(['with' => ['ressource']]), 'id', 'fullName'),
	['empty' => '-']
);
?>

<div class="control-group">
	<label class="control-label" for="SearchTitre_categorie">
		Thématique
	</label>
	<div class="controls">
		<div id="categorie-liste" class="categories-labels"
			title="Cliquer sur un terme pour le retirer de la recherche ; cliquer sur le R pour activer/désactiver la recherche récursive sur les sous-thèmes.">
			<?php
			if ($model->categorie) {
				foreach ($model->getCategories() as $categorie) {
					echo '<span class="categorie">' . CHtml::encode($categorie->categorie)
						. '<input name="SearchTitre[categorie][]" type="hidden" value="'
						. $categorie->id . '" />';
					if ($categorie->profondeur < 3) {
						$nonRec = in_array($categorie->id, $model->cNonRec);
						echo ' <i class="categorie-recursive' . ($nonRec ? " disabled" : "")
							. '" title="inclure les sous-thèmes">R'
							. ($nonRec ? '<input type="hidden" name="SearchTitre[cNonRec][]" value="' . $categorie->id . '" />' : '')
							. '</i>';
					}
					echo '</span> ';
				}
			}
			?>
		</div>
		<div id="categories-combine" class="<?= (count($model->categorie) > 1 ? '' : 'hidden') ?>">
			<label>
				<input name="SearchTitre[categoriesEt]" type="checkbox" value="1" <?= ($model->categoriesEt ? 'checked="checked"' : "") ?>" />
				Revues ayant tous ces thèmes à la fois
			</label>
		</div>
		<input class="span6" id="SearchTitre_categorieComplete" type="text" name="SearchTitre[categorieComplete]"
			data-role="<?= (Yii::app()->user->checkAccess('indexation') ? '' : 'public') ?>" />
		<?php
		$categorieHint = $form->getHint($model, 'categorie');
		if ($categorieHint) {
			?>
			<a href="#" data-title="Thématique" data-content="<?= CHtml::encode($categorieHint) ?>" data-html="true" data-trigger="hover" rel="popover">?</a>
			<?php
		}
		?>
	</div>
</div>
<?php
Yii::app()->clientScript->registerScript('categorie-complete', file_get_contents(__DIR__ . '/js/categories-autocomplete.js'));

if (!Yii::app()->user->isGuest) {
	echo $form->checkBoxRow($model, 'sanscategorie', ['uncheckValue' => '', 'class' => 'not-guest']);
}
?>

<?php
echo $this->renderPartial(
	'/global/_autocompleteLang',
	['model' => $model, 'attribute' => 'langues', 'form' => $form]
);
?>

<?= (new \widgets\DynamicLinkFilter())->run($model->lien) ?>

<?php if (!empty($partenaireId)) { ?>
<fieldset>
	<legend><?= CHtml::encode(Yii::app()->user->getState('institute_name')) ?></legend>
	<?php
	echo $form->checkBoxRow($model, 'owned', ['uncheckValue' => '']);

	$hasAbo = Yii::app()->db->createCommand("SELECT 1 FROM Abonnement WHERE partenaireId = :pid LIMIT 1")
		->queryScalar([':pid' => $partenaireId]);
	if ($hasAbo) {
		echo $form->checkBoxRow($model, 'abonnement', ['uncheckValue' => '']);
	}

	if (!Yii::app()->user->isGuest) {
		echo $form->checkBoxRow($model, 'monitoredByMe', ['uncheckValue' => '', 'class' => 'not-guest']);
	}
	?>
</fieldset>
<?php } ?>

<fieldset>
	<legend>Dans Mir@bel</legend>
	<?php
	echo $form->textFieldRow($model, 'hdateModif');
	echo $form->textFieldRow($model, 'hdateVerif');
	if (!Yii::app()->user->isGuest) {
		echo $form->radioButtonListRow(
			$model,
			'monitored',
			[
				SearchTitre::MONIT_YES => "Seulement les revues suivies",
				SearchTitre::MONIT_NO => "Seulement les revues non suivies",
				SearchTitre::MONIT_IGNORE => "Toutes les revues", ],
			['class' => 'not-guest']
		);
	}
	if (!$model->monitoredByMe && !$model->monitored && $model->suivi) {
		?>
		<div class="control-group ">
			<label class="control-label" for="SearchTitre[suivi]">Suivi par </label>
			<div class="controls">
				<label class="checkbox">
					<input type="checkbox" name="SearchTitre[suivi]" checked value="<?= join(',', $model->suivi) ?>">
					<em><?= CHtml::encode($model->getSuiviText()) ?></em>
				</label>
			</div>
		</div>
		<?php
	}
	?>
</fieldset>

<div class="form-actions">
	<?php
	$this->widget(
		'bootstrap.widgets.BootButton',
		[
			'buttonType' => 'submit',
			'label' => 'Rechercher',
		]
	);
	?>
</div>

<?php $this->endWidget(); ?>
