<?php

/** @var Controller $this */
/** @var SearchTitre $search */
assert($this instanceof Controller);

$model = new Service('filter');
Yii::app()->clientScript->registerScript(
	'export-alerts',
	'function alertOnce() {
		if ($("#Service_lacunaire:checked").length > 0 || $("#Service_selection:checked").length > 0) {
			return;
		}
		if (!sessionStorage.getItem("alertExportOnce")) {
			sessionStorage.setItem("alertExportOnce", 1);
			alert("Attention, par défaut les accès incomplets ne sont pas exportés.");
		}
	}
	$("#service-filter-form").on("submit", alertOnce);
	'
);

?>
<div class="well">
	<h4>Exporter les accès en ligne</h4>
	<p class="collapse-toggle" title="Détail du filtrage">
		Filtrer par :
	</p>
	<div class="collapse-target">
		<?php
		/** @var BootActiveForm $form */
		$form = $this->beginWidget(
			'bootstrap.widgets.BootActiveForm',
			[
				'id' => 'service-filter-form',
				'method' => 'get',
				'enableAjaxValidation' => false,
				'type' => BootActiveForm::TYPE_VERTICAL,
				'action' => $this->createUrl('/service/export'),
			]
		);
		foreach ($search->attributes as $k => $value) {
			if ($value) {
				if (is_array($value)) {
					foreach ($value as $subvalue) {
						echo CHtml::hiddenField("q[$k][]", $subvalue);
					}
				} else {
					echo CHtml::hiddenField("q[$k]", $value);
				}
			}
		}
		$this->renderPartial(
			'/global/_autocompleteField',
			[
				'model' => $model, 'attribute' => 'ressourceId',
				'url' => '/ressource/ajaxComplete', 'form' => $form, 'value' => '',
				'htmlOptions' => ['class' => 'span10'],
			]
		);
		?>
		<div style="clear: left;"></div>
		<?= $form->checkBoxListRow($model, 'type', Service::$enumType, ['labelOptions' => ['label' => 'Type limité à']]) ?>
		<div style="clear: left;"></div>
		<label>Inclure aussi</label>
		<div>
			<?php
			echo '<label>' . $form->checkBox($model, 'lacunaire') . ' lacunaires</label>';
			echo '<label>' . $form->checkBox($model, 'selection') . " sélection d'articles</label>";
			?>
		</div>
		<?php
		echo $form->dropDownListRow($model, 'acces', Service::$enumAcces, ['empty' => 'Tous', 'class' => 'span10']);
		echo $form->textFieldRow(
			$model,
			'hdateModif',
			['size' => 10, 'class' => 'span10', 'labelOptions' => ['label' => 'Modifié après'], 'placeholder' => "YYYY-MM-DD"]
		);
		?>
	</div>
	<button type="submit" name="format" value="csv">Exporter (CSV)</button>
	<button type="submit" name="format" value="kbart">Exporter (KBART)</button>
	<?php
	$this->endWidget();
	?>
</div>
