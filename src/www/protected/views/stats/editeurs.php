<?php

/** @var Controller $this */
/** @var StatsController $this */
/** @var object $liens */

use models\stats\Editeurs;

assert($this instanceof \Controller);

$this->pageTitle = "Indicateurs des éditeurs";
$this->breadcrumbs = [
	"Indicateurs" => ['/stats'],
	$this->pageTitle,
];

Yii::app()->clientScript->registerScriptFile("/js/tsorter.min.js");
?>

<div<?= $this->localhostConnection ? ' class="localhost-connection"' : '' ?>>

	<h1><?= CHtml::encode($this->pageTitle) ?> au <?= date('d/m/Y') ?></h1>

<section>
	<h2>Principaux pays</h2>

	<?= Editeurs::countByCountry()
		->addClass('exportable sortable')
		->setHtmlAttributes(['id' => 'editeurs-par-pays'])
		->toHtml(false) ?>
	<?php
	Yii::app()->clientScript->registerScript('sortable', 'tsorter.create("editeurs-par-pays");');
	?>
</section>

<section>
	<h2>Nombre d'IdRef</h2>

	<?php
	$countAll = Editeurs::count();
	$countIdref = Editeurs::countHavingIdref();
	?>
	<p>
		Parmi les <?= $countAll ?> éditeurs de Mir@bel,
		<?= $countIdref ?> (<?= round(100*$countIdref/$countAll) ?> %) ont un IdRef.
	</p>
</section>

<section>
	<h2>Nombre de revues</h2>

	<?= Editeurs::countByRevues()->addClass('exportable')->toHtml(false) ?>

	<?= Editeurs::getEditeursMaxRevues()->addClass('exportable')->toHtml(false) ?>
</section>


	<?php
	$this->renderPartial('_liens-editeurs', ['liens' => $liens]);
	?>
</div>
