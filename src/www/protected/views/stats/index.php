<?php

use models\forms\StatsForm;
use models\stats\Contenu;
use models\stats\Activite;

/** @var StatsController $this */
/** @var StatsForm $userData */
/** @var Contenu $stats */
/** @var Activite $statsActivite */
/** @var int $countInterventions */
/** @var object $liens */

assert($this instanceof Controller);

$this->pageTitle = Yii::app()->name . ' - Indicateurs';
$this->breadcrumbs = [
	'Indicateurs',
];
?>

<?php if (!Yii::app()->user->isGuest) { ?>
<div class="lateral-menu-small">
	<ul class="nav nav-tabs nav-stacked">
		<li class="active"><?= CHtml::link("Indicateurs") ?></li>
		<li><?= CHtml::link("… d'activité", '#title-activity') ?></li>
		<li><?= CHtml::link("… d'éditeurs", ['/stats/editeurs']) ?></li>
		<li><?= CHtml::link("… de mon partenariat", ['activite', 'partenaireId' => Yii::app()->user->partenaireId]) ?></li>
		<?php if (Yii::app()->user->checkAccess('stats/suivi')) { ?>
		<li><?= CHtml::link("… de suivi", ['/stats/suivi']) ?></li>
		<?php } ?>
	</ul>
</div>
<?php } ?>

<div<?= $this->localhostConnection ? ' class="localhost-connection"' : '' ?>

<h1>Indicateurs de contenu et d'activité</h1>

<h2 style="clear:right">Contenu actuel <em>(<?php echo date('Y-m-d'); ?>)</em></h2>

<div class="span12" style="margin-bottom:3ex">
<div class="row">
	<div class="span4"><?= $stats->getMainInfoRevues()->addClass('exportable')->toHtml() ?></div>
	<div class="span4"><?= $stats->getMainInfoRessources()->addClass('exportable')->toHtml() ?></div>
	<div class="span4"><?= $stats->getMainInfo()->addClass('exportable')->toHtml() ?></div>
</div>
</div>

<h3>Ressources par type</h3>
<?php
echo HtmlTable::build($stats->getRessourcesParType(), null)->addClass('exportable')->toHtml();
?>

<h3>Ressources par nombre de revues/titres/accès</h3>
<?php
echo HtmlHelper::table($stats->getRessourcesParAcces(20), 'exportable');
?>

<h3>Ressources avec import automatique</h3>
<?php
echo HtmlHelper::table($stats->getRessourcesImportees(), 'exportable');
?>

<h3>Revues par accès</h3>
<?php
echo HtmlTable::buildFromAssoc($stats->getRevuesParAcces())->addClass('exportable')->toHtml();
?>

<h3>Accès par type</h3>
<table class="table table-striped table-bordered table-condensed exportable">
	<thead>
		<tr>
			<th>Type d'accès</th>
			<th>Libre</th>
			<th>Restreint</th>
			<th>Rapport libre/restreint</th>
			<th>Total des accès</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$acces = $stats->getAccesParType();
		foreach ($acces as $a) {
			?>
		<tr>
			<td><?php echo CHtml::encode($a['type']); ?></td>
			<td><?php echo CHtml::encode($a['Libre']); ?></td>
			<td><?php echo CHtml::encode($a['Restreint']); ?></td>
			<td><?php echo CHtml::encode($a['Taux']); ?> %</td>
			<td><?php echo CHtml::encode($a['Total']); ?></td>
		</tr>
		<?php
		}
		?>
	</tbody>
</table>

<h3>Éditeurs des revues</h3>
<?php
echo $stats->getEditeursNb()->addClass('exportable')->toHtml();
?>

<h3>Langues des revues</h3>
<p>
	Mir@bel a actuellement <?= $stats->countLangGroups() ?> déclarations distinctes
	pour les langues de revues (par exemple "français" ou "français, anglais, allemand, slave", l'ordre étant significatif)
	et <strong><?= $stats->countLangGroupsUnordered() ?> ensembles de langues</strong> (en ignorant leur ordre).
	Plutôt que d'afficher une liste exhaustive, on se restreint à deux tableaux de synthèse.
	Voir les <?= CHtml::link("indicateurs d'éditeurs", ['/stats/editeurs']) ?> pour des décomptes de revues par pays.
</p>
<p>
	Ces tableaux analysent la langue du titre le plus récent d'une revue —
	contrairement aux pages de recherche dans Mir@bel qui cherchent les langues de
	tous les titres d'une revue.
</p>
<?= $stats->getLangNb()->addClass('exportable')->toHtml() ?>
<?= $stats->getLangInfo()->addClass('exportable')->toHtml() ?>

<?php
$this->renderPartial('_liens-titres', ['liens' => $liens]);
?>

<h2 id="title-activity">Activité</h2>

<div>
	<?php
	$form = $this->beginWidget(
	'BootActiveForm',
	[
		'id' => 'interventions-count-form',
		'type' => BootActiveForm::TYPE_INLINE,
		'enableClientValidation' => true,
		'action' => $this->createUrl('index') . '#title-activity',
		'method' => 'GET',
	]
);
	/** @var BootActiveForm $form */
	echo $form->errorSummary($userData);
	echo "Période du "
		. $form->textFieldRow($userData, 'startDate', ['class' => 'input-small'])
		. " au "
		. $form->textFieldRow($userData, 'endDate', ['class' => 'input-small']);
	?>
	<button type="submit" class="btn">OK</button>
	<?php $this->endWidget(); ?>
</div>

<h3>Interventions validées<?= $statsActivite->getIntervalMessage() ?></h3>
<p>
	Décompte des interventions validées pendant cette période, qu'elles l'aient été immédiatement
	(par exemple, lors de l'import) ou postérieurement à la proposition.
</p>
<?php
echo $statsActivite->countInterventions()->addClass('exportable')->toHtml();
?>

<h3>Nombre d'interventions créées <?= $statsActivite->getIntervalMessage() ?></h3>
<p>
	Les interventions par import ne figurent pas dans ce tableau.
</p>
<?php
echo $statsActivite->getProp()->addClass('exportable')->toHtml();
?>

<h3>Détail par statut des interventions créées <?= $statsActivite->getIntervalMessage() ?></h3>
<p>
	La répartition se fait selon le statut <em>actuel</em> de ces interventions.
	La sélection d'une période ne détermine que le choix des interventions qui ont surgi dans cet intervalle.
	Les interventions par import ne figurent pas dans ce tableau.
</p>
<?php
echo $statsActivite->getPropParStatut()->addClass('exportable')->toHtml();
?>

<h3>Détail des validations par type d'action <?= $statsActivite->getIntervalMessage() ?></h3>
<p>
	La sélection d'une période restreint aux interventions qui ont été validées dans cet intervalle.
	Les interventions par import ne figurent pas dans ce tableau.
</p>
<?= $statsActivite->getValParType()->addClass('exportable')->toHtml() ?>


<hr />
<?php echo CHtml::link('Historique des rapports statistiques', ['history']); ?>
<?php
if (!Yii::app()->user->isGuest) {
	echo " — "
		. CHtml::link("Rapport d'activité de mon partenariat", ['activite', 'partenaireId' => Yii::app()->user->partenaireId]);
}

Yii::import('application.controllers.StatsaccesController');
if (file_exists(StatsaccesController::getStatsPath())) {
	echo '<p id="statsacces">'
		. CHtml::link("Statistiques de fréquentation du site Mir@bel", ['/statsacces/index'])
		. "</p>";
}
?>

</div>
