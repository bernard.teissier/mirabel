<?php

/** @var StatsController $this */
/** @var CSqlDataProvider $provider */
/** @var array $possessions [partenaireId => #] */
/** @var array $abonnements [partenaireId => #] */
/** @var array $masques [partenaireId => #] */
assert($this instanceof Controller);

$this->pageTitle = Yii::app()->name . " - État des suivis";
$this->breadcrumbs = [
	"Indicateurs" => ['/stats/index'],
	"État des suivis",
];
?>

<div<?= $this->localhostConnection ? ' class="localhost-connection"' : '' ?>

	<h1>État des suivis au <?= date('d/m/Y') ?></h1>

	<?php
	$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'suivis-grid',
		'dataProvider' => $provider,
		'filter' => null,
		'columns' => [
			[
				'name' => 'nom',
				'header' => 'Partenaire',
				'type' => 'raw',
				'value' => function ($data) {
					return CHtml::link(
						CHtml::encode($data['nom']),
						['/partenaire/view', 'id' => $data['id']],
						['title' => 'statut ' . $data['statut']]
					);
				},
				'cssClassExpression' => function ($rank, array $data) {
					return 'partenaire statut-' . $data['statut'];
				},
			],
			[
				'name' => 'type',
				'header' => 'Type',
				'type' => 'raw',
				'value' => function ($data) {
					$statut = ($data['statut'] === 'actif' ? '' : $data['statut']);
					if ($data['type'] === 'normal') {
						return $statut;
					}
					return $statut . " " . Partenaire::$enumType[$data['type']];
				},
			],
			'derConnexion:date:Der Connexion',
			'login:text:Login',
			'verifRecente:date:Vérif. + récente',
			'verifAncienne:date:Vérif. + ancienne',
			'verifNon:text:Revues non vérifiées',
			'revuesSuivies:text:Revues suivies',
			'ressourcesSuivies:text:Ressources suivies',
			[
				'header' => "#possessions",
				'type' => 'text',
				'value' => function (array $data) use ($possessions) {
					return isset($possessions[$data['id']]) ? (int) $possessions[$data['id']] : '';
				},
			],
			[
				'header' => "#abonnements",
				'type' => 'text',
				'value' => function (array $data) use ($abonnements) {
					return isset($abonnements[$data['id']]) ? (int) $abonnements[$data['id']] : '';
				},
			],
			[
				'header' => "#ressources masquées",
				'type' => 'text',
				'value' => function (array $data) use ($masques) {
					return isset($masques[$data['id']]) ? (int) $masques[$data['id']] : '';
				},
			],
		],
		'ajaxUpdate' => false,
		'itemsCssClass' => 'exportable',
	]
);
	?>
</div>
