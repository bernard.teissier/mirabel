<?php

/** @var Controller $this */
/** @var object $liens */

assert($this instanceof Controller);
?>

<h2>Liens internes et externes</h2>

<p>
	Cette section concerne les liens libres dans les éditeurs.
	Les données sont recaclulées toutes les heures.
</p>

<h3>Noms des liens</h3>
<p>
	Tous les noms apparaissant au moins 2 fois parmi les <em>autres liens</em> d'éditeurs.
	La casse est ignorée.
</p>
<?php
echo HtmlTable::build(
	array_map(
		function ($x) {
			$x['num'] = CHtml::link($x['num'], ['stats/liens', 'name' => $x['name']]);
			return $x;
		},
		$liens->names
	),
	["Nom", "Nombre d'éditeurs"]
)->addClass('exportable')->toHtml();
?>

<h3>Domaines des URL</h3>
<p>
	Tous les domaines apparaissant au moins 2 fois.
	Les sous-domaines <code>www.</code> sont ignorés.
</p>
<?php
echo HtmlTable::build(
	array_map(
		function (array $x) {
			$x['num'] = CHtml::link($x['num'], ['stats/liens', 'domain' => $x['domain']]);
			$source = Sourcelien::identifyUrl($x['domain']);
			if ($source) {
				$x['domain'] .= " " . $source->getLogo();
				$x['identifié'] = "oui";
			} else {
				$x['identifié'] = "non";
			}
			return $x;
		},
		$liens->domains
	),
	["Domaine", "Nombre d'éditeurs", "Identifié"]
)->addClass('exportable')->toHtml();
?>
