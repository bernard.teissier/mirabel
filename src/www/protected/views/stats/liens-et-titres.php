<?php

/** @var Controller $this */
/** @var string $titrePage */
/** @var array $liensTitre */
/** @var array $liensEditeur */
assert($this instanceof Controller);

$this->pageTitle = Yii::app()->name . ' - Sélection de liens';

$this->breadcrumbs = Yii::app()->user->isGuest ? ['Indicateurs' => '#'] : ['Indicateurs' => ['index']];
$this->breadcrumbs[] = 'Sélection de liens';
?>

<h1>Sélection de liens</h1>

<h2><?= sprintf($titrePage, count($liensTitre) + count($liensEditeur)) ?></h2>

<h3><?= count($liensTitre) ?> dans les revues</h3>
<?php
echo HtmlTable::build(
	array_map(
		function ($x) { // Each row is an array with keys revueId, name, domain, url
			return [
				CHtml::link(CHtml::encode($x['titre']), ['revue/view', 'id' => $x['revueId']]),
				CHtml::encode($x['name']),
				CHtml::link(CHtml::encode($x['url']), $x['url']),
			];
		},
		$liensTitre
	),
	["Revue", "Nom du lien", "URL"]
)->addClass('exportable')->toHtml();
?>

<h3><?= count($liensEditeur) ?> dans les éditeurs</h3>
<?php
echo HtmlTable::build(
	array_map(
		function ($x) { // Each row is an array with keys editeurId, name, domain, url
			return [
				CHtml::link(CHtml::encode($x['nom']), ['editeur/view', 'id' => $x['editeurId']]),
				CHtml::encode($x['name']),
				CHtml::link(CHtml::encode($x['url']), $x['url']),
			];
		},
		$liensEditeur
	),
	["Éditeur", "Nom du lien", "URL"]
)->addClass('exportable')->toHtml();
