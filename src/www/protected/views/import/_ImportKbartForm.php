<?php
/** @var ImportKbartForm $model */
?>
<h1>Import KBART d'accès en ligne</h1>

<ul class="alert alert-warning">
	<li>
		Le format KBART (I ou II) est un fichier texte, encodé en UTF-8, et dont les colonnes sont séparées par des tabulations.
	</li>
	<li>
		Les identifiants, imprimés ou numériques, qui ne sont pas de type ISSN seront ignorés.
	</li>
	<li>
		Les champs <code><?php echo join('</code>, <code>', $model->ignoredFields); ?></code> seront ignorés.
	</li>
	<li>
		Seules les lignes de revues sont traitées ; les lignes où <code>publication_type</code> existe et n'est pas à "serial" sont ignorées.
	</li>
</ul>

<div class="form">
	<?php
	/*
	<li>
		Mir@bel permet une plus grande finesse sur la couverture :
		si "coverage_depth" est à "selected articles", alors le contenu sera déclaré en teste intégral ;
		pour d'autres valeurs de "coverage_depth" (fulltext/abstract),
		les cases à cocher globales "Lacunaire" et "Sélection d'articles" seront utilisées.
	</li>
	*/
	/** @var BootActiveForm $form */
	$form = $this->beginWidget(
		'BootActiveForm',
		[
			'id' => 'import-kbart-form',
			'type' => 'horizontal',
			'enableClientValidation' => false,
			'htmlOptions' => ['enctype' => 'multipart/form-data'],
			'hints' => Hint::model()->getMessages('ImportKbartForm'),
		]
	);
	?>

	<?php
	echo $form->errorSummary($model);

	$this->renderPartial(
		'/global/_autocompleteField',
		[
			'model' => $model, 'attribute' => 'ressourceId',
			'url' => '/ressource/ajaxComplete', 'urlParams' => ['noimport' => 0],
			'form' => $form, 'value' => (empty($model->ressourceId) ? '' : $model->ressource->nom),
		]
	);

	if ($model->ressourceId) {
		$query = Collection::model()->findAllBySql(
			"SELECT * FROM Collection WHERE ressourceId = :ressourceId ORDER BY nom ASC",
			[':ressourceId' => (int) $model->ressourceId]
		);
		$collections = [];
		foreach ($query as $c) {
			/** @var Collection $c */
			$collections[$c->id] = "{$c->nom}   ({$c->type})";
		}
		echo $form->dropDownListRow($model, 'collectionId', $collections);
	} else {
		echo $form->dropDownListRow($model, 'collectionId');
	}
	Yii::app()->clientScript->registerScript('collectionsParRessource', '
$("#ImportKbartForm_ressourceIdComplete").on("autocompleteselect", function(event, ui) {
	$.ajax({
		url: ' . CJavaScript::encode($this->createUrl('collection/ajaxList')) . ',
		data: { ressourceId: ui.item.id },
		method: "get",
		success: function (data, more) {
			console.log(data);
			$("#ImportKbartForm_collectionId").empty().append($("<option></option>"));
			$.each(data, function(index, item) {
				console.log(item);
				var option = $("<option></option>").attr("value", item.id).text(item.label);
				$("#ImportKbartForm_collectionId").append(option);
			});
		}
	});
});
	');

	echo $form->fileFieldRow($model, 'csvfile', ['class' => 'span10']);
	echo $form->dropDownListRow($model, 'acces', Service::$enumAcces, ['empty' => '…']);
	echo $form->dropDownListRow($model, 'defaultType', Service::$enumType, ['empty' => '…']);
	echo $form->checkBoxRow($model, 'lacunaire', ['label-position' => 'left']);
	echo $form->checkBoxRow($model, 'selection', ['label-position' => 'left']);
	echo $form->checkBoxRow($model, 'checkDeletion', ['label-position' => 'left']);
	echo $form->checkBoxRow($model, 'simulation', ['label-position' => 'left']);
	echo $form->checkBoxRow($model, 'ignoreUnknownTitles', ['label-position' => 'left', 'class' => 'exclusive']);
	echo $form->checkBoxRow($model, 'ignoreUrl', ['label-position' => 'left']);
	if (Yii::app()->user->checkAccess('import/admin')) {
		echo $form->checkBoxRow($model, 'directCreation', ['label-position' => 'left', 'class' => 'exclusive']);
		Yii::app()->clientScript->registerScript('exclusiveCheckboxes', '
			$("form#import-kbart-form").on("click", ".exclusive:checkbox", function() {
				if ($(this).is(":checked")) {
					$(this).closest("form").find(".exclusive:checked").prop("checked", false);
					$(this).prop("checked", true);
				}
			}).on("submit", function() {
				if ($("#ImportKbartForm_directCreation").prop("checked")) {
					return confirm("ATTENTION ! Vous allez créer directement les titres, sans validation au cas par cas. Vous confirmez ?");
				} else {
					return true;
				}
			});
;
		');
	}

	echo '<div class="form-actions">';
	$this->widget(
		'bootstrap.widgets.BootButton',
		[
			'buttonType' => 'submit',
			'type' => 'primary',
			'label' => 'Envoyer',
		]
	);
	echo '</div>';

	$this->endWidget();
	?>

</div>
