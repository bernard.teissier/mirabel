<?php
/** @var ImportSpecForm $model */
?>
<h1>Import d'accès en ligne, source spécifique</h1>

<div class="form">
	<?php
	/** @var BootActiveForm $form */
	$form = $this->beginWidget(
		'BootActiveForm',
		[
			'id' => 'import-spec-form',
			'type' => 'horizontal',
			'enableClientValidation' => false,
		]
	);
	?>

	<?php
	echo $form->errorSummary($model);

	echo $form->dropDownListRow($model, 'className', ImportSpecForm::$enumClassName, ['empty' => '…']);
	echo $form->dropDownListRow($model, 'verbose', [0 => 'Concis', 1 => 'Bavard', 2 => 'Logorrhée inextinguible']);
	echo $form->checkBoxRow($model, 'checkDeletion', ['label-position' => 'left']);
	echo $form->checkBoxRow($model, 'simulation', ['label-position' => 'left']);

	echo '<div class="form-actions">';
	$this->widget(
		'bootstrap.widgets.BootButton',
		[
			'buttonType' => 'submit',
			'type' => 'primary',
			'label' => 'Envoyer',
		]
	);
	echo '</div>';

	$this->endWidget();
	?>

</div>
