<?php
/** @var ImportKbartForm|ImportSpecForm $model */
/** @var import\Log $importLog */
/** @var array $deleted */
/** @var array $deadTitles */
/** @var array $ignoredTitles */

assert($this instanceof Controller);

$this->pageTitle = Yii::app()->name . ' - Imports';
$this->breadcrumbs = [
	'Imports',
];
$this->menu = [
	['label' => 'KBART', 'url' => ['kbart']],
	['label' => 'Spécifique', 'url' => ['spec']],
	['label' => 'Doc technique', 'itemOptions' => ['class' => 'nav-header']],
	['label' => "Doc Import", 'url' => ['/doc/view', 'view' => 'import']],
];

echo $this->renderPartial('_' . get_class($model), ['model' => $model]);

if ($importLog) {
	echo "<h2>Logs de l'action précédente</h2>";

	if ($deleted) {
		echo "<h3>Ces titres ne sont plus importés</h3>"
			. "<p>Ces titres comportent des accès en ligne qui ont été importés mais ne le sont plus.</p>"
			. "<ol>";
		foreach ($deleted as $d) {
			echo "<li>" . $d['title']->getSelfLink() . " : accès d'identifiants ";
			foreach ($d['services'] as $s) {
				echo $s->id . " ";
			}
			echo "</li>";
		}
		echo "</ol>";
	}

	echo "<h3>Titres importés</h3>";
	$logsHtml = $importLog->format('html');
	if ($logsHtml) {
		echo $logsHtml;
	}

	if ($deadTitles !== null) {
		echo "<h3>Titres morts créés</h3>";
		echo "<ol>\n";
		foreach ($deadTitles as $id => $title) {
			echo "<li>"
				. CHtml::link(CHtml::encode($title), ['/titre/view', 'id' => $id])
				. "</li>\n";
		}
		echo "</ol>\n";
	}

	if ($ignoredTitles) {
		$hasUrl = isset($ignoredTitles[0]['url']); ?>
		<h3>Titres ignorés car inconnus de Mir@bel</h3>
		<table class="table table-striped table-bordered table-condensed exportable">
			<theader>
				<th>Titre</th>
				<th>Éditeur</th>
				<th>ISSN</th>
				<th>ISSN-E</th>
				<th><abbr title="Identifiant du titre interne à la ressource (title_id en KBART)">ID</abbr></th>
				<?= $hasUrl ? '<th class="print-only">URL</th><th>Lien</th>' : "" ?>
			</theader>
			<tbody>
				<?php
				foreach ($ignoredTitles as $title) {
					$url = empty($title['url']) ? "" : $title['url'];
					echo "<tr>"
						. "<td>" . CHtml::encode($title['titre']) . "</td>"
						. "<td>" . (isset($title['editeur']) ? CHtml::encode($title['editeur']) : '') . "</td>"
						. "<td>" . (isset($title['issn']) ? CHtml::encode($title['issn']) : '') . "</td>"
						. "<td>" . (isset($title['issne']) ? CHtml::encode($title['issne']) : '') . "</td>"
						. "<td>" . (isset($title['idInterne']) ? CHtml::encode($title['idInterne']) : '') . "</td>"
						. ($hasUrl
							? '<td class="print-only">'
								. CHtml::encode($url)
								. "</td>"
								. '<td>'
								. CHtml::link(" →", $url)
								. "</td>"
							: "")
						. "</tr>\n";
				} ?>
			</tbody>
		</table>
		<?php
	}
}
