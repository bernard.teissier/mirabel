<?php

use models\forms\SudocImport;

/** @var SudocController $this */
/** @var SudocImport $model */
/** @var string $reportFile */

assert($this instanceof Controller);

$this->pageTitle = Yii::app()->name . " - Import du Sudoc";
$this->breadcrumbs = [
	"Import du Sudoc",
];
?>

<h1>Importer des ISSN d'un rapport du Sudoc</h1>

<?php
if ($reportFile) {
	?>
	<section style="margin-bottom: 1em;">
		<h2>Résultat de l'import d'ISSN</h2>
		<?= CHtml::link($reportFile, "/public/sudoc-import/$reportFile", ['class' => 'btn btn-primary']) ?>
	</section>
	<?php
}
?>

<section>
	<h2>Dépôt de fichier</h2>
	<p class="alert alert-info">
		Déposer ici le fichier au format <em>xlsx</em> fourni par l'ABES.
		Les corrections d'ISSN qu'il suggère seront intégrées aux données de Mir@bel,
		après vérifications et interrogation du web-service du Sudoc.
	</p>

	<?php
	$form = $this->beginWidget(
		'BootActiveForm',
		[
			'type' => BootActiveForm::TYPE_HORIZONTAL,
			'enableClientValidation' => false,
			'method' => 'POST',
			'htmlOptions' => ['enctype' => 'multipart/form-data'],
		]
	);
	/** @var BootActiveForm $form */
	echo $form->errorSummary($model);
	echo $form->checkBoxRow($model, 'simulation', ['label' => "Simulation (ne pas enregistrer les modifications)"]);
	echo $form->fileFieldRow($model, 'xlsx', ['class' => 'input-small']);
	?>
	<button type="submit" class="btn btn-primary">Importer ce fichier</button>
	<?php $this->endWidget(); ?>
</section>
