<?php

/** @var Controller $this */
/** @var Partenaire $partenaire */
/** @var PossessionImport $model */
assert($this instanceof Controller);

$this->pageTitle = Yii::app()->name . ' - Import CSV des possessions';
$this->breadcrumbs = [
	$partenaire->nom => ['/partenaire/view', 'id' => $partenaire->id],
	'Import CSV des possessions',
];

$this->menu = [
	['label' => 'Retour à ce partenaire', 'url' => ['/partenaire/view', 'id' => $partenaire->id]],
	['label' => ''],
	['label' => 'Liste des partenaires', 'url' => ['/partenaire/index']],
	['label' => ''],
	['label' => 'Possessions', 'itemOptions' => ['class' => 'nav-header']],
	['label' => 'Gestion des titres', 'url' => ['/partenaire/possession', 'id' => $partenaire->id]],
	['label' => 'Export CSV', 'url' => ['/possession/export', 'partenaireId' => $partenaire->id]],
];
?>

<h1>Importer des possessions : <?php echo CHtml::encode($partenaire->getFullName()); ?></h1>

<?php
if ($model->importedRows) {
	?>
	<ul>
		<?php
		if ($model->simulation) {
			echo "<li>Mode de simulation : <strong>aucune action n'est enregistrée</strong>.</li>";
		} ?>
		<li>
			Pour modifier cet import de possessions, vous pouvez télécharger le CSV de ce
			<?= CHtml::link("tableau des résultats", "#", ['class' => 'btn btn-info btn-small', 'onclick' => 'exportResults(this)']) ?>,
			<em>supprimer les trois dernières colonnes</em>,
			puis l'importer à nouveau.
		</li>
		<li>
			<button type="button" id="toggle-success-rows" class="btn btn-primary btn-small">Afficher/masquer les lignes sans problème</button>
		</li>
	</ul>
	<table class="table table-striped table-bordered table-condensed exportable" id="imported-rows">
		<thead>
			<th>possession</th>
			<th>bouquet</th>
			<th>titre</th>
			<th>titre ID</th>
			<th>issn</th>
			<th>issne</th>
			<th>issnl</th>
			<th>revue ID</th>
			<th>identifiant local</th>
			<th>statut/possession</th>
			<th>titre Mir@bel</th>
			<th>lien</th>
		<tbody>
		<?php
		foreach ($model->importedRows as $row) {
			$titreId = $row[3];
			$status = $row['status']; ?>
			<tr class="<?= $status ?>">
				<?php
				for ($i = 0; $i < 8; $i++) {
					echo CHtml::tag('td', [], $row[$i]);
				} ?>
				<td><?= CHtml::encode($row['message']) ?></td>
				<td><?= CHtml::encode($row['titre M']) ?></td>
				<td><?= $titreId ? CHtml::link("→", ['/titre/view', 'id' => $titreId]) : "" ?></td>
			</tr>
			<?php
		} ?>
		</tbody>
	</table>
	<?php
	$today = date('Y-m-d');
	Yii::app()->clientScript->registerScript(
		'toggle-success-rows',
		<<<EOJS
			$("#toggle-success-rows").on('click', function() {
				$("#imported-rows").toggleClass('hide-success');
			});
			window.exportResults = function (e) {
				exportTableToCSV.apply(e, [$("#imported-rows"), "Mirabel_possessions_import_{$today}.csv"]);
			}
			EOJS
	);
}
?>

<?php
$form = $this->beginWidget(
	'BootActiveForm',
	[
		'id' => 'import-form',
		'type' => 'horizontal',
		'enableClientValidation' => false,
		'hints' => Hint::model()->getMessages("PossessionImport"),
		'htmlOptions' => ['enctype' => 'multipart/form-data'],
	]
);
/** @var BootActiveForm $form */

echo $form->errorSummary($model);

echo $form->fileFieldRow($model, 'importfile', ['errorOptions' => ['class' => 'hidden']]);
echo $form->textFieldRow($model, 'separator', ['class' => 'span1']);
echo $form->checkBoxRow($model, 'overwrite', ['label-position' => 'left']);
echo $form->checkBoxRow($model, 'simulation', ['label-position' => 'left']);
?>

<div class="form-actions">
	<?php echo CHtml::htmlButton('Importer', ['class' => 'btn', 'type' => 'submit']); ?>
</div>

<?php $this->endWidget(); ?>

<h3>Instructions</h3>
<ul>
	<li>
		Pour modifier l'ensemble de vos possessions, vous pouvez importer le
		<?= CHtml::link("tableau de vos possessions", ['possession/export', 'partenaireId' => $partenaire->id], ['class' => 'btn btn-info btn-small']) ?>
	</li>
	<li>
		Le fichier déposé doit suivre le format du fichier CSV d'export.
		Par exemple, télécharger ce <?= CHtml::link("modèle de fichier CSV", Yii::app()->baseUrl . '/public/mirabel_possession.csv', ['class' => 'btn btn-info btn-small']) ?>
		puis modifier le contenu, et enfin déposer le CSV obtenu dans le formulaire en haut de page.
		Les colonnes de ce CSV sont :
		<dl>
			<dt>possession</dt><dd>1 pour ajouter cette revue, 0 pour la retirer si la case de modification est cochée<dd>
			<dt>bouquet</dt><dd>Permet de regrouper les possessions en bouquets<dd>
			<dt>titre</dt><dd>inutilisé<dd>
			<dt>titre ID</dt><dd>L'identifiant numérique du titre dans Mir@bel, visible dans les exports ou dans l'URL d'une page titre (détail par titre d'une revue)<dd>
			<dt>issn</dt><dd>Un ISSN ou ISSN-E de la possession<dd>
			<dt>issne</dt><dd>Un ISSN ou ISSN-E de la possession<dd>
			<dt>issnl</dt><dd>Un ISSN-L de la possession<dd>
			<dt>revue ID</dt><dd>L'identifiant numérique de la revue dans Mir@bel, visible dans les exports ou dans l'URL d'une page revue (page principale)<dd>
			<dt>identifiant local</dt><dd>L'identifiant de la possession dans votre base de données.<dd>
		</dl>
	</li>
	<li>La première ligne sera ignorée, elle contient normalement les titres des colonnes.</li>
	<li>
		Un titre sera identifié par <code>titre ID</code>, <code>issn</code>, <code>issnl</code>, et evéntuellement <code>revue ID</code>.
		À chaque ligne CSV, au moins un de ces champs doit être rempli.
	</li>
	<li>
		Par défaut, les modifications des possessions ne sont pas activées ;
		on ne peut changer l'identifiant local ou le nom du bouquet
		qu'en cochant la case "Modification de l'existant".
	</li>
	<li>
		<p>Après l'envoi du formulaire, un récapitulatif des actions effectuées sera affiché.</p>
		<table class="table table-striped table-bordered table-condensed" style="max-width: 70ex;">
			<thead><th>Légende</th></thead>
			<tbody>
				<tr class="action"><td>Changement effectif</td></tr>
				<tr><td>Pas de changement</td></tr>
				<tr class="warning"><td>Modification non appliquée (refusée)</td></tr>
				<tr class="error"><td>Erreur, données incorrectes ou problème de base de données</td></tr>
			<tbody>
		</table>
	</li>
</ul>
