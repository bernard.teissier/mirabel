<?php

/** @var Controller $this */
/** @var \models\verifications\Liens $model */
assert($this instanceof Controller);

$this->pageTitle = "Vérification de liens";
$this->breadcrumbs = [
	'Vérification' => ['/verification/index'],
	'Autres liens',
];
?>
<?php $this->renderPartial('_nav-links') ?>
<div style="float:right; max-width:30ex;">
	<ul class="well nav nav-list">
		<li class="nav-header">Navigation locale</li>
		<li><?= CHtml::link("Nom différent de la source", '#noms-faux') ?></li>
		<li><?= CHtml::link("domaine ≠ URL", '#domaines-faux') ?></li>
	</ul>
</div>

<h1>Vérifications des autres liens</h1>

<section id="noms-faux" style="clear: right">
	<h2>Liens dont le nom diffère de la source</h2>
	<?php
	if (empty($model->nomsFauxTitres)) {
		echo "<p>Aucune revue.</p>";
	} else {
		?>
	<table class="table table-condensed">
		<thead>
			<tr>
				<th>Titre</th>
				<th>Nom de l'URL</th>
				<th>Nom de la source</th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($model->nomsFauxTitres as $row) {
				$titre = Titre::model()->populateRecord($row);
				echo "<tr>"
					. "<td>{$titre->getSelfLink()}</td>"
					. "<td>{$row['linkName']}</td>"
					. "<td>{$row['sourceName']}</td>"
					. "</tr>";
			} ?>
		</tbody>
	</table>
	<?php
	}
	?>
	<?php
	if (empty($model->nomsFauxEditeurs)) {
		echo "<p>Aucun éditeur.</p>";
	} else {
		?>
	<table class="table table-condensed">
		<thead>
			<tr>
				<th>Éditeur</th>
				<th>Nom de l'URL</th>
				<th>Nom de la source</th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($model->nomsFauxEditeurs as $row) {
				$editeur = Editeur::model()->populateRecord($row);
				echo "<tr>"
					. "<td>{$editeur->getSelfLink()}</td>"
					. "<td>{$row['linkName']}</td>"
					. "<td>{$row['sourceName']}</td>"
					. "</tr>";
			} ?>
		</tbody>
	</table>
	<?php
	}
	?>
</section>

<section id="domaines-faux" style="margin-top: 2em">
	<h2>Liens avec discordance entre nom et URL</h2>
	<p>
		Chaque source est liée à un domaine (par exemple "facebook.com").
		Pour ces autres liens, l'URL passe par un domaine qui n'est pas celui de la source déclarée.
		Par exemple, un lien dont la source est déclarée à "Facebook" doit utiliser le domaine "facebook.com",
		donc l'URL "https://facebook.fr/XYZ" déclenchera une erreur ici.
	</p>
	<?php
	if (empty($model->domainesFauxTitres)) {
		echo "<p>Aucune revue.</p>";
	} else {
		?>
	<table class="table table-condensed">
		<thead>
			<tr>
				<th>Titre</th>
				<th>Domaine de la source</th>
				<th>URL</th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($model->domainesFauxTitres as $row) {
				$titre = Titre::model()->populateRecord($row);
				$url = strlen($row['linkUrl']) > 60
					? CHtml::tag('span', ['title' => $row['linkUrl']], CHtml::encode(substr($row['linkUrl'], 0, 60)) . '&hellip;')
					: CHtml::encode($row['linkUrl']);
				echo "<tr>"
					. "<td>{$titre->getSelfLink()}</td>"
					. "<td>{$row['expectedDomain']}</td>"
					. "<td>{$url}</td>"
					. "</tr>";
			} ?>
		</tbody>
	</table>
	<?php
	}
	?>
	<?php
	if (empty($model->domainesFauxEditeurs)) {
		echo "<p>Aucun éditeur.</p>";
	} else {
		?>
	<table class="table table-condensed">
		<thead>
			<tr>
				<th>Éditeur</th>
				<th>Domaine de la source</th>
				<th>URL</th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($model->domainesFauxEditeurs as $row) {
				$editeur = Editeur::model()->populateRecord($row);
				$url = strlen($row['linkUrl']) > 60
					? CHtml::tag('span', ['title' => $row['linkUrl']], CHtml::encode(substr($row['linkUrl'], 0, 60)) . '&hellip;')
					: CHtml::encode($row['linkUrl']);
				echo "<tr>"
					. "<td>{$editeur->getSelfLink()}</td>"
					. "<td>{$row['expectedDomain']}</td>"
					. "<td>{$url}</td>"
					. "</tr>";
			} ?>
		</tbody>
	</table>
	<?php
	}
	?>
</section>
