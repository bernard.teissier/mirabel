<?php

/** @var Controller $this */
/** @var array $conflictingIssnl */
/** @var array $discordingDates */
/** @var Titre[] $issnEncours */
/** @var Titre[] $issnlAbsents */
/** @var Titre[] $issnlSansIssn */
/** @var Titre[] $issnSansIssnl */
/** @var Titre[] $multipleIssnp */
/** @var Titre[] $supportInconnu */
/** @var Titre[] $sansIssn */
/** @var Titre[] $sharedIssnl */
/** @var Titre[] $titresElectroMultiIssne */
/** @var Titre[] $titresIssneSansAcces */
assert($this instanceof Controller);

$this->pageTitle = "Vérification des ISSN";
$this->breadcrumbs = [
	'Vérification' => ['/verification/index'],
	"Vérifications d'ISSN",
];
?>
<?php $this->renderPartial('_nav-links') ?>
<div style="float:right; max-width:30ex;">
	<ul class="well nav nav-list">
		<li class="nav-header">Navigation dans la page</li>
		<li><?= CHtml::link("ISSN-E sans <em>texte intégral</em>", "#issne-sans-integral") ?></li>
		<li><?= CHtml::link("ISSN sans ISSN-L", "#issn-sans-issnl") ?></li>
		<li><?= CHtml::link("ISSN-L sans ISSN", "#issnl-sans-issn") ?></li>
		<li><?= CHtml::link("ISSN-L non ISSN-(P|E)", "#issnl-non-issn") ?></li>
		<li><?= CHtml::link("Doublons d'ISSN-E", "#issne-doublon") ?></li>
		<li><?= CHtml::link("Support d'ISSN inconnu", "#issn-support-inconnu") ?></li>
		<li><?= CHtml::link("ISSN-L conflictuels", "#issnl-conflictuels") ?></li>
		<li><?= CHtml::link("ISSN-L partagés", "#issnl-partages") ?></li>
		<li><?= CHtml::link("Titres à multiples ISSN-P", "#issnp-multiples") ?></li>
		<li><?= CHtml::link("Dates discordantes", "#dates-discordantes") ?></li>
		<li><?= CHtml::link("Titres sans ISSN", "#sans-issn") ?></li>
		<li><?= CHtml::link("Titres avec un ISSN en cours", "#issn-encours") ?></li>
	</ul>
</div>

<h1>Vérifications d'ISSN</h1>

<section id="issne-sans-integral">
	<h2>Revues avec un ISSN-E mais sans accès <em>texte intégral</em></h2>
	<p>
		Un de titres de la revue a un ISSN-E,
		mais aucun titre n'a d'accès en <em>texte intégral</em>.
	</p>
	<?php
	if ($titresIssneSansAcces) {
		echo "<ol>";
		foreach ($titresIssneSansAcces as $t) {
			/** @var Titre $t */
			echo '<li>'
				. $t->getSelfLink()
				. '</li>';
		}
		echo "</ol>";
	} else {
		echo '<p>Aucun.</p>';
	}
	?>
</section>

<section id="issn-sans-issnl">
	<h2>ISSN sans ISSN-L</h2>
	<p>
		Un titre de revue a un ISSN qui n'est pas associé à un ISSN-L.
	</p>
	<?php
	if ($issnSansIssnl) {
		echo "<ol>";
		$model = Titre::model();
		foreach ($issnSansIssnl as $row) {
			$issn = $row['issn'];
			unset($row['issn']);
			$issnStatut = ($row['issnStatut'] == Issn::STATUT_VALIDE ? "" : " " . Issn::$enumStatut[$row['issnStatut']]);
			unset($row['issnStatut']);
			$t = $model->populateRecord($row);
			echo "<li><code>{$issn}{$issnStatut}</code> {$t->getSelfLink()}</li>\n";
		}
		echo "</ol>";
	} else {
		echo '<p>Aucun.</p>';
	}
	?>
</section>

<section id="issnl-sans-issn">
	<h2>ISSN-L sans ISSN</h2>
	<p>
		Un titre de revue a un ISSN-l qui n'est pas associé à un ISSN.
	</p>
	<?php
	if ($issnlSansIssn) {
		echo "<ol>";
		foreach ($issnlSansIssn as $t) {
			/** @var Titre $t */
			echo "<li><code>{$t->confirm}</code> {$t->getSelfLink()}</li>\n";
		}
		echo "</ol>";
	} else {
		echo '<p>Aucun.</p>';
	}
	?>
</section>

<section id="issnl-non-issn">
	<h2>ISSN-L absents en tant qu'ISSN-(P|E)</h2>
	<p>
		Un des titres de la revue a un ISSN-L qui n'est pas présent sous forme d'ISSN-P ou d'ISSN-E,
		dans aucun des titres de la revue.
	</p>
	<?php
	if ($issnlAbsents) {
		echo "<ol>";
		foreach ($issnlAbsents as $t) {
			/** @var Titre $t */
			echo "<li><code>{$t->confirm}</code> {$t->getSelfLink()}</li>\n";
		}
		echo "</ol>";
	} else {
		echo '<p>Aucun.</p>';
	}
	?>
</section>

<section id="issne-doublon">
	<h2>Titres électroniques à doublons d'ISSN-E</h2>
	<p>
		Le dernier titre de ces revues est électronique,
		et l'ISSN-E associé se retrouve aussi dans de précédents titres.
	</p>
	<?php
	if ($titresElectroMultiIssne) {
		echo "<ol>";
		foreach ($titresElectroMultiIssne as $t) {
			/** @var Titre $t */
			echo "<li><code>{$t->confirm}</code> {$t->getSelfLink()}</li>\n";
		}
		echo "</ol>";
	} else {
		echo '<p>Aucun.</p>';
	}
	?>
</section>

<section id="issn-support-inconnu">
	<h2>Support d'ISSN inconnu</h2>
	<?php
	if ($supportInconnu) {
		echo "<ol>";
		foreach ($supportInconnu as $t) {
			/** @var Titre $t */
			echo "<li><code>{$t->confirm}</code> {$t->getSelfLink()}</li>\n";
		}
		echo "</ol>";
	} else {
		echo '<p>Aucun.</p>';
	}
	?>
</section>

<section id="issnl-conflictuels">
	<h2>ISSN-L conflictuels</h2>
	<p>
		Un même titre a plusieurs ISSN-L distincts.
	</p>
	<?php
	if ($conflictingIssnl) {
		echo HtmlTable::build($conflictingIssnl)->toHtml();
	} else {
		echo '<p>Aucun.</p>';
	}
	?>
</section>

<section id="issnl-partages">
	<h2>ISSN-L partagés</h2>
	<p>
		Au moins deux titres de ces revues ont le même ISSN-L.
	</p>
	<?php
	if ($sharedIssnl) {
		echo "<ol>";
		foreach ($sharedIssnl as $t) {
			/** @var Titre $t */
			echo "<li><code>{$t->confirm}</code> {$t->getSelfLink()}</li>\n";
		}
		echo "</ol>";
	} else {
		echo '<p>Aucun.</p>';
	}
	?>
</section>

<section id="issnp-multiples">
	<h2>Titres à multiples ISSN-P</h2>
	<p>
		Chacun de ces titres a plusieurs ISSN-Papier distincts et valides.
		Le premier de ces ISSN est cité à côté du lien vers la revue.
	</p>
	<?php
	if ($multipleIssnp) {
		echo "<ol>";
		foreach ($multipleIssnp as $t) {
			/** @var Titre $t */
			echo "<li><code>{$t->confirm}</code> {$t->getSelfLink()}</li>\n";
		}
		echo "</ol>";
	} else {
		echo '<p>Aucun.</p>';
	}
	?>
</section>

<section id="dates-discordantes">
	<h2>Dates discordantes entre titre et ISSN</h2>
	<p>
		Les dates approximatives ("19XX", "201.", etc) ou non-remplies dans les ISSN sont ignorées.
	</p>
	<?php
	if ($discordingDates) {
		echo HtmlTable::build($discordingDates)->toHtml();
	} else {
		echo '<p>Aucun.</p>';
	}
	?>
</section>

<section id="sans-issn">
	<h2>Titres sans ISSN</h2>
	<?php
	if ($sansIssn) {
		echo "<ol>";
		foreach ($sansIssn as $t) {
			/** @var Titre $t */
			echo "<li>" . CHtml::link($t->getFullTitle(), ["/titre/view", 'id' => $t->id]) . "</li>\n";
		}
		echo "</ol>";
	} else {
		echo '<p>Aucun.</p>';
	}
	?>
</section>

<section id="issn-encours">
	<h2>Titres avec un ISSN en cours</h2>
	<?php
	if ($issnEncours) {
		echo "<ol>";
		foreach ($issnEncours as $t) {
			/** @var Titre $t */
			echo "<li><code>{$t->confirm}</code>" . CHtml::link($t->getFullTitle(), ["/titre/view", 'id' => $t->id]) . "</li>\n";
		}
		echo "</ol>";
	} else {
		echo '<p>Aucun.</p>';
	}
	?>
</section>
