<?php

/** @var Controller $this */
/** @var int $minImports */
/** @var array $revues */
assert($this instanceof Controller);

$this->pageTitle = "Vérification thématique";
$this->breadcrumbs = [
	'Vérification' => ['/verification/index'],
	'Thématique',
];
?>
<?php $this->renderPartial('_nav-links') ?>

<h1>Vérification de la thématique</h1>

<h2>Revues indexées par seuls imports</h2>

<form>
<p>
	Ces revues ont reçu au moins
	<input type="text" name="minImports" value="<?= $minImports ?>" class="input-mini" />
	thèmes par import extérieur,
	et n'ont aucune indexation manuelle.
	<button type="submit">Actualiser la liste</button>
</p>

<?php
$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'verification-thematique-grid',
		'dataProvider' => new CArrayDataProvider(
			$revues,
			['pagination' => ['pageSize' => 50]]
		),
		'filter' => null,
		'ajaxUpdate' => false,
		'columns' => [
			[
				'name' => "revueId",
				'type' => 'raw',
				'header' => 'Revue',
				'value' => function ($data) {
					return CHtml::link($data['id'], ['/revue/view', 'id' => $data['id']]);
				},
			],
			"titre:text:Titre",
			"importees:text:#thèmes",
		],
	]
);
?>
