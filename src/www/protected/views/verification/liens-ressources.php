<?php

/** @var Controller $this */
/** @var CDataProvider $provider */
/** @var DateTime $dateMajMin */
/** @var DateTime $dateMajMax */
/** @var VerifUrlForm $input */
/** @var array $suivi ID => nomPartenaire */
assert($this instanceof Controller);

$this->pageTitle = "Vérification des liens de ressources";
$this->breadcrumbs = [
	'Vérification' => ['/verification/index'],
	'Liens morts de ressources',
];
?>
<?php $this->renderPartial('_nav-links') ?>

<h1>Vérification des liens de ressources</h1>

<?= $this->renderPartial("_date-verif", ['dateMajMin' => $dateMajMin, 'dateMajMax' => $dateMajMax]) ?>

<?php
$form = $this->beginWidget(
	'BootActiveForm',
	[
		'action' => '/verification/liensRessources',
		'id' => 'filtre-verifurl-form',
		'type' => 'horizontal',
		'enableClientValidation' => false,
	]
);
/** @var BootActiveForm $form */
?>
<fieldset>
	<legend>Limiter par type d'erreur</legend>
	<?= $form->errorSummary($input); ?>

	<?php
	$suiviPartenaireId = (int) ($input->suiviPar ?: Yii::app()->user->partenaireId);

	echo $form->checkBoxRow(
		$input,
		'suiviPar',
		[
			'value' => $suiviPartenaireId,
			'label' => ($suiviPartenaireId == Yii::app()->user->partenaireId ?
				"Ressources que mon partenaire suit"
				: "Ressources que <em>" . CHtml::encode(Partenaire::model()->findByPk($suiviPartenaireId)->nom) . "</em> suit"),
		]
	);
	echo $form->textFieldRow($input, 'msgContient', ['class' => "input-block-level"]);
	echo $form->textFieldRow($input, 'msgExclut', ['class' => "input-block-level"]);
	echo $form->textFieldRow($input, 'urlContient', ['class' => "input-block-level"]);
	echo $form->textFieldRow($input, 'urlExclut', ['class' => "input-block-level"]);
	?>
	<div class="control-group">
		<?= CHtml::submitButton('Filtrer') ?>
	</div>
</fieldset>
<?php $this->endWidget(); ?>

<?php
$columns = [
	[
		'value' => function () {
			static $k = 1;
			return $k++;
		},
	],
	'id',
	[
		'name' => 'ressource',
		'type' => 'raw',
		'value' => function (array $x): string {
			return CHtml::link(
				substr($x['ressource'], 0, 40),
				['/ressource/view', 'id' => $x['ressourceId']]
			);
		},
	],
	[
		'name' => 'url',
		'type' => 'raw',
		'value' => function (array $x): string {
			return CHtml::link(substr($x['url'], 0, 40), $x['url']);
		},
	],
	[
		'name' => 'msg',
		'header' => 'Message',
	],
	[
		'header' => 'Suivi',
		'type' => 'text',
		'value' => function (array $x) use ($suivi) {
			return ($suivi[$x['ressourceId']] ?? '-');
		},
	],
];

$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'verification-liens-grid',
		'dataProvider' => $provider,
		'filter' => null,
		'columns' => $columns,
		'ajaxUpdate' => false,
		'rowCssClassExpression' => function (int $rowNum, array $data): string {
			if (empty($data['ressourceId'])) {
				return '';
			}
			return (Yii::app()->user->isMonitoring(['model' => 'Ressource', 'id' => $data['ressourceId']]) ? 'suivi-self' : '');
		},
	]
);
?>
