<?php

/** @var Controller $this */
/** @var CDataProvider $provider */
/** @var DateTime $dateMajMin */
/** @var DateTime $dateMajMax */
/** @var VerifUrlForm $input */
/** @var array $suivi ID => nomPartenaire */
assert($this instanceof Controller);

$this->pageTitle = "Vérification des liens de revues";
$this->breadcrumbs = [
	'Vérification' => ['/verification/index'],
	'Liens morts de revues',
];
?>
<?php $this->renderPartial('_nav-links') ?>

<h1>Vérification des liens de revues</h1>

<?= $this->renderPartial("_date-verif", ['dateMajMin' => $dateMajMin, 'dateMajMax' => $dateMajMax]) ?>

<p class="alert alert-warning">
	Les liens vers
	<?= CHtml::link("<code>brepolsonline.net</code>", ['/stats/liens', 'domain' => 'brepolsonline.net']) ?>,
	<?= CHtml::link("<code>jstor.org</code>", ['/stats/liens', 'domain' => 'jstor.org']) ?>,
	<?= CHtml::link("<code>linkedin.com</code>", ['/stats/liens', 'domain' => 'linkedin.com']) ?>
	et
	<code>worldcat.org</code> (lien induit par le numéro OCN Worldcat)
	sont exclus de cette vérification,
	parce que ces sites web refusent les requêtes automatiques.
</p>

<?php
$form = $this->beginWidget(
	'BootActiveForm',
	[
		'action' => '/verification/liensRevues',
		'id' => 'filtre-verifurl-form',
		'type' => 'horizontal',
		'enableClientValidation' => false,
	]
);
/** @var BootActiveForm $form */
?>
<fieldset>
	<legend>Limiter par type d'erreur</legend>
	<?= $form->errorSummary($input); ?>

	<?php
	$suiviPartenaireId = $input->suiviPar ?: Yii::app()->user->partenaireId;

	echo $form->checkBoxRow(
		$input,
		'suiviPar',
		[
			'value' => $suiviPartenaireId,
			'label' => ($suiviPartenaireId == Yii::app()->user->partenaireId ?
				"Revues que mon partenaire suit"
				: "Revues que <em>" . CHtml::encode(Partenaire::model()->findByPk($suiviPartenaireId)->nom) . "</em> suit"),
		]
	);
	echo $form->checkBoxRow($input, 'nonSuivi');
	echo $form->checkBoxRow(
		$input,
		'accesIntegralLibre',
		[
			'value' => '1',
			'label' => "Limiter aux accès libres en texte intégral",
		]
	);
	echo $form->textFieldRow($input, 'msgContient', ['class' => "input-block-level"]);
	echo $form->textFieldRow($input, 'msgExclut', ['class' => "input-block-level"]);
	echo $form->textFieldRow($input, 'urlContient', ['class' => "input-block-level"]);
	echo $form->textFieldRow($input, 'urlExclut', ['class' => "input-block-level"]);
	?>
	<div class="control-group">
		<?php echo CHtml::submitButton('Filtrer'); ?>
	</div>
</fieldset>
<?php $this->endWidget(); ?>

<?php
$columns = [
	[
		'value' => function () {
			static $k = 1;
			return $k++;
		},
	],
	'id',
	[
		'name' => 'titre',
		'type' => 'raw',
		'value' => function ($x) {
			return CHtml::link(
				substr($x['titre'], 0, 40),
				['/revue/view', 'id' => $x['revueId']]
			);
		},
	],
	[
		'name' => 'url',
		'type' => 'raw',
		'value' => function ($x) {
			return CHtml::link(substr($x['url'], 0, 40), $x['url']);
		},
	],
	[
		'name' => 'msg',
		'header' => 'Message',
	],
	[
		'header' => 'Suivi',
		'type' => 'text',
		'value' => function ($x) use ($suivi) {
			return ($suivi[$x['revueId']] ?? '-');
		},
	],
];

$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'verification-liens-grid',
		'dataProvider' => $provider,
		'filter' => null,
		'columns' => $columns,
		'ajaxUpdate' => false,
		'rowCssClassExpression' => function ($line, $data) {
			return (isset($data['revueId']) && Yii::app()->user->isMonitoring(['model' => 'Revue', 'id' => $data['revueId']]) ? 'suivi-self' : '');
		},
	]
);
?>
