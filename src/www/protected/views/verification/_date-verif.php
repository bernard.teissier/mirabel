<?php

/** @var Controller $this */
/** @var ?DateTime $dateMajMin */
/** @var ?DateTime $dateMajMax */
assert($this instanceof Controller);

?>
<div class="alert alert-warning">
	<p>
		<strong>Attention</strong>, cette page n'est pas mise à jour automatiquement lors des corrections d'URL.
		Elle l'est à chaque fois que le script de vérification des URL est lancé.
	</p>
	<?php
	if ($dateMajMin && $dateMajMax) {
		$diffMin = (new DateTime)->diff($dateMajMin);
		$diffMax = (new DateTime)->diff($dateMajMax); ?>
	<div><strong>
		Dernière vérification globale : <?= $dateMajMin->format("d/m/Y H:i") ?>
		(il y a <?= ($diffMin->d > 0 ? $diffMin->d . " jours" : $diffMin->h . " heures") ?>)
	</strong></div>
	<div>
		Dernière vérification partielle : <?= $dateMajMax->format("d/m/Y H:i") ?>
		(il y a <?= ($diffMax->d > 0 ? $diffMax->d . " jours" : $diffMax->h . " heures") ?>)
	</div>
		<?php
	}
	?>
</div>
