<?php

/** @var Controller $this */
assert($this instanceof Controller);

$user = Yii::app()->user;
?>
<div class="pull-right">
	<ul class="well nav nav-list">
		<li class="nav-header">Pages de vérification</li>
		<?php
		$links = [
			["Autres liens", ['/verification/liens']],
			["Générales", ['/verification/data']],
			["Éditeurs", ['/verification/editeurs']],
			["Issn", ['/verification/issn']],
			["Liens morts / Revues", ['/verification/liensRevues']],
			["Liens morts / Editeurs", ['/verification/liensEditeurs']],
			["Liens morts / Ressources", ['/verification/liensRessources']],
			["PPN et Sudoc", ['/verification/ppn']],
			["Revues", ['/verification/index']],
			["Thématique", ['/verification/index']],
			["Wikidata", ['/verification/wikidata']],
		];
		if ($user->checkAccess('wikidata/admin')) {
			$links[] = ["Wikidata / Incohérences", ['/verification/wikidatabugs']];
		}

		foreach ($links as $link) {
			$options = [];
			if (isset($link[1][0]) && $link[1][0] === "/{$this->id}/{$this->action->id}") {
				$options['class'] = 'active';
			}
			echo CHtml::tag('li', $options, CHtml::link($link[0], $link[1]));
		}
		?>
	</ul>
</div>
