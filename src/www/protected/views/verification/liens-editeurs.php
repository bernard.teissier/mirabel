<?php

/** @var Controller $this */
/** @var CDataProvider $provider */
/** @var DateTime $dateMajMin */
/** @var DateTime $dateMajMax */
/** @var VerifUrlForm $input */
/** @var array $suivi ID => nomPartenaire */
assert($this instanceof Controller);

$this->pageTitle = "Vérification des liens d'éditeurs";
$this->breadcrumbs = [
	'Vérification' => ['/verification/index'],
	"Liens morts d'éditeurs",
];
?>
<?php $this->renderPartial('_nav-links') ?>

<h1>Vérification des liens d'éditeurs</h1>

<?= $this->renderPartial("_date-verif", ['dateMajMin' => $dateMajMin, 'dateMajMax' => $dateMajMax]) ?>

<p class="alert alert-warning">
	Les <?= CHtml::link("liens vers <code>linkedin.com</code>", ['/stats/liens', 'domain' => 'linkedin.com']) ?>
	sont exclus de cette vérification,
	parce que leur site web refuse les requêtes automatiques.
</p>

<?php
$form = $this->beginWidget(
	'BootActiveForm',
	[
		'id' => 'filtre-verifurl-form',
		'type' => 'horizontal',
		'enableClientValidation' => false,
	]
);
/** @var BootActiveForm $form */
?>
<fieldset>
	<legend>Limiter par type d'erreur</legend>
	<?= $form->errorSummary($input); ?>

	<?php
	echo $form->hiddenField($input, 'suiviPar');
	echo $form->textFieldRow($input, 'msgContient', ['class' => "input-block-level"]);
	echo $form->textFieldRow($input, 'msgExclut', ['class' => "input-block-level"]);
	echo $form->textFieldRow($input, 'urlContient', ['class' => "input-block-level"]);
	echo $form->textFieldRow($input, 'urlExclut', ['class' => "input-block-level"]);
	?>
	<div class="control-group">
		<?php echo CHtml::submitButton('Filtrer'); ?>
	</div>
</fieldset>
<?php $this->endWidget(); ?>

<?php
$columns = [
	[
		'value' => function () {
			static $k = 1;
			return $k++;
		},
	],
	'id',
	[
		'name' => 'nom',
		'type' => 'raw',
		'value' => function (array $x) {
			return CHtml::link(
				substr($x['nom'], 0, 40),
				['/editeur/view', 'id' => $x['editeurId']]
			);
		},
	],
	[
		'name' => 'url',
		'type' => 'raw',
		'value' => function (array $x) {
			return CHtml::link(substr($x['url'], 0, 40), $x['url']);
		},
	],
	[
		'name' => 'msg',
		'header' => 'Message',
	],
];

$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'verification-liens-grid',
		'dataProvider' => $provider,
		'filter' => null,
		'columns' => $columns,
		'ajaxUpdate' => false,
	]
);
?>
