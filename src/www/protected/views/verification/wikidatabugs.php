<?php
use models\wikidata\Compare;

/** @var Controller $this */
/** @var Wikidata $model */
assert($this instanceof Controller);

$this->pageTitle = "Vérification Incohérences wikidata";
$this->breadcrumbs = [
	'Vérification' => ['/verification/index'],
	'Wikidatabugs',
];
$this->renderPartial('_nav-links');
?>

<h1>
	Incohérences Wikidata / Mir@bel
</h1>

<table class="table table-striped table-bordered table-condensed">
	<thead>
		<tr>
			<th>Incohérences ?</th>
			<th>Enregistrements</th>
			<th>Revues</th>
			<th>Titres</th>
			<th>QIDs</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach (Compare::getComparisonStatistics(true) as $row) {
			echo "<tr>";
			echo "<td>" . join('</td> <td>', $row) . "</td>\n";
			echo "</tr>\n";
		}
		?>
	</tbody>
</table>

<?php
$this->widget('ext.bootstrap.widgets.BootGridView', [
	'id' => 'wikidata-grid',
	'ajaxUpdate' => false,
	'dataProvider' => $model->searchInconsistencies(),
	'filter' => $model,
	'rowCssClassExpression' => function (int $line, \Wikidata $data) {
		return $data->getComparisonLevel();
	},
	'columns' => [
		[
			'name' => 'revue',
			'value' => '$data->revueLink',
			'type' => 'raw',
			'filter' => false,
		],
		[
			'header' => 'Page Wikidata',
			'name' => 'qidLink',
			'type' => 'raw',
			'filter' => false,
		],
		[
			'header' => 'Titre Wikidata',
			'name' => 'value', //Titre
		],
		[
			'name' => 'compUrl',
			'type' => 'raw',
			'visible' => !empty($model->property),
			'filter' => false,
		],
		[
			'name' => 'comparison',
			'value' => function (\Wikidata $data) {
				return $data->getComparisonText();
			},
			'type' => 'raw',
			'filter' => Compare::getComparisonDDL(true),
		],
		'compDetails',
		[
			'class' => 'CButtonColumn',
			'template' => '{update}',
			'buttons'=> [
				'update' => [
					'label' => 'Modifier le titre',
					'options' => ['target' => '_blank'],
					'url' => function (\Wikidata $data) {
						return Yii::app()->createUrl('titre/update', ['id' => $data->titreId, '#' => 'titre-liens']);
					},
					'visible' => '$data->isEditable()',
				],
			],
		],
	],
]);
