<?php

/** @var Controller $this */
/** @var CDataProvider $provider */
/** @var ?DateTime $dateMaj */
/** @var string $contient */
/** @var string $exclut */
assert($this instanceof Controller);

$this->pageTitle = "Vérification des liens";
$this->breadcrumbs = [
	'Vérification' => ['/verification/index'],
	'Liens',
];
?>
<?php $this->renderPartial('_nav-links') ?>

<h1>Vérification des liens</h1>

<p class="alert alert-notice">
	Cette page n'est pas mis à jour automatiquement lors des corrections d'URL.
	Elle l'est à chaque fois que le script de vérification des URL est lancé.
	<?php
	if ($dateMaj) {
		$diff = (new DateTime)->diff($dateMaj); ?>
	<strong>
		Dernière vérification globale : <?= $dateMaj->format("d/m/Y") ?>
		(il y a <?= ($diff->d > 1 ? $diff->d . " jours" : $diff->h . " heures") ?> )
	</strong>
		<?php
	}
	?>
</p>

<p class="alert alert-info">
	Les revues que je suis sont surlignées.
</p>

<form name="liens-filtre" method="get">
	<fieldset>
		<legend>Limiter par type d'erreur</legend>
		<div>
			Le message d'erreur doit contenir <input type="text" name="contient" value="<?= CHtml::encode($contient) ?>" />
		</div>
		<div>
			mais exclure <input type="text" name="exclut" value="<?= CHtml::encode($exclut) ?>" />
		</div>
		<button type="submit">OK</button>
	</fieldset>
</form>

<?php
$columns = [
	[
		'value' => function () {
			static $k = 1;
			return $k++;
		},
	],
	[
		'name' => 'revueId',
		'header' => 'revue',
		'type' => 'raw',
		'value' => function ($x) {
			return CHtml::link($x['revueId'], ['/revue/view', 'id' => $x['revueId']]);
		},
	],
	[
		'name' => 'URL',
		'type' => 'raw',
		'value' => function ($x) {
			return CHtml::link(substr($x['URL'], 0, 50), $x['URL']);
		},
	],
	'Erreur',
];

$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'verification-liens-grid',
		'dataProvider' => $provider,
		'filter' => null,
		'columns' => $columns,
		'ajaxUpdate' => false,
		'rowCssClassExpression' => function ($line, $data) {
			return (isset($data['revueId']) && Yii::app()->user->isMonitoring(['model' => 'Revue', 'id' => $data['revueId']]) ? 'suivi-self' : '');
		},
	]
);
?>
