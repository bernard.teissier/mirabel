<?php

/** @var Controller $this */
assert($this instanceof Controller);
/** @var \models\verifications\Editeurs $verif */

$this->breadcrumbs = [
	'Vérification' => ['/verification/index'],
	'Éditeurs',
];
?>
<?php $this->renderPartial('_nav-links') ?>
<div style="float:right; max-width:30ex;">
	<ul class="well nav nav-list">
		<li class="nav-header">Navigation dans la page</li>
		<li><?= CHtml::link("Éditeurs sans titre", "#editeurs-sans-titre") ?></li>
		<li><?= CHtml::link("Éditeurs sans pays", "#sans-pays") ?></li>
		<li><?= CHtml::link("Éditeurs sans IdRef", "#idref") ?></li>
		<li><?= CHtml::link("Éditeurs sans Sherpa", "#sherpa") ?></li>
		<li><?= CHtml::link("Rôle absent", "#relations") ?></li>
	</ul>
</div>

<h1>Éditeurs</h1>

<section id="editeurs-sans-titre">
	<h2>Éditeurs sans titre</h2>
	<?php
	if ($verif->sansTitre) {
		echo "<ol>";
		foreach ($verif->sansTitre as $e) {
			echo '<li>' . $e->getSelfLink() . '</li>';
		}
		echo "</ol>";
	} else {
		echo '<p>Tous les éditeurs ont au moins un titre.</p>';
	}
	?>
</section>

<section id="sans-pays">
	<h2>Éditeurs sans pays</h2>
	<?php
	if ($verif->sansPays) {
		echo "<ol>";
		foreach ($verif->sansPays as $e) {
			echo '<li>' . $e->getSelfLink() . '</li>';
		}
		echo "</ol>";
	} else {
		echo '<p>Tous les éditeurs de Mir@bel ont un pays assigné.</p>';
	}
	?>
</section>

<section id="idref">
	<h2>Éditeurs français sans IdRef</h2>
	<p>
		<?php
		if ($verif->sansIdref) {
			echo CHtml::link("{$verif->sansIdref} éditeurs français", ['/editeur/search', 'q' => ['idref' => '!', 'pays' => 'FRA']])
				. " n'ont pas d'identifiant IdRef.";
		} else {
			echo 'Tous les éditeurs français ont un identifiant IdRef.';
		}
		?>
	</p>
</section>

<section id="sherpa">
	<h2>Éditeurs français sans Sherpa</h2>
	<p>
		<?php
		if ($verif->sansIdref) {
			echo CHtml::link("{$verif->sansSherpa} éditeurs français", ['/editeur/search', 'q' => ['sherpa' => '!', 'pays' => 'FRA']])
				. " n'ont pas d'identifiant Sherpa Romeo.";
		} else {
			echo 'Tous les éditeurs français ont un identifiant Sherpa Romeo.';
		}
		?>
	</p>
</section>

<section id="relations">
	<h2>Éditeurs français sans rôle sur leurs titres</h2>
	<p>
		Ces éditeurs n'ont aucun rôle (ni direction & rédaction, ni publication & diffusion) pour au moins un titre.
	</p>
	<?php
	if ($verif->relationIncomplete) {
		?>
		<p>
			<strong><?= $verif->relationIncomplete ?></strong> éditeurs français concernés.
		</p>
		<h3>Liste de titres concernés (tronquée à 500)</h3>
		<ol>
			<?php
			foreach ($verif->titresRelationIncomplete as $t) {
				echo CHtml::tag('li', [], $t->getSelfLink());
			}
			?>
		</ol>
		<?php
	} else {
		echo '<p>Toutes les relations entre titres et éditeurs sont parfaitement déterminées.</p>';
	}
	?>
</section>
