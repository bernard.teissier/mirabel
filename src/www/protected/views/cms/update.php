<?php

/** @var Controller $this */
/** @var Cms $model */
assert($this instanceof Controller);

$this->breadcrumbs = [
	'Blocs éditoriaux' => ['admin'],
	"Bloc \"{$model->name}\"" => ['view', 'id' => $model->id],
	'Modification',
];

$this->menu = [
	['label' => 'Liste des blocs', 'url' => ['admin']],
	['label' => 'Voir ce bloc', 'url' => ['view', 'id' => $model->id]],
	['label' => 'Créer une brève', 'url' => ['create'], 'visible' => Yii::app()->user->checkAccess('redaction/editorial')],
	['label' => 'Supprimer cette brève', 'url' => '#', 'visible' => ($model->name === 'brève') && Yii::app()->user->checkAccess('redaction/editorial'),
		'linkOptions' => ['submit' => ['delete', 'id' => $model->id], 'confirm' => 'Êtes-vous certain ?'],
	],
];
?>

<h1>Modifier le bloc <em><?php
	echo $model->name;
	if ($model->partenaireId) {
		echo ' / <em>' . CHtml::encode($model->partenaire->nom) . '</em>';
	}
?></em></h1>

<?php
echo $this->renderPartial('_form', ['model' => $model]);
?>
