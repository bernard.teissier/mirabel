<?php

/** @var Controller $this */
/** @var Cms $model */
assert($this instanceof Controller);

$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'cms-form',
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'enableAjaxValidation' => false,
		'hints' => Hint::model()->getMessages('Cms'),
		'htmlOptions' => ['class' => 'well'],
	]
);
/** @var BootActiveForm $form */
?>

<?php
echo $form->errorSummary($model);

if ($model->partenaireId && $model->name === 'partenaire') {
	echo $form->hiddenField($model, 'name');
} elseif (!$model->id && $model->name !== 'brève') {
	echo $form->textFieldRow($model, 'name', ['class' => 'span5']);
}
if ($model->singlePage) {
	echo $form->textFieldRow($model, 'pageTitle', ['class' => 'span10']);
	echo $form->checkBoxRow($model, 'private');
}
echo $form->textAreaRow($model, 'content', ['rows' => 30, 'cols' => 80, 'class' => 'span10']);
if ($model->name === 'brève') {
	if (empty($model->hdateCreat)) {
		$model->hdateCreat = $_SERVER['REQUEST_TIME'];
	}
	echo '<div class="control-group"><label class="control-label">'
		. $model->getAttributeLabel('hdateCreat')
		. '</label><div class="controls">'
		. '<strong>Brève créée le ' . strftime('%d %B %Y', $model->hdateCreat) . '</strong>'
		. ' (' . strftime('%Y-%m-%d %H:%M', $model->hdateCreat) . ')'
		. '</div></div>';
	echo $form->textFieldRow($model, 'newdate', ['size' => 20]);
}
if (Yii::app()->user->checkAccess('partenaire/admin') && $model->isNewRecord && $model->name === 'partenaire') {
	echo $form->dropDownListRow(
		$model,
		'partenaireId',
		Tools::sqlToPairs("SELECT id, nom FROM Partenaire WHERE type='normal' ORDER BY nom"),
		['class' => 'span6', 'empty' => '']
	);
} elseif ($model->partenaireId) {
	echo $form->hiddenField($model, 'partenaireId');
}

if ($model->type !== 'markdown') {
	?>
	<div class="control-group">
		<div class="controls">
			Format : <?= CHtml::encode($model->type) ?>
		</div>
	</div>
	<?php
}
?>

<div class="form-actions">
	<?php
	$this->widget(
	'bootstrap.widgets.BootButton',
	[
		'type' => 'info',
		'buttonType' => BootButton::BUTTON_AJAXSUBMIT,
		'label' => 'Prévisualiser',
		'url' => $this->createUrl('preview'),
		'ajaxOptions' => ['success' => 'js:function(data) {
				$("#preview .modal-body").html(data).parent().modal("show");
			}'],
	]
);
	?>
	<?php
	$this->widget(
		'bootstrap.widgets.BootButton',
		[
			'buttonType' => 'submit',
			'type' => 'primary',
			'label' => $model->isNewRecord ? 'Créer' : 'Enregistrer les modifications',
		]
	);
	?>
</div>

<?php
$this->endWidget();
?>

<?php
$this->beginWidget(
	'bootstrap.widgets.BootModal',
	['id' => 'preview', 'htmlOptions' => ['class' => 'large']]
);
?>
	<div class="modal-header">
		<a href="#" class="close" data-dismiss="modal">&times;</a>
		<h3>Prévisualisation (non-sauvegardée)</h3>
	</div>
	<div class="modal-body">
	</div>
	<div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal">Fermer</a>
	</div>
<?php
$this->endWidget();

if ($model->type === 'markdown') {
	$this->renderPartial('_syntax', []);
}
?>
