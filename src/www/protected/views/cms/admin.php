<?php

/** @var Controller $this */
/** @var Cms $model */
/** @var string $filter */
assert($this instanceof Controller);

$this->breadcrumbs = [
	'Blocs éditoriaux',
];

$this->menu = [
	['label' => 'Nouvelle brève', 'url' => ['create'], 'visible' => Yii::app()->user->checkAccess('redaction/editorial')],
	['label' => 'Nouvelle page', 'url' => ['createPage'], 'visible' => Yii::app()->user->checkAccess('redaction/editorial')],
	[
		'label' => 'Description du partenaire',
		'url' => ['create', 'partenaire' => Yii::app()->user->partenaireId],
		'visible' => Yii::app()->user->checkAccess('partenaire/editorial', ['partenaireId' => Yii::app()->user->partenaireId]),
	],
	['label' => 'Listes', 'itemOptions' => ['class' => 'nav-header']],
	['label' => 'Tout', 'url' => ['admin']],
	['label' => 'Blocs', 'url' => ['admin', 'filter' => 'blocs']],
	['label' => 'Brèves', 'url' => ['admin', 'filter' => 'breves']],
	['label' => 'Pages', 'url' => ['admin', 'filter' => 'pages']],
];
?>

<h1>Liste des blocs éditoriaux<?= CHtml::encode($filter ? " ($filter)" : ''); ?></h1>

<?php
$columns = [
	[
		'name' => 'name',
		'visible' => ($filter !== 'breves'),
	],
	//'type',
	[
		'name' => 'content',
		'filter' => false,
		'htmlOptions' => ['class' => 'contenu'],
		'value' => function ($data) {
			if (strlen($data->content) < 200) {
				return $data->content;
			}
			return strtok($data->content, "\n");
		},
	],
	[
		'name' => 'partenaireId',
		'value' => function ($data) {
			return ($data->partenaireId ? $data->partenaire->nom : '');
		},
		'filter' => CHtml::listData(Partenaire::model()->ordered()->findAll(), 'id', 'nom'),
		'visible' => ($filter !== 'breves' && $filter !== 'pages'),
	],
	[
		'name' => 'hdateCreat',
		'type' => 'date',
		'filter' => false,
	],
	[
		'name' => 'hdateModif',
		'type' => 'date',
		'filter' => false,
	],
	[
		'class' => 'BootButtonColumn',
		'template' => '{view}{update}',
	],
];
$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'cms-grid',
		'dataProvider' => $model->search(),
		'filter' => $model, // null to disable
		'columns' => $columns,
		'ajaxUpdate' => false,
	]
);
?>
