<h3>Documentation Wiki-Markdown</h3>

<dl>
	<dt>Texte formaté</dt>
	<dd>
		<ul>
			<li><em>*italique*</em></li>
			<li><strong>**gras**</strong></li>
			<li><code>`châsse fixe`</code></li>
			<li>Les balises HTML sont conservées sans modification ni vérification.</li>
		</ul>
		<div>Pour séparer deux paragraphes, il faut insérer une ligne vide.</div>
	</dd>
	<dt>Liens et images</dt>
	<dd>
		[texte du lien](http://example.com/)
		<div>Pour inclure une image, il faut préfixer le lien par ! : ![desc img](http://img.com/a.png)</div>
		<div>Pour écrire une url, la syntaxe est &lt;http://example.com&gt;</div>
	</dd>
	<dt>Titres</dt>
	<dd>
		<div class="row-fluid">
		<div class="span6">
		<pre>
## Titre de niveau 2
### Titre de niveau 3
#### Titre de niveau 4</pre>
		</div>
		<div class="span6">
			<h2>Titre de niveau 2</h2>
			<h3>Titre de niveau 3</h3>
			<h4>Titre de niveau 4</h4>
		</div>
	</dd>
	<dt>Listes</dt>
	<dd>
		<div>Attention, il faut une ligne vide avant la liste.</div>
		<div class="row-fluid">
		<div class="span6">
		<pre>
* premier
* second
* troisième</pre>
		</div>
		<div class="span6">
			<ul>
			<li>premier</li>
			<li>second</li>
			<li>troisième</li>
			</ul>
		</div>
		</div>

		<div class="row-fluid">
		<div class="span6">
		<pre>
1. premier
2. second
3. troisième</pre>
		</div>
		<div class="span6">
			<ol>
			<li>premier</li>
			<li>second</li>
			<li>troisième</li>
			</ol>
		</div>
		</div>
	</dd>
	<dt>Tableaux</dt>
	<dd>
		<div class="span6">
		<pre>
| col1 | col2 | aligné à droite |
|------|----|--:|
| a | b | 1 |
| *it* | **gr** | 14 |</pre>
		</div>
		<div class="span6">
<table class="table table-striped table-hover">
<thead>
<tr>
  <th>col1</th>
  <th>col2</th>
  <th style="text-align:right;">aligné à droite</th>
</tr>
</thead>
<tbody>
<tr>
  <td>a</td>
  <td>b</td>
  <td style="text-align:right;">1</td>
</tr>
<tr>
  <td><em>it</em></td>
  <td><strong>gr</strong></td>
  <td style="text-align:right;">14</td>
</tr>
</tbody>
</table>
		</div>
	</dd>
</dl>

<h3>Variables</h3>
<dl>
	<dt>%NB_COMPTES_TYPE_EDITEUR%</dt><dd>Nombre de comptes actifs, de type partenaire-éditeur.</dd>
	<dt>%NB_COMPTES_TYPE_PARTENAIRES%</dt><dd>Nombre de comptes actifs, de type partenaire-veilleur.</dd>
	<dt>%NB_COMPTES_TYPE_TOUT%</dt><dd>Somme des deux précédents.</dd>
	<dt>%NB_EDITEURS%</dt><dd>Nombre d'éditeurs dans Mir@bel.</dd>
	<dt>%NB_LIENS%</dt><dd>Nombre de liens externes (URL distinctes de titres|accès|editeurs|ressources + somme des sudoc|worldcat|lienslibres de titres). Mis à jour toutes les 2 heures.</dd>
	<dt>%RATIO_LIENS%</dt><dd>Nombre de liens externes divisé par le nombre de revues. Mis à jour toutes les 2 heures.</dd>
	<dt>%NB_PARTENAIRES%</dt><dd>Nombre de partenaires actifs, de type normal, dans Mir@bel.</dd>
	<dt>%NB_PARTENAIRES_STATUT_INACTIF%</dt><dd>Nombre de partenaires de statut inactif.</dd>
	<dt>%NB_PARTENAIRES_STATUT_PROVISOIRE%</dt><dd>Nombre de partenaires de statut provisoire.</dd>
	<dt>%NB_PARTENAIRES_TOUT%</dt><dd>Nombre de partenaires de tout statut (même inactif) et de tout type.</dd>
	<dt>%NB_PARTENAIRES_TYPE_EDITEUR%</dt><dd>Nombre de partenaires actifs, de type <em>éditeur</em>.</dd>
	<dt>%NB_PARTENAIRES_TYPE_IMPORT%</dt><dd>Nombre de partenaires actifs, de type <em>import</em>.</dd>
	<dt>%NB_PARTENAIRES_TYPE_TOUT%</dt><dd>Nombre de partenaires actifs, de tout type (normal, éditeur, import).</dd>
	<dt>%NB_RESSOURCES%</dt><dd>Nombre de ressources dans Mir@bel.</dd>
	<dt>%NB_RESSOURCES_SUIVIES%</dt><dd>Nombre de ressources suivies dans Mir@bel.</dd>
	<dt>%NB_RESSOURCES_IMPORT%</dt><dd>Nombre de ressources importées dans Mir@bel.</dd>
	<dt>%NB_REVUES%</dt><dd>Nombre de revues dans Mir@bel.</dd>
	<dt>%NB_REVUES_IMPORT%</dt><dd>Nombre de revues ayant au moins un service importé.</dd>
	<dt>%NB_REVUES_IMPORT_AUTOMATISE%</dt><dd>Nombre de revues ayant au moins un service importé par un import automatique de ressource ou collection.</dd>
	<dt>%NB_REVUES_SUIVIES%</dt><dd>Nombre de revues suivies dans Mir@bel.</dd>
	<dt>%NB_TITRES%</dt><dd>Nombre de titres dans Mir@bel.</dd>
	<dt>%ABCD_REVUES%</dt><dd>Abécédaire des revues</dd>
	<dt>%BIBLIOS_LOGICIELLES%</dt><dd>Liste des composants logiciels dont dépend Mir@bel, avec leurs licences respectives.</dd>
	<dt>%DATE_DER_MAJ%</dt><dd>Date de la dernière mise à jour</dd>
	<dt>%DER_ACTU_<em>?</em>%</dt>
	<dd>
		Affiche une liste des <em>?</em> dernières brèves, triées par leur date de création.
		<code>%DER_ACTU_0%</code> est sans limite.
	</dd>
	<dt>%DER_ACTU_TITRES_<em>?</em>%</dt>
	<dd>Idem <code>%DER_ACTU_?</code> mais avec seulement les titres, liens vers ces brèves.</dd>
	<dt>%DER_CREA_<em>?</em>%</dt>
	<dd>
		Affiche une liste des <em>?</em> dernières créations de revues.
	</dd>
	<dt>%DER_MAJ_<em>?</em>%</dt>
	<dd>
		Affiche une liste des <em>?</em> dernières mises à jour de la base Mir@bel.
		Par exemple <code>%DER_MAJ_3%</code> pour les 3 dernières.
	</dd>
	<dt>%PARTENAIRE%</dt><dd>Le nom de l'institut de la personne consultant la page.</dd>
	<dt>%PARTENAIRE.ID%</dt><dd>L'ID numérique de l'institut de la personne consultant la page.</dd>
	<dt>%SI_CONNECTE_DEB% … %SI_CONNECTE_FIN%</dt>
	<dd>
		Le texte "…" ne sera affiché que si l'utilisateur s'est authentifié sur le site.
	</dd>
</dl>
