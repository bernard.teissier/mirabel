<?php

/** @var Controller $this */
/** @var Cms $model */
assert($this instanceof Controller);

$this->pageTitle = 'CMS - ' . $model->name;

$this->breadcrumbs = [
	'Blocs éditoriaux' => ['admin'],
	"Bloc \"{$model->name}\"",
];

$this->menu = [
	['label' => 'Liste des blocs', 'url' => ['admin']],
	['label' => 'Modifier ce bloc', 'url' => ['update', 'id' => $model->id]],
	['label' => 'Créer une brève', 'url' => ['create'], 'visible' => Yii::app()->user->checkAccess('redaction/editorial')],
	['label' => 'Supprimer cette brève', 'url' => '#', 'visible' => ($model->name === 'brève') && Yii::app()->user->checkAccess('redaction/editorial'),
		'linkOptions' => ['submit' => ['delete', 'id' => $model->id], 'confirm' => 'Êtes-vous certain ?'],
	],
];
?>

<h1><?= $model->singlePage ? "Page éditoriale" : "Bloc éditorial" ?> <em><?= CHtml::encode($model->name); ?></em></h1>

<?php
$this->widget(
	'bootstrap.widgets.BootDetailView',
	[
		'data' => $model,
		'attributes' => [
			'id',
			'name',
			'singlePage:boolean',
			'private:boolean',
			[
				'label' => 'URL publique',
				'type' => 'url',
				'value' => $model->singlePage ? $this->createAbsoluteUrl('/site/page', ['p' => $model->name]) : null,
			],
			'pageTitle',
			'type',
			['name' => 'content', 'type' => 'raw', 'value' => $model->toHtml(false)],
			'partenaireId',
			'hdateCreat:datetime',
			'hdateModif:datetime',
		],
	]
);
?>
