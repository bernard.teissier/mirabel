<?php

/** @var Controller $this */
/** @var Cms $model */
assert($this instanceof Controller);

$this->breadcrumbs = [
	'Blocs éditoriaux' => ['admin'],
	'Nouveau bloc',
];

$this->menu = [
	['label' => 'Liste des blocs', 'url' => ['admin']],
];
?>

<h1>
	<?php
	if ($model->partenaireId) {
		echo "Bloc partenaire <em>" . CHtml::encode($model->partenaire->nom) . "</em>";
	} else {
		echo "Nouvelle brève";
	}
	?>
</h1>

<?php
echo $this->renderPartial('_form', ['model' => $model]);
?>
