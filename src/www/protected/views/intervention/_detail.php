<?php

/** @var Controller $this */
/** @var InterventionDetail $detail */
assert($this instanceof Controller);

if (empty($detail) || $detail->isEmpty()) {
	echo '<p class="well">Cette intervention ne modifie rien.</p>';
	return;
}
?>
<ol id="intervention-detail" class="well">
	<?php
	foreach ($detail->toArray() as $change) {
		?>
	<li>
		<?php
		$subview = sprintf("_detail-%s", strtolower($change['model']));
		if (!file_exists(__DIR__ . "/$subview.php")) {
			$subview = "_detail-default";
		}
		$this->renderPartial($subview, ['detail' => $detail, 'change' => $change]); ?>
	</li>
	<?php
	}
	?>
</ol>
