<?php

/** @var Controller $this */
/** @var InterventionSearch $model */

assert($this instanceof InterventionController);

$this->breadcrumbs = [
	'Interventions' => ['admin'],
	'Administration',
];

$this->menu = [];
?>

<h1>Interventions</h1>

<p>
	Vous pouvez saisir un opérateur de comparaison
	(<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
	ou <b>=</b>) au début de chaque zone de recherche afin de désigner le mode de comparaison.
</p>

<?php
$summary = $model->getSummary();
if ($summary) {
	?>
	<p class="alert alert-info">
		Filtres actifs : <?= CHtml::encode(join(" ", $summary)) ?>
	</p>
	<?php
}
?>

<?php
$this->renderPartial('_search', ['model' => $model]);

$columns = [
	'description',
	[
		'name' => 'ressource_nom',
		'header' => 'Ressource',
		'value' => function (Intervention $data): string {
			if (empty($data->ressourceId)) {
				return '';
			}
			$ressource = $data->ressource;
			if ($ressource === null) {
				return "Supprimée (ID {$data->ressourceId})";
			}
			return $ressource->nom;
		},
		'cssClassExpression' => function ($row, Intervention $data): string {
			$param = [];
			if (!$data->suivi) {
				return false;
			}
			if ($data->ressourceId) {
				$param['model'] = 'Ressource';
				$param['id'] = $data->ressourceId;
			}
			return HtmlHelper::linkItemClass('intervention', $param);
		},
	],
	[
		'name' => 'titre_titre',
		'header' => 'Revue',
		'value' => function (Intervention $data): string {
			if (empty($data->revueId)) {
				return '';
			}
			return ($data->titreRevue ? $data->titreRevue->getShortTitle() : '*sans-titre*');
		},
		'cssClassExpression' => function ($row, Intervention $data) {
			$param = [];
			if (!$data->suivi) {
				return false;
			}
			if ($data->revueId) {
				$param['model'] = 'Revue';
				$param['id'] = $data->revueId;
			}
			return HtmlHelper::linkItemClass('intervention', $param);
		},
	],
	[
		'name' => 'editeur_nom',
		'header' => 'Éditeur',
		'value' => function (Intervention $data) {
			if (!$data->editeurId) {
				return '';
			}
			$e = $data->editeur;
			return ($e ? $e->nom : '(suppr)');
		},
		'cssClassExpression' => function ($row, Intervention $data) {
			if ($data->suivi && $data->editeurId) {
				return HtmlHelper::linkItemClass('', ['model' => 'Editeur', 'id' => $data->editeurId]);
			}
			return false;
		},
	],
	[
		'name' => 'utilisateurIdProp',
		'header' => 'Par …<br>→ pour …',
		'filter' => false,
		'type' => 'html',
		'value' => function (Intervention $data): string {
			if ($data->utilisateurIdProp) {
				$u = $data->utilisateurProp;
				$from = $u ? CHtml::encode($data->utilisateurProp->login) : '(compte suppr.)';
			} else {
				$from = "";
			}
			$to = "";
			$suivi = array_unique(array_filter(explode(',', $data->suivi)));
			foreach ($suivi as $pId) {
				if (!$pId) {
					continue;
				}
				$partenaire = Partenaire::model()->findByPk($pId);
				if ($partenaire) {
					$to .= "<div>→ " . $partenaire->getSelfLink(true) . "</div>";
				}
			}
			return $from . $to;
		},
	],
	[
		'name' => 'statut',
		'value' => function (Intervention $data): string {
			return Intervention::STATUTS[$data->statut];
		},
		'filter' => Intervention::STATUTS,
	],
	'hdateProp:datetime',
	'hdateVal:datetime',
	[
		'class' => 'BootButtonColumn', // CButtonColumn
		'template' => '{view}',
		'buttons' => [
			'delete' => [
				'label' => 'Refuser',
				'url' => function (Intervention $data) {
					return Yii::app()->createUrl('/intervention/reject', ['id' => $data->id]);
				},
				'visible' => '$data->statut === "attente"',
			],
			'update' => [
				'label' => 'Accepter',
				'url' => function (Intervention $data) {
					return Yii::app()->createUrl('/intervention/accept', ['id' => $data->id]);
				},
				'visible' => '$data->statut === "attente"',
			],
			//'view' => ,
		],
		'deleteConfirmation' => 'Êtes-vous sûr de vouloir rejeter cette intervention ?',
	],
];
$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'intervention-grid',
		'dataProvider' => $model->search(),
		'filter' => $model, // null to disable
		'columns' => $columns,
		'ajaxUpdate' => false,
	]
);
?>

<?php
$this->loadViewJs(__FILE__);
