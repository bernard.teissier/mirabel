<?php

/** @var Controller $this */
/** @var array $interventions */
/** @var array $descriptions [title, comment] */
assert($this instanceof Controller);

if ($descriptions) {
	echo "<div>",
		"<strong>" . $descriptions[0] . ".</strong>",
		(count($descriptions) > 1 ? " {$descriptions[1]}." : ""),
		"</div>";
}
if (count($interventions)) {
	echo '<ul>';
	foreach ($interventions as $intervention) {
		/* @var $intervention Intervention */
		if (!empty($intervention)) {
			echo '<li>' . $intervention->render()->link() . '</li>';
		} else {
			Yii::log("Intervention : erreur dans Intervention::listInterventions*()", 'error');
		}
	}
	echo '</ul>';
} else {
	echo '<p>Aucune intervention.</p>';
}
