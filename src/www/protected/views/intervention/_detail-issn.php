<?php

/** @var Controller $this */
/** @var InterventionDetail $detail */
/** @var array $change */
assert($this instanceof Controller);
?>
<h3>ISSN : <?= strtolower($detail->getLabel($change['operation'])) ?></h3>

<?php
if (isset($change["id"])) {
	$existing = Issn::model()->findByPk($change["id"]);
	if ($existing) {
		echo CHtml::tag("div", [], "ISSN {$existing->issn}" . ($existing->issnl ? ", ISSN-L {$existing->issnl}" : ""));
	}
}
?>

<?php
if (isset($change['msg'])) {
	echo "<p>" . CHtml::encode($change['msg']) . '</p>';
}
?>

<div class="row-fluid">
	<?php
	$attributes = [
		[
			'name' => 'id',
			'type' => 'raw',
			'value' => function (\Issn $issn) {
				if ($issn->id === null) {
					return '<em>nouvel ID</em>';
				}
				return $issn->id;
			},
		],
		[
			'name' => 'titreId',
			'type' => 'raw',
			'value' => function (\Issn $issn) {
				if ($issn->titreId === 0) {
					return '<em>ID du titre créé</em>';
				}
				return $issn->titreId;
			},
		],
		[
			'name' => 'issn',
		],
		[
			'name' => 'statut',
			'value' => function (\Issn $issn) {
				if ($issn->statut === null) {
					return '';
				}
				return Issn::$enumStatut[$issn->statut];
			},
		],
		[
			'name' => 'support',
			'value' => function (\Issn $issn) {
				if ($issn->support === null) {
					return '';
				}
				return Issn::$enumSupport[$issn->support];
			},
		],
		[
			'name' => 'issnl',
		],
		[
			'name' => 'dateDebut',
		],
		[
			'name' => 'dateFin',
		],
		[
			'name' => 'pays',
			'label' => "Pays",
			'value' => function (\Issn $issn) {
				return $issn->getCountry();
			},
		],
		[
			'name' => 'titreReference',
		],
		[
			'name' => 'sudocPpn',
			'label' => 'PPN',
			'type' => 'raw',
			'value' => function (\Issn $issn) {
				return $issn->getSudocLink(false);
			},
		],
		[
			'name' => 'sudocNoHolding',
			'value' => function (\Issn $issn) {
				if ($issn->sudocNoHolding === null) {
					return '';
				}
				return $issn->sudocNoHolding ? "Oui : sans notice Sudoc" : "Non : notice Sudoc présente";
			},
		],
		[
			'name' => 'bnfArk',
			'type' => 'raw',
			'label' => "BNF Ark",
			'value' => function (\Issn $issn) {
				return $issn->getBnfLink();
			},
		],
		[
			'name' => 'worldcatOcn',
			'type' => 'raw',
			'label' => "Worldcat",
			'value' => function (\Issn $issn) {
				return $issn->getWorldcatLink();
			},
		],
	];
	?>
	<div class="span6">
		<?php
		if (!empty($change['before'])) {
			$before = new Issn();
			$before->unsetAttributes();
			$before->setAttributes($change['before'], false);
			if (!empty($change['after'])) {
				echo "<h4>Avant</h4>";
			}
			$this->widget(
				'bootstrap.widgets.BootDetailView',
				[
					'data' => $before,
					'attributes' => array_filter(
						$attributes,
						function ($a) use ($change) {
							return array_key_exists($a['name'], $change['before']);
						}
					),
					'htmlOptions' => [
						'class' => "table table-striped table-bordered table-condensed",
					],
				]
			);
		}
		?>
	</div>
	<div class="span6">
		<?php
		if (!empty($change['after'])) {
			$after = new Issn();
			$after->unsetAttributes();
			$after->setAttributes($change['after'], false);
			if (!empty($change['before'])) {
				echo "<h4>Après</h4>";
			}
			$this->widget(
				'bootstrap.widgets.BootDetailView',
				[
					'data' => $after,
					'attributes' => array_filter(
						$attributes,
						function ($a) use ($change) {
							return array_key_exists($a['name'], $change['after']);
						}
					),
					'htmlOptions' => [
						'class' => "table table-striped table-bordered table-condensed",
					],
				]
			);
		}
		?>
	</div>
</div>
