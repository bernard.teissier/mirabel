$('table').on('click', 'td', function(){
	var url = $(this).siblings('td:last-child').find('a[title="Voir"]').first().attr('href');
	if (url) {
		window.location = url;
	}
});

$("#extra-search").hide();
$("#toggle-extra-search").on('click', function() {
	$("#extra-search").toggle();
	return false;
});
