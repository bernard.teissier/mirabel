<?php

/** @var Controller $this */
/** @var InterventionDetail $detail */
/** @var array $change */
assert($this instanceof Controller);

?>
<h3>
	<?= \widgets\InterventionViewer::prettifyModelName($change["model"]) ?>	:
	<?= strtolower($detail->getLabel($change['operation'])) ?>
</h3>
<?php
$existing = false;
switch ($change['model']) {
	case 'Service':
		if ($change['operation'] !== 'create' && !empty($change["id"])) {
			$service = Service::model()->findByPk((int) $change["id"]);
			if ($service) {
				printf(
					"<div>État actuel de cet accès : %s — accès %s — %s (nouvelle page)</div>\n",
					$service->type,
					strtolower($service->acces),
					CHtml::link("modifier cet accès", ['/service/update', 'id' => $service->id], ['target' => '_blank'])
				);
			}
		}
		break;
	default:
		if (isset($change["id"])) {
			$existing = $change["model"]::model()->findByPk($change["id"]);
		}
}
if ($existing) {
	if ($existing instanceof Titre) {
		echo CHtml::link($existing->getFullTitle(), ['/titre/view', 'id' => $existing->id]);
	} elseif ($existing instanceof Issn) {
		echo "ISSN $existing->issn"
			. ($existing->issnl ? ", ISSN-L {$existing->issnl}" : "");
	} elseif (method_exists($existing, 'getSelfLink')) {
		echo $existing->getSelfLink();
	} elseif (method_exists($existing, 'getFullName')) {
		echo $existing->getFullName();
	}
}
if (isset($change['msg'])) {
	echo "<p>" . CHtml::encode($change['msg']) . '</p>';
}
if (!empty($change['before']['liensJson'])) {
	$change['before']['liensJson'] = new TitreLiens($change['before']['liensJson']);
}
if (!empty($change['after']['liensJson'])) {
	$change['after']['liensJson'] = new TitreLiens($change['after']['liensJson']);
}
?>
<div class="row-fluid">
	<div class="span6 content-before">
		<?php
		if (!empty($change['before'])) {
			if (!empty($change['after'])) {
				echo "<h4>Avant</h4>";
			}
			echo Tools::htmlTable(
				\widgets\InterventionViewer::expandKeys($change['before'], $change["model"]::model()),
				true,
				false
			);
		}
		?>
	</div>
	<div class="span6 content-after">
		<?php
		if (!empty($change['after'])) {
			if (!empty($change['before'])) {
				echo "<h4>Après</h4>";
			}
			echo Tools::htmlTable(
				\widgets\InterventionViewer::expandKeys($change['after'], $change["model"]::model()),
				true,
				false
			);
		}
		?>
	</div>
</div>
