<?php

/** @var Controller $this */
/** @var Intervention $intervention */
/** @var BootActiveForm $form */
/** @var bool $canForceValidating */
/** @var bool $forceProposition */
assert($this instanceof Controller);

if (!isset($intervention)) {
	$intervention = new Intervention();
}
if (!isset($forceProposition)) {
	$forceProposition = !empty($_POST['force_proposition']);
}

?>
<fieldset>
	<legend>Contact</legend>
	<?php
	if (
		$canForceValidating
		&& Yii::app()->user->checkAccess('admin') && $intervention->suivi // intervention d'un admin sur un objet suivi
		&& (empty($intervention->ressourceId) || empty($intervention->ressource->autoImport)) // mais pas réservé aux admin
	) {
		echo CHtml::hiddenField('force_proposition', '0');
		echo $form->freeRow(
			'force_proposition',
			"Proposition",
			CHtml::checkBox('force_proposition', $forceProposition)
			. " Passer par l'état <em>en attente</em> au lieu d'une modification directe en tant qu'admin"
		);
	}
	if (Yii::app()->user->isGuest) {
		?>
		<p>
			Si vous le souhaitez, vous pouvez laisser vos coordonnées,
			ou un commentaire sur la modification que vous proposez.
		</p>
		<?php
		echo $form->textFieldRow($intervention, 'email', ['class' => 'span6']);
	} else {
		echo $form->hiddenField($intervention, 'email');
	}
	echo $form->textAreaRow($intervention, 'commentaire', ['cols' => '100', 'rows' => 6, 'class' => 'span6']);
	if (Yii::app()->user->isGuest) {
		echo $form->hiddenField($intervention, 'spam1');
		echo $form->textFieldRow($intervention, 'spam2');
	}
	?>
</fieldset>
