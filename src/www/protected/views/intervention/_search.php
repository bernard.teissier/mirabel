<?php

/** @var Controller $this */
/** @var InterventionSearch $model */

assert($this instanceof Controller);

$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'action' => Yii::app()->createUrl($this->route),
		'method' => 'get',
		'type' => BootActiveForm::TYPE_HORIZONTAL,
	]
);
/** @var BootActiveForm $form */

if (Yii::app()->user->checkAccess('admin')) {
	echo '<button type="button" class="btn btn-default" id="toggle-extra-search">Recherche avancée…</button>';

	echo '<div id="extra-search">';

	$users = Tools::sqlToPairs(
		"SELECT u.id, CONCAT(p.nom, ' — ', u.login) AS name"
		. " FROM Utilisateur u JOIN Partenaire p ON u.partenaireId = p.id"
		. " ORDER BY p.nom, u.login"
	);
	$partenaires = Tools::sqlToPairs(
		"SELECT p.id, p.nom"
		. " FROM Partenaire p"
		. " ORDER BY p.nom"
	);

	echo $form->dropDownListRow($model, 'statut', Intervention::STATUTS, ['prompt' => '']);

	echo $form->textFieldRow($model, 'ressource_nom');
	echo $form->textFieldRow($model, 'titre_titre');
	echo $form->textFieldRow($model, 'editeur_nom');

	echo $form->dropDownListRow($model, 'import', ImportType::getImportList(), ['prompt' => '']);
	echo $form->dropDownListRow($model, 'partenaireId', $partenaires, ['prompt' => '']);
	echo $form->dropDownListRow($model, 'utilisateurIdProp', array_merge(['0' => 'ANONYME'], $users), ['prompt' => '']);
	echo $form->dropDownListRow($model, 'utilisateurIdVal', $users, ['prompt' => '']);

	echo $form->textFieldRow($model, 'hdateProp', ['class' => 'span5']);
	echo $form->textFieldRow($model, 'hdateVal', ['class' => 'span5']);
	echo $form->textFieldRow($model, 'delai', ['class' => 'span3', 'placeholder' => "secondes"]);

	echo $form->dropDownListRow($model, 'suiviPartenaireId', $partenaires, ['prompt' => '']);
	echo "</div>\n";
}

echo $form->checkBoxRow($model, 'suivi');
?>

<div class="form-actions">
	<button type="submit" class="btn btn-primary">Filtrer</button>
</div>

<?php $this->endWidget(); ?>
