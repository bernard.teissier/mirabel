<?php

/** @var Controller $this */
assert($this instanceof Controller);

$this->breadcrumbs=[
	'Hint',
];

$this->menu=[
	['label' => 'Create Hint', 'url' => ['create']],
	['label' => 'Manage Hint', 'url' => ['admin']],
];
?>

<h1>Hint</h1>

<?php
$this->widget(
	'bootstrap.widgets.BootListView',
	[
		'dataProvider' => $dataProvider,
		'itemView' => '_view',
	]
);
?>
