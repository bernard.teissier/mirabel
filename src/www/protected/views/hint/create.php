<?php

/** @var Controller $this */
/** @var Hint $model */
assert($this instanceof Controller);

$this->breadcrumbs = [
	'Hint' => ['admin'],
	'Créer',
];

$this->menu = [
	['label' => 'Administration', 'url' => ['admin']],
];
?>

<h1>Nouveau Hint</h1>

<?php
echo $this->renderPartial('_form', ['model' => $model]);
?>
