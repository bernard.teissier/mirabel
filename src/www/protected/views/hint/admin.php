<?php

/** @var Controller $this */
/** @var Hint $model */
assert($this instanceof Controller);

$this->breadcrumbs = [
	'Formulaires' => ['admin'],
	'Administration',
];

$this->menu = [
	['label' => 'Créer', 'url' => ['create']],
];
?>

<h1>Documentation des formulaires</h1>

<p>
	Vous pouvez saisir un opérateur de comparaison
	(<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
	ou <b>=</b>) au début de chaque zone de recherche afin de désigner le mode de comparaison.
</p>

<?php
$columns = [
	[
		'name' => 'model',
		'filter' => Hint::$enumModel,
		'value' => function (Hint $data) {
			return Hint::$enumModel[$data->model] ?? $data->model;
		},
	],
	'attribute',
	'name',
	'description:html',
	'hdateModif:datetime',
	[
		'class' => 'BootButtonColumn', // CButtonColumn
		'template' => '{update}',
	],
];
$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'hints-grid',
		'dataProvider' => $model->search(),
		'filter' => $model, // null to disable
		'columns' => $columns,
		'ajaxUpdate' => false,
	]
);
?>
