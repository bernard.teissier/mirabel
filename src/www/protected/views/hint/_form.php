<?php

/** @var Controller $this */
/** @var Hint $model */
assert($this instanceof Controller);

/** @var BootActiveForm $form */
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'hint-form',
		'enableAjaxValidation' => false,
		'hints' => Hint::model()->getMessages('Hint'),
		'type' => BootActiveForm::TYPE_HORIZONTAL,
	]
);
?>

<?php

echo $form->errorSummary($model);

if ($model->isNewRecord) {
	$labels = $model->attributeLabels();
	echo $form->dropDownListRow(
		$model,
		'model',
		Hint::$enumModel,
		[
			'ajax' => [
				'type'=>'POST',
				'url' => $this->createUrl('/hint/dynamicAttributes'),
				'update' => '#Hints_attribute',
			],
		]
	);
	reset(Hint::$enumModel);
	$objet = current(Hint::$enumModel);
	echo $form->dropDownListRow($model, 'attribute', $model->getListHintsCreate($objet));
	echo $form->textFieldRow($model, 'name', ['class' => 'span5']);
} else {
	echo "<p>Les premiers champs ne sont pas modifiables, ils décrivent l'entrée de formulaire à documenter.</p>";
	echo $form->uneditableRow($model, 'model');
	echo $form->uneditableRow($model, 'attribute');
	if (empty($model->name)) {
		echo $form->textFieldRow($model, 'name', ['class' => 'span5']);
	} else {
		echo $form->uneditableRow($model, 'name');
	}
}

Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/ckeditor/ckeditor.js');
Yii::app()->clientScript->registerScript('ckeditor', "CKEDITOR.replaceAll('htmleditor');");
echo $form->textAreaRow($model, 'description', ['rows' => 8, 'cols' => 50, 'class' => 'span8 htmleditor']);

?>
<div class="form-actions">
	<?php
$this->widget(
	'bootstrap.widgets.BootButton',
	[
		'buttonType' => 'submit',
		'type' => 'primary',
		'label' => $model->isNewRecord ? 'Créer' : 'Enregistrer',
	]
);
?>
</div>

<?php
$this->endWidget();
?>
