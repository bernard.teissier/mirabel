<?php

/** @var Controller $this */
/** @var Hint $model */
assert($this instanceof Controller);

$this->breadcrumbs=[
	'Formulaires' => ['admin'],
	$model->model => ['admin', 'Hint[model]' => $model->model],
	$model->name,
	'Modifier',
];

$this->menu = [
	['label' => 'Administration', 'url' => ['admin']],
];
?>

<h1>Modifier l'aide de : <em><?php echo "{$model->model} / {$model->name}"; ?></em></h1>

<?php
echo $this->renderPartial('_form', ['model' => $model]);
?>
