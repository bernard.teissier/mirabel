<?php

/** @var Controller $this */
/** @var Hint $model */
assert($this instanceof Controller);

$this->breadcrumbs = [
	'Hint' => ['admin'],
	$model->name,
];

$this->menu = [
	['label' => 'Administration', 'url' => ['admin']],
	['label' => 'Créer', 'url' => ['create']],
	['label' => 'Modifier', 'url' => ['update', 'id' => $model->id]],
	['label' => 'Supprimer', 'url' => '#',
		'linkOptions' => [
			'submit' => ['delete', 'id' => $model->id],
			'confirm' => 'Êtes-vous sûr de vouloir supprimer cet élément ?',
		],
	],
];
?>

<h1>View <em>Hint #<?php echo $model->id; ?></em></h1>

<?php
$this->widget(
	'bootstrap.widgets.BootDetailView',
	[
		'data' => $model,
		'attributes' => [
			'id',
			'model',
			'attribute',
			'name',
			'description',
			'hdateModif',
		],
	]
);
?>
