# Imports de titres et d'accès en ligne

## Identification d'un titre

Après extraction des données d'une ligne,
les opérations commencent toujours par une tentative d'identification du titre.
Les méthodes employées sont, dans l'ordre par défaut :

1. ISSN
2. ISSN-L (avec ce même ISSN lu)
3. ISSN-E
4. idInterne pour cette ressource
5. URL
6. titre + éditeur

Ce comportement par défaut peut être configuré au niveau de la ressource avec
le champ `importIdentifications`.

Si plusieurs titres correspondent (Mirabel autorise un ISSN a être répété à l'intérieur d'une revue),
alors le titre actif est choisi en priorité,
et à défaut le titre qui a la plus grande date de fin.

NB développeur : Cf classe `TitleIdentifier` dans `src/www/protected/models/import/TitleIdentifier.php`.


## Actions pour un titre non identifié

1. Si le paramètre *N'importer que les accès des revues déjà dans Mir@bel* (`ignoreUnknownTitles`)
   est coché, alors le titre inconnu apparaîtra seulement dans les logs.

2. Si le paramètre *Création directe des titres (admin+DEBUG)* (`directCreation`)
   est coché, alors le titre sera créé à la volée.

3. Sinon, la création du titre est proposée (et validée en mode *Création directe*) avec :

	* une nouvelle revue,
	* un titre formé des données importées,
	* les champs complétés par le webservice SUDOC si possible,
	* les données liées au couple (Ressource, Titre) : bimpe, idInterne, issne,
	* la création des éditeurs non identifiés (cf section *Mise à jour des éditeurs*),
	* les liens avec les éditeurs identifiés.

   Les interventions en attente de validation proposant cette même création sont effacées.

## Actions pour un titre identifié

Les données liées au couple (Ressource, Titre) sont mises à jour :

* identification (idInterne) ;
* ISSN (complété à la volée par le Sudoc, d'abord en interrogeant par ISSN, puis par PPN) ;
* accès en ligne.

Si rien ne change, l'intervention n'est pas enregistrée.

### Mise à jour des éditeurs

Ceci ne s'applique que pour les nouveaux titres.
Si le titre existe déjà dans la base, rien ne se passe concernant ses éditeurs.

* L'éditeur est supposé unique (un seul champ importé).
* Si l'éditeur d'un titre n'y est pas encore lié, on cherche à identifier l'éditeur par son nom.
  En cas d'homonymies, on s'arrête là.

1. Si l'éditeur est trouvé, on ajoute la création du lien Titre-Editeur à l'intervention liée au titre en cours.
2. Si l'éditeur n'existe pas, on en propose la création (en effaçant les précédentes propositions identiques).
   On ne propose pas de lien Titre-Editeur.
   Avec le mode *Création directe*, l'éditeur est créé directement, et le lien Titre-Editeur aussi.


### Mise à jour des accès en ligne

Si l'accès à importer est associé à une ou plusieurs collections,
le traitement se fait séparément pour chaque type de collection.
Le détail et des exemples sont dans le ticket #3538.

Si l'accès à importer n'a pas de collection,
on ne considère que les accès du même couple (Ressource, Titre).

Pour chaque accès importé :

* Si plusieurs accès ont les mêmes valeurs pour `acces` ("Libre" ou "Restreint") et `type` ("Sommaire", etc),
  alors on garde le premier et on efface les suivants (*doublons*).
* Si un accès de même valeurs pour `acces` ("Libre" ou "Restreint") et `type` ("Sommaire", etc) est trouvé,
  il est modifié par l'accès importé.
* Sinon, un nouvel accès est créé.

Dans tous les cas, l'horodatage de dernier import `hdateImport` est modifié.

Aucun accès n'est supprimé à ce stade.

Des collections peuvent être reliés à l'accès dans la même intervention de création|modification de l'accès.
Cet import de liens vers les collections de la ressource est en ajout et en suppression.
La collection est identifiée par son champ `identifiant` (présent dans la structure SQL depuis 2014-10-15).

#### Exemple concret d'accès à collections

Supposons qu'on importe par KBART un accès en ligne :

 - ressource: `JSTOR`
 - URL: `http://monacceslibre.org`
 - acces: `Libre`
 - type: `Texte intégral`
 - Collections: ["Science1" (courant), "Science2" (courant)]

Les deux collections étant de même type ("courant"), elles seront traitées ensemble.

1. On liste des accès dans Mirabel qui portent sur la même ressource et le même titre
   *et sont rattachés à une collection de type "courant"*.
2. Supposons que l'un de ces accès soit lui aussi "Libre" et "Texte intégral",
   Mirabel considère alors que l'accès importé en est une mise à jour.
   Supposons que l'ancien accès est dans les collections ["Science3" (courant), "Science1900" (archive)"]
3. On ajoute à l'intervention la modification de cet accès (nouvelle URL, par exemple)
   et le changement des collections courantes (ajout de "Science1" et "Science2", sans changer l'appartenance à la collection de type "archive").

Dans un cas plus réaliste, l'accès existant dans Mirabel n'aurait été rattaché qu'à une collection
de type "archive", donc l'import ne l'aurait pas modifié,
et aurait créé un nouvel accès avec les deux catégories courantes.



### Vérifier si des titres ont été retirés

Plus précisément, ce sont les titres pour lesquels au moins un accès a été retiré.

À la fin d'un import, on cherche les accès en ligne vérifiant les critères suivants : 

* de la même ressource que cet import,
* du même type d'import (Cairn, KBART, etc) que cet import
 (chaque type d'import a un identifiant numérique interne, par exemple 7 pour KBART),
* créés avant le dernier import de cette ressource et ce type d'import,
* non modifiés par ce dernier import.

On affiche alors les titres correspondant à ces accès.

Par exemple, après un import quotidien de CairnMagazine dans Cairn,
on cherchera les accès qui avaient été déjà importés dans la ressource Cairn,
en passant par un fichier au format CairnMagazine.
accès qui ne figurent pas dans l'import qui vient d'être fait,
et qui ne sont pas des créations récentes (ils n'ont pas été créés la veille).



## Annexes

### URL interrogées par les anciens imports automatiques

Certaines ressources sont globalament alimentées par un import automatique (cf `Ressource.autoImport`).
De même pour des collections (cf `Collection.importee`).
Dans les deux cas, les accès produits sont verrouillés (modification restreinte aux administrateurs).

Les ressources importées automatiquement (chacune avait son type d'import avant KBART) :

* Cairn : <http://dedi.cairn.info/NL/revues_cairn_csv_2015.php> (le "2015" est nécessaire à ce jour, 2018-01-17)
* Cairn Magazine : <http://dedi.cairn.info/NL/magazines_cairn_csv_2015.php>
* Persée : <http://tools.persee.fr/Mirabel/revues_persee.csv>
* Sign@l - KBART : <http://signal.sciencespo-lyon.fr/index.php?r=site/export&indexation=1>  
  <http://signal.sciencespo-lyon.fr/index.php?r=site/export&indexation=0>

   Ces URL pour le format "KBART" dans Sign@l sont données en paramètre de script dans cron,
   contrairement aux précédentes qui sont inscrites dans le code de chaque type d'import.

### ID internes à une ressource

Suivant le mode d'import, un ID interne à la ressource peut être rattaché à un titre.
Il est utilisé pour identifier le titre si les méthodes précédentes ne réussissent pas.

* Cairn : Partie de l'URL entre `http://www.cairn.info/` et `.htm` dans le champ "URL DE LA REVUE".
* CairnMagazine : Idem Cairn.
* Érudit : La partie finale (après le dernier slash) de l'URL du titre chez Érudit (champ "JOURNAL URL").
* Persée : Champ "REVUE_KEY".
* Revues.org : Pas d'identifiant interne.

On peut accéder à ces données et les modifier en suivant le lien "Éditer ISSN-E et id internes" sur la page d'un *titre*
(et non d'une revue).

### Ligne de commande

L'import dans Mir@bel a une interface en ligne de commande, principalement prévue pour un usage en *cron*.
Tous les imports spécifiques et KBart sont possibles.

La liste des commandes, avec les options, est affichée par :
```
./yii import
```

Un cas particulier est la liste des accès importés qui semblent avoir été supprimés.
```
./yii import delete [--ressource=1] [--since=YYYY-MM-DD]
```

La syntaxe de date est en réalité plus souple.
Le cron utilise par exemple `./yii import delete --since="-1 week"`
pour lister les accès qui ont déjà été importés mais pas depuis une semaine.

### Interventions créées

Toute intervention provenant d'un import a un marqueur correspondant à la méthode d'import.
Par exemple, `Intervention.import = 7` pour KBART.

Cela joue un double rôle :

- distinguer les opérations d'import des opérations manuelles,
- repérer les changements depuis le précédent import de même type sur la même ressource
  (voir la section "Vérifier si des titres ont été retirés" plus haut).

### Générer des données pour Mir@bel au format KBART

Pour produire des données que Mir@bel pourra assimiler,
le format le plus standard est KBART, qui est assez simple à préparer :

1. C'est un fichier textuel type CSV, où les colonnes sont séparées par
   des tabulations.
2. La norme décrit des colonnes obligatoires, d'autres optionnelles.
   Les colonnes "Collection" et "Discipline" sont par exemple
   facultatives dans l'exemple ci-dessous.
3. Un fichier est produit pour chaque collection/bouquet proposé (ou commercialisé).
   Le nom du fichier est normé avec le nom du fournisseur et du bouquet.
4. Il faut produire un fichier KBART par type d'accès (libre, restreint).
5. Il y a une convention de nommage complexe pour décrire les dates
   barrières, comme "365D" (accès à la dernière année glissante) ou
   "P2Y" (accès en dehors des 2 dernières années calendaires).
6. Les [spécifications de KBART I](http://www.uksg.org/kbart/s1/summary)
   détaillent ce format.
   On peut compléter avec [KBART II](http://www.niso.org/workrooms/kbart).

#### Exemple (extrait de JSTOR, avec l'entête et une ligne de contenu)

```
publication_title	print_identifier	online_identifier	date_first_issue_online	num_first_vol_online	num_first_issue_online	date_last_issue_online	num_last_vol_online	num_last_issue_online	title_url	first_author	title_id	embargo_info	coverage_depth	coverage_notes	publisher_name	full_coverage	collection	discipline	date_added_online	catalog_identifier_oclc	catalog_identifier_lccn
Contemporary Sociology	0094-3061	1939-8638	1972-01-01	1	1	2011-11-01	40	6	http://www.jstor.org/action/showPublication?journalCode=contsoci		contsoci	P3Y	fulltext	2 years	American Sociological Association	1972-01-01 - 2011-11-01	Arts & Sciences I Collection;For-Profit Academic Arts & Sciences I Collection	Sociology	1997-12-01	38037337	sn97-23245
```

Ce qui correspond au tableau suivant :

publication_title|print_identifier|online_identifier|date_first_issue_online|num_first_vol_online|num_first_issue_online|date_last_issue_online|num_last_vol_online|num_last_issue_online|title_url|first_author|title_id|embargo_info|coverage_depth|coverage_notes|publisher_name|full_coverage|collection|discipline|date_added_online|catalog_identifier_oclc|catalog_identifier_lccn
-----------------|----------------|-----------------|-----------------------|--------------------|----------------------|----------------------|-------------------|---------------------|---------|------------|--------|------------|--------------|--------------|--------------|-------------|----------|----------|-----------------|-----------------------|-----------------------
Contemporary Sociology|0094-3061|1939-8638|1972-01-01|1|1|2011-11-01|40|6|http://www.jstor.org/action/showPublication?journalCode=contsoci||contsoci|P3Y|fulltext|2 years|American Sociological Association|1972-01-01 - 2011-11-01|Arts & Sciences I Collection;For-Profit Academic Arts & Sciences I Collection|Sociology|1997-12-01|38037337|sn97-23245
