Wikidata
========

En bref
-------

- On importe de Wikidata toutes les données qu'il a sur les revues portant la propriété *Mir@belID*.
  Ces données sont lues par un script en ligne de commande et enregistrées dans une table de la base de données.
- On n'exploite que les URL vers Wikipedia qui sont ajoutées aux titres (un lien par langue de Wikipedia).
  L'import Wikidata (en cron) ajoute donc des liens vers Wikipedia.
- La page de vérification des liens d'une revue affiche les données associées dans Wikidata.
- La principale difficulté est la correspondance entre les titres d'une revue de Mir@bel et Wikidata.
  Cf [Affichages web](#affichages-web-des-informations-et-comparaisons) pour les contrôles possibles.


Import Wikidata global (quotidien)
----------------------------------

### Termes et concepts Wikidata

Chaque **élément** wikidata est identifié par son **QId**, par exemple [Q3428731](https://www.wikidata.org/wiki/Q3428731)
pour la *Revue française de science politique*, et porte des déclarations **Propriété = Valeur**.
Chaque propriété a un numéro, et son identifiant est de la forme **Pnnnn**.

*Certaines* des propriétés Wikidata sont des identifiants externes, qui peuvent donner des url, **ce qui nous intéresse principalement**.
Par exemple [P236](https://www.wikidata.org/wiki/Property:P236) l'identifiant ISSN,
**[P4730](https://www.wikidata.org/wiki/Property:P4730) appelée identifiant Mir@bel** en français et *Mir@bel journal ID* en anglais et **Mir@belID** en abrégé,
ou encore [P2002](https://www.wikidata.org/wiki/Property:P2002) l'identifiant Twitter.

D'autres propriétés Wikidata sont des simples chaînes de caractères, comme le Titre [P1476](https://www.wikidata.org/wiki/Property:P1476)
et le Sous-titre [P1680](https://www.wikidata.org/wiki/Property:P1680). Elles nous intéressent
pour vérifier la bonne identification de la revue.

### Cron quotidien

La synchronisation quotidienne (par une entrée crontab) fait deux choses :

1. `cronFetchCompare` met à jour les deux tables `Wikidata` et `WikidataIssn` en interrogeant WD
([M#4559](https://tickets.silecs.info/view.php?id=4559))
2. `cronImportWplinks` importe les liens Wikipedia à partir des données de WD qui sont dans la table `Wikidata`
([M#4691](https://tickets.silecs.info/view.php?id=4691))


### Caches Wikidata et WikidataIssn

#### table Wikidata

Le cache Wikidata est stocké dans la table `Wikidata`.
La récupération des données Wikidata se fait pour tous les *éléments* Wikidata qui portent la propriété
**P4730**, qui est l'identifiant **de revue** de Mir@bel (et pas de titre).
Cela représente à ce jour environ 7k Qids et 21k enregistrements.

Les colonnes de la table `Wikidata` se décomposent en deux types :
le cache brut des données extraites par l'API Wikidata et la comparaison avec les données Mir@bel.

* cache brut : colonnes `revueId`, `qId`, `property`, `propertyType`, `value`, `url` , `hdate`
* comparaison : colonnes `titreId`, `comparison`, `compUrl`, `compDetails`, `compDate`

Les propriétés Wikidata récupérées se répartissent en quatre catégories (champ `Wikidata.propertyType` et classe `wikidata/Compare`) :

1.  liens comparable aux lignes de la table `LienTitre`. **Ce sont les seules comparaisons utiles pour le contenu.**
2.  liens comparables aux colonnes de la table `Issn` (notamment P236 ISSN)
3.  liens importés de sources, ex. Cairn (fr), DOAJ
4.  Titres. C'est une propriété chaîne de caractères non comparable.

#### Cache des liens Wikipédia

Pour Wikidata, un lien Wikipédia *n'est pas* une "propriété" mais un attribut interne, qui sert à centraliser
les liens interlangues de Wikipédia.
Un élément de Wikidata "contient" donc les liens vers les articles Wikipédia existants pour cet élément,
un par langue.

On conserve tout de même l'information dans la table `Wikidata` comme s'il s'agissait de propriétés standard :
pour chaque lien Wikipédia, on ajoute à la table un nouvel enregistrement, en renseignant la colonne `property` avec
une pseudo-propriété de la forme `WP:fr`, `WP:en` ...
On récupère les liens de toutes les langues associées à la revue, plus le français et l'anglais.
Le processus est distinct de la récupération des "vraies" propriétés.

**Les liens Wikipédia ne seront pas comparés mais importés ensuite.**

#### table WikidataIssn

La deuxième table `WikidataIssn` va servir à l'export de Mir@bel vers Wikidata des identifiants de revues (Mir@belID).
Elle contient un cache de **tous les éléments Wikidata munis d'une déclaration ISSN** (autour de 200k éléments).
Elle récupère optionnellement les Mi@rabelID (P4730) et les titres (P1476).

C'est un cache brut, sans comparaison avec Mir@bel, contenant les colonnes `qId`, `issn`, `revueId`, `titre`.


### Alignement Mir@bel / Wikidata

La difficulté de base est la diversité des représentations dans Wikidata.
On y trouve des déclarations ISSN (éventuellement multiples, éventuellement représentant des titres successifs),
mais toutes les revues n'en ont pas.

Dans certains cas, *un élément = une revue*, dans d'autres cas moins fréquents, *un élément = un titre*.

On trouve aussi des cas exotiques, par exemple plusieurs éditions (nationales et/ou linguistiques comme Newsweek...)
qui ne sont pas représentées de manière uniforme (un seul élément ou plusieurs).

Quand on remplit le cache Wikidata on se fie à l'identifiant de revue porté par P4730 (Mir@belID).
Les liens-propriétés dans Wikidata sont portées par l'élément Wikidata, qui peut donc être **soit une revue soit un titre**.

#### Détermination du titreId

Dans Mir@bel, les liens sont portés *seulement par des titres*, qui sont identifiés par les ISSN.
Après la mise en cache brute des données Wikidata, on a donc un identifiant de revue (P4730) mais pas d'identifiant de titre.

On détermine quand c'est possible l'identifiant de titre par un post-traitement.
L'heuristique cherche l'intersection entre tous les ISSN déclarés dans Wikidata et tous ceux des titres successifs déclarés dans Mir@bel.

* s'il n'y a qu'un titre commun (cas très majoritaire), c'est correct, on l'inscrit dans la table Wikidata, puis
    * s'il correspond au dernier titre de revue, c'est OK.
    * sinon, on marque un warning **80 "Titre porteur obsolète"**, qui indique soit une lacune dans Mir@bel soit une erreur dans Wikidata (parfois compliquée).
* s'il n'y a aucun titre commun, on marque l'erreur **98 "ISSN introuvable"**, et on supprime toutes les autres comparaisons.
* s'il y a plusieurs titres communs, **TODO CONVENTION TEMPORAIRE** on marque l'avertissement **81 "Titres multiples"**
  et on garde les comparaisons mais avec un titreId nul. Donc ça indiquera toujours "seulement dans Wikidata".

Dans le cas **98 "ISSN introuvable"**, la majorité des cas constatés est un rapprochement manuel abusif dans Wikidata entre 2 revues différentes.
Ce sont souvent des revues de langues différentes (fra et eng par exemple) mais de même titre.
Dans ce cas, la seule correction possible est de supprimer la déclaration **Mir@belID** dans l'élément Wikidata, hors Mir@bel.


#### Analyses complémentaires

Ensuite on lance des analyses complémentaires pour détecter les incohérences globales, de mineures à critiques.

* **99 "Revue disparue"**  Erreur critique.

Comme pour **98**, la seule correction possible est la suppression de la déclaration **Mir@belID** dans Wikidata.

Les autres détections sont des avertissements qui ne sont pas forcément des erreurs, mais tempèrent la fiabilité des comparaisons.
**TODO CONVENTION TEMPORAIRE** on laisse titreId à 0, ce qui neutralise les comparaisons plus précises.

* **81 "Plusieurs titres par revue"** (mineur)
* **82 "Plusieurs Qids par revue"** (moyen)
* **83 "Plusieurs revues par Qid"** (moyen)


### Comparaisons des propriétés

Les comparaisons portent toujours sur des url bien formés (`https://` ou `http://`).
Elles se font systématiquement à partir de la table `Wikidata`, remplissent le champ `comparison` et marquent :

* 01 "Égales"
* 11 "Seulement Wikidata" (avertissement)
* 20 "Différentes" (erreur)

Les comparaisons "différentes" peuvent être des faux positifs pour une grande quantité de raison,
cf. [4581](https://tickets.silecs.info/view.php?id=4581) notamment :

* différence de protocole avec redirection côté serveur (http et https)
* une même url encodée de deux façons différentes
* plusieurs url envoyant vers la même page, dépendant de la logique de l'application ciblée (ex. Twitter est insensible à la casse)

Il est difficile (voire très difficile) de les traiter. Imposer une forme canonique à la saisie est une autre piste possible.


NB. Les comparaisons suivantes initialement prévues ne sont pas réalisées pour l'instant :

* 10 "Seulement Mirabel"
* 30 "Multivaluée"


Affichages web des informations et comparaisons
-----------------------------------------------

### verification/wikidatabugs

Le lien [Accueil > Vérification des données > Wikidata / Incohérences](/verification/wikidatabugs) n'est affiché
que pour les administrateurs, mais la page est visible par toute personne authentifiée car il n'y a pas d'information critique.

La page indique les incohérences globales.
Elle est plutôt conçue pour des opérateurs qui ont une bonne connaissance de Wikidata,
afin de déterminer si l'erreur provient de Mir@bel ou de Wikidata.

### verification/wikidata

Le lien [Accueil > Vérification des données > Wikidata](/verification/wikidata) est affiché pour tous les utilisateurs.
La page est conçue comme une grande récapitulation triable par propriété et par résultat de comparaison.

Les comparaisons affichées sont "Différentes", "Seulement Wikidata" ou "Égales".
Dans les deux premiers cas, un bouton "crayon" envoie l'utilisateur vers le formulaire *Modifier le titre > Liens extérieurs*.

### revue/checkLinks/

Pour un utilisateur qui suit une revue, la dernière section de la page *Vérification des liens et données Wikidata*
est consacrée à Wikidata ([exemple](/revue/checkLinks/730#revue-wikidatabox)) et elle est constituée de deux blocs.

Le bloc *Comparaison Wikidata / Mir@bel* est toujours affiché, avec les comparaisons "Différentes", "Seulement Wikidata" ou "Égales".

Le bloc *Informations Wikidata* est dépliable.
Il ne fournit pas de comparaison mais il est très utile pour évaluer la fiabilité globale de la correspondance Wikidata/Mir@bel.


Import des liens Wikipédia
--------------------------

### Procédure normale d'import

Les liens Wikipédia sont importés pour toutes les langues du cache Wikidata dans Mir@bel
par la commande `./yii wikidata importWplinks`.
Ajouter `--limit=500` pour tester l'import à plus petite échelle.

On a défini autant de sources différentes dans `Sourcelien` que de langues disponibles dans Wikipedia pour les revues.
Le processus d'import est relié à l'import `Wikidata` défini dans la classe `ImportType`,
qui est associé à toutes les sources nommées `wikipedia-XX` identifiées par leur champ `Sourcelien.nomcourt`.

Le processus affiche un log CSV des liens ajoutés et modifiés.
On peut également compter les ajouts en lançant la commande `./yii wikidata wplinksStats` avant et après l'import,
mais c'est limité aux liens *ajoutés*.

### Cas particuliers

Dans des cas très rares, Wikidata référence une page Wikipédia qui est une redirection (vers une page ou vers une section) :

* <https://reseau-mirabel.info/revue/8588/Revue-d-histoire-de-Nimes-et-du-Gard-RHNG>
* <https://fr.wikipedia.org/w/index.php?title=Revue_d%27histoire_de_N%C3%AEmes_et_du_Gard&redirect=no>

Le problème est que Wikidata ne sait pas que c'est une page Wikipédia spéciale.
Une évolution possible serait de supprimer automatiquement tous les liens qui sont des redirections internes.
[Ticket 4545](https://tickets.silecs.info/view.php?id=4545#c18111)


Exports de Mir@bel vers Wikidata
--------------------------------

### Export "one-shot" par Quickstatements

L'export ponctuel par Quickstatements repose sur la deuxième table `WikidataIssn`.

#### En ligne de commande
Le lancement peut s'effectuer en ligne de commande pour un contrôle complet.

```
./yii wikidata exportFillTable --limit=999000  #30 s d'exécution environ
./yii wikidata exportQuickstatements
```

L'utilisateur obtient un fichier `quickstatements.csv` et 3 fichiers de logs csv :
`log-multiple-revues.csv` (warning), `log-multiple-titres.csv` (info), `log-multiple-elements.csv` (info).
Les logs "warning" correspondent aux imports supprimés du fichier Quickstatements.


#### Par l'interface web

Il n'y a pas d'interface web intégrée, mais les liens suivants fournissent le fichier principal
et les cas particuliers "dégénérés" à vérifier manuellement de préférence.

* [quickstatements](/revue/exportToWikidata?target=quickstatements) le fichier principal
* [log-multi-revues](/revue/exportToWikidata?target=log-multi-revues) plusieurs **revues** Mir@bel pour un seul Qid
* [log-multi-titres](/revue/exportToWikidata?target=log-multi-titres) : plusieurs **titres** Mir@bel pour un seul Qid
* [log-multi-elements](/revue/exportToWikidata?target=log-multi-elements) : plusieurs **Qid** pour une seule revue (MirabelID) ; possibles doublons dans Wikidata

#### Import dans Wikidata

L'utilisateur peut contrôler et éventuellement modifier le fichier obtenu (suppression de lignes...) puis
utiliser l'interface web [QuickStatements2](https://quickstatements.toolforge.org/#/batch).

* Bouton `Nouveau lot`
* Créer une nouvelle commande de lot pour `Wikidata` avec le nom `A_REMPLIR`
* Copier-coller le contenu de `quickstatements.csv` (pas d'import de fichier dans QS2)
* Bouton `Importer des commandes dans le format V1`
* L'interface QS2 affiche un prétraitement des données -> bouton `Lancer`
