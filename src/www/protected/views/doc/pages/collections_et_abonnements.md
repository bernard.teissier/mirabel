Collections et abonnements
==========================

Collections et accès
--------------------

* Le **type d'une collection** est parmi :

	- courant (défaut)
	- archive
	- temporaire (à la création d'une collection normale, on stocke les accès existants dans une collection temporaire)
	- partenaire (pas utilisé pour le moment)

* Si le type n'est pas *partenaire*,
  la collection est obligatoirement rattachée à une ressource.

* Chaque **accès** en ligne est rattaché à une ressource,
  et facultativement à des collections.
  Un même accès peut donc appartenir à plusieurs collections d'une ressource.

* Dans une ressource ayant des collections,
  tous les accès sont rattachés à au moins une collection.

	* Les collections *temporaires* ont été conçues pour maintenir à cet état, à la création d'une première collection.
	  Tous les accès pré-existant sont déplacés dans cette collection "Par défaut".
	* Les formulaires de création et modification d'accès imposent d'utiliser au moins une collection (s'il en existe dans la ressource).
	* L'import impose l'usage d'une collection si la source n'a pas cette information (colonne "Collection" etc).

* Une collection *importée* (champ SQL `importee`) verrouille la modification de ses accès en ligne.

### Remarques

Si les accès doivent varier suivant la collection, alors il ne faut pas utiliser des collections mais des ressources.

Par exemple, pour Cairn, un titre appartenant à 3 bouquets de Cairn,
avec dans chaque cas un accès libre et un restreint,
sera représenté par 2 accès (et non plus 2×3).
Chaque accès sera rattaché aux 3 collections (bouquets Cairn).
Modifier un de ces accès changera donc les données pour les 3 bouquets Cairn.

A contrario, pour EbscoHost, les états de collections sont variables.
On utilisera donc un ressource "EbscoHost - …" pour chaque source,
et donc un même titre pourra avoir plusieurs accès, éventuellement similaires,
mais rattachés à des collections différentes, et donc pouvant évoluer de façon indépendante.
Modifier un de ces accès changera donc les données pour la seule ressource "EbscoHost - …" concernée.


Abonnements
-----------

* Un partenaire peut s'abonner à une collection.

* Si une ressource ne contient aucune collection,
  alors on peut s'y abonner (collection maîtresse|globale).

* Si le partenaire a rempli une URL de proxy (champ textuel `Partenaire.proxyUrl`),
  alors l'abonnement l'utilise par défaut (champ booléen `Abonnement.proxy`).
  La page d'une ressource permet de désactiver le proxy, collection par collection.

* Depuis avril 2018, un partenaire peut définir aussi une URL de proxy spécifique à un abonnement.
  Dans ce cas, ce proxy sera appliqué aux accès en ligne abonnés,
  avec ensuite un second niveau de proxy si le proxy global du partenaire s'applique à la collection.

Affichage synthétique des accès
-------------------------------

Par défaut, les accès sont affichés de façon synthétique ; ils sont regroupés en cas de similitude.
En mode "vue détaillée", aucun regroupement ne s'applique, mais les accès sont triés.

L'algorithme actuel est le suivant :

1. Trier les accès bruts :

	- par contenu (texte intégral, etc)
	- par diffusion (libre, etc)
    - si égalité, tri par date de début décroissante (les plus récentes d'abord)

2. En vue regroupée, ajouter un par un les accès triés,
  sauf si le triplet (ressource, URL, type) a déjà été rencontré pour un *accès de même degré de liberté ou que les deux accès sont non-restreints*.
  En ce cas, on modifie (visuellement, pas dans la base) le premier accès et on ignore le nouveau :

	- si un des accès a le statut *abonné*, ce statut est prioritaire ;
	- si la date de début du nouvel accès est antérieure,
      date, volume et numéro de débuts sont repris ;
	- idem pour la date de fin. 

3. Trier pour l'affichage les accès (qui peuvent être des accès bruts ou des fusions d'accès) :

	- texte intégral, puis résumé, puis sommaire, puis indexation
	- si égalité, priorité à l'accès abonné, puis libre, puis restreint
    - si égalité, tri par date de fin décroissante (d'abord les accès sans date de fin, puis des dates récentes aux anciennes).

Par exemple, pour les accès suivants :

```
Ressource      URL             Type      Liberté     Début    Fin
-------------  --------------  -----     ---------  -------  -------
Cairn          cairn.info/A    TexteInt  Abonné     2007     2016
Cairn          cairn.info/A    TexteInt  Libre      2000     2006
Cairn Mag      cairn.info/A    Sommaire  Libre      1990     2016
```

L'accès Cairn Mag ne pourra être fusionné, pour cause de différence de type et de ressource.
Par contre, les deux premiers accès seront fusionnés.

```
Ressource      URL             Type      Liberté     Début    Fin
-------------  --------------  -----     ---------  -------  -------
Cairn          cairn.info/A    TexteInt  Abonné     2000     2016
Cairn Mag      cairn.info/A    Sommaire  Libre      1990     2016
```

### Remarques

1. À terme, des accès d'URL différentes pourraient être fusionnés.
  Le mécanisme actuel le permet (mais ne le fait pas).
