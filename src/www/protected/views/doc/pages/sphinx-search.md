Sphinx Search
=============

C'est le moteur de recherche en texte intégral utilisé en complément de MySQL.
Ses données sont périodiquement synchronisées avec celles de la base de données.


Documentation officielle
------------------------

Pour la version 2.1.X : <http://sphinxsearch.com/docs/archives/2.1.9/>
avec notamment la [syntaxe de recherche](http://sphinxsearch.com/docs/archives/2.1.9/extended-syntax.html).


Principe général
----------------

* Un démon Sphinx nommé `searchd` tourne sur le serveur.
* Il contient plusieurs *index* que l'on peut interroger (comme des tables SQL).
* Ces index sont recréés par la commande `indexer` de Sphinx à partir de requêtes SQL.
* Le fichier `sphinx.conf` donne ces requêtes SQL et décrit la nature des champs
  (texte intégral, date, multi-valué, etc).

Les requêtes SQL dénormalisent les données pour les préparer à la recherche.


Utilisation dans Mir@bel
------------------------

* Il y a une seule instance de Sphinx pour toutes les instances Mir@bel du serveur.
* Cette instance est dans `/srv/sphinx/mirabel`, pointée par
  un lien symbolique `sphinx` est dans le répertoire principal de l'utilisateur `mirabel`.
* La connexion se fait par port TCP ou socket UNIX,
  cf lignes `listen` de `/srv/sphinx/mirabel/etc/sphinx.conf`.

### Configuration

* `/srv/sphinx/mirabel/etc/sphinx.conf`.
* Ce fichier est généré par un script, mieux vaut ne pas le modifier.
* Pour générer la config de la prod :

		cd /www/mirabel2/mirabel2-prod/
		php src/www/protected/yiic.php sphinx conf > src/sphinx/sphinx.conf

* Pour ajouter la configuration d'une autre instance, par exemple devel :

		cd /www/mirabel2/mirabel2-devel/
		php src/www/protected/yiic.php sphinx confLocal >> /www/mirabel2/mirabel2-prod/src/sphinx/sphinx.conf

Le script utilise le fichier de configuration PHP `config/local.php` de l'instance,
notamment les blocs concernant MySQL et Sphinx.


### Maintenance du démon

#### Démarrer/arrêter le démon

	cd /srv/sphinx/mirabel
	/opt/sphinx/bin/searchd -c /srv/sphinx/mirabel/etc/sphinx.conf
	/opt/sphinx/bin/searchd -c /srv/sphinx/mirabel/etc/sphinx.conf --stopwait

#### Mettre à jour les index (avec les données de MySQL)

	cd /srv/sphinx/mirabel
	/opt/sphinx/bin/indexer -c /srv/sphinx/mirabel/etc/sphinx.conf --rotate --all

#### Tester Sphinx en SQL

	mysql --host=localhost --socket=/www/mirabel2/sphinx/runtime/sphinx.socket --protocol=SOCKET
	SHOW TABLES ;
	DESC devel_titres ;
	SELECT * FROM devel_titres WHERE acces = 2 LIMIT 10 ;
	SELECT id, titrecomplet FROM devel_titres WHERE MATCH('international') ;
	quit

#### Autres opérations

	cd /srv/sphinx/mirabel
	/opt/sphinx/bin/searchd --help


### Détails très techniques

Les templates de `sphinx.conf` utilisés par le générateur sont dans `src/www/protected/commands/views/sphinx/`.

Le format des indexes est décrit dans le dépôt par `doc/sphinx.md`.

Le code PHP utilise une extension en PHP, dérivée de celle de Sign@l (et rétroportée ensuite),
qui est une surcouche Yii sur le fichier officiel `sphinxapi.php`.
Cf `src/www/protected/extensions/Sphinx`.
Cela permet notamment d'utiliser une classe dérivée de `CProviderData` comme pour l'ORM MySQL.

À l'installation, il faut au minimum Sphinx 2.1.0.
Le code n'a pas été testé avec Sphinx 2.2.
Si Sphinx est compilé, il faut lui ajouter la biblio de lexématisation du français. Par exemple :

	sudo aptitude install autoconf automake libmysql-dev g++
	cd sphinx_src
	wget http://snowball.tartarus.org/dist/libstemmer_c.tgz
	tar xf libstemmer_c.tgz
	# si le code ne vient pas d'une archive, mais du VCS
	# ./buildconf.sh 
	./configure --with-libstemmer --with-static-mysql
	make
	cp src/indexer src/searchd ../../src/sphinx/bin/

#### Exemple de service systemd

	[Unit]
	Description=Sphinx-Mirabel
	After=local-fs.target network.target

	[Service]
	Type=simple
	Restart=always
	PIDFile=          /srv/sphinx/mirabel/runtime/searchd.pid
	WorkingDirectory= /srv/sphinx/mirabel
	ExecStart=        /opt/sphinx/bin/searchd --config etc/sphinx.conf --nodetach
	ExecStop=         /opt/sphinx/bin/searchd --config etc/sphinx.conf --stopwait
	User=mirabel

	[Install]
	WantedBy=multi-user.target
