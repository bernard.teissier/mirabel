# Service REST dans Mir@bel

## 1 Comment interroger le service

Mir@bel fournit une API REST très simple pour interroger les revues de la base, accessible à l'adresse suivante :
<http://reseau-mirabel.info/site/service>

Toute requête d'interrogation est au format GET. Elle se compose d'un filtre principal qui permet de filtrer les titres de revues et de critères complémentaires (optionnels) qui s'appliquent principalement aux accès en ligne.

Un exemple de client interrogeant ce webservice pour alimenter Koha est disponible :
<https://github.com/silecs/mirabel-koha>


### 1.1 Liste des services supprimés

`suppr=<YYYY-MM-DD>`
:   Renvoie la liste des identifiants numériques des services supprimés depuis cette date.
    Ce critère n'est cumulable avec aucun autre (ils seront ignorés silencieusement).

### 1.2 Filtres principaux

Le filtre principal est :

`partenaire=<identifiant du partenaire dans mir@bel>`
:   Renvoie toutes les revues et tous les services de ce partenaire.  
    Ce filtre peut être associé à un autre filtre "issn", "issnl", "issne" ou "ressource".
    Si ce filtre est absent, les revues n'auront pas de champ pour l'identifiant local au partenaire.
	Ce critère n'est donc pas uniquement un critère de sélection.

D'autres paramètres de sélection de revues sont disponibles, indépendamment du précédent :

`issn=<texte>` ou `issnl=<texte>` ou `issne=<texte>`
:   Renvoie les informations pour une revue suivant son issn papier, de liaison ou électronique.  
    Le format attendu est « 1234-1234 ».

`ressource=<identifiant(s) de la ressource dans Mir@bel>`
:   Renvoie toutes les revues associées à la ressource (ex-fournisseur).  
    Pour transmettre plusieurs identifiants dans la même requête,
	on peut utiliser la syntaxe `ressource[]=1&ressource[]=2`,
	ou plus simplement `ressource=1,2`

`collection=<identifiant(s) de la collection dans Mir@bel>`
:   Renvoie toutes les revues associées à la collection.  
    Par exemple, `collection=4,11`.

Si aucun filtre n'est donné, alors toutes les revues figureront dans la liste.

### 1.3 Critères supplémentaires d'interrogation

Il est possible d'élargir la sélection à l'ensemble des titres de même revue.
Par exemple, la sélection d'un titre par ISSN sera élargie aux titres d'ISSN différents, mais rattachés à la même revue.

`revue=1`
:   si ce paramètre est présent, la liste de titres sera étendue aux revues.

D'autres critères sont disponibles pour filtrer au niveau des accès en ligne :

`type=<valeur>`
:   type de service, avec <valeur> parmi integral ; sommaire ; resume ; indexation

`acces=<valeur>`
:   type d'accès, avec <valeur> parmi libre ; restreint

`selection=<0|1>`
:   Filtre par ce type de couverture.
    Attention, `selection=0` limitera aux accès qui ne sont pas qu'une sélection d'articles ; ce n'est pas équivalent à l'absence du critère `selection`.

`lacunaire=<0|1>`
:   Filtre par ce type de couverture.

### 1.4 Exemples d'interrogation

`?issn=0335-5322`
:    Cette interrogation renvoie le titre d'issn 0335-5322 ainsi que tous les services qui lui sont associés.

`?issn=0767-4775&revue=1&partenaire=2`
:    Cette interrogation renvoie non seulement le titre d'issn 0767-4775, mais aussi les titres d'ISSN 1769-101X et 0249-3756 qui sont dans la même revue. Tous les accès en ligne associés à ces titres sont décrits. Pour chaque titre, la réponse précise l'identifiant local associé au partenaire d'identifiant 2.

`?partenaire=3&type=integral&acces=restreint`
:   Cette interrogation renvoie toutes tous les titres et tous les accès en ligne de type « texte intégral » et d'accès « restreint » du partenaire d'identifiant 3 (« ENS de Lyon »).

`?issn=0335-5322&selection=0`
:   Cette interrogation renvoie le titre d'issn 0335-5322 ainsi que tous les services qui lui sont associés qui ne sont pas qu'une sélection d'articles. La revue ne sera pas renvoyée si aucun service ne répond à l'interrogation (i.e. balise `<revues>` vide).

## 2 Réponse du service REST

### 2.1 Réponse normale

Mir@bel fournit une réponse au format XML sous la forme suivante
(les champs entre `<!--` et `-->` sont des commentaires de l'exemple qui ne figurent pas dans la véritable réponse).
L'élément racine est `<revues>`.

~~~xml
<revues>
    <revue>
        <!-- si filtre "partenaire" présent, le champ suivant contient
             l'identifiant local de ce titre pour ce partenaire dans Mir@bel  -->
        <idpartenairerevue>5</idpartenairerevue>
        <issn>2-07-040268-1</issn>
        <url>http://example.com</url>
        <idmirabel>123</idmirabel>
        <services>
            <service>
                <id>identifiant SQL du service</id>
                <nom>nom du service</nom>
                <acces>libre ou restreint</acces>
                <type>type de service</type>
                <couverture>Description à partir des 2 champs suivants</couverture>
                <lacunaire/>
                <selection/>
                <urlservice>http://example.com/serviceA/</urlservice>
                <urldirecte>http://example.com/serviceA/12</urldirecte>
                <debut>2000-12</debut>
                <fin>2005-05</fin> <!-- valeur vide quand c'est en cours -->
            </service> <!-- suivie éventuellement d'autres services -->
        </services>
    </revue><!-- suivie éventuellement d'autres revues -->
</revues>
~~~

L'élément `<revue>` apparaîtra uniquement si l'interrogation REST lui associe des éléments `<service>`.

### 2.2 Liste des accès en lignes supprimés

Exemple de réponse à la requête `?suppr=2010-01-01` :

~~~xml
<services_suppr>
   <service>1236</service>
   <service>1723</service>
</services_suppr>
~~~

### 2.3 Messages d'erreur

Autre réponse possible en cas d'erreur dans le traitement de la requête (normalement lors de la validation des paramètres), avec un élément racine `<erreurs>` :

~~~xml
<erreurs>
	<msg critere="acces">Critère invalide</msg>
	<msg critere="partenaire">Ce paramètre doit être un nombre entier</msg>
</erreurs>
~~~

### 2.4 Exemple réel

L'interrogation `?partenaire=3&type=texte&acces=restreint` renvoie le résultat suivant (tronqué) :

~~~xml
<revues>
    <revue>
        <issn>0035-2950</issn>
        <url>http://www.afsp.msh-paris.fr/publi/rfsp/rfsp.html</url>
        <idmirabel>25</idmirabel>
        <idpartenairerevue>212646</idpartenairerevue>
        <services>
            <service>
                <id>5186</id>
                <nom>Cairn</nom>
                <acces>Restreint</acces>
                <type>Intégral</type>
                <couverture>Sélection d'articles</couverture>
                <selection/>
                <urlservice>http://www.cairn.info</urlservice>
                <urldirecte>http://www.cairn.info/revue-fr-science-po.htm</urldirecte>
                <debut>2009</debut>
                <fin>2012</fin>
            </service>
        </services>
    </revue>
    <revue>
        <issn>0008-0055</issn>
        <url>http://www.editions.ehess.fr/revues/cahiers-detudes-africaines/</url>
        <idmirabel>644</idmirabel>
        <idpartenairerevue>test-ens</idpartenairerevue>
        <services>
            <service>
                <id>1326</id>
                <nom>Cairn</nom>
                <acces>Restreint</acces>
                <type>Texte Intégral</type>
                <couverture>0</couverture>
                <urlservice>http://www.cairn.info</urlservice>
                <urldirecte>
                    http://www.cairn.info/revue-cahiers-d-etudes-africaines.htm
                </urldirecte>
                <debut>2006-00</debut>
                <fin>2010-00</fin>
            </service>
            <service>
                <id>2668</id>
                <nom>Revues.org</nom>
                <acces>Restreint</acces>
        	   <type>Texte Intégral</type>
                <couverture>Lacunaire - Sélection d'articles</couverture>
                <lacunaire/>
                <selection/>
        	   <urlservice>http://www.revues.org/</urlservice>
        	   <urldirecte>http://etudesafricaines.revues.org</urldirecte>
        	   <debut>2008-00</debut>
        	   <fin>2004-00</fin>
            </service>
        </services>
    </revue>
	...
</revues>
~~~

<style type="text/css">
#content { max-width: 100ex; margin: 3px auto; }

code { background-color: #E5E5F5; padding: 1px 2px; }
dl { margin-left: 2em; }
dt { font-weight: bold; }

/*
Tables have borders
*/
table { border-collapse: collapse; }
th, td { border: 1px solid black; padding: 3px 2ex 3px 1ex; }

/*
TOC
*/
.toc { border: 1px dotted red; }
.toc li { list-style: none; }
.toc li a { text-decoration: none; }
.toc > ul > li > ul > li:before { counter-increment: toc_chno; content: counter(toc_chno, upper-latin) ". "; counter-reset: toc_secno;  } /* h2 */
.toc > ul > li > ul > li > ul > li:before { counter-increment: toc_secno; content: counter(toc_chno, upper-latin) "." counter(toc_secno) ". "; counter-reset: toc_divno; }
.toc > ul > li > ul > li > ul > li > ul > li:before { counter-increment: toc_divno; content: counter(toc_chno, upper-latin) "." counter(toc_secno) "." counter(toc_divno) ". "; }
.toc > ul > li > ul > li > ul > li > ul > li > ul { display: none; }
.toc > ul > li > a { display: none; }
.toc > ul > li > ul > li > a { font-weight: bold; }

/*
Code blocks
*/
pre > code { background-color: inherit; }
pre,
div.codehilite { margin-left: 2em; padding-left: 2ex; border-left: 1px solid green; }
</style>
