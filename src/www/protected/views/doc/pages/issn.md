# ISSN

Voir aussi la doc [Sudoc et issn](sudoc_et_issn.md).

## Validation de syntaxe

Par défaut, sont autorisés :

 * `""`, un issn vide
 * `"en cours"`
 * un ISSN complet `"1234-5678"` avec tiret central et clé finale.

Tout ceci est paramétrable dans le code.


## Validation d'unicité

* Tout ISSN vide ou "en cours" est valable.
* Un ISSN (de tout type) ne doit pas figurer
  comme ISSN-E pour une autre revue, ou comme ISSN ou ISSN-L pour un autre titre.
* L'ISSN et ISSN-E d'un titre sont distincts.


## Organisation du code

Tout est dans la classe `IssnValidator` placée dans `src/www/extensions/validators/`.
Les différents attributs permettent de configurer le validateur.

Au niveau d'un modèle Yii, on ajoute des règles de validation.
Par exemple pour `Titre` :

```
[php]
public function rules()
{
	return array(
		array('issn', 'ext.validators.IssnValidator', 'unique' => true, 'type' => 'ISSN'),
		array('issnl', 'ext.validators.IssnValidator', 'unique' => true, 'type' => 'ISSN-L'),
```

Les constantes de types peuvent aussi s'écrire `IssnValidator::TYPE_ISSN`.
