Instances de Mir@bel
====================

Fonctionnement général
----------------------

* Deux branches dans le code :

	* *master* où apparaissent toutes les nouveautés,
	* *production* où sont versés par à-coup les développements validés.
	  Les tickets Mantis passent alors en état `fermé`.

* La synchronisation est à plusieurs niveaux

	* code PHP
	* contenu (données MySQL et fichiers déposés par le web)

### Synchronisation du code

Chaque mise à jour du code est automatiquement suivie (par un *hook Git*) des étapes suivantes :

* mise à jour de la base de données, si la structure a changé
* indexation des données MySQL pour la recherche par Sphinx
* vérification des permissions d'accès aux fichiers déposés
* insertion du numéro de version du code dans les pages web

Cf scripts shell `.git/hooks/post-*` dans les différentes instances de Mir@bel.
Les scripts varient légèrement suivant l'instance (prod ou devel, etc).

### Synchronisation des données

Pour appliquer les données de l'instance de prod dans une autre instance (SQL et fichiers),
le plus simple est d'utiliser la commande `make synchro` depuis la racine de l'instance cible :

    cd mirabel-demo && make synchro

### Robots

Sur l'instance de production, les pages de modification (URL en `*/update`) sont exclues.
Sur les autres instances, le `robots.txt` demande que rien ne soit indexé par les bots.



Liste des instances
-------------------

### 1. Production

* <https://reseau-mirabel.info/>

* Branche git : production (développements basculés après validation)

* Synchronisation : Le code est mis à jour lorsque des développements passent en production.

* Imports automatiques : Par cron quotidien, avec envoi de courriels à la personne en charge de la ressource dans Mir@bel.

Le **cron** se configure dans le compte "mirabel" du serveur, avec `crontab -e`,
notamment les horaires et les adresses email d'envoi.

Les propositions de modification sont envoyées par courriel quotidien à minuit.
Les rappels de propositions non traitées ayant exactement 10 jours sont envoyés
quotidiennement à 10h (pour les éventuels rappels à 30 jours un envoi à 14h).


### 2. Test — développement

* <https://mirabel-devel.sciencespo-lyon.fr/>

* Branche git : master

* Synchronisation : Code source à chaque développement publié.  
   La synchronisation des données venant de la production est à la demande
   (simple `make synchro` dans le répertoire).


### 3. Démo

* <https://mirabel-demo.sciencespo-lyon.fr/>

* Branche git : production

* Synchronisation : Hebdomadaire (dimanche matin), code et données de la production.


### 4. Import de masse

* <https://mirabel-import.sciencespo-lyon.fr/>

* Branche git : production

* Synchronisation : Code, à chaque mise à jour de la production.
  Données, à la demande


### 5. Temporaire (collections)

* <https://mirabel-tmp.sciencespo-lyon.fr/>

* Branche git : selon le besoin temporaire




