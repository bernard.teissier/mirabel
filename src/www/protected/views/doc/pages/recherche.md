La recherche dans Mir@bel
=========================

Interface web pour les recherches
---------------------------------

### Recherche de revues

Le formulaire de `/revue/search` donne accès à une partie des filtres possibles.

Les paramètres scalaires autorisés (cd fichier `SearchTitre.php`) :

- titre
- langues
- issn
- hdateModif (par exemple, "2016" ou "<2012-04")
- hdateVerif
- collectionId
- paysId
- categoriesEt (par défaut, "non", donc on cherche les revues ayant une des catégories demandées)
- sanscategorie (par défaut, "non")
- vivant
- accesLibre
- sansAcces
- abonnement
- owned = false;
- monitoredByMe = false;
- monitored = false;
- monitoredNot = false;

Certains des paramètres ci-dessus ne sont accessibles qu'aux personnes identifiées,
ou à celles qui ont un établissement actif.
Par exemple `q[owned]=1` est équivalent à `q[detenu]=ID` où `ID` est l'identifiant numérique
de l'établissement actif (cf menu déroulant dans la page).

D'autre paramètres sont en tableau, par exemple `?q[editeurId][]=3&q[editeurId][]=4`.
Certains acceptent aussi une valeur tableau ou scalaire, par exemple `?q[detenu]=5`.

- editeurId
- ressourceId
- categorie
- cNonRec (categorie sans récursivité)
- suivi (IDs de partenaires)
- detenu (IDs de partenaires)
- acces


Organisation technique : MySQL et Sphinx
----------------------------------------

Mir@bel n'interroge pas directement la base de données SQL (cf [schéma PDF](Mirabel_SQL-diagram.pdf)).
Un cron importe périodiquement les données dans Sphinx-Search (cf doc technique de Sphinx).
Cette page décrit la structure *cherchable*, correspondant au fichier de configuration de Sphinx.

Pour chaque *table* de Sphinx, certaines colonnes sont utilisées pour la recherche en texte intégral,
d'autres sont des listes de valeurs,
et enfin certains champs fonctionnent plus comme des formats SQL classiques.

Par défaut une recherche en texte intégral couvre tous les champs textuels.


Sphinx : structure de données
-----------------------------

Ceci concerne surtout les développeurs,
où les administrateurs qui interrogent directement la base de données de Sphinx.

### titres

- champs textuels (pour la recherche en texte intégral)
	- sigle
	- titrecomplet
	- langues
- champs numériques (pour filtrer ou regrouper les résultats)
	- nbacces : nombre d'accès en ligne
	- revueid : sert à regrouper les titres par revues
	- lettre1 : la lettre initiale du titre, pour grouper par page "&0", "A", "B", etc
	- cletri
- booléens (vrai/faux)
	- obsolete
- champs multi-valués (listes pour filtrer)
	- editeurid
	- ressourceid
	- collectionid
	- paysid : liste des identifiants des pays des éditeurs du titre
	- suivi : identifiants des partenaires suivant la revue
	- detenu : identifiants des partenaires détenant la revue
	- acces : liste des types d'accès possibles (résumé, sommaire, etc)
	- acceslibre : idem *acces*, mais restreint aux accès libres
	- issn : liste des ISSN (de toutes sortes)
	- categorie : categorieId associé soit via la revue soit par un alias sur ce titre
	- revuecategorie : categorieId associé soit via la revue soit par un alias sur un des titres de la revue
- dates/heures
	- hdatemodif
	- hdateverif

### ressources

- champs textuels
	- sigle
	- nomcomplet
	- description
	- notecontenu
	- disciplines
- champs numériques
	- nbrevues
	- lettre1 : la lettre initiale du titre, pour grouper par page "&0", "A", "B", etc
	- cletri
- booléens (vrai/faux)
	- autoimport
	- web
- champs multi-valués (listes)
	- suivi : identifiants des partenaires suivant la ressource
- dates/heures
	- hdatemodif
	- hdateverif

### editeurs

- champs textuels
	- sigle
	- nomcomplet
	- description
	- geo
- champs numériques
	- nbrevues
	- paysid : identifiant du pays
	- lettre1 : la lettre initiale du titre, pour grouper par page "&0", "A", "B", etc
	- cletri
- champs multi-valués (listes)
	- suivi : identifiants des partenaires suivant l'éditeur
- dates/heures
	- hdatemodif
	- hdateverif
