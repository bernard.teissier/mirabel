Sherpa-RoMEO
============

Auparavant, chaque navigateur consultant la page d'une revue
émettait une requête AJAX vers l'API v1 de Sherpa.
Ce mécanisme a disparu, ainsi que l'ancienne API de Sherpa.
Désormais les données sont stockées par Mir@bel
et synchronisées grâce au cron.

Import global (périodique)
-------------

L'import est réalisé tous les mercredi matin via le cron:
```
24 04 * * 3   ./yii sherpa import ...
```
Cf doc [Liens et imports de liens](liens_et_imports_de_liens.md) pour l'explication du script.

Il s'applique à tous les titres de Mir@bel qui ont au moins un ISSN.
Une requête est envoyée à l'API v2 de Sherpa pour chaque ISSN.
Chaque requête à l'API est signée par une clé spécifique pour Mirabel.

Les données stockées par Mir@bel sont :

- le lien vers Sherpa, enregistré parmi les liens du Titre via une intervention ;
- la réponse JSON complète dans `src/www/protected/runtime/sherpa/<ISSN>.json` ;
- un résumé de la politique éditoriale dans la table MySQL `RomeoPolicy`,
  avec les champs "submitted, accepted, "published".
  Ce résumé n'est plus utilisé pour le moment.
  Il sera peut-être supprimé.

Pour en savoir plus, consulter le [ticket 3117](https://tickets.silecs.info/view.php?id=3117).


Interrogation ciblée (web)
--------------------

Certains événements déclenchent des actions liées à Sherpa :

- Lors de la création ou la modification d'un titre de revue par l'interface web,
  Mirabel ajoute un lien vers Sherpa.
  L'utilisateur est prévenu par un message sur la page qui suit la soumission du formulaire.
- Lorsqu'une intervention est acceptée dans l'interface web,
  Mirabel ajoute un lien vers Sherpa au titre créé ou modifié ainsi.
  L'intervention est complétée an arrière-plan avant son application et son enregistrement.
  Ceci s'applique aux interventions venant de l'import.

Au final, toute création de titre, que ce soit manuelle ou par un import KBART web ou cron,
aura automatiquement un lien vers Sherpa, si Sherpa reconnaît son ISSN.

En plus du lien vers Sherpa, les données détaillées sont stockées, comme pour l'import global.

Pour en savoir plus, consulter le [ticket 4574](https://tickets.silecs.info/view.php?id=3117)
Le code PHP correspondant est dans `models/sherpa/Hook.php`.


Affichage web
-------------

### Lien

Le lien Sherpa associé au titre est affiché dans le bloc "Politique de publication",
au-dessus des "Autres liens".

Il est suivi d'un lien vers la version française hébergée par Mir@bel, décrite ci-dessous.

### Page de la politique de publication

En mode "production", chaque page web `/titre/publication/XYZ` est conservée dans un
**cache côté serveur** pendant 1 heure.

Le contenu de `/titre/publication/[ID-de-titre]` est généré à partir du JSON reçu de Sherpa.
La *vue* qui affiche les données en HTML est dans `src/www/protected/views/titre/publication.php`.
C'est ce fichier qu'il faut éditer pour modifier un titre, etc.

Un mécanisme de traduction existe sous la forme de tableaux "msg EN;msg FR",
dans la page de configuration réservée aux administrateurs.

Pour en savoir plus, consulter le [ticket 4558](https://tickets.silecs.info/view.php?id=4558).
