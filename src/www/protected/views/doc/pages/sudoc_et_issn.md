Sudoc et issn
=============

Mirabel utilise deux web-services du Sudoc :

1. [issn2ppn](http://documentation.abes.fr/sudoc/manuels/administration/aidewebservices/index.html#issn2ppn)
2. [SudocMarcXML](http://documentation.abes.fr/sudoc/manuels/administration/aidewebservices/index.html#SudocMarcXML)


Filouterie en attendant 2021
----------------------------

Les données annexes aux numéros ISSN sont retirés des réponses MARC du Sudoc,
mais pas des réponses RDF, pourtant construites à partir du MARC.
Pour remplir les champs de dates et de langue dans les cas où un ISSN est présent,
Mir@bel complète la réponse MarcXML avec celle en RDF.
Ce contournement prendra fin en janvier 2021 avec le changement de la politique ISSN.


Notice locale Sudoc construite dans Mirabel
------------------------------

Que l'on parte d'un ISSN ou d'un PPN, Mirabel interrogera le Sudoc
pour construire une notice locale contenant toutes les données utiles à Mirabel.

Pour visualiser la notice Mirabel déduite des données du Sudoc,
on peut interroger directement l'API web utilisée par le Javascript de Mirabel (cas 1 et 2 ci-dessus).
Par exemple [issn=2104-1881](https://mirabel-devel.sciencespo-lyon.fr/titre/ajaxSudoc?issn=2104-1881)
ou [ppn=139611983](https://mirabel-devel.sciencespo-lyon.fr/titre/ajaxSudoc?ppn=139611983).
(Le site mirabel-devel renvoie un JSON indenté, contrairement au site principal).


Utilisation de la notice locale Sudoc dans Mir@bel
-------------------------------------

Cette notice Mirabel (construite à partir de la notice MARC du Sudoc) est utilisée :

1. lors de la création de titre par ISSN.
   Le titre initialisé avec `titre`, `url`, `dateDebut`, `dateFin`, `langues` venant de la première notice remplie
   (cf [Titre::fillBySudoc()](https://gitlab.com/reseau-mirabel/mirabel/-/blob/master/src/www/protected/models/Titre.php#L986)).
   Pour chaque bloc ISSN, voir [Issn::fillBySudoc()](https://gitlab.com/reseau-mirabel/mirabel/-/blob/master/src/www/protected/models/Issn.php#L433).
2. dans le formulaire de titre et ses blocs d'ISSN.
   Interrogation à la volée, et remplissage des champs par JavaScript, cf [setIssnData()](https://gitlab.com/reseau-mirabel/mirabel/-/blob/master/src/www/protected/views/titre/js/_form-issn.js#L25).
3. lors d'un import KBART contenant de nouveaux titres,
4. dans les scripts d'import de données du Sudoc.

**Les données d'une notice locale Sudoc de Mir@bel ne sont utilisées que si elles sont non-vides**.
Autrement dit le script d'import ne passera jamais un support à la valeur "inconnu" (valeur considérée vide),
et l'interrogation à la volée qui recevrait une date "" ou "XXXX" selon le Sudoc n'effacera pas le champ du formulaire.
Voir ci-dessous [Annexe : écrasement des données](#annexe--remplissage-des-champs) pour davantage de détails.


Construction de la notice locale Sudoc Mirabel
---------------------------------

La suite de cette documentation détaille la façon dont est élaborée
cette notice locale Sudoc de Mirabel ([sudoc\Notice](https://gitlab.com/reseau-mirabel/mirabel/-/blob/master/src/www/protected/models/sudoc/Notice.php) dans le code source).


### Attribution d'un PPN

Si le point de départ est un ISSN, on utilise le web-service *issn2ppn*.
Par exemple, <https://www.sudoc.fr/services/issn2ppn/0002-0060> (noHolding).

- Si l'ISSN n'a pas de PPN, tout va bien.
- Si l'ISSN est associé à un seul PPN, tout va bien.
- S'il est associé à plusieurs PPN, on choisit le premier localisé (noHolding=0),
  et à défaut de localisation le premier tout court.

NB : Le PPN reçu n'est pas forcément exploitable car il peut avoir été supprimé,
par exemple après une fusion de notices, ou tout simplement être sans notice faute de localisation.


### Présence d'une notice MARC ?

Si on dispose d'un PPN, éventuellement après l'interrogation *issn2ppn* ci-dessus,
on utilise le web-service *SudocMarcXML*.
Auparavant Mir@bel interrogeait le web-service RDF, moins détaillé que le MarcXML.

1. Si le PPN n'a pas de notice MarcXML, alors :

	- En cas d'ISSN, on en déduit que le PPN est correct mais non localisé (noHolding = 1).
	- En l'absence d'ISSN, on suppose que le PPN est faux.

2. Si le PPN a une notice MarcXML, on en extrait certaines données pour construire un enregistrement sudoc\Notice dans Mirabel.
   Voir ci-dessous les détails.


### Lecture MARC, champ par champ

La [documentation MARC du Sudoc](http://documentation.abes.fr/sudoc/formats/unmb/index.htm)
a parfois été complétée par *reverse-engineering*.
Par exemple pour détecter les URL, les champs 9XX ne sont pas documentés.

#### localisation (no holding)

- Si `002` est vide, localisation (noHolding = 0).
- Si au moins un `930##` a un `$5` non-vide, localisation (noHolding = 0).
- Sinon, pas de localisation (noHolding = 1).

#### BNF Ark

On parcourt toutes les valeurs en `033##$` en cherchant le motif `https://catalogue.bnf.fr/ark:/12148/...`,
ce qui permet d'en déduire le numéro Ark.

#### ISSN et ses attributs

Sont lus :
l'ISSN, son ISSN-L, le titre de référence, son statut ("a" ongoing, "b" dead, "c" unknown).

#### dates de début et fin

Les années de début et de fin sont extraites de `100`.

Pour le moment elles sont vides (politique issn.org), *sauf pour les notices sans ISSN*.

#### pays

Le code du pays (2 lettres) est lu en `102##$a`.
En cas de valeurs multiples, la première est conservée.
La valeur spéciale "ZZ" (plusieurs pays principaux) est acceptée.

#### support

Le support est lu en `100$5` (un octet).
Les valeurs "a" (papier) et "l" (électronique) sont acceptées.
Les autres valeurs sont en support "inconnu".

Le fichier de log "runtime/sudoc.log" liste les PPN dont le support est à "m" (multimédia).

#### titre

Le titre est lu en `200|1#$a`.
À défaut, on utilise le titre alternatif de `200|0#$a`.
En cas de titres multiples, on prend le premier.

NB : le titre peut être absent, ou différent du titre de référence ISSN.

#### url

- On lit les multiples valeurs de `856|4#$u` si le champ n'est pas localisé (absence de `$5`).
- À défaut, on lit `999|##$u` s'il existe.
- On conserve la première de ces URL.

#### worldcat

Lecture de `035` (autres numéros de contrôle) avec l'identifiant "OCoLC".


Annexe : remplissage des champs
------

Concrètement, on convertit la "notice MARC du Sudoc" en "notice locale Sudoc de Mirabel", notée sudoc\Notice,
et on oublie les données MARC brutes.
Pour appliquer cette sudoc\Notice, on ne traite que ses champs non-vides,
 et on s'en sert pour remplir ou écraser les champs d'un titre ou d'un bloc Issn.

Exemples, en notant `sudoc\Notice` pour "Notice locale Sudoc construite dans Mirabel" :

- sudoc\Notice.support = "papier" => sera écrit dans Issn.support (écrasement si une valeur différente existait)
- sudoc\Notice.support = "inconnu" => sera ignoré ("inconnu" est assimilé à "vide")
- sudoc\Notice.dateDebut = null => sera ignoré
- sudoc\Notice.dateDebut = "2010" => sera écrit dans Issn.dateDebut
- sudoc\Notice.worldcat = null => sera ignoré
- sudoc\Notice.worldcat = "12345678" => sera écrit dans Issn.worldcatOcn

Le comportement est donc le même en création d'ISSN ou en modification.

Il n'y a pas de traitement particulier pour les notices MARC de support multimédia
puisqu'elles sont converties en "sudoc\Notice.support=inconnu".
Or un champ à "inconnu" n'écrasera jamais les données présentes dans Mirabel.
