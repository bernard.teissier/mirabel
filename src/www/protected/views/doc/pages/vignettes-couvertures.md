# Vignettes des couvertures de revues

## Principe

* Chaque titre peut déclarer une URL de couverture.
* La page de la revue affiche la première image de couverture de ses titres.
* La vignette est produite en 3 étapes

    1. attribuer une URL d'image au titre
    2. télécharger l'image *brute*
    3. convertir cette image aux dimmensions souhaitées par Mir@bel


## Organisation des fichiers

### Répertoires des images

* `src/www/images/titres-couvertures/raw/` contient les images *brutes*, telles que téléchargées,
* `src/www/images/titres-couvertures/` contient les images *redimensionnées*,
  construites à partir des images brutes.

Les URL correspondant à ces chemins sont donc "/images/titres-couvertures/…".

### Format des images brutes

* Les images brutes sont au format d'origine ("gif", "jpg" ou "png").
* Le fichier est nommé avec l'ID du titre sur 9 chiffres,
  par exemple `raw/000002873.jpg` ou `raw/000002483.gif`.

### Format des images visibles
* Les images utilisées sont toujours au format PNG.
* Elles sont redimensionnées pour tenir dans un carré de 128x128 pixels.
* Le fichier est nommé avec l'ID du titre sur 9 chiffres,
  par exemple `000002873.png`.


## Outils en ligne de commande

### 1. Affecter des URL de couverture aux titres

`./yii couverture` affiche une aide minimale intégrée.

Les commandes sont de la forme `./yii couverture …` où `…` est parmi :

* `persee`
   Utilise l'ID interne attribué par Persée pour en déduire la couverture.

* `revuesOrgCsv [--file=...] [--separator=,]`
   Utilise un fichier CSV fourni par Revues.org, sans intervention.

* `erudit`
   Utilise l'ISSN-e de la revue pour affecter une couverture chez Erudit.org, sans intervention.

* `eruditCsv --csv=value [--separator=;]`
   Utilise un fichier CSV fourni par Erudit, sans intervention.

* `cairnWeb`
   Télécharge la page de la revue chez Cairn.info, extrait la couverture, sans intervention.

* `cairnCsv --csv=value [--separator=;]`
    Utilise un fichier CSV fourni par Cairn. Passe par une intervention.

Options communes :

* `--verbose=0` : N'afficher que les erreurs.
* `--limit=X` : S'arrêter après X titres.
* `--interventions=1` : Créer des interventions pour enregistrer les changements d'URL (donc visibilité dans les RSS).
* `--maj=1` : Forcer la mise à jour des URL existantes.
* `--majMemeSource=1` : Forcer la mise à jour des URL existantes si elles viennent de la même source.
  Par exemple, `./yii couverture cairnWeb --majMemeSource=1` n'affectera une URL de couverture
  qu'aux titres qui n'en ont pas ou qui ont déjà une URL chez Cairn, de type `http://www.cairn.info/static/vign/…`.

### 2. Télécharger les images brutes

* `download [--urlPattern="…"]`

Le motif est au sens SQL, avec `%` pour comme caractère global. Voir l'exemple ci-dessous.

### 3. Convertir les images téléchargées

* `resize`

### Exemples complets

Mettre à jour les couvertures venant de Cairn (ou les créer) à partir des pages web de Cairn.info,
en passant par des interventions.
```
./yii couverture cairnWeb --interventions=1 --majMemeSource=1
./yii couverture download --urlPattern='http://www.cairn.info/static/%'
./yii couverture resize
```

Récupérer les nouvelles images à toutes les sources automatiques, sans interventions.
```
./yii couverture cairnWeb --majMemeSource=1
./yii couverture erudit --majMemeSource=1
./yii couverture persee --majMemeSource=1
./yii couverture download
./yii couverture resize
```

## Détails techniques

* Le téléchargement se fait via l'extension `cURL` de PHP.
* La conversion utilise la commande `convert` d'ImageMagick (l'exécutable et non l'extension PHP).
* Le format des images téléchargées est déduit du *Mime-Type* déclaré par le serveur.
  Il arrive que des sites donnent de fausses informations (déclarent que l'image est du HTML, etc),
  mais c'est rare.
