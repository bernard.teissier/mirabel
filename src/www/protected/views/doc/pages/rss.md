# Flux RSS

Il y a 2 familles de flux dans Mir@bel.
Pour des raisons historiques, elles sont pour le moment traitées différemment,
à des adresses distinctes.

La taille des flux se configure sur la page [/config](http://www.reseau-mirabel.info/config).


## Flux paramétrés

Ces flux permettent de suivre les interventions concernant certains objets,
sélectionnés par des paramètres optionnels donnés dans l'URL.

URL d'accès
:	<http://www.reseau-mirabel.info/feeds.php> pour l'accès public,  
    <http://www.reseau-mirabel.info/private-feeds.php> pour l'accès privé (contrôle d'accès dans Apache).

Taille maximale
:	Paramètre de configuration `rss.limite.interventions`, sinon 50.

Format
:	atom.
	On peut le changer avec un paramètre dans l'URL, comme `type=rss2`.

### Critères disponibles

Les critères suivant sont passés en paramètres GET dans l'URL.
Ils peuvent être combinés.

* `revue`
* `ressource`
* `editeur`

Par exemple <http://www.reseau-mirabel.info/feeds.php> ou <http://www.reseau-mirabel.info/feeds.php?revue=7> ou <http://www.reseau-mirabel.info/feeds.php?revue=7&ressource=7>.

Pour l'accès privé, les critéres suivant sont ajoutés :

* `valide`=(0|1) : par défaut, toutes les interventions ; 0 = en-attente ; 1 = validé.
* `editeurs`=1 : uniquement les modifications d'éditeurs.
* `partenaire`=1234 : tout ce qui est suivi par ce partenaire.

Par exemple <http://www.reseau-mirabel.info/private-feeds.php?valide=0&partenaire=1>
pour lister les interventions en attente sur des objets suivis par Sciences Po Lyon.

### Cache

Ces flux peuvent être complexes à générer.
Or les abonnements RSS sont souvent configurés pour envoyer des requêtes toutes les 5 ou 15 minutes.
Les fichiers sont donc mis en cache quotidien,
c'est-à-dire que le flux n'est généré qu'à la première requête de la journée.


## Flux globaux

### Brèves

URL d'accès
:	<http://www.reseau-mirabel.info/rss/breves>.

Taille maximale
:	Paramètre de configuration `rss.limite.breves`.

Format
:	atom.


### Création de revues

URL d'accès
:	<http://www.reseau-mirabel.info/rss/revues>.

Taille maximale
:	Paramètre de configuration `rss.limite.revues`.

Format
:	atom.

