Thématique et thèmes
====================

Lexique
-------

* **thème** :
  Initialement appelé *catégorie*. Dans le code et le SQL, le nom est resté à `Categorie`.
* **thématique** :
  Ensemble de thèmes.
  Par exemple, l'ensemble des thèmes de Mir@bel, ou l'ensemble des thèmes affectés à une revue.
* **vocabulaire** :
  Ensemble de correspondances entre un *thème de Mir@bel* et un *mot-clé extérieur*.
  Par exemple, un vocabulaire "Érudit" traduira les mots-clés venant de cette source.
* **alias** :
  Une de ces correspondances dans un vocabulaire.
  Par exemple, le mot-clé "Cinéma" dans le vocabulaire d'Érudit correspond au thème "Arts vivants" de Mir@bel.

Mécanisme général
-----------------

* Les thèmes sont organisés en un arbre (chaque thème est rattaché à un unique thème parent).
* Un utilisateur attribue des thèmes à une *revue*.
* Un import thématique utilise un vocabulaire pour transcrire une indexation extérieure
  en une attribution de thèmes à des *titres*.
* La page d'une revue affiche sa thématique directe (attribuée manuellement),
  fusionnée avec sa thématique indirecte (celle importée sur ses titres).

Historique des modifications
----------------------------

Les interventions sont le seul mécanisme permettant un historique des modifications.

* Les opérations manuelles (attribuer une catégorie à une revue) passent par des interventions,
  et sont donc visibles dans les flux RSS.
* Les opérations massives (charger un vocabulaire, importer une indexation extérieure)
  n'utilisent pas d'intervention (pas de trace dans les RSS).

Pour l'indexation extérieure d'un titre,
Mir@bel ne dispose donc que de la date de la dernière attribution pour chaque vocabulaire,
sans historique.

Permissions
-----------

Chaque groupe hérite des capacités précédentes :

* *Tout public* :
	Voir l'arbre thématique,
	filtrer les revues par des thèmes (recherche avancée).

* *Utilisateurs authentifiés (hors éditeurs)* :
	Proposer de nouveaux thèmes (candidats),
	voir les candidats refusés,
	attribuer des thèmes publics à une revue non suivie ou qu'ils suivent,
	proposer des thèmes sur une revue quelconque.

* *Utilisateurs indexation* :
	Créer des thèmes,
	valider des thèmes candidats,
	accéder aux vocabulaires et à l'import/export,
	attribuer des thèmes candidats à une revue non suivie ou qu'ils suivent.

Les propositions thématiques sur une revue sont validées par ceux qui suivent la revue.

Un utilisateur qui n'a pas la permission d'indexation ne peut pas modifier les thèmes,
sauf les thèmes candidats qu'il a lui-même proposé.
De même pour les suppressions.


Import d'indexation par un vocabulaire
-------------------

Il s'agit donc d'utiliser un vocabulaire pour traduire une indexation extérieure (Érudit, etc)
en une attribution de thèmes à des *titres*.

L'opération est immédiate, sans intervention.
Mir@bel détecte le format du fichier chargé :

1. un CSV à 2 colonnes : identifiant de titre (ISSN ou ID Mir@bel), et mot-clé ;
2. un fichier KBART (tabulation comme séparateur) avec une colonne discipline où les mots-clés sont séparés par `;`.

Le seul moyen actuel de supprimer la thématique d'un *titre* est de réaliser un nouvel import d'indexation
en cochant la case "Remplace les indexations de ce même vocabulaire".

Mir@bel garde la trace des vocabulaires qui ont attribué des catégories à un titre.
Il est donc techniquement possible, pour une revue donnée, de lister séparément les thèmes
venant de chaque source : par exemple, lister les thèmes attribués (indirectement) par Érudit,
ceux de Revues.org, et ceux attribués (directement) par les utilisateurs de Mir@bel.


Mantis (principaux tickets)
------

* Ticket initial : [#2727 Catégories thématiques (plan de classement)](http://tickets.silecs.info/mantis/view.php?id=2727)
  Contient également des *pistes pour plus tard*.
* [#2779: Catégorie : interventions et saisie non-auth](http://tickets.silecs.info/mantis/view.php?id=2779)
  Introduit l'utilisation d'interventions (propositions, validations, etc).
* [#2775: Catégories : gestion des vocabulaires et alias](http://tickets.silecs.info/mantis/view.php?id=2775)
* [#2777: Catégories : navigation publique dans l'arbre](http://tickets.silecs.info/mantis/view.php?id=2777)
* [#2778: Catégories : recherche de revues par mots-clés](http://tickets.silecs.info/mantis/view.php?id=2778)
