# Plan d'une instance Mir@bel

La plupart des pages sont acessibles par des liens dans l'interface web,
cette page liste les exceptions.

## Page d'une revue

En plus de l'URL usuelle de forme `/revue/<id>` (par exemple [/revue/155](/revue/155)),
on peut désigner une revue autrement que par son identifiant dans Mir@bel :

* `/revue/titre/<nom de la revue>`
	Le titre complet, avec ou sans son préfixe ("Le ", etc).
	On peut utiliser `_` au lieu du caractère espace ` `, par exemple [/revue/titre/Acta_Fabula](/revue/titre/Acta_Fabula).
	En cas d'ambiguïté, on arrive sur la page de recherche listant les différentes revues qui correspondent.

* `/revue/titre-id/<id>`
	L'identifiant numérique d'un des titres de la revue.

* `/revue/issn/<id>`
	L'ISSN, y compris ISSN-e et ISSN-l.

* `/revue/sudoc/<id>`
	L'identifiant numérique du SUDOC, c'est-à-dire le PPN (Pica production number).

* `/revue/worldcat/<id>`
	L'identifiant numérique de Worldcat.


## API v2

Utiliser l'aide intégrée [/api](/api) pour découvrir les requêtes possibles.
Le web-service suit la norme OpenAPI, dérivée du format Swagger.
