Dépendances logicielles
=======================

Cf Mantis #4470.

Liste automatique
-----------------

Dans les blocs rédactionnels, la variable `%BIBLIOS_LOGICIELLES%`
est remplacée par la liste de toutes les **dépendances directes** chargées automatiquement.

Mir@bel utilise l'outil PHP standard [Composer](https://getcomposer.org] pour
installer et mettre à jour certaines bibliothèques.
La liste automatique extrait les informations des fichiers `composer.*`
(qui ne sont pas forcément complètes si les bibliothèques sont peu ou mal maintenues).

Limites :

- Certaines dépendances ne sont pas chargées par ce système.
  Par exemple les cartes avec Leaflet.
- Les dépendances indirectes n'y figurent pas.
- Seuls les composants intégrés au code sont concernés,
  or les outils comme la recherche Sphinx n'ont pas besoin d'une bibliothèque spécifique.

Liste manuelle
--------------

Pour compenser les limites de la liste automatique,
voici une liste manuelle (pas forcément à jour) :

* structure du code PHP bâtie sur le cadre Yii
  <http://www.yiiframework.com/> (licence MIT) et ses dépendances ;
* Mise en forme sur une base Bootstrap <https://getbootstrap.com/>
  (licence Apache v2.0) ;
* cartes réalisées avec Leaflet <http://leafletjs.com/> (licence BSD
  2-clause) ;
* arbres dépliables avec JsTree <http://www.jstree.com> (licence MIT) ;
* lecture et écriture aux formats Excel avec PhpSpreadsheet
  <https://phpspreadsheet.readthedocs.io> (licence MIT) ;
* flux RSS et Atom générés avec ZetaComponents Feed
  <https://github.com/zetacomponents/Feed> (licence Apache v2.0) ;
* envois de courriels avec SwiftMailer <https://swiftmailer.symfony.com/>
  (licence MIT) ;
* tests avec Codeception <https://codeception.com/> (licence MIT).

Le projet utilise d'autres outils, en plus des briques logicielles dans
le code :

* langage et interpréteur PHP avec cURL, ImageMagick et des dizaines
  d'autres bibliothèques intégrées à PHP (licence libre PHP)
* systèmes Linux Debian <https://debian.org>
* serveur web Apache httpd (licence Apache v2.0)
* base de données SQL MariaDB <https://mariadb.com/> (licences GPLv2 et
  LGPLv2)
* recherche assurée par Sphinx Search <http://www.sphinxsearch.com/>
  (licence GPL v2)
* collecte anonymisée des usages avec Matomo <https://matomo.org/>
  (licence GPL v3)

Remarques
---------

Au moins une des bibliothèques de Mirabel est abandonnée en amont :
ZetaComponents Feeds.
Mais comme les normes RSS et Atom sont stables,
ce n'est pas critique de trouver un remplaçant
(et réécrire le code des RSS pour l'adapter à une autre biblio).
