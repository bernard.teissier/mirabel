Liens et imports de liens
=========================


Imports de liens
----------------

Le cron (visible par `crontab -l | less`) lance périodiquement un script par source d'import,
avec les paramètres :

- `--obsoleteSince="7 days ago"` pour les imports quotidiens,
  afin de détecter les liens auparavant importés, mais absents depuis une semaine au moins.
- `--email="sophie@..." --email="bernard@..."` pour envoyer à ces destinataires un courriel
  qui liste les changements et les liens obsolètes.

En ligne de commande, d'autres options sont possibles, listées avec par exemple `./yii help sherpa`.
Les options `--simulation=1` et `--verbose=2` sont toujours suivies,
mais certains imports n'implémentent pas `--limit=100`.

### Processus général

Dans tous les cas (sauf Wikidata), on trouve le lien d'un titre grâce à ses ISSN,
et on insère ou met à jour l'URL du lien.
Si plusieurs liens correspondent à un même titre pour une source donnée,
seul le dernier sera conservé (un seul lien par source).

On conserve au passage la date courante comme trace d'activité de chaque lien rencontré,
ce qui permet ensuite de détecter les liens importés mais absents récemment.

### Source DOAJ

Les données sont dans un CSV <https://doaj.org/csv> téléchargé s'il est plus récent qu'au précédent téléchargement.

On récupère alors des ISSN,
sachant que l'URL du lien se déduit de l'ISSN électronique s'il est présent, papier sinon.

### Source Erih+

Les données sont fournies par un [csv en ligne](https://dbh.nsd.uib.no/publiseringskanaler/erihplus/periodical/listApprovedAsCsv).

On récupère alors des couples (identifiant-Erih+, ISSN),
sachant que l'URL du lien se déduit de l'identifiant Erih+.

### Source HAL

1. On interroge l'API `ref journal` de HAL par lots de 100 ISSN extraits de Mir@bel,
   avec identification par ISSN-p puis par ISSN-e (HAL se trompe parfois de support).
2. Pour les revues trouvées, on demande à l'API `search` de HAL, par lots de 100,
   à connaître leur nombre de notices, pour exclure les revues qui n'en ont pas.
3. On regroupe ces revues par titre de Mirabel,
   pour obtenir qu'à un `Titre.id` de M soit associé une liste d'identifiants HAL (`journal docid`).
4. On construit l'URL HAL avec cette liste triée par docid croissant (donc stable).

Cf [Mantis #4772](https://tickets.silecs.info/view.php?id=4772) pour les détails.

### Source ROAD

Les données sont dans un fichier MARCXML `ROAD.xml` téléchargé avec une compression gzip
sur <ftp://ROAD:open_access@ftp.issn.org/>.
Le FTP est interrogé à chaque fois, mais le fichier n'est téléchargé que s'il est nouveau.
Par contre, le traitement se déclenche quel que soit l'âge du fichier XML.

### Source Sherpa

Tout le processus est décrit dans la page [Sherpa-Romeo](sherpa-romeo.md).

Contrairement aux précédents, l'import passe par une commande `./yii sherpa …` et non `./yii links …`.

### Source Wikipédia

Le processus est décrit dans la page [Wikidata](wikidata.md).

Contrairement aux précédents, l'import passe par une commande `./yii wikidata …` et non `./yii links …`.


Icônes
------

L'ajout d'icônes étant rare, il n'y a pas d'interface web pour les gérer.
Il faut passer par [Mantis #3127](https://tickets.silecs.info/view.php?id=3127) ou par email.

Concrètement, il suffit de déposer une image 16×16 sous la forme
`src/www/images/liens/{nom}.png` où `{nom}` est un identifiant de source de lien
(`Sourcelien.nomcourt` en SQL).
Par exemple, `erihplus.png` pour "ERIH PLUS".


Structure des données
---------------------

Initialement, les liens d'un titre étaient stockés sérialisés dans un champ de la table Titre.
Comme ils n'étaient utilisés qu'en affichage sur la page d'une revue,
cette technique a permis d'ajouter la fonctionnalité sans bousculer le mécanisme des interventions.

Avec le besoin de statistiques sur les liens, la désérialisation est devenue nécessaire.
Les liens sont désormais stockés sérialisés dans la table `Titre.liensJson`
et en relationnel dans la table `LienTitre`.
De même pour `Editeur` au lieu de `Titre`.

À terme, il faudra probablement retirer la version sérialisée,
mais alors il faudra revoir le mécanisme des interventions,
et la structure de la table de liens (ordre, groupes, etc).
