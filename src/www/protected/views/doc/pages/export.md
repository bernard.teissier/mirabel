Intégration des données de Mir@bel dans des applications externes
=================================================================

Possessions dans Mir@bel
------------------------

Chaque partenaire peut déclarer ses possessions, par exemple les titres de son Koha,
en leur atttribuant au passage leurs identifiants numériques locaux, par exemple ceux de Koha.

API v1
------

Auto documentée sur [/api](/api).

Un **plugin Koha** sur cette API a été créé par Tamil :
<https://github.com/fredericd/Koha-Plugin-Tamil-Mirabel>

Export web des accès
--------------------

### export générique des accès

Pour un utilisateur authentifié, les résultats d'une recherche de revue
sont accompagnés d'un bloc d'export des accès.
L'URL d'export peut être construite *à la main* sous la forme :
`/service/export?q[titre]=politique&Service[acces]=Libre&format=kbart`
où

* `q[...]` est un critère de recherche avancée sur les revues,
  correspondant à un attribut public de la classe `SearchTitre` ;
* `Service[...]` est un critère de sélection des accès,
  correspondant à un attribut public de la classe `ServiceFilter`.
* `format` est par défaut à "csv" (séparateur ";") mais accepte aussi "kbart".

### exports Bacon des accès

Cet export utilise l'export web des accès en ligne décrit ci-dessus,
avec trois paramètres exclusifs, non présents dans le formulaire web :

* [baconLibre](/service/export?Service[baconLibre]=1&format=kbart)
	- accès libre
	- texte intégral
	- pas de lacunaire
	- pas de sélection d'articles
* [baconDoaj](/service/export?Service[baconDoaj]=1&format=kbart)
	- accès libre
	- texte intégral
	- pas de lacunaire
	- pas de sélection d'articles
	- le titre contient un lien DOAJ
* [baconGlobal](/service/export?Service[baconGlobal]=1&format=kbart)
	- texte intégral
	- pas de sélection d'articles

Il est possible d'ajouter des critères sur les titres (`q[...]`)
dans les URL ci-dessus, par exemple [baconDoaj+Cairn](/service/export?Service[baconDoaj]=1&q[ressourceId]=3&format=kbart).
L'interprétation exacte de ces critères "bacon*" et de tous les filtres d'accès est dans
`ExportServices::findServices()`.

Par ailleurs, un mécanisme de dépôt dans le webdav de Bacon est
dans la classe `BaconUpload`, appelé depuis `commands/ExportController.php`.
Il est associé à la ligne de commande `./yii export bacon`.
Pour fonctionner, le bloc `bacon` doit être configuré dans `config/local.php` :
```
'bacon' => [
   'url' => 'https://cloud.abes.fr/path/to/webdav/root',
   'username' => '...',
   'password' => '...',
 ],
```

Export des titres
-----------------

Des exports web de tous les titres sont accessibles aux admins :

- `/titre/export?type=identifiants` titres avec tous leurs identifiants, cf #3223
- `/titre/export?type=maxi` : titres avec leur thématique, cf #3510

Un export destiné à l'ABES est accessible sans authentification :

- `/titre/export?type=ppn` : PPN + URL-du-titre, cf #4429

De même pour la BnF :

- `/titre/export?type=bnf` : cf #4580


Koha (ancienne API, obsolète)
----

Cf la section API plus haut pour un plugin utilisant la nouvelle API.

Koha peut récupérer les données de Mir@bel en utilisant :

* le [webservice](webservice.html) de Mir@bel ;
* un script Perl `mirabel_to_koha.pl` qui doit être exécuté sur le serveur Koha.

Il existe trois implémentations du script pour Koha :

1. Le script initial a été développé par [Biblibre](http://git.biblibre.com/?p=mirabel;a=summary)
   mais il n'est plus maintenu.

2. Une version enrichie de ce script est maintenue par Silecs :
   <https://github.com/silecs/mirabel-koha>

3. Un script simplifié a été développé par Tamil :
   <https://github.com/fredericd/Koha-Contrib-Mirabel>

Les scripts 2 et 3 sont accompagnés de documentations détaillées.

Le script 3 n'utilise que 2 paramètres (l'identifiant de partenaire Mir@bel
et le champ MARC où écrire, tous deux ajoutés à la configuration Koha).
Tout le reste est pré-déterminé : récupération des titres possédés par ce partenaire,
remplissages des champs MARC.

Le script 2 permet de sélectionner les accès selon différents critères,
notamment les possessions du partenaire, mais aussi les collections, le type d'accès, etc.
On peut également configurer finement les champs MARC à remplir dans Koha.
Ce script est donc plus puissant, mais plus complexe.


### Notes sur l'intégration du Webservice v1 de Mir@bel (obsolète)

* L'accès est réservé aux partenaires, et contrôlé par IP
  (déclarées sur le serveur de Mir@bel dans `/etc/httpd/webservice-hosts.allow`).

* Le webservice n'est pas prévu pour être interrogé à la volée par un site web,
  mais pour alimenter une base de donnée.
