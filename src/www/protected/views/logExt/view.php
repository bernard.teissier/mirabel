<?php

/** @var Controller $this */
/** @var LogExt $model */
assert($this instanceof Controller);

$this->breadcrumbs = [
	'Logs' => ['/logExt'],
	$model->id,
];

$this->menu = [];
/**
 * @todo Liens: logs du même utilisateur, de la même session.
 */
?>

<h1>Détail du log <em>#<?php echo $model->id; ?></em></h1>

<?php
$this->widget(
	'bootstrap.widgets.BootDetailView',
	[
		'data' => $model,
		'attributes' => [
			'id',
			'level',
			'category',
			'logtime:datetime',
			[
				'name' => 'message',
				'type' => 'raw',
				'value' => "<pre>" . CHtml::encode($model->message) . "</pre>",
			],
			'controller',
			'action',
			'modelId',
			'ip',
			'userId',
			'session',
		],
	]
);
?>
