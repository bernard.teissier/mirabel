<?php


/** @var Controller $this */
/** @var LogExt $model */
assert($this instanceof Controller);

$this->breadcrumbs = [
	'Logs',
];

$this->menu = [];
?>

<h1>Logs</h1>

<p>
	You may optionally enter a comparison operator
	(<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
	or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<form action="">
	<button type="submit">Filtrer</button>
<?php
/**
 * @todo Afficher les critères qui ne sont pas visibles dans le filtre.
 * @todo Ajouter un lien pour réinitialiser les critères.
 * @todo Optimiser le SQL (trop de requêtes).
 */
$loginsUsed = Tools::sqlToPairs(
	"SELECT l.userId, u.login "
		. "FROM LogExt l JOIN Utilisateur u ON l.userId = u.id "
		. "GROUP BY u.id ORDER BY u.login ASC",
	['null' => 'Anonyme']
);
$columns = [
	//'id',
	[
		'name' => 'level',
		'filter' => ['error' => 'Erreur', 'warning' => 'Avertissement'],
	],
	[
		'name' => 'category',
		'filter' => Tools::sqlToPairs("SELECT DISTINCT category, category FROM LogExt ORDER BY category"),
	],
	'logtime:datetime',
	[
		'name' => 'messageTitle',
		'filter' => false,
		'type' => 'raw',
		'value' => function ($data, $row) {
			return CHtml::link($data->getMessageTitle(), ['view', 'id' => $data->id]);
		},
	],
	'controller',
	'action',
	[
		'name' => 'modelId',
		'filter' => false,
		'type' => 'raw',
		'value' => function ($data, $row) {
			return $data->modelId ?
				CHtml::link($data->modelId, ["{$data->controller}/view", "id" => $data->modelId])
				: "";
		},
	],
	[
		'name' => 'userId',
		'value' => function ($data, $row) {
			if (!empty($data->userId) and empty($data->utilisateur->login)) {
				Yii::log(
					"Utilisateur inexistant mais référencé, "
					. "LogExt.id={$data->id}, userId={$data->userId}",
					'warning'
				);
				return '-';
			}
			return (!empty($data->userId) ? $data->utilisateur->login : '');
		},
		'filter' => $loginsUsed,
	],
	'ip',
	[
		'class' => 'BootButtonColumn', // CButtonColumn
		'template' => '{view}',
	],
];
$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'log-ext-grid',
		'dataProvider' => $model->search(),
		'filter' => $model, // null to disable
		'columns' => $columns,
		'ajaxUpdate' => false,
		'rowCssClassExpression' => '"level-" . $data->level',
	]
);
?>
</form>
