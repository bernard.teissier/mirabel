<?php

/** @var Controller $this */
/** @var array $collections */
/** @var Ressource $ressource */
assert($this instanceof Controller);

$this->pageTitle = "Collections / " . $ressource->nom;

$this->breadcrumbs = [];
$this->breadcrumbs[$ressource->nom] = ['/ressource/view', 'id' => $ressource->id];
$this->breadcrumbs[] = 'Collections';
?>

<h1><?= CHtml::encode($this->pageTitle) ?></h1>

<?php
if (empty($collections)) {
	echo "<p>Aucune collection actuellement.</p>";
} else {
	$this->widget(
		'ext.bootstrap.widgets.BootGridView',
		[
			'id' => 'collection-grid',
			'dataProvider' => new CArrayDataProvider($collections, ['pagination' => false]),
			'filter' => null,
			'columns' => [
				'nom',
				'type',
				[
					'class' => 'BootButtonColumn', // CButtonColumn
					'template' => '{update} {delete}',
				],
			],
			'ajaxUpdate' => false,
		]
	);
}
?>
