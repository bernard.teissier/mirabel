<?php

/** @var Controller $this */
/** @var Collection $model */
/** @var int $services */
/** @var int $servicesDeleted */
/** @var int $numAbos */
assert($this instanceof Controller);

$this->pageTitle = "Collection {$model->nom} - supprimer";

$this->breadcrumbs = [];
$this->breadcrumbs[$model->ressource->nom] = ['/ressource/view', 'id' => $model->ressourceId];
$this->breadcrumbs[] = $model->nom;
?>

<h1>Supprimer la collection <em><?= CHtml::encode($model->getFullName()) ?></em></h1>

<p>Cette collection contient <em><?= $services ?> accès</em> sur des titres.</p>

<?php if ($services > 0) { ?>
<p class="alert alert-danger">
	Les <strong><?= $servicesDeleted ?></strong> accès liés à cette seule collection seront supprimés.
</p>
<?php } ?>

<?php
if ($numAbos === 0) {
	?>
	<form name="colection-delete" action="" method="post">
		<p>
			<input type="hidden" name="confirm" value="1" />
			<input type="hidden" name="id" value="<?= (int) $model->id ?>" />
			<button type="submit" class="btn btn-danger">Confirmer cette suppression</button>
		</p>
	</form>
	<?php
} else {
		?>
	<p class="alert alert-danger">
		<strong>Il y a <?= $numAbos ?> abonnements.</strong>
		La suppression de collections ayant des abonnements est interdite.
		Il faut que les partenaires se désabonnent au préalable.
	</p>
	<?php
	}
