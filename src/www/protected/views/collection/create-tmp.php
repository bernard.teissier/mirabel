<?php

/** @var Controller $this */
/** @var Collection $model */
/** @var int $nbAcces */
/** @var int $nbAbos */
assert($this instanceof Controller);

$this->pageTitle = "Collection par défaut";

$this->breadcrumbs = [
	$model->ressource->nom => ['/ressource/view', 'id' => $model->ressourceId],
	"Collection par défaut",
];
?>

<h1>Création d'une collection par défaut</h1>

<p class="alert alert-info">
	Cette ressource <?= $model->ressource->getSelfLink() ?> possède des accès en ligne, mais aucune collection.
	Avant de créer une nouvelle collection, il faut donc migrer les accès pré-existant dans une collection.
	Par défaut, celle-ci à un statut spécial <strong>temporaire</strong>
	qui la rend <em>invisible aux extérieurs</em> et <em>interdit de s'y abonner</em> par la suite.
</p>

<div class="well">
	À la soumission du formulaire :
	<ol>
		<li>La collection sera enregistrée.</li>
		<li>Les <b><?= $nbAcces ?></b> accès en ligne de la ressource seront rattachés à cette collection.</li>
		<li>Les <b><?= $nbAbos ?></b> abonnements à la ressource seront rattachés à cette collection.</li>
		<li>Vous serez redirigé vers la création d'une seconde collection dans cette ressource <?= $model->ressource->getSelfLink() ?>.</li>
	</ol>
</div>

<?= $this->renderPartial(
	'_form',
	[
		'model' => $model,
	]
); ?>
