<?php

/** @var Controller $this */
/** @var Collection $model */
assert($this instanceof Controller);

$this->pageTitle = "Collection {$model->nom} - modifier";

$this->breadcrumbs = [];
$this->breadcrumbs[$model->ressource->nom] = ['/ressource/view', 'id' => $model->ressourceId];
$this->breadcrumbs[] = $model->nom;
?>

<h1>Modifier la collection <em><?= CHtml::encode($model->getFullName()) ?></em></h1>

<?= $this->renderPartial('_form', ['model' => $model]) ?>
