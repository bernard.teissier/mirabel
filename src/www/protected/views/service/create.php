<?php

/** @var Controller $this */
/** @var Service $model */
/** @var bool $direct */
/** @var Intervention $intervention */
/** @var array $titres */
/** @var bool $forceProposition */
assert($this instanceof Controller);

$this->pageTitle = 'Créer un accès en ligne';

$this->breadcrumbs = [];
if (!empty($model->titreId)) {
	$this->breadcrumbs['Revues'] = ['/revue/index'];
	if (Yii::app()->user->isGuest) {
		$this->breadcrumbs[] = $model->titre->getFullTitle();
	} else {
		$this->breadcrumbs[$model->titre->getFullTitle()] = ['/revue/view', 'id' => $model->titre->revueId];
	}
} elseif (!empty($titres)) {
	$titreId = array_key_first($titres);
	$titre = Titre::model()->findByPk($titreId);
	$this->breadcrumbs = [
		'Revues' => ['/revue/index'],
		$titre->revue->getFullTitle() => ['/revue/view', 'id' => $titre->revueId],
	];
	if (!Yii::app()->user->isGuest) {
		$this->breadcrumbs[$titres[$titreId]] = ['/titre/view', 'id' => $titreId];
	}
} elseif (!empty($model->ressourceId)) {
	$this->breadcrumbs = [
		'Ressource' => ['/ressource/index'],
		$model->ressource->getFullName() => ['/ressource/view', 'id' => $model->ressourceId],
	];
}
$this->breadcrumbs[] = 'Ajouter un accès en ligne';

?>

<h1>Nouvel accès en ligne</h1>

<?php
echo $this->renderPartial(
	'_form',
	[
		'model' => $model,
		'direct' => $direct,
		'titres' => $titres,
		'intervention' => $intervention,
		'forceProposition' => $forceProposition,
		'forbidCollectionsUpdate' => false,
	]
);
?>
