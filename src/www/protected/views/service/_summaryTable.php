<?php

/** @var Controller $this */
/** @var DisplayServices $display */
/** @var bool $directAccess */
/** @var int $partenaireId */
/** @var bool $publicView */
/** @var array|string $publicViewUrl */
assert($this instanceof Controller);

require_once Yii::getPathOfAlias('application.models.import') . '/Normalize.php';

if (!$display->filterServices()) {
	echo "<p>Les partenaires de Mir@bel n'ont à ce jour trouvé aucun accès en ligne pour
		cette revue : ni texte intégral, ni sommaires, ni résumé&hellip;</p>";
	return;
}

if ($partenaireId) {
	$partenaire = Partenaire::model()->findByPk($partenaireId);
} else {
	$partenaire = null;
}

$imgUrl = Yii::app()->getAssetManager()
	->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview';
?>
<table class="table table-striped table-bordered table-condensed table-services">
	<thead>
		<tr>
			<th></th>
			<th>Ressource</th>
			<th>Accès</th>
			<th>Numéros</th>
			<th>Autres liens</th>
			<th><em>Action</em></th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($display->filterServices() as $service) {
			/** @var Service $service */ ?>
		<tr class="service-<?= $service->fusion ? 'fusion' : $service->id ?>">
			<td class="service-link"><?php
				$proxyUrl = $service->getProxies($partenaire, $display);
			echo $service->getSelfLink($proxyUrl); ?></td>
			<td><?php
				echo $service->ressource->getSelfLink(true);
			$collections = $service->getVisibleCollections(Yii::app()->user->isGuest);
			echo '<div class="service-collections">';
			echo "<p>Présentation détaillée de la ressource " . CHtml::encode($service->ressource->nom) . " dans Mir@bel</p>";
			if ($service->fusion && Collection::model()->countByAttributes(['ressourceId' => $service->ressourceId])) {
				echo "<p>Utiliser le bouton <em>Afficher le détail</em> pour voir les collections.</p>";
			} elseif ($collections) {
				echo "Collections :<ol>";
				foreach ($collections as $c) {
					/** @var Collection $c */
					echo "<li>" . CHtml::encode($c->nom) . "</li>";
				}
				echo "</ol>\n";
			}
			echo "</div>"; ?></td>
			<td><?php echo $display->getServiceAccessWithTitle($service); ?></td>
			<td><?php
				echo '<span class="s-couverture" title="'
					. ($service->notes ? "Notes du fournisseur : " . CHtml::encode($service->notes) : "")
					. '">' . $service->getPeriode() . '</span>';
			if ($service->dateBarrInfo) {
				echo ' <abbr title="' . CHtml::encode($service->dateBarrInfo) . '">'
						. CHtml::image(Yii::app()->baseUrl . "/images/datebarriere.png", "verrou")
						. '</abbr>';
			} elseif ($service->embargoInfo) {
				echo ' <abbr title="' . CHtml::encode(\import\Normalize::getReadableKbartEmbargo($service->embargoInfo)) . '">'
						. CHtml::image(Yii::app()->baseUrl . "/images/datebarriere.png", "verrou")
						. '</abbr>';
			}
			$couverture = [];
			if ($service->lacunaire) {
				$couverture[] = '<span title="Numéros manquants dans la collection">[lacunaire]</span>';
			}
			if ($service->selection) {
				$couverture[] = '<span title="Pour chaque numéro, une sélection d\'articles">[sélection d\'articles]</span>';
			}
			if ($couverture) {
				echo '<div class="s-rem">' . join('<br />', $couverture) . '</div>';
			} ?></td>
			<td><?php echo $service->getLinks(); ?></td>
			<td><?php
				if ($service->fusion) {
					echo CHtml::link(
						'<i class="icon-th-list"></i>',
						$publicViewUrl,
						['title' => "Cet accès est une vue simplifiée de plusieurs accès. Passer en mode \"Afficher le détail\" pour le modifier."]
					);
				}
			if ($service->hasImportSource()) {
				if (!$service->fusion) {
					echo CHtml::link(
						CHtml::image($imgUrl . '/update.png', 'Modifier', ['style' => 'position:absolute; z-index:20; opacity: .8;']) . '<i style="position:relative; top:8px; left:2px;" class="icon-envelope"></i>',
						['/service/contact', 'id' => $service->id],
						['title' => "Proposer une modification via un formulaire de contact"]
					) . " ";
				}
				if (Yii::app()->user->checkAccess('service/admin')) {
					if (!$service->fusion) {
						echo CHtml::link(
							CHtml::image($imgUrl . '/update.png', 'Modifier'),
							['/service/update', 'id' => $service->id],
							['title' => 'Modifier un accès sur une ressource importée automatiquement (admin seulement)']
						);
					}
					echo ' <abbr title="'
							. ($service->hdateImport ? 'Dernier import: ' . date('Y-m-d', $service->hdateImport) : 'Accès créé/modifié le ' . date('Y-m-d', $service->hdateModif))
							. '">Import</abbr> ';
				}
			} elseif (!$service->fusion) {
				echo CHtml::link(
					CHtml::image($imgUrl . '/update.png', 'Modifier'),
					['/service/update', 'id' => $service->id],
					['title' => $directAccess ? 'Modifier' : 'Proposer une modification']
				);
				if ($directAccess) {
					$orphan = '';
					$countServices = Service::model()->countByAttributes(['ressourceId' => $service->ressourceId]);
					if ($countServices < 2) {
						$orphan = "\nSupprimer cet accès rendra la ressource orpheline";
					}
					$selfUrl = Yii::app()->request->url;
					echo " " . CHtml::link(
						CHtml::image($imgUrl . '/delete.png', 'Supprimer'),
						$this->createUrl(
							'/service/delete',
							['id' => $service->id, 'returnUrl' => $selfUrl]
						),
						[
							'submit' => ['/service/delete', 'id' => $service->id, 'returnUrl' => $selfUrl],
							'title' => "Supprimer cet accès, après confirmation",
							'confirm' => "Êtes vous certain de vouloir supprimer cet accès ?\n" . $orphan,
						]
					);
				}
			} ?></td>
		</tr>
		<?php
		}
		?>
	</tbody>
</table>
