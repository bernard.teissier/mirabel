
$('input.volno-num').each(function() {
	$(this).data('val', $(this).val());
});

$('div.volno').on('keyup', "input.volno-num", function() {
	if ($(this).val().match(/^\d*$/)) {
		$(this).parent().removeClass('error').find('.help-inline.error').remove();
	} else {
		if (!$(this).parent().hasClass('error')) {
			$(this).parent().addClass('error').append('<span class="help-inline error"> Ces champs sont numériques !</span>');
		}
		return true;
	}
	if ($(this).val() == $(this).data('val')) {
		return true;
	}
	$(this).data('val',  $(this).val());
	var dir = $(this).closest('div.volno');
	var vol = $('.vol', dir).val();
	vol = vol ? 'Vol. ' + vol : '';
	var no = $('.no', dir).val();
	no = no ? 'no ' + no : '';
	if ($('.joined', dir).val().match(/^(Vol\. \d+)?(, )?(no \d+)?$/)) {
		$('.joined', dir).val(vol !== '' && no !== '' ? vol + ", " + no : vol + no);
	} else {
		var group = $('.joined', dir).closest('.control-group').addClass('warning');
		if (group.find(".alert").size() === 0) {
			group.append("<div class=\"alert alert-warning\">L'affichage public a été personnalisé. Vérifier qu'il correspond bien aux champs numériques.</div>");
		}
	}
}).on('keyup', "input.joined", function() {
	$(this).closest('.control-group').removeClass('warning').find('.alert').remove();
});

// Collection list depends on the choice of a resource

var collectionsContainer = $("#service-collections-dynamic");
if (collectionsContainer.size() > 0) {
	var collections = collectionsContainer.data('collections');
	if (collections) {
		addCollectionCheckboxes(collections);
	} else {
		$("#Service_ressourceIdComplete").on("autocompleteselect", function(event, ui) {
			$.ajax({
				url: '/collection/ajaxList',
				data: { ressourceId: ui.item.id },
				method: "get",
				success: function (data) {
					console.log("AJAX collection: ", data);
					addCollectionCheckboxes(data);
				}
			});
		});
	}
}

function addCollectionCheckboxes(collections) {
	$("#service-collections-dynamic").empty();
	if (collections.length === 0) {
		return;
	}
	$("#service-collections-dynamic").html(
		'<div class="control-group "><label class="control-label" for="Service_collectionIds">'
		+ 'Collections<span class="required">*</span></label>'
		+ '<div class="controls"></div></div>'
	);
	var container = $("#service-collections-dynamic .controls").first();
	var excludeImport = collectionsContainer.attr('data-excludeimport');
	$.each(collections, function(index, item) {
		var checkbox = $('<label for="Service_collectionIds_' + index + '"' + (item.disabled || item.imported ? ' title="collection gérée par import automatique"' : '') + '></label>')
			.text(item.label + " ")
			.prepend('<input id="Service_collectionIds_' + index
				+ '" value="' + item.id
				+ '" name="Service[collectionIds][]" type="checkbox" '
				+ ('checked' in item && item.checked ? 'checked="checked" ' : '')
				+ (item.disabled || (excludeImport && item.imported) ? 'disabled="disabled" ' : '')
				+ '/>'
			);
		var div = $('<div></div>').append(checkbox);
		if ('hint' in item && item.hint) {
			var hint = $('<i class="icon-info-sign"></i>');
			hint.attr('title', item.hint);
			div.append(" ").append(hint);
		}
		container.append(div);
	});
	console.log("Collections display...done!");
}
