<?php

/** @var Controller $this */
/** @var Service $model */
/** @var bool $direct */
/** @var Intervention $intervention */
/** @var ?array $titres */
/** @var bool $forceProposition */
/** @var bool $forbidCollectionsUpdate */
assert($this instanceof Controller);

$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'service-form',
		'enableAjaxValidation' => false,
		'hints' => Hint::model()->getMessages('Service'),
		'htmlOptions' => ['class' => 'well'],
		'type' => BootActiveForm::TYPE_HORIZONTAL,
	]
);
/** @var BootActiveForm $form */
?>

<div class="alert">
	<?php
	if ($direct) {
		echo "Les modifications seront appliquées immédiatement.";
	} else {
		echo "Les modifications que vous proposez devront être validées "
			,"par un membre partenaire de Mi@bel avant d'être appliquées.";
	}
	?>
</div>

<p class="help-block">Les champs avec <span class="required">*</span> sont obligatoires.</p>

<?php
echo $form->errorSummary($model);
if (!Yii::app()->user->isGuest) {
	echo UrlFetchableValidator::toggleUrlChecker($model);
}
if ($model->titreId) {
	$this->renderPartial(
		'/intervention/_errors',
		[
			'form' => $form,
			'intervention' => $intervention,
			'model' => $model->titre->revue,
		]
	);
}
?>
<fieldset>
	<legend>Accès en ligne</legend>
<?php
if ($model->isNewRecord) {
	if (isset($titres)) {
		echo $form->dropDownListRow($model, 'titreId', $titres, ["class" => "span6"]);
	} else {
		$this->renderPartial(
			'/global/_autocompleteField',
			[
				'model' => $model,
				'attribute' => 'titreId',
				'url' => '/titre/ajaxComplete',
				'form' => $form,
				'value' => (empty($model->titreId) ? '' : $model->titre->titre),
			]
		);
	}
	$this->renderPartial(
		'/global/_autocompleteField',
		[
			'model' => $model,
			'attribute' => 'ressourceId',
			'url' => '/ressource/ajaxComplete',
			'urlParams' => ['noimport' => 1],
			'form' => $form,
			'value' => (empty($model->ressourceId) ? '' : $model->ressource->nom),
		]
	);
	if (Yii::app()->user->isGuest) {
		echo "<div style=\"margin-bottom: 1.5ex;\">Si la ressource n'existe pas encore dans Mir@bel, "
			. CHtml::link("proposez sa création", ['ressource/create'])
			. " en indiquant dans le champ commentaire "
			. "quel accès en ligne et quelle revue vous souhaitiez signaler.</div>";
	} else {
		echo "<div style=\"margin-bottom: 1.2ex; margin-left: 180px;\">Si la ressource n'existe pas encore dans Mir@bel, "
			. CHtml::link("créer une nouvelle ressource", ['ressource/create'], ['target' => '_blank'])
			. " (nouvelle page).</div>";
	}
} else {
	// modifying an existing Service record
	if ($titres) {
		echo $form->dropDownListRow($model, 'titreId', $titres, ["class" => "span6"]);
	} else {
		?>
		<div class="control-group ">
			<label class="control-label"><?php echo $model->getAttributeLabel('titreId'); ?></label>
			<div class="controls">
				<span class="uneditable-input span8"><?php echo $model->titre->getFullTitle(); ?></span>
			</div>
		</div>
	<?php
	}
	if (Yii::app()->user->isGuest || $model->ressource->hasCollections()) {
		echo $form->hiddenField($model, 'ressourceId'); ?>
	<div class="control-group ">
		<label class="control-label"><?php echo $model->getAttributeLabel('ressourceId'); ?></label>
		<div class="controls">
			<span class="uneditable-input span8"><?php echo $model->ressource->getFullName(); ?></span>
		</div>
	</div>
		<?php
	} else {
		$this->renderPartial(
			'/global/_autocompleteField',
			[
				'model' => $model,
				'attribute' => 'ressourceId',
				'url' => '/ressource/ajaxComplete',
				'urlParams' => ['noimport' => 1],
				'form' => $form,
				'value' => (empty($model->ressourceId) ? '' : $model->ressource->nom),
			]
		);
		echo "<div style=\"margin-bottom: 1.2ex; margin-left: 180px;\">Si la ressource n'existe pas encore dans Mir@bel, "
			. CHtml::link("créer une nouvelle ressource", ['ressource/create'], ['target' => '_blank'])
			. " (nouvelle page).</div>";
	}
}
$isAdminCollections = Yii::app()->user->checkAccess('collection/admin');
if (!empty($model->ressourceId)) {
	$ressourceCollections = $model->ressource->getCollectionsDiffuseur(false, false);
	if ($forbidCollectionsUpdate) {
		// affichage fixe
		if ($model->collections) {
			?>
			<div class="control-group ">
				<label class="control-label">Collections du diffuseur</label>
				<div class="controls">
					<ul>
						<?php
						foreach ($model->collections as $c) {
							echo CHtml::tag(
								"li",
								[],
								CHtml::encode($c->nom)
									. " "
									. ($c->description ? CHtml::tag('i', ['class' => 'icon-info-sign', 'title' => $c->description], '') : '')
							);
						} ?>
					</ul>
				</div>
			</div>
			<?php
		}
	} elseif ($ressourceCollections) {
		// modification possible dans une liste fixée de collections
		$colls = [];
		$collectionAppliedIds = $model->id
			? Tools::sqlToPairs("SELECT collectionId, 1 FROM Service_Collection WHERE serviceId = {$model->id}")
			: [];
		foreach (array_merge($ressourceCollections, $model->collections) as $c) {
			if (!isset($colls[$c->id])) {
				if (!$c->visible) {
					$c->nom .= " (non visible)";
				}
				if ($c->importee) {
					$c->nom .= " (importée)";
				}
				$colls[$c->id] = [
					'label' => $c->nom,
					'id' => $c->id,
					'checked' => !empty($collectionAppliedIds[$c->id]),
					'hint' => $c->description,
					'disabled' => (!$c->visible || $c->importee) && !$isAdminCollections,
				];
			}
		}
		usort($colls, function ($a, $b) {
			return strcmp($a['label'], $b['label']);
		});
		echo '<div id="service-collections-dynamic" data-collections="'
			. CHtml::encode(json_encode(array_values($colls)))
			. '"></div>';
	}
} else {
	// modification possible dans une liste dynamique de collections
	echo '<div id="service-collections-dynamic" data-excludeimport="' . ($isAdminCollections ? 0 : 1) . '"></div>';
}
?>
</fieldset>

<fieldset>
	<legend>URLs</legend>
	<?php
	echo $form->textFieldRow($model, 'url', ['class' => 'span8']);
	echo $form->textFieldRow($model, 'alerteRssUrl', ['class' => 'span8']);
	echo $form->textFieldRow($model, 'alerteMailUrl', ['class' => 'span8']);
	echo $form->textFieldRow($model, 'derNumUrl', ['class' => 'span8']);
	?>
</fieldset>

<fieldset>
	<legend>Détails</legend>
	<?php
	echo $form->dropDownListRow($model, 'type', Service::$enumType, ['empty' => '']);
	echo $form->dropDownListRow($model, 'acces', Service::$enumAcces, ['empty' => '']);
	if ($model->notes) {
		?>
		<div class="control-group ">
			<label class="control-label">Notes du fournisseur</label>
			<div class="controls" style="line-height: 28px;">
				<?= CHtml::encode($model->notes) ?>
			</div>
		</div>
		<?php
	}
	?>
</fieldset>

<fieldset>
	<legend>Couverture</legend>
	<?php
	echo $form->checkBoxRow($model, 'lacunaire', ['label-position' => 'left']);
	echo $form->checkBoxRow($model, 'selection', ['label-position' => 'left']);
	?>
</fieldset>

<fieldset>
	<legend>Début de l'accès en ligne</legend>
	<?php
	echo $form->textFieldRow($model, 'dateBarrDebut');
	?>
	<div class="volno input-prepend">
		<div class="control-label">Format numérique (info interne)</div>
		<div class="controls">
			<span class="add-on">Vol. </span>
			<?php
			echo $form->textFieldInline($model, 'volDebut', ['prepend' => 'Vol. ', 'class' => 'input-large volno-num vol']);
			?>
			,
			<span class="add-on">no </span>
			<?php
			echo $form->textFieldInline($model, 'noDebut', ['prepend' => 'no ', 'class' => 'input-large volno-num no']);
			?>
		</div>
		<div style="clear:left;"></div>
		<?php
		echo $form->textFieldRow(
				$model,
				'numeroDebut',
				['class' => 'input-xxlarge joined', 'labelOptions' => ['label' => 'Format textuel (affichage public)']]
			);
		?>
	</div>
</fieldset>

<fieldset>
	<legend>Fin de l'accès en ligne</legend>
	<?php
	echo $form->textFieldRow($model, 'dateBarrFin');
	?>
	<div class="volno input-prepend">
		<div class="control-label">Format numérique (info interne)</div>
		<div class="controls">
			<span class="add-on">Vol. </span>
			<?php
			echo $form->textFieldInline($model, 'volFin', ['prepend' => 'Vol. ', 'class' => 'input-large volno-num vol']);
			?>
			,
			<span class="add-on">no </span>
			<?php
			echo $form->textFieldInline($model, 'noFin', ['prepend' => 'no ', 'class' => 'input-large volno-num no']);
			?>
		</div>
		<div style="clear:left;"></div>
		<?php
		echo $form->textFieldRow(
				$model,
				'numeroFin',
				['class' => 'input-xxlarge joined', 'labelOptions' => ['label' => 'Format textuel (affichage public)']]
			);
		?>
	</div>
</fieldset>

<fieldset>
	<legend>Date barrière</legend>
	<?php
	echo $form->textFieldRow($model, 'dateBarrInfo', ['class' => 'input-xxlarge']);
	if ($model->embargoInfo) {
		require_once Yii::getPathOfAlias('application.models.import') . '/Normalize.php'; ?>
		<p>
			L'accès est importé automatiquement, et notamment son champ <em>embargo info</em>, non modifiable.
		</p>
		<div class="control-group ">
			<label class="control-label">embargo_info</label>
			<div class="controls" style="line-height: 28px;">
				<?= $model->embargoInfo ?>
				<em>(<?= \import\Normalize::getReadableKbartEmbargo($model->embargoInfo) ?>)</em>
			</div>
		</div>
		<?php
	}
	if (Yii::app()->user->checkAccess('service/admin')) {
		echo $form->textFieldRow($model, 'embargoInfo', ['append' => ' réservé aux admins']);
	}
	?>
</fieldset>

<?php
$this->renderPartial(
		'/intervention/_contact',
		[
			'form' => $form,
			'intervention' => $intervention,
			'canForceValidating' => true,
			'forceProposition' => $forceProposition,
		]
	);
?>

<div class="form-actions">
	<?php
$this->widget(
	'bootstrap.widgets.BootButton',
	[
		'buttonType' => 'submit',
		'type' => 'primary',
		'label' => $model->isNewRecord ?
			($direct ? 'Créer' : 'Proposer cette création')
			: ($direct ? 'Modifier' : 'Proposer cette modification'),
	]
);
?>
</div>

<?php
$this->endWidget();
$this->loadViewJs(__FILE__);
?>
