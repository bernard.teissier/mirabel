<?php

/** @var Controller $this */
/** @var Service $model */
/** @var bool $direct */
/** @var Intervention $intervention */
/** @var array $titres */
/** @var bool $forceProposition */
/** @var bool $forbidCollectionsUpdate */

assert($this instanceof ServiceController);

$this->breadcrumbs = [];
if (!empty($model->titreId)) {
	$this->breadcrumbs = [
		'Revues' => ['/revue/index'],
		$model->titre->getFullTitle() => ['/revue/view', 'id' => $model->titre->revueId],
	];
} elseif (!empty($model->ressourceId)) {
	$this->breadcrumbs = [
		'Ressource' => ['/ressource/index'],
		$model->ressource->getFullName() => ['/ressource/view', 'id' => $model->ressourceId],
	];
}
$this->breadcrumbs[] = 'Modifier un accès en ligne';

if (!Yii::app()->user->isGuest) {
	$orphan = '';
	$countServices = Service::model()->countByAttributes(['ressourceId' => $model->ressourceId]);
	if ($countServices < 2) {
		$orphan = "\nSupprimer cet accès rendra la ressource orpheline";
	}
	$this->menu = [
		['label' => 'Supprimer cet accès', 'url' => '#',
			'linkOptions' => [
				'submit' => ['delete', 'id' => $model->id],
				'confirm' => "Êtes vous certain de vouloir supprimer ceci ?\n" . $orphan,
			],
		],
	];
}
?>

<h1>Modifier un accès en ligne</h1>

<?php
echo $this->renderPartial(
	'_form',
	[
		'model' => $model,
		'direct' => $direct,
		'intervention' => $intervention,
		'titres' => $titres,
		'forceProposition' => $forceProposition,
		'forbidCollectionsUpdate' => $forbidCollectionsUpdate,
	]
);
?>
