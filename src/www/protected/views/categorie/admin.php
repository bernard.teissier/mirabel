<?php

/** @var Controller $this */
/** @var Categorie $model */
/** @var int $filterByUserId */
assert($this instanceof Controller);

$this->pageTitle = "Admin thématique";
$this->breadcrumbs[] = 'Administration';

$jstreeConfig = new ExtJsTreeCore();
$jstreeConfig->data = ['url' => $this->createUrl('/categorie/ajaxNode', ['filterByUserId' => $filterByUserId])];
$jstreeConfig->translateToFr();
$jstreeConfig->checkCallback = true;
$jstree = new ExtJsTree($jstreeConfig, ["search", "sort", "state", "types"]);
$jstree->types = [
	'root' => [
		'icon' => '/images/icons/glyphicons-318-tree-deciduous.png',
	],
	'default' => [
		'max_depth' => 2,
	],
	'candidat' => [
		'max_depth' => 2,
		'icon' => '/images/icons/glyphicons-195-circle-question-mark.png',
	],
	'refus' => [
		'max_depth' => 2,
		'icon' => '/images/icons/glyphicons-200-ban-circle.png',
	],
];
$jstree->init(".jstree");

$ajaxUrls = [
	'create' => $this->createUrl('/categorie/create'),
	'rename' => $this->createUrl('/categorie/ajaxUpdate'),
	'update' => $this->createUrl('/categorie/update'),
	'delete' => $this->createUrl('/categorie/ajaxDelete'),
];
Yii::app()->clientScript->registerScript(
	'treeEditUrls',
	'$("#categorie-tree").jstree(true).urls = ' . CJavaScript::encode($ajaxUrls) . ';'
);

Yii::app()->clientScript->registerScriptFile(
	Yii::app()->assetManager->publish(__DIR__ . '/js/tree-admin.js')
);
?>

<h1>Administration de la thématique</h1>

<form id="jstree-search">
	<div>
		<input type="search" id="jstree-search-q" />
		<button type="submit">Chercher</button>
	</div>
	<div class="jstree-edit">
		<a href="<?= $this->createUrl('create') ?>" class="btn">Ajouter un thème…</a>
		<!--
		<button type="button" class="jstree-edit-create" title="Sélectionner d'abord le thème parent">Ajouter un thème</button>
		<button type="button" class="jstree-edit-rename" title="Sélectionner d'abord le thème à renommer">Renommer</button>
		<button type="button" class="jstree-edit-update" title="formulaire de modification du thème sélectionné">Modifier</button>
		<button type="button" class="jstree-edit-delete" title="Sélectionner d'abord le thème à supprimer">Supprimer</button>
		 -->
	</div>
	<div id="jstree-edit-message"></div>
</form>
<div id="categorie-tree" class="jstree"></div>
