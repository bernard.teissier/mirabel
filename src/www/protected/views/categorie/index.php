<?php

/** @var Controller $this */
/** @var CDataProvider $dataProvider */
assert($this instanceof Controller);

$this->pageTitle = "Thématique";

$jstreeConfig = new ExtJsTreeCore();
$jstreeConfig->data = ['url' => $this->createUrl('/categorie/ajaxNode')];
$jstreeConfig->translateToFr();
$jstree = new ExtJsTree($jstreeConfig, ["search", "sort", "state", "types", "revues"]);
$jstree->types = [
	'root' => [
		'icon' => '/images/icons/glyphicons-318-tree-deciduous.png',
	],
	'candidat' => [
		'icon' => '/images/icons/glyphicons-195-circle-question-mark.png',
	],
	'refus' => [
		'icon' => '/images/icons/glyphicons-200-ban-circle.png',
	],
];
$jstree->init(".jstree");

$jsUrl = Yii::app()->assetManager->publish(__DIR__ . '/js/index-tree.js');
Yii::app()->clientScript->registerScriptFile($jsUrl);
?>

<h1><?= $this->pageTitle ?></h1>

<?php if (Yii::app()->user->isGuest) { ?>
<p class="alert alert-warning">
	Les revues de Mir@bel peuvent désormais être classées par thème.
	<br />
	Cet arbre dépliable permet de visualiser l'organisation des thèmes
	et de rebondir sur les revues auxquelles un thème a été attribué.
</p>
<?php } else { ?>
<p class="alert alert-warning">
	Cet arbre dépliable permet de visualiser l'organisation des thèmes.
	Les thèmes candidats, en attente de confirmation, sont préfixés d'une icône en point d'interrogation.
</p>
<?php } ?>

<form id="jstree-search">
  <input type="search" id="jstree-search-q" placeholder="Recherche dans les titres et descriptions des thèmes" class="input-xxlarge" />
  <button type="submit">Chercher</button>
</form>
<div id="categorie-tree" class="jstree avec-revues" data-revues-direct="<?= Yii::app()->user->isGuest ? "0" : "1" ?>"></div>
