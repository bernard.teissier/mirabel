<?php

/** @var Controller $this */
/** @var CDataProvider $dataProvider */
assert($this instanceof Controller);

$this->pageTitle = "Thèmes refusés";
$this->breadcrumbs[] = "Thèmes refusés";
?>

<h1><?= $this->pageTitle ?></h1>

<?php
$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'utilisateur-grid',
		'dataProvider' => $dataProvider,
		'filter' => null, // null to disable
		'columns' => [
			'categorie', 'parent.categorie:text:Parent', 'hdateCreation:date', 'hdateModif:date', 'modifiePar.login:text:Auteur/modif',
			['class' => 'BootButtonColumn', 'template' => '{update}'],
		],
		'ajaxUpdate' => false,
	]
);
?>
