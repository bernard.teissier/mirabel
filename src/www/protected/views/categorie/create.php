<?php

/** @var Controller $this */
/** @var Categorie $model */
assert($this instanceof Controller);

$this->pageTitle = "Nouveau thème";
$this->breadcrumbs[] = 'Nouveau thème';
?>

<h1><?= $this->pageTitle ?></h1>

<?php
echo $this->renderPartial('_form', ['model' => $model]);
?>
