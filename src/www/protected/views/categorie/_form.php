<?php

/** @var Controller $this */
/** @var Categorie $model */
assert($this instanceof Controller);

$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'categorie-form',
		'enableAjaxValidation' => false,
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'hints' => Hint::model()->getMessages('Categorie'),
	]
);
/** @var BootActiveForm $form */
?>

<?php
echo $form->errorSummary($model);

echo $form->textFieldRow($model, 'categorie');
echo $this->renderPartial(
	'/global/_autocompleteField',
	['model' => $model, 'attribute' => 'parentId', 'form' => $form, 'url' => '/categorie/complete', 'urlParams' => ['minDepth' => 1], 'value' => ($model->parentId ? $model->parent->categorie : ''),
	]
);
echo $form->textAreaRow($model, 'description', ['cols' => 74, 'rows' => 10, 'class' => 'span6']);
if (Yii::app()->user->checkAccess('indexation')) {
	echo $form->dropDownListRow($model, 'role', Categorie::$enumRole);
} else {
	echo '<p class="alert alert-warning">Ce thème aura un statut de <em>candidat</em>.'
		. " Un utilisateur chargé de l'indexation pourra le convertir en thème public.</p>";
}
?>
<div class="form-actions">
	<?php
$this->widget(
	'bootstrap.widgets.BootButton',
	[
		'buttonType' => 'submit',
		'type' => 'primary',
		'label' => $model->isNewRecord ? 'Créer ce thème' : 'Enregistrer',
	]
);
?>
</div>

<?php
$this->endWidget();
?>
