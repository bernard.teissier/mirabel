<?php

/** @var Controller $this */
/** @var Categorie $model */
assert($this instanceof Controller);

$this->pageTitle = "Thème " . $model->categorie;

$this->breadcrumbs[] = $model->categorie;
?>

<h1>
	Thème <em><?php echo CHtml::encode($model->categorie); ?></em>
</h1>

<?php
$ancestors = '';
foreach ($model->getAncestors() as $a) {
	$ancestors .= '<li>'
		. CHtml::link($a->categorie, ['/categorie/view', 'id' => $a->id])
		. '</li>';
}

$revues = '';
foreach ($model->categorieRevues as $cr) {
	$revues .= '<li>'
		. $cr->revue->getSelfLink() . " (" . $cr->hdateModif . ")"
		. '</li>';
}

$attributes = [
	'id',
	'parentId',
	'categorie',
	'description:ntext',
	'role',
	'chemin',
	[
		'label' => 'Chemin explicite',
		'type' => 'raw',
		'value' => ($ancestors ? '<ol>' . $ancestors . '</ol>' : ''),
	],
	'profondeur',
	'hdateCreation:datetime',
	'hdateModif:datetime',
	'modifiePar.login:text:Modifié par',
];
if (!Yii::app()->user->isGuest) {
	$attributes[] = [
		'label' => 'Revues',
		'type' => 'raw',
		'value' => ($revues ? '<ol>' . $revues . '</ol>' : ''),
	];
}

$this->widget(
	'bootstrap.widgets.BootDetailView',
	[
		'data' => $model,
		'attributes' => $attributes,
	]
);
?>
