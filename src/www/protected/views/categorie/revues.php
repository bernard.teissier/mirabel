<?php

/** @var Controller $this */
/** @var Categorie $model */
/** @var SearchTitre $search */
/** @var SphinxDataProvider $results */
assert($this instanceof Controller);

$this->pageTitle = "Thème " . $model->categorie;

$this->breadcrumbs = [
	'Thèmes' => ['index'],
];
if ($model->profondeur > 2) {
	$this->breadcrumbs[$model->parent->categorie] = $model->parent->getSelfUrl();
}
$this->breadcrumbs[] = $model->categorie;

$this->sidebarInsert .= $this->renderPartial('/revue/_legend', null, true);
?>

<h1>
	<small>Thématique</small>
	<?= CHtml::encode($model->categorie); ?>
	<small>dans les revues de Mir@bel</small>
</h1>

<p>
	<?php
	foreach (explode("\n", $model->description) as $expr) {
		echo CHtml::tag("span", ["class" => "label"], CHtml::encode($expr)), " ";
	}
	?>
</p>

<?php
$subthemes = $model->with('categorieStats')->children;
if ($subthemes) { ?>
<form action="<?= CHtml::encode($this->createUrl('/revue/search')) ?>" method="GET" id="categorie-revues">
	<div id="search-summary">
		Le thème <em><?= CHtml::encode($model->categorie); ?></em> est composé de
		<ul>
			<li>
				<input type="hidden" name="q[cNonRec][]" value="<?= $model->id ?>" />
				<input type="checkbox" name="q[categorie][]" value="<?= $model->id ?>" checked />
				<?= CHtml::link(
	'<strong>' . CHtml::encode($model->categorie) . '</strong>'
						. ' <span class="num-revues label">' . $model->categorieStats->numRevuesInd . ' revues</span>',
	["/revue/search", 'q' => ['categorie' => [$model->id], 'cNonRec' => [$model->id]]]
) ?>
			</li>
			<?php foreach ($subthemes as $subtheme) { ?>
			<li>
				<input type="checkbox" name="q[categorie][]" value="<?= $subtheme->id ?>" checked />
				<?= CHtml::link(
	CHtml::encode($subtheme->categorie)
						. ' <span class="num-revues label">' . $subtheme->categorieStats->numRevuesInd . ' revues</span>',
	$subtheme->getSelfUrl()
					//["/revue/search", 'q' => ['categorie' => [$subtheme->id], 'cNonRec' => [$subtheme->id]]]
) ?>
			</li>
			<?php } ?>
		</ul>
		<button type="submit">Chercher les revues de ces thèmes</button>
	</div>
</form>
<?php } ?>

<section id="extended-search-results">
	<h2>Revues</h2>
	<?php
	$this->widget(
	'bootstrap.widgets.BootListView',
	[
		'dataProvider' => $results,
		'itemView' => '/global/_sphinxTitre',
		'viewData' => [],
	]
);
	?>
</section>

<section>
	<h2 class="collapse-toggle">Rechercher dans ces revues…</h2>
	<div class="collapse-target">
		<?php
		$this->renderPartial(
		'/revue/_search',
		['model' => $search, 'partenaireId' => $this->institute ? $this->institute->id : null]
	);
		?>
	</div>
</section>
