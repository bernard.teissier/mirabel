<?php

/** @var Controller $this */
/** @var Categorie $model */
assert($this instanceof Controller);

$this->pageTitle = "Modifier le thème";
$this->breadcrumbs[$model->categorie] = ['view', 'id' => $model->id];
$this->breadcrumbs[] = 'Modifier';
?>

<h1>Modifier le thème <em><?php echo CHtml::encode($model->categorie); ?></em></h1>

<?php
echo $this->renderPartial('_form', ['model' => $model]);
?>
