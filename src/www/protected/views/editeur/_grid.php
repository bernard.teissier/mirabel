<?php

use models\sphinx\Editeurs;

/** @var Controller $this */
/** @var \models\searches\EditeurSearch $model */
/** @var int $pagination */
/** @var bool $displaySearchSummary */

if (!$model->validate() || $model->isEmpty()) {
	return;
}

try {
	$search = $model->search($pagination);
	$search->getTotalItemCount();
} catch (\Throwable $ex) {
	echo
		<<<EOP
		<p class="alert alert-danger">Cette recherche n'est pas valable. Le terme recherché doit contenir au moins un mot.</p>
		EOP;
	return;
}

if ($displaySearchSummary) {
	?>
	<p id="search-summary">
		<strong>Critères :</strong>
		<?= CHtml::encode($model->getSummary()) ?>
	</p>
	<?php
}
?>

<div style="clear: both;"></div>
<?php
$pays = Tools::sqlToPairs("SELECT id, nom FROM Pays");
$columns = array(
	[
		'name' => 'nom',
		'header' => 'Éditeur',
		'type' => 'raw',
		'value' => function (Editeurs $e) {
			return $e->getRecord()->getSelfLink();
		}
	],
	[
		'name' => 'nbrevues',
		'header' => 'Revues',
		'type' => 'raw',
		'value' => function (Editeurs $e){
			return CHtml::link($e->nbrevues, ['/revue/search', 'q[editeurId][]' => $e->id]);
		},
	],
	[
		'name' => 'nbtitresvivants',
		'header' => 'Revues vivantes',
		'value' => function (Editeurs $e) {
			return $e->nbtitresvivants;
		},
		'visible' => !Yii::app()->user->isGuest,
	],
	[
		'header' => 'Pays',
		'type' => 'raw',
		'value' => function (Editeurs $e) use ($pays) {
			if ($e->paysid) {
				return CHtml::link(
					CHtml::encode($pays[$e->paysid]),
					['/editeur/pays', 'paysId' => $e->paysid, 'nom' =>  Norm::urlParam($pays[$e->paysid])]
				);
			}
			return '';
		},
		'visible' => !$model->pays,
	],
	[
		'header' => 'Repère géo',
		'value' => function (Editeurs $e) {
			return $e->getRecord()->geo;
		}
	],
);
$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	array(
		'id' => 'editeur-grid',
		'dataProvider' => $search,
		'filter' => null,
		'columns' => $columns,
		'ajaxUpdate' => false,
		'rowCssClassExpression' => function ($index, Editeurs $e) {
			return HtmlHelper::linkItemClass('editeur', $e->getSuiviIds());
		},
		'summaryText' => $pagination > 0
			? 'Résultats de {start} à {end} sur {count} éditeurs'
			: '{count} éditeurs',
	)
);
