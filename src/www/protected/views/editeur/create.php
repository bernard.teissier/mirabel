<?php

/** @var Controller $this */
/** @var Editeur $model */
/** @var bool $direct */
/** @var Intervention $intervention */
assert($this instanceof Controller);

$this->breadcrumbs = [
	'Editeurs' => ['index'],
	'Créer',
];
$this->pageTitle = "Nouvel éditeur";
?>

<h1>Nouvel éditeur</h1>

<?php
echo $this->renderPartial(
	'_form',
	['model' => $model, 'direct' => $direct, 'intervention' => $intervention]
);
?>
