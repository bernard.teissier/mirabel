<?php

/** @var Controller $this */
/** @var Editeur $model */
/** @var bool $direct */
/** @var Intervention $intervention */
assert($this instanceof Controller);

$this->pageTitle = "Modifier l'éditeur " . $model->nom;

$this->breadcrumbs=[
	'Editeurs' => ['index'],
	$model->getFullName() => ['view', 'id' => $model->id],
	'Modifier',
];
?>

<h1>Modifier l'éditeur <em><?php echo CHtml::encode($model->getFullName()); ?></em></h1>

<?php
echo $this->renderPartial(
	'_form',
	['model' => $model, 'direct' => $direct, 'intervention' => $intervention]
);
?>
