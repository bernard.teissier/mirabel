<?php

/** @var Controller $this */
/** @var Editeur $model */
/** @var Upload $upload */
assert($this instanceof Controller);

$this->pageTitle = "Éditeur {$model->nom} - Logo";

$this->breadcrumbs=[
	'Editeurs' => ['index'],
	$model->getFullName() => ['view', 'id' => $model->id],
	'Logo',
];
?>

<h1>Logo de l'éditeur <em><?php echo CHtml::encode($model->getFullName()); ?></em></h1>

<p class="alert alert-info">
	Le logo sera redimensionné après téléchargement.
	<?php
	if ($model->partenaire) {
		echo "<br />Le logo sera commun avec le compte partenaire associé à cet éditeur.";
	}
	?>
</p>

<div>
	<?php
	$img = $model->getLogoImg();
	if ($img) {
		echo "<div style=\"display: inline-block;\">Logo actuel : $img</div>";
		if (!$model->logoUrl) {
			echo ' <div style="display: inline-block;">'
				. HtmlHelper::postButton("Supprimer", ['logo', 'id' => $model->id], ['delete' => 1])
				. '</div>';
		}
	} else {
		echo "<strong>Pas encore de logo.</strong>";
	}
	?>
</div>

<section style="margin-top: 2ex">
	<h2>Utiliser un logo déjà en ligne</h2>
	<?php
	$form = $this->beginWidget(
		'bootstrap.widgets.BootActiveForm',
		[
			'id' => 'editeur-logo-url-form',
			'action' => ['logo', 'id' => $model->id],
			'enableAjaxValidation' => false,
			'type' => BootActiveForm::TYPE_HORIZONTAL,
			'hints' => Hint::model()->getMessages('Editeur'),
		]
	);
	/** @var BootActiveForm $form */
	echo $form->errorSummary($model);
	echo $form->textFieldRow($model, 'logoUrl', ['class' => 'span11']);
	?>
	<div class="form-actions">
		<button class="btn btn-primary" type="submit">Télécharger ce logo</button>
		<?php
		if ($model->logoUrl) {
			echo "   <span>Une URL vide effacera le logo.</span>";
		}
		?>
	</div>
	<?php $this->endWidget() ?>
</section>

<section>
	<h2>Envoyer un fichier image</h2>
	<?php
	$formUp = $this->beginWidget(
		'bootstrap.widgets.BootActiveForm',
		[
			'id' => 'editeur-logo-upload-form',
			'action' => ['logo', 'id' => $model->id],
			'enableAjaxValidation' => false,
			'type' => BootActiveForm::TYPE_HORIZONTAL,
			'hints' => Hint::model()->getMessages('Editeur'),
			'htmlOptions' => ['enctype' => 'multipart/form-data'],
		]
	);
	/** @var BootActiveForm $formUp */
	echo $formUp->errorSummary($upload);
	echo $formUp->fileFieldRow($upload, 'file', ['class' => 'span11']);
	?>
	<div class="form-actions">
		<button class="btn btn-primary" type="submit">Envoyer ce fichier</button>
	</div>
	<?php $this->endWidget() ?>
</section>
