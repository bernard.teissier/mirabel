<?php

/** @var Controller $this */
/** @var \models\searches\EditeurSearch $model */

$this->breadcrumbs = array(
	'Éditeurs' => ['/editeur'],
	'Recherche'
);
$this->pageTitle = "Recherche d'éditeurs";
if (!$model->isEmpty()) {
	$this->menu[] = [
		'label' => 'Nouvelle recherche ↓',
		'url' => '#editeur-search-form',
	];

	$this->sidebarInsert .= $this->renderPartial('_search-export', ['search' => $model], true);
}
?>

<?= $model->isEmpty() ? '' : $this->renderPartial('_suivi-legend') ?>

<h1><?= CHtml::encode($this->pageTitle) ?></h1>

<?php
if ($model->isEmpty()) {
	echo '<p>Il est aussi possible de ' . CHtml::link("chercher des revues", ['/revue/search']) . '.</p>';
}
?>

<?= $this->renderPartial(
	'_grid',
	[
		'model' => $model,
		'pagination' => 25,
		'displaySearchSummary' => true,
	]
) ?>

<?= $this->renderPartial('_search-form', ['model' => $model]) ?>
