<?php

/** @var Controller $this */
/** @var Editeur $editeur */
/** @var array $checks */
assert($this instanceof Controller);

$this->pageTitle = $editeur->nom . " - Vérification des liens";

$this->breadcrumbs = [
	'Éditeurs' => ['index'],
	$editeur->nom => ['view', 'id' => $editeur->id],
	'Vérification des liens',
];

$this->menu = [
	['label' => 'Liste des éditeurs', 'url' => ['index']],
	['label' => 'Cet éditeur', 'url' => ['view', 'id' => $editeur->id]],
];

$checks['urls'][''] = true;
$buildTr = HtmlHelper::builderCheckLinks($checks['urls']);
?>

<h1>
	Vérification des liens de l'éditeur <?= CHtml::encode($editeur->nom) ?>
</h1>

<h2>URLs vérifiées</h2>
<table class="table table-bordered table-condensed">
	<thead>
		<tr>
			<th>URL</th>
			<th>Résultat</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach (array_keys($checks['urls']) as $url) {
			echo $buildTr($url);
		}
		?>
	</tbody>
</table>

<?php
