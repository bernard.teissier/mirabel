<?php

/** @var Controller $this */
/** @var Editeur $model */
/** @var bool $embedded */
/** @var bool $direct */
/** @var Intervention $intervention */
assert($this instanceof Controller);

echo '<p style="margin-top: 1ex;">Veuillez consulter la '
	. CHtml::link(
		"documentation détaillée au format PDF",
		Yii::app()->baseUrl . '/public/Procedure_Editeurs.pdf',
		['target' => '_blank', 'title' => "PDF dans une nouvelle page"]
	)
	. " (nouvelle page).</p>";

if ($model->isNewRecord) {
	$url = $this->createUrl('/editeur/create');
} else {
	$url = $this->createUrl('/editeur/update', ['id' => $model->id]);
}
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'editeur-form',
		'action' => $url,
		'enableAjaxValidation' => false,
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'hints' => Hint::model()->getMessages('Editeur'),
		'htmlOptions' => ['class' => 'well'],
	]
);
/** @var BootActiveForm $form */
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/ckeditor/ckeditor.js');
Yii::app()->clientScript->registerScript('ckeditorall', "CKEDITOR.replaceAll('htmleditor');");
?>

<?php
if (empty($embedded)) {
	?>
<div class="alert">
	<?php
	if ($direct) {
		echo "Les modifications seront appliquées immédiatement.";
	} else {
		echo "Les modifications que vous proposez devront être validées "
			, "par un membre partenaire de Mi@bel avant d'être appliquées.";
	}
	?>
</div>
<?php
}
?>

<?php
echo HtmlHelper::errorSummary($model);
if (!Yii::app()->user->isGuest) {
	$hasUrlErrors = ($model->getError('url') !== null) || $model->getLiens()->hasUrlError();
	echo UrlFetchableValidator::toggleUrlChecker($model, $hasUrlErrors);
}
$this->renderPartial(
	'/intervention/_errors',
	[
		'form' => $form,
		'intervention' => $intervention,
		'model' => $model,
	]
);

echo $form->textFieldRow($model, 'nom', ['class' => 'span11']);
if (isset($model->confirm) && $model->confirm === false) {
	echo $form->checkBoxRow($model, 'confirm');
}
echo $form->textFieldRow($model, 'prefixe', ['class' => 'span2']);
echo $form->textFieldRow($model, 'sigle', ['class' => 'span4']);
echo $form->textAreaRow($model, 'description', ['rows' => 6, 'cols' => 50, 'class' => 'span11']);
if ($model->id && $model->partenaire) {
	echo $form->textAreaRow($model, 'presentation', ['rows' => 12, 'cols' => 80, 'class' => 'span11 htmleditor']);
}
echo $form->textFieldRow($model, 'url', ['class' => 'span11']);
echo $form->dropDownListRow($model, 'paysId', CHtml::listData(Pays::model()->sorted()->findAll(), 'id', 'nom'), ['empty' => '—']);
echo $this->renderPartial(
	'/global/_autocompleteDistinct',
	['model' => $model, 'attribute' => 'geo', 'form' => $form]
);
echo $form->textFieldRow($model, 'idref', ['size' => '10']);
echo $form->textFieldRow($model, 'sherpa', ['size' => '10']);
?>

<fieldset class="other-links">
	<legend>Liens</legend>
	<?php
	$this->renderPartial(
		'/global/_form_links',
		[
			'form' => $form,
			'model' => $model,
		]
	);
	?>
</fieldset>

<input type="hidden" name="editeur-duplicate-id" id="editeur-duplicate-id" value="<?= uniqid() ?>" />

<?php
if (empty($embedded)) {
	$this->renderPartial('/intervention/_contact', ['form' => $form, 'intervention' => $intervention, 'canForceValidating' => !$model->isNewRecord]);
	?>
	<div class="form-actions">
		<?php
		$this->widget(
			'bootstrap.widgets.BootButton',
			[
				'buttonType' => 'submit',
				'type' => 'primary',
				'label' => $model->isNewRecord ?
					($direct ? 'Créer cet éditeur' : "Proposer de créer cet éditeur")
					: ($direct ? 'Enregistrer' : 'Proposer cette modification'),
			]
		); ?>
	</div>
	<?php
}

$this->endWidget('editeur-form');
?>
