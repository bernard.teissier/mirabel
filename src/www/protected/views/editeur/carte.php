<?php

/** @var Controller $this */
/** @var array $mapConfig */
assert($this instanceof Controller);

$this->pageTitle = "Carte des éditeurs";

$this->breadcrumbs = [
	'Éditeurs' => ['index'],
	'Carte',
];

$this->appendToHtmlHead('<link rel="stylesheet" href="/js/leaflet/leaflet.css"');
Yii::app()->clientScript->registerScriptFile("/js/leaflet/leaflet.js");
Yii::app()->clientScript->registerScriptFile("/js/countries.js");
Yii::app()->clientScript->registerScriptFile('/js/world-map.js');

Yii::app()->clientScript->registerScript(
	'mapinit',
	'drawWorldMap(' . CJavaScript::encode($mapConfig) . ');'
);
?>

<h1>Localisation des éditeurs recensés dans Mir@bel</h1>

<div id="world-map" style="height: 600px;"></div>

<noscript>
	<p class="alert alert-warning">
		Cette carte ne s'affiche pas si JavaScript est désactivé dans votre navigateur.
	</p>
</noscript>

<p>
	Cliquer sur un pays vous permet d'afficher les détails
	puis de rebondir sur les listes correspondantes d'éditeurs et de revues.
</p>
