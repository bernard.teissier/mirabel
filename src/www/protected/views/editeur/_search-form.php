<?php

/** @var Controller $this */
/** @var \models\searches\EditeurSearch $model */

assert($this instanceof EditeurController);
?>
<section id="editeur-search-form">
	<h2>Nouvelle recherche</h2>
	<?php
	$form = $this->beginWidget(
		'bootstrap.widgets.BootActiveForm',
		[
			'id' => 'editeur-search-form',
			'action' => $this->createAbsoluteUrl('/editeur/search'),
			'method' => 'get',
			'enableAjaxValidation' => false,
			'type' => BootActiveForm::TYPE_HORIZONTAL,
			'htmlOptions' => ['class' => 'well'],
			'hints' => Hint::model()->getMessages('editeur/search'),
		]
	);
	/** @var BootActiveForm $form */
	?>

	<?= HtmlHelper::errorSummary($model) ?>

	<?php
	echo $form->textFieldRow($model, 'nom', ['class' => 'span11']);
	$countries = Tools::sqlToPairs("SELECT p.id, p.nom FROM Pays p JOIN Editeur e ON e.paysId = p.id GROUP BY p.id ORDER BY p.nom");
	echo $form->dropDownListRow($model, 'pays', $countries, ['empty' => '—']);
	?>
	<div class="control-group">
		<label class="control-label">Identifiants</label>
		<div class="controls">
			<div>
			<div class="input-prepend">
				<span class="add-on" style="min-width: 9ex; text-align: right;">IdRef</span>
				<?= $form->textFieldInline($model, 'idref', ['placeholder' => '* OU ! OU IdRef']) ?>
			</div>
			</div>
			<div>
			<div class="input-prepend">
				<span class="add-on" style="min-width: 9ex; text-align: right;">ID Sherpa</span>
				<?= $form->textFieldInline($model, 'sherpa', ['placeholder' => '* OU ! OU ID Sherpa']) ?>
			</div>
			</div>
		</div>
	</div>
	<?php
	echo $form->checkBoxRow($model, 'vivants', ['label' => "Restreindre aux éditeurs ayant des revues vivantes"]);
	?>

	<fieldset>
		<legend>Dans Mir@bel</legend>
		<?php
		echo $form->textFieldRow($model, 'hdateModif', ['placeholder' => '> 2021-02 OU 2020 OU 2015 2019']);
		echo $form->textFieldRow($model, 'hdateVerif', ['placeholder' => '> 2021-02 OU 2020 OU 2015 2019']);
		?>
	</fieldset>

	<div class="form-actions">
		<button type="submit" class="btn btn-primary">Rechercher</button>
	</div>

	<?php
	$this->endWidget('editeur-form');
	?>
</section>
