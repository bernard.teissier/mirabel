<?php

/** @var Controller $this */
/** @var CDataProvider $dataProvider */
/** @var int $suivi partenaireId */
/** @var string $lettre */
/** @var string $searchHash */
/** @var int $totalEditeurs */
assert($this instanceof Controller);

$this->breadcrumbs = [
	'Éditeurs',
];
$this->pageTitle = "Éditeurs";

$dataProvider->getData();
?>

<div class="legende">
	<h3>Légende</h3>
	<ul>
		<?php
		if (Yii::app()->user->isGuest) {
			?>
			<li class="editeur suivi-other">Suivi par un partenaire</li>
			<?php
		} else {
			?>
			<li class="editeur suivi-self">Suivi par son propre partenaire</li>
			<li class="editeur suivi-other">Suivi par un autre partenaire</li>
			<?php
		}
		?>
	</ul>
</div>
<h1>Éditeurs<?php
if (!empty($lettre)) {
	echo " — " . $lettre;
}
?></h1>

<figure class="link-to-map">
	<?= CHtml::link(
	CHtml::image('/public/carte_editeurs_vignette.png', "Carte des $totalEditeurs éditeurs dans Mir@bel"),
	['/editeur/carte']
) ?>
	<figcaption>Carte des <?= $totalEditeurs ?> éditeurs</figcaption>
</figure>
<div>
	<?= CHtml::link("Recherche avancée d'éditeurs", ['/editeur/search']) ?>
</div>

<?php
Tools::printSubPagination($dataProvider);
?>

<?php
if (!$suivi) {
	echo CHtml::link(
		CHtml::image(Yii::app()->baseUrl . "/images/plus.png", "Ajout d'éditeur"),
		['create'],
		[
			'style' => 'float: right; margin-right: 40px; clear: right;',
			'title' => (Yii::app()->user->checkAccess('editeur/create-direct') ? "Créer" : "Proposer") . " un nouvel éditeur",
		]
	);
}
?>

<table class="items table list-results">
	<thead>
		<th>Nom</th>
		<th>Revues</th>
	</thead>
	<tbody>
		<?php
		foreach ($dataProvider->getData() as $result) {
			echo '<tr><td>'
				. HtmlHelper::editeurLinkItem($result['attrs'], $searchHash)
				. '</td><td>'
				. CHtml::link(
					$result['attrs']['nbrevues'] . ' revue' . ($result['attrs']['nbrevues'] > 1 ? 's' : ''),
					['/revue/search', 'SearchTitre[editeurId][]' => $result['attrs']['id']]
				) . '</td></tr>';
		}
		?>
	</tbody>
</table>

<?php Tools::printSubPagination($dataProvider); ?>
