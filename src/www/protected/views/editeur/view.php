<?php

/** @var Controller $this */
/** @var Editeur $model */
/** @var bool $forceRefresh */
/** @var ?SearchNavigation $searchNavigation */
assert($this instanceof Controller);

$this->pageTitle = "Éditeur : " . $model->getFullName();

$this->breadcrumbs = [
	'Éditeurs' => ['index'],
	$model->getFullName(),
];

$imgUrl = Yii::app()->getAssetManager()
	->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview';

$encodedTitle = CHtml::encode($model->getFullName());
$encodedCanonicalUrl = CHtml::encode($model->getSelfAbsoluteUrl());
$this->appendToHtmlHead(
	<<<EOL
			<meta name="description" content="Mirabel liste les accès en ligne aux revues de l'éditeur $encodedTitle" lang="fr" />
			<meta name="keywords" content="éditeur, $encodedTitle, revue, accès en ligne, texte intégral, sommaire, périodique" lang="fr" />
			<link rel="schema.dcterms" href="http://purl.org/dc/terms/" />
			<meta name="dcterms.title" content="Mirabel : éditeur $encodedTitle" />
			<meta name="dcterms.subject" content="revue, accès en ligne, texte intégral, sommaire, périodique, éditeur, $encodedTitle" />
			<meta name="dcterms.language" content="fr" />
			<meta name="dcterms.creator" content="Mirabel" />
			<meta name="dcterms.publisher" content="Sciences Po Lyon" />

			<meta property="og:title" content="Mir@bel - $encodedTitle" />
			<meta property="og:type" content="website" />
			<meta property="og:url" content="$encodedCanonicalUrl" />

			<meta name="twitter:card" content="summary" />
			<meta name="twitter:site" content="@mirabel_revues" />
			<meta name="twitter:title" content="Mir@bel - $encodedTitle" />
			<meta name="twitter:description" content="Mirabel liste les accès en ligne aux revues de l'éditeur $encodedTitle" />
		EOL
);
$encodedLogoUrl = CHtml::encode(
	$model->getLogoUrl(false)
	?: Yii::app()->getBaseUrl(true) . '/images/logo-mirabel-carre.png'
);
$this->appendToHtmlHead(
	<<<EOL
			<meta name="twitter:image" content="$encodedLogoUrl" />
			<meta property="og:image" content="$encodedLogoUrl" />
		EOL
);


$this->renderPartial('/global/_interventionLocal', ['model' => $model]);
?>

<?= $this->renderPartial(
	'/global/_search-navigation',
	['searchNavigation' => $searchNavigation, 'currentId' => $model->id, 'attr' => 'nomcomplet', 'anchor' => '#search-editeurs']
); ?>

<h1>
	Éditeur <em><?= CHtml::encode($model->getFullName()) ?></em>
</h1>

<?php
$presentation = $model->getLogoImg(true, $forceRefresh);
if ($model->presentation) {
	$presentation .= "\n" . (new CHtmlPurifier())->purify($model->presentation);
}
if ($presentation) {
	echo '<div id="editeur-presentation">' . $presentation . "</div>\n";
}

if (Yii::app()->user->isGuest) {
	echo '<div class="guest-operations">';
	echo CHtml::link("Proposer une modification", ['update', 'id' => $model->id]);
	echo '</div>';
}

$attributes = [
	[
		'name' => 'nom',
		'value' => $model->getLongName(),
	],
	'sigle',
	[
		'name' => 'description',
		'type' => 'raw',
		'value' => nl2br(CHtml::encode($model->description)),
	],
	'url:url',
	'liensExternes:raw:Autres liens',
	'liensInternes:raw:Liens Mir@bel',
	[
		'name' => 'paysId',
		'value' => ($model->paysId ? $model->pays->nom : ''),
	],
	'geo',
	[
		'label' => 'Identifiants',
		'type' => 'raw',
		'value' => ($model->idref
			? CHtml::link(
				CHtml::image('/images/logo-idref.png', "IdRef {$model->idref}", ['height' => 30, 'width' => 90, 'style' => 'width:inherit; height:16px;']),
				"https://www.idref.fr/{$model->idref}",
				['title' => "IdRef {$model->idref}"]
			) : "")
			. "   "
			. ($model->sherpa ?
				CHtml::link(
					CHtml::image('/images/logo-jisc.png', "Sherpa Romeo ID éditeur {$model->sherpa}", ['height' => 16, 'width' => 16]) . "Sherpa Romeo",
					"https://v2.sherpa.ac.uk/id/publisher/{$model->sherpa}",
					['title' => "Sherpa Romeo ID éditeur {$model->sherpa}"]
				) : "")
			,
		'visible' => $model->idref || $model->sherpa,
	],
];
$this->widget(
	'bootstrap.widgets.BootDetailView',
	[
		'data' => $model,
		'attributes' => $attributes,
		'hideEmptyLines' => true,
	]
);
?>

<h2>Revues de cet éditeur dans Mir@bel</h2>
<p>
	<?php
	$num = $model->countRevues();
	if ($num) {
		echo CHtml::link(
			$num . ' revue' . ($num > 1 ? 's' : '') . ' de cet éditeur',
			['/revue/search', 'SearchTitre[editeurId][]' => $model->id]
		) . ($num > 1 ? ' sont présentes': ' est présente') . ' dans Mir@bel';
	} else {
		echo "Cet éditeur n'a aucun titre dans Mir@bel";
	}
	?>.
</p>

<?php
if ($model->partenaire && $model->partenaire->statut !== 'inactif') {
		echo $this->renderPartial('/partenaire/_suivi', ['partenaire' => $model->partenaire]);
	}
?>

<?php
$this->renderPartial('/global/_modif-verif', ['target' => $model]);
