<?php

/** @var Controller $this */
/** @var models\searches\EditeurSearch $search */
assert($this instanceof Controller);

?>
<div class="well">
	<h4 title="Export au format tableur CSV de cette liste des éditeurs">Exporter les éditeurs</h4>
	<form action="<?php echo $this->createUrl('/editeur/export'); ?>" method="get" class="form-horizontal">
		<?php
		foreach ($search->attributes as $k => $value) {
			if ($value) {
				if (is_array($value)) {
					echo CHtml::hiddenField("q[$k][]", join(",", $value));
				} else {
					echo CHtml::hiddenField("q[$k]", $value);
				}
			}
		}
		?>
		<button type="submit">Fichier CSV<br /></button>
	</form>
</div>
