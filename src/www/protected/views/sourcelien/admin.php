<?php

/** @var Controller $this */
/** @var Sourcelien $model */
assert($this instanceof Controller);

$this->breadcrumbs = [
	'Sources de lien',
];
?>

<h1>Sources de liens</h1>

<p>
	<?php echo CHtml::link("Ajouter une nouvelle source…", ['create'], ['class' => 'btn btn-primary']); ?>
</p>

<?php
$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'partenaire-grid',
		'dataProvider' => $model->search(),
		'filter' => $model, // null to disable
		'columns' => [
			'nom',
			'nomlong',
			'nomcourt',
			//[ 'name' => 'url', 'filter' => false, ],
			[
				'name' => 'saisie',
				'type' => 'boolean',
				'filter' => false,
			],
			[
				'header' => 'Logo',
				'type' => 'raw',
				'value' => function (\Sourcelien $data) {
					return ($data->hasLogo() ? $data->nomcourt : '<b>Non</b>');
				},
			],
			'hdateCrea:datetime',
			[
				'class' => 'BootButtonColumn', // CButtonColumn
				'template' => '{update}{delete}',
				'deleteButtonOptions' => ['title' => "Supprimer cette source"],
				'deleteConfirmation' => "Voulez-vous vraiment supprimer cette source ?",
				'updateButtonOptions' => ['title' => "Modifier cette source"],
			],
		],
		'ajaxUpdate' => false,
	]
);
