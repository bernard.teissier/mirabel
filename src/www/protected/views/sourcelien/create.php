<?php

/** @var Controller $this */
/** @var Sourcelien $model */
assert($this instanceof Controller);

$this->breadcrumbs = [
	'Sources de liens' => ['admin'],
	'Créer',
];
?>

<h1>Nouvelle source de liens</h1>

<?php
echo $this->renderPartial('_form', ['model' => $model]);
?>
