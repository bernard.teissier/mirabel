<?php

/** @var ConfigController $this */
/** @var Config $model */

assert($this instanceof ConfigController);

$this->breadcrumbs = [
	'Admin' => ['/admin/index'],
	'Paramètres' => ['/config/index'],
	($model->category ?: 'Tous'),
];

$this->menu = [
	['label' => 'Créer un paramètre', 'url' => ['create']],
];
?>

<h1>Paramètres : <?= CHtml::encode($model->category ?: 'Tous') ?></h1>

<?php
if (!$model->category) {
	?>
	<p>
		Vous pouvez saisir un opérateur de comparaison
		(<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
		ou <b>=</b>) au début de chaque zone de recherche afin de désigner le mode de comparaison.
	</p>
	<?php
}
?>

<?php
$columns = [
	[
		'name' => 'category',
		'visible' => !$model->category,
	],
	'key',
	'value:ntext',
	'description:html',
	'updatedOn:date',
	[
		'class' => 'BootButtonColumn',
		'template' => '{update}',
		'updateButtonUrl' => function (Config $data) {
			return Yii::app()->createUrl("/config/set", ["id" => $data->id]);
		},
	],
];
$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'config-grid',
		'dataProvider' => $model->search(false),
		'filter' => ($model->category && $model->category !== 'autres') ? null : $model, // null to disable
		'columns' => $columns,
		'ajaxUpdate' => false,
		'rowCssClassExpression' => function (int $rownum, Config $data) {
			if ($data->required && !$data->updatedOn) {
				return 'error';
			}
			return '';
		},
	]
);
?>
