<?php

/** @var Controller $this */
/** @var Config $model */
assert($this instanceof Controller);

$this->breadcrumbs = [
	'Paramètres' => ['admin'],
	$model->key,
];

$this->menu = [
	['label' => 'Créer un paramètre', 'url' => ['create']],
	['label' => 'Modifier le paramètre', 'url' => ['update', 'id' => $model->id]],
	['label' => 'Supprimer le paramètre', 'url' => '#', 'linkOptions' =>
		['submit' => ['delete', 'id' => $model->id], 'confirm' => 'Êtes-vous sûr de vouloir supprimer ce paramètre ?'],
	],
	['label' => 'Liste des paramètres', 'url' => ['admin']],
];
?>

<h1>Paramètre <em><?php echo $model->key; ?></em></h1>

<?php
$this->widget(
	'bootstrap.widgets.BootDetailView',
	[
		'data' => $model,
		'attributes' => [
			'id',
			'category',
			'key',
			[
				'name' => 'value',
				'type' => 'raw',
				'value' => function (Config $data) {
					if ($data->type === 'html') {
						return $data->decode();
					} else {
						return CHtml::encode(print_r($data->decode(), true));
					}
				},
			],
			'type',
			'descriptionraw',
			'createdOn:datetime',
			'updatedOn:datetime',
		],
	]
);
?>
