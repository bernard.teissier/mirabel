<?php

/** @var ConfigController $this */
/** @var Config $model */
assert($this instanceof ConfigController);

$this->breadcrumbs = [
	'Paramètres' => ['admin'],
	$model->key => ['view', 'id' => $model->id],
	'Modifier la valeur',
];

$this->menu = [
	['label' => 'Créer un paramètre', 'url' => ['create']],
	['label' => 'Modifier le paramètre', 'url' => ['update', 'id' => $model->id]],
	['label' => 'Liste des paramètres', 'url' => ['admin']],
];
?>

<h1>Modifier la valeur de <em><?= CHtml::encode($model->key) ?></em></h1>

<?php
echo $this->renderPartial('_form', ['model' => $model, 'valueOnly' => true]);
