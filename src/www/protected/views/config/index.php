<?php

/** @var ConfigController $this */
/** @var array $categories */
assert($this instanceof ConfigController);

$this->breadcrumbs = [
	'Paramètres',
];
?>

<h1>Paramètres de configuration</h1>

<h2>Paramètres fixés par le fichier de configuration</h2>
<table class="table">
	<thead>
		<tr>
			<th>Clé</th>
			<th>Valeur</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach (Yii::app()->params as $k => $v) {
			echo "<tr>"
				. "<th>" . CHtml::encode($k) . "</th>"
				. "<td><pre>" . CHtml::encode(var_export($v, true)) . "</pre></td>"
				. "</tr>";
		}
		?>
	</tbody>
</table>

<h2>Paramètres modifiables en ligne</h2>
<ul>
	<?php
	foreach ($categories as $c) {
		echo CHtml::tag('li', [], CHtml::link($c, ['/config/admin', 'Config' => ['category' => $c]]));
	}
	?>
</ul>
