<?php

/** @var Controller $this */
/** @var string $message */
/** @var int $code */
assert($this instanceof Controller);

$this->pageTitle = Yii::app()->name . " - " . ($code == 404 ? "la page n'existe pas" : "erreur");
$this->breadcrumbs = [
	'Erreur',
];

if ($code == 404) {
	?>
	<h1>Erreur</h1>
	<?php
	if (!empty($message)) {
		?>
		<h2><?= $message ?></h2>
		<div class="error">
			<?= (empty($details) ? "Utilisez le moteur de recherche de Mir@bel pour retrouver la revue que vous recherchez." : $details) ?>
		</div>
	<?php
	} else {
		?>
		<h2>La page demandée n'existe pas</h2>
		<div class="error">
			Utilisez le moteur de recherche de Mir@bel pour retrouver la revue que vous recherchez.
		</div>
	<?php
	}
} else {
	?>
	<h2>Erreur <?= $code ?></h2>

	<?php
	if (!empty($message)) {
		echo "<p class=\"alert alert-error\">$message</p>";
	}
	if (Yii::app()->user->isGuest || empty(Yii::app()->params['mantisId'])) {
		if ($code != 403) {
			?>
			<div class="error">
				L'application a rencontré une erreur fatale.
				Son signalement a été enregistré, et nous ferons de notre mieux pour la corriger rapidement.
			</div>
			<?php
		}
	} else {
		?>
		<div class="error">
			<h2><?= CHtml::encode($message) ?></h2>
			<p class="alert alert-error">
				Cet affichage détaillé vous est accessible car vous êtes un utilisateur authentifié.
			</p>
			<p>
				Cette erreur a été enregistrée dans les logs de l'application,
				elle est donc accessible aux administrateurs du site.
				Si vous pensez que c'est nécessaire,
				vous pouvez créer un ticket de bug avec le bouton ci-dessous.
			</p>
			<?php
			$this->renderPartial('/system/_bugMantis', ['bugData' => $message]);
			?>
		</div>
		<?php
	}
}
