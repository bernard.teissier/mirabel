<?php

function traceFormat($trace)
{
	if (empty($trace)) {
		return '';
	}
	$msg = '';
	$count = 0;
	foreach ($trace as $row) {
		$msg .= "{$count}. " . $row['file'] . ' l.' . $row['line'] . "\n";
		$count++;
	}
	return $msg;
}
