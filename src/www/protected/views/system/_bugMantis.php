<?php

/** @var Controller $this */
/** @var array $bugData */
assert($this instanceof Controller);

require_once __DIR__ . '/_helpers.php';

$summary = '';
if (!empty(Yii::app()->request->pathInfo)) {
	$summary = "Bug " . Yii::app()->request->pathInfo;
}
$info = Yii::app()->params['vcsVersion'] . "\n\n";
if (!empty($bugData)) {
	if (is_string($bugData)) {
		$info .= $bugData;
	} elseif (isset($bugData['trace'])) {
		$info .= $bugData['type'] . "\n" . $bugData['message']
			. "\n\n" . $bugData['file'] . " l." . $bugData['line']
			. "\n\n" . ($bugData['trace']);
	} else {
		$info .= $bugData['type'] . "\n" . $bugData['message']
			. "\n\n" . $bugData['file'] . " l." . $bugData['line']
			. "\n\n" . traceFormat($bugData['traces']);
	}
}

$steps = "{$_SERVER['SERVER_NAME']}
{$_SERVER['REQUEST_METHOD']} {$_SERVER['REQUEST_URI']}
" . (empty($_POST) ? "" : "POST: " . var_export($_POST, true));

?>
<div style="text-align: right">
<div style="display: inline-block; border: thin solid black; margin-top: 1ex; padding: 1ex;  text-align: left;">
	<strong>Créer un ticket dans Mantis</strong>
	<form action="https://tickets.silecs.info/plugin.php?page=TicketCreation/bugreport" method="post" target="_blank">
		<p>
			Le bouton suivant ouvre une nouvelle page avec un formulaire partiellement rempli de Mantis.
			<input type="hidden" name="project_id" value="<?= (int) Yii::app()->params['mantisId'] ?>" />
			<input type="hidden" name="summary" value="<?= htmlspecialchars($summary) ?>" />
			<input type="hidden" name="description" value="" />
			<input type="hidden" name="steps_to_reproduce" value="<?= htmlspecialchars($steps) ?>" />
			<input type="hidden" name="additional_info" value="<?= htmlspecialchars($info) ?>" />
			<button type="submit">Soumettre un bug</button>
		</p>
	</form>
</div>
</div>
