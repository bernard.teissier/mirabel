<?php

require_once dirname(__DIR__, 3) . '/vendor/autoload.php';
spl_autoload_register(['YiiBase', 'autoload']);

$config = CMap::mergeArray(
	CMap::mergeArray(require __DIR__ . '/config/main.php', require __DIR__ . '/config/local.php'),
	require __DIR__ . '/config/console.php'
);

// fix for fcgi
defined('STDIN') or define('STDIN', fopen('php://stdin', 'r'));

require __DIR__ . '/YiiConsole.php';
$app = Yii::createConsoleApplication($config);
$app->commandRunner->addCommands(YII_PATH . '/cli/commands');
if ($app->params->contains('baseUrl')) {
	$app->getComponent('urlManager')->setBaseUrl($app->params->itemAt('baseUrl'));
	$app->getRequest()->setScriptUrl($app->params->itemAt('baseUrl'));
}

$env = @getenv('YII_CONSOLE_COMMANDS');
if (!empty($env)) {
	$app->commandRunner->addCommands($env);
}

$app->run();
