<?php

require_once dirname(dirname(__DIR__)) . '/vendor/autoload.php';
spl_autoload_register(array('YiiBase','autoload'));

require __DIR__ . '/protected/components/FeedGenerator.php';

ini_set('date.timezone', 'Europe/Paris');

$type = 'atom';
if (isset($_GET['type'])) {
	$type = $_GET['type'];
}

$gen = new FeedGenerator($type);
$gen->showValidatedOnly(true);
foreach (array('revue', 'ressource', 'editeur') as $o) {
	if (!empty($_GET[$o])) {
		$id = (int) $_GET[$o];
		$gen->findFeedInFiles($o, $id);
		$gen->addCriteria($o . 'Id', $id);
	}
}
$gen->run();
