Placer ici les logos de partenaires au format suivant :

* extension png ou jpg (en minuscules)
* nom sur 3 chiffres contenant l'identifiant du partenaire
* si possible, hauteur de 55 pixels

Par exemple : "012.png" pour le logo associé à l'ID 12.

