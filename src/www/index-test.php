<?php
/**
 * This is the bootstrap file for test application.
 */

require_once dirname(__DIR__, 2) . '/vendor/autoload.php';

spl_autoload_register(array('YiiBase','autoload'));
require_once __DIR__ . '/protected/components/Yii.php';
require_once dirname(__DIR__, 2) . '/vendor/codeception/yii-bridge/yiit.php';

defined('YII_ENV') or define('YII_ENV', 'test');

$mainConfig = require __DIR__ . '/protected/config/main.php';
$localConfig = require __DIR__ . '/protected/config/local.php';
$testConfig = require __DIR__ . '/protected/config/test.php';
$config = CMap::mergeArray(
	CMap::mergeArray($mainConfig, $localConfig),
	$testConfig
);
unset($mainConfig, $localConfig, $testConfig);

$config['basePath'] = __DIR__ . '/protected';

$_SERVER['SERVER_NAME'] = 'localhost';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

//Yii::createWebApplication($config)->run();
return [
	'class' => 'WebApplication',
	'config' => $config,
];
