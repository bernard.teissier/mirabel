This directory can contain the Sphinx binaries.

If your Sphinx search server is not installed globally on the system,
then you can put its executables binaries here.

