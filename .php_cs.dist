<?php

// php-cs-fixer fix --allow-risky=yes 

/*
 * Most of this settings come from the rule set @PhpCsFixer:
 * https://github.com/FriendsOfPHP/PHP-CS-Fixer/blob/master/doc/ruleSets/PhpCsFixer.rst
 * A few are from @Symfony.
 *
 * The main change is the indentation with \t.
 */

$finder = PhpCsFixer\Finder::create()
	->in("src/www/protected")
	->notPath('src/www/protected/migrations/template.php')
	->exclude('src/www/protected/runtime')
	->exclude('src/www/protected/views')
;

$config = new \PhpCsFixer\Config('yii1');
if ((new \PhpCsFixer\ToolInfo())->getVersion() >= 3) {
	$config->setRules([
		'clean_namespace' => true,
		'echo_tag_syntax' => ['format' => 'short'],
		'single_space_after_construct' => true,
	]);
}
return $config
	->setRules([
		'@PSR2' => true,
		'@PHP73Migration' => true,
		'align_multiline_comment' => true,
		'array_indentation' => true,
		'array_syntax' => ['syntax' => 'short'],
		'cast_spaces' => ['space' => 'single'],
		'class_attributes_separation' => true,
		'concat_space' => ['spacing' => 'one'],
		'dir_constant' => true,
		'explicit_indirect_variable' => true, // ${$a} $x->{$a}
		'function_typehint_space' => true,
		'heredoc_indentation' => false, // ['indentation' => 'same_as_start']
		'is_null' => true,
		'method_argument_space' => ['on_multiline' => 'ensure_fully_multiline'],
		'method_chaining_indentation' => true,
		'native_function_casing' => true,
		'native_function_type_declaration_casing' => true,
		'no_superfluous_elseif' => true,
		'no_useless_else' => true,
		'no_useless_return' => true,
		'no_whitespace_in_blank_line' => true,
		'normalize_index_brace' => true,
		'object_operator_without_whitespace' => true,
		'ordered_class_elements' => true, // traits, then public properties, etc.
		'phpdoc_order' => true,
		//'phpdoc_separation' => true,
		'phpdoc_scalar' => true,
		'return_assignment' => true,
		'simple_to_complex_string_variable' => true,
		'single_blank_line_at_eof' => true,
		'trailing_comma_in_multiline_array' => true,
		'unary_operator_spaces' => true,
		'whitespace_after_comma_in_array' => true,
	])
	->setIndent("\t")
	->setLineEnding("\n")
	->setFinder($finder)
;
